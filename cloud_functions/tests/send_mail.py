from app.send_mail import main
import environ
import unittest
from unittest.mock import MagicMock, patch
import os
import shutil
import base64

TEST_FILE_DIR = os.path.join(os.path.dirname(__file__), '__temp__/')
PDF_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.pdf')
MS_WORD_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.docx')
MS_EXCEL_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.xlsx')
MS_PPT_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.pptx')
JPG_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.jpg')
PNG_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.png')
HTML_FILE_PATH = os.path.join(TEST_FILE_DIR, 'test_send_mail.html')

class Request:
    args = None

    def __init__(self, payload):
        self.payload = payload

    def get_json(self):
        return self.payload



class TestSendMail(unittest.TestCase):
    """ Test Class of send_mail/main.py
    """
    def setUp(self):
        super().setUpClass()

        if not os.path.exists(TEST_FILE_DIR):
            os.makedirs(os.path.dirname(TEST_FILE_DIR))

        with open(PDF_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(MS_WORD_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(MS_EXCEL_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(MS_PPT_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(JPG_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(PNG_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        with open(HTML_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        smtp = {
            'host': 'EMAIL_HOST',
            'port': 'EMAIL_PORT',
            'user': 'EMAIL_HOST_USER',
            'password': 'EMAIL_HOST_PASSWORD',
            'tls': True,
            'ssl': False,
        }

        self.payload = {
            'total': 2,
            'email_id': 'c654906086e941bb81235a95281f965f',
            'attachments': [
                {'filename': PDF_FILE_PATH},
                {'filename': MS_WORD_FILE_PATH},
                {'filename': MS_EXCEL_FILE_PATH},
                {'filename': MS_PPT_FILE_PATH},
                {'filename': JPG_FILE_PATH},
                {'filename': PNG_FILE_PATH},
                {'filename': HTML_FILE_PATH},
            ],
            'type': 'normal',
            'smtp': smtp,
            'contexts': [
                # Text / Mailアドレスの日本語名前あり / CCあり
                {
                    'contact_id': '06243f2aea614dbcac4d0b3a993ab2e2',
                    'subject': 'テストメール0',
                    'message': 'メッセージ0',
                    'to': 'ほかげ くん <hokage@example.com>',
                    'from': 'ほげちゃん  <hoge@example.com>',
                    'format': 'text',
                    'cc': ['CCちゃん<hoge3@example.com>'],
                },

                # HTML / Mailアドレスの日本語名前なし / CCなし
                {
                    'contact_id': '22bc9a96-5ff6-4dd3-ab0b-8583dd5f0b01',
                    'subject': 'テストメール1',
                    'message': 'メッセージ1',
                    'to': 'huke@example.com',
                    'from': 'hoge@example.com',
                    'format': 'html',
                    'cc': [],
                },
            ]
        }

        return True

    def tearDown(self):
        shutil.rmtree(path=TEST_FILE_DIR)
        return True

    def test_send_mail(self):
        mock = MagicMock()
        with patch('app.send_mail.main.smtplib.SMTP', return_value=mock):
            # ToDo: _create_resultsに対するテストは今後作成するとし、今回は一旦モック化。
            with patch('app.send_mail.main._create_results', return_value=mock):
                main.send_mail(Request(self.payload))
                call_args, _ = mock.send_message.call_args_list[0]
                email_message = call_args[0]
                message0 = email_message.as_string()

                self.assertTrue('Subject: =?utf-8?b?%s' % base64.b64encode('テストメール0'.encode()).decode() in message0)
                self.assertTrue('Content-Type: text/plain;' in message0)
                self.assertTrue('From: =?utf-8?b?%s?= <hoge@example.com>' % base64.b64encode('ほげちゃん'.encode()).decode() in message0)
                self.assertTrue('To: =?utf-8?b?%s?= <hokage@example.com>' % base64.b64encode('ほかげ くん'.encode()).decode() in message0)
                self.assertTrue('Cc: =?utf-8?b?%s?= <hoge3@example.com>' % base64.b64encode('CCちゃん'.encode()).decode() in message0)
                self.assertTrue('DKIM-Signature:' in message0)
                self.assertTrue('Content-Type: text/plain; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\n\n%s' % base64.b64encode('メッセージ0'.encode()).decode() in message0)
                self.assertTrue('Content-Type: application/pdf\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pdf' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.docx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.xlsx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pptx' in message0)
                self.assertTrue('Content-Type: image/png\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.png' in message0)
                self.assertTrue('Content-Type: image/jpeg\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.jpg' in message0)
                self.assertTrue('Content-Type: text/html\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.html' in message0)


                call_args, _ = mock.send_message.call_args_list[1]
                email_message = call_args[0]
                message1 = email_message.as_string()

                self.assertTrue('Subject: =?utf-8?b?%s' % base64.b64encode('テストメール1'.encode()).decode() in message1)
                self.assertTrue('From: hoge@example.com' in message1)
                self.assertTrue('To: huke@example.com' in message1)
                self.assertTrue('DKIM-Signature:' in message1)
                self.assertTrue('Content-Type: text/html; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\n\n%s' % base64.b64encode('メッセージ1'.encode()).decode() in message1)
                self.assertTrue('Content-Type: application/pdf\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pdf' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.docx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.xlsx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pptx' in message0)
                self.assertTrue('Content-Type: image/png\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.png' in message0)
                self.assertTrue('Content-Type: image/jpeg\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.jpg' in message0)
                self.assertTrue('Content-Type: text/html\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.html' in message0)

    def test_send_mail_ssl(self):
        self.smtp['tls'] = False
        self.smtp['ssl'] = True
        mock = MagicMock()
        # ssl = Trueにしている場合はコネクションクラスにsmtplib.SMTP_SSLが使われる
        with patch('app.send_mail.main.smtplib.SMTP_SSL', return_value=mock):
            # ToDo: _create_resultsに対するテストは今後作成するとし、今回は一旦モック化。
            with patch('app.send_mail.main._create_results', return_value=mock):
                main.send_mail(Request(self.payload))
                call_args, _ = mock.send_message.call_args_list[0]
                email_message = call_args[0]
                message0 = email_message.as_string()

                self.assertTrue('Subject: =?utf-8?b?%s' % base64.b64encode('テストメール0'.encode()).decode() in message0)
                self.assertTrue('Content-Type: text/plain;' in message0)
                self.assertTrue('From: =?utf-8?b?%s?= <hoge@example.com>' % base64.b64encode('ほげちゃん'.encode()).decode() in message0)
                self.assertTrue('To: =?utf-8?b?%s?= <hokage@example.com>' % base64.b64encode('ほかげ くん'.encode()).decode() in message0)
                self.assertTrue('Cc: =?utf-8?b?%s?= <hoge3@example.com>' % base64.b64encode('CCちゃん'.encode()).decode() in message0)
                self.assertTrue('DKIM-Signature:' in message0)
                self.assertTrue('Content-Type: text/plain; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\n\n%s' % base64.b64encode('メッセージ0'.encode()).decode() in message0)
                self.assertTrue('Content-Type: application/pdf\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pdf' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.docx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.xlsx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pptx' in message0)
                self.assertTrue('Content-Type: image/png\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.png' in message0)
                self.assertTrue('Content-Type: image/jpeg\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.jpg' in message0)
                self.assertTrue('Content-Type: text/html\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.html' in message0)


                call_args, _ = mock.send_message.call_args_list[1]
                email_message = call_args[0]
                message1 = email_message.as_string()

                self.assertTrue('Subject: =?utf-8?b?%s' % base64.b64encode('テストメール1'.encode()).decode() in message1)
                self.assertTrue('From: hoge@example.com' in message1)
                self.assertTrue('To: huke@example.com' in message1)
                self.assertTrue('DKIM-Signature:' in message1)
                self.assertTrue('Content-Type: text/html; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\n\n%s' % base64.b64encode('メッセージ1'.encode()).decode() in message1)
                self.assertTrue('Content-Type: application/pdf\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pdf' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.docx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.xlsx' in message0)
                self.assertTrue('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.pptx' in message0)
                self.assertTrue('Content-Type: image/png\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.png' in message0)
                self.assertTrue('Content-Type: image/jpeg\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.jpg' in message0)
                self.assertTrue('Content-Type: text/html\nMIME-Version: 1.0\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename="test_send_mail.html' in message0)
