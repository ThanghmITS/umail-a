import traceback
import sys
import os
from importlib import import_module
from dotenv import load_dotenv
import unittest
import inspect

# ToDo: CloudFunction 内の環境変数を入れる
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

sys.path.append(os.path.dirname(__file__))

def run_test(cls):
    """
    Runs unit tests from a test class
    """
    suite = unittest.TestLoader().loadTestsFromTestCase(cls)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)
    return True

if len(sys.argv) == 2:
    try:
        module = import_module("tests.%s" % sys.argv[1])
        for name, member in inspect.getmembers(module):
            if inspect.isclass(member):
                if name.startswith("Test"):
                   run_test(member)

    except Exception as e:
        print("Test `%s` failed: " % sys.argv[1])
        print(traceback.format_exc())
