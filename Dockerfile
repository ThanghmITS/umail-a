FROM python:3.6.8
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y sudo

# 下記の問題が解消されるまではnodesourceにソース元を記載する方式でインストールを行う
# https://github.com/nodesource/distributions/issues/1266
# 解消されたらこっちに戻す
# RUN curl -sLk https://deb.nodesource.com/setup_11.x | sudo -E bash -
# RUN apt-get update && sudo apt-get install -y npm
RUN curl -s http://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
RUN sudo sh -c "echo deb http://deb.nodesource.com/node_16.x stretch main > /etc/apt/sources.list.d/nodesource.list"
RUN apt-get update && sudo apt-get install -y npm

WORKDIR /code

RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh  \
 -O ./wait-for-it.sh \
 && chmod 755 ./wait-for-it.sh

COPY ./requirements/ requirements/
COPY ./package.json ./

RUN pip install -r requirements/production.txt
RUN npm install

RUN node -v
RUN npm -v

COPY . ./
RUN npm run build

CMD gunicorn --env DJANGO_SETTINGS_MODULE=config.settings.production config.wsgi
