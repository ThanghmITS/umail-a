"""
This setting inherits base settings and override values.
"""

from .production import *

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
ENABLE_INTEGRATION_TEST = True
