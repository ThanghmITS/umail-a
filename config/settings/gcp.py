import json
from .base import *
from google.oauth2 import service_account

DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'

# Load a JSON string of a Google Service Account credential.
GS_CREDENTIAL_INFO = json.loads(env('GS_CREDENTIAL_INFO'))

GS_CREDENTIALS = service_account.Credentials.from_service_account_info(
    GS_CREDENTIAL_INFO
)

GS_BUCKET_NAME = env('GS_BUCKET_NAME')

EMAIL_BACKEND = 'app_staffing.email_backend.DKIMBackend'

CHECK_EMAIL_EXIST_PORT = 465

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': env('DB_HOST'),
        'PORT': env('DB_PORT'),
        'CONN_MAX_AGE': 360,
        'TEST': {
            'NAME': 'django-staffing-test',
        },
    }
}
