from .gcp import *

MAIL_OPEN_COUNT_ACCESS_POINT = 'https://asia-northeast1-dev-umail-303302.cloudfunctions.net/dev-send-mail-open'
SCHEDULED_EMAIL_PROMOTION_IMAGE_URL = 'https://storage.googleapis.com/umail-development-static/cmrb.png'
SCHEDULED_EMAIL_PROMOTION_MESSAGE = f'''
<br />
    <span style="padding: 5px;display: inline-block;background-color:#f5f5f5;width: 68%;">
        <img src='{SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' style="width: 29%;"/>
        <span style="font-size:x-small;vertical-align:bottom;height: 20px;display: inline-block;">
            普段のメール業務を、より簡単に
        </span>
        <span style="margin-left:50px;margin-right:10px;margin-top:15px;background-color:#ffffe0;padding-left:15px;padding-right:15px;display: inline-block;float: right;">
            <a style="color:#000000;" href='https://cmrb.jp' target="_blank" rel="noopener noreferrer">
                詳しく
            </a>
        </span>
    </span>
<br />
'''
