"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include

from rest_framework.documentation import include_docs_urls

from config.views import CustomAuthTokenView, Logout
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

from app_staffing.views.admin import AdminBatchPaymentView, AdminBatchCalculateStatsView, AdminDisplayOrganizationGroupModelsView, AdminDisplayEmailGroupModelsView, AdminDisplayScheduledEmailGroupModelsView, AdminDisplayOtherGroupModelsView

urlpatterns = staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
    # Views per server.
    path('health-check', include('health_check.urls')),
    path('robots.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('admin/', admin.site.urls),
    re_path('^admin/batch_payment/?', AdminBatchPaymentView.as_view(), name='batch_payment'),
    re_path('^admin/batch_calculate_stats/?', AdminBatchCalculateStatsView.as_view(), name='batch_calculate_stats'),
    re_path('^admin/display_organization_group_models/?', AdminDisplayOrganizationGroupModelsView.as_view(), name='display_organization_group_models'),
    re_path('^admin/display_email_group_models/?', AdminDisplayEmailGroupModelsView.as_view(), name='display_email_group_models'),
    re_path('^admin/display_scheduled_email_group_models/?', AdminDisplayScheduledEmailGroupModelsView.as_view(), name='display_scheduled_email_group_models'),
    re_path('^admin/display_other_group_models/?', AdminDisplayOtherGroupModelsView.as_view(), name='display_other_group_models'),
    path('auth/', include('rest_framework.urls')),
    path('api-token-auth/', CustomAuthTokenView.as_view(), name='auth'),
    path('api-token-auth/logout', Logout.as_view(), name='logout'),
    path('docs/', include_docs_urls(title='Staffing API', description='For deeper analysis.')),  # TODO: This Not works yet.
    # Views per app.
    path('app_staffing/', include('app_staffing.urls')),
    path('', include('frontend.urls')),
]
