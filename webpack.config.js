const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleTracker = require("webpack-bundle-tracker");
const WriteFilePlugin = require("write-file-webpack-plugin");

const themeColor = "#FFFFD2";
const primaryColor = "#A7CF69";
const dangerColor = "#E1585A";

module.exports = (env, args) => {
    const isDevMode = args.mode === "development";
    return [
        {
            devtool: isDevMode ? "inline-source-map" : "source-map",
            entry: "./frontend/src/index.jsx",
            output: {
                filename: "main.js",
                path: path.resolve(__dirname, "static_npm"),
            },
            plugins: [
                new WriteFilePlugin(),
                new BundleTracker({ filename: "./webpack-stats.json" }),
            ],
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        use: {
                            loader: "babel-loader",
                        },
                    },
                    {
                        test: /\.scss$/, // for custom SCSS
                        use: [
                            "style-loader",
                            {
                                loader: "css-loader",
                                options: {
                                    modules: {
                                        localIdentName:
                                            "[name]-[local]-[hash:base64:5]",
                                    },
                                },
                            },
                            "sass-loader",
                        ],
                    },
                    {
                        test: /\.less$/, // for Ant Design builtin style
                        use: [
                            "style-loader",
                            {
                                loader: "css-loader",
                            },
                            {
                                loader: "less-loader",
                                options: {
                                    lessOptions: {
                                        modifyVars: {
                                            // Customize default Style
                                            // We need to change font-family from the default values to display Japanese characters correctly.
                                            "font-family":
                                                "'Avenir','Helvetica Neue','Helvetica','Arial','Hiragino Sans','ヒラギノ角ゴシック',YuGothic,'Yu Gothic','メイリオ', Meiryo,'ＭＳ Ｐゴシック','MS PGothic', 'sans-serif'",
                                            "font-size-base": "13px",
                                            "font-size-sm": "11px",
                                            "page-header-heading-title": "23px",
                                            "comment-padding-base":
                                                "5px 0px 8px",
                                            "primary-color": primaryColor,
                                            "info-color": "#1990FF",
                                            "error-color": dangerColor,
                                            "layout-header-background":
                                                themeColor,
                                            "layout-footer-background":
                                                themeColor,
                                            "comment-nest-indent": "32px",
                                            "tooltip-bg":
                                                "rgba(97, 87, 72, 0.95)",
                                            "progress-default-color":
                                                primaryColor,
                                        },
                                        javascriptEnabled: true,
                                        sourceMap: true,
                                    },
                                },
                            },
                        ],
                    },
                    {
                        test: /\.(ts|tsx)$/,
                        use: ["ts-loader"],
                        exclude: /node_modules/,
                    },
                    {
                        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                        issuer: /\.[jt]sx?$/,
                        use: ["babel-loader", "@svgr/webpack", "file-loader"],
                        exclude: /node_modules/,
                    },
                    {
                        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                        loader: "file-loader",
                        exclude: /node_modules/,
                    },
                ],
            },
            resolve: {
                alias: {
                    "~": path.resolve(__dirname, "./frontend/src/"),
                    coreStylesheet: path.resolve(
                        __dirname,
                        "./frontend/stylesheets/constants.scss"
                    ),
                },
                extensions: [".js", ".jsx", ".ts", ".tsx"],
                conditionNames: ["..."],
                fallback: {
                    assert: false,
                    stream: false,
                    zlib: false,
                    zlib_bindings: false,
                    url: false,
                },
            },
        },
        {
            entry: "./frontend/stylesheets/index.scss",
            plugins: [
                new MiniCssExtractPlugin({
                    filename: "[name].css",
                    chunkFilename: "[id].css",
                }),
            ],
            output: {
                path: path.resolve(__dirname, "static"),
            },
            module: {
                rules: [
                    {
                        test: /\.scss$/, // 対象となるファイルの拡張子
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    publicPath: path.resolve(
                                        __dirname,
                                        "static"
                                    ),
                                },
                            },
                            "css-loader",
                            "sass-loader",
                        ],
                    },
                ],
            },
        },
    ];
};
