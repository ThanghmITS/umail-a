from django.contrib import admin

from app_staffing.models.email import ScheduledEmailOpenHistory


class ScheduledEmailOpenHistoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ScheduledEmailOpenHistory, ScheduledEmailOpenHistoryAdmin)
