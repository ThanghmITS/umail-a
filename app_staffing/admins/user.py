from django.contrib import admin

from app_staffing.admins.base import ModelAdminBase
from app_staffing.models.user import UserRole


class UserRoleAdmin(ModelAdminBase):
    pass


admin.site.register(UserRole, UserRoleAdmin)
