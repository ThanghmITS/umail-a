from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.tests import DummyModel

class DummyAdmin(ModelAdminBase):
    pass


admin.site.register(DummyModel, DummyAdmin)
