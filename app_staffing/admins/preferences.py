from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.preferences import (
    Location,
    ContactPreference,
    TypePreference,
    SkillPreference,
    ContactJobTypePreference,
    ContactJobSkillPreference,
    ContactPersonnelTypePreference,
    ContactPersonnelSkillPreference,
)

class LocationAdmin(ModelAdminBase):
    pass


admin.site.register(Location, LocationAdmin)


class ContactPreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(ContactPreference, ContactPreferenceAdmin)


class TypePreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(TypePreference, TypePreferenceAdmin)


class SkillPreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(SkillPreference, SkillPreferenceAdmin)


class ContactJobTypePreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(ContactJobTypePreference, ContactJobTypePreferenceAdmin)


class ContactJobSkillPreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(ContactJobSkillPreference, ContactJobSkillPreferenceAdmin)


class ContactPersonnelTypePreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(ContactPersonnelTypePreference, ContactPersonnelTypePreferenceAdmin)


class ContactPersonnelSkillPreferenceAdmin(ModelAdminBase):
    pass


admin.site.register(ContactPersonnelSkillPreference, ContactPersonnelSkillPreferenceAdmin)
