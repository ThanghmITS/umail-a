from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.addon import (
    AddonMaster,
    AddonTarget,
    AddonTargetAssignment,
    AddonParentAssignment,
    Addon,
)

class AddonMasterAdmin(ModelAdminBase):
    search_fields = ("title", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('addon_target')

    def addon_target(self, obj):
        return ",\n".join(x.addon_target.name for x in obj.target_assignment.all())
    addon_target.short_description = "対象"


admin.site.register(AddonMaster, AddonMasterAdmin)


class AddonTargetAdmin(ModelAdminBase):
    pass


admin.site.register(AddonTarget, AddonTargetAdmin)


class AddonTargetAssignmentAdmin(ModelAdminBase):
    pass


admin.site.register(AddonTargetAssignment, AddonTargetAssignmentAdmin)


class AddonParentAssignmentAdmin(ModelAdminBase):
    pass


admin.site.register(AddonParentAssignment, AddonParentAssignmentAdmin)


class AddonAdmin(ModelAdminBase):
    pass


admin.site.register(Addon, AddonAdmin)
