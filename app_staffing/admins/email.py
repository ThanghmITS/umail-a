from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.email import (
    Email,
    EmailAttachment,
    ScheduledEmail,
    ScheduledEmailAttachment,
    ScheduledEmailTarget,
    MailboxMapping,
    SMTPServer,
    SharedEmailSetting,
    EmailComment,
    ScheduledEmailStatus,
    ScheduledEmailSendType,
    SharedEmailTransferHistory,
    ScheduledEmailSetting,
)

class EmailAdmin(ImportExportModelAdmin, ModelAdminBase):
    class EmailResource(resources.ModelResource):
        class Meta:
            model = Email
            skip_unchanged = True
            fields = ('mail_id', 'from_address', 'subject', 'body', 'received_date', 'attachment_names', 'category')
            import_id_fields = ['mail_id']

    resource_class = EmailResource


admin.site.register(Email, EmailAdmin)


class EmailAttachmentAdmin(ModelAdminBase):
    pass


admin.site.register(EmailAttachment, EmailAttachmentAdmin)


class ScheduledEmailAdmin(ModelAdminBase):
    search_fields = ("subject", "text", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('scheduled_email_status_name')
        self.list_display.append('scheduled_email_send_type_name')

    def scheduled_email_status_name(self, obj):
        message = "設定されていません"
        scheduled_email_status = obj.scheduled_email_status
        if not scheduled_email_status:
            return message
        name = scheduled_email_status.name
        if name:
            return name
        return message
    scheduled_email_status_name.short_description = "配信ステータス"

    def scheduled_email_send_type_name(self, obj):
        message = "設定されていません"
        scheduled_email_send_type = obj.scheduled_email_send_type
        if not scheduled_email_send_type:
            return message
        name = scheduled_email_send_type.name
        if name:
            return name
        return message
    scheduled_email_send_type_name.short_description = "配信種類"


admin.site.register(ScheduledEmail, ScheduledEmailAdmin)


class ScheduledEmailAttachmentAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailAttachment, ScheduledEmailAttachmentAdmin)


class ScheduledEmailTargetAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailTarget, ScheduledEmailTargetAdmin)


class MailboxMappingAdmin(ModelAdminBase):
    pass


admin.site.register(MailboxMapping, MailboxMappingAdmin)


class SMTPServerAdmin(ModelAdminBase):
    pass


admin.site.register(SMTPServer, SMTPServerAdmin)


class SharedEmailSettingAdmin(ModelAdminBase):
    pass


admin.site.register(SharedEmailSetting, SharedEmailSettingAdmin)


class EmailCommentAdmin(ModelAdminBase):
    pass

admin.site.register(EmailComment, EmailCommentAdmin)


class ScheduledEmailStatusAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailStatus, ScheduledEmailStatusAdmin)


class ScheduledEmailSendTypeAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailSendType, ScheduledEmailSendTypeAdmin)


class SharedEmailTransferHistoryAdmin(ModelAdminBase):
    pass


admin.site.register(SharedEmailTransferHistory, SharedEmailTransferHistoryAdmin)


class ScheduledEmailSettingAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailSetting, ScheduledEmailSettingAdmin)
