from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.notification import (
    EmailNotificationRule,
    EmailNotificationCondition,
    SystemNotification,
    UserSystemNotification,
)

class EmailNotificationRuleAdmin(ModelAdminBase):
    pass

admin.site.register(EmailNotificationRule, EmailNotificationRuleAdmin)


class EmailNotificationConditionAdmin(ModelAdminBase):
    pass


admin.site.register(EmailNotificationCondition, EmailNotificationConditionAdmin)


class SystemNotificationAdmin(ModelAdminBase):
    search_fields = ("title", )


admin.site.register(SystemNotification, SystemNotificationAdmin)


class UserSystemNotificationAdmin(ModelAdminBase):
    pass


admin.site.register(UserSystemNotification, UserSystemNotificationAdmin)
