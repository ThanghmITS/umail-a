{% autoescape off %}
以下の条件に該当したメールを検知しました。
ログインし、ご確認ください。
{% for rule_id, item in rules_and_emails.items %}
ルール名：{{ item.rule_name }}
ルール条件：{{ item.master_rule }}：{{ item.conditions }}
{% for email_id in item.email_ids %}
https://{{ domain }}/sharedMails/{{ email_id }}{% endfor %}

{% endfor %}
{% endautoescape %}
