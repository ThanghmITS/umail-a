# About
* This is a directory of Definitions of REST API of the app backend.
# Architecture
* Architecture is based on Django, especially for [Django REST framework](https://www.django-rest-framework.org/).
  * When we use Django REST framework, we use *Serializer* instead of *Form* to map between an interface and a database model.
# Directory structure.
  * Basically, this directory follows the structure of official document of django, but changes are:
    * ***models, views, tests***, are changed from a file to a directory so that we can avoid making fat module.
    * We have ***serializers*** directory instead of *form*, because we are building APIs.
    * We have original ***estimators*** directory, which is used for email classification.
      * but not active so far.
    * We have ***services*** directory, which defines app specific logic.
    * We have ***signals*** directory, which defines callback function that should be triggered at receiving an email.
    * ***templates*** directory has a different meaning from the official document.
      * Instead of defining an HTML template, we define a text template for sending an email.
    * We Have ***utils*** directory, which contains helper functions for this app.
