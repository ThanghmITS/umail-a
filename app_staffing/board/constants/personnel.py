PROPER = 'proper'
FREELANCER = 'freelancer'
ONE_COMPANY_PROPER = 'one_company_proper'
ONE_COMPANY_FREELANCER = 'one_company_freelancer'
TWO_COMPANY_PROPER = 'two_company_proper'
TWO_COMPANY_FREELANCER = 'two_company_freelancer'
THREE_COMPANY_PROPER = 'three_company_proper'
THREE_COMPANY_FREELANCER = 'three_company_freelancer'

PERSONNEL_AFFILIATION = (
    (PROPER, 'proper'),
    (FREELANCER, 'freelancer'),
    (ONE_COMPANY_PROPER, 'one_company_proper'),
    (ONE_COMPANY_FREELANCER, 'one_company_freelancer'),
    (TWO_COMPANY_PROPER, 'two_company_proper'),
    (TWO_COMPANY_FREELANCER, 'two_company_freelancer'),
    (THREE_COMPANY_PROPER, 'three_company_proper'),
    (THREE_COMPANY_FREELANCER, 'three_company_freelancer'),
)

MALE = 'male'
FEMALE = 'female'

PERSONNEL_GENDER = (
    (MALE, 'male'),
    (FEMALE, 'female'),
)

NONE = 'none'
PARALLEL = 'parallel'
PARALLEL_ONE = 'parallel_one'
PARALLEL_TWO = 'parallel_two'
PARALLEL_THREE = 'parallel_three'

PERSONNEL_PARALLEL = (
    (NONE, 'none'),
    (PARALLEL, 'parallel'),
    (PARALLEL_ONE, 'parallel_one'),
    (PARALLEL_TWO, 'parallel_two'),
    (PARALLEL_THREE, 'parallel_three'),
)

TEXT = 'text'
HTML = 'html'

TEXT_FORMAT = (
    (TEXT, 'text'),
    (HTML, 'html'),
)


class CardListTitle:
    PREPARING = "準備中"
    OPENING_AFTER_THREE_MONTHS = "営業中 翌翌々月"
    OPENING_AFTER_TWO_MONTHS = "営業中 翌々月"
    OPENING_AFTER_ONE_MONTH = "営業中 翌月"
    CURRENT_OPENING = "営業中 即日"
    WAITING_EVIDENCE = "エビデンス待ち"
    ONLINE = "稼働中"


CARD_LIST_TITLES = [
    (CardListTitle.PREPARING, CardListTitle.PREPARING),
    (CardListTitle.OPENING_AFTER_THREE_MONTHS, CardListTitle.OPENING_AFTER_THREE_MONTHS),
    (CardListTitle.OPENING_AFTER_TWO_MONTHS, CardListTitle.OPENING_AFTER_TWO_MONTHS),
    (CardListTitle.OPENING_AFTER_ONE_MONTH, CardListTitle.OPENING_AFTER_ONE_MONTH),
    (CardListTitle.CURRENT_OPENING, CardListTitle.CURRENT_OPENING),
    (CardListTitle.WAITING_EVIDENCE, CardListTitle.WAITING_EVIDENCE),
    (CardListTitle.ONLINE, CardListTitle.ONLINE),
]

CARD_LIST_ARRAY = ('準備中', '営業中 翌翌々月', '営業中 翌々月', '営業中 翌月', '営業中 即日', 'エビデンス待ち', '稼働中')

URGENT = 'urgent'
IMPORTANT = 'important'
HIGH = 'high'
MEDIUM = 'medium'
LOW = 'low'

PRIORITY = (
    (URGENT, 'urgent'),
    (IMPORTANT, 'important'),
    (HIGH, 'high'),
    (MEDIUM, 'medium'),
    (LOW, 'low'),
)

PRIORITY_MAP = {
    'urgent': '緊急',
    'important': '重大',
    'high': '高',
    'medium': '中',
    'low': '低',
}


class BoardTypes:
    PERSONNEL = 1
    PROJECT = 2


MAX_PERSONNEL_CONTRACT = 50
