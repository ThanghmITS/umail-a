from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models import PersonnelCheckList, ProjectCheckList
from app_staffing.models.base import PerCompanyModel


class BaseCheckListItemModel(PerCompanyModel, SoftDeleteObject):
    content = models.CharField(max_length=255, blank=False, null=False)
    is_finished = models.BooleanField(default=False)

    class Meta:
        abstract = True


class PersonnelCheckListItem(BaseCheckListItemModel):
    check_list = models.ForeignKey(PersonnelCheckList, on_delete=models.CASCADE, related_name="items")


class ProjectCheckListItem(BaseCheckListItemModel):
    check_list = models.ForeignKey(ProjectCheckList, on_delete=models.CASCADE, related_name="items")
