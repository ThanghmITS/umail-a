from django.db import models
from django.db.models.query_utils import Q
from ordered_model.models import OrderedModel
from softdelete.models import SoftDeleteObject

from app_staffing.board.constants import PERSONNEL_AFFILIATION, PERSONNEL_GENDER, PERSONNEL_PARALLEL, PRIORITY, \
    SETTLE_INCREMENT, \
    SETTLE_METHOD, DISTRIBUTION_TO
from app_staffing.board.models.card_list import ProjectCardList, PersonnelCardList
from app_staffing.board.models.train_station import TrainStation
from app_staffing.models import Tag
from app_staffing.models.base import PerCompanyModel
from django.db.models import prefetch_related_objects

def generate_upload_path_of_personnel_board_card(instance, filename):
    return f"personnel_board/images/{instance.id}/{filename}"


def generate_upload_path_of_project_board_card(instance, filename):
    return f"project_board/images/{instance.id}/{filename}"


def generate_upload_path_of_personnel_skill_sheet(instance, filename):
    return f"personnel_board/skill_sheets/{instance.personnel_id}/{filename}"


class BaseBoardModel(PerCompanyModel, OrderedModel, SoftDeleteObject):
    priority = models.CharField(max_length=20, choices=PRIORITY, null=True, blank=True)
    is_archived = models.BooleanField(default=False)
    start = models.DateField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    is_finished = models.BooleanField(default=False)

    order_with_respect_to = 'card_list'

    @property
    def period(self):
        return dict(start=self.start, end=self.end, is_finished=self.is_finished)

    class Meta:
        abstract = True


class Personnel(BaseBoardModel):
    """A model that describes personnel card related to a personnel card_list.
    """
    card_list = models.ForeignKey(PersonnelCardList, on_delete=models.CASCADE, related_name="personnel_items")
    request = models.TextField(max_length=500, blank=True, null=True)
    train_station = models.CharField(max_length=256, blank=True, null=True)
    affiliation = models.CharField(max_length=24, choices=PERSONNEL_AFFILIATION, null=True, blank=True)
    last_name = models.CharField(max_length=50, help_text='取引先担当者姓', blank=True, null=True, )
    first_name = models.CharField(blank=True, null=True, max_length=50, help_text='取引先担当者名')
    last_name_initial = models.CharField(max_length=1, blank=False, null=False)
    first_name_initial = models.CharField(max_length=1, blank=False, null=False)
    age = models.IntegerField(blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=6, choices=PERSONNEL_GENDER, null=True, blank=True)
    parallel = models.CharField(max_length=14, choices=PERSONNEL_PARALLEL, null=True, blank=True)
    operate_period = models.DateTimeField(blank=True, null=True)
    is_negotiable = models.BooleanField(default=False)
    price = models.IntegerField(null=True, blank=True)
    image_name = models.CharField(max_length=1024, blank=True, null=True)
    image = models.ImageField(max_length=255, upload_to=generate_upload_path_of_personnel_board_card, null=True)
    # Assignment of related skill
    skills = models.ManyToManyField(to=Tag, through='PersonnelSkillAssignment', related_name='assigned_personnel')

    @property
    def display_name(self):
        if self.last_name and self.first_name:
            return f'{self.company.name}, {self.last_name} {self.first_name}'
        else:
            return f'{self.company.name}'

    @property
    def total_byte_size(self):
        total_byte_size = 0

        for skill_sheet in self.skill_sheets.all():
            total_byte_size += skill_sheet.size

        return total_byte_size

    def prefetch_skill_sheets(self):
        try:
            self._prefetched_objects_cache[self.skill_sheets.prefetch_cache_name]
        except (AttributeError, KeyError):
            prefetch_related_objects([self], 'skill_sheets')

    class Meta:
        verbose_name = 'カード'


class PersonnelSkillSheet(PerCompanyModel, SoftDeleteObject):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="skill_sheets")
    name = models.CharField(max_length=255)
    file = models.FileField(max_length=255, upload_to=generate_upload_path_of_personnel_skill_sheet)
    size = models.PositiveIntegerField(default=0)


class PersonnelSkillAssignment(PerCompanyModel, SoftDeleteObject):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name='skill_assignment')
    skill = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='personnel_assignment')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['personnel', 'skill'], condition=Q(deleted_at=None),
                                    name='uniq_personnel_skill_assignment')
        ]
        indexes = [
            models.Index(fields=['personnel', 'skill'], name='stf_PerSkillAssign_nPk_idx'),
            # nPk = natural Primary keys
        ]

    def __str__(self):
        return f'Skill "{self.skill.value}" is assigned to Personnel Card {self.personnel.display_name}'


class Project(BaseBoardModel):
    card_list = models.ForeignKey(ProjectCardList, on_delete=models.CASCADE, related_name="project_items")
    place = models.ForeignKey(TrainStation, on_delete=models.SET_NULL, null=True, blank=True,
                              related_name="train_stations")
    project_attachment = models.FileField(max_length=255, upload_to=generate_upload_path_of_project_board_card,
                                          null=True)
    detail = models.TextField(max_length=50, blank=False, null=False)
    description = models.TextField(max_length=500, blank=True, null=True)

    project_period_datetime = models.DateField(blank=True, null=True)
    project_period_immediate = models.BooleanField(default=False)
    project_period_long_term = models.BooleanField(default=False)
    project_period_remarks = models.CharField(max_length=100, null=True, blank=True)

    working_hours_start = models.TimeField(blank=True, null=True)
    working_hours_end = models.TimeField(blank=True, null=True)
    working_hours_remarks = models.CharField(max_length=100, null=True, blank=True)

    people_amount = models.IntegerField(blank=True, null=True)
    people_is_multiple = models.BooleanField(default=False)
    people_is_set_proposal_welcome = models.BooleanField(default=False)
    people_remarks = models.CharField(max_length=100, null=True, blank=True)

    price_amount = models.IntegerField(blank=True, null=True)
    price_is_skill_assessment = models.BooleanField(default=False)
    price_remarks = models.CharField(max_length=100, null=True, blank=True)

    settle_width_start = models.IntegerField(blank=True, null=True)
    settle_width_end = models.IntegerField(blank=True, null=True)
    settle_width_is_fixed = models.BooleanField(default=False)
    settle_width_remarks = models.CharField(max_length=100, null=True, blank=True)

    increment = models.IntegerField(choices=SETTLE_INCREMENT, blank=True, null=True)
    settle_increment_remarks = models.CharField(max_length=100, null=True, blank=True)

    method = models.CharField(choices=SETTLE_METHOD, max_length=255, blank=True, null=True)
    settle_method_remarks = models.CharField(max_length=100, blank=True, null=True)

    payment_days = models.IntegerField(blank=True, null=True)
    payment_remarks = models.CharField(max_length=100, null=True, blank=True)

    interview_amount = models.IntegerField(blank=True, null=True)
    interview_is_join = models.BooleanField(default=False)
    interview_remarks = models.CharField(max_length=100, null=True, blank=True)

    distribution_to = models.CharField(choices=DISTRIBUTION_TO, max_length=255, null=True, blank=True)
    distribution_remarks = models.CharField(max_length=100, null=True, blank=True)

    train_station_remote = models.BooleanField(default=False)
    train_station_remarks = models.CharField(max_length=100, blank=True, null=True)

    skills = models.ManyToManyField(to=Tag, through='ProjectSkillAssignment', related_name='assigned_project')

    @property
    def project_period(self):
        return dict(
            datetime=self.project_period_datetime,
            immediate=self.project_period_immediate,
            long_term=self.project_period_long_term,
            remarks=self.project_period_remarks
        )

    @property
    def working_hours(self):
        return dict(start=self.working_hours_start, end=self.working_hours_end, remarks=self.working_hours_remarks)

    @property
    def train_station(self):
        return dict(id=self.place_id, remote=self.train_station_remote, remarks=self.train_station_remarks)

    @property
    def people(self):
        return dict(amount=self.people_amount, is_multiple=self.people_is_multiple,
                    is_set_proposal_welcome=self.people_is_set_proposal_welcome, remarks=self.people_remarks)

    @property
    def price(self):
        return dict(amount=self.price_amount, is_skill_assessment=self.price_is_skill_assessment,
                    remarks=self.price_remarks)

    @property
    def settle_width(self):
        return dict(start=self.settle_width_start, end=self.settle_width_end, is_fixed=self.settle_width_is_fixed,
                    remarks=self.settle_width_remarks)

    @property
    def settle_increment(self):
        return dict(increment=self.increment, remarks=self.settle_increment_remarks)

    @property
    def payment(self):
        return dict(day=self.payment_days, remarks=self.payment_remarks)

    @property
    def interview(self):
        return dict(amount=self.interview_amount, is_join=self.interview_is_join, remarks=self.interview_remarks)

    @property
    def distribution(self):
        return dict(to=self.distribution_to, remarks=self.distribution_remarks)

    @property
    def settle_method(self):
        return dict(method=self.method, remarks=self.settle_method_remarks)

    @property
    def is_immediate(self):
        return self.project_period_immediate


class ProjectSkillAssignment(PerCompanyModel, SoftDeleteObject):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='skill_assignment')
    skill = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='project_assignment')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['project', 'skill'], condition=Q(deleted_at=None),
                                    name='uniq_project_skill_assignment')
        ]
        indexes = [
            models.Index(fields=['project', 'skill'], name='stf_ProjectSkillAssign_nPk_idx'),
            # nPk = natural Primary keys
        ]

    def __str__(self):
        return f'Skill "{self.skill.value}" is assigned to Project Card {self.project.detail}'
