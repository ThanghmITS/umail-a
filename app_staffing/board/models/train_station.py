from django.db import models


class TrainStation(models.Model):
    name = models.CharField(max_length=256)
    location_key = models.CharField(max_length=50)
    area = models.CharField(max_length=50)
