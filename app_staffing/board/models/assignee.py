from django.conf import settings
from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import PerCompanyModel


class BaseAssignee(PerCompanyModel, SoftDeleteObject):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class PersonnelAssignee(BaseAssignee):
    card = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="personnel_assignees", null=True,
                                  blank=True)


class ProjectAssignee(BaseAssignee):
    card = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="project_assignees", null=True,
                                blank=True)
