from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import CommentBaseModel, check_fields_consistency


class PersonnelComment(CommentBaseModel, SoftDeleteObject):
    """A model that describes comment related to a card personnel.
    """
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name='comments')

    def clean(self):
        check_fields_consistency([self.personnel.company.id, self.company.id])

    def __str__(self):
        return f'"{self.personnel}" on Personnel {self.personnel.display_name}' \
               f' at {self.created_time} by {self.created_user}'

    class Meta:
        indexes = [
            models.Index(fields=['personnel'], name='stf_CdComment_personnel_idx'),
        ]


class ProjectComment(CommentBaseModel, SoftDeleteObject):
    """A model that describes comment related to a project card.
    """
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="comments")

    def clean(self):
        check_fields_consistency([self.project.company.id, self.company.id])

    def __str__(self):
        return f'"{self.Project}" on Project' \
               f' at {self.created_time} by {self.created_user}'

    class Meta:
        indexes = [
            models.Index(fields=['project'], name='stf_PtComment_project_idx'),
        ]
