from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.models.base import PerCompanyModel


class BaseCardList(PerCompanyModel, SoftDeleteObject):
    """A base model that describes card list, including card.
    """
    title = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.company.name}, {self.title}'

    class Meta:
        abstract = True


class PersonnelCardList(BaseCardList):
    """A model that describes personnel card list, including personnel card.
    """

    class Meta:
        verbose_name = 'カードリスト'


class ProjectCardList(BaseCardList):
    """A model that describes project card list, including project card.
    """

    class Meta:
        verbose_name = 'カードリスト'
