import django_filters.rest_framework as django_filters


class TrainStationFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
