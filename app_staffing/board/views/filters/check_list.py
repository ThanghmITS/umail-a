import django_filters.rest_framework as django_filters


class PersonnelCheckListFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(field_name='title', lookup_expr='icontains')
    cardId = django_filters.UUIDFilter(field_name='personnel', lookup_expr='exact')


class ProjectCheckListFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(field_name='title', lookup_expr='icontains')
    cardId = django_filters.UUIDFilter(field_name='project', lookup_expr='exact')
