from django.db.models import Q

from app_staffing.board.models import ProjectComment, Project
from app_staffing.board.serializers import ProjectCommentSerializer, ProjectSubCommentSerializer
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.views.base import MultiTenantMixin, GenericCommentListView, CommentValidationHelper, \
    GenericCommentDetailView, CustomValidationHelper, GenericSubCommentListView, PersonnelBoardBaseView


class ProjectCommentListView(PersonnelBoardBaseView, MultiTenantMixin, GenericCommentListView, CommentValidationHelper):
    queryset = ProjectComment.objects.all_with_deleted().filter(parent__isnull=True). \
        filter(Q(deleted_at__isnull=True) | Q(deleted_at__isnull=False, has_subcomment=True)). \
        select_related('created_user').prefetch_related('sub_comments')
    related_model_class = Project
    serializer_class = ProjectCommentSerializer
    related_field_name = 'project'

    ordering_fields = ('is_important', 'created_time',)
    ordering = ('-is_important', '-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_project_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().post(request, *args, **kwargs)


class ProjectSubCommentListView(PersonnelBoardBaseView, MultiTenantMixin, GenericSubCommentListView,
                                CommentValidationHelper):
    queryset = ProjectComment.objects.filter(parent__isnull=False, is_important=False).select_related('created_user')
    related_model_class = Project
    serializer_class = ProjectSubCommentSerializer
    related_field_name = 'project'

    parent_model_class = ProjectComment
    parent_field_name = 'parent'

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class ProjectCommentDetailView(PersonnelBoardBaseView, MultiTenantMixin, GenericCommentDetailView,
                               CustomValidationHelper,
                               CommentValidationHelper):
    queryset = ProjectComment.objects.select_related('created_user')
    serializer_class = ProjectCommentSerializer
    related_field_name = 'project'
    comment_model = ProjectComment

    def patch(self, request, pk, *args, **kwargs):
        self.exec_custom_validation(request)
        if self.update_project_important_comment_or_pass(pk) and \
                self.is_project_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().patch(request, *args, **kwargs)
