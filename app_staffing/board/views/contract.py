from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter

from app_staffing.board.models import ProjectContract, PersonnelContract
from app_staffing.board.serializers import ProjectContractSerializer, PersonnelContractSerializer, \
    PersonnelContractSummarySerializer, ProjectContractSummarySerializer
from app_staffing.board.views.filters.contract import PersonnelContractFilter, ProjectContractFilter
from app_staffing.views.base import MultiTenantMixin, GenericDetailAPIView, PersonnelBoardBaseView, GenericListAPIView, \
    CustomValidationHelper, ContractValidationHelper


class BaseContractCreateAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView):

    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('created_time',)

    ordering = ('created_time',)


class ProjectContractCreateAPIView(BaseContractCreateAPIView):
    queryset = ProjectContract.objects
    serializer_class = ProjectContractSerializer

    filterset_class = ProjectContractFilter


class PersonnelContractCreateAPIView(BaseContractCreateAPIView, CustomValidationHelper, ContractValidationHelper):
    queryset = PersonnelContract.objects
    serializer_class = PersonnelContractSerializer

    filterset_class = PersonnelContractFilter

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        self.check_set_organization_contact(request)
        return super().post(request, *args, **kwargs)


class ProjectContractActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = ProjectContract.objects
    serializer_class = ProjectContractSummarySerializer


class PersonnelContractActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = PersonnelContract.objects
    serializer_class = PersonnelContractSummarySerializer
