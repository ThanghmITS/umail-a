from .check_list import ProjectCheckListCreateAPIView, PersonnelCheckListCreateAPIView, PersonnelCheckListActionAPIView, \
    ProjectCheckListActionAPIView, PersonnelCheckListItemCreateAPIView, ProjectCheckListItemCreateAPIView, \
    PersonnelCheckListItemActionAPIView, ProjectCheckListItemActionAPIView
from .contract import ProjectContractCreateAPIView, PersonnelContractCreateAPIView, ProjectContractActionAPIView, \
    PersonnelContractActionAPIView
from .personnel.comment import PersonnelCommentListView, PersonnelSubCommentListView, PersonnelCommentDetailView
from .personnel.personnel import PersonnelCardListAPIView, GetPersonnelCardListAPIView, \
    PersonnelBoardCardListCreateAPIView, CopyCardPersonnelAPIView, ActionPersonnelCardListDetailAPIView, \
    PersonnelOrderUpdateAPIView, PersonnelDetailUpdateDestroyAPIView, PersonnelUpdateTemplateAPIView, \
    PersonnelSkillSheetAPIView, PersonnelSkillSheetDetailAPIView
from .project.comment import ProjectCommentListView, ProjectSubCommentListView, ProjectCommentDetailView
from .project.project import ProjectCardListAPIView, ProjectBoardCardListCreateAPIView, GetProjectCardListAPIView, \
    ProjectDetailUpdateDestroyAPIView, ProjectOrderUpdateAPIView, CopyCardProjectAPIView, ProjectUpdateTemplateAPIView, \
    ActionProjectCardListDetailAPIView
from .train_station import TrainstationListAPIView
