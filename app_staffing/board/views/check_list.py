from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter

from app_staffing.board.models import ProjectCheckList, PersonnelCheckList, ProjectCheckListItem, PersonnelCheckListItem
from app_staffing.board.serializers import ProjectCheckListSerializer, PersonnelCheckListSerializer, \
    PersonnelCheckListSummarySerializer, ProjectCheckListSummarySerializer, PersonnelCheckListItemSerializer, \
    ProjectCheckListItemSerializer
from app_staffing.board.views.filters.check_list import PersonnelCheckListFilter, ProjectCheckListFilter
from app_staffing.views.base import MultiTenantMixin, GenericDetailAPIView, PersonnelBoardBaseView, \
    GenericListAPIView, SummaryForListMixin, CheckListItemCreateBaseViewMixin, CustomValidationHelper


class BaseCheckListCreateAPIView(PersonnelBoardBaseView, MultiTenantMixin, SummaryForListMixin, GenericListAPIView):
    serializer_class = None
    serializer_class_for_list = None
    queryset = None

    filterset_class = PersonnelCheckListFilter

    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class ProjectCheckListCreateAPIView(BaseCheckListCreateAPIView):
    serializer_class = ProjectCheckListSerializer
    queryset = ProjectCheckList.objects.prefetch_related('items')
    serializer_class_for_list = ProjectCheckListSummarySerializer

    filterset_class = ProjectCheckListFilter


class PersonnelCheckListCreateAPIView(BaseCheckListCreateAPIView, CustomValidationHelper):
    serializer_class = PersonnelCheckListSerializer
    queryset = PersonnelCheckList.objects.prefetch_related('items')
    serializer_class_for_list = PersonnelCheckListSummarySerializer

    filterset_class = PersonnelCheckListFilter

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().post(request, *args, **kwargs)


class PersonnelCheckListActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = PersonnelCheckList.objects
    serializer_class = PersonnelCheckListSummarySerializer


class ProjectCheckListActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = ProjectCheckList.objects
    serializer_class = ProjectCheckListSummarySerializer


class PersonnelCheckListItemCreateAPIView(PersonnelBoardBaseView, MultiTenantMixin, CheckListItemCreateBaseViewMixin):
    serializer_class = PersonnelCheckListItemSerializer
    queryset = PersonnelCheckListItem.objects.order_by("created_time")

    related_model_class = PersonnelCheckList  # Override in child class
    related_field_name = 'check_list'  # Override in child class


class ProjectCheckListItemCreateAPIView(PersonnelBoardBaseView, MultiTenantMixin, CheckListItemCreateBaseViewMixin,
                                        CustomValidationHelper):
    serializer_class = ProjectCheckListItemSerializer
    queryset = ProjectCheckListItem.objects.order_by("created_time")

    related_model_class = ProjectCheckList  # Override in child class
    related_field_name = 'check_list'  # Override in child class

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().post(request, *args, **kwargs)


class PersonnelCheckListItemActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = PersonnelCheckListItem.objects
    serializer_class = PersonnelCheckListItemSerializer


class ProjectCheckListItemActionAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    queryset = ProjectCheckListItem.objects
    serializer_class = ProjectCheckListItemSerializer
