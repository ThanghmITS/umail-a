from django.conf import settings
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import NotFound

from app_staffing.board.exceptions import PersonnelFileSizeException, PersonnelNumberOfFileSizeException
from app_staffing.board.exceptions.card import OverloadDynamicRow
from app_staffing.board.models import PersonnelCardList, Personnel, PersonnelSkillSheet, PersonnelScheduledEmailTemplate
from app_staffing.board.serializers.assignee import PersonnelAssigneeSummarySerializer
from app_staffing.board.serializers.base import BaseOrderUpdateSerializer, \
    BaseBoardCardListCreateSerializer
from app_staffing.board.serializers.check_list import PersonnelCheckListSummarySerializer
from app_staffing.board.serializers.comment import PersonnelCommentSummarySerializer
from app_staffing.board.serializers.skill_sheet import SkillSheetSummarySerializer
from app_staffing.board.services import PersonnelService
from app_staffing.serializers.base import BaseCardSerializer, MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.addon import personnel_max_byte_size_image_and_error_message
from app_staffing.utils.custom_validation import custom_validation
from app_staffing.board.constants import PRIORITY_MAP
from app_staffing.services.gcs import gcs_service


class CardListSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = PersonnelCardList
        fields = ("id", "title")


class PersonnelOrderUpdateSerializer(BaseOrderUpdateSerializer):
    model = Personnel

    class Meta:
        model = Personnel
        fields = ('list_id',)
        extra_kwargs = {'list_id': {'write_only': True}}


class PersonnelBoardCardListCreateSerializer(BaseBoardCardListCreateSerializer):

    def custom_validation(self, data):
        custom_validation(data, 'personnel')

    def create(self, validated_data):
        new_instance = super().create(validated_data)
        PersonnelScheduledEmailTemplate.objects.create(personnel=new_instance)
        return new_instance

    class Meta:
        model = Personnel
        fields = ('id', 'first_name_initial', 'last_name_initial', 'card_list', 'company')
        extra_kwargs = {
            'company': {'write_only': True},
            'card_list': {'write_only': True},
            'first_name_initial': {'write_only': True},
            'last_name_initial': {'write_only': True}
        }


class PersonnelBoardCardSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    assignees = PersonnelAssigneeSummarySerializer(many=True, read_only=True)
    checklist = PersonnelCheckListSummarySerializer(many=True, read_only=True)
    skills = serializers.SerializerMethodField()
    comments = PersonnelCommentSummarySerializer(many=True, read_only=True)
    skill_sheets = SkillSheetSummarySerializer(many=True, read_only=True)

    def get_skills(self, obj):
        return [
            dict(id=obj_as.skill.id, name=obj_as.skill.value, color=obj_as.skill.color, is_skill=obj_as.skill.is_skill)
            for obj_as in obj.skill_assignment.all() if obj_as]

    def to_representation(self, data):
        data = super().to_representation(data)
        priority = data.get('priority')
        if priority:
            data['priority'] = dict(title=PRIORITY_MAP.get(priority), value=priority)
        return data

    class Meta:
        model = Personnel
        fields = ('id', 'order', 'assignees', 'last_name', 'first_name', 'last_name_initial', 'first_name_initial',
                  'age', 'gender', 'train_station', 'affiliation', 'skills', 'period', 'parallel', 'price',
                  'checklist', 'comments', 'is_archived', 'skill_sheets', 'image', 'image_name', 'priority', 'operate_period')


class ActionPersonnelCardListDetailSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    list_id = serializers.CharField(allow_blank=True)

    class Meta:
        model = PersonnelCardList
        fields = ('list_id',)


class PersonnelDetailUpdateDestroySerializer(BaseCardSerializer):
    assignees = serializers.SerializerMethodField()
    dynamic_rows = serializers.SerializerMethodField()
    skills = serializers.SerializerMethodField()
    comments = PersonnelCommentSummarySerializer(many=True, read_only=True)

    def get_skills(self, obj):
        return [skill.skill_id for skill in obj.skill_assignment.all() if skill]

    def get_dynamic_rows(self, obj):
        return [dict(id=dynamic_row.id, title=dynamic_row.title, content=dynamic_row.content) for dynamic_row \
                in obj.dynamic_row if dynamic_row]

    def get_assignees(self, obj):
        return [assignee.user_id for assignee in obj.assignees if assignee]

    def update(self, instance, validated_data):
        if "image" in validated_data.keys():
            if validated_data.get('image'):
                total_byte_size = validated_data['image'].size

                max_byte_size, max_byte_size_error_message = personnel_max_byte_size_image_and_error_message()
                if max_byte_size < total_byte_size:
                    raise PersonnelFileSizeException(max_byte_size_error_message)

                validated_data['image_name'] = validated_data['image'].name
            else:
                validated_data['image_name'] = None

        with transaction.atomic():
            # Handle assignees
            assignees = self.initial_data.get("assignees", None)
            if 'assignees' in self.initial_data.keys():
                instance = PersonnelService.update_assignees(instance, assignees)

            # Handle skills
            skills = self.initial_data.get("skills", None)
            if 'skills' in self.initial_data.keys():
                PersonnelService.update_skills(instance, skills)

            # Update dynamic_rows
            dynamic_rows = self.initial_data.get("dynamic_rows", None)
            if 'dynamic_rows' in self.initial_data.keys():
                if len(dynamic_rows) > settings.MAX_DYNAMIC_ROW:
                    raise OverloadDynamicRow()
                instance = PersonnelService.update_dynamic_row(instance, dynamic_rows)

            current_card_list_id = instance.card_list_id
            if 'card_list' in validated_data.keys() and validated_data.get('card_list').id == current_card_list_id:
                validated_data.pop('card_list')

            update_instance = super().update(instance, validated_data)

            if 'card_list' in validated_data.keys() and validated_data.get('card_list').id != current_card_list_id:
                update_instance.bottom()
            return super().update(instance, validated_data)

    class Meta:
        model = Personnel
        fields = ('id', 'order', 'list_id', 'assignees', 'priority', 'period', 'last_name', 'first_name', \
                  'last_name_initial', 'first_name_initial', 'age', 'birthday', 'gender', 'train_station', \
                  'affiliation', 'skills', 'operate_period', 'parallel', 'price', 'is_negotiable', 'request', \
                  'dynamic_rows', 'image', 'image_name', 'comments', 'is_archived',
                  'start', 'end', 'is_finished', 'card_list', 'created_time', 'created_user', 'created_user_name',
                  'modified_time', 'modified_user', 'modified_user_name', 'scheduled_email_template',
                  )

        extra_kwargs = {
            'order': {'read_only': True},
            'comments': {'read_only': True},
            'scheduled_email_template': {'read_only': True},
            'card_list': {'write_only': True},
            'list_id': {'read_only': True},
            'start': {'write_only': True},
            'end': {'write_only': True},
            'is_finished': {'write_only': True},
        }


class PersonnelSkillSheetSerializer(MultiTenantFieldsMixin, AppResourceSerializer):

    def to_internal_value(self, data):
        data.update(personnel=self.context['view'].kwargs['parent_pk'])
        return super().to_internal_value(data)

    def validate(self, data):
        personnel: Personnel = data['personnel']
        personnel.prefetch_skill_sheets()

        if personnel.total_byte_size + data['file'].size > settings.PERSONNEL_MAX_BYTE_SIZE_SKILL_SHEET:
            raise PersonnelFileSizeException(settings.MAX_BYTE_SIZE_PERSONNEL_SKILL_SHEET_ERROR_MESSAGE)

        if len(personnel.skill_sheets.all()) + 1 > settings.PERSONNEL_MAX_SKILL_SHEET_FILE:
            raise PersonnelNumberOfFileSizeException(settings.MAX_FILE_PERSONNEL_SKILL_SHEET_ERROR_MESSAGE)

        return data

    def create(self, validated_data):
        validated_data['name'] = validated_data['file'].name
        validated_data['size'] = validated_data['file'].size
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if gcs_service:
            data['file'] = gcs_service.get_download_url(instance.file.name)
        return data

    class Meta:
        model = PersonnelSkillSheet
        fields = ('id', 'personnel', 'name', 'file', 'company')

        extra_kwargs = {
            'company': {'write_only': True},
            'name': {'read_only': True},
            'personnel': {'write_only': True}
        }


class PersonnelCopyCardSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    card_id = serializers.SerializerMethodField()
    list_id = serializers.SerializerMethodField()

    def custom_validation(self, data):
        custom_validation(data, 'copy_card')

    def get_list_id(self, obj):
        return obj.card_list_id

    def to_internal_value(self, data):
        personnel_id = data.get('card_id', None)
        try:
            personnel = Personnel.objects.get(pk=personnel_id)
            instance_copy = personnel.__dict__
            instance_copy.pop('id')
            instance_copy.pop('_state')
            instance_copy.pop('last_name_initial')
            instance_copy.pop('first_name_initial')
            instance_copy.pop('card_list_id')
            instance_copy.pop('order')
            if bool(personnel.image) is False:
                instance_copy.pop('image')
            data.update(instance_copy)
        except Personnel.DoesNotExist:
            raise NotFound()
        list_id = data.get('list_id', None)
        if list_id:
            data.update(card_list=list_id)
        return super().to_internal_value(data)

    def get_card_id(self, obj):
        return None

    def create(self, validated_data):
        new_instance = super().create(validated_data)
        copy_personnel = Personnel.objects.get(pk=self.initial_data.get('card_id'))
        # clone related objects
        PersonnelService.clone_related_objects(copy_personnel, new_instance)
        return new_instance

    def to_representation(self, data):
        data = super().to_representation(data)
        priority = data.get('priority')
        if priority:
            data['priority'] = dict(title=PRIORITY_MAP.get(priority), value=priority)
        return data

    class Meta:
        model = Personnel
        fields = ('id', 'card_id', 'order', 'card_list', 'priority', 'period', 'last_name', 'first_name',
                  'last_name_initial', 'first_name_initial', 'age', 'birthday', 'gender', 'train_station',
                  'affiliation', 'operate_period', 'parallel', 'price', 'is_negotiable', 'request',
                  'image', 'image_name', 'is_archived', 'start', 'end', 'is_finished', 'company', 'list_id'
                  )

        extra_kwargs = {
            'order': {'read_only': True},
            'start': {'write_only': True},
            'end': {'write_only': True},
            'is_finished': {'write_only': True},
            'company': {'write_only': True},
        }
