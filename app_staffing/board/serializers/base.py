from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.custom_validation import custom_validation


class BaseCreateWithPersonnelSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    card_id = serializers.UUIDField(required=False)

    def to_internal_value(self, data):
        card_id = data.pop('card_id', None)

        if card_id:
            data.update(personnel=card_id)
        else:
            raise ValidationError({'card_id': '必須項目です。'})
        return super().to_internal_value(data)


class BaseCreateWithProjectSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    card_id = serializers.UUIDField(required=False)

    def to_internal_value(self, data):
        card_id = data.pop('card_id', None)
        if card_id:
            data.update(project=card_id)
        else:
            raise ValidationError({'card_id': '必須項目です。'})
        return super().to_internal_value(data)


class BaseOrderUpdateSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    list_id = serializers.CharField(allow_blank=True, required=False)
    model = None

    def custom_validation(self, data):
        custom_validation(data, 'move_card')

    def update(self, instance, validated_data):
        if 'list_id' in validated_data.keys():
            if validated_data.get('list_id') != instance.card_list:
                card_list = validated_data.get('list_id')
                max_order_card = self.model.objects.filter(card_list=card_list).order_by('-order').values(
                    'order').first()
                if max_order_card:
                    validated_data['order'] = max_order_card['order'] + 1
                else:
                    validated_data['order'] = 0
            validated_data.update(card_list_id=validated_data.get('list_id'))
        update_instance = super().update(instance, validated_data)
        return update_instance



class BaseBoardCardListCreateSerializer(MultiTenantFieldsMixin, AppResourceSerializer):

    def create(self, validated_data):
        instance = super().create(validated_data)
        instance.top()
        return instance
