from rest_framework import serializers

from app_staffing.models.organization import Tag
from app_staffing.serializers.base import AppResourceSerializer, MultiTenantFieldsMixin


class TagSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.value

    class Meta:
        model = Tag
        fields = ('id', 'name', 'color', 'is_skill')
        read_only_fields = ('id', 'name', 'color', 'is_skill')


class SkillSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'value',)
