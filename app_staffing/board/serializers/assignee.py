from rest_framework import serializers

from app_staffing.board.models import PersonnelAssignee, ProjectAssignee
from app_staffing.serializers.base import AppResourceSerializer, MultiTenantFieldsMixin


class BaseAssigneeSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.user.display_name

    def get_id(self, obj):
        return obj.user.id

    def get_avatar(self, obj):
        if not obj.user.avatar:
            return None
        return obj.user.avatar.url


class PersonnelAssigneeSummarySerializer(BaseAssigneeSummarySerializer):
    class Meta:
        model = PersonnelAssignee
        fields = ('id', 'name', 'avatar')
        read_only_fields = ('id', 'name', 'avatar')


class ProjectAssigneeSummarySerializer(BaseAssigneeSummarySerializer):
    class Meta:
        model = ProjectAssignee
        fields = ('id', 'name', 'avatar')
        read_only_fields = ('id', 'name', 'avatar')
