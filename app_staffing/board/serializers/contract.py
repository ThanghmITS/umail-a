from app_staffing.board.models import PersonnelContract, ProjectContract
from app_staffing.board.serializers.base import BaseCreateWithPersonnelSerializer, BaseCreateWithProjectSerializer
from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.custom_validation import custom_validation


class BaseContractSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    def validate(self, attrs):
        return attrs


class BasePersonnelContractSerializer(BaseContractSerializer):

    def to_internal_value(self, data):
        project_period = data.pop('project_period', None)
        if project_period and len(project_period) > 1:
            data.update(start=project_period[0], end=project_period[1])
        return super().to_internal_value(data)


class BaseProjectContractSerializer(BaseContractSerializer):

    def validate(self, attrs):
        return super().validate(attrs)


class ProjectContractSerializer(BaseProjectContractSerializer, BaseCreateWithProjectSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'contract')

    class Meta:
        model = ProjectContract
        fields = (
            'id', 'project', 'personnel', 'higher_organization', 'higher_contact', 'lower_organization',
            'lower_contact', 'company')

        extra_kwargs = {
            'company': {'write_only': True},
            'project': {'write_only': True},
        }


class PersonnelContractSerializer(BasePersonnelContractSerializer, BaseCreateWithPersonnelSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'contract')

    class Meta:
        model = PersonnelContract
        fields = (
            'id', 'personnel', 'detail', 'price', 'project_period', 'higher_organization', 'higher_contact',
            'lower_organization', 'lower_contact', 'company', 'start', 'end')

        extra_kwargs = {
            'company': {'write_only': True},
            'start': {'write_only': True},
            'end': {'write_only': True},
            'personnel': {'write_only': True},
        }


class ProjectContractSummarySerializer(BaseProjectContractSerializer):
    class Meta:
        model = ProjectContract
        fields = ('id', 'personnel', 'higher_organization', 'higher_contact', 'lower_organization', 'lower_contact',)


class PersonnelContractSummarySerializer(BasePersonnelContractSerializer):
    class Meta:
        model = PersonnelContract
        fields = (
            'id', 'detail', 'price', 'project_period', 'higher_organization', 'higher_contact', 'lower_organization',
            'lower_contact', 'start', 'end')

        extra_kwargs = {
            'start': {'write_only': True},
            'end': {'write_only': True},
        }
