from app_staffing.board.models import PersonnelScheduledEmailTemplate, ProjectScheduledEmailTemplate
from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.custom_validation import custom_validation

SCHEDULED_EMAIL_TEMPLATE_FIELDS = (
    'text_format', 'subject', 'body', 'send_copy_to_share', 'send_copy_to_sender', 'condition', 'lower', 'upper',
    'personnelskill_dev_beginner', 'personnelskill_dev_hosyu', 'personnelskill_dev_kihon', 'personnelskill_dev_seizou',
    'personnelskill_dev_syousai', 'personnelskill_dev_test', 'personnelskill_dev_youken',
    'personnelskill_infra_hosyu', 'personnelskill_infra_kanshi', 'personnelskill_infra_kihon',
    'personnelskill_infra_kouchiku', 'personnelskill_infra_syousai', 'personnelskill_infra_test',
    'personnelskill_infra_youken', 'personneltype_dev', 'personneltype_dev_designer', 'personneltype_dev_front',
    'personneltype_dev_other', 'personneltype_dev_pm', 'personneltype_dev_server', 'personneltype_infra',
    'personneltype_infra_database', 'personneltype_infra_network', 'personneltype_infra_other',
    'personneltype_infra_security', 'personneltype_infra_server', 'personneltype_infra_sys', 'personneltype_other',
    'personneltype_other_eigyo', 'personneltype_other_kichi', 'personneltype_other_other',
    'personneltype_other_support', 'personnelskill_infra_beginner',
)


class BaseUpdateScheduledEmailTemplateSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'card_scheduled_email_template')

    def to_internal_value(self, data):
        if 'condition' in data.keys():
            condition = data.pop('condition', None)
            for k, v in condition.items():
                if k in SCHEDULED_EMAIL_TEMPLATE_FIELDS:
                    data[k] = v
        if 'body' in data.keys():
            body = data.pop('body', None)
            if 'upper' in body.keys():
                data.update(upper=body.get('upper', None))
            if 'lower' in body.keys():
                data.update(lower=body.get('lower', None))
        return super().to_internal_value(data)


class UpdatePersonnelScheduledEmailTemplateSerializer(BaseUpdateScheduledEmailTemplateSerializer):
    class Meta:
        model = PersonnelScheduledEmailTemplate

        fields = SCHEDULED_EMAIL_TEMPLATE_FIELDS

        extra_kwargs = {
            'body': {'read_only': True},
            'condition': {'read_only': True},
            'lower': {'write_only': True},
            'upper': {'write_only': True},
            'personnelskill_dev_beginner': {'write_only': True},
            'personnelskill_dev_hosyu': {'write_only': True},
            'personnelskill_dev_kihon': {'write_only': True},
            'personnelskill_dev_seizou': {'write_only': True},
            'personnelskill_dev_syousai': {'write_only': True},
            'personnelskill_dev_test': {'write_only': True},
            'personnelskill_dev_youken': {'write_only': True},
            'personnelskill_infra_beginner': {'write_only': True},
            'personnelskill_infra_hosyu': {'write_only': True},
            'personnelskill_infra_kanshi': {'write_only': True},
            'personnelskill_infra_kihon': {'write_only': True},
            'personnelskill_infra_kouchiku': {'write_only': True},
            'personnelskill_infra_syousai': {'write_only': True},
            'personnelskill_infra_test': {'write_only': True},
            'personnelskill_infra_youken': {'write_only': True},
            'personneltype_dev': {'write_only': True},
            'personneltype_dev_designer': {'write_only': True},
            'personneltype_dev_front': {'write_only': True},
            'personneltype_dev_other': {'write_only': True},
            'personneltype_dev_pm': {'write_only': True},
            'personneltype_dev_server': {'write_only': True},
            'personneltype_infra': {'write_only': True},
            'personneltype_infra_database': {'write_only': True},
            'personneltype_infra_network': {'write_only': True},
            'personneltype_infra_other': {'write_only': True},
            'personneltype_infra_security': {'write_only': True},
            'personneltype_infra_server': {'write_only': True},
            'personneltype_infra_sys': {'write_only': True},
            'personneltype_other': {'write_only': True},
            'personneltype_other_eigyo': {'write_only': True},
            'personneltype_other_kichi': {'write_only': True},
            'personneltype_other_other': {'write_only': True},
            'personneltype_other_support': {'write_only': True},
        }


class UpdateProjectScheduledEmailTemplateSerializer(BaseUpdateScheduledEmailTemplateSerializer):
    class Meta:
        model = ProjectScheduledEmailTemplate

        fields = SCHEDULED_EMAIL_TEMPLATE_FIELDS

        extra_kwargs = {
            'body': {'read_only': True},
            'condition': {'read_only': True},
            'lower': {'write_only': True},
            'upper': {'write_only': True},
            'personnelskill_dev_beginner': {'write_only': True},
            'personnelskill_dev_hosyu': {'write_only': True},
            'personnelskill_dev_kihon': {'write_only': True},
            'personnelskill_dev_seizou': {'write_only': True},
            'personnelskill_dev_syousai': {'write_only': True},
            'personnelskill_dev_test': {'write_only': True},
            'personnelskill_dev_youken': {'write_only': True},
            'personnelskill_infra_beginner': {'write_only': True},
            'personnelskill_infra_hosyu': {'write_only': True},
            'personnelskill_infra_kanshi': {'write_only': True},
            'personnelskill_infra_kihon': {'write_only': True},
            'personnelskill_infra_kouchiku': {'write_only': True},
            'personnelskill_infra_syousai': {'write_only': True},
            'personnelskill_infra_test': {'write_only': True},
            'personnelskill_infra_youken': {'write_only': True},
            'personneltype_dev': {'write_only': True},
            'personneltype_dev_designer': {'write_only': True},
            'personneltype_dev_front': {'write_only': True},
            'personneltype_dev_other': {'write_only': True},
            'personneltype_dev_pm': {'write_only': True},
            'personneltype_dev_server': {'write_only': True},
            'personneltype_infra': {'write_only': True},
            'personneltype_infra_database': {'write_only': True},
            'personneltype_infra_network': {'write_only': True},
            'personneltype_infra_other': {'write_only': True},
            'personneltype_infra_security': {'write_only': True},
            'personneltype_infra_server': {'write_only': True},
            'personneltype_infra_sys': {'write_only': True},
            'personneltype_other': {'write_only': True},
            'personneltype_other_eigyo': {'write_only': True},
            'personneltype_other_kichi': {'write_only': True},
            'personneltype_other_other': {'write_only': True},
            'personneltype_other_support': {'write_only': True},
        }
