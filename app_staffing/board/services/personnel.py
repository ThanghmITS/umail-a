from app_staffing.board.models import PersonnelCardList, PersonnelCheckList, PersonnelCheckListItem, PersonnelAssignee, PersonnelDynamicRow, \
    PersonnelSkillAssignment, PersonnelComment
from app_staffing.board.services.base import BaseCardService


class PersonnelService(BaseCardService):
    model_card_list = PersonnelCardList
    model_assignee = PersonnelAssignee
    model_dynamic_row = PersonnelDynamicRow
    model_card_tag = PersonnelSkillAssignment
    model_comment = PersonnelComment
    model_checklist = PersonnelCheckList
    model_checklist_item = PersonnelCheckListItem
    model_name = 'personnel'
