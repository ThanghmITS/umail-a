from rest_framework.exceptions import APIException


class ActionCardListNotAllowedException(APIException):
    status_code = 404
    default_code = 'action_not_allowed'
