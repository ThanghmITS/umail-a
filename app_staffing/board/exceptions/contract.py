from rest_framework.exceptions import APIException


class PersonnelContractSetOrganizationAndContactException(APIException):
    status_code = 400
    default_detail = '取引先と取引先担当者が関連していません。'


class NumberPersonnelContractSetOrganizationContactException(APIException):
    status_code = 400
    default_detail = '登録可能な上限件数に達しています。'
