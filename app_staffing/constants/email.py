

class AttachmentFileStatus:
    AVAILABLE = 1
    DOWNLOADED = 2


class ScheduledEmailSettingFileStatus:
    URL = 1
    FILE = 2


SCHEDULED_EMAIL_CONTENT_WITH_URL_SETTING = """
                        ▼ファイルダウンロードURL
                        https://{}/scheduledMail/files/download/{}
                        ▼ダウンロードパスワード
                        {}
                    """
