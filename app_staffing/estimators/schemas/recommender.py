from jubakit.recommender import Schema

# An object - jubatus schema map definition that regards to Email model.
# TODO: Support attachment names on new model.
EmailSchema = Schema(
    {
        'id': Schema.ID,
        'subject': (Schema.STRING, 'subject'),
        'body': (Schema.STRING, 'text'),
    }, fallback=Schema.IGNORE
)
