from jubakit.classifier import Schema

# An object - jubatus schema map definition that regards to Email model.
# TODO: Support attachment names on new model.
EmailSchema = Schema(
    {
        'category': (Schema.LABEL, 'category'),
        'subject': (Schema.STRING, 'subject'),
        'body': (Schema.STRING, 'text'),
    }, fallback=Schema.IGNORE
)
