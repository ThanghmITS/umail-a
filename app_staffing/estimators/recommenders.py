from datetime import timedelta

from django.conf import settings
from django.utils import timezone

from jubakit.recommender import Recommender
from jubakit.recommender import Dataset

from app_staffing.models import Email
from app_staffing.estimators.schemas.recommender import EmailSchema
from app_staffing.estimators.loaders.models import QuerysetLoader, ModelLoader


# TODO: Create Interface or abstract class before increasing the number of classes.
class JobEmailRecommender(object):
    """This estimator recommends the latest job emails by similarity with a given email of any category.
    Note: Estimator will keep old emails unless enabling unlearn process in the estimator.
    You can define this unlearn process in the configuration file of a Jubatus server.
    """
    _model_class = Email
    _schema = EmailSchema

    _batch_train_target = _model_class.objects.filter(
        estimated_category__exact='job',
        sent_date__gt=(timezone.now() - timedelta(days=settings.BATCH_TRAIN_DAY_RANGE))
    )

    _batch_remove_target = _model_class.objects.filter(
        estimated_category__exact='job',
        sent_date__lt=(timezone.now() - timedelta(days=settings.DAYS_TO_KEEP_RECOMMENDATION))
    )

    def __init__(self):
        self.service = Recommender(settings.MAIL_RECOMMENDER_HOST, int(settings.MAIL_RECOMMENDER_PORT))

    @staticmethod
    def _represent_prediction_result(raw_result):
        """Convert the raw result to another one based on the Interface or abstract class."""
        scores = raw_result[2]
        converted_scores = [dict(id=item.id, score=item.score) for item in scores]
        sorted_scores = sorted(converted_scores, key=lambda x: x['score'], reverse=True)  # sort by score. (desc)
        return sorted_scores

    def batch_train(self):
        # Register job data as a recommendation data.
        training_data = self._batch_train_target

        loader = QuerysetLoader(queryset=training_data)
        dataset = Dataset(loader=loader, schema=self._schema).shuffle()

        raw_result_generator = self.service.update_row(dataset)

        for item in raw_result_generator:
            is_registered = item[2]
            if not is_registered:
                raise RuntimeError('Can not register item key={0} to the recommender.'.format(item[1]))

    def batch_remove(self):
        """This method Removes old entries from the remote jubatus recommender
        Note: Jubatus Recommender also has an old data purging function.
        So the number of the document available on the recommender depends on both this batch and a jubatus config.
        """
        target = self._batch_remove_target
        loader = QuerysetLoader(queryset=target)
        dataset = Dataset(loader=loader, schema=self._schema)

        raw_results = list(self.service.clear_row(dataset))
        formatted_results = [{'id': tpl[1], 'result':tpl[2]} for tpl in raw_results]
        # TODO: Log result.

        return formatted_results

    def train(self, instance):
        """This method register django model instance data to remote jubatus recommender.
        :param instance: Django model object instance.
        :type instance: instance of django.db.models.Model
        :return: Nothing
        """
        loader = ModelLoader(instance=instance)
        dataset = Dataset(loader=loader, schema=self._schema)

        # In this condition, list length must be 1 because the dataset length is 1.
        raw_result = list(self.service.update_row(dataset))[0]
        is_registered = raw_result[2]

        if not is_registered:
            raise RuntimeError('Can not register data {0} to the recommender.'.format(instance.pk))

    def remove(self, instance):
        """This method removes vectorized documents from the remote recommender.

        :param instance: A Django model instance that you want to remove from the remote recommender.
        :type instance: An instance of django.db.models.Model
        :return: True if done, False if not done.
        :rtype: bool
        :raise TODO: Make an error wrapper if possible.
        """
        loader = ModelLoader(instance=instance)
        dataset = Dataset(loader=loader, schema=self._schema)
        raw_result = list(self.service.clear_row(dataset))[0]
        is_removed = raw_result[2]  # TODO: Log result.
        return is_removed

    def predict(self, instance, size=3):
        """This method retrieve document id similar to given instance from jubatus recommender.
        :param instance: Django model object instance.
        :type instance: instance of django.db.models.Model
        :param size: The maximum number of items the recommender recommends.
        :type size: int
        :return: similar_objects.
        :rtype: list of dict
        """
        loader = ModelLoader(instance=instance)
        dataset = Dataset(loader=loader, schema=self._schema)
        # In this condition, list length must be 1 because the dataset length is 1.
        raw_result = list(self.service.similar_row_from_datum(dataset, size=size))[0]

        return self._represent_prediction_result(raw_result)

    def reset(self):
        """WARNING: This Completely clears the ALL items from servers."""
        self.service.clear()
