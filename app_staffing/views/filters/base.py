"""Helper functions and classes for filter modules."""
from rest_framework import filters

from functools import reduce
import operator
from django.db.models import Q
from django.db.models.fields import CharField, TextField

from app_staffing.views.base import InvalidQueryParameterException

from datetime import timedelta, datetime, time
import pytz

class SearchFilter(filters.SearchFilter):
    def get_search_fields(self, view, request):
        if request.query_params.get('fields'):
            fields = request.query_params.get('fields').split(',')
            search_fields = tuple(fields)
        else:
            search_fields = super().get_search_fields(view, request)

        model_fields = set()
        for field in view.queryset.model._meta.fields:
            if isinstance(field, CharField) or isinstance(field, TextField):
                model_fields.add(field.name)

        normalized_search_fields = list(map(lambda f:f.lstrip('^=@$'), search_fields))

        if [f for f in normalized_search_fields if f not in model_fields]:
            raise InvalidQueryParameterException(params=['fields'])

        return search_fields


def multiple_word_filter(filter_type='AND', matcher_name='icontains'):
    """This method returns a method that enables Multiple keyword searches with AND / OR conditions
     when a filter received multiple words in a single field.

    :param filter_type: A Filter type. Either AND or OR should be specified.
    :type filter_type: str
    :param matcher_name: A filter condition such as 'exact' 'contains'
    :type matcher_name: str
    :return: A callable Python object (method) for a filter method of django_filters Filter.
    :rtype: object
    """
    operator_map = {'AND': operator.and_, 'OR': operator.or_}

    try:
        operator_func = operator_map[filter_type]
    except KeyError:
        raise ValueError('Invalid Filter Type has Specified. choices are: "AND, OR"')

    def method_filter(queryset, name, value):
        """ This enables Multiple keyword searches with OR condition when a filter received multiple words in a single field.
        Use this as a callback function of a filter method of django_filters Filter.
        ex: value = 'Word1 Word2 Word3'

        :param queryset: An original queryset, that does not have filtered with this field name.
        :type queryset: django.db.models.QuerySet
        :param name: A field name of the filter target.
        :type name: str
        :param value: An Input value for filtering given from a view.
        :param value: str
        :return: filtered_queryset: A new queryset that has filter statements.
        :rtype: django.db.models.QuerySet
        """
        lookup = '__'.join([name, matcher_name])
        words = value.split()
        filter_clauses = [Q(**{lookup: keyword}) for keyword in words]
        filtered_queryset = queryset.filter(reduce(operator_func, filter_clauses))

        return filtered_queryset

    return method_filter

def date_range_lte_filter(queryset, name, value):
    """ DatePickerによる日付範囲検索のlte側の検索パラメータ設定メソッド
    日付検索に関してgte側とlte側の値が一致している場合(ex. 2021-05-01 ~ 2021-05-01のような検索の場合)、
    本来であればその日付(exだと2021-05-21)のレコードが検索対象になって欲しい。
    そのため検索実行時、lte側の日付を1日加算して範囲検索を実行する。
    """
    filter_params = {f'{name}__lt': value + timedelta(days=1)}
    return queryset.filter(**filter_params)

def datetime_range_lt_filter(queryset, name, value):
    filter_params = {f'{name}__lt': datetime.combine(value + timedelta(days=1), time()).astimezone(pytz.timezone('Asia/Tokyo'))}
    return queryset.filter(**filter_params)

def datetime_range_gte_filter(queryset, name, value):
    filter_params = {f'{name}__gte': datetime.combine(value, time()).astimezone(pytz.timezone('Asia/Tokyo'))}
    return queryset.filter(**filter_params)
