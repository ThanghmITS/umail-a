from app_staffing.models import PurchaseHistory
from app_staffing.serializers import PurchaseHistorySerializer
from app_staffing.views.base import MultiTenantMixin, GenericListAPIView
from app_staffing.views.classes.pagenation import StandardPagenation

class PurchaseHistoryView(MultiTenantMixin, GenericListAPIView):
    queryset = PurchaseHistory.objects.order_by('-created_time')
    serializer_class = PurchaseHistorySerializer
    pagination_class = StandardPagenation
