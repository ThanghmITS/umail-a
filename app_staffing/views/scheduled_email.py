from copy import deepcopy

from django.utils import timezone
from django.db.transaction import atomic
from django.utils.crypto import get_random_string

from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
from rest_framework.views import APIView

from django_filters.rest_framework import DjangoFilterBackend

from app_staffing.models.email import ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailTarget, DRAFT,\
    QUEUED, ScheduledEmailSearchCondition, ScheduledEmailStatus, ScheduledEmailSetting, ScheduledEmailOpenHistory, \
    ScheduledEmailSendError, TEXT
from app_staffing.models import Addon
from app_staffing.views.base import SummaryForListMixin, OrderIdHelper, CustomValidationHelper, BulkOperationHelper
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.views.classes.scheduled_email_pagination import ScheduledEmailOpenerCustomPagination
from app_staffing.views.filters import ScheduledEmailFilter
from app_staffing.serializers import ScheduledEmailSerializer, ScheduledEmailAttachmentSerializer,\
    ScheduledEmailSummarySerializer, ScheduledEmailPreviewSerializer, ScheduledEmailSettingSerializer, ScheduledEmailOpenHistorySerializer, ScheduledEmailSendErrorSerializer
from app_staffing.utils.addon import scheduled_email_limit_target_count, scheduled_email_max_byte_size_and_error_message
from config.settings.base import EMAIL_HOST_BLOCKED
from app_staffing.constants.email import ScheduledEmailSettingFileStatus
from app_staffing.services.email.scheduled_email import ScheduledEmailService
from .base import GenericActionView, GenericListAPIView, GenericReadOnlyView, GenericDetailAPIView,\
    FileListView, FileDetailView, MultiTenantMixin

from datetime import datetime, timedelta
from django.utils.dateparse import parse_datetime
from django.utils import dateformat

from django.conf import settings

import pytz

import jpholiday
import json

from django.db.models import Prefetch, Subquery, OuterRef, Case, When, Value, IntegerField, F

from rest_framework.exceptions import MethodNotAllowed

import smtplib
from dateutil.relativedelta import relativedelta

from app_staffing.utils.gcp import CloudTasks

from app_staffing.exceptions.scheduled_email import ScheduledEmailPerameterException, \
    ScheduledEmailTargetNotSelectedException, \
    ScheduledEmailAttachementSizeException, ScheduledEmailTargetCountException, \
    ScheduledEmailReserveAllowedPeriodException, \
    ScheduledEmailExistsByTermException, ScheduledEmailNotWorkingTimeException, ScheduledEmailHolidayException, \
    ScheduledEmailConnectionException, EmailDomainBlocked
from app_staffing.exceptions.scheduled_email import ScheduledEmailPerameterException, \
    ScheduledEmailTargetNotSelectedException, \
    ScheduledEmailAttachementSizeException, ScheduledEmailTargetCountException, \
    ScheduledEmailReserveAllowedPeriodException, \
    ScheduledEmailExistsByTermException, ScheduledEmailNotWorkingTimeException, ScheduledEmailHolidayException, \
    ScheduledEmailConnectionException, ScheduledEmailDeliveryTimeIsNull, ScheduledEmailIncorrectPassword, \
    ScheduledEmailAttachmentNotExist

from app_staffing.exceptions.base import BulkOperationLimitException
from app_staffing.views.exceptions.scheduled_email import ScheduledEmailDeliveryIntervalException

from app_staffing.exceptions.validation import CannotDeleteException

from django_auto_prefetching import AutoPrefetchViewSetMixin

from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from djangorestframework_camel_case.parser import CamelCaseJSONParser

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


class ScheduledEmailsHelper(object):
    def init_tasks(self, instance):
        # まだ配信されてないメールの中で、CloudTaskにタスクがある場合、ジョブの状態を変更(CloudTasksにジョブがない状態に変更)し、CloudTasksからジョブを削除を行う
        if instance.status == 'tasks' and instance.scheduled_email_status.name in [DRAFT, QUEUED]:
            cloud_tasks = CloudTasks(settings.CLOUD_TASKS_FOR_MAIL, settings.WORKER_NAME_FOR_MAIL)
            instance.status = ''
            instance.save()
            cloud_tasks.delete_task(str(instance.id))
            logger_stdout.info('Deleted Mail Tasks: ID: {0}'.format(instance.id))

    def delete_tasks(self, instance):
        # まだ配信されてないメールの中で、CloudTaskにタスクがある場合、削除を行う。
        if instance.status == 'tasks' and instance.scheduled_email_status.name in [DRAFT, QUEUED]:
            cloud_tasks = CloudTasks(settings.CLOUD_TASKS_FOR_MAIL, settings.WORKER_NAME_FOR_MAIL)
            cloud_tasks.delete_task(str(instance.id))
            logger_stdout.info('Deleted Mail Tasks: ID: {0}'.format(instance.id))

    def get_mails_by_term(self, query_set, base_datetime, kwargs):
        gte = base_datetime - timedelta(minutes=settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_MINUTES)
        lte = base_datetime - timedelta(minutes=-settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_MINUTES)
        if 'pk' in kwargs and kwargs['pk']:
            return query_set.filter(date_to_send__gte=gte, date_to_send__lte=lte).exclude(id=kwargs['pk'])
        else:
            return query_set.filter(date_to_send__gte=gte, date_to_send__lte=lte)

    def get_error_response_data(self, illegal_datetime):
        formated_illegal_datetime = dateformat.format(illegal_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y/n/d H:i:s')
        raise ScheduledEmailExistsByTermException(
            settings.SCHEDULED_EMAIL_EXISTS_BY_TERM_ERROR_MESSAGE.format(
                formated_illegal_datetime,
                settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_MINUTES
            )
        )

    # 業務時間外(8:00 ~ 19:00以外)の判定処理
    def is_not_worktime(self, base_datetime):
        formated_base_datetime = base_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)).time()
        worktime_start = datetime.strptime(settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_NOT_WORKTIME_START, '%H:%M:%S').time()
        worktime_end = datetime.strptime(settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_NOT_WORKTIME_END, '%H:%M:%S').time()
        return True if formated_base_datetime < worktime_start or formated_base_datetime > worktime_end else False

    def get_error_response_data_not_worktime(self, illegal_datetime):
        formated_illegal_datetime = dateformat.format(illegal_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y/n/d H:i:s')
        raise ScheduledEmailNotWorkingTimeException(
            settings.SCHEDULED_EMAIL_NOT_WORKING_TIME_ERROR_MESSAGE.format(
                formated_illegal_datetime,
                settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_NOT_WORKTIME_START,
                settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_NOT_WORKTIME_END,
            )
        )

    # 休日(土日祝日)の判定処理
    def is_holiday(self, base_datetime):
        formated_base_datetime = dateformat.format(base_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y-m-d')
        Date = datetime.strptime(formated_base_datetime, '%Y-%m-%d').date()
        return True if Date.weekday() >= 5 or jpholiday.is_holiday(Date) else False

    def get_error_response_data_holiday(self, illegal_datetime):
        formated_illegal_datetime = dateformat.format(illegal_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y/n/d H:i:s')
        raise ScheduledEmailHolidayException(
            settings.SCHEDULED_EMAIL_HOLIDAY_ERROR_MESSAGE.format(
                formated_illegal_datetime
            )
        )

    # 登録可能期間(現在時刻から一ヶ月先まで)の判定処理
    def is_not_allowed_period(self, base_datetime):
        return (timezone.now() + relativedelta(months=settings.SCHEDULED_EMAIL_RESERVE_ALLOWED_PERIOD_MONTH)) < base_datetime

    def get_error_response_data_not_allowed_period(self, illegal_datetime):
        formated_illegal_datetime = dateformat.format(illegal_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y/n/d H:i:s')
        raise ScheduledEmailReserveAllowedPeriodException(
            settings.SCHEDULED_EMAIL_RESERVE_ALLOWED_PERIOD_ERROR_MESSAGE.format(
                formated_illegal_datetime,
                settings.SCHEDULED_EMAIL_RESERVE_ALLOWED_PERIOD_MONTH
            )
        )

    def is_undeletable(self, resource_ids):
        return ScheduledEmail.objects.filter(
            id__in=resource_ids,
            scheduled_email_status__name='sending',
            company=self.request.user.company
        ).exists()

    def validate_target_count(self, request):
        if 'target_contacts' not in request.data or ('target_contacts' in request.data and len(request.data['target_contacts']) <= 0):
            raise ScheduledEmailTargetNotSelectedException()


class ScheduledEmailsView(AutoPrefetchViewSetMixin, MultiTenantMixin, SummaryForListMixin, GenericListAPIView, ScheduledEmailsHelper, OrderIdHelper, BulkOperationHelper):
    queryset = ScheduledEmail.objects.extra(
        select={
            'open_ratio': 'CASE WHEN "app_staffing_scheduledemail"."text_format" = "text" THEN NULL WHEN '
                          '"app_staffing_scheduledemail"."text_format" = \'html\' THEN CASE WHEN '
                          '"app_staffing_scheduledemail"."send_total_count" = 0 THEN NULL WHEN '
                          '"app_staffing_scheduledemail"."scheduled_email_status_id" = 1 THEN NULL WHEN '
                          '"app_staffing_scheduledemail"."scheduled_email_status_id" = 2 THEN NULL WHEN '
                          '"app_staffing_scheduledemail"."scheduled_email_status_id" = 3 THEN NULL ELSE ROUND('
                          '"app_staffing_scheduledemail"."open_count" * 100.0 / '
                          '"app_staffing_scheduledemail"."send_total_count", 2) END END',
            'open_count_format': f'CASE WHEN "app_staffing_scheduledemail"."text_format" = "text" THEN NULL WHEN '
                                 f'"app_staffing_scheduledemail"."text_format" = \'html\' THEN CASE WHEN '
                                 f'"app_staffing_scheduledemail"."send_total_count" = 0 THEN NULL WHEN '
                                 f'"app_staffing_scheduledemail"."scheduled_email_status_id" = 1 THEN NULL WHEN '
                                 f'"app_staffing_scheduledemail"."scheduled_email_status_id" = 2 THEN NULL WHEN '
                                 f'"app_staffing_scheduledemail"."scheduled_email_status_id" = 3 THEN NULL ELSE '
                                 f'"app_staffing_scheduledemail"."open_count" END END',
        },
    ).prefetch_related('personnel_histories', 'project_histories').all()
    serializer_class = ScheduledEmailSerializer
    serializer_class_for_list = ScheduledEmailSummarySerializer
    pagination_class = StandardPagenation
    filterset_class = ScheduledEmailFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = (
        'scheduled_email_send_type__order',
        'scheduled_email_status__order',
        'sender__last_name',
        'date_to_send',
        'sent_date',
        'send_total_count',
        'created_time',
        'modified_time',
    )
    ordering = ('-created_time',)

    @atomic
    def post(self, request, *args, **kwargs):
        if 'date_to_send' in request.data and request.data['date_to_send']:
            base_datetime = parse_datetime(request.data['date_to_send'])
            if self.is_holiday(base_datetime):
                return Response(self.get_error_response_data_holiday(base_datetime), status=status.HTTP_400_BAD_REQUEST)
            elif self.is_not_worktime(base_datetime):
                return Response(self.get_error_response_data_not_worktime(base_datetime), status=status.HTTP_400_BAD_REQUEST)
            elif self.is_not_allowed_period(base_datetime):
                return Response(self.get_error_response_data_not_allowed_period(base_datetime), status=status.HTTP_400_BAD_REQUEST)

            scheduled_emails = self.get_mails_by_term(self.get_queryset(), base_datetime, kwargs)
            if scheduled_emails.exists():
                return Response(self.get_error_response_data(base_datetime), status=status.HTTP_400_BAD_REQUEST)

            is_delivery_interval_addon_purchased = Addon.objects.filter(
                company=self.request.user.company,
                addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID
            ).exists()

            if not is_delivery_interval_addon_purchased:
                selected_minute = base_datetime.astimezone(pytz.timezone(settings.TIME_ZONE)).minute
                if selected_minute in settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_NOT_ALLOWED_MINUTES:
                    raise ScheduledEmailDeliveryIntervalException()

        return super().post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        if self.is_undeletable(resource_ids):
            raise CannotDeleteException()
        files_attachments = ScheduledEmailAttachment.objects.filter(
            email_id__in=resource_ids, file_type=ScheduledEmailSettingFileStatus.URL
        )
        for file in files_attachments:
            # Delete file from storage
            file.file.storage.delete(name=file.file.name)

        ScheduledEmail.objects.select_related('created_user').filter(id__in=resource_ids, company=self.request.user.company).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class ScheduledEmailDetailView(MultiTenantMixin, GenericDetailAPIView, ScheduledEmailsHelper, CustomValidationHelper):
    queryset = ScheduledEmail.objects.select_related('sender').prefetch_related('attachments', 'targets', 'targets__contact', 'searchcondition')
    serializer_class = ScheduledEmailSerializer

    @atomic
    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)

        instance = self.get_object()
        self.init_tasks(instance)

        all_date_to_send = []
        if 'date_to_send' in request.data and request.data['date_to_send']:
            date_to_send = parse_datetime(request.data['date_to_send'])
            if self.is_holiday(date_to_send):
                return Response(self.get_error_response_data_holiday(date_to_send), status=status.HTTP_400_BAD_REQUEST)
            elif self.is_not_worktime(date_to_send):
                return Response(self.get_error_response_data_not_worktime(date_to_send), status=status.HTTP_400_BAD_REQUEST)
            elif self.is_not_allowed_period(date_to_send):
                return Response(self.get_error_response_data_not_allowed_period(date_to_send), status=status.HTTP_400_BAD_REQUEST)

            scheduled_emails = self.get_mails_by_term(self.get_queryset(), date_to_send, kwargs)
            if scheduled_emails.exists():
                return Response(self.get_error_response_data(date_to_send), status=status.HTTP_400_BAD_REQUEST)
            else:
                all_date_to_send.append(date_to_send)

        # 配信待ちのデータを更新する場合は宛先件数のバリデーションを行う
        if not 'status' in request.data and instance.scheduled_email_status.name == QUEUED:
            self.validate_target_count(request)

        # 配信予約が実行された際、PATCH対象のメールをコピーして、additional_date_to_sendを変更して登録する
        # status = QUEUEDの場合(配信予約を実行した場合)、メール本文、添付ファイル、配信対象は確定している(ユーザーがプレビューで確認済み)なので、PATCH対象のメールからコピーしても問題ない
        # statusがそれ以外の場合は登録フローの途中なのでメールのコピーと予約は実施しない
        if 'status' in request.data and request.data['status'] == QUEUED:
            # 入力値のバリデーション
            text_format_exists = request.data.get('text_format') or instance.text_format
            sender_exists = request.data.get('sender') or instance.sender
            subject_exists = request.data.get('subject') or instance.subject
            text_exists = request.data.get('text') or instance.text
            if not text_format_exists or not sender_exists or not subject_exists or not text_exists:
                raise ScheduledEmailPerameterException()

            # 配信件数がゼロの場合は予約できない
            self.validate_target_count(request)

            # 配信件数が上限値を超えている場合は予約できない
            limit = scheduled_email_limit_target_count(self.request.user.company)
            if limit < len(request.data['target_contacts']):
                raise ScheduledEmailTargetCountException(settings.SCHEDULED_EMAIL_TARGET_COUNT_LIMIT_ERROR_MESSAGE.format(str(limit)))

            additional_date_to_send_array = []
            for num in range(1, 5):
                additional_date_to_send_key = f'additional_date_to_send{num}'
                if additional_date_to_send_key in request.data and request.data[additional_date_to_send_key]:
                    additional_date_to_send = parse_datetime(request.data[additional_date_to_send_key])
                    additional_date_to_send_array.append(additional_date_to_send)
                    all_date_to_send.append(additional_date_to_send)

            all_date_to_send.sort()
            if len(all_date_to_send) > 1:
                for num in range(len(all_date_to_send) - 1):
                    if self.is_holiday(all_date_to_send[num]):
                        return Response(self.get_error_response_data_holiday(all_date_to_send[num]), status=status.HTTP_400_BAD_REQUEST)
                    elif self.is_not_worktime(all_date_to_send[num]):
                        return Response(self.get_error_response_data_not_worktime(all_date_to_send[num]), status=status.HTTP_400_BAD_REQUEST)
                    elif self.is_not_allowed_period(all_date_to_send[num]):
                        return Response(self.get_error_response_data_not_allowed_period(all_date_to_send[num]), status=status.HTTP_400_BAD_REQUEST)
                    elif settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_SECONDS > (all_date_to_send[num + 1] - all_date_to_send[num]).total_seconds():
                        return Response(self.get_error_response_data(all_date_to_send[num]), status=status.HTTP_400_BAD_REQUEST)

            for additional_date_to_send in additional_date_to_send_array:
                if self.is_holiday(additional_date_to_send):
                    return Response(self.get_error_response_data_holiday(additional_date_to_send), status=status.HTTP_400_BAD_REQUEST)
                elif self.is_not_worktime(additional_date_to_send):
                    return Response(self.get_error_response_data_not_worktime(additional_date_to_send), status=status.HTTP_400_BAD_REQUEST)
                elif self.is_not_allowed_period(additional_date_to_send):
                    return Response(self.get_error_response_data_not_allowed_period(additional_date_to_send), status=status.HTTP_400_BAD_REQUEST)

                scheduled_emails = self.get_mails_by_term(self.get_queryset(), additional_date_to_send, kwargs)
                if scheduled_emails.exists():
                    return Response(self.get_error_response_data(additional_date_to_send), status=status.HTTP_400_BAD_REQUEST)

            max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message(self.request.user.company)
            if max_byte_size < instance.total_byte_size:
                raise ScheduledEmailAttachementSizeException(max_byte_size_error_message)

            minutes = [date_to_send.astimezone(pytz.timezone(settings.TIME_ZONE)).minute for date_to_send in all_date_to_send]
            is_delivery_interval_addon_purchased = Addon.objects.filter(
                company=self.request.user.company,
                addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID
            ).exists()

            if not is_delivery_interval_addon_purchased:
                for minute in minutes:
                    if minute in settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_NOT_ALLOWED_MINUTES:
                        raise ScheduledEmailDeliveryIntervalException()

            original_instance = deepcopy(instance)
            targets = ScheduledEmailTarget.objects.filter(email=original_instance)
            attachments = ScheduledEmailAttachment.objects.filter(email=original_instance)

            if 'search_conditions' not in request.data:
                search_conditions = {}
            else:
                search_conditions = request.data['search_conditions']

            for additional_date_to_send in additional_date_to_send_array:
                self.create_additional_email(request, additional_date_to_send, instance, targets, attachments, search_conditions)

        return super().patch(request, *args, **kwargs)

    @atomic
    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.is_undeletable([str(instance.id)]):
            raise CannotDeleteException()
        ret = super().delete(request, *args, **kwargs)
        self.delete_tasks(instance)
        return ret

    def create_additional_email(self, request, additional_date_to_send, instance, targets, attachments, search_conditions):
        instance.pk = None  # This will create another instance.
        instance.subject = self.request.data["subject"]
        instance.text = self.request.data["text"]
        instance.text_format = self.request.data["text_format"]
        status_queued = ScheduledEmailStatus.objects.get(name=QUEUED)
        instance.scheduled_email_status = status_queued
        # Clear send statuses
        instance.sent_date = None
        instance.date_to_send = additional_date_to_send
        # Update Meta-data
        instance.created_user = self.request.user
        instance.modified_user = self.request.user
        instance.created_time = timezone.now()
        instance.modified_time = timezone.now()
        instance.save()
        # Copy related objects
        # Copy Email targets
        target_data = []
        for target in request.data["target_contacts"]:
            query_object = ScheduledEmailTarget(pk=None, email=instance, contact_id=target,\
                created_user_id=self.request.user.id, modified_user_id=self.request.user.id, company_id=instance.company_id)
            target_data.append(query_object)
        ScheduledEmailTarget.objects.bulk_create(target_data)
        # Copy Email attachments references.
        copied_attachments = []
        for attachment in attachments:
            attachment.pk = None  # This will create another instance.
            attachment.email = instance
            copied_attachments.append(attachment)
        ScheduledEmailAttachment.objects.bulk_create(copied_attachments)

        ScheduledEmailSearchCondition.objects.create(
            email=instance,
            created_user=instance.created_user,
            modified_user=instance.modified_user,
            search_condition=json.dumps(search_conditions)
        )


class ScheduledEmailPreviewView(MultiTenantMixin, GenericReadOnlyView):
    queryset = ScheduledEmail.objects.select_related('sender')
    serializer_class = ScheduledEmailPreviewSerializer


class ScheduledEmailCopyView(MultiTenantMixin, GenericActionView):
    queryset = ScheduledEmail.objects.all()
    serializer_class = ScheduledEmailSerializer

    def create(self, request, *args, **kwargs):
        with atomic():
            instance = self.get_object()
            original_instance = deepcopy(instance)

            instance.pk = None  # This will create another instance.
            instance.subject = instance.subject
            status_draft = ScheduledEmailStatus.objects.get(name=DRAFT)
            instance.scheduled_email_status = status_draft
            # Clear send statuses
            instance.sent_date = None
            instance.date_to_send = None
            instance.send_total_count = None
            instance.send_type = None
            instance.open_count = 0
            # text_format
            if instance.text_format is None:
                instance.text_format = TEXT
            # Update Meta-data
            instance.created_user = self.request.user
            instance.modified_user = self.request.user
            instance.created_time = timezone.now()
            instance.modified_time = timezone.now()
            instance.save()

            # Copy related objects
            # Copy Email targets
            targets = ScheduledEmailTarget.objects.filter(email=original_instance)
            copied_targets = []
            for target in targets:
                target.pk = None  # This will create another instance.
                target.email = instance
                copied_targets.append(target)
            ScheduledEmailTarget.objects.bulk_create(copied_targets)

            # Copy Email attachments references.
            attachments = ScheduledEmailAttachment.objects.filter(email=original_instance)
            copied_attachments = []
            for attachment in attachments:
                attachment.pk = None  # This will create another instance.
                attachment.email = instance
                copied_attachments.append(attachment)
            ScheduledEmailAttachment.objects.bulk_create(copied_attachments)

        # Return a newly created Scheduled-Email information.
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ScheduledEmailBulkCopyView(MultiTenantMixin, GenericActionView, BulkOperationHelper):
    queryset = ScheduledEmail.objects.all()
    serializer_class = ScheduledEmailSerializer

    def create(self, request, *args, **kwargs):
        with atomic():
            resource_ids = self.get_unique_items(request.data['resource_ids'])
            if self.is_over_bulk_operation_limit(resource_ids):
                raise BulkOperationLimitException()
            instances = ScheduledEmail.objects.filter(id__in=resource_ids, company=self.request.user.company)
            status_draft = ScheduledEmailStatus.objects.get(name=DRAFT)

            for instance in instances:
                original_instance = deepcopy(instance)

                instance.pk = None  # This will create another instance.
                instance.subject = instance.subject
                instance.scheduled_email_status = status_draft
                # Clear send statuses
                instance.sent_date = None
                instance.date_to_send = None
                instance.send_total_count = None
                instance.open_count = 0
                # text_format
                if instance.text_format is None:
                    instance.text_format = TEXT
                # Update Meta-data
                instance.created_user = self.request.user
                instance.modified_user = self.request.user
                instance.created_time = timezone.now()
                instance.modified_time = timezone.now()
                instance.save()

                # Copy related objects
                # Copy Email targets
                targets = ScheduledEmailTarget.objects.filter(email=original_instance)
                copied_targets = []
                for target in targets:
                    target.pk = None  # This will create another instance.
                    target.email = instance
                    copied_targets.append(target)
                ScheduledEmailTarget.objects.bulk_create(copied_targets)

                # Copy Email attachments references.
                attachments = ScheduledEmailAttachment.objects.filter(email=original_instance)
                copied_attachments = []
                for attachment in attachments:
                    attachment.pk = None  # This will create another instance.
                    attachment.email = instance
                    copied_attachments.append(attachment)
                ScheduledEmailAttachment.objects.bulk_create(copied_attachments)

        return Response({}, status=status.HTTP_201_CREATED)


class ScheduledEmailAttachmentsView(MultiTenantMixin, FileListView, ScheduledEmailsHelper):
    queryset = ScheduledEmailAttachment.objects.all()
    serializer_class = ScheduledEmailAttachmentSerializer
    pagination_class = StandardPagenation
    lookup_field = 'email'
    lookup_url_kwarg = 'pk'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(email=self.kwargs['pk'])  # Show attachments only which belong to a specific email.

    @atomic
    def perform_create(self, serializer):
        email = ScheduledEmail.objects.get(id=self.kwargs[self.lookup_url_kwarg])
        is_url_setting = (
            ScheduledEmailSetting.objects.filter(
                company=email.company, file_type=ScheduledEmailSettingFileStatus.URL).exists()
        )
        email.file_type = ScheduledEmailSettingFileStatus.URL if is_url_setting else ScheduledEmailSettingFileStatus.FILE
        if is_url_setting:
            if not email.password:
                email.password = get_random_string(length=10)
            today = datetime.today()
            today = datetime(year=today.year, month=today.month, day=today.day)
            email.expired_date = today + relativedelta(months=6)  # next 6 months
        email.save()
        obj = serializer.save(email=email)  # Append additional data.
        if is_url_setting:
            gcp_link = getattr(obj.file, "name", None)
            if gcp_link:
                obj.gcp_link = gcp_link
        obj.file_type = email.file_type
        obj.save()
        self.init_tasks(email)


class ScheduledEmailAttachmentDownloadAPIView(MultiTenantMixin, GenericDetailAPIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        attachment_ids = data.get("attachment_ids")
        files_data = ScheduledEmailService.get_stream_file_data_from_gcp(attachment_ids)
        return Response(files_data, status=status.HTTP_200_OK)


class ScheduledEmailAttachmentDownloadAuthorizeAPIView(MultiTenantMixin, GenericDetailAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializers = ScheduledEmailAttachmentSerializer
    renderer_classes = (CamelCaseJSONRenderer,)
    parser_classes = (CamelCaseJSONParser,)

    def get(self, request, pk, *args, **kwargs):
        data = request.GET.copy()
        password = data.get("password")
        today = datetime.today()
        ScheduledEmailService.delete_expired_attachments(today)
        query_set = ScheduledEmail.objects.filter(id=pk, expired_date__gte=today)
        if password:
            query_set = query_set.filter(password=password)

        if query_set.exists():
            if not password:
                return Response(status=status.HTTP_200_OK)
            else:
                data = ScheduledEmailService.get_list_attachments(pk)
                return Response(data, status=status.HTTP_200_OK)
        else:
            if password:
                raise ScheduledEmailIncorrectPassword()
            else:
                raise ScheduledEmailAttachmentNotExist()


class ScheduledEmailAttachmentDetailView(MultiTenantMixin, FileDetailView, ScheduledEmailsHelper):
    queryset = ScheduledEmailAttachment.objects.all()
    serializer_class = ScheduledEmailAttachmentSerializer

    @atomic
    def delete(self, request, *args, **kwargs):
        email_attachment = self.get_object()
        self.init_tasks(email_attachment.email)
        return super().delete(request, *args, **kwargs)


class ScheduledEmailOpenhistoryListView(GenericListAPIView,):
    queryset = ScheduledEmailOpenHistory.objects.select_related('contact', 'contact__organization')
    serializer_class = ScheduledEmailOpenHistorySerializer
    pagination_class = ScheduledEmailOpenerCustomPagination
    ordering_fields = ('opened_time')
    ordering = ('-opened_time',)

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(email=self.kwargs['pk'])


class ScheduledEmailSendErrorListView(GenericListAPIView,):
    queryset = ScheduledEmailSendError.objects.select_related('contact', 'contact__organization')
    serializer_class = ScheduledEmailSendErrorSerializer
    pagination_class = StandardPagenation

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(email=self.kwargs['pk'])


class ScheduledEmailSettingView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    serializer_class = ScheduledEmailSettingSerializer

    def get_object(self):
        company = self.request.user.company
        if hasattr(company, 'scheduledemailsetting'):
            return company.scheduledemailsetting
        else:
            return ScheduledEmailSetting.objects.create(company=company, use_open_count=False)

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)


class ScheduledEmailConnectionView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    serializer_class = ScheduledEmailSettingSerializer

    def check_smtp_connection(self, server, username, password, use_tls=False):
        if use_tls:
            server.ehlo()
            server.starttls()
            server.ehlo()
        server.login(username, password)
        server.close()

    def check_server_connection(self, username, hostname, port_number, password, use_tls, use_ssl):
        if use_ssl:
            server = smtplib.SMTP_SSL(hostname, port_number, timeout=30)
            self.check_smtp_connection(server, username, password)
        else:
            server = smtplib.SMTP(hostname, port_number, timeout=30)
            self.check_smtp_connection(server, username, password, use_tls)
        return True

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        username = request.data['username']
        hostname = request.data['hostname']
        port_number = request.data['port_number']
        password = request.data['password']
        if hostname in EMAIL_HOST_BLOCKED:
            raise EmailDomainBlocked(hostname)
        connection_type = request.data['connection_type']
        use_tls = connection_type and connection_type == settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_TLS
        use_ssl = connection_type and connection_type == settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_SSL

        try:
            if self.check_server_connection(username, hostname, port_number, password, use_tls, use_ssl):
                return Response(status=status.HTTP_200_OK, data={})
        except Exception:
            raise ScheduledEmailConnectionException()

        raise ScheduledEmailConnectionException()


class ScheduledEmailAttachmentSizeLimitAPIView(APIView):

    def get(self, request):
        company = request.user.company
        addon_id = settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID
        addon = Addon.objects.filter(company=company, addon_master_id=addon_id)
        data = {
            "is_attachment_size_extended": addon.count() == 1
        }
        return Response(status=status.HTTP_200_OK, data=data)
