from django.conf import settings
from django.db.transaction import atomic
from app_staffing.models.addon import Addon
from app_staffing.models.organization import CompanyAttribute
from app_staffing.models.plan import PlanPaymentError

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import APIException, MethodNotAllowed

from app_staffing.views.base import PayjpHelper
from app_staffing.models import User

import payjp

from datetime import datetime
from app_staffing.models import Plan
from app_staffing.utils.payment import validate_active_plans, extract_addon_total_price_and_master_ids,\
    validate_company_attributes, extract_user_add_price_and_additional_options, create_new_plan,\
    create_purchase_history, exec_charge, get_main_card_id, get_backup_card_id, get_taxed_price
from app_staffing.utils.system_mail import send_system_mail

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


class PayjpPaymentListView(APIView, PayjpHelper):

    def get(self, request, *args, **kwargs):
        try:
            data = self._get_cards(self.request.user.company_id)

            for card in data.data:
                card['isMainCard'] = card.get('id') == self._get_default_card_id(self.request.user.company_id)

            if data.count > 1 :
                if data.data[0].get('metadata').get('card_order') == '1':
                    # cardの先頭が２枚目の場合は入れ替える
                    # card_orderがnoneの場合はカード登録上限が１枚の時に作成されたものなので、順番の入れ替えは不要
                    data.data = [data.data[1], data.data[0]]
                    
            return Response(status=status.HTTP_200_OK, data=data)
        except Exception:
            raise PayjpPaymentCardReadException()

    @atomic
    def post(self, request, *args, **kwargs):
        # クレジットカードの登録処理
        try:
            payjp.api_key=settings.PAYJP_API_KEY
            old_card_id = self.request.data.get('oldCardId')
            old_default_card_id = self._get_default_card_id(self.request.user.company_id)
            # 古いカード情報が存在する場合は削除した上で登録する
            if old_card_id:
                try:
                    card = self._get_customer(self.request.user.company.id).cards.retrieve(old_card_id)
                    self._get_customer(self.request.user.company.id).cards.retrieve(old_card_id).delete()
                    logger_stdout.info('card {0} {1} for user deleted'.format(card['id'],card['last4']))
                except:
                    logger_stderr.error('no cards available for this company{0}'.format(self.request.user.company_id))
            
            # カードを登録する際にデフォルトするべきかの判定
            # １枚目の登録 or デフォルト指定カードの更新時 に デフォルト指定する必要がある
            is_default = self._get_cards(self.request.user.company_id).get("count") == 0 or old_default_card_id == old_card_id
            
            # メタデータとして作成時の画面位置(上(0) or 下(1))を設定しておく
            self._get_customer(self.request.user.company_id).cards.create(card=self.request.data.get('token'), metadata={'card_order': self.request.data.get('cardOrder')}, default=is_default)

            try:
                # メールを送信する
                send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_PAYMENT_NOTIFICATION_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_NOTIFICATION_MESSAGE)
            except Exception:
                # ここのエラーはスローしても支払い情報がロールバックされないため、ログとかにエラーを吐き出すようにする
                logger_stderr.error('send notification in update charge : failed to send notification :  company {0}'.format(self.request.user.company_id))
        except Exception:
            raise PayjpPaymentCardCreateException()

        # プラン情報を取得
        company = self.request.user.company
        try:
            current_active_plan = validate_active_plans(Plan.objects.filter(company=company, is_active=True))
        except Exception:
            current_active_plan = None

        # 現在のプランが支払いエラーになっているかどうかを確認し、なっている場合はその場で請求処理を実行する
        if current_active_plan and PlanPaymentError.objects.filter(plan=current_active_plan).exists():
            try:
                # プランに設定されている金額を取得
                plan_price = current_active_plan.plan_master.price

                # アドオン情報とアドオンの合計金額を取得する
                addon_total_price, addon_master_ids = extract_addon_total_price_and_master_ids(Addon.objects.filter(company=company))

                # テナント設定情報を取得
                company_attribute = validate_company_attributes(CompanyAttribute.objects.filter(company=company))

                # ユーザー追加料金を取得
                user_add_price, additional_options = extract_user_add_price_and_additional_options(company_attribute, current_active_plan)

                # プラン料金とアドオン料金とユーザー追加料金を加算した値が請求額になる
                total_price = plan_price + addon_total_price + user_add_price

                # 税率を乗算する
                total_taxed_price = get_taxed_price(total_price)

                # 現在のプランを非アクティブにする
                current_active_plan.is_active = False
                current_active_plan.save()

                # 新しいプランを作成
                new_plan = create_new_plan(company, current_active_plan.plan_master_id, current_active_plan.payment_date)

                # 支払い履歴を作成
                create_purchase_history(company, current_active_plan, new_plan, addon_master_ids, additional_options, total_taxed_price)

                # エラーによりデータはロールバックされて、請求だけが走ってしまうのを防ぐため、請求処理は一番最後に回す
                is_success, err_with_main, err_with_backup = exec_charge(
                    total_taxed_price,
                    company,
                    f'charge plan master : {current_active_plan.plan_master_id}, addon master : {",".join(addon_master_ids)}'
                )

                context = {'url': settings.HOST_URL}
                if is_success:
                    try:
                        send_system_mail(company.id, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_MESSAGE, context)
                    except Exception as e:
                        logger_stderr.error('update card and exec charge succeeded notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))

                    if err_with_main is not None:
                        try:
                            send_system_mail(company.id, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE, context)
                        except Exception as e:
                            logger_stderr.error('update card and exec charge succeeded with backup notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))
                else:
                    try:
                        send_system_mail(company.id, settings.TEMPLATE_EMAIL_PAYMENT_FAILED_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_FAILED_MESSAGE, context)
                    except Exception as e:
                        logger_stderr.error('update card and exec charge failed notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))
                    raise err_with_main
            except Exception as e:
                logger_stderr.error('update card and exec charge: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))
                raise PayjpPaymentChargeException()

        return Response(status=status.HTTP_201_CREATED, data=[])

    def patch(self, request, *args, **kwargs):
        # デフォルトカードIDの更新
        default_card_id = self.request.data.get('defaultCardId')
        try:
            customer = self._get_customer(self.request.user.company.id)
            # 未指定でリクエストが飛んできた場合は、現在のデフォルトが非デフォルト化された場合なので予備をデフォルトに切り替える
            if not default_card_id:
                cards = self._get_cards(self.request.user.company_id).get('data')
                for card in cards:
                    if card.get('id') != self._get_default_card_id(self.request.user.company_id):
                        default_card_id = card.get('id')
            customer.default_card = default_card_id
            customer.save()
            return Response(status=status.HTTP_204_NO_CONTENT, data=[])
        except Exception:
            raise PayjpPaymentCardUpdateException()

    def delete(self, request, *args, **kwargs):
        target_card_id = self.request.data.get('card_id')
        try:
            card = self._get_customer(self.request.user.company.id).cards.retrieve(target_card_id)
            card.delete()
            
            # ２枚の状態から削除した場合は、残りのカードの登録位置を画面の上段にして、デフォルトに指定しておく
            cards = self._get_cards(self.request.user.company_id).get('data')
            if cards:
                cards[0].metadata = {'card_order': 0}
                cards[0].save()
                
                customer = self._get_customer(self.request.user.company.id)
                customer.default_card = cards[0].get('id')
                customer.save()
                
            return Response(status=status.HTTP_204_NO_CONTENT, data=[])
        except Exception:
            raise PayjpPaymentCardDeleteException()

class PayjpPaymentCardCreateException(APIException):
    status_code = 400
    default_detail = 'クレジットカード情報の登録に失敗しました。'
    default_code = 'payjp_card_create_failed'

class PayjpPaymentCardReadException(APIException):
    status_code = 400
    default_detail = 'クレジットカード情報の取得に失敗しました。'
    default_code = 'payjp_card_read_failed'

class PayjpPaymentCardUpdateException(APIException):
    status_code = 400
    default_detail = 'クレジットカード情報の更新に失敗しました。'
    default_code = 'payjp_card_update_failed'

class PayjpPaymentCardDeleteException(APIException):
    status_code = 400
    default_detail = 'クレジットカード情報の削除に失敗しました。'
    default_code = 'payjp_card_delete_failed'

class PayjpPaymentChargeException(APIException):
    status_code = 400
    default_detail = '支払いに失敗しました。'
    default_code = 'payjp_charge_failed'
