import pytz
from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings
from rest_framework import status
from rest_framework.response import Response

from app_staffing.views.base import StatisticsAPIView
from app_staffing.services.stats import (
    ContactStats,
    OrganizationStats,
    ScheduledEmailStats,
    SharedEmailStats,
    ReceiveEmailStats,
    StaffInChargeStats,
)


class DashboardStatisView(StatisticsAPIView):

    def get(self, request, *args, **kwargs):
        company_id = request.user.company.id

        contact_stats = ContactStats(company_id=company_id).get_stats()
        organization_stats = OrganizationStats(company_id=company_id).get_stats()
        scheduled_email_stats = ScheduledEmailStats(company_id=company_id).get_stats()
        shared_email_stats = SharedEmailStats(company_id=company_id).get_stats()
        staff_in_charge_stats = StaffInChargeStats(company_id=company_id).get_stats()

        current_time = datetime.now(pytz.timezone(settings.TIME_ZONE))  # Current time by current timezone
        current_time_str = current_time.strftime(settings.DASHBOARD_DATETIME_FORMAT)

        scan_time = current_time + relativedelta(months=1)  # Refresh time 1 month after user login (refresh_time)
        scan_time_str = scan_time.strftime(settings.DASHBOARD_DATETIME_FORMAT)

        dashboard_data = {
            'contact_stats': contact_stats,
            'organization_stats': organization_stats,
            'scheduled_email_stats': scheduled_email_stats,
            'shared_email_stats': shared_email_stats,
            'staff_in_charge_stats': staff_in_charge_stats,
            'dashboard_time': {
                'calculate_time': current_time_str,
                'refresh_time': scan_time_str
            }
        }

        return Response(status=status.HTTP_200_OK, data=dashboard_data)
