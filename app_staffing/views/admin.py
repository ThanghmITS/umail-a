from django.views.generic import TemplateView
from django.conf import settings
from django.core.paginator import Paginator
from django.db.models import Prefetch
from django.template.loader import render_to_string
from django_mailbox.models import Mailbox
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from datetime import datetime
import json

from app_staffing.models import Company, Plan, PlanPaymentError, Addon, CompanyAttribute, PurchaseHistory, OrganizationStat, ContactStat, ScheduledEmailStat, Contact, ContactCategory, ContactCC, ContactComment,\
    ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference, ContactPersonnelSkillPreference, ContactPreference, ContactScore, ContactTagAssignment,\
    Organization, OrganizationBranch, OrganizationCategory, OrganizationComment, ExceptionalOrganization, Email, EmailAttachment, EmailComment, MailboxMapping,\
    ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailTarget, Addon, Plan, PlanPaymentError, PurchaseHistory, Tag
from app_staffing.models.user import User
from app_staffing.services.payment import exec_company_payment
from app_staffing.services.stats import ContactStats, OrganizationStats, ScheduledEmailStats

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)

class AdminBatchPaymentView(TemplateView, APIView):
    template_name = "admin/batch_payment.html"
    
    def _send_email(self, company, subject_template_name, message_template_name):
        master_users = company.user_set.all()
        if len(master_users) == 1:
            context = {
                'url': settings.HOST_URL,
                'display_name': master_users[0].display_name,
            }
            subject = render_to_string(subject_template_name, context)
            message = render_to_string(message_template_name, context)
            master_users[0].email_user(subject, '', html_message=message)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "決済処理の手動実行"

        companies = Company.objects.all().prefetch_related(
            Prefetch(
                'plan_set',
                queryset=Plan.objects.select_related('plan_master')
            )
        ).prefetch_related(
            Prefetch(
                'planpaymenterror_set',
                queryset=PlanPaymentError.objects.all()
            )
        ).prefetch_related(
            Prefetch(
                'purchasehistory_set',
                queryset=PurchaseHistory.objects.all()
            )
        )
        
        data = {}
        for company in companies:
            data[str(company.id)] = {
                'company': company,
                'plan': company.plan_set.all(),
                'plan_payment_error': [v for v in company.planpaymenterror_set.all()],
                'purchase_history': [v for v in company.purchasehistory_set.all()],
            }
        context["data"] = data
        
        return context
    
    def post(self, request, *args, **kwargs):
        now = datetime.now()
        company = Company.objects.prefetch_related(
            Prefetch(
                'addon_set',
                queryset=Addon.objects.select_related('addon_master')
            )
        ).prefetch_related(
            Prefetch(
                'plan_set',
                queryset=Plan.objects.select_related('plan_master').filter(is_active=True, payment_date__lte=now)
            )
        ).prefetch_related(
            Prefetch(
                'companyattribute_set',
                queryset=CompanyAttribute.objects.all()
            )
        ).prefetch_related(
            Prefetch(
                'user_set',
                queryset=User.objects.filter(user_role_id=settings.USER_ROLE_MASTER_ID)
            )
        ).get(id=request.data.get("company_id"))

        succeeded, succeeded_with_backup, failed = exec_company_payment(company)

        context = {'url': settings.HOST_URL}
        if succeeded:
            try:
                self._send_email(succeeded, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_MESSAGE)
            except Exception as e:
                logger_stderr.error('payment executed from the administration screen successed notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(succeeded.id, e))
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            if succeeded_with_backup:
                try:
                    self._send_email(succeeded_with_backup, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE)
                except Exception as e:
                    logger_stderr.error('payment executed from the administration screen successed with backup notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(succeeded_with_backup.id, e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            # 支払処理とメール送信まで成功した場合のみ200
            return Response(status=status.HTTP_200_OK)
        
        if failed:
            try:
                self._send_email(failed, settings.TEMPLATE_EMAIL_PAYMENT_FAILED_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_FAILED_MESSAGE)
            except Exception as e:
                logger_stderr.error('payment executed from the administration screen failed notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(failed.id, e))
            
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class AdminBatchCalculateStatsView(TemplateView, APIView):
    template_name = "admin/batch_calculate_stats.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "集計処理の手動実行"

        companies = Company.objects.all().prefetch_related(
            Prefetch(
                'organizationstat_set',
                queryset=OrganizationStat.objects.all()
            )
        ).prefetch_related(
            Prefetch(
                'contactstat_set',
                queryset=ContactStat.objects.all()
            )
        ).prefetch_related(
            Prefetch(
                'scheduledemailstat_set',
                queryset=ScheduledEmailStat.objects.all()
            )
        )

        data = {}
        for company in companies:
            data[str(company.id)] = {
                'company': company,
                'organization_stat': company.organizationstat_set.all(),
                'contact_stat': company.contactstat_set.all(),
                'scheduled_email_stat': company.scheduledemailstat_set.all(),
            }
        context["data"] = data

        return context

    def post(self, request, *args, **kwargs):
        company_id = request.data.get("company_id")
        try:
            target_date = None
            if request.data.get("target_year_month"):
                target_date = datetime.strptime(request.data.get("target_year_month")+'/01', '%Y/%m/%d')
            if request.data.get("is_target_contact") == 'true':
                ContactStats(company_id=company_id).calc_stats(target_date)
            if request.data.get("is_target_organization") == 'true':
                OrganizationStats(company_id=company_id).calc_stats(target_date)
            if request.data.get("is_target_scheduled_email") == 'true':
                ScheduledEmailStats(company_id=company_id).calc_stats(target_date)
        except Exception as e:
            logger_stderr.error('calculate stats  executed from the administration screen: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)

class AdminDisplayOrganizationGroupModelsView(TemplateView, APIView):
    template_name = "admin/display_organization_group_models.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "テナントごとの取引先関連モデル表示"
        company_id = self.request.GET.get('company_id')
        target_table = self.request.GET.get('target_table')

        target_table_page_num = self.request.GET.get('target_table_page_num')
        if target_table_page_num is None:
            target_table_page_num = 1
        else:
            target_table_page_num = int(target_table_page_num)

        context['selected_id'] = company_id if company_id is not None else ""
        data = {"companies": [{"id": str(v.id), "name": v.name, "is_selected": str(v.id) == company_id} for v in Company.objects.all() ]}
        if company_id is None:
            context['data'] = data
            return context

        company = Company.objects.prefetch_related(
            Prefetch(
                'companyattribute_set',
                queryset=CompanyAttribute.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contact_set',
                queryset=Contact.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactcategory_set',
                queryset=ContactCategory.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactcc_set',
                queryset=ContactCC.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactcomment_set',
                queryset=ContactComment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactjobskillpreference_set',
                queryset=ContactJobSkillPreference.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactjobtypepreference_set',
                queryset=ContactJobTypePreference.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactpersonnelskillpreference_set',
                queryset=ContactPersonnelSkillPreference.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactpersonneltypepreference_set',
                queryset=ContactPersonnelTypePreference.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactpreference_set',
                queryset=ContactPreference.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contactscore_set',
                queryset=ContactScore.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'tag_set',
                queryset=Tag.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'contacttagassignment_set',
                queryset=ContactTagAssignment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'organization_set',
                queryset=Organization.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'organizationbranch_set',
                queryset=OrganizationBranch.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'organizationcomment_set',
                queryset=OrganizationComment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'exceptionalorganization_set',
                queryset=ExceptionalOrganization.objects.all().order_by('-modified_time')
            )
        ).get(id=company_id)

        per_page = 50
        if target_table == "all":
            data["Company"] = {
                "objectList": [company]
            }

        if target_table == "all" or target_table == "CompanyAttribute":
            data["CompanyAttribute"] = get_page_attr(company.companyattribute_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "Contact":
            data["Contact"] = get_page_attr(company.contact_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactCategory":
            data["ContactCategory"] = get_page_attr(company.contactcategory_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactCC":
            data["ContactCC"] = get_page_attr(company.contactcc_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactComment":
            data["ContactComment"] = get_page_attr(company.contactcomment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactJobSkillPreference":
            data["ContactJobSkillPreference"] = get_page_attr(company.contactjobskillpreference_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactJobTypePreference":
            data["ContactJobTypePreference"] = get_page_attr(company.contactjobtypepreference_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactPersonnelSkillPreference":
            data["ContactPersonnelSkillPreference"] = get_page_attr(company.contactpersonnelskillpreference_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactPersonnelTypePreference":
            data["ContactPersonnelTypePreference"] = get_page_attr(company.contactpersonneltypepreference_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactPreference":
            data["ContactPreference"] = get_page_attr(company.contactpreference_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactScore":
            data["ContactScore"] = get_page_attr(company.contactscore_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "Tag":
            data["Tag"] = get_page_attr(company.tag_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ContactTagAssignment":
            data["ContactTagAssignment"] = get_page_attr(company.contacttagassignment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "Organization":
            data["Organization"] = get_page_attr(company.organization_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "OrganizationBranch":
            data["OrganizationBranch"] = get_page_attr(company.organizationbranch_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "OrganizationComment":
            data["OrganizationComment"] = get_page_attr(company.organizationcomment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ExceptionalOrganization":
            data["ExceptionalOrganization"] = get_page_attr(company.exceptionalorganization_set.all(), target_table_page_num)

        context['data'] = data
        return context

class AdminDisplayEmailGroupModelsView(TemplateView, APIView):
    template_name = "admin/display_email_group_models.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "テナントごとの共有メール関連モデル表示"
        company_id = self.request.GET.get('company_id')
        target_table = self.request.GET.get('target_table')

        target_table_page_num = self.request.GET.get('target_table_page_num')
        if target_table_page_num is None:
            target_table_page_num = 1
        else:
            target_table_page_num = int(target_table_page_num)

        context['selected_id'] = company_id if company_id is not None else ""
        data = {"companies": [{"id": str(v.id), "name": v.name, "is_selected": str(v.id) == company_id} for v in Company.objects.all() ]}
        if company_id is None:
            context['data'] = data
            return context

        company = Company.objects.prefetch_related(
            Prefetch(
                'email_set',
                queryset=Email.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'emailattachment_set',
                queryset=EmailAttachment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'emailcomment_set',
                queryset=EmailComment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'mailboxmapping_set',
                queryset=MailboxMapping.objects.all().order_by('-id')
            )
        ).get(id=company_id)

        per_page = 50
        if target_table == "all":
            data["Company"] = {
                "objectList": [company]
            }

        if target_table == "all" or target_table == "Email":
            data["Email"] = get_page_attr(company.email_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "EmailAttachment":
            data["EmailAttachment"] = get_page_attr(company.emailattachment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "EmailComment":
            data["EmailComment"] = get_page_attr(company.emailcomment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "MailboxMapping":
            data["MailboxMapping"] = get_page_attr(company.mailboxmapping_set.all(), target_table_page_num)

        mailbox_ids = list(company.mailboxmapping_set.all().values_list('mailbox_id', flat=True))
        if target_table == "all" or target_table == "Mailbox":
            data["Mailbox"] = get_page_attr(Mailbox.objects.filter(id__in=mailbox_ids).order_by('-name'), target_table_page_num)

        context['data'] = data
        return context

class AdminDisplayScheduledEmailGroupModelsView(TemplateView, APIView):
    template_name = "admin/display_scheduled_email_group_models.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "テナントごとの配信メール関連モデル表示"
        company_id = self.request.GET.get('company_id')
        target_table = self.request.GET.get('target_table')

        target_table_page_num = self.request.GET.get('target_table_page_num')
        if target_table_page_num is None:
            target_table_page_num = 1
        else:
            target_table_page_num = int(target_table_page_num)

        context['selected_id'] = company_id if company_id is not None else ""
        data = {"companies": [{"id": str(v.id), "name": v.name, "is_selected": str(v.id) == company_id} for v in Company.objects.all() ]}
        if company_id is None:
            context['data'] = data
            return context

        company = Company.objects.prefetch_related(
            Prefetch(
                'scheduledemail_set',
                queryset=ScheduledEmail.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'scheduledemailattachment_set',
                queryset=ScheduledEmailAttachment.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'scheduledemailtarget_set',
                queryset=ScheduledEmailTarget.objects.all().order_by('-modified_time')
            )
        ).get(id=company_id)

        per_page = 50
        if target_table == "all":
            data["Company"] = {
                "objectList": [company]
            }

        if target_table == "all" or target_table == "ScheduledEmail":
            data["ScheduledEmail"] = get_page_attr(company.scheduledemail_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ScheduledEmailAttachment":
            data["ScheduledEmailAttachment"] = get_page_attr(company.scheduledemailattachment_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "ScheduledEmailTarget":
            data["ScheduledEmailTarget"] = get_page_attr(company.scheduledemailtarget_set.all(), target_table_page_num)

        context['data'] = data
        return context

class AdminDisplayOtherGroupModelsView(TemplateView, APIView):
    template_name = "admin/display_other_group_models.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "テナントごとのその他モデル表示"
        company_id = self.request.GET.get('company_id')
        target_table = self.request.GET.get('target_table')

        target_table_page_num = self.request.GET.get('target_table_page_num')
        if target_table_page_num is None:
            target_table_page_num = 1
        else:
            target_table_page_num = int(target_table_page_num)

        context['selected_id'] = company_id if company_id is not None else ""
        data = {"companies": [{"id": str(v.id), "name": v.name, "is_selected": str(v.id) == company_id} for v in Company.objects.all() ]}
        if company_id is None:
            context['data'] = data
            return context

        company = Company.objects.prefetch_related(
            Prefetch(
                'addon_set',
                queryset=Addon.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'plan_set',
                queryset=Plan.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'planpaymenterror_set',
                queryset=PlanPaymentError.objects.all().order_by('-modified_time')
            )
        ).prefetch_related(
            Prefetch(
                'purchasehistory_set',
                queryset=PurchaseHistory.objects.all().order_by('-modified_time')
            )
        ).get(id=company_id)

        per_page = 50
        if target_table == "all":
            data["Company"] = {
                "objectList": [company]
            }

        if target_table == "all" or target_table == "Plan":
            data["Plan"] = get_page_attr(company.plan_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "Addon":
            data["Addon"] = get_page_attr(company.addon_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "PlanPaymentError":
            data["PlanPaymentError"] = get_page_attr(company.planpaymenterror_set.all(), target_table_page_num)

        if target_table == "all" or target_table == "PurchaseHistory":
            data["PurchaseHistory"] = get_page_attr(company.purchasehistory_set.all(), target_table_page_num)

        context['data'] = data
        return context

def get_page_attr(query_set, page_num):
    per_page = 50
    page = Paginator(query_set, per_page)
    return {
        "objectList": page.page(page_num).object_list,
        "description": '合計{0}件中、{1}から{2}を表示'.format(page.count, min((page_num-1)*per_page+1, page.count), min(page_num*per_page, page.count)),
        "numPages": page.num_pages,
        "pageNum": page_num,
        "prevPageNum": page_num-1,
        "nextPageNum": page_num+1,
        "hasNext": str(page.page(page_num).has_next()),
        "hasPrevious": str(page.page(page_num).has_previous()),
    }
