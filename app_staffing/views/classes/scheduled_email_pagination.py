from collections import OrderedDict

from django.conf import settings
from rest_framework import status
from rest_framework.response import Response

from .pagenation import StandardPagenation
from app_staffing.models import Addon


class ScheduledEmailOpenerCustomPagination(StandardPagenation):
    #
    def get_scheduled_email_open_count_extra_period_available(self):
        try:
            user = self.request.user
            company = user.company
            addon_master_id = settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID
            addon = Addon.objects.filter(company=company, addon_master_id=addon_master_id)
            return addon.count() == 1
        except BaseException:
            return False

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('is_open_count_extra_period_available', self.get_scheduled_email_open_count_extra_period_available()),
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
