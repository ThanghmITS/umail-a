from rest_framework.renderers import BaseRenderer


class BinaryFileRenderer(BaseRenderer):
    """A Renderer for file serving View"""
    media_type = 'application/octet-stream'
    format = None
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        raise RuntimeError('This method must not be called.')
        # This method must not be called in actual views.
