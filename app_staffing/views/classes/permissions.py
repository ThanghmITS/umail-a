from django.conf import settings
from rest_framework import permissions

from app_staffing.models import UserRole
from app_staffing.utils.user_role import is_admin_or_master


class IsSelfOrUserAdmin(permissions.BasePermission):
    """
    Object-level permission to only allow self User object to edit it.
    Assumes the model instance is a User and the model User instance has an `is_user_admin` attribute.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if not request.user.is_authenticated:  # Deny all unauthorized access.
            return False

        if request.method == 'POST':  # Only an Admin user can create a new object.
            return is_admin_or_master(request.user)
        return True

    def has_object_permission(self, request, view, obj):
        # Read operations are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Note: obj must be an instance of User and it must have a field `is_user_admin`.
        return obj == request.user or is_admin_or_master(request.user)


class EditableOnlySelfItem(permissions.BasePermission):
    """
    Prohibit Unsafe method for any users who are not the created_user of the object,
    except system admins who's is_superuser = True.
    """

    def has_object_permission(self, request, view, obj):
        # Read operations are allowed to any user.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Note: obj must have an attribute `created_user`,
        # and the User model must have attributes and 'is_superuser'.
        return (obj.created_user == request.user) or request.user.is_superuser or is_admin_or_master(request.user)


class CommentPinablePermission(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # NOTE(joshua-hashimoto): GETリクエストなどの場合は変更が行われないので許可
        method = request.method
        if method in permissions.SAFE_METHODS:
            return True

        # NOTE(joshua-hashimoto): ログインしているユーザーの権限を取得する
        current_user = request.user
        authorized_actions = settings.ROLE_AUTHORIZED_ACTIONS
        user_authorized_actions = authorized_actions[current_user.user_role.name]
        another_user_comment = user_authorized_actions.get("another_user_comment", None)

        # NOTE(joshua-hashimoto): そもそも権限が取得できない場合は操作を一律で許可しない
        if another_user_comment is None:
            return False

        pin_authorization = another_user_comment.get("pin", False)
        is_comment_pin_permitted = (obj.created_user == current_user) or pin_authorization
        return is_comment_pin_permitted
