from django.db.models import Prefetch

from app_staffing.models import EmailNotificationRule, EmailNotificationCondition, SystemNotification, UserSystemNotification
from app_staffing.views.base import GenericListAPIView, GenericDetailAPIView,\
    MultiTenantMixin, PerUserMixin, CustomValidationHelper, BulkOperationHelper
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.serializers import EmailNotificationRuleSerializer, SystemNotificationSerializer
from app_staffing.views.filters import EmailNotificationRuleFilter, SystemNotificationFilter
from rest_framework.response import Response
from rest_framework import status

from datetime import datetime, timedelta

from django.conf import settings

from app_staffing.exceptions.base import BulkOperationLimitException
import pytz

def _get_rule_queryset():
    return EmailNotificationRule.objects \
           .select_related('created_user') \
           .select_related('modified_user') \
           .prefetch_related(Prefetch('conditions', queryset=EmailNotificationCondition.objects.order_by('order')))


class EmailNotificationRuleListView(PerUserMixin, MultiTenantMixin, GenericListAPIView, BulkOperationHelper):
    queryset = _get_rule_queryset()
    serializer_class = EmailNotificationRuleSerializer
    pagination_class = StandardPagenation
    ordering_fields = ('created_time',)
    ordering = ('-created_time',)
    filterset_class = EmailNotificationRuleFilter

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        EmailNotificationRule.objects.select_related('created_user').filter(id__in=resource_ids).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class EmailNotificationRuleDetailView(PerUserMixin, MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    queryset = _get_rule_queryset()
    serializer_class = EmailNotificationRuleSerializer

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)


class SystemNotificationListView(GenericListAPIView):
    queryset = SystemNotification.objects.all()
    serializer_class = SystemNotificationSerializer
    pagination_class = StandardPagenation
    ordering_fields = ('order',)
    ordering = ('-order',)
    filterset_class = SystemNotificationFilter

    def get_queryset(self):
        now = datetime.now().astimezone(pytz.timezone(settings.TIME_ZONE))
        expiration_date = now - timedelta(settings.NOTIFICATION_DISPLAY_EXPIRATION_DATE)
        qs = super().get_queryset().filter(release_time__lte=now, release_time__gte=expiration_date)
        user = self.request.user
        qs = qs.prefetch_related(
            Prefetch('checkedusers', queryset=UserSystemNotification.objects.filter(user=user))
        )
        if self.request.GET.get("unread") == 'True':
            user_checked_system_notification_ids = UserSystemNotification.objects.filter(user=user).values_list('system_notification_id', flat=True)
            qs = qs.exclude(id__in=user_checked_system_notification_ids)
        return qs

    def post(self, request, *args, **kwargs):
        resource_ids = request.data['source']
        user = self.request.user
        resources = []
        for resource_id in resource_ids:
            resources.append(UserSystemNotification(user=user, system_notification_id=resource_id))
        UserSystemNotification.objects.bulk_create(resources)
        posted_items = SystemNotification.objects.filter(id__in=resource_ids)
        serialized_datas = []
        for item in posted_items:
            serialized_data = SystemNotificationSerializer(item).data
            serialized_datas.append(serialized_data)
        return Response(status=status.HTTP_201_CREATED, data=serialized_datas)
