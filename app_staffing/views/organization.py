from urllib import response

from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods

from app_staffing.models.email import ScheduledEmail, ScheduledEmailTarget
from .base import GenericListAPIView, GenericDetailAPIView, GenericCommentListView, GenericCommentDetailView, \
    LoneWolfException, GenericSubCommentListView

from django_filters.rest_framework import DjangoFilterBackend
from django.http import StreamingHttpResponse
from django.core.paginator import Paginator

from rest_framework import status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.exceptions import MethodNotAllowed, APIException
from rest_framework.response import Response
from rest_framework_csv.renderers import CSVRenderer,CSVStreamingRenderer
from rest_framework import serializers

from app_staffing.views.base import SummaryForListMixin, StatisticsAPIView, CsvDownloadView, MultiTenantMixin, CsvUploadView,\
    OrderIdHelper, CustomValidationHelper, CommentValidationHelper, BulkOperationHelper, CsvDownloadHelper
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.views.filters import OrganizationFilter, ContactFilter, ContactPreferenceFilter, TagFilter, OrganizationNameFilter, scheduled_email
from app_staffing.services.stats import OrganizationTrendStats

from app_staffing.models import Contact, ContactPreference, ContactComment, Company, Location, Organization,\
    OrganizationComment, ContactCC, User, ContactScore, ContactTagAssignment, Tag, ExceptionalOrganization, \
    ContactJobTypePreference, DisplaySetting, UserDisplaySetting, OrganizationCountry, OrganizationCategory,\
    OrganizationEmployeeNumber, ContactPersonnelTypePreference, OrganizationBranch, ContactCategory, ContactJobSkillPreference,\
    ContactPersonnelSkillPreference, TypePreference, SkillPreference, organization
from app_staffing.serializers import NestedContactSerializer, ContactPreferenceSummarySerializer, \
    ContactPreferenceSerializer, ContactCommentSerializer, CompanySerializer, LocationSerializer,\
    OrganizationSummarySerializer, OrganizationSerializer, OrganizationCsvSerializer, \
    OrganizationCommentSerializer, ContactCsvSerializer, OrganizationNameSummarySerializer, OrganizationNameSerializer, NestedContactEmailSerializer, \
    TagSerializer, DisplaySettingSerializer, UserDisplaySettingSerializer, OrganizationBranchSerializer, \
    ContactFullCsvSerializer, NestedContactSummarySerializer, OrganizationSubCommentSerializer, ContactSubCommentSerializer

import csv
import os
import re
import itertools
from io import TextIOWrapper, StringIO
from django.http import FileResponse
from django.db import transaction
from django.conf import settings

from django.db.transaction import atomic
from django.db.models import Prefetch, Subquery, OuterRef, Case, When, Value, Exists, IntegerField

from django.db.models.query_utils import Q
import datetime
from dateutil.relativedelta import relativedelta

from django.db.models import Prefetch

from app_staffing.utils.query_builder import generate_or_query
from app_staffing.utils.organization import is_valid_corporate_number_format


import json

from logging import getLogger
logger = getLogger(__name__)

from app_staffing.utils.custom_validation import custom_validation
from app_staffing.exceptions.validation import CannotDeleteException
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.exceptions.base import BulkOperationLimitException
from app_staffing.exceptions.organization import (
    TagRegisteredValueException,
    CsvRegisterLimitException,
    CannotUpdateContactException,
    CannotDeleteContactException
)
from app_staffing.exceptions.tag import TagRemoveException


class LocationListView(GenericListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    ordering_fields = ('name',)
    ordering = ('-name',)

    pagination_class = StandardPagenation
    page_size = 100000


class MyCompanyView(GenericDetailAPIView, CustomValidationHelper):
    serializer_class = CompanySerializer

    def get_object(self):
        company = self.request.user.company
        if company:
            return Company.objects.get(id=company.id)
        else:
            raise LoneWolfException

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

    @atomic
    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        exceptional_organizations = request.data.get("exceptional_organizations", [])
        if len(exceptional_organizations) > 3:
            raise ExceptionalOrganizationLimitException()
        exceptional_organization_ids = [x for x in exceptional_organizations]

        company = self.get_object()
        ExceptionalOrganization.objects.filter(company=company).delete()

        if len(exceptional_organization_ids) > 0:
            new_exceptional_organizations = []
            for organization in Organization.objects.filter(id__in=exceptional_organization_ids, company=company):
                new_exceptional_organization = ExceptionalOrganization(
                    organization=organization,
                    company=company,
                    created_user=request.user,
                    modified_user=request.user,
                )
                new_exceptional_organizations.append(new_exceptional_organization)

            ExceptionalOrganization.objects.bulk_create(new_exceptional_organizations)

        return super().patch(request, *args, **kwargs)


class OrganizationHelper(object):
    def is_undeletable(self, resource_ids):
        contact_ids = Contact.objects.filter(
            organization_id__in=resource_ids,
            company=self.request.user.company
        ).values_list('id', flat=True)

        scheduled_email_ids = ScheduledEmailTarget.objects.filter(
            contact_id__in=contact_ids,
            company=self.request.user.company
        ).values_list('email_id', flat=True)

        return ScheduledEmail.objects.filter(
            id__in=scheduled_email_ids,
            scheduled_email_status__name__in=['sending'],
            company=self.request.user.company
        ).exists()


class OrganizationListView(MultiTenantMixin, SummaryForListMixin, GenericListAPIView, BulkOperationHelper, OrganizationHelper):
    comments_subquery = Subquery(
        OrganizationComment.objects
            .filter(is_important=True,organization_id=OuterRef('organization_id'))
            .values_list('id', flat=True)
            .order_by('-modified_time')[:settings.LIST_COMMENT_VIEW_LIMIT_COUNT]
    )
    queryset = Organization.objects.select_related('created_user', 'modified_user', 'organization_category').prefetch_related(
        Prefetch(
            'organizationcomment_set',
            queryset=OrganizationComment.objects.filter(id__in=comments_subquery).order_by('-modified_time')
        )
    ).prefetch_related(
        Prefetch(
            'exceptionalorganization_set',
            queryset=ExceptionalOrganization.objects.all(),
            to_attr="exceptional_organizations"
        )
    ).prefetch_related(
        Prefetch(
            'branches',
            queryset=OrganizationBranch.objects.all().only('id', 'organization_id', 'name', 'address', 'building',
                                                           'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'fax3'),
        )
    ).prefetch_related('company')
    serializer_class = OrganizationSerializer
    serializer_class_for_list = OrganizationSummarySerializer
    pagination_class = StandardPagenation
    filterset_class = OrganizationFilter
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        qs = super().get_queryset()

        order = self.request.GET.get("ordering", '-created_time')
        if order in ['created_time', '-created_time']:
            qs = qs.order_by(order)
        elif order in settings.ORGANIZATION_ORDERING_FIELDS:
            # GETパラメータの値を第1ソート、データの作成日時を第2ソートにする
            qs = qs.order_by(order, '-created_time')
        else:
            qs = qs.order_by('-created_time')

        if self.request.GET.get("ignore_blocklist_filter") == 'use_blocklist_filter':
            qs = qs.exclude(is_blacklisted=True)

        if self.request.GET.get("ignore_filter") == 'ignore_filter':
            return qs

        company = self.request.user.company

        filter_params = self.trade_condition_company(company)
        if not filter_params:
            return qs

        # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=company)
        if exceptional_organizations:
            exceptional_organization_ids = [exceptional_organization.organization.id for exceptional_organization in exceptional_organizations]
            return qs.filter(Q(*filter_params) | Q(id__in=exceptional_organization_ids))
        else:
            return qs.filter(*filter_params)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        if self.is_undeletable(resource_ids):
            raise CannotDeleteException()
        Contact.objects.filter(organization_id__in=resource_ids, company=self.request.user.company).delete()
        Organization.objects.filter(id__in=resource_ids, company=self.request.user.company).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class OrganizationNamesListView(MultiTenantMixin, SummaryForListMixin, GenericListAPIView):
    queryset = Organization.objects.select_related('created_user').prefetch_related('branches').only('id', 'name', 'created_user', 'created_time')
    serializer_class = OrganizationNameSerializer
    serializer_class_for_list = OrganizationNameSummarySerializer
    pagination_class = StandardPagenation
    filterset_class = OrganizationNameFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('name', 'created_time')
    ordering = ('-created_time',)


class OrganizationDetailView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, OrganizationHelper):
    queryset = Organization.objects.select_related('created_user').prefetch_related('contact_set', 'branches')
    serializer_class = OrganizationSerializer

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.is_undeletable([str(instance.id)]):
            raise CannotDeleteException()
        Contact.objects.filter(organization_id__in=[str(instance.id)], company=self.request.user.company).delete()
        return super().delete(request, *args, **kwargs)


class OrganizationBranchDeleteView(MultiTenantMixin, GenericDetailAPIView):
    queryset = OrganizationBranch.objects.select_related('created_user').prefetch_related('contact_set')
    serializer_class = OrganizationBranchSerializer

    def delete(self, request, *args, **kwargs):
        referenced_contact_ids = Contact.objects.filter(organization_branch_id=kwargs["pk"]).values_list('id', flat=True)
        if referenced_contact_ids:
            return Response(status=status.HTTP_409_CONFLICT, data={'contact_ids': referenced_contact_ids})
        else:
            return super().delete(request, *args, **kwargs)


class OrganizationCsvRenderer(CSVStreamingRenderer):
    header = [  # Define headers to define the order of columns of rendered CSV.
        'corporate_number', 'name', 'organization_category__name', 'score', 'organization_country__name',
        'establishment_date', 'settlement_month', 'address', 'building', 'tel_full', 'fax_full', 'domain_name',
        'organization_employee_number__name', 'has_distribution_int', 'contract_int', 'capital_man_yen', 'has_p_mark_or_isms_int', 'has_invoice_system_int',
        'has_haken_int', 'branch__name', 'branch__address', 'branch__building', 'branch__tel_full', 'branch__fax_full',
        'establishment_year', 'capital_man_yen_required_for_transactions', 'p_mark_or_isms_int', 'invoice_system_int', 'haken_int',
        'is_blacklisted_int',
    ]
    labels = {  # Convert field name to display name for CSV.
        'corporate_number': '法人番号',
        'name': '取引先名',
        'organization_category__name': '取引先ステータス',
        'score': '取引先評価',
        'organization_country__name': '国籍',
        'establishment_date': '設立年月',
        'settlement_month': '決算期',
        'address': '住所(市区町村・町名・番地)',
        'building': '住所(建物)',
        'tel_full': 'TEL',
        'fax_full': 'FAX',
        'domain_name': 'URL',
        'organization_employee_number__name': '社員数',
        'has_distribution_int': '商流',
        'contract_int': '請負',
        'capital_man_yen': '資本金',
        'has_p_mark_or_isms_int': '保有資格 > Pマーク／ISMS',
        'has_invoice_system_int': '保有資格 > インボイス登録事業者',
        'has_haken_int': '保有資格 > 労働者派遣事業',
        'branch__name': '取引先支店名',
        'branch__address': '取引先支店住所(市区町村・町名・番地)',
        'branch__building': '取引先支店住所(建物)',
        'branch__tel_full': '取引先支店TEL',
        'branch__fax_full': '取引先支店FAX',
        'establishment_year': '取引に必要な設立年数',
        'capital_man_yen_required_for_transactions': '取引に必要な資本金',
        'p_mark_or_isms_int': '取引に必要な資格 > Pマーク／ISMS',
        'invoice_system_int': '取引に必要な資格 > インボイス登録事業者',
        'haken_int': '取引に必要な資格 > 労働者派遣事業',
        'is_blacklisted_int': 'ブロックリスト',
    }


class OrganizationCsvDownloadView(CsvDownloadHelper, MultiTenantMixin, CsvDownloadView):
    filterset_class = OrganizationFilter
    filter_backends = (DjangoFilterBackend,)
    page_size = 100
    queryset = Organization.objects.select_related(
        'created_user',
        'modified_user',
        'organization_country',
        'organization_employee_number',
        'organization_category',
    ).prefetch_related('branches')

    ordering_fields = settings.ORGANIZATION_ORDERING_FIELDS

    def get_filter_params(self, company):
        return self.trade_condition_company(company)

    def get_is_blacklisted_query(self, qs):
        return qs.exclude(is_blacklisted=True)

    def get_exceptional_organization_q_obj(self, exceptional_organization_ids):
        return Q(id__in=exceptional_organization_ids)

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        renderer = OrganizationCsvRenderer()
        response = StreamingHttpResponse(renderer.render(self._stream_serialized_data(qs)),
                                         content_type='text/csv')
        return response

    def _stream_serialized_data(self, queryset):
        paginator = Paginator(queryset, self.page_size)
        for page in paginator.page_range:
            yield from OrganizationCsvSerializer(paginator.page(page).object_list, many=True).data


class OrganizationCsvUploadView(CsvUploadView):
    organization_category_queryset = OrganizationCategory.objects.filter(name__in=['prospective', 'approached', 'exchanged', 'client'])
    organization_category_dict = {}
    organization_country_queryset = OrganizationCountry.objects.filter(name__in=['JP', 'KR', 'CN', 'OTHER'])
    organization_country_dict = {}
    organization_employee_queryset = OrganizationEmployeeNumber.objects.filter(name__in=['very_low', 'low', 'middle', 'semi_middle', 'high', 'very_high'])
    organization_employee_dict = {}
    def _valid_corporate_number(self, index, value, errorMessages, required_items, corporate_numbers_in_csv, already_registered_numbers):
        corporate_number = None
        if is_valid_corporate_number_format(value):
            if value in corporate_numbers_in_csv:
                errorMessages.append(self._get_duplicate_error_message(index, '法人番号', value))
            elif int(value) in already_registered_numbers:
                errorMessages.append(self._get_already_registered_error_message(index, '法人番号', value))
            else:
                corporate_numbers_in_csv.append(value)
                corporate_number = value
        elif value == '':
            if 'corporate_number' in required_items:
                errorMessages.append(self._get_required_error_message(index, '法人番号'))
        else:
            errorMessages.append(self._get_input_format_error_message(index, '法人番号', value, '13桁の数字で'))
        return corporate_number

    def _valid_name(self, index, column_name, value, errorMessages, max_length):
        name = None
        if value == '':
            errorMessages.append(self._get_required_error_message(index, column_name))
        else:
            name = value
            if str(value).find(' ') != -1 or str(value).find('　') != -1:
                errorMessages.append(self._get_cannot_input_space_error_message(index, column_name, value))
            if len(value) > max_length:
                errorMessages.append(self._get_input_format_error_message(index, column_name, value, f'{max_length}文字以内で'))
        return name

    def _valid_category(self, index, value, errorMessages):
        enum = ['見込み客', 'アプローチ済', '情報交換済', '契約実績有']
        category = organization_category = None
        if value in enum:
            if value == '見込み客':
                category = 'prospective'
            elif value == 'アプローチ済':
                category = 'approached'
            elif value == '情報交換済':
                category = 'exchanged'
            elif value == '契約実績有':
                category = 'client'
            if category in self.organization_category_dict:
                organization_category = self.organization_category_dict[category]
        elif value == '':
            errorMessages.append(self._get_required_error_message(index, '取引先ステータス'))
        else:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '取引先ステータス', value, enum))
        return category, organization_category

    def _valid_score(self, index, value, errorMessages):
        score = None
        if value in {'1', '2', '3', '4', '5'}:
            score = value
        elif value == '':
            errorMessages.append(self._get_required_error_message(index, '取引先評価'))
        else:
            errorMessages.append(self._get_out_of_range_error_message(index, '取引先評価', value, '1', '5'))
        return score

    def _valid_country(self, index, value, errorMessages, required_items):
        enum = ['JP', 'KR', 'CN', 'OTHER']
        country = organization_country = None
        if value in enum:
            country = value
            if value in self.organization_country_dict:
                organization_country = self.organization_country_dict[value]
        elif value == '':
            if 'country' in required_items:
                errorMessages.append(self._get_required_error_message(index, '国籍'))
            else:
                country = ''
        else:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '国籍', value, enum))
        return country, organization_country

    def _valid_establishment_date(self, index, value, errorMessages, required_items):
        establishment_date = None
        year_and_month = re.compile(r'^(?P<year>\d{4})[-/](?P<month>0?[1-9]|1[0-2])').search(value)
        if year_and_month:
            establishment_date = datetime.datetime(
                year=int(year_and_month['year']),
                month=int(year_and_month['month']),
                day=1
            )
            if establishment_date > datetime.datetime.now():
                errorMessages.append(self._get_future_date_error_message(index, '設立年月', value))
            establishment_date = establishment_date.strftime("%Y-%m-%d")
        elif value == '':
            if 'establishment_date' in required_items:
                errorMessages.append(self._get_required_error_message(index, '設立年月'))
        else:
            errorMessages.append(self._get_input_format_error_message(index, '設立年月', value, 'yyyy-mmの形式で'))

        return establishment_date

    def _valid_settlement_month(self, index, value, errorMessages, required_items):
        enum = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        settlement_month = None
        if value in enum:
            settlement_month = value
        elif value == '':
            if 'settlement_month' in required_items:
                errorMessages.append(self._get_required_error_message(index, '決算期'))
        else:
            errorMessages.append(self._get_out_of_range_error_message(index, '決算期', value, '1', '12'))
        return settlement_month

    def _valid_adress_building(self, index, address_value, building_value, errorMessages, required_items, max_length, is_branch):
        branch_prefix = ''
        if is_branch:
            branch_prefix = '取引先支店'

        # adress
        address = None
        if address_value != '' and str(address_value).find(' ') == -1 and str(address_value).find('　') == -1:
            address = address_value
        elif address_value == '':
            if 'address' in required_items:
                errorMessages.append(self._get_required_error_message(index, branch_prefix+'住所(市区町村・町名・番地)'))
        else:
            errorMessages.append(self._get_cannot_input_space_error_message(index, branch_prefix+'住所(市区町村・町名・番地)', address_value))

        # building
        building = None
        if str(building_value).find(' ') == -1 and str(building_value).find('　') == -1:
            building = building_value
        elif building_value != '':
            errorMessages.append(self._get_cannot_input_space_error_message(index, branch_prefix+'住所(建物)', building_value))

        if len(address_value) + len(building_value) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, branch_prefix+'住所', address_value+building_value, f'建物名を合わせて{max_length}文字以内で'))

        return address, building

    def _valid_tel(self, index, value, errorMessages, required_items, max_length, is_branch):
        column_name = 'TEL'
        if is_branch:
            column_name = '取引先支店' + column_name

        tel1 = tel2 = tel3 = None
        if re.compile(r'(^[0-9]+)-([0-9]+)-([0-9]+$)').search(value):
            tel1 = value.split("-")[0]
            tel2 = value.split("-")[1]
            tel3 = value.split("-")[2]
        elif value == '':
            if 'tel' in required_items:
                errorMessages.append(self._get_required_error_message(index, column_name))
        else:
            errorMessages.append(self._get_hyphen_format_error_message(index, column_name, value))

            if not re.compile(r'^[0-9]+$').search(value.replace('-', '').replace('ー', '')):
                errorMessages.append(self._get_input_format_error_message(index, column_name, value, '半角数字で'))

        if len(value) > max_length + 2: # ハイフンの２文字を追加
            errorMessages.append(self._get_input_format_error_message(index, column_name, value, f'半角ハイフンを含めて{max_length+2}文字以内で'))

        return tel1, tel2, tel3

    def _valid_fax(self, index, value, errorMessages, required_items, max_length, is_branch):
        column_name = 'FAX'
        if is_branch:
            column_name = '取引先支店' + column_name

        fax1 = fax2 = fax3 = None
        if re.compile(r'(^[0-9]+)-([0-9]+)-([0-9]+$)').search(value):
            fax1 = value.split("-")[0]
            fax2 = value.split("-")[1]
            fax3 = value.split("-")[2]
        elif value == '':
            if 'fax' in required_items:
                errorMessages.append(self._get_required_error_message(index, column_name))
        else:
            errorMessages.append(self._get_hyphen_format_error_message(index, column_name, value))

            if not re.compile(r'^[0-9]+$').search(value.replace('-', '').replace('ー', '')):
                errorMessages.append(self._get_input_format_error_message(index, column_name, value, '半角数字で'))

        if len(value) > max_length + 2:
            errorMessages.append(self._get_input_format_error_message(index, column_name, value, f'半角ハイフンを含めて{max_length+2}文字以内で'))

        return fax1, fax2, fax3

    def _valid_domain_name(self, index, value, errorMessages, required_items, max_length):
        domain_name = None
        if re.compile(r'^[a-zA-Z0-9!-/:-@¥[-`{-~]+$').search(value):
            domain_name = value
        elif value == '':
            if 'domain_name' in required_items:
                errorMessages.append(self._get_required_error_message(index, 'URL'))
        else:
            errorMessages.append(self._get_input_format_error_message(index, 'URL', value, '半角英数字記号で'))

        if len(value) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, 'URL', value, f'{max_length}文字以内で'))

        return domain_name

    def _valid_employee_number(self, index, value, errorMessages, required_items):
        enum = ['~10名', '11~30名', '31~50名', '51~100名', '101~300名', '301名~']
        employee_number = organization_employee_number = None
        if value in enum:
            if value == '~10名':
                employee_number = 'very_low'
            elif value == '11~30名':
                employee_number = 'low'
            elif value == '31~50名':
                employee_number = 'middle'
            elif value == '51~100名':
                employee_number = 'semi_middle'
            elif value == '101~300名':
                employee_number = 'high'
            elif value == '301名~':
                employee_number = 'very_high'
            if employee_number in self.organization_employee_dict:
                organization_employee_number = self.organization_employee_dict[employee_number]
        elif value == '':
            if 'employee_number' in required_items:
                errorMessages.append(self._get_required_error_message(index, '社員数'))
            else:
                employee_number = ''
        else:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '社員数', value, enum))

        return employee_number, organization_employee_number

    def _valid_boolean_item(self, index, column_name, value, errorMessages, required_items, key, default):
        result = None
        if value in {'0', '1'}:
            if value == '0':
                result = False
            elif value == '1':
                result = True
        elif value == '':
            if key in required_items:
                errorMessages.append(self._get_required_error_message(index, column_name))
            else:
                result = default
        else:
            errorMessages.append(self._get_boolean_item_error_message(index, column_name, value))

        return result

    def _valid_capital_man_yen(self, index, value, errorMessages, required_items, max_length, min_value):
        capital_man_yen = None
        if re.compile(r'^[0-9]+$').search(value):
            if int(value) < min_value:
                errorMessages.append(self._get_input_format_error_message(index, '資本金', value, f'資本金には{min_value}以上の数値を'))
            else:
                capital_man_yen = value
        elif value == '':
            if 'capital_man_yen' in required_items:
                errorMessages.append(self._get_required_error_message(index, '資本金'))
        else:
            errorMessages.append(self._get_input_format_error_message(index, '資本金', value, '半角の整数で'))

        if len(value) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, '資本金', value, f'{max_length}桁以内で'))

        return capital_man_yen

    def _valid_establishment_year(self, index, value, errorMessages, max_length, min_value):
        establishment_year = None
        if re.compile(r'^[0-9]+$').search(value):
            if int(value) < min_value:
                errorMessages.append(self._get_input_format_error_message(index, '取引に必要な設立年数', value, f'取引に必要な設立年数には{min_value}以上の数値を'))
            else:
                establishment_year = value
        elif value != '':
            errorMessages.append(self._get_input_format_error_message(index, '取引に必要な設立年数', value, '半角の整数で'))

        if len(value) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, '取引に必要な設立年数', value, f'{max_length}桁以内で'))

        return establishment_year

    def _valid_capital_man_yen_required_for_transactions(self, index, value, errorMessages, max_length, min_value):
        capital_man_yen_required_for_transactions = None
        if re.compile(r'^[0-9]+$').search(value):
            if int(value) < min_value:
                errorMessages.append(self._get_input_format_error_message(index, '取引に必要な資本金', value, f'取引に必要な資本金には{min_value}以上の数値を'))
            else:
                capital_man_yen_required_for_transactions = value
        elif value != '':
            errorMessages.append(self._get_input_format_error_message(index, '取引に必要な資本金', value, '半角の整数で'))

        if len(value) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, '取引に必要な資本金', value, f'{max_length}桁以内で'))

        return capital_man_yen_required_for_transactions

    def _valid_branchs(self, index, branch_names_value, branch_addresses_value, branch_buildings_value, branch_full_tels_value, branch_full_faxes_value, errorMessages, max_length_dic, branches_hash, organization_hash):
        if len(branch_names_value) > 0:
            branch_names = branch_names_value.split(',')
            for branch_name in branch_names:
                self._valid_name(index, '取引先支店名', branch_name, errorMessages, max_length_dic['name']['max'])

            branches_length = len(branch_names)
            branch_addresses = branch_addresses_value.split(',')
            if len(branch_addresses_value) != 0 and len(branch_addresses) != branches_length:
                errorMessages.append(self._get_unmatch_branch_value_length_error_message(index, '取引先支店住所(市区町村・町名・番地)', branch_addresses_value))
            branch_buildings = branch_buildings_value.split(',')
            if len(branch_buildings_value) != 0 and len(branch_buildings) != branches_length:
                errorMessages.append(self._get_unmatch_branch_value_length_error_message(index, '取引先支店住所(建物)',branch_buildings_value))
            branch_full_tels = branch_full_tels_value.split(',')
            if len(branch_full_tels_value) != 0 and len(branch_full_tels) != branches_length:
                errorMessages.append(self._get_unmatch_branch_value_length_error_message(index, '取引先支店TEL', branch_full_tels_value))
            branch_full_faxes = branch_full_faxes_value.split(',')
            if len(branch_full_faxes_value) != 0 and len(branch_full_faxes) != branches_length:
                errorMessages.append(self._get_unmatch_branch_value_length_error_message(index, '取引先支店FAX', branch_full_faxes_value))

            branches = []
            for i, branch_name in enumerate(branch_names):
                # 住所
                branch_addresses_value = branch_buildings_value = ''
                if len(branch_addresses) > i:
                    branch_addresses_value = branch_addresses[i]
                if len(branch_buildings) > i:
                    branch_buildings_value = branch_buildings[i]
                address, building = self._valid_adress_building(index, branch_addresses_value, branch_buildings_value, errorMessages, {}, max_length_dic['building']['max'], True)

                # TEL
                tel_value = ''
                if len(branch_full_tels) > i:
                    tel_value = branch_full_tels[i]
                tel1, tel2, tel3 = self._valid_tel(index, tel_value, errorMessages, {}, max_length_dic['tel']['max'], True)

                # FAX
                fax_value = ''
                if len(branch_full_faxes) > i:
                    fax_value = branch_full_faxes[i]
                fax1, fax2, fax3 = self._valid_fax(index, fax_value, errorMessages, {}, max_length_dic['fax']['max'], True)

                branch = OrganizationBranch(
                    name=branch_name,
                    address=address,
                    building=building,
                    tel1=tel1,
                    tel2=tel2,
                    tel3=tel3,
                    fax1=fax1,
                    fax2=fax2,
                    fax3=fax3,
                )

                branches.append(branch)

            if len(branches) > 0:
                branches_hash[organization_hash] = branches

    def _map_values(self):
        organization_category_queryset = self.organization_category_queryset
        for organization_category in organization_category_queryset:
            self.organization_category_dict[organization_category.name] = organization_category

        organization_country_queryset = self.organization_country_queryset
        for organization_country in organization_country_queryset:
            self.organization_country_dict[organization_country.name] = organization_country

        organization_employee_queryset = self.organization_employee_queryset
        for organization_employee in organization_employee_queryset:
            self.organization_employee_dict[organization_employee.name] = organization_employee

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        register_successed = False
        self._map_values()

        f = request.FILES['file']
        data = TextIOWrapper(f.file, encoding='shift_jis')
        raw_csv = csv.reader(data)
        next(raw_csv) # ヘッダ行を飛ばす処理

        csv_datas = list(raw_csv)
        if len(csv_datas) > settings.CSV_REGISTERABLE_LIMIT:
            raise CsvRegisterLimitException(
                settings.CSV_REGISTER_LIMIT_ERROR_MESSAGE.format(settings.CSV_REGISTERABLE_LIMIT)
            )

        corporate_numbers_for_search = []
        for index, line in enumerate(csv_datas):
            if not len(line) == settings.CSV_ORGANIZATION_COLUMN_LENGTH:
                continue

            corporate_number_in_csv = line[0].replace(" ", "")
            if corporate_number_in_csv and is_valid_corporate_number_format(corporate_number_in_csv):
                corporate_numbers_for_search.append(corporate_number_in_csv)

        already_registered_numbers = Organization.objects.filter(
            corporate_number__in=corporate_numbers_for_search,
            company_id=request.user.company_id
        ).values_list('corporate_number', flat=True)
        corporate_numbers_in_csv = []
        required_items = self._get_required_items('organizations')
        organizations = []
        branches_hash = {}
        errorMessages = []
        length_valid = settings.LENGTH_VALIDATIONS['organization']
        value_range_valid = settings.VALUE_RANGE_VALIDATIONS['organization']

        for index, line in enumerate(csv_datas):
            if not len(line) == settings.CSV_ORGANIZATION_COLUMN_LENGTH:
                errorMessages.append(self._get_column_length_error_message(index))
                continue

            # 法人番号
            corporate_number = self._valid_corporate_number(index, line[0], errorMessages, required_items, corporate_numbers_in_csv, already_registered_numbers)

            # 取引先名称
            name = self._valid_name(index, '取引先名', line[1], errorMessages, length_valid['name']['max'])

            # 取引作ステータス
            category, organization_category = self._valid_category(index, line[2], errorMessages)

            # 取引先評価
            score = self._valid_score(index, line[3], errorMessages)

            # 国籍
            country, organization_country = self._valid_country(index, line[4], errorMessages, required_items)

            # 設立年月
            establishment_date = self._valid_establishment_date(index, line[5], errorMessages, required_items)

            # 決算期
            settlement_month = self._valid_settlement_month(index, line[6], errorMessages, required_items)

            # 住所(市区町村・町名・番地), 住所(建物)
            address, building = self._valid_adress_building(index, line[7], line[8], errorMessages, required_items, length_valid['building']['max'], False)

            # TEL
            tel1, tel2, tel3 = self._valid_tel(index, line[9], errorMessages, required_items, length_valid['tel']['max'], False)

            # FAX
            fax1, fax2, fax3 = self._valid_fax(index, line[10], errorMessages, required_items, length_valid['fax']['max'], False)

            # URL
            domain_name = self._valid_domain_name(index, line[11], errorMessages, required_items, length_valid['domain_name']['max'])

            # 社員数
            employee_number, organization_employee_number = self._valid_employee_number(index, line[12], errorMessages, required_items)

            # 商流
            has_distribution = self._valid_boolean_item(index, '商流', line[13], errorMessages, required_items, 'has_distribution', None)

            # 請負
            contract = self._valid_boolean_item(index, '請負', line[14], errorMessages, required_items, 'contract', None)

            # 資本金
            capital_man_yen = self._valid_capital_man_yen(index, line[15], errorMessages, required_items, length_valid['capital_man_yen']['max'], value_range_valid['capital_man_yen']['min'])

            # 保有資格 > Pマーク／ISMS
            has_p_mark_or_isms = self._valid_boolean_item(index, '保有資格 > Pマーク／ISMS', line[16], errorMessages, required_items, 'license', None)

            # 保有資格 > インボイス登録事業者
            has_invoice_system = self._valid_boolean_item(index, '保有資格 > インボイス登録事業者', line[17], errorMessages, required_items, 'license', None)

            # 保有資格 > 労働者派遣事業
            has_haken = self._valid_boolean_item(index, '保有資格 > 労働者派遣事業', line[18], errorMessages, required_items, 'license', None)

            # 取引に必要な設立年数
            establishment_year = self._valid_establishment_year(index, line[24], errorMessages, length_valid['establishment_year']['max'], value_range_valid['establishment_year']['min'])

            # 取引に必要な資本金
            capital_man_yen_required_for_transactions = self._valid_capital_man_yen_required_for_transactions(index, line[25], errorMessages, length_valid['capital_man_yen_required_for_transactions']['max'], value_range_valid['capital_man_yen_required_for_transactions']['min'])

            # 取引に必要な資格 > Pマーク／ISMS
            p_mark_or_isms = self._valid_boolean_item(index, '取引に必要な資格 > Pマーク／ISMS', line[26], errorMessages, {}, '', False)

            # 取引に必要な資格 > インボイス登録事業者
            invoice_system = self._valid_boolean_item(index, '取引に必要な資格 > インボイス登録事業者', line[27], errorMessages, {}, '', False)

            # 取引に必要な資格 > 労働者派遣事業
            haken = self._valid_boolean_item(index, '取引に必要な資格 > 労働者派遣事業', line[28], errorMessages, {}, '', False)

            # ブロックリスト
            is_blacklisted = self._valid_boolean_item(index, 'ブロックリスト', line[29], errorMessages, {}, '', False)

            organization = Organization(
                corporate_number=corporate_number,
                name=name,
                category=category,
                is_blacklisted=is_blacklisted,
                score=score,
                country=country,
                establishment_date=establishment_date,
                settlement_month=settlement_month,
                address=address,
                building=building,
                tel1=tel1,
                tel2=tel2,
                tel3=tel3,
                fax1=fax1,
                fax2=fax2,
                fax3=fax3,
                domain_name=domain_name,
                employee_number=employee_number,
                contract=contract,
                capital_man_yen=capital_man_yen,
                has_p_mark_or_isms=has_p_mark_or_isms,
                has_invoice_system=has_invoice_system,
                has_haken=has_haken,
                has_distribution=has_distribution,
                establishment_year=establishment_year,
                capital_man_yen_required_for_transactions=capital_man_yen_required_for_transactions,
                p_mark_or_isms=p_mark_or_isms,
                invoice_system=invoice_system,
                haken=haken,
                company=request.user.company,
                created_user=request.user,
                modified_user=request.user,
                organization_category=organization_category,
                organization_employee_number=organization_employee_number,
                organization_country=organization_country,
            )

            # 取引先支店情報
            self._valid_branchs(index, line[19], line[20], line[21], line[22], line[23], errorMessages, length_valid['branches']['columns'], branches_hash, organization.organization_hash)

            organizations.append(organization)

        # 入力エラーが存在しない場合のみ登録処理を実行する
        if len(errorMessages) == 0 and len(organizations) > 0:
            created_organizations = Organization.objects.bulk_create(organizations)
            self._register_organization_branches(created_organizations, branches_hash)
            register_successed = True

        return Response(status=status.HTTP_201_CREATED, data={ 'register_successed': register_successed, 'errorMessages': errorMessages })

    def _register_organization_branches(self, created_organizations, branches_hash):
        tobe_created = []
        for organization in created_organizations:
            branches = branches_hash.get(organization.organization_hash)
            if branches is not None:
                for branch in branches:
                    branch.organization = organization
                    branch.company = organization.company
                    tobe_created.append(branch)
        if len(tobe_created) > 0:
            OrganizationBranch.objects.bulk_create(tobe_created)

class OrganizationStatsView(StatisticsAPIView):

    def get(self, request, *args, **kwargs):
        cid = request.user.company.id
        trend = OrganizationTrendStats(company_id=cid).get_stats()
        data = {
            'trend': {
                'register': {
                    'labels': trend['labels'],
                    'counts': trend['counts']
                }
            }
        }

        return Response(status=status.HTTP_200_OK, data=data)


class ContactHelper(object):
    def is_undeletable(self, resource_ids):
        scheduled_email_ids = ScheduledEmailTarget.objects.filter(
            contact_id__in=resource_ids,
            company=self.request.user.company
        ).values_list('email_id', flat=True)

        return ScheduledEmail.objects.filter(
            id__in=scheduled_email_ids,
            scheduled_email_status__name__in=['sending'],
            company=self.request.user.company
        ).exists()

    def _check_scheduled_email_exists(self, resource_ids):
        return ScheduledEmailTarget.objects.filter(
            contact_id__in=resource_ids,
            company=self.request.user.company,
            email__scheduled_email_status__name__in=['sending']
        ).exists()

    def _check_info_contact(self, data_request, resource_id):
        """Check the change information of contact"""

        record_contact_exists = Contact.objects.filter(
            company=self.request.user.company,
            id=resource_id,
            first_name=data_request['first_name'],
            last_name=data_request['last_name'],
            email=data_request['email'],
            organization_id=data_request['organization']
        ).exists()

        if not record_contact_exists:
            raise CannotUpdateContactException()

        """Check CC mail"""
        cc_mail = data_request.get('cc_mails', [])
        record_contact_cc_mail = ContactCC.objects.filter(
            contact_id=resource_id,
            company=self.request.user.company
        )

        request_cc_mail = len(cc_mail)
        if request_cc_mail != record_contact_cc_mail.count():
            raise CannotUpdateContactException()
        else:
            same_cc_mail = record_contact_cc_mail.filter(
                email__in=cc_mail).count()
            if same_cc_mail != request_cc_mail:
                raise CannotUpdateContactException()

    def _check_data_preference(self, list_data_preference, data_request_preference, list_field_preference, data_type):
        """Check data preference in database with preference data request"""
        unreachable_value = "-999"
        for data_preference_key in list_field_preference:
            data_request_preference_value = data_request_preference.get(data_preference_key, unreachable_value)
            existed_in_list = bool(data_preference_key in list_data_preference)
            if existed_in_list != (data_request_preference_value is existed_in_list):
                raise CannotUpdateContactException()

    def _check_contact_skill_preference(self, data_request, resource_id):
        """check the skill preference changes"""

        """Data format skill preference"""
        contact_skill_preference = [
            "dev_youken",
            "dev_kihon",
            "dev_syousai",
            "dev_seizou",
            "dev_test",
            "dev_hosyu",
            "dev_beginner",
            "infra_youken",
            "infra_kihon",
            "infra_syousai",
            "infra_kouchiku",
            "infra_test",
            "infra_hosyu",
            "infra_kanshi",
            "infra_beginner"
        ]

        """
        Check contact job skill preference
        Query data job skill preference of contact in ContactJobSkillPreference
        """
        list_contact_job_skill_preference = ContactJobSkillPreference.objects.filter(
            company=self.request.user.company,
            contact_id=resource_id,
        ).values_list('skill_preference__name', flat=True)

        self._check_data_preference(list_contact_job_skill_preference, data_request['jobskillpreference'],
                                    contact_skill_preference, 'job skill')

        """
        Check contact personnel skill preference
        Query data personnel skill preference of contact in ContactPersonnelSkillPreference
        """
        list_contact_personnel_skill_preference = ContactPersonnelSkillPreference.objects.filter(
            company=self.request.user.company,
            contact_id=resource_id,
        ).values_list('skill_preference__name', flat=True)

        self._check_data_preference(list_contact_personnel_skill_preference, data_request['personnelskillpreference'],
                                    contact_skill_preference, 'personnel skill')

    def _check_contact_type_preference(self, data_request, resource_id):
        """check the type preference changes"""

        """Data format type preference"""
        contact_type_preference = [
            "dev_designer",
            "dev_front",
            "dev_server",
            "dev_pm",
            "dev_other",
            "infra_server",
            "infra_network",
            "infra_security",
            "infra_database",
            "infra_sys",
            "infra_other",
            "other_eigyo",
            "other_kichi",
            "other_support",
            "other_other"
        ]

        """
        Check contact job type preference
        Query data job type preference of contact in ContactJobTypePreference
        """
        list_contact_job_type_preference = ContactJobTypePreference.objects.filter(
            company=self.request.user.company,
            contact_id=resource_id,
        ).values_list('type_preference__name', flat=True)

        self._check_data_preference(list_contact_job_type_preference, data_request['jobtypepreference'],
                                    contact_type_preference, 'job type')

        """
        Check personnel type preference
        Query data personnel type preference of contact in ContactPersonnelTypePreference
        """
        list_contact_personnel_type_preference = ContactPersonnelTypePreference.objects.filter(
            company=self.request.user.company,
            contact_id=resource_id,
        ).values_list('type_preference__name', flat=True)

        self._check_data_preference(list_contact_personnel_type_preference, data_request['personneltypepreference'],
                                    contact_type_preference, 'personnel type')

    def _check_contact_preference(self, data_request, resource_id):
        """Check contact preference"""
        data_request_preference = data_request['preference']

        """Query data preference of contact in ContactPreference"""
        contact_personnel_preference = ContactPreference.objects.filter(
            company=self.request.user.company,
            contact_id=resource_id,
        ).first()

        """Data format contact preference"""
        checked_fields = [
            "wants_location_hokkaido_japan",
            "wants_location_touhoku_japan",
            "wants_location_kanto_japan",
            "wants_location_kansai_japan",
            "wants_location_chubu_japan",
            "wants_location_kyushu_japan",
            "wants_location_other_japan",
            "wants_location_chugoku_japan",
            "wants_location_shikoku_japan",
            "wants_location_toukai_japan",
            "job_koyou_proper",
            "job_koyou_free",
            "job_syouryu",
            "personnel_syouryu",
        ]

        """Check data preference of contact"""
        unreachable_value = '-999'
        for data_preference_key in checked_fields:
            data_preference_value = getattr(contact_personnel_preference, data_preference_key)
            data_request_preference_value = data_request_preference.get(data_preference_key, unreachable_value)
            if data_request_preference_value != data_preference_value:
                raise CannotUpdateContactException()

class ContactListView(MultiTenantMixin, GenericListAPIView, OrderIdHelper, BulkOperationHelper, ContactHelper):
    comments_subquery = Subquery(
        ContactComment.objects
            .filter(is_important=True,contact_id=OuterRef('contact_id'))
            .values_list('id', flat=True)
            .order_by('-modified_time')[:settings.LIST_COMMENT_VIEW_LIMIT_COUNT]
    )
    queryset = Contact.objects.all().select_related(
        'organization',
        'organization_branch',
        'contactpreference',
        'staff',
        'created_user',
        'modified_user',
        'company'
    ).prefetch_related(
        Prefetch(
            'contactcomment_set',
            queryset=ContactComment.objects.filter(id__in=comments_subquery).order_by('-modified_time')
        )
    ).prefetch_related(
        Prefetch(
            'organization__exceptionalorganization_set',
            queryset=ExceptionalOrganization.objects.all(),
            to_attr="exceptional_organizations"
        )
    ).prefetch_related(
        'cc_addresses',
        'categories',
        'contactjobtypepreferences',
        'contactjobskillpreferences',
        'contactpersonneltypepreferences',
        'contactpersonnelskillpreferences',
        'contactjobtypepreferences__type_preference',
        'contactjobskillpreferences__skill_preference',
        'contactpersonneltypepreferences__type_preference',
        'contactpersonnelskillpreferences__skill_preference',
        'scores',
    ).prefetch_related(
        Prefetch(
            'tag_assignment',
            queryset=ContactTagAssignment.objects.select_related('tag').filter(tag__deleted_at__isnull=True).order_by('tag__created_time'),
        )
    )
    serializer_class = NestedContactSerializer
    pagination_class = StandardPagenation
    filter_backends = (DjangoFilterBackend,)
    filterset_class = ContactFilter

    def get_queryset(self):
        qs = super().get_queryset()

        order = self.request.GET.get("ordering", '-created_time')
        if order in ['created_time', '-created_time']:
            qs = qs.order_by(order)
        elif order in settings.CONTACT_ORDERING_FIELDS:
            # GETパラメータの値を第1ソート、データの作成日時を第2ソートにする
            qs = qs.order_by(order, '-created_time')
        else:
            qs = qs.order_by('-created_time')

        if self.request.GET.get("ignore_blocklist_filter") == 'use_blocklist_filter':
            qs = qs.exclude(organization__is_blacklisted=True)

        if self.request.GET.get("ignore_filter") == 'ignore_filter':
            return qs

        company = self.request.user.company
        filter_params = self.trade_condition_company(
            company=company,
            capital_man_yen_key='organization__capital_man_yen',
            has_p_mark_or_isms_key='organization__has_p_mark_or_isms',
            has_invoice_system_key='organization__has_invoice_system',
            has_haken_key='organization__has_haken',
            has_distribution_key='organization__has_distribution',
            establishment_date_key='organization__establishment_date'
        )
        if not filter_params:
            return qs

        # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=company)
        if exceptional_organizations:
            exceptional_organization_ids = [exceptional_organization.organization.id for exceptional_organization in exceptional_organizations]
            qs = qs.filter(Q(*filter_params) | Q(organization_id__in=exceptional_organization_ids))
        else:
            qs = qs.filter(*filter_params)

        return qs

    def get(self, request, *args, **kwargs):
        """Get contact list"""
        self.serializer_class = NestedContactSummarySerializer
        return super().get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        if self.is_undeletable(resource_ids):
            raise CannotDeleteContactException()
        Contact.objects.filter(id__in=resource_ids, company=self.request.user.company).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class ContactEmailsListView(MultiTenantMixin, GenericListAPIView):
    queryset = Contact.objects.all().select_related('organization', 'contactpreference', 'created_user')
    serializer_class = NestedContactEmailSerializer
    pagination_class = StandardPagenation
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = ContactFilter

    ordering_fields = ('name', 'email', 'organization__name', 'staff_name', 'created_time', 'modified_time')
    ordering = ('-created_time',)


class ContactDetailView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, ContactHelper):
    queryset = Contact.objects.select_related('organization', 'staff')
    serializer_class = NestedContactSerializer

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        resource_id = str(instance.id)
        self.exec_custom_validation(request)

        if self._check_scheduled_email_exists([resource_id]):
            data_request = request.data.copy()
            self._check_info_contact(data_request, resource_id)
            self._check_contact_skill_preference(data_request, resource_id)
            self._check_contact_type_preference(data_request, resource_id)
            self._check_contact_preference(data_request, resource_id)

        return super().patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        resource_id = str(instance.id)
        if self.is_undeletable([resource_id]):
            raise CannotDeleteContactException()
        return super().delete(request, *args, **kwargs)


class ContactFullCsvRenderer(CSVStreamingRenderer):
    header = [  # Define headers to define the order of columns of rendered CSV.
        'last_name', 'first_name', 'related_organization__name', 'email', 'cc', 'tel_full',
        'position', 'department', 'related_staff__email', 'last_visit', 'tag', 'category',
        'contactpreference_locations.wants_location_hokkaido_japan', 'contactpreference_locations.wants_location_touhoku_japan', 'contactpreference_locations.wants_location_kanto_japan',
        'contactpreference_locations.wants_location_chubu_japan', 'contactpreference_locations.wants_location_toukai_japan', 'contactpreference_locations.wants_location_kansai_japan',
        'contactpreference_locations.wants_location_shikoku_japan', 'contactpreference_locations.wants_location_chugoku_japan', 'contactpreference_locations.wants_location_kyushu_japan',
        'contactpreference_locations.wants_location_other_japan',
        'contactjobtypepreferences.dev_designer', 'contactjobtypepreferences.dev_front', 'contactjobtypepreferences.dev_server', 'contactjobtypepreferences.dev_pm', 'contactjobtypepreferences.dev_other',
        'contactjobskillpreferences.dev_youken', 'contactjobskillpreferences.dev_kihon', 'contactjobskillpreferences.dev_syousai', 'contactjobskillpreferences.dev_seizou', 'contactjobskillpreferences.dev_test', 'contactjobskillpreferences.dev_hosyu', 'contactjobskillpreferences.dev_beginner',
        'contactjobtypepreferences.infra_server', 'contactjobtypepreferences.infra_network', 'contactjobtypepreferences.infra_security', 'contactjobtypepreferences.infra_database', 'contactjobtypepreferences.infra_sys', 'contactjobtypepreferences.infra_other',
        'contactjobskillpreferences.infra_youken', 'contactjobskillpreferences.infra_kihon', 'contactjobskillpreferences.infra_syousai', 'contactjobskillpreferences.infra_kouchiku', 'contactjobskillpreferences.infra_test', 'contactjobskillpreferences.infra_kanshi', 'contactjobskillpreferences.infra_hosyu', 'contactjobskillpreferences.infra_beginner',
        'contactjobtypepreferences.other_eigyo', 'contactjobtypepreferences.other_kichi', 'contactjobtypepreferences.other_support', 'contactjobtypepreferences.other_other',
        'contactpreference_job_syouryu',
        'contactpersonneltypepreferences.dev_designer', 'contactpersonneltypepreferences.dev_front', 'contactpersonneltypepreferences.dev_server', 'contactpersonneltypepreferences.dev_pm', 'contactpersonneltypepreferences.dev_other',
        'contactpersonnelskillpreferences.dev_youken', 'contactpersonnelskillpreferences.dev_kihon', 'contactpersonnelskillpreferences.dev_syousai', 'contactpersonnelskillpreferences.dev_seizou', 'contactpersonnelskillpreferences.dev_test', 'contactpersonnelskillpreferences.dev_hosyu', 'contactpersonnelskillpreferences.dev_beginner',
        'contactpersonneltypepreferences.infra_server', 'contactpersonneltypepreferences.infra_network', 'contactpersonneltypepreferences.infra_security', 'contactpersonneltypepreferences.infra_database', 'contactpersonneltypepreferences.infra_sys', 'contactpersonneltypepreferences.infra_other',
        'contactpersonnelskillpreferences.infra_youken', 'contactpersonnelskillpreferences.infra_kihon', 'contactpersonnelskillpreferences.infra_syousai', 'contactpersonnelskillpreferences.infra_kouchiku', 'contactpersonnelskillpreferences.infra_test', 'contactpersonnelskillpreferences.infra_kanshi', 'contactpersonnelskillpreferences.infra_hosyu', 'contactpersonnelskillpreferences.infra_beginner',
        'contactpersonneltypepreferences.other_eigyo', 'contactpersonneltypepreferences.other_kichi', 'contactpersonneltypepreferences.other_support', 'contactpersonneltypepreferences.other_other',
        'contactpreference_koyou.job_koyou_proper', 'contactpreference_koyou.job_koyou_free', 'contactpreference_personnel_syouryu',
    ]
    labels = {  # Convert field name to display name for CSV.
        'last_name': '取引先担当者名(姓)',
        'first_name': '取引先担当者名(名)',
        'related_organization__name': '所属取引先',
        'email': 'メールアドレス > TO',
        'cc': 'メールアドレス > CC',
        'tel_full': 'TEL',
        'position': '役職',
        'department': '部署',
        'related_staff__email': '自社担当者(メールアドレス)',
        'last_visit': '最終商談日',
        'tag': 'タグ',
        'category': '相性',
        'contactpreference_locations.wants_location_hokkaido_japan': '希望エリア > 北海道',
        'contactpreference_locations.wants_location_touhoku_japan': '希望エリア > 東北',
        'contactpreference_locations.wants_location_kanto_japan': '希望エリア > 関東',
        'contactpreference_locations.wants_location_chubu_japan': '希望エリア > 中部',
        'contactpreference_locations.wants_location_toukai_japan': '希望エリア > 東海',
        'contactpreference_locations.wants_location_kansai_japan': '希望エリア > 関西',
        'contactpreference_locations.wants_location_shikoku_japan': '希望エリア > 四国',
        'contactpreference_locations.wants_location_chugoku_japan': '希望エリア > 中国',
        'contactpreference_locations.wants_location_kyushu_japan': '希望エリア > 九州',
        'contactpreference_locations.wants_location_other_japan': '希望エリア > その他',
        'contactjobtypepreferences.dev_designer': '案件配信 > 開発 > 職種詳細 > デザイナー',
        'contactjobtypepreferences.dev_front': '案件配信 > 開発 > 職種詳細 > フロントエンド',
        'contactjobtypepreferences.dev_server': '案件配信 > 開発 > 職種詳細 > バックエンド',
        'contactjobtypepreferences.dev_pm': '案件配信 > 開発 > 職種詳細 > PM・ディレクター',
        'contactjobtypepreferences.dev_other': '案件配信 > 開発 > 職種詳細 > その他',
        'contactjobskillpreferences.dev_youken': '案件配信 > 開発 > スキル詳細 > 要件定義',
        'contactjobskillpreferences.dev_kihon': '案件配信 > 開発 > スキル詳細 > 基本設計',
        'contactjobskillpreferences.dev_syousai': '案件配信 > 開発 > スキル詳細 > 詳細設計',
        'contactjobskillpreferences.dev_seizou': '案件配信 > 開発 > スキル詳細 > 製造',
        'contactjobskillpreferences.dev_test': '案件配信 > 開発 > スキル詳細 > テスト・検証',
        'contactjobskillpreferences.dev_hosyu': '案件配信 > 開発 > スキル詳細 > 保守・運用',
        'contactjobskillpreferences.dev_beginner': '案件配信 > 開発 > スキル詳細 > 未経験',
        'contactjobtypepreferences.infra_server': '案件配信 > インフラ > 職種詳細 > サーバー',
        'contactjobtypepreferences.infra_network': '案件配信 > インフラ > 職種詳細 > ネットワーク',
        'contactjobtypepreferences.infra_security': '案件配信 > インフラ > 職種詳細 > セキュリティ',
        'contactjobtypepreferences.infra_database': '案件配信 > インフラ > 職種詳細 > データベース',
        'contactjobtypepreferences.infra_sys': '案件配信 > インフラ > 職種詳細 > 情報システム',
        'contactjobtypepreferences.infra_other': '案件配信 > インフラ > 職種詳細 > その他',
        'contactjobskillpreferences.infra_youken': '案件配信 > インフラ > スキル詳細 > 要件定義',
        'contactjobskillpreferences.infra_kihon': '案件配信 > インフラ > スキル詳細 > 基本設計',
        'contactjobskillpreferences.infra_syousai': '案件配信 > インフラ > スキル詳細 > 詳細設計',
        'contactjobskillpreferences.infra_kouchiku': '案件配信 > インフラ > スキル詳細 > 構築',
        'contactjobskillpreferences.infra_test': '案件配信 > インフラ > スキル詳細 > テスト・検証',
        'contactjobskillpreferences.infra_kanshi': '案件配信 > インフラ > スキル詳細 > 監視',
        'contactjobskillpreferences.infra_hosyu': '案件配信 > インフラ > スキル詳細 > 保守・運用',
        'contactjobskillpreferences.infra_beginner': '案件配信 > インフラ > スキル詳細 > 未経験',
        'contactjobtypepreferences.other_eigyo': '案件配信 > その他 > 職種詳細 > 営業・事務',
        'contactjobtypepreferences.other_kichi': '案件配信 > その他 > 職種詳細 > 基地局',
        'contactjobtypepreferences.other_support': '案件配信 > その他 > 職種詳細 > コールセンター・サポートデスク',
        'contactjobtypepreferences.other_other': '案件配信 > その他 > 職種詳細 > その他',
        'contactpreference_job_syouryu': '案件配信 > 商流制限',
        'contactpersonneltypepreferences.dev_designer': '要員配信 > 開発 > 職種詳細 > デザイナー',
        'contactpersonneltypepreferences.dev_front': '要員配信 > 開発 > 職種詳細 > フロントエンド',
        'contactpersonneltypepreferences.dev_server': '要員配信 > 開発 > 職種詳細 > バックエンド',
        'contactpersonneltypepreferences.dev_pm': '要員配信 > 開発 > 職種詳細 > PM・ディレクター',
        'contactpersonneltypepreferences.dev_other': '要員配信 > 開発 > 職種詳細 > その他',
        'contactpersonnelskillpreferences.dev_youken': '要員配信 > 開発 > スキル詳細 > 要件定義',
        'contactpersonnelskillpreferences.dev_kihon': '要員配信 > 開発 > スキル詳細 > 基本設計',
        'contactpersonnelskillpreferences.dev_syousai': '要員配信 > 開発 > スキル詳細 > 詳細設計',
        'contactpersonnelskillpreferences.dev_seizou': '要員配信 > 開発 > スキル詳細 > 製造',
        'contactpersonnelskillpreferences.dev_test': '要員配信 > 開発 > スキル詳細 > テスト・検証',
        'contactpersonnelskillpreferences.dev_hosyu': '要員配信 > 開発 > スキル詳細 > 保守・運用',
        'contactpersonnelskillpreferences.dev_beginner': '要員配信 > 開発 > スキル詳細 > 未経験',
        'contactpersonneltypepreferences.infra_server': '要員配信 > インフラ > 職種詳細 > サーバー',
        'contactpersonneltypepreferences.infra_network': '要員配信 > インフラ > 職種詳細 > ネットワーク',
        'contactpersonneltypepreferences.infra_security': '要員配信 > インフラ > 職種詳細 > セキュリティ',
        'contactpersonneltypepreferences.infra_database': '要員配信 > インフラ > 職種詳細 > データベース',
        'contactpersonneltypepreferences.infra_sys': '要員配信 > インフラ > 職種詳細 > 情報システム',
        'contactpersonneltypepreferences.infra_other': '要員配信 > インフラ > 職種詳細 > その他',
        'contactpersonnelskillpreferences.infra_youken': '要員配信 > インフラ > スキル詳細 > 要件定義',
        'contactpersonnelskillpreferences.infra_kihon': '要員配信 > インフラ > スキル詳細 > 基本設計',
        'contactpersonnelskillpreferences.infra_syousai': '要員配信 > インフラ > スキル詳細 > 詳細設計',
        'contactpersonnelskillpreferences.infra_kouchiku': '要員配信 > インフラ > スキル詳細 > 構築',
        'contactpersonnelskillpreferences.infra_test': '要員配信 > インフラ > スキル詳細 > テスト・検証',
        'contactpersonnelskillpreferences.infra_kanshi': '要員配信 > インフラ > スキル詳細 > 監視',
        'contactpersonnelskillpreferences.infra_hosyu': '要員配信 > インフラ > スキル詳細 > 保守・運用',
        'contactpersonnelskillpreferences.infra_beginner': '要員配信 > インフラ > スキル詳細 > 未経験',
        'contactpersonneltypepreferences.other_eigyo': '要員配信 > その他 > 職種詳細 > 営業・事務',
        'contactpersonneltypepreferences.other_kichi': '要員配信 > その他 > 職種詳細 > 基地局',
        'contactpersonneltypepreferences.other_support': '要員配信 > その他 > 職種詳細 > コールセンター・サポートデスク',
        'contactpersonneltypepreferences.other_other': '要員配信 > その他 > 職種詳細 > その他',
        'contactpreference_koyou.job_koyou_proper': '要員配信 > 希望雇用形態 > プロパー',
        'contactpreference_koyou.job_koyou_free': '要員配信 > 希望雇用形態 > フリーランス',
        'contactpreference_personnel_syouryu': '要員配信 > 商流制限',
    }


class ContactFullCsvDownloadView(CsvDownloadHelper, MultiTenantMixin, CsvDownloadView):
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = ContactFilter

    page_size = 100
    queryset = Contact.objects.select_related(
        'organization',
        'staff',
        'contactpreference',
        'contactpreference__created_user',
        'contactpreference__modified_user',
        'created_user',
        'modified_user',
    ).prefetch_related(
        'cc_addresses',
        'categories',
        'tag_assignment__tag',
        'contactjobtypepreferences',
        'contactjobskillpreferences',
        'contactpersonneltypepreferences',
        'contactpersonnelskillpreferences',
        'contactjobtypepreferences__type_preference',
        'contactjobskillpreferences__skill_preference',
        'contactpersonneltypepreferences__type_preference',
        'contactpersonnelskillpreferences__skill_preference',
    ).prefetch_related(
        Prefetch(
            'contactpreference',
            queryset=ContactPreference.objects.filter(contact_id=OuterRef('id'))
        )
    )

    ordering_fields = settings.CONTACT_ORDERING_FIELDS

    def get_filter_params(self, company):
        return self.trade_condition_company(
            company=company,
            capital_man_yen_key='organization__capital_man_yen',
            has_p_mark_or_isms_key='organization__has_p_mark_or_isms',
            has_invoice_system_key='organization__has_invoice_system',
            has_haken_key='organization__has_haken',
            has_distribution_key='organization__has_distribution',
            establishment_date_key='organization__establishment_date'
        )

    def get_is_blacklisted_query(self, qs):
        return qs.exclude(organization__is_blacklisted=True)

    def get_exceptional_organization_q_obj(self, exceptional_organization_ids):
        return Q(organization_id__in=exceptional_organization_ids)

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        renderer = ContactFullCsvRenderer()
        response = StreamingHttpResponse(renderer.render(self._stream_serialized_data(qs)),
                                         content_type='text/csv')
        return response

    def _stream_serialized_data(self, queryset):
        paginator = Paginator(queryset, self.page_size)
        for page in paginator.page_range:
            yield from ContactFullCsvSerializer(paginator.page(page).object_list, many=True, context={'request': self.request}).data


class ContactCsvRenderer(CSVStreamingRenderer):
    header = [  # Define headers to define the order of columns of rendered CSV.
        'last_name', 'first_name', 'related_organization__name', 'email', 'cc', 'tel_full',
        'position', 'department', 'related_staff__email', 'last_visit', 'tag', 'category',
    ]
    labels = {  # Convert field name to display name for CSV.
        'last_name': '取引先担当者名(姓)',
        'first_name': '取引先担当者名(名)',
        'related_organization__name': '所属取引先',
        'email': 'メールアドレス > TO',
        'cc': 'メールアドレス > CC',
        'tel_full': 'TEL',
        'position': '役職',
        'department': '部署',
        'related_staff__email': '自社担当者(メールアドレス)',
        'last_visit': '最終商談日',
        'tag': 'タグ',
        'category': '相性',
    }


class ContactCsvDownloadView(CsvDownloadHelper, MultiTenantMixin, CsvDownloadView):
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = ContactFilter

    page_size = 100
    queryset = Contact.objects.select_related(
        'organization',
        'staff',
        'created_user',
        'modified_user',
    ).prefetch_related(
        'cc_addresses',
        'categories',
        'tag_assignment__tag',
        'contactjobtypepreferences',
        'contactpersonneltypepreferences',
    )

    ordering_fields = settings.CONTACT_ORDERING_FIELDS

    def get_filter_params(self, company):
        return self.trade_condition_company(
            company=company,
            capital_man_yen_key='organization__capital_man_yen',
            has_p_mark_or_isms_key='organization__has_p_mark_or_isms',
            has_invoice_system_key='organization__has_invoice_system',
            has_haken_key='organization__has_haken',
            has_distribution_key='organization__has_distribution',
            establishment_date_key='organization__establishment_date'
        )

    def get_is_blacklisted_query(self, qs):
        return qs.exclude(organization__is_blacklisted=True)

    def get_exceptional_organization_q_obj(self, exceptional_organization_ids):
        return Q(organization_id__in=exceptional_organization_ids)

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        renderer = ContactCsvRenderer()
        response = StreamingHttpResponse(renderer.render(self._stream_serialized_data(qs)),
                                         content_type='text/csv')
        return response

    def _stream_serialized_data(self, queryset):
        paginator = Paginator(queryset, self.page_size)
        for page in paginator.page_range:
            yield from ContactCsvSerializer(paginator.page(page).object_list, many=True, context={'request': self.request}).data


class ContactCsvUploadView(CsvUploadView):
    type_preference_queryset = TypePreference.objects.all()
    skill_preference_queryset = SkillPreference.objects.all()
    COL_NAMES = [
        "取引先担当者名(姓)",
        "取引先担当者名(名)",
        "所属取引先",
        "メールアドレス > TO",
        "メールアドレス > CC",
        "TEL",
        "役職",
        "部署",
        "自社担当者(メールアドレス)",
        "最終商談日",
        "タグ",
        "相性",
        "希望エリア > 北海道",
        "希望エリア > 東北",
        "希望エリア > 関東",
        "希望エリア > 中部",
        "希望エリア > 東海",
        "希望エリア > 関西",
        "希望エリア > 四国",
        "希望エリア > 中国",
        "希望エリア > 九州",
        "希望エリア > その他",
        "案件配信 > 開発 > 職種詳細 > デザイナー",
        "案件配信 > 開発 > 職種詳細 > フロントエンド",
        "案件配信 > 開発 > 職種詳細 > バックエンド",
        "案件配信 > 開発 > 職種詳細 > PM・ディレクター",
        "案件配信 > 開発 > 職種詳細 > その他",
        "案件配信 > 開発 > スキル詳細 > 要件定義",
        "案件配信 > 開発 > スキル詳細 > 基本設計",
        "案件配信 > 開発 > スキル詳細 > 詳細設計",
        "案件配信 > 開発 > スキル詳細 > 製造",
        "案件配信 > 開発 > スキル詳細 > テスト・検証",
        "案件配信 > 開発 > スキル詳細 > 保守・運用",
        "案件配信 > 開発 > スキル詳細 > 未経験",
        "案件配信 > インフラ > 職種詳細 > サーバー",
        "案件配信 > インフラ > 職種詳細 > ネットワーク",
        "案件配信 > インフラ > 職種詳細 > セキュリティ",
        "案件配信 > インフラ > 職種詳細 > データベース",
        "案件配信 > インフラ > 職種詳細 > 情報システム",
        "案件配信 > インフラ > 職種詳細 > その他",
        "案件配信 > インフラ > スキル詳細 > 要件定義",
        "案件配信 > インフラ > スキル詳細 > 基本設計",
        "案件配信 > インフラ > スキル詳細 > 詳細設計",
        "案件配信 > インフラ > スキル詳細 > 構築",
        "案件配信 > インフラ > スキル詳細 > テスト・検証",
        "案件配信 > インフラ > スキル詳細 > 保守・運用",
        "案件配信 > インフラ > スキル詳細 > 監視",
        "案件配信 > インフラ > スキル詳細 > 未経験",
        "案件配信 > その他 > 職種詳細 > 営業・事務",
        "案件配信 > その他 > 職種詳細 > 基地局",
        "案件配信 > その他 > 職種詳細 > コールセンター・サポートデスク",
        "案件配信 > その他 > 職種詳細 > その他",
        "案件配信 > 商流制限",
        "要員配信 > 開発 > 職種詳細 > デザイナー",
        "要員配信 > 開発 > 職種詳細 > フロントエンド",
        "要員配信 > 開発 > 職種詳細 > バックエンド",
        "要員配信 > 開発 > 職種詳細 > PM・ディレクター",
        "要員配信 > 開発 > 職種詳細 > その他",
        "要員配信 > 開発 > スキル詳細 > 要件定義",
        "要員配信 > 開発 > スキル詳細 > 基本設計",
        "要員配信 > 開発 > スキル詳細 > 詳細設計",
        "要員配信 > 開発 > スキル詳細 > 製造",
        "要員配信 > 開発 > スキル詳細 > テスト・検証",
        "要員配信 > 開発 > スキル詳細 > 保守・運用",
        "要員配信 > 開発 > スキル詳細 > 未経験",
        "要員配信 > インフラ > 職種詳細 > サーバー",
        "要員配信 > インフラ > 職種詳細 > ネットワーク",
        "要員配信 > インフラ > 職種詳細 > セキュリティ",
        "要員配信 > インフラ > 職種詳細 > データベース",
        "要員配信 > インフラ > 職種詳細 > 情報システム",
        "要員配信 > インフラ > 職種詳細 > その他",
        "要員配信 > インフラ > スキル詳細 > 要件定義",
        "要員配信 > インフラ > スキル詳細 > 基本設計",
        "要員配信 > インフラ > スキル詳細 > 詳細設計",
        "要員配信 > インフラ > スキル詳細 > 構築",
        "要員配信 > インフラ > スキル詳細 > テスト・検証",
        "要員配信 > インフラ > スキル詳細 > 保守・運用",
        "要員配信 > インフラ > スキル詳細 > 監視",
        "要員配信 > インフラ > スキル詳細 > 未経験",
        "要員配信 > その他 > 職種詳細 > 営業・事務",
        "要員配信 > その他 > 職種詳細 > 基地局",
        "要員配信 > その他 > 職種詳細 > コールセンター・サポートデスク",
        "要員配信 > その他 > 職種詳細 > その他",
        "要員配信 > 希望雇用形態 > プロパー",
        "要員配信 > 希望雇用形態 > フリーランス",
        "要員配信 > 商流制限",
    ]

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        register_successed = False

        f = request.FILES['file']
        data = TextIOWrapper(f.file, encoding='shift_jis')
        raw_csv = csv.reader(data)
        next(raw_csv) # ヘッダ行を飛ばす処理

        csv_datas = list(raw_csv)
        if len(csv_datas) > settings.CSV_REGISTERABLE_LIMIT:
            raise CsvRegisterLimitException(
                settings.CSV_REGISTER_LIMIT_ERROR_MESSAGE.format(settings.CSV_REGISTERABLE_LIMIT)
            )

        organization_names = []
        contact_email_hash = {}
        tag_values = []
        emails_for_search = []
        errorMessages = []
        for index, line in enumerate(csv_datas):
            if not len(line) == settings.CSV_CONTACT_COLUMN_LENGTH:
                continue

            if line[2]:
                organization_names.append(line[2])

            if line[3]:
                emails_for_search.append(line[3])
                contact_email_hash[line[3]] = line

            for tag_value in line[10].split(','):
                if tag_value != '': tag_values.append(tag_value)

        registered_emails = Contact.objects.filter(company=request.user.company, email__in=emails_for_search).values_list('email', flat=True)
        emails_in_csv = []
        organizations_name_hash = self._organizations_name_hash(request.user, organization_names) # CSV側のキー(取引先名)で探せるよう、取引先名をキーにしたハッシュにする
        staffs_email_hash = self._staffs_email_hash(request.user) # CSV側のキー(取引先担当者)で探せるよう、取引先担当者をキーにしたハッシュにする
        required_items = self._get_required_items('contacts')
        contacts = []
        for index, line in enumerate(csv_datas):
            if not len(line) == settings.CSV_CONTACT_COLUMN_LENGTH:
                errorMessages.append(self._get_column_length_error_message(index))
                continue

            organization = organizations_name_hash.get(line[2])
            staff = staffs_email_hash.get(line[8].replace(' ',''))

            email_in_csv = line[3]
            if email_in_csv in emails_in_csv:
                errorMessages.append(self._get_duplicate_error_message(index, 'メールアドレス > TO', email_in_csv))
            elif email_in_csv in registered_emails:
                errorMessages.append(self._get_already_registered_error_message(index, 'メールアドレス > TO', email_in_csv))
            else:
                emails_in_csv.append(email_in_csv)

            # 各項目のエラー確認
            self._get_errors(line, organization, staff, required_items, index, errorMessages)

            # preference のエラー確認
            self._check_preference_validation_error_messages(index, line, errorMessages)

            contact = self._contact(line, organization, staff, request.user)
            contacts.append(contact)

        # 入力エラーが存在しない場合のみ登録処理を実行する
        created_contacts = None
        if len(errorMessages) == 0 and len(contacts) > 0:
            created_contacts = Contact.objects.bulk_create(contacts)

            # 存在しないタグを先に作成しておく
            tags_value_hash = self._get_or_create_tags(tag_values, request.user)

            # 取引先担当者付属情報を登録する
            self._register_contact_additional_data(created_contacts, contact_email_hash, tags_value_hash, request.user)
            register_successed = True

        return Response(status=status.HTTP_201_CREATED, data={ 'register_successed': register_successed, 'errorMessages': errorMessages })

    def _organizations_name_hash(self, user, organization_names):
        organizations_name_hash = {}
        organizations = Organization.objects.filter(company=user.company, name__in=organization_names)
        for organization in organizations:
            organizations_name_hash[organization.name] = organization

        return organizations_name_hash

    def _staffs_email_hash(self, user):
        staffs_email_hash = {}
        staffs = User.objects.filter(company=user.company)
        for staff in staffs:
            staffs_email_hash[staff.email] = staff

        return staffs_email_hash

    def _get_error_name(self, index, last_name, first_name, errorMessages, max_length):
        if last_name == '':
            errorMessages.append(self._get_required_error_message(index, "取引先担当者名(姓)"))
        else:
            if str(last_name).find(' ') != -1 or str(last_name).find('　') != -1:
                errorMessages.append(self._get_cannot_input_space_error_message(index, "取引先担当者名(姓)", last_name))

        if str(first_name).find(' ') != -1 or str(first_name).find('　') != -1:
            errorMessages.append(self._get_cannot_input_space_error_message(index, "取引先担当者名(名)", first_name))

        name = last_name + first_name
        if len(name) > max_length:
            errorMessages.append(self._get_input_format_error_message(index, "取引先担当者名", name, f'姓名合わせて{max_length}文字以内で'))

    def _get_error_organization(self, index, value, organization, errorMessages):
        if value == '':
            errorMessages.append(self._get_required_error_message(index, "所属取引先"))
        else:
            if organization is None:
                errorMessages.append(self._get_unregistered_organization_error_message(index, value))

    def _get_error_mail_to(self, index, value, errorMessages, max_length):
        if value == '':
            errorMessages.append(self._get_required_error_message(index, 'メールアドレス > TO'))
        else:
            if not re.compile(r'[\w\d_-]+@[\w\d_-]+\.[\w\d._-]+').search(value):
                errorMessages.append(self._get_illegal_email_address_error_message(index, 'メールアドレス > TO', value))

            if len(value) > max_length:
                errorMessages.append(self._get_input_format_error_message(index, "メールアドレス > TO", value, f'{max_length}文字以内で'))

    def _get_error_mail_cc(self, index, value, errorMessages, max_length, required_items):
        if value == '':
            if 'email_cc' in required_items:
                errorMessages.append(self._get_required_error_message(index, 'メールアドレス > CC'))
        else:
            for m in value.replace(' ','').split(','):
                if not re.compile(r'[\w\d_-]+@[\w\d_-]+\.[\w\d._-]+').search(m):
                    errorMessages.append(self._get_illegal_email_address_error_message(index, 'メールアドレス > CC', m))

            CC_MAILS_LIMIT = 5
            if len(value.split(',')) > CC_MAILS_LIMIT:
                errorMessages.append(self._get_over_registration_limit_error_message(index, 'メールアドレス > CC', CC_MAILS_LIMIT))

            if len(value) > max_length:
                errorMessages.append(self._get_email_cc_over_length_error_message(index))

    def _get_error_tel(self, index, value, errorMessages, max_length, required_items):
        if value == '':
            if 'tel' in required_items:
                errorMessages.append(self._get_required_error_message(index, 'TEL'))
        else:
            if not re.compile(r'(^[0-9]+)-([0-9]+)-([0-9]+$)').search(value):
                errorMessages.append(self._get_hyphen_format_error_message(index, 'TEL', value))

            if not re.compile(r'^[0-9]+$').search(value.replace('-', '').replace('ー', '')):
                errorMessages.append(self._get_input_format_error_message(index, 'TEL', value, '半角数字で'))

            if len(value) > max_length + 2: # ハイフンの２文字を追加
                errorMessages.append(self._get_input_format_error_message(index, 'TEL', value, f'半角ハイフンを含めて{max_length+2}文字以内で'))

    def _get_error_position(self, index, value, errorMessages, max_length, required_items):
        if value == '':
            if 'position' in required_items:
                errorMessages.append(self._get_required_error_message(index, '役職'))
        else:
            if len(value) > max_length:
                errorMessages.append(self._get_input_format_error_message(index, '役職', value, f'{max_length}文字以内で'))

    def _get_error_department(self, index, value, errorMessages, max_length, required_items):
        if value == '':
            if 'department' in required_items:
                errorMessages.append(self._get_required_error_message(index, '部署'))
        else:
            if len(value) > max_length:
                errorMessages.append(self._get_input_format_error_message(index, '部署', value, f'{max_length}文字以内で'))

    def _get_error_staff(self, index, value, staff, errorMessages, required_items):
        if value == '':
            if 'staff' in required_items:
                errorMessages.append(self._get_required_error_message(index, '自社担当(メールアドレス)'))
        elif staff is None:
            errorMessages.append(self._get_unregistered_staff_error_message(index, value))

    def _get_error_last_visit(self, index, value, errorMessages, required_items):
        if value == '':
            if 'last_visit' in required_items:
                errorMessages.append(self._get_required_error_message(index, '最終商談日'))
        else:
            year_and_month_day = re.compile(r'(^(?P<year>[0-9]{4})[-/](?P<month>[0-9]{2})[-/](?P<day>[0-9]{2})$)').search(value)
            if year_and_month_day:
                last_visit_date = datetime.datetime(
                    year=int(year_and_month_day['year']),
                    month=int(year_and_month_day['month']),
                    day=int(year_and_month_day['day'])
                )
                if last_visit_date > datetime.datetime.now():
                    errorMessages.append(self._get_future_date_error_message(index, '最終商談日', value))
            else:
                errorMessages.append(self._get_input_format_error_message(index, '最終商談日', value, "yyyy-mm-ddの形式で"))

    def _get_error_tags(self, index, value, errorMessages, max_length, required_items):
        if value == '':
            if 'tag' in required_items:
                errorMessages.append(self._get_required_error_message(index, 'タグ'))
        else:
            TAG_LIMIT = 10
            if len(value.split(',')) > TAG_LIMIT:
                errorMessages.append(self._get_over_registration_limit_error_message(index, 'タグ', TAG_LIMIT))

            for v in value.split(','):
                if len(v) > max_length:
                    errorMessages.append(self._get_input_format_error_message(index, 'タグ', v, f'タグ名はそれぞれ{max_length}文字以内で'))

    def _get_error_category(self, index, value, errorMessages, required_items):
        enum = ['良い', '悪い']
        if value == '':
            if 'category' in required_items:
                errorMessages.append(self._get_required_error_message(index, '相性'))
        elif value not in enum:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '相性', value, enum))

    def _get_error_job_syouryu(self, index, value, errorMessages):
        enum = ['なし', 'エンド直・元請直まで', '1次請まで', '2次請まで']
        if value != '' and value not in enum:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '案件配信 > 商流制限', value, enum))

    def _get_error_personnel_syouryu(self, index, value, errorMessages):
        enum = ['なし', '自社所属まで', '1社先所属まで', '2社先所属まで']
        if value != '' and value not in enum:
            errorMessages.append(self._get_unkwon_enum_item_error_message(index, '要員配信 > 商流制限', value, enum))

    def _get_errors(self, line, organization, staff, required_items, index, errorMessages):
        max_length_dic = settings.LENGTH_VALIDATIONS['contact']
        # 取引先担当者名
        self._get_error_name(index, line[0], line[1], errorMessages, max_length_dic['name']['max'])

        # 所属取引先
        self._get_error_organization(index, line[2], organization, errorMessages)

        # メールアドレス > TO
        self._get_error_mail_to(index, line[3], errorMessages, max_length_dic['email']['max'])

        # メールアドレス > CC
        self._get_error_mail_cc(index, line[4], errorMessages, max_length_dic['cc_mails']['max'], required_items)

        # TEL
        self._get_error_tel(index, line[5], errorMessages, max_length_dic['tel']['max'], required_items)

        # 役職
        self._get_error_position(index, line[6], errorMessages, max_length_dic['position']['max'], required_items)

        # 部署
        self._get_error_department(index, line[7], errorMessages, max_length_dic['department']['max'], required_items)

        # 自社担当者(メールアドレス)
        self._get_error_staff(index, line[8], staff, errorMessages, required_items)

        # 最終商談日
        self._get_error_last_visit(index, line[9], errorMessages, required_items)

        # タグ

        self._get_error_tags(index, line[10], errorMessages, settings.LENGTH_VALIDATIONS['tag']['value']['max'], required_items)

        # 相性
        self._get_error_category(index, line[11], errorMessages, required_items)

        #  希望エリア、案件配信
        for num in range(12, 52):
            if line[num] not in {'1', '0', ''}: errorMessages.append(self._get_boolean_item_error_message(index, self.COL_NAMES[num], line[num]))

        #  案件配信 > 商流制限
        self._get_error_job_syouryu(index, line[52], errorMessages)

        # 要員配信
        for num in range(53, 85):
            if line[num] not in {'1', '0', ''}: errorMessages.append(self._get_boolean_item_error_message(index, self.COL_NAMES[num], line[num]))

        # 要員配信 > 商流制限
        self._get_error_personnel_syouryu(index, line[85], errorMessages)

    def _check_preference_validation_error_messages(self, index, line, error_messages):
        is_location_selected = '1' in [line[num] for num in range(12, 21)]

        is_jobtype_dev_selected = '1' in [line[num] for num in range(22, 27)]
        is_jobtype_infra_selected = '1' in [line[num] for num in range(34, 40)]
        is_jobtype_other_selected = '1' in [line[num] for num in range(48, 52)]
        is_jobskill_dev_selected = '1' in [line[num] for num in range(27, 34)]
        is_jobskill_infra_selected = '1' in [line[num] for num in range(40, 48)]
        is_jobtype_selected = is_jobtype_dev_selected or is_jobtype_infra_selected or is_jobtype_other_selected
        is_jobskill_selected = is_jobskill_dev_selected or is_jobskill_infra_selected
        is_job_selected = is_jobtype_selected or is_jobskill_selected
        is_job_syouryu_selected = line[52] != ''

        is_personneltype_dev_selected = '1' in [line[num] for num in range(53, 58)]
        is_personneltype_infra_selected = '1' in [line[num] for num in range(65, 71)]
        is_personneltype_other_selected = '1' in [line[num] for num in range(79, 83)]
        is_personnelskill_dev_selected = '1' in [line[num] for num in range(58, 65)]
        is_personnelskill_infra_selected = '1' in [line[num] for num in range(71, 79)]
        is_personneltype_selected = is_personneltype_dev_selected or is_personneltype_infra_selected or is_personneltype_other_selected
        is_personnelskill_selected = is_personnelskill_dev_selected or is_personnelskill_infra_selected
        is_personnel_selected = is_personneltype_selected or is_personnelskill_selected
        is_personnel_syouryu_selected = line[85] != ''
        is_koyou_proper_selected = line[83] == '1'
        is_koyou_free_selected = line[84] == '1'
        is_koyou_selected = is_koyou_proper_selected or is_koyou_free_selected

        # 案件開発の職種詳細とスキル詳細の、どちらかにしか1が入っていない場合のチェック
        job_dev_type_skill_pair_error = (is_jobtype_dev_selected and not is_jobskill_dev_selected) or (is_jobskill_dev_selected and not is_jobtype_dev_selected)
        if job_dev_type_skill_pair_error:
            error_messages.append(f'{index+1}行目: 案件配信 > 開発の職種詳細とスキル詳細は必ず1つ選択してください')

        # 案件インフラの職種詳細とスキル詳細の、どちらかにしか1が入っていない場合のチェック
        job_infra_type_skill_pair_error = (is_jobtype_infra_selected and not is_jobskill_infra_selected) or (is_jobskill_infra_selected and not is_jobtype_infra_selected)
        if job_infra_type_skill_pair_error:
            error_messages.append(f'{index+1}行目: 案件配信 > インフラの職種詳細とスキル詳細は必ず1つ選択してください')

        # 要員開発の職種詳細とスキル詳細の、どちらかにしか1が入っていない場合のチェック
        personnel_dev_type_skill_pair_error = (is_personneltype_dev_selected and not is_personnelskill_dev_selected) or (is_personnelskill_dev_selected and not is_personneltype_dev_selected)
        if personnel_dev_type_skill_pair_error:
            error_messages.append(f'{index+1}行目: 要員配信 > 開発の職種詳細とスキル詳細は必ず1つ選択してください')

        # 要員インフラの職種詳細とスキル詳細の、どちらかにしか1が入っていない場合のチェック
        personnel_infra_type_skill_pair_error = (is_personneltype_infra_selected and not is_personnelskill_infra_selected) or (is_personnelskill_infra_selected and not is_personneltype_infra_selected)
        if personnel_infra_type_skill_pair_error:
            error_messages.append(f'{index+1}行目: 要員配信 > インフラの職種詳細とスキル詳細は必ず1つ選択してください')

        # 配信条件のいずれかに1があるが、希望エリアに値がない場合のチェック
        location_error = (is_job_selected or is_personnel_selected) and not is_location_selected
        if location_error:
            error_messages.append(f'{index+1}行目: 希望エリアは必ず1つ選択してください')

        # 案件の配信条件のいずれかに1があるが、案件の商流制限に値がない場合のチェック
        job_syouryu_error = is_job_selected and not is_job_syouryu_selected
        if job_syouryu_error:
            error_messages.append(f'{index+1}行目: 案件配信 > 商流制限は必ず1つ選択してください')

        # 要員の配信条件のいずれかに1があるが、要員の商流制限に値がない場合のチェック
        personnel_syouryu_error = is_personnel_selected and not is_personnel_syouryu_selected
        if personnel_syouryu_error:
            error_messages.append(f'{index+1}行目: 要員配信 > 商流制限は必ず1つ選択してください')

        # 要員の配信条件のいずれかに1があるが、希望雇用形態に値がない場合のチェック
        koyou_error = is_personnel_selected and not is_koyou_selected
        if koyou_error:
            error_messages.append(f'{index+1}行目: 要員配信 > 希望雇用形態は必ず1つ選択してください')

    def _contact(self, line, organization, staff, user):
        last_name = line[0]
        first_name = line[1]
        email = line[3]
        if re.compile(r'(^\d*)-(\d*)-(\d*$)').search(line[5]):
            tel1 = line[5].split("-")[0]
            tel2 = line[5].split("-")[1]
            tel3 = line[5].split("-")[2]
        else:
            tel1 = None
            tel2 = None
            tel3 = None
        position = line[6]
        department = line[7]
        last_visit = None if line[9] == '' else line[9].replace('/', '-')

        return Contact(
            last_name=last_name,
            first_name=first_name,
            email=email,
            organization=organization,
            tel1=tel1,
            tel2=tel2,
            tel3=tel3,
            position=position,
            department=department,
            staff=staff,
            last_visit=last_visit,
            company=user.company,
            created_user=user,
            modified_user=user,
        )

    def _get_or_create_tags(self, tag_values, user):
        tag_internal_values = [value.lower() for value in tag_values if value != '']
        tags = Tag.objects.filter(company=user.company, internal_value__in=tag_internal_values)
        tags_value_hash = {}
        for tag in tags:
            tags_value_hash[tag.internal_value] = tag

        not_regsiterd_tag_values = []
        for tag_value in tag_values:
            if tags_value_hash.get(tag_value.lower()) == None and not tag_value in not_regsiterd_tag_values:
                not_regsiterd_tag_values.append(tag_value)

        if len(not_regsiterd_tag_values) > 0:
            tags_for_register = []
            for not_regsiterd_tag_value in not_regsiterd_tag_values:
                new_tag = Tag(
                    value=not_regsiterd_tag_value,
                    internal_value=not_regsiterd_tag_value.lower(),
                    company=user.company,
                    created_user=user,
                    modified_user=user,
                )
                tags_for_register.append(new_tag)

            created_tags = Tag.objects.bulk_create(tags_for_register)
            for created_tag in created_tags:
                tags_value_hash[created_tag.internal_value] = created_tag

        return tags_value_hash

    def _register_contact_additional_data(self, created_contacts, contact_email_hash, tags_value_hash, user):
        contactccs = []
        contact_categories = []
        contact_tags = []
        contact_preferences = []
        contact_jobtypepreferences = []
        contact_jobskillpreferences = []
        contact_personneltypepreferences = []
        contact_personnelskillpreferences = []
        if created_contacts != None:
            for contact in created_contacts:
                contact_csv_data = contact_email_hash.get(contact.email)

                # CC
                cc_mails = [mail for mail in contact_csv_data[4].replace(' ','').split(',') if mail != '']
                for ccmail in cc_mails:
                    contactccs.append(self._contact_cc(ccmail, contact, user))

                # 相性
                categories = [category for category in contact_csv_data[11].replace(' ','').split(',') if category != '']
                for category in categories:
                    contact_category = self._contact_category(category, contact, user)
                    if contact_category:
                        contact_categories.append(contact_category)

                # タグ
                tags = [tag for tag in contact_csv_data[10].replace(' ','').split(',') if tag != '']
                for tag in tags:
                    contact_tags.append(self._contact_tag(tags_value_hash[tag.lower()], contact, user))

                # メール配信設定
                contact_preferences.append(self._contact_preference(contact, contact_csv_data, user))

                # 案件職種詳細設定
                for contact_jobtypepreference in self._contact_jobtypepreference(contact, contact_csv_data, user):
                    contact_jobtypepreferences.append(contact_jobtypepreference)

                # 案件スキル詳細設定
                for contact_jobskillpreference in self._contact_jobskillpreference(contact, contact_csv_data, user):
                    contact_jobskillpreferences.append(contact_jobskillpreference)

                # 要員職種詳細設定
                for contact_personneltypepreference in self._contact_personneltypepreference(contact, contact_csv_data, user):
                    contact_personneltypepreferences.append(contact_personneltypepreference)

                # 要員スキル詳細設定
                for contact_personnelskillpreference in self._contact_personnelskillpreference(contact, contact_csv_data, user):
                    contact_personnelskillpreferences.append(contact_personnelskillpreference)

        if len(contactccs) > 0: ContactCC.objects.bulk_create(contactccs)
        if len(contact_categories) > 0: ContactCategory.objects.bulk_create(contact_categories)
        if len(contact_tags) > 0: ContactTagAssignment.objects.bulk_create(contact_tags)
        if len(contact_preferences) > 0: ContactPreference.objects.bulk_create(contact_preferences)
        if len(contact_jobtypepreferences) > 0: ContactJobTypePreference.objects.bulk_create(contact_jobtypepreferences)
        if len(contact_jobskillpreferences) > 0: ContactJobSkillPreference.objects.bulk_create(contact_jobskillpreferences)
        if len(contact_personneltypepreferences) > 0: ContactPersonnelTypePreference.objects.bulk_create(contact_personneltypepreferences)
        if len(contact_personnelskillpreferences) > 0: ContactPersonnelSkillPreference.objects.bulk_create(contact_personnelskillpreferences)

    def _contact_cc(self, ccmail, contact, user):
        return ContactCC(
            email=ccmail,
            contact=contact,
            company=user.company,
            created_user=user,
            modified_user=user,
        )

    def _contact_category(self, category, contact, user):
        category_key = None
        if category == '良い':
            category_key = 'heart'
        elif category == '悪い':
            category_key = 'frown'

        if category_key and contact.staff:
            return ContactCategory(
                category=category_key,
                user=contact.staff,
                contact=contact,
                company=user.company,
                created_user=user,
                modified_user=user,
            )
        else:
            return None

    def _contact_tag(self, tag, contact, user):
        return ContactTagAssignment(
            tag=tag,
            contact=contact,
            company=user.company,
            created_user=user,
            modified_user=user,
        )

    def _contact_preference(self, contact, contact_csv_data, user):
        wants_location_hokkaido_japan = True if contact_csv_data[12] == '1' else False
        wants_location_touhoku_japan = True if contact_csv_data[13] == '1' else False
        wants_location_kanto_japan = True if contact_csv_data[14] == '1' else False
        wants_location_chubu_japan = True if contact_csv_data[15] == '1' else False
        wants_location_toukai_japan = True if contact_csv_data[16] == '1' else False
        wants_location_kansai_japan = True if contact_csv_data[17] == '1' else False
        wants_location_shikoku_japan = True if contact_csv_data[18] == '1' else False
        wants_location_chugoku_japan = True if contact_csv_data[19] == '1' else False
        wants_location_kyushu_japan = True if contact_csv_data[20] == '1' else False
        wants_location_other_japan = True if contact_csv_data[21] == '1' else False

        job_syouryu = None
        if contact_csv_data[52] == 'なし':
            job_syouryu = 1
        elif contact_csv_data[52] == '2次請まで':
            job_syouryu = 2
        elif contact_csv_data[52] == '1次請まで':
            job_syouryu = 3
        elif contact_csv_data[52] == 'エンド直・元請直まで':
            job_syouryu = 4

        job_koyou_proper = True if contact_csv_data[83] == '1' else False

        job_koyou_free = True if contact_csv_data[84] == '1' else False

        personnel_syouryu = None
        if contact_csv_data[85] == 'なし':
            personnel_syouryu = 1
        elif contact_csv_data[85] == '2社先所属まで':
            personnel_syouryu = 2
        elif contact_csv_data[85] == '1社先所属まで':
            personnel_syouryu = 3
        elif contact_csv_data[85] == '自社所属まで':
            personnel_syouryu = 4

        return ContactPreference(
            contact=contact,
            wants_location_hokkaido_japan=wants_location_hokkaido_japan,
            wants_location_touhoku_japan=wants_location_touhoku_japan,
            wants_location_kanto_japan=wants_location_kanto_japan,
            wants_location_chubu_japan=wants_location_chubu_japan,
            wants_location_toukai_japan=wants_location_toukai_japan,
            wants_location_kansai_japan=wants_location_kansai_japan,
            wants_location_shikoku_japan=wants_location_shikoku_japan,
            wants_location_chugoku_japan=wants_location_chugoku_japan,
            wants_location_kyushu_japan=wants_location_kyushu_japan,
            wants_location_other_japan=wants_location_other_japan,
            job_syouryu=job_syouryu,
            personnel_syouryu=personnel_syouryu,
            job_koyou_proper=job_koyou_proper,
            job_koyou_free=job_koyou_free,
            company=user.company,
            created_user=user,
            modified_user=user
        )

    def _contact_jobtypepreference(self, contact, contact_csv_data, user):
        # 22 ~ 26, 34 ~ 39, 48 ~ 51
        type_and_index_dict = {
            'dev_designer': 22,
            'dev_front': 23,
            'dev_server': 24,
            'dev_pm': 25,
            'dev_other': 26,
            'infra_server': 34,
            'infra_network': 35,
            'infra_security': 36,
            'infra_database': 37,
            'infra_sys': 38,
            'infra_other': 39,
            'other_eigyo': 48,
            'other_kichi': 49,
            'other_support': 50,
            'other_other': 51,
        }
        preferences = []
        for type_preference in self.type_preference_queryset:
            type_and_index = type_and_index_dict[type_preference.name]
            if contact_csv_data[type_and_index] == '1':
                preferences.append(
                    ContactJobTypePreference(
                        contact=contact,
                        type_preference=type_preference,
                        company=contact.company,
                        created_user=user,
                        modified_user=user,
                    )
                )
        return preferences

    def _contact_jobskillpreference(self, contact, contact_csv_data, user):
        # 27 ~ 33, 40 ~ 47
        skill_and_index_dict = {
            'dev_youken': 27,
            'dev_kihon': 28,
            'dev_syousai': 29,
            'dev_seizou': 30,
            'dev_test': 31,
            'dev_hosyu': 32,
            'dev_beginner': 33,
            'infra_youken': 40,
            'infra_kihon': 41,
            'infra_syousai': 42,
            'infra_kouchiku': 43,
            'infra_test': 44,
            'infra_hosyu': 45,
            'infra_kanshi': 46,
            'infra_beginner': 47,
        }
        preferences = []
        for skill_preference in self.skill_preference_queryset:
            skill_and_index = skill_and_index_dict[skill_preference.name]
            if contact_csv_data[skill_and_index] == '1':
                preferences.append(
                    ContactJobSkillPreference(
                        contact=contact,
                        skill_preference=skill_preference,
                        company=contact.company,
                        created_user=user,
                        modified_user=user,
                    )
                )
        return preferences

    def _contact_personneltypepreference(self, contact, contact_csv_data, user):
        # 53 ~ 57, 65 ~ 70, 79 ~ 82
        type_and_index_dict = {
            'dev_designer': 53,
            'dev_front': 54,
            'dev_server': 55,
            'dev_pm': 56,
            'dev_other': 57,
            'infra_server': 65,
            'infra_network': 66,
            'infra_security': 67,
            'infra_database': 68,
            'infra_sys': 69,
            'infra_other': 70,
            'other_eigyo': 79,
            'other_kichi': 80,
            'other_support': 81,
            'other_other': 82,
        }
        preferences = []
        for type_preference in self.type_preference_queryset:
            type_and_index = type_and_index_dict[type_preference.name]
            if contact_csv_data[type_and_index] == '1':
                preferences.append(
                    ContactPersonnelTypePreference(
                        contact=contact,
                        type_preference=type_preference,
                        company=contact.company,
                        created_user=user,
                        modified_user=user,
                    )
                )
        return preferences

    def _contact_personnelskillpreference(self, contact, contact_csv_data, user):
        # 58 ~ 64, 71 ~ 78
        skill_and_index_dict = {
            'dev_youken': 58,
            'dev_kihon': 59,
            'dev_syousai': 60,
            'dev_seizou': 61,
            'dev_test': 62,
            'dev_hosyu': 63,
            'dev_beginner': 64,
            'infra_youken': 71,
            'infra_kihon': 72,
            'infra_syousai': 73,
            'infra_kouchiku': 74,
            'infra_test': 75,
            'infra_hosyu': 76,
            'infra_kanshi': 77,
            'infra_beginner': 78,
        }
        preferences = []
        for skill_preference in self.skill_preference_queryset:
            skill_and_index = skill_and_index_dict[skill_preference.name]
            if contact_csv_data[skill_and_index] == '1':
                preferences.append(
                    ContactPersonnelSkillPreference(
                        contact=contact,
                        skill_preference=skill_preference,
                        company=contact.company,
                        created_user=user,
                        modified_user=user,
                    )
                )
        return preferences


class ContactPreferenceListView(MultiTenantMixin, SummaryForListMixin, GenericListAPIView):
    serializer_class = ContactPreferenceSerializer
    serializer_class_for_list = ContactPreferenceSummarySerializer
    queryset = ContactPreference.objects.all().select_related('contact__organization')

    pagination_class = StandardPagenation
    filterset_class = ContactPreferenceFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('contact__created_time',)
    ordering = ('-contact__created_time',)

    def get_queryset(self):
        company = self.request.user.company

        subqry_with_request_user = Subquery(
            ContactScore.objects.filter(
                user=self.request.user,
                contact_id=OuterRef('contact_id')
            ).values_list('id', flat=True).order_by('-created_time')
        )

        q = super().get_queryset().prefetch_related(
            Prefetch(
                'contact__scores',
                queryset=ContactScore.objects.filter(id__in=subqry_with_request_user)
            )
        )

        location_items = [
            'wants_location_hokkaido_japan', 'wants_location_touhoku_japan', 'wants_location_kanto_japan', 'wants_location_kansai_japan',
            'wants_location_chubu_japan', 'wants_location_kyushu_japan', 'wants_location_other_japan',
            'wants_location_chugoku_japan', 'wants_location_shikoku_japan', 'wants_location_toukai_japan'
        ]
        location_query_params = []
        for location in location_items:
            if self.request.GET.get(location):
                location_query_params.append({f'{location}': True})
        if location_query_params:
            q = generate_or_query(q, location_query_params)

        category_items = ['prospective', 'approached', 'exchanged', 'client']
        organization_category_query_params = []
        for category in category_items:
            if self.request.GET.get(f'contact__organization__category_{category}'):
                organization_category_query_params.append({'contact__organization__organization_category__name': category})
        if organization_category_query_params:
            q = generate_or_query(q, organization_category_query_params)

        country_items = ['jp', 'kr', 'cn', 'other']
        organization_country_query_params = []
        for country in country_items:
            if self.request.GET.get(f'contact__organization__organization_country_{country}'):
                organization_country_query_params.append({'contact__organization__organization_country__name': country.upper()})
        if organization_country_query_params:
            q = generate_or_query(q, organization_country_query_params)


        type_dev_items = ['dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other']
        skill_dev_items = ['dev_youken', 'dev_kihon', 'dev_syousai', 'dev_seizou', 'dev_test', 'dev_hosyu', 'dev_beginner']
        type_infra_items = ['infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other']
        skill_infra_items = ['infra_youken', 'infra_kihon', 'infra_syousai', 'infra_kouchiku', 'infra_test', 'infra_kanshi', 'infra_hosyu', 'infra_beginner']
        type_other_items = ['other_eigyo', 'other_kichi', 'other_support', 'other_other']

        # 検索種別はラジオボタンなので、どれか一つが選択される
        if self.request.GET.get("searchtype") == 'job':
            # 配信職種(案件)はラジオボタンなので、どれか一つが選択される
            if self.request.GET.get("jobtype") == 'dev':
                # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
                for item in type_dev_items:
                    if self.request.GET.get(f'jobtype_{item}'):
                        q = q.filter(contact__contactjobtypepreferences__type_preference__name=item)

                # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
                jobskill_dev_search_params = []
                for item in skill_dev_items:
                    if self.request.GET.get(f'jobskill_{item}'):
                        jobskill_dev_search_params.append(item)
                if jobskill_dev_search_params:
                    q = q.filter(contact__contactjobskillpreferences__skill_preference__name__in=jobskill_dev_search_params)
            elif self.request.GET.get("jobtype") == 'infra':
                # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
                for item in type_infra_items:
                    if self.request.GET.get(f'jobtype_{item}'):
                        q = q.filter(contact__contactjobtypepreferences__type_preference__name=item)

                # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
                jobskill_infra_search_params = []
                for item in skill_infra_items:
                    if self.request.GET.get(f'jobskill_{item}'):
                        jobskill_infra_search_params.append(item)
                if jobskill_infra_search_params:
                    q = q.filter(contact__contactjobskillpreferences__skill_preference__name__in=jobskill_infra_search_params)
            elif self.request.GET.get("jobtype") == 'other':
                # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
                for item in type_other_items:
                    if self.request.GET.get(f'jobtype_{item}'):
                        q = q.filter(contact__contactjobtypepreferences__type_preference__name=item)
        elif self.request.GET.get("searchtype") == 'personnel':
            # 配信職種(要員)はチェックボックスなので複数選択される(検索はOR検索)
            personnel_search_params = []
            if self.request.GET.get("personneltype_dev"):
                # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
                personneltype_dev_search_query = []
                personneltype_dev_search_params = []
                for item in type_dev_items:
                    if self.request.GET.get(f'personneltype_{item}'):
                        personneltype_dev_search_params.append(item)
                if personneltype_dev_search_params:
                    personneltype_dev_search_query.append(
                        Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_dev_search_params)
                    )
                # 配信スキル詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
                personnelskill_dev_search_params = []
                for item in skill_dev_items:
                    if self.request.GET.get(f'personnelskill_{item}'):
                        personnelskill_dev_search_params.append(item)
                if personnelskill_dev_search_params:
                    personneltype_dev_search_query.append(
                        Q(contact__contactpersonnelskillpreferences__skill_preference__name__in=personnelskill_dev_search_params)
                    )
                personnel_search_params.append(
                    # 職種詳細とスキル詳細はANDでつなげる
                    Q(*personneltype_dev_search_query)
                )
            if self.request.GET.get("personneltype_infra"):
                # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
                personneltype_infra_search_query = []
                personneltype_infra_search_params = []
                for item in type_infra_items:
                    if self.request.GET.get(f'personneltype_{item}'):
                        personneltype_infra_search_params.append(item)
                if personneltype_infra_search_params:
                    personneltype_infra_search_query.append(
                        Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_infra_search_params)
                    )

                # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
                personnelskill_infra_search_params = []
                for item in skill_infra_items:
                    if self.request.GET.get(f'personnelskill_{item}'):
                        personnelskill_infra_search_params.append(item)
                if personnelskill_infra_search_params:
                    personneltype_infra_search_query.append(
                        Q(contact__contactpersonnelskillpreferences__skill_preference__name__in=personnelskill_infra_search_params)
                    )
                personnel_search_params.append(
                    # 職種詳細とスキル詳細はANDでつなげる
                    Q(*personneltype_infra_search_query)
                )
            if self.request.GET.get("personneltype_other"):
                # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
                personneltype_other_search_query = []
                personneltype_other_search_params = []
                for item in type_other_items:
                    if self.request.GET.get(f'personneltype_{item}'):
                        personneltype_other_search_params.append(item)
                if personneltype_other_search_params:
                    personneltype_other_search_query.append(
                        Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_other_search_params)
                    )
                personnel_search_params.append(
                    # 職種詳細とスキル詳細はANDでつなげる
                    Q(*personneltype_other_search_query)
                )
            if personnel_search_params:
                personnel_search_query = personnel_search_params.pop()
                for item in personnel_search_params:
                    personnel_search_query |= item
                q = q.filter(personnel_search_query)
        elif self.request.GET.get("searchtype") == 'other':
            q = q.filter(has_send_guide=True)

        # https://trello.com/c/Dh3VKd8Q/172-%E8%87%AA%E7%A4%BE%E5%8F%96%E5%BC%95%E6%9D%A1%E4%BB%B6%E3%81%AEp%E3%83%9E%E3%83%BC%E3%82%AF%E3%81%8C%E5%8A%B9%E3%81%84%E3%81%A6%E3%81%AA%E3%81%84%E3%80%82#comment-60330b639aaf893aade8800f
        # 上記理由により、人だしの資本金上限値によりバリデーションは一旦取り除く
        # if self.request.GET.get("searchtype") == 'personnel' and company and company.capital_value_for_search_restriction:
        #     q = q.filter(contact__organization__capital_man_yen__gte=company.capital_value_for_search_restriction)

        # 取引先取引条件で取引先を絞る
        # 取引先の取引条件が設定されていない場合は、この制御は無効になる
        # 取引先の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と自社の自社情報設定「設立年月」「保有資格」「資本金」を比較
        filter_params = []
        if not company.has_distribution:
            filter_params.append(
                Q(
                    Q(contact__organization__capital_man_yen_required_for_transactions__lte=company.capital_man_yen) |
                    Q(contact__organization__capital_man_yen_required_for_transactions=None)
                ),
            )
            filter_params.append(
                Q(
                    Q(contact__organization__p_mark_or_isms=company.has_p_mark_or_isms) |
                    Q(contact__organization__p_mark_or_isms=False)
                ),
            )
            filter_params.append(
                Q(
                    Q(contact__organization__invoice_system=company.has_invoice_system) |
                    Q(contact__organization__invoice_system=False)
                ),
            )
            filter_params.append(
                Q(
                    Q(contact__organization__haken=company.has_haken) |
                    Q(contact__organization__haken=False)
                ),
            )
            base_year = relativedelta(datetime.datetime.now(), company.establishment_date).years
            filter_params.append(
                Q(
                    Q(contact__organization__establishment_year__lte=base_year) |
                    Q(contact__organization__establishment_year=None)
                ),
            )

        # 自社取引条件で取引先を絞る
        # 自社の取引条件が設定されていない場合は、この制御は無効になる
        # 自社の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と取引先の取引先情報「設立年月」「保有資格」「資本金」を比較
        if company.capital_man_yen_required_for_transactions:
            filter_params.append(
                Q(contact__organization__capital_man_yen__gte=company.capital_man_yen_required_for_transactions) |
                Q(contact__organization__capital_man_yen__isnull=True) |
                Q(contact__organization__has_distribution=True)
            )

        if company.p_mark_or_isms:
            filter_params.append(
                Q(
                    Q(contact__organization__has_p_mark_or_isms=company.p_mark_or_isms) |
                    Q(contact__organization__has_p_mark_or_isms__isnull=True) |
                    Q(contact__organization__has_distribution=company.p_mark_or_isms)
                )
            )

        if company.invoice_system:
            filter_params.append(
                Q(
                    Q(contact__organization__has_invoice_system=company.invoice_system) |
                    Q(contact__organization__has_invoice_system__isnull=True) |
                    Q(contact__organization__has_distribution=company.invoice_system)
                )
            )

        if company.haken:
            filter_params.append(
                Q(
                    Q(contact__organization__has_haken=company.haken) |
                    Q(contact__organization__has_haken__isnull=True) |
                    Q(contact__organization__has_distribution=company.haken)
                ),
            )

        if company.establishment_year:
            base_date = datetime.datetime.now() - relativedelta(years=company.establishment_year)
            filter_params.append(
                Q(contact__organization__establishment_date__lte=base_date) |
                Q(contact__organization__establishment_date__isnull=True) |
                Q(contact__organization__has_distribution=True)
            )

        # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=company)
        if exceptional_organizations:
            exceptional_organization_ids = [exceptional_organization.organization.id for exceptional_organization in exceptional_organizations]
            q = q.filter(Q(*filter_params) | Q(contact__organization__id__in=exceptional_organization_ids))
        else:
            q = q.filter(*filter_params)

        return q.distinct()


class ContactPreferenceView(MultiTenantMixin, GenericDetailAPIView):
    """Preference will be automatically created when created a contact.

    Note: Preference will also be automatically deleted when deleted a corresponding model,
     by on cascade constraint on the database.
    """
    queryset = ContactPreference.objects.select_related('created_user')
    serializer_class = ContactPreferenceSerializer
    lookup_field = 'contact_id'
    lookup_url_kwarg = 'pk'

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='DELETE')


class OrganizationCommentListView(MultiTenantMixin, GenericCommentListView, CommentValidationHelper):
    queryset = OrganizationComment.objects.all_with_deleted().filter(parent__isnull=True).\
        filter(Q(deleted_at__isnull=True) | Q(deleted_at__isnull=False, has_subcomment=True)).\
        select_related('created_user').prefetch_related('sub_comments')
    related_model_class = Organization
    serializer_class = OrganizationCommentSerializer
    related_field_name = 'organization'

    ordering_fields = ('is_important','created_time',)
    ordering = ('-is_important','-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_organization_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().post(request, *args, **kwargs)


class OrganizationSubCommentListView(MultiTenantMixin, GenericSubCommentListView, CommentValidationHelper):
    queryset = OrganizationComment.objects.filter(parent__isnull=False, is_important=False).select_related('created_user')
    related_model_class = Organization
    serializer_class = OrganizationSubCommentSerializer
    related_field_name = 'organization'

    parent_model_class = OrganizationComment
    parent_field_name = 'parent'

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class OrganizationCommentDetailView(MultiTenantMixin, GenericCommentDetailView, CustomValidationHelper, CommentValidationHelper):
    queryset = OrganizationComment.objects.select_related('created_user')
    serializer_class = OrganizationCommentSerializer
    related_field_name = 'organization'
    comment_model = OrganizationComment

    def patch(self, request, pk, *args, **kwargs):
        self.exec_custom_validation(request)
        if self.update_organization_important_comment_or_pass(request, pk) and self.is_organization_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().patch(request, *args, **kwargs)


class ContactCommentListView(MultiTenantMixin, GenericCommentListView, CommentValidationHelper):
    queryset = ContactComment.objects.all_with_deleted().filter(parent__isnull=True).\
        filter(Q(deleted_at__isnull=True) | Q(deleted_at__isnull=False, has_subcomment=True)).\
        select_related('created_user').prefetch_related('sub_comments')
    related_model_class = Contact
    serializer_class = ContactCommentSerializer
    related_field_name = 'contact'

    ordering_fields = ('is_important','created_time',)
    ordering = ('-is_important','-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_contact_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().post(request, *args, **kwargs)

class ContactSubCommentListView(MultiTenantMixin, GenericSubCommentListView, CommentValidationHelper):
    queryset = ContactComment.objects.filter(parent__isnull=False, is_important=False).select_related('created_user')
    related_model_class = Contact
    serializer_class = ContactSubCommentSerializer
    related_field_name = 'contact'

    parent_model_class = ContactComment
    parent_field_name = 'parent'

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class ContactCommentDetailView(MultiTenantMixin, GenericCommentDetailView, CustomValidationHelper, CommentValidationHelper):
    queryset = ContactComment.objects.select_related('created_user')
    serializer_class = ContactCommentSerializer
    related_field_name = 'contact'
    comment_model = ContactComment

    def patch(self, request, pk, *args, **kwargs):
        self.exec_custom_validation(request)
        if self.update_contact_important_comment_or_pass(request, pk) and self.is_contact_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().patch(request, *args, **kwargs)


class TagHelper(object):
    def is_registerd_value(self, value, ignore_id=None):
        query_set = Tag.objects.filter(internal_value=value.lower(), company=self.request.user.company)
        if ignore_id:
            query_set = query_set.exclude(id=ignore_id)
        return query_set.exists()

    def is_tag_can_be_removed(self, resource_ids):
        """Check exists record in table ScheduledEmailTarget with status: draft, queued, sending and contact_tags_id includes input list"""
        scheduled_email_exists = ScheduledEmailTarget.objects.filter(
            company=self.request.user.company,
            contact__tags__id__in=resource_ids,
            email__scheduled_email_status__name__in=[
                'draft',  # 下書き
                'queued',  # 配信待ちdư
                'sending',  # 送信中
            ]).exists()

        if scheduled_email_exists:
            raise TagRemoveException()
        return True


class TagListView(MultiTenantMixin, GenericListAPIView, TagHelper, BulkOperationHelper):
    queryset = Tag.objects.select_related('created_user')
    serializer_class = TagSerializer
    pagination_class = StandardPagenation
    filterset_class = TagFilter

    ordering_fields = ('created_time')
    ordering = ('-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_registerd_value(request.data['value']):
            raise TagRegisteredValueException()
        return super().post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        self.is_tag_can_be_removed(resource_ids)
        Tag.objects.filter(id__in=resource_ids).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class TagDetailView(MultiTenantMixin, GenericDetailAPIView, TagHelper, CustomValidationHelper):
    queryset = Tag.objects.select_related('created_user')
    serializer_class = TagSerializer

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        if self.is_registerd_value(request.data['value'], self.get_object().id):
            raise TagRegisteredValueException()
        return super().patch(request, *args, **kwargs)

    def delete(self, request, pk, *args, **kwargs):
        self.is_tag_can_be_removed(resource_ids=[pk])
        super().delete(request, *args, **kwargs)
        response_body = {
            "id": pk
        }
        return Response(status=status.HTTP_200_OK, data=response_body)

class DisplaySettingView(MultiTenantMixin, GenericDetailAPIView):
    serializer_class = DisplaySettingSerializer

    def get_object(self):
        company = self.request.user.company
        if hasattr(company, 'displaysetting'):
            return company.displaysetting
        else:
            return DisplaySetting.objects.create(company=company, content=json.dumps(settings.DISPLAY_SETTING_DEFAULT))

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

class UserDisplaySettingView(MultiTenantMixin, GenericDetailAPIView):
    serializer_class = UserDisplaySettingSerializer

    def get_object(self):
        user = self.request.user
        if hasattr(user, 'userdisplaysetting'):
            return user.userdisplaysetting
        else:
            return UserDisplaySetting.objects.create(user=user, content=json.dumps(settings.USER_DISPLAY_SETTING_DEFAULT))

    def patch(self, request, *args, **kwargs):
        user = self.request.user
        if not hasattr(user, 'userdisplaysetting'):
            UserDisplaySetting.objects.create(user=user, content=json.dumps(settings.USER_DISPLAY_SETTING_DEFAULT))
        return super().patch(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)


class ExceptionalOrganizationLimitException(APIException):
    status_code = 400
    default_detail = "3つ以上の除外取引先を設定することはできません。"
    default_code = 'exceptional_organization_limit_exception'
