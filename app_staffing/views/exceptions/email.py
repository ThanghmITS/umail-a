from rest_framework.exceptions import APIException


class EmailTOContactInvalidException(APIException):
    status_code = 400
    default_code = 'invalid_email_TO_contact'

    def __init__(self, email):
        self.default_detail = f'メールアドレス(TO)「{email}」は無効のメールアドレスです。'
        super().__init__(self.default_detail)


class EmailCCContactInvalidException(APIException):
    status_code = 400
    default_code = 'invalid_email_CC_contact'

    def __init__(self, emails):
        self.default_detail = f'メールアドレス(CC){emails}は無効のメールアドレスです。'
        super().__init__(self.default_detail)


class FieldDoesNotExits(APIException):
    status_code = 400
    default_code = 'field_does_not_exist '

    def __init__(self, message):
        self.default_detail = message
        super().__init__(self.default_detail)
