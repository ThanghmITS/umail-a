from rest_framework.exceptions import APIException

class ScheduledEmailDeliveryIntervalException(APIException):
    status_code = 400
    default_detail = '10分間隔の予約はできません'
    default_code = 'scheduled_email_delivery_interval_failed'
