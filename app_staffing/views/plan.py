from email import utils
from django.conf import settings
from django.db.transaction import atomic

from rest_framework import status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.exceptions import APIException, MethodNotAllowed
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from datetime import datetime, time, timedelta
from app_staffing.models import Plan, PlanMaster, CompanyAttribute, PurchaseHistory, PlanPaymentError, CardPaymentError, User, user
from app_staffing.serializers import PlanSerializer
from app_staffing.views.base import MultiTenantMixin, GenericListAPIView, PayjpHelper
from app_staffing.exceptions.payment import PaymentCardNotRegisteredException

from django.utils import timezone

import payjp
import pytz

from app_staffing.utils.payment import exec_charge, get_taxed_price
from app_staffing.utils.system_mail import send_system_mail

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


from app_staffing.exceptions.base import CardNotRegisteredException

class PlanListView(PayjpHelper, MultiTenantMixin, GenericListAPIView):
    serializer_class = PlanSerializer
    queryset = Plan.objects.filter(is_active=True)

    def get(self, request, *args, **kwargs):
        active_plans = Plan.objects.filter(is_active=True, company=self.request.user.company)

        if not active_plans or 0 == len(active_plans):
            # プランに入っていない場合は、無料トライアルの情報を返す
            attribute = CompanyAttribute.objects.get(company=self.request.user.company)
            ret = {
                "id": None,
                "expiration_date": attribute.trial_expiration_date,
                "plan_master_id": None,
                "user_registration_limit": 5,
                "default_user_count": 5,
                "current_user_count": User.objects.filter(company=self.request.user.company, is_active=True).count(),
            }
        else:
            ret = PlanSerializer(active_plans[0]).data

        return Response(status=status.HTTP_200_OK, data=ret)

    def _exec_charge(self, plan):
        # card番号取得する
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(str(self.request.user.company_id))
        if customer['cards']['data'] and 0 < len(customer['cards']['data']):
            last4 = customer['cards']['data'][0]['last4']
        else:
            raise CardNotRegisteredException()

        # 税率を乗算する
        taxed_price = get_taxed_price(plan.plan_master.price)

        # PurchaseHistoryをCreateする
        PurchaseHistory.objects.create(
            company_id=self.request.user.company_id,
            period_start=datetime.now(pytz.timezone(settings.TIME_ZONE)),
            period_end=plan.expiration_date,
            plan=plan,
            card_last4=last4,
            price=taxed_price,
            method=settings.PURCHASE_HISTORY_METHOD_CREDIT_CARD,
        )

        return exec_charge(
            taxed_price,
            self.request.user.company,
            f'purchase plan master : {plan.plan_master.id}'
        )

    @atomic
    def perform_create(self, serializer):
        if Plan.objects.filter(is_active=True, company=self.request.user.company).exists():
            raise PlanPurchaseException()

        try:
            plan_master = PlanMaster.objects.get(id=self.request.data.get("plan_master_id"))

            try:
                attribute = CompanyAttribute.objects.get(company=self.request.user.company)
                if attribute.user_registration_limit < plan_master.default_user_count:
                    attribute.user_registration_limit = plan_master.default_user_count
                attribute.trial_expiration_date = None
                attribute.save()
            except:
                attribute = CompanyAttribute.objects.create(
                    company=self.request.user.company,
                    user_registration_limit=plan_master.default_user_count,
                    trial_expiration_date=None,
                )

            # attributeをserializerで参照するため、attribute作成後にsaveする
            created_item = serializer.save(
                company=self.request.user.company,
                plan_master=plan_master
            )

            cards = self._get_cards(self.request.user.company_id)
            if "data" in cards and len(cards["data"]) < 1:
                raise PaymentCardNotRegisteredException()

            # 有効化処理が全て完了したら支払い処理を実行する(PayjpのAPIを実行)
            is_success, err_with_main, err_with_backup = self._exec_charge(created_item)

            if not is_success:
                try:
                    if err_with_backup is None:
                        raise err_with_main
                    else:
                        logger_stderr.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, err_with_main))
                        raise err_with_backup
                except payjp.error.CardError as err:
                    error_code = err.json_body['error']['code']
                    if error_code in settings.PAYJP_CHARGE_ERROR_MESSAGES:
                        raise PaymentException(detail=settings.PAYJP_CHARGE_ERROR_MESSAGES[error_code])
                    else:
                        raise PaymentUnknownException()
                except Exception as err:
                    raise PaymentUnknownException()

            try:
                context = {
                    'plan_name': plan_master.title,
                    'url': settings.HOST_URL,
                }
                if is_success:
                    send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_PLAN_PURCHASE_SUBJECT, settings.TEMPLATE_EMAIL_PLAN_PURCHASE_MESSAGE, context)
                    if err_with_main is not None:
                        send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE, context)
                    logger_stdout.info('plan_purchase_finish_info : CompanyId: {0}, CompanyName: {1}, User: {2}, Email: {3}, Plan: {4}'.format(
                        self.request.user.company.id,
                        self.request.user.company.name,
                        self.request.user.display_name,
                        self.request.user.email,
                        plan_master.title
                    ))
            except Exception as e:
                logger_stderr.error('plan purchased notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, e))

            return created_item
        except (PaymentException, PaymentUnknownException, PaymentCardNotRegisteredException) as e:
            # 既知のエラーに関してはそのままRaiseする
            raise e
        except Exception:
            # 未知のエラーについては共通の500エラーではなく、専用のエラーをraiseする
            raise PlanPurchaseException()

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def patch(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PATCH')

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='DELETE')

class PlanPurchaseException(APIException):
    status_code = 500
    default_detail = settings.PLAN_PURCHASE_ERROR_MESSAGE
    default_code = 'plan_purchase_failed'

class PaymentException(APIException):
    status_code = 400
    default_detail = settings.PLAN_PURCHASE_PAYMENT_ERROR_MESSAGE
    default_code = 'plan_purchase_payment_failed'

class PaymentUnknownException(APIException):
    status_code = 500
    default_detail = settings.PLAN_PURCHASE_PAYMENT_UNKNOWN_ERROR_MESSAGE
    default_code = 'plan_purchase_payment_unknown_failed'

class AddUserCountView(MultiTenantMixin, GenericListAPIView):

    def get(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='GET')

    @atomic
    def post(self, request, *args, **kwargs):
        if CompanyAttribute.objects.filter(company=self.request.user.company, trial_expiration_date__gte=datetime.now()).exists():
            raise AddUserCountException()

        try:
            # 加入中のプラン情報を取得する
            active_plans = Plan.objects.filter(is_active=True, company=self.request.user.company)

            if not active_plans or 0 == len(active_plans):
                return Response(status=status.HTTP_400_BAD_REQUEST)
            elif 1 < len(active_plans):
                return Response(status=status.HTTP_400_BAD_REQUEST)
            current_active_plan = active_plans[0]

            # 利用可能ユーザ数を加算する
            try:
                attribute = CompanyAttribute.objects.get(company=self.request.user.company)
            except:
                attribute = CompanyAttribute.objects.create(
                    company=self.request.user.company,
                    user_registration_limit=current_active_plan.plan_master.default_user_count
                )
            attribute.user_registration_limit += 1
            attribute.save()

            # card番号取得する
            payjp.api_key = settings.PAYJP_API_KEY
            customer = payjp.Customer.retrieve(str(self.request.user.company_id))
            if customer['cards']['data'] and 0 < len(customer['cards']['data']):
                last4 = customer['cards']['data'][0]['last4']
            else:
                raise CardNotRegisteredException()

            # 税率を乗算する
            taxed_price = get_taxed_price(current_active_plan.plan_master.user_add_price)

            # PurchaseHistoryをCreateする
            PurchaseHistory.objects.create(
                company_id=self.request.user.company_id,
                period_start=timezone.now(),
                period_end=current_active_plan.expiration_date,
                card_last4=last4,
                price=taxed_price,
                method=settings.PURCHASE_HISTORY_METHOD_CREDIT_CARD,
                additional_options=settings.PURCHASE_HISTORY_USER_ADD_MESSAGE.format(1),
            )

            try:
                # 請求処理を実行
                is_success, err_with_main, err_with_backup = exec_charge(
                    taxed_price,
                    self.request.user.company,
                    f'user add, registration limit: {attribute.user_registration_limit}'
                )

                if is_success:
                    # 決済成功
                    if err_with_main is not None:
                        logger_stderr.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, err_with_main))
                else:
                    # 決済失敗
                    if err_with_backup is None:
                        raise err_with_main
                    else:
                        logger_stderr.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, err_with_main))
                        raise err_with_backup

            except payjp.error.CardError as err:
                error_code = err.json_body['error']['code']
                if error_code in settings.PAYJP_CHARGE_ERROR_MESSAGES:
                    raise PaymentException(detail=settings.PAYJP_CHARGE_ERROR_MESSAGES[error_code])
                else:
                    raise PaymentUnknownException()

            try:
                context = {
                    'url': settings.HOST_URL,
                }
                if is_success:
                    send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_ADD_USER_COUNT_SUBJECT, settings.TEMPLATE_EMAIL_ADD_USER_COUNT_MESSAGE, context)
                    if err_with_main is not None:
                        send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE, context)
            except Exception as e:
                logger_stderr.error('plan purchased notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, e))

            return Response(status=status.HTTP_201_CREATED, data={})
        except (CardNotRegisteredException, PaymentException, PaymentUnknownException) as e:
            raise e
        except Exception:
            raise AddUserCountException()

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def patch(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PATCH')

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='DELETE')

class RemoveUserCountView(MultiTenantMixin, GenericListAPIView):

    def get(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='GET')

    @atomic
    def post(self, request, *args, **kwargs):
        # 加入中のプラン情報を取得する
        active_plans = Plan.objects.filter(is_active=True, company=self.request.user.company)

        if not active_plans or 0 == len(active_plans):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        elif 1 < len(active_plans):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        current_active_plan = active_plans[0]

        # 利用可能ユーザ数を減算する
        try:
            attribute = CompanyAttribute.objects.get(company=self.request.user.company)
        except:
            attribute = CompanyAttribute.objects.create(
                company=self.request.user.company,
                user_registration_limit=current_active_plan.plan_master.default_user_count
            )

        # プランに設定されているデフォルトより少なくしようとした場合はエラー
        if attribute.user_registration_limit <= current_active_plan.plan_master.default_user_count:
            raise RemoveUserCountException()

        if (User.objects.filter(company=self.request.user.company,is_active=True).count()==attribute.user_registration_limit):
            raise RemoveUserCountException()

        attribute.user_registration_limit -= 1
        attribute.save()

        send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_REMOVE_USER_COUNT_SUBJECT, settings.TEMPLATE_EMAIL_REMOVE_USER_COUNT_MESSAGE)

        return Response(status=status.HTTP_201_CREATED, data={})

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def patch(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PATCH')

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='DELETE')

class AddUserCountException(APIException):
    status_code = 500
    default_detail = 'ユーザーの追加に失敗しました。サポートまでお問い合わせください。'
    default_code = 'add_user_count_failed'

class RemoveUserCountException(APIException):
    status_code = 400
    default_detail = 'ユーザー登録上限数の削除に失敗しました。'
    default_code = 'remove_user_count_failed'

class PlanSummaryView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication,)

    def get(self, request, *args, **kwargs):
        company = self.request.user.company
        try:
            plan = Plan.objects.get(company=company, is_active=True)
        except Exception:
            plan = None

        now = datetime.now(pytz.timezone(settings.TIME_ZONE))
        base_date = now.date()
        remaining_days = None
        if plan:
            plan_expiration_date = plan.expiration_date
            # 支払い猶予期間(10日間分)を加算した日付をもとに判定
            plan_expiration_date_with_postponement = plan_expiration_date + timedelta(days=settings.PLAN_EXPIRE_POSTPONEMENT_DAYS)
            # 支払い猶予期間込みのプラン終了日が現在日以上の場合は、残り何日でログインできなくなるかの日数を返却する
            if base_date <= plan_expiration_date_with_postponement:
                remaining_days = str((plan_expiration_date_with_postponement - base_date).days)
            return Response(status=status.HTTP_200_OK, data={
                "plan_id": plan.plan_master_id,
                "plan_name": plan.plan_master.title,
                "remaining_days": remaining_days,
                "payment_error_exists": PlanPaymentError.objects.filter(company=company, plan=plan).exists(),
                "payment_error_card_ids": CardPaymentError.objects.filter(company=company, plan=plan).values_list('card_id', flat=True),
            })
        else:
            try:
                company_attribute = CompanyAttribute.objects.get(company=company)
                trial_expiration_date = company_attribute.trial_expiration_date
                # 無料トライアル終了日が現在日以上の場合は、残り何日でトライアルが終了するかの日数を返却する
                if base_date <= trial_expiration_date:
                    remaining_days = str((trial_expiration_date - base_date).days)
            except Exception:
                raise TrialException() # デフォルトメッセージで問題なし
            return Response(status=status.HTTP_200_OK, data={
                "plan_id": 0,
                "plan_name": "無料トライアル",
                "remaining_days": remaining_days,
                "payment_error_exists": False,
                "payment_error_card_ids": [],
            })

    def post(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def patch(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PATCH')

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='DELETE')

class TrialException(APIException):
    status_code = 400
    default_detail = 'トライアル情報の取得に失敗しました。'
    default_code = 'get_trial_failed'
