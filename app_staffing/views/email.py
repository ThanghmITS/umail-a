from django.conf import settings
from django.db.models import ObjectDoesNotExist, Subquery, OuterRef, Prefetch
from django.db.transaction import atomic
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.filters import OrderingFilter
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from app_staffing.estimators.recommenders import JobEmailRecommender
from app_staffing.models import (
    Email,
    EmailAttachment,
    INBOUND,
    ExceptionalOrganization,
    Contact,
    Organization,
    SharedEmailSetting,
    EmailComment,
    User,
    SharedEmailTransferHistory,)
from app_staffing.serializers import (EmailSerializer, EmailAttachmentSerializer, EmailDetailSerializer,
                                      EmailSummarySerializer, SharedEmailSettingSerializer,
                                      EmailCommentSerializer, EmailSubCommentSerializer)
from app_staffing.services.email.forward_mail import forward_mail
from app_staffing.utils.custom_validation import mail_address_check
from app_staffing.views.base import (FileDetailView, CustomValidationHelper,
                                     BulkOperationHelper, IsAuthorizedAction,
                                     CustomTokenAuthentication, GenericSubCommentListView, CommentValidationHelper)
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.views.filters import EmailFilter, SearchFilter
from app_staffing.utils.email import filter_email_by_sent_date_gte

from app_staffing.services.stats import EmailTrendStats, EmailRankingStats, EmailReplyTrendStats
from config.settings.base import EMAIL_HOST_BLOCKED

from .base import (
    GenericActionView, GenericListAPIView, GenericDetailAPIView, GenericRecommendAPIView,
    StatisticsAPIView, MultiTenantMixin, GenericCommentListView, GenericCommentDetailView, OrderIdHelper
)

from django.db.models.query_utils import Q
import imaplib
from poplib import POP3, POP3_SSL

from app_staffing.exceptions.base import BulkOperationLimitException
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.views.exceptions.email import EmailTOContactInvalidException, EmailCCContactInvalidException, FieldDoesNotExits

from app_staffing.utils.logger import get_info_logger, get_error_logger
from app_staffing.exceptions.scheduled_email import EmailDomainBlocked

logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


# Create your views here.
class SharedEmailSummaryView(MultiTenantMixin, GenericListAPIView, OrderIdHelper, BulkOperationHelper):
    email_comments_subquery = Subquery(
        EmailComment.objects
            .filter(is_important=True, email_id=OuterRef('email_id'))
            .values_list('id', flat=True)
            .order_by('-modified_time')[:settings.LIST_COMMENT_VIEW_LIMIT_COUNT])
    shared_histories_subquery = Subquery(
        SharedEmailTransferHistory.objects
            .filter(email_id=OuterRef('email_id'))
            .values_list('id', flat=True).order_by('-transfer_date')[:settings.LIST_COMMENT_VIEW_LIMIT_COUNT]
    )
    attachments_subquery = Subquery(
        EmailAttachment.objects
            .filter(email_id=OuterRef('email_id'))
            .values_list('id', flat=True)
            .order_by('created_time')
    )
    queryset = Email.objects.filter(
        direction=INBOUND,
        shared__exact=True,
    ).select_related(
        'staff_in_charge'
    ).prefetch_related(
        Prefetch(
            'emailcomment_set',
            queryset=EmailComment.objects.filter(id__in=email_comments_subquery)
            .select_related('created_user', 'modified_user')
            .order_by('-modified_time')
        )
    ).prefetch_related(
        Prefetch(
            'shared_history',
            queryset=SharedEmailTransferHistory.objects.filter(id__in=shared_histories_subquery)
            .select_related('sender', 'receiver')
            .order_by('-transfer_date')
        )
    ).prefetch_related(
        Prefetch(
            'attachments',
            queryset=EmailAttachment.objects.filter(id__in=attachments_subquery).order_by('created_time')
        )
    )

    serializer_class = EmailSummarySerializer
    pagination_class = StandardPagenation
    filterset_class = EmailFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)

    ordering_fields = (
        'staff_in_charge__last_name',
        'sent_date',
    )
    ordering = ('-sent_date',)
    search_fields = ('@text',)

    def get_queryset(self):
        qs = filter_email_by_sent_date_gte(super().get_queryset())

        if self.request.GET.get("ignore_filter") == 'ignore_filter':
            return qs

        company = self.request.user.company

        filter_params = self.trade_condition_company(company)
        if not filter_params:
            return qs
        # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=company)
        email_ids = []
        from_mails = [email.from_address for email in qs]
        exclusion_organization_ids = [contact.organization_id for contact in Contact.objects.filter(email__in=from_mails)]
        organizations = Organization.objects.filter(pk__in=exclusion_organization_ids).filter(*filter_params)
        if exceptional_organizations:
            e_organizations = [exceptional_organization.organization for exceptional_organization in exceptional_organizations]
            if organizations or e_organizations:
                contacts = Contact.objects.filter(Q(organization__in=organizations) | Q(organization__in=e_organizations))
                emails = [contact.email for contact in contacts]
                email_ids = [email.id for email in Email.objects.filter(from_address__in=emails)]

            filter_params = [Q(id__in=email_ids)]
            return qs.filter(*filter_params)
        else:
            if organizations:
                emails = [contact.email for contact in Contact.objects.filter(organization__in=organizations)]
                email_ids = [email.id for email in Email.objects.filter(from_address__in=emails)]

            filter_params = [Q(id__in=email_ids)]
            return qs.filter(*filter_params)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        Email.objects.select_related('created_user').filter(id__in=resource_ids, company=self.request.user.company).delete()
        return Response(status=status.HTTP_200_OK, data=resource_ids)


class EmailListView(MultiTenantMixin, GenericListAPIView):
    queryset = Email.objects.prefetch_related('cc_addresses')
    serializer_class = EmailSerializer

    @atomic
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        # After save record, we also need classify
        # TODO: Implement Classification procedure to Classifier.
        # TODO: Store the classification result to record.
        # TODO: Implement Registration procedure to Recommender.

        return response

    def create(self, request, *args, **kwargs):
        # This view supports get or create
        # TODO: Implement query_param validation.
        success_on_exists = request.query_params.get('success_on_exists', False) == 'True'

        # TODO: Export following logic to Mixin. (Refactor)
        if success_on_exists:
            try:
                self.queryset.get(mail_id=request.data.get('mail_id'))
                headers = self.get_success_headers(data=request.data)
                return Response({'info': 'same mail_id already exists.'}, status=status.HTTP_204_NO_CONTENT, headers=headers)
            except ObjectDoesNotExist:
                return super().create(request, *args, **kwargs)
        else:
            return super().create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class EmailDetailView(MultiTenantMixin, GenericDetailAPIView):
    queryset = Email.objects.select_related('staff_in_charge').prefetch_related('attachments')
    serializer_class = EmailDetailSerializer

    def get_queryset(self):
        return filter_email_by_sent_date_gte(super().get_queryset())


class EmailForwardView(MultiTenantMixin, GenericActionView):
    queryset = Email.objects.all()

    def create(self, request, *args, **kwargs):
        if 'staff_in_charge_id' in request.data:
            instance = self.get_object()
            staff_in_charge_id = request.data['staff_in_charge_id']
            staff_in_charge = User.objects.get(id=staff_in_charge_id, company=request.user.company)
            instance.staff_in_charge = staff_in_charge
            instance.save()
            forward_mail(original_email=instance, forward_to=staff_in_charge.email)
            # NOTE(joshua-hashimoto): 転送履歴を作成
            user = self.request.user
            SharedEmailTransferHistory.objects.create(
                email=instance,
                sender=user,
                receiver=staff_in_charge,
            )
        return Response(status=status.HTTP_200_OK, data={})


class SharedEmailAttachmentView(MultiTenantMixin, FileDetailView):
    queryset = EmailAttachment.objects.all()
    serializer_class = EmailAttachmentSerializer

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)


class JobEmailRecommendView(MultiTenantMixin, GenericRecommendAPIView):
    """This view returns a list of related Emails (category=job) based on a given email's content."""
    queryset = Email.objects.all()
    serializer_class = EmailDetailSerializer
    recommender_class = JobEmailRecommender


class SharedEmailStatsView(StatisticsAPIView):

    def get(self, request, *args, **kwargs):
        cid = request.user.company.id

        trend = EmailTrendStats(company_id=cid).get_stats()
        ranking = EmailRankingStats(company_id=cid).get_stats()
        reply = EmailReplyTrendStats(company_id=cid).get_stats()

        data = {
            'trend': {
                'labels': trend['labels'],
                'job_email_counts': trend['job_email_counts'],
                'personnel_email_counts': trend['personnel_email_counts'],
            },
            'ranking': {
                'monthly': {
                    'from_headers': ranking['labels'],
                    'counts': ranking['values']
                }
            },
            'reply': {
                'labels': reply['labels'],
                'counts': reply['counts'],
             },
        }

        return Response(status=status.HTTP_200_OK, data=data)

class SharedEmailSettingView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    serializer_class = SharedEmailSettingSerializer

    def get_object(self):
        company = self.request.user.company
        if hasattr(company, 'sharedemailsetting'):
            return company.sharedemailsetting
        else:
            return SharedEmailSetting.objects.create(company=company, allow_self_domain=True)

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        raise MethodNotAllowed(method=self.request.method)

class SharedEmailConnectionView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    serializer_class = SharedEmailSettingSerializer

    def check_imap_connection(self, server, username, password):
        server.login(username, password)
        server.logout()

    def check_pop3_coonnection(self, server, username, password):
        server.user(username)
        server.pass_(password)
        server.quit()

    def check_server_connection(self, protocol, domain, username, password):
        if protocol== 'imap+ssl':
            server = imaplib.IMAP4_SSL(domain, 993)
            self.check_imap_connection(server, username, password)
        elif protocol== 'imap':
            server = imaplib.IMAP4(domain, 143)
            self.check_imap_connection(server, username, password)
        elif protocol== 'pop3+ssl':
            server = POP3_SSL(domain, 995)
            self.check_pop3_coonnection(server, username, password)
        elif protocol== 'pop3':
            server = POP3(domain, 110)
            self.check_pop3_coonnection(server, username, password)

        return True

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        from_email = request.data['from_email']
        password = request.data['password']
        protocol = request.data['protocol']
        splited_items = from_email.split('@')

        if len(splited_items) <= 1:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={})

        username = splited_items[0]
        domain = splited_items[1]
        if domain in EMAIL_HOST_BLOCKED:
            raise EmailDomainBlocked(domain)

        try:
            if self.check_server_connection(protocol, domain, username, password):
                return Response(status=status.HTTP_200_OK, data={})
        except Exception as e:
            logger_stderr.warning('Could not connect to server. domain:{0}, username:{1}, protocol:{2}, Error Details:{3}'.format(domain, username, protocol, e))
            return Response(status=status.HTTP_400_BAD_REQUEST, data={})

        return Response(status=status.HTTP_400_BAD_REQUEST, data={})


class EmailCommentListView(MultiTenantMixin, GenericCommentListView, CommentValidationHelper):
    queryset = EmailComment.objects.all_with_deleted().filter(parent__isnull=True).\
        filter(Q(deleted_at__isnull=True) | Q(deleted_at__isnull=False, has_subcomment=True)).\
        select_related('created_user', 'modified_user').prefetch_related('sub_comments')
    related_model_class = Email
    serializer_class = EmailCommentSerializer
    related_field_name = 'email'

    ordering_fields = ('is_important','created_time',)
    ordering = ('-is_important','-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_shared_email_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().post(request, *args, **kwargs)


class EmailCommentDetailView(MultiTenantMixin, GenericCommentDetailView, CommentValidationHelper):
    queryset = EmailComment.objects.select_related('created_user')
    serializer_class = EmailCommentSerializer
    related_field_name = 'email'
    comment_model = EmailComment

    def patch(self, request, *args, **kwargs):
        if self.update_shared_email_important_comment_or_pass(kwargs.get('pk')) \
                and self.is_shared_email_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().patch(request, *args, **kwargs)


class EmailSubCommentListView(MultiTenantMixin, GenericSubCommentListView):
    queryset = EmailComment.objects.filter(parent__isnull=False, is_important=False).select_related('created_user', 'modified_user')
    related_model_class = Email
    serializer_class = EmailSubCommentSerializer
    related_field_name = 'email'

    parent_model_class = EmailComment
    parent_field_name = 'parent'

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class EmailAddressCheckView(CreateAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    def post(self, request, *args, **kwargs):
        if 'email' in request.data:
            email = request.data.get('email')
            if mail_address_check(email):
                return Response(status=status.HTTP_200_OK, data={'detail': 'メールアドレス(TO)は有効です。'})
            else:
                raise EmailTOContactInvalidException(email=email)
        elif 'cc_emails' in request.data:
            cc_emails = request.data.get('cc_emails').split(',')
            invalid_mails = [mail for mail in cc_emails if not mail_address_check(mail)]
            if invalid_mails:
                cc = ''.join(f'「{e}」' for e in invalid_mails)
                raise EmailCCContactInvalidException(emails=cc)
            else:
                return Response(status=status.HTTP_200_OK, data={'detail': 'メールアドレス(CC)は有効です。'})
        else:
            raise FieldDoesNotExits(message='メールアドレスを正しく入力してください。')
