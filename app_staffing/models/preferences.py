"""Models related to preferences of email marketing."""
from uuid import uuid4

from django.db import models
from django_countries.fields import CountryField

from app_staffing.models.base import PerCompanyOneToOneModel, PerCompanyModel

from softdelete.models import SoftDeleteObject

ENTRUST_PROHIBITED = 0
ENTRUST_NO_LIMITATION = 255


COMMERCIAL_DISTRIBUTION_LIMIT = (  # 商流制限。0は自社まで、100は無制限扱い。
    (ENTRUST_PROHIBITED, 'Re-entrust prohibited'),  # 再委託不可
    (ENTRUST_NO_LIMITATION, 'No limitation')  # 制限なし
)


class Location(SoftDeleteObject, models.Model):  # Currently, this app supports japanese locations only.
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=128, help_text='地域の名称(多言語対応)')
    country = CountryField(default='JP')

    def __str__(self):
        return self.name + '@' + self.country.name


class ContactPreference(PerCompanyOneToOneModel):
    """A model for mail marketing configuration that correspond to contacts."""
    contact = models.OneToOneField('Contact', on_delete=models.CASCADE, primary_key=True)

    # 地域に関する条件
    wants_location_kanto_japan = models.BooleanField(default=False, help_text='関東地方の情報を募集しているか')
    wants_location_kansai_japan = models.BooleanField(default=False, help_text='関西地方の情報を募集しているか')
    wants_location_hokkaido_japan = models.BooleanField(default=False, help_text='北海道地方の情報を募集しているか')
    wants_location_touhoku_japan = models.BooleanField(default=False, help_text='東北地方の情報を募集しているか')
    wants_location_chubu_japan = models.BooleanField(default=False, help_text='中部地方の情報を募集しているか')
    wants_location_kyushu_japan = models.BooleanField(default=False, help_text='九州地方の情報を募集しているか')
    wants_location_other_japan = models.BooleanField(default=False, help_text='その他地方の情報を募集しているか')
    wants_location_chugoku_japan = models.BooleanField(default=False, help_text='中国地方の情報を募集しているか')
    wants_location_shikoku_japan = models.BooleanField(default=False, help_text='四国地方の情報を募集しているか')
    wants_location_toukai_japan = models.BooleanField(default=False, help_text='東海地方の情報を募集しているか')

    job_koyou_proper = models.BooleanField(default=False, help_text='案件配信_雇用形態_プロパー')
    job_koyou_free = models.BooleanField(default=False, help_text='案件配信_雇用形態_フリーランス')
    job_syouryu = models.IntegerField(blank=True, null=True, help_text='案件配信_商流制限')
    personnel_syouryu = models.IntegerField(blank=True, null=True, help_text='要員配信_商流制限')

    personnel_country_japan = models.BooleanField(default=False, null=True, help_text='国籍_日本')
    personnel_country_other = models.BooleanField(default=False, null=True, help_text='国籍_日本以外の外国籍')
    has_send_guide = models.BooleanField(default=True, null=True, blank=True)

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} のプロファイル'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtProfile_contact_idx'),
            models.Index(fields=['company'], name='stf_CtProfile_company_idx'),
        ]


class TypePreference(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=128, help_text='タイプ名称')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='type_preference_name_idx'),
        ]


class SkillPreference(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=128, help_text='スキル名称')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='skill_preference_name_idx'),
        ]


class ContactJobTypePreference(PerCompanyModel):
    contact = models.ForeignKey('Contact', on_delete=models.PROTECT, related_name='contactjobtypepreferences')
    type_preference = models.ForeignKey(TypePreference, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} の希望職種(案件)プロファイル'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtJtProfile_contact_idx'),
            models.Index(fields=['company'], name='stf_CtJtProfile_company_idx'),
        ]


class ContactJobSkillPreference(PerCompanyModel):
    contact = models.ForeignKey('Contact', on_delete=models.PROTECT, related_name='contactjobskillpreferences')
    skill_preference = models.ForeignKey(SkillPreference, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} の希望スキル(案件)プロファイル'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtJsProfile_contact_idx'),
            models.Index(fields=['company'], name='stf_CtJsProfile_company_idx'),
        ]


class ContactPersonnelTypePreference(PerCompanyModel):
    contact = models.ForeignKey('Contact', on_delete=models.PROTECT, related_name='contactpersonneltypepreferences')
    type_preference = models.ForeignKey(TypePreference, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} の希望職種(要員)プロファイル'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtPtProfile_contact_idx'),
            models.Index(fields=['company'], name='stf_CtPtProfile_company_idx'),
        ]


class ContactPersonnelSkillPreference(PerCompanyModel):
    contact = models.ForeignKey('Contact', on_delete=models.PROTECT, related_name='contactpersonnelskillpreferences')
    skill_preference = models.ForeignKey(SkillPreference, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} の希望スキル(要員)プロファイル'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtPsProfile_contact_idx'),
            models.Index(fields=['company'], name='stf_CtPsProfile_company_idx'),
        ]
