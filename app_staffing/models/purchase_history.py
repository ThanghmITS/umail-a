import json
from uuid import uuid4

from django.conf import settings
from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.models import Plan
from app_staffing.models.base import PerCompanyModel

class PurchaseHistory(SoftDeleteObject, PerCompanyModel):
    period_start = models.DateField()
    period_end = models.DateField()
    plan = models.ForeignKey(Plan, null=True, on_delete=models.PROTECT)
    addon_ids = models.CharField(max_length=128)
    additional_options = models.CharField(max_length=512)
    card_last4 = models.CharField(max_length=4)
    price = models.IntegerField()
    method = models.IntegerField()

    def __str__(self):
        return f"{self.price} purchased by: {self.company.name} at: {self.created_time}"
    
    class Meta:
        indexes = [
            models.Index(fields=['company', '-created_time'], name='stf_ph_ct_idx'),
        ]
