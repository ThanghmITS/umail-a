import logging
from .tests import DummyModel

from .user import User, UserRole
from .email import (
    Email,
    EmailAttachment,
    ScheduledEmail,
    ScheduledEmailAttachment,
    ScheduledEmailTarget,
    MailboxMapping,
    SMTPServer,
    INBOUND,
    SharedEmailSetting,
    EmailComment,
    ScheduledEmailStatus,
    ScheduledEmailSendType,
    SharedEmailTransferHistory,
    ScheduledEmailSetting,)
from .notification import EmailNotificationRule, EmailNotificationCondition, SystemNotification, UserSystemNotification
from .organization import Contact, ContactCC, ContactScore, ContactComment, Company, Organization, OrganizationComment,\
    Tag, ContactTagAssignment, PROSPECTIVE, APPROACHED, CLIENT, INFORMATION_EXCHANGED, ExceptionalOrganization, VERY_LOW, LOW, MIDDLE, SEMI_MIDDLE,\
    HIGH, VERY_HIGH, DisplaySetting, UserDisplaySetting, ContactCategory, OrganizationCategory, OrganizationCountry, OrganizationEmployeeNumber,\
    OrganizationBranch, JP, CN, KR, OTHER, HEART, FROWN, CompanyAttribute
from .preferences import Location, ContactPreference, ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference, ContactPersonnelSkillPreference,\
    TypePreference, SkillPreference
from .addon import Addon, AddonMaster
from .stats import OrganizationStat, ContactStat, ScheduledEmailStat, SharedEmailStat
from .plan import PlanMaster, Plan, PlanPaymentError, CardPaymentError
from .purchase_history import PurchaseHistory

import django
from django.db import models
import datetime
import warnings
from django.conf import settings
from django.utils import timezone
from django.utils.dateparse import parse_date, parse_datetime
from django.core import exceptions
from app_staffing.utils.logger import get_info_logger
logger_stdout = get_info_logger(__name__)

# https://github.com/django/django/blob/stable/2.2.x/django/db/models/fields/__init__.py#L1366
# GCP上ではログがErrorレベルで吐き出されてしまうため、
def _to_python(self, value):
    if value is None:
        return value
    if isinstance(value, datetime.datetime):
        return value
    if isinstance(value, datetime.date):
        value = datetime.datetime(value.year, value.month, value.day)
        if settings.USE_TZ:
            # For backwards compatibility, interpret naive datetimes in
            # local time. This won't work during DST change, but we can't
            # do much about it, so we let the exceptions percolate up the
            # call stack.

            # 元々の実装はこちら
            # warnings.warn("DateTimeField %s.%s received a naive datetime "
            #               "(%s) while time zone support is active." %
            #               (self.model.__name__, self.name, value),
            #               RuntimeWarning)
            #
            # GCP上ではログがErrorレベルで吐き出されてしまうため、明示的にinfoで吐き出すよう変更している
            if django.VERSION[0] != 2 or django.VERSION[1] != 2:
                raise RuntimeError("django version updated. please check compatibility.")
            logger_stdout.info("DateTimeField %s.%s received a naive datetime (%s) while time zone support is active." % (self.model.__name__, self.name, value))
            default_timezone = timezone.get_default_timezone()
            value = timezone.make_aware(value, default_timezone)
        return value

    try:
        parsed = parse_datetime(value)
        if parsed is not None:
            return parsed
    except ValueError:
        raise exceptions.ValidationError(
            self.error_messages['invalid_datetime'],
            code='invalid_datetime',
            params={'value': value},
        )

    try:
        parsed = parse_date(value)
        if parsed is not None:
            return datetime.datetime(parsed.year, parsed.month, parsed.day)
    except ValueError:
        raise exceptions.ValidationError(
            self.error_messages['invalid_date'],
            code='invalid_date',
            params={'value': value},
        )

    raise exceptions.ValidationError(
        self.error_messages['invalid'],
        code='invalid',
        params={'value': value},
    )

models.fields.DateTimeField.to_python = _to_python
