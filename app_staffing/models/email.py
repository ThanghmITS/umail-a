import os
import hashlib
from uuid import uuid4

from django.conf import settings
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from storages.backends.gcloud import GoogleCloudStorage

from app_staffing.models.base import CommentBaseModel, PerCompanyModel, check_fields_consistency, OneToOneModel, OrderIdMixin

from softdelete.models import SoftDeleteObject

from django.db.models.query_utils import Q

from app_staffing.constants.email import AttachmentFileStatus, ScheduledEmailSettingFileStatus
from django.utils.crypto import get_random_string

PERSON = 'person'
JOB = 'job'
OTHER = 'other'

EMAIL_CATEGORIES = (
    (PERSON, 'person'),
    (JOB, 'job'),
    (OTHER, 'other'),
)

INBOUND = 'inbound'
OUTBOUND = 'outbound'

MAIL_DIRECTION = (
    (INBOUND, 'in'),
    (OUTBOUND, 'out'),  # Going to be sent.
)

DRAFT = 'draft'
QUEUED = 'queued'
SENT = 'sent'
SENDING = 'sending'
ERROR = 'error'
TIMEOUT = 'timeout'

MAIL_STATUS = (
    (DRAFT, 'draft'),
    (QUEUED, 'queued'),  # Going to be sent.
    (SENDING, 'sending'),
    (SENT, 'sent'),
    (ERROR, 'error'),
    (TIMEOUT, 'timeout')
)

ATTACHMENT_CATEGORIES = (
    (INBOUND, 'inbound'),
    (OUTBOUND, 'outbound')
)

JOB = 'job'
PERSONNEL = 'personnel'
OTHER = 'other'

SEND_TYPE = (
    (JOB, 'job'),
    (PERSONNEL, 'personnel'),
    (OTHER, 'other'),
)

TEXT = 'text'
HTML = 'html'

TEXT_FORMAT = (
    (TEXT, 'text'),
    (HTML, 'html'),
)

class MailboxMapping(SoftDeleteObject, models.Model):
    """An model that describes mapping between Company and django-mailbox's Mailbox model.
    Note: to describe mapping, we only need to add column company_id to Mailbox model,
    but Mailbox model is a library model, so we can not modify it.
    In stead of modify that library, we define mapping model in this app.
    """
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey(
        'Company',
        on_delete=models.CASCADE,
        help_text='A reference to company model'
    )
    mailbox = models.ForeignKey(
        'django_mailbox.Mailbox',
        on_delete=models.CASCADE,
        help_text='A reference to Mailbox'
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['company'], condition=Q(deleted_at=None), name='uniq_one_company_has_one_mailbox'),
            models.UniqueConstraint(fields=['mailbox'], condition=Q(deleted_at=None), name='uniq_one_mailbox_is_occupied_by_one_company')
        ]
        indexes = [
            models.Index(fields=['company'], name='stf_mailbox_ref_company'),
            models.Index(fields=['mailbox'], name='stf_mailbox_ref_mailbox')
        ]

    def __str__(self):
        return self.company.name + ' - ' + self.mailbox.name


class SMTPServer(SoftDeleteObject, models.Model):
    """A model that represent SMTP server for sending email."""
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey(
        'Company',
        on_delete=models.CASCADE,
        help_text='A reference to company model'
    )
    hostname = models.CharField(max_length=255, help_text='A FQDN or IP address of the SMTP server.')
    port_number = models.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(65535)],
        help_text='A Port number of the SMTP server listening.'
    )
    use_tls = models.BooleanField(
        default=True,
        help_text='If you connect the SMTP server with TLS, leave it True.'
    )
    username = models.CharField(
        max_length=255,
        help_text='A username to login a SMTP server. (Does not always correspond to a sender address )'
    )
    password = models.CharField(
        max_length=255,
        help_text='A password to login a SMTP server.'
    )
    use_ssl = models.BooleanField(default=False)
    send_count_per_task = models.IntegerField(default=200)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['company'], condition=Q(deleted_at=None), name='uniq_one_company_has_one_smtp_server'),
        ]
        indexes = [
            models.Index(fields=['company'], name='stf_smtp_server_company'),
        ]

    def __str__(self):
        return self.hostname


class Email(PerCompanyModel, SoftDeleteObject):
    """An model definition of Received / Sent Email.
    Note: This is a snapshot model email, so should not have relationships with other models.
    Also, Data validation is minimum because we can't validate email via database constraints.

    """

    message_id = models.CharField(max_length=255, blank=True, null=True,  # Some client sends this value as null,
                                  # so we can't consider it unique.
                                  help_text='An original email ID (Received only).')
    shared = models.BooleanField(default=False)
    direction = models.CharField(max_length=8, choices=MAIL_DIRECTION)

    from_header = models.CharField(max_length=255, help_text='An original From header of the email.', blank=True, null=True)
    reply_to_header = models.CharField(max_length=255, help_text='An original Reply-To header of the email.', blank=True, null=True)
    from_name = models.CharField(max_length=255, blank=True, null=True, help_text='From Name of the email.')
    from_address = models.EmailField(help_text='From address of the email.', blank=True, null=True)
    to_header = models.CharField(max_length=255, blank=True, null=True, help_text='An original To header of the email.')
    cc_header = models.CharField(max_length=4096, help_text='An original CC header of the email.', blank=True, null=True)
    subject = models.TextField(max_length=255, help_text='Subject of the email.', blank=True, null=True)
    text = models.TextField(help_text='Message text of the email. If the mail body is written in html,'
                                      ' converted text will be stored', blank=True, null=True)
    html = models.TextField(help_text='Message body of the email. will stored only if the mail body is written in html.',
                            blank=True, null=True)
    sent_date = models.DateTimeField(null=True, help_text='A date to sent the email (JST)')
    has_attachments = models.BooleanField(
        null=True, default=False, help_text='A flag that indicates whether this email has attachments.(For filtering)'
    )
    category = models.CharField(max_length=16, blank=True, null=True, choices=EMAIL_CATEGORIES,
                                help_text='An email category labeled by human.')

    # Fields that store auto generated values by classifier or something.
    estimated_category = models.CharField(max_length=16, blank=True, null=True, choices=EMAIL_CATEGORIES,
                                          editable=False,
                                          help_text='An email category estimated by Text Classifier.')

    message_digest = models.CharField(max_length=64, null=True, editable=False)
    staff_in_charge = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='emails_trying_to_reply',
                                        help_text='A staff ID who tries to reply this email',
                                        on_delete=models.PROTECT)
    notifications_have_done = models.BooleanField(
        default=False,
        help_text='A flag that indicates whether this email has already processed'
                  ' by a batch process related to email notification.'
    )

    @property
    def to_values(self):  # Return a list of To addresses, including display name. (Original value)
        if self.to_header:
            return [to.strip() for to in self.to_header.split(',')]
        return []

    @property
    def reply_to_values(self):  # Return a list of Reply-to addresses, including display name. (Original value)
        if self.reply_to_header:
            return [reply_to.strip() for reply_to in self.reply_to_header.split(',')]
        return []

    @property
    def cc_values(self):  # Return a list of CC addresses, including display name. (Original value)
        if self.cc_header:
            return [cc.strip() for cc in self.cc_header.split(',')]
        return []

    def save(self, *args, **kwargs):
        raw_digest = str(self.from_address) + str(self.subject) + str(self.text) + str(self.sent_date) + str(self.to_header)
        self.message_digest = hashlib.sha256(raw_digest.encode('utf-8')).hexdigest()
        super().save(*args, **kwargs)

    @property
    def editable(self):
        return not self.sent_date

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['message_digest'], condition=Q(deleted_at=None), name='uniq_message_digest'),
        ]
        indexes = [
            models.Index(fields=['company'], name='stf_email_company_idx'),
            models.Index(fields=['-sent_date', 'company'], name='stf_email_sent_date_idx'),
            models.Index(fields=['from_name'], name='stf_email_from_name_idx'),
            models.Index(fields=['subject'], name='stf_email_subject_idx'),
        ]

    def __str__(self):
        return self.subject + '@' + str(self.sent_date)

    def to_dictionary(self):
        attachments_qs = self.attachments.all()
        comments_qs = self.emailcomment_set.all()
        shared_history_qs = self.shared_history.all()
        return {
            "id": str(self.id),
            "company": str(self.company.id),
            "created_user": str(self.created_user.id) if self.created_user else None,
            "created_time": str(self.created_time),
            "modified_user": str(self.modified_user.id) if self.modified_user else None,
            "modified_time": str(self.modified_time),
            "message_id": self.message_id,
            "direction": self.direction,
            "from_header": self.from_header,
            "reply_to_header": self.reply_to_header,
            "from_name": self.from_name,
            "from_address": self.from_address,
            "to_header": self.to_header,
            "cc_header": self.cc_header,
            "subject": self.subject,
            "text": self.text,
            "html": self.html,
            "sent_date": str(self.sent_date),
            "has_attachments": self.has_attachments,
            "category": self.category,
            "estimated_category": self.estimated_category,
            "message_digest": self.message_digest,
            "staff_in_charge": str(self.staff_in_charge.id) if self.staff_in_charge else None,
            "notifications_have_done": self.notifications_have_done,
            "attachments": [item.to_dictionary() for item in attachments_qs] if attachments_qs.exists() else [],
            "comments": [item.to_dictionary() for item in comments_qs] if comments_qs.exists() else [],
            "shared_history": [item.to_dictionary() for item in shared_history_qs] if shared_history_qs.exists() else []
        }


# A callback function for EmailAttachment
def generate_upload_path(instance, filename):
    """Generate file path
    WARNING: This rule only works when creating new FileField.
    If you copy a file field from another instance, this rule will not be applied.

    :param instance: An instance trying to create.
    :param filename: An original file name of the attachment.
    :return: A relative file path from MEDIA_ROOT.
    :rtype: str
    """
    return '{base}/{category}/{email_id}/{filename}'.format(
        base=settings.MAIL_ATTACHMENT_DIR, category=instance.category, email_id=instance.email_id, filename=filename
    )


class EmailAttachment(PerCompanyModel, SoftDeleteObject, OrderIdMixin,):
    email = models.ForeignKey('Email', on_delete=models.CASCADE, related_name='attachments')  # This is not message ID so that we can save drafts.
    name = models.CharField(max_length=1024)
    category = models.CharField(max_length=8, choices=ATTACHMENT_CATEGORIES)
    file = models.FileField(max_length=255, upload_to=generate_upload_path)

    class Meta:
        indexes = [
            models.Index(fields=['email'], name='stf_attachment_mail_idx')
        ]

    def clean(self):
        check_fields_consistency([self.email.company.id, self.company.id])

    def __str__(self):
        return f'{self.name} ===> {self.email.subject}'

    def to_dictionary(self):
        return {
            "id": str(self.id),
            "company": str(self.company.id),
            "created_user": str(self.created_user.id) if self.created_user else None,
            "created_time": str(self.created_time),
            "modified_user": str(self.modified_user.id) if self.modified_user else None,
            "modified_time": str(self.modified_time),
            "name": self.name,
            "category": self.category,
            "file": self.file.path,
        }


class ScheduledEmailStatus(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='sm_status_name_idx'),
            models.Index(fields=['order'], name='sm_status_order_idx'),
        ]


class ScheduledEmailSendType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='sm_send_type_name_idx'),
            models.Index(fields=['order'], name='sm_send_type_order_idx'),
        ]


class ScheduledEmail(PerCompanyModel, SoftDeleteObject):
    """A model that contains specific fields for Scheduled Email"""
    sender = models.ForeignKey('User', on_delete=models.PROTECT)
    subject = models.TextField(max_length=255, help_text='Subject of the email.', blank=True, null=True,db_index=True)
    text = models.TextField(help_text='Message text of the email.', blank=True, null=True)
    status = models.CharField(max_length=8, choices=MAIL_STATUS,db_index=True)
    date_to_send = models.DateTimeField(blank=True, null=True, help_text='A date to send the email (JST)',db_index=True)
    send_copy_to_sender = models.BooleanField(
        default=False, help_text='If True, this app sends a copy of the email to the sender.'
    )
    sent_date = models.DateTimeField(blank=True, null=True, help_text='A date to sent the email (JST)',db_index=True)
    send_total_count = models.IntegerField(null=True, help_text='配信数')
    send_success_count = models.IntegerField(null=True, help_text='成功')
    send_error_count = models.IntegerField(null=True, help_text='失敗')
    send_type = models.CharField(blank=True, null=True, max_length=16, choices=SEND_TYPE, help_text='配信種別',db_index=True)
    text_format = models.CharField(blank=True, null=True, max_length=8, choices=TEXT_FORMAT, default=TEXT,db_index=True)
    open_count = models.IntegerField(help_text='開封数', default=0)
    send_copy_to_share = models.BooleanField(default=False)

    scheduled_email_status = models.ForeignKey(ScheduledEmailStatus, on_delete=models.PROTECT, null=True)
    scheduled_email_send_type = models.ForeignKey(ScheduledEmailSendType, on_delete=models.PROTECT, null=True)
    password = models.CharField(max_length=255, null=True, blank=True, default=None)
    expired_date = models.DateTimeField(blank=True, null=True, default=None)
    file_type = models.PositiveSmallIntegerField(default=ScheduledEmailSettingFileStatus.URL, blank=True, null=True)

    @property
    def editable(self):
        return not self.sent_date

    @property
    def total_byte_size(self):
        total_byte_size = 0

        if self.subject:
            total_byte_size += len(self.subject.encode('utf-8'))

        if self.text:
            total_byte_size += len(self.text.encode('utf-8'))

        for attachment in self.attachments.all():
            total_byte_size += attachment.file.size

        return total_byte_size

    @property
    def scheduled_email_status_name(self):
        if self.scheduled_email_status:
            return self.scheduled_email_status.name
        else:
            return ''

    @property
    def scheduled_email_send_type_name(self):
        if self.scheduled_email_send_type:
            return self.scheduled_email_send_type.name
        else:
            return ''

    def clean(self):
        check_fields_consistency([self.sender.company.id, self.company.id])

    def __str__(self):
        return f'件名:{self.subject} || 送信主:{self.sender.username} || 送信予定時刻:{self.date_to_send}'

    class Meta:
        verbose_name = '配信メール'
        indexes = [
            models.Index(fields=['sender'], name='stf_schedMail_sender_idx'),
            models.Index(fields=['company'], name='stf_schedMail_company_idx'),
            models.Index(fields=['-date_to_send', 'company'], name='stf_schedMail_dateToSend_idx'),
            models.Index(fields=['status'], name='stf_schedMail_status_idx'),
            models.Index(fields=['send_type'], name='stf_schedMail_send_type_idx'),
        ]


# A callback function for ScheduledEmailAttachment
def generate_upload_path_of_scheduled_mail_attachment(instance, filename):
    """Generate file path
    WARNING: This rule only works when creating new FileField.
    If you copy a file field from another instance, this rule will not be applied.
    :param instance: An instance trying to create.
    :param filename: An original file name of the attachment.
    :return: A relative file path from MEDIA_ROOT.
    :rtype: str
      """
    return '{base}/scheduled_email/{email_id}/{filename}'.format(
        base=settings.MAIL_ATTACHMENT_DIR, email_id=instance.email.id, filename=filename
    )


class ScheduledEmailAttachment(OrderIdMixin, PerCompanyModel, SoftDeleteObject):
    email = models.ForeignKey('ScheduledEmail', on_delete=models.CASCADE, related_name='attachments')
    name = models.CharField(max_length=255, null=True)
    file = models.FileField(max_length=255, upload_to=generate_upload_path_of_scheduled_mail_attachment)
    size = models.PositiveIntegerField(default=0)
    gcp_link = models.CharField(max_length=255, null=True, blank=True, default=None)
    status = models.PositiveSmallIntegerField(null=True, blank=True, default=AttachmentFileStatus.AVAILABLE, db_index=True)
    file_type = models.PositiveSmallIntegerField(default=ScheduledEmailSettingFileStatus.URL, blank=True, null=True)

    def __str__(self):
        file_name = os.path.basename(self.file.name)
        return f'{file_name} => 件名:{self.email.subject} || 送信主:{self.email.sender.username} || 送信予定時刻:{self.email.date_to_send}'

    class Meta:
        indexes = [
            models.Index(fields=['email'], name='stf_SEm_attach_e_idx'),
        ]


class ScheduledEmailTarget(PerCompanyModel):
    email = models.ForeignKey('ScheduledEmail', on_delete=models.CASCADE, related_name='targets')
    contact = models.ForeignKey('Contact', help_text='From address of an email.', on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.contact.display_name}<{self.contact.email}> on' \
            f' 件名:{self.email.subject} || 送信主:{self.email.sender.username} || 送信予定時刻:{self.email.date_to_send}'

    def clean(self):
        check_fields_consistency([self.email.company.id, self.contact.company.id])

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['email', 'contact'], condition=Q(deleted_at=None), name='uniq_email_target_assignment')
        ]
        indexes = [
            models.Index(fields=['email'], name='stf_SEm_target_e_idx'),
        ]


class ScheduledEmailSearchCondition(OneToOneModel, SoftDeleteObject):
    email = models.OneToOneField('ScheduledEmail', on_delete=models.CASCADE, related_name='searchcondition', primary_key=True)
    search_condition = models.TextField(help_text='Search condition of targets of the email.')

    def __str__(self):
        return f'検索条件 on' \
            f' 件名:{self.email.subject} || 送信主:{self.email.sender.username} || 送信予定時刻:{self.email.date_to_send}'


class SharedEmailSetting(OneToOneModel, SoftDeleteObject):
    company = models.OneToOneField('Company', on_delete=models.CASCADE, related_name='sharedemailsetting', primary_key=True)
    allow_self_domain = models.BooleanField(default=True)

    def __str__(self):
        return f'共有メール設定 on {self.company.name}'


# class ScheduledEmailSentResult(models.Model):
#     target = models.ForeignKey('ScheduledEmailTarget', on_delete=models.CASCADE)
#     result = models.CharField('')

class EmailComment(CommentBaseModel, SoftDeleteObject):
    email = models.ForeignKey(Email, on_delete=models.CASCADE)

    def clean(self):
        check_fields_consistency([self.email.company.id, self.company.id])

    def __str__(self):
        return f'"{self.content}" on {self.email.message_id} at {self.created_time} by {self.created_user}'

    def to_dictionary(self):
        return {
            "id": str(self.id),
            "company": str(self.company.id),
            "created_user": str(self.created_user.id) if self.created_user else None,
            "created_time": str(self.created_time),
            "modified_user": str(self.modified_user.id) if self.modified_user else None,
            "modified_time": str(self.modified_time),
            "content": self.content,
            "is_important": self.is_important,
        }

    class Meta:
        indexes = [
            models.Index(fields=['email'], name='stf_emailComment_email_idx'),
        ]


class ScheduledEmailOpenHistory(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.ForeignKey('ScheduledEmail', on_delete=models.CASCADE, related_name='open_history')
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE, related_name='open_history')
    opened_time = models.DateTimeField(blank=True, null=True, help_text='A date to sent the email (JST)', auto_now_add=True)

    def __str__(self):
        return f'開封履歴 on' \
            f' 件名:{self.email.subject} || 送信主:{self.email.sender.username} || 担当者名:{self.contact.display_name}'

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['email', 'contact'], condition=Q(deleted_at=None), name='uniq_email_open_history')
        ]
        indexes = [
            models.Index(fields=['opened_time'], name='openHistory_opened_time_idx'),
        ]
        verbose_name = "配信メール開封者"


class SharedEmailTransferHistory(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.ForeignKey(Email, on_delete=models.CASCADE, related_name="shared_history")
    sender = models.ForeignKey('User', on_delete=models.PROTECT, related_name="shared_email_sender_history")
    receiver = models.ForeignKey('User', on_delete=models.PROTECT, related_name="shared_email_receiver_history")
    transfer_date = models.DateTimeField(blank=True, null=True, help_text='A date to sent the email (JST)', auto_now_add=True)

    def __str__(self):
        return self.email.subject

    def to_dictionary(self):
        return {
            "id": str(self.id),
            "sender": str(self.sender.id),
            "receiver": str(self.receiver.id),
            "transfer_date": str(self.transfer_date) if self.transfer_date else None,
        }


class ScheduledEmailSendError(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.ForeignKey('ScheduledEmail', on_delete=models.CASCADE, related_name='send_errors')
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE, related_name='send_errors')

    def __str__(self):
        return f'送信エラー on' \
            f' 件名:{self.email.subject} || 送信主:{self.email.sender.username} || 担当者名:{self.contact.display_name}'

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['email', 'contact'], condition=Q(deleted_at=None), name='uniq_email_error')
        ]


class ScheduledEmailSetting(OneToOneModel, SoftDeleteObject):
    company = models.OneToOneField('Company', on_delete=models.CASCADE, related_name='scheduledemailsetting', primary_key=True)
    use_open_count = models.BooleanField(default=False)
    use_remove_promotion = models.BooleanField(default=False)
    use_attachment_max_size = models.BooleanField(default=False)
    use_open_count_extra_period = models.BooleanField(default=False)
    file_type = models.PositiveSmallIntegerField(default=ScheduledEmailSettingFileStatus.URL, blank=True, null=True)

    def __str__(self):
        return f'配信メール設定 on {self.company.name}'
