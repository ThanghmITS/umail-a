from uuid import uuid4
from django.db import models

from django.conf import settings
from app_staffing.models.base import PerCompanyModel, OneToOneModel

from softdelete.models import SoftDeleteObject
from django.db.models.query_utils import Q

KEYWORD = 'keyword'
ATTACHEMENT = 'attachment'

FROM_ADDRESS = 'from_address'
SUBJECT = 'subject'
CONTENT = 'content'

INCLUDE = 'include'
EXCLUDE = 'exclude'
EQUAL = 'equal'
NOT_EQUAL = 'not_equal'
START_WITH = 'start_with'
END_WITH = 'end_with'
EXISTS = 'exists'
NOT_EXISTS = 'not_exists'

ALL = 'all'
ANY = 'any'

EXTRACTION_TYPES = (
    (KEYWORD, 'keyword'),
    (ATTACHEMENT, 'attachment'),
)

TARGET_TYPES = (
    (FROM_ADDRESS, 'from_address'),
    (SUBJECT, 'subject'),
    (CONTENT, 'content'),
    (ATTACHEMENT, 'attachment'),
)

CONDITION_TYPES = (
    (INCLUDE, 'include'),
    (EXCLUDE, 'exclude'),
    (EQUAL, 'equal'),
    (NOT_EQUAL, 'not_equal'),
    (START_WITH, 'start_with'),
    (END_WITH, 'end_with'),
    (EXISTS, 'exists'),
    (NOT_EXISTS, 'not_exists'),
)

MASTER_RULES = (
    (ALL, 'all'),
    (ANY, 'any'),
)


class EmailNotificationRule(PerCompanyModel, SoftDeleteObject):
    # Note: Actually, this model is being treated as per user model in views and services.
    name = models.CharField(max_length=64, help_text='ルールの名前')
    master_rule = models.CharField(max_length=8, choices=MASTER_RULES, blank=True, null=True)
    is_active = models.BooleanField(default=False, null=False)

    class Meta:
        indexes = [
            models.Index(fields=['created_user'], name='stf_email_notify_usr_idx')
        ]


class EmailNotificationCondition(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    rule = models.ForeignKey(EmailNotificationRule, on_delete=models.CASCADE, related_name='conditions')
    order = models.PositiveSmallIntegerField(
        help_text='Store the original position of conditions not to lose its order when Saving/loading from a Database.'
    )
    extraction_type = models.CharField(max_length=16, choices=EXTRACTION_TYPES)
    target_type = models.CharField(max_length=16, choices=TARGET_TYPES)
    condition_type = models.CharField(max_length=16, choices=CONDITION_TYPES)
    value = models.CharField(max_length=128)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['rule', 'order'], condition=Q(deleted_at=None), name='uniq_eml_notify_condition_order_per_rule')
        ]
        indexes = [
            models.Index(fields=['rule'], name='stf_email_notifyCdt_rule_idx')
        ]

class SystemNotification(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=256)
    order = models.IntegerField(unique=True)
    release_time = models.DateTimeField()
    url = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return f"{self.title}"

    class Meta:
        indexes = [
            models.Index(fields=['order'], name='sys_notify_order_idx'),
        ]

class UserSystemNotification(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='systemnotifications')
    system_notification = models.ForeignKey(SystemNotification, on_delete=models.CASCADE, related_name='checkedusers')

    def __str__(self):
        return f'{self.user.display_name} @ {self.system_notification.title} の閲覧履歴'

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'system_notification'], condition=Q(deleted_at=None), name='uniq_user_sys_notify'),
        ]
        indexes = [
            models.Index(fields=['user'], name='usersysnotify_user_idx'),
        ]
