from copy import deepcopy
from uuid import uuid4

from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models.query_utils import Q
from softdelete.models import SoftDeleteObject

from app_staffing.models.base import CommentBaseModel, PerCompanyModel, check_fields_consistency, OneToOneModel, AutoUserFieldsMixin
from app_staffing.models.preferences import ContactPreference

STAFF_STATUS = (
    ('active', 'active'),
    ('inactive', 'inactive'),
    ('retired', 'retired'),
)

PROSPECTIVE = 'prospective'
APPROACHED = 'approached'
INFORMATION_EXCHANGED = 'exchanged'
CLIENT = 'client'

ORGANIZATION_CATEGORIES = (
    (PROSPECTIVE, 'prospective'),  # 見込み客
    (APPROACHED, 'approached'),  # アプローチ済
    (INFORMATION_EXCHANGED, 'exchanged'),  # 情報交換済み
    (CLIENT, 'client'),  # 契約実績有
)

VERY_LOW = 'very_low'
LOW = 'low'
MIDDLE = 'middle'
SEMI_MIDDLE = 'semi_middle'
HIGH = 'high'
VERY_HIGH = 'very_high'

EMPLOYEE_NUMBER_CATEGORIES = (
    (VERY_LOW, 'very_low'),
    (LOW, 'low'),
    (MIDDLE, 'middle'),
    (SEMI_MIDDLE, 'semi_middle'),
    (HIGH, 'high'),
    (VERY_HIGH, 'very_high')
)

TAG_VALUE_MAX_LENGTH = 50
SINGLETON_ID = '323d0e3d-61a2-4cb1-b094-4338c90aa95a'


HEART = 'heart'
FROWN = 'frown'

CONTACT_CATEGORIES = (
    (HEART, 'heart'),
    (FROWN, 'frown'),
)

JP = 'JP'
CN = 'CN'
KR = 'KR'
OTHER = 'OTHER'

TAG_COLORS = (
    ('default', 'Default'),
    ('geekblue', 'Geek Blue'),
    ('orange', 'Orange'),
    ('green', 'Green'),
    ('brown', 'Brown'),
    ('purple', 'Purple'),
    ('yellow', 'Yellow'),
    ('volcano', 'Volcano'),
    ('cyan', 'Cyan'),
    ('pink', 'Pink'),
    ('gray', 'Gray'),
)


class Company(AutoUserFieldsMixin, SoftDeleteObject, models.Model):
    """A model that stores company info.
    このアプリを利用する会社の情報を格納するためのモデル。
    """
    # 自社情報
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=128, blank=True, null=True, help_text='このAppを利用する会社名')
    domain_name = models.CharField(max_length=64, blank=True, null=True, help_text='会社のURL')
    address = models.CharField(max_length=256, blank=True, null=True, help_text='会社の本社住所')
    building = models.CharField(max_length=256, blank=True, null=True, help_text='建物')
    capital_man_yen = models.BigIntegerField(blank=True, null=True, help_text='資本金(万円)', validators=[MinValueValidator(0)])
    establishment_date = models.DateField(blank=True, null=True, help_text='会社設立日 (JST)')
    settlement_month = models.IntegerField(blank=True, null=True, help_text='決済期')
    has_p_mark_or_isms = models.BooleanField(default=False, help_text='自社がPマークまたはISMSのどちらかを保有しているかどうか)')
    has_invoice_system = models.BooleanField(default=False, help_text='自社が適格請求書発行事業者を保有しているかどうか)')
    has_haken = models.BooleanField(default=False, help_text='自社が労働者派遣事業資格保有かどうか')
    has_distribution = models.BooleanField(default=False, help_text='労働者派遣事業資格保有してない場合、商流を抜けるかどうか')
    employee_number = models.CharField(max_length=12, blank=True, choices=EMPLOYEE_NUMBER_CATEGORIES, help_text='社員数')

    # 取引条件
    capital_man_yen_required_for_transactions = models.BigIntegerField(blank=True, null=True, help_text='取引に要する資本金(万円)', validators=[MinValueValidator(0)])
    establishment_year = models.PositiveIntegerField(blank=True, null=True, help_text='設立年数')
    p_mark_or_isms = models.BooleanField(default=False, help_text='PマークまたはISMSのどちらかを保有しているかどうか)')
    invoice_system = models.BooleanField(default=False, help_text='適格請求書発行事業者を保有しているかどうか')
    haken = models.BooleanField(default=False, help_text='労働者派遣事業資格保有かどうか')

    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='created_%(class)s')
    created_time = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    modified_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='modified_%(class)s')
    modified_time = models.DateTimeField(auto_now=True, editable=False, null=True)

    deactivated_time = models.DateTimeField(editable=True, null=True, blank=True)

    def __str__(self):
        return f"{self.name} : {self.domain_name}"


class CompanyAttribute(PerCompanyModel, SoftDeleteObject):
    user_registration_limit = models.IntegerField()
    trial_expiration_date = models.DateField(editable=True, null=True, blank=True)
    tenant_register_current_step = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f"{self.company.name} attributes"


class OrganizationCountry(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='org_country_name_idx'),
            models.Index(fields=['order'], name='org_country_order_idx'),
        ]


class OrganizationEmployeeNumber(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='org_employee_number_name_idx'),
            models.Index(fields=['order'], name='org_employee_number_order_idx'),
        ]


class OrganizationCategory(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='org_category_name_idx'),
            models.Index(fields=['order'], name='org_category_order_idx'),
        ]


class Organization(PerCompanyModel, SoftDeleteObject):
    """A model that describes organizations, including prospectives.
    見込み客を含む, 会社を定義するモデル
    """
    corporate_number = models.BigIntegerField(
        help_text='法人番号 (外国の取引先などは空白となる場合あり)',
        null=True,
        verbose_name='法人番号',
    )
    name = models.CharField(max_length=128, help_text='会社名')
    category = models.CharField(max_length=16, choices=ORGANIZATION_CATEGORIES, help_text='会社の分類')
    employee_number = models.CharField(max_length=12, blank=True, choices=EMPLOYEE_NUMBER_CATEGORIES, help_text='社員数')
    contract = models.BooleanField(null=True)
    country = models.CharField(max_length=8, help_text='会社の国籍')
    address = models.CharField(max_length=256, blank=True, null=True, help_text='会社の本社住所')
    building = models.CharField(max_length=256, blank=True, null=True, help_text='建物')
    domain_name = models.CharField(max_length=64, blank=True, null=True, help_text='会社のURL')
    capital_man_yen = models.BigIntegerField(blank=True, null=True, help_text='資本金(万円)', validators=[MinValueValidator(0)])
    establishment_date = models.DateField(blank=True, null=True, help_text='会社設立日 (JST)')
    settlement_month = models.IntegerField(blank=True, null=True, help_text='決算期')
    has_p_mark_or_isms = models.BooleanField(null=True)
    has_invoice_system = models.BooleanField(null=True)
    has_haken = models.BooleanField(null=True)
    has_distribution = models.BooleanField(null=True)
    score = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], help_text='取引先評価')
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    tel3 = models.CharField(max_length=15, blank=True, null=True)
    fax1 = models.CharField(max_length=15, blank=True, null=True)
    fax2 = models.CharField(max_length=15, blank=True, null=True)
    fax3 = models.CharField(max_length=15, blank=True, null=True)

    # その他
    capital_man_yen_required_for_transactions = models.BigIntegerField(blank=True, null=True, help_text='取引に要する資本金(万円)', validators=[MinValueValidator(0)])
    establishment_year = models.PositiveIntegerField(blank=True, null=True, help_text='設立年数')
    is_blacklisted = models.BooleanField(default=False, help_text='ブロックリスト扱いかどうか')
    old_id = models.IntegerField(null=True, help_text='An unique ID on Old System.')
    p_mark_or_isms = models.BooleanField(default=False, help_text='PマークまたはISMSのどちらかを保有しているかどうか)')
    invoice_system = models.BooleanField(default=False, help_text='適格請求書発行事業者を保有しているかどうか')
    haken = models.BooleanField(default=False, help_text='労働者派遣事業資格保有かどうか')

    organization_country = models.ForeignKey(OrganizationCountry, on_delete=models.PROTECT, null=True)
    organization_employee_number = models.ForeignKey(OrganizationEmployeeNumber, on_delete=models.PROTECT, null=True)
    organization_category = models.ForeignKey(OrganizationCategory, on_delete=models.PROTECT, null=True)

    @property
    def organization_country_name(self):
        if self.organization_country:
            return self.organization_country.name
        else:
            return ''

    @property
    def organization_employee_number_name(self):
        if self.organization_employee_number:
            return self.organization_employee_number.name
        else:
            return ''

    @property
    def organization_category_name(self):
        if self.organization_category:
            return self.organization_category.name
        else:
            return ''

    @property
    def organization_hash(self):
        hash = "${0}-${1}-${2}-${3}".format(self.corporate_number, self.name, self.address, self.building)
        return hash

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '取引先'
        constraints = [
            models.UniqueConstraint(fields=['corporate_number', 'company'], condition=Q(deleted_at=None), name='uniq_coop_number_per_company'),
            models.UniqueConstraint(fields=['old_id'], condition=Q(deleted_at=None), name='uniq_old_id_per_organizations')  # For migrated data.
            # It must be None for resources created on this app.
        ]
        indexes = [
            models.Index(fields=['name'], name='stf_organization_name_idx'),
            models.Index(fields=['company'], name='stf_organization_company_idx'),
            models.Index(fields=['employee_number'], name='stf_organization_e_number_idx'),
            models.Index(fields=['-created_time', 'company'], name='stf_organization_cTime_idx'),
            models.Index(fields=['tel1'], name='stf_organization_tel1_idx'),
            models.Index(fields=['tel2'], name='stf_organization_tel2_idx'),
            models.Index(fields=['tel3'], name='stf_organization_tel3_idx'),
            models.Index(fields=['fax1'], name='stf_organization_fax1_idx'),
            models.Index(fields=['fax2'], name='stf_organization_fax2_idx'),
            models.Index(fields=['fax3'], name='stf_organization_fax3_idx'),
            models.Index(fields=['has_p_mark_or_isms'], name='stf_organization_has_poi_idx'),
            models.Index(fields=['has_invoice_system'], name='stf_organization_has_i_sys_idx'),
            models.Index(fields=['has_haken'], name='stf_organization_has_haken_idx'),
        ]


class OrganizationComment(CommentBaseModel, SoftDeleteObject):
    """A model that describes comment related to a organization.
    会社と1対0..nの関係にある、コメントモデル。会社に対する履歴を残すことができる。
    """
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

    def clean(self):
        check_fields_consistency([self.organization.company.id, self.company.id])

    def __str__(self):
        return f'"{self.content}" on {self.organization.name} at {self.created_time} by {self.created_user}'

    class Meta:
        indexes = [
            models.Index(fields=['organization'], name='stf_orgComment_org_idx'),
        ]


class OrganizationBranch(PerCompanyModel, SoftDeleteObject):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='branches')
    name = models.CharField(max_length=128, help_text='支店名')
    address = models.CharField(max_length=256, blank=True, null=True, help_text='支店住所')
    building = models.CharField(max_length=256, blank=True, null=True, help_text='支店建物名')
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    tel3 = models.CharField(max_length=15, blank=True, null=True)
    fax1 = models.CharField(max_length=15, blank=True, null=True)
    fax2 = models.CharField(max_length=15, blank=True, null=True)
    fax3 = models.CharField(max_length=15, blank=True, null=True)

    def __str__(self):
        return f'{self.organization.name} {self.name}'

    class Meta:
        indexes = [
            models.Index(fields=['organization'], name='stf_OrgBranch_org_idx'),
        ]


class Tag(PerCompanyModel, SoftDeleteObject):
    value = models.CharField(max_length=TAG_VALUE_MAX_LENGTH, help_text='タグの内容')
    internal_value = models.CharField(max_length=TAG_VALUE_MAX_LENGTH, help_text='タグの内部値(検索用, 小文字統一)', editable=False)
    color = models.CharField(max_length=8, choices=TAG_COLORS, default='default')
    is_skill = models.BooleanField(default=False)

    @staticmethod
    def to_internal_value(value):
        return value.lower()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['value', 'company'], condition=Q(deleted_at=None), name='uniq_tag_value_per_company')
        ]
        indexes = [
            models.Index(fields=['value'], name='stf_Tag_value_idx'),
            models.Index(fields=['internal_value'], name='stf_Tag_internalVal_idx'),
            models.Index(fields=['color'], name='stf_Tag_color_idx'),
            models.Index(fields=['company'], name='stf_Tag_company_idx'),
            models.Index(fields=['-created_time', 'company'], name='stf_Tag_cTime_idx'),
        ]

    def __str__(self):
        return self.value

    def save(self, *args, **kwargs):
        self.internal_value = self.to_internal_value(self.value)
        super().save(*args, **kwargs)


class Contact(PerCompanyModel, SoftDeleteObject):
    """A model that describes contacts.
    会社と1対0..nの関係にある、取引先担当者のモデル
    Note: 厳密には、１人の担当者が複数の会社に勤務するケースもあり得るが、
    メールアドレスが異なっており、別扱いにして問題がないため、1対多の関係とする。
    """

    last_name = models.CharField(max_length=128, help_text='取引先担当者姓', blank=True, null=True,)
    first_name = models.CharField(blank=True, null=True, max_length=128, help_text='取引先担当者名')
    email = models.EmailField(verbose_name='Email')
    position = models.CharField(max_length=256, blank=True, null=True, help_text='役職')
    department = models.CharField(max_length=256, blank=True, null=True, help_text='部門')
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT)
    organization_branch = models.ForeignKey(OrganizationBranch, blank=True, null=True, on_delete=models.PROTECT)
    staff = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.PROTECT)  # Staff will be marked as inactive or retired instead
    last_visit = models.DateField(blank=True, null=True, help_text='最終商談日 (JST)')
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    tel3 = models.CharField(max_length=15, blank=True, null=True)

    old_id = models.IntegerField(null=True, help_text='An unique ID on Old System.')

    # Assignment of related manager
    tags = models.ManyToManyField(to=Tag, through='ContactTagAssignment', related_name='assigned_contacts')

    @property
    def display_name(self):
        if self.last_name and self.first_name:
            return self.last_name + ' ' + self.first_name
        else:
            return self.last_name

    def active_tags(self):
        return self.tags.filter(contact_assignment__deleted_at__isnull=True, contact_assignment__contact_id=self.id).order_by('created_time')

    def clean(self):
        check_fields_consistency([self.organization.company.id, self.company.id])
        if self.staff:
            check_fields_consistency([self.staff.company.id, self.company.id])

    def save(self, *args, **kwargs):
        creating = deepcopy(self._state.adding)
        super().save(*args, **kwargs)
        if creating:
            ContactPreference(contact=self, company=self.company).save()  # Create a preference with default parameters.

        return

    def __str__(self):
        return self.display_name + '@' + self.organization.name

    class Meta:
        verbose_name = '連絡先'
        constraints = [
            models.UniqueConstraint(fields=['email', 'company'], condition=Q(deleted_at=None), name='uniq_contact_email_per_company'),
            models.UniqueConstraint(fields=['old_id'], condition=Q(deleted_at=None), name='uniq_old_id_per_contacts')  # For migrated data.
            # It must be None for resources created on this app.
        ]
        indexes = [
            models.Index(fields=['last_name'], name='stf_Contact_last_name_idx'),
            models.Index(fields=['first_name'], name='stf_Contact_first_name_idx'),
            models.Index(fields=['email'], name='stf_Contact_email_idx'),
            models.Index(fields=['staff'], name='stf_Contact_staff_idx'),
            models.Index(fields=['company'], name='stf_Contact_company_idx'),
            models.Index(fields=['-created_time', 'company'], name='stf_Contact_cTime_idx'),
            models.Index(fields=['position'], name='stf_Contact_position_idx'),
            models.Index(fields=['department'], name='stf_Contact_department_idx'),
            models.Index(fields=['-last_visit', 'company'], name='stf_Contact_last_visit_idx'),
            models.Index(fields=['tel1'], name='stf_Contact_tel1_idx'),
            models.Index(fields=['tel2'], name='stf_Contact_tel2_idx'),
            models.Index(fields=['tel3'], name='stf_Contact_tel3_idx'),
        ]


class ContactCC(PerCompanyModel, SoftDeleteObject):
    """A model that describes the mail CC target per contact.
    Contact毎に登録される同報先を定義するモデル
    """
    email = models.EmailField()
    # temporally allow null to easily register the contact model at the same time.
    contact = models.ForeignKey(Contact, null=True, on_delete=models.CASCADE, related_name='cc_addresses')

    def __str__(self):
        return f'{self.contact.display_name} @ {self.contact.organization.name} s CC -> {self.email}'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_ContactCC_contact_idx'),
        ]


class ContactScore(PerCompanyModel, SoftDeleteObject):
    score = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name='scores')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='contact_favorite_scores')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['contact', 'user'], condition=Q(deleted_at=None), name='uniq_score_per_contact_and_user'),
        ]
        indexes = [
            models.Index(fields=['user', 'contact'], name='stf_CtScore_nPk_idx'),  # nPk = natural Primary keys
        ]

    def clean(self):
        check_fields_consistency([self.contact.company.id, self.user.company.id])

    def __str__(self):
        return f'☆{self.score} on {self.contact.display_name} @ {self.contact.organization.name} by {self.user.username}'


class ContactComment(CommentBaseModel, SoftDeleteObject):
    """A model that describes comment related to a organization.
    連絡先と1対0..nの関係にある、コメントモデル。連絡先に対する履歴を残すことができる。
    """
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    def clean(self):
        check_fields_consistency([self.contact.company.id, self.company.id])

    def __str__(self):
        return f'"{self.content}" on Contact {self.contact.display_name} @ {self.contact.organization.name}' \
            f' at {self.created_time} by {self.created_user}'

    class Meta:
        indexes = [
            models.Index(fields=['contact'], name='stf_CtComment_contact_idx'),
        ]


class ContactTagAssignment(PerCompanyModel, SoftDeleteObject):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name='tag_assignment')
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='contact_assignment')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['contact', 'tag'], condition=Q(deleted_at=None), name='uniq_contact_tag_assignment')
        ]
        indexes = [
            models.Index(fields=['contact', 'tag'], name='stf_CtTagAssign_nPk_idx'),  # nPk = natural Primary keys
        ]

    def __str__(self):
        return f'Tag "{self.tag.value}" is assigned to Contact {self.contact.display_name} @ {self.contact.organization.name}'


class ExceptionalOrganization(PerCompanyModel, SoftDeleteObject):
    """A model that describes organization related to a company.
    自社と1対0..nの関係にある、会社モデル。自社に対する例外取引先を登録することができる。
    """
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.organization.name}" on {self.company.name} at {self.created_time} by {self.created_user}'

    class Meta:
        indexes = [
            models.Index(fields=['company'], name='stf_excepOrg_company_idx'),
        ]


class DisplaySetting(OneToOneModel, SoftDeleteObject):
    company = models.OneToOneField('Company', on_delete=models.CASCADE, related_name='displaysetting', primary_key=True)
    content = models.TextField(help_text='表示項目設定内容(検索項目、検索結果項目、必須項目)')

    def __str__(self):
        return f' 表示項目設定 on {self.company.name}'


class ContactCategory(PerCompanyModel, SoftDeleteObject):
    category = models.CharField(max_length=8, choices=CONTACT_CATEGORIES, help_text='分類')
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, related_name='categories')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='contact_categories')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['contact', 'user'], condition=Q(deleted_at=None), name='uniq_category_per_contact_and_user'),
        ]
        indexes = [
            models.Index(fields=['user', 'contact'], name='stf_CtCategory_nPk_idx'),  # nPk = natural Primary keys
        ]

    def clean(self):
        check_fields_consistency([self.contact.company.id, self.user.company.id])

    def __str__(self):
        return f'カテゴリ : {self.category} on {self.contact.display_name} @ {self.contact.organization.name} by {self.user.username}'


class UserDisplaySetting(OneToOneModel, SoftDeleteObject):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='userdisplaysetting', primary_key=True)
    content = models.TextField(help_text='表示項目設定内容(検索項目、検索結果項目、必須項目)')

    def __str__(self):
        return f' ユーザ表示項目設定 on {self.user.display_name}'
