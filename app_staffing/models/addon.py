import json
from uuid import uuid4

from django.conf import settings
from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.models.base import PerCompanyModel

class AddonMaster(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    price = models.IntegerField()
    is_recommended = models.BooleanField()
    expiration_time = models.DateTimeField(editable=True, null=True, blank=True)
    limit = models.IntegerField()
    order = models.IntegerField(unique=True)
    help_url = models.CharField(max_length=516, blank=True, null=True)

    def __str__(self):
        return f"{self.title}"

    class Meta:
        indexes = [
            models.Index(fields=['order'], name='addon_master_order_idx'),
        ]

class AddonTarget(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['order'], name='addon_target_order_idx'),
        ]

class AddonTargetAssignment(models.Model):
    id = models.IntegerField(primary_key=True)
    addon_master = models.ForeignKey(AddonMaster, on_delete=models.PROTECT, null=True, related_name='target_assignment')
    addon_target = models.ForeignKey(AddonTarget, on_delete=models.PROTECT, null=True, related_name='addon_assignment')

    def __str__(self):
        return f"{self.addon_master.title} @ {self.addon_target.name}"

class AddonParentAssignment(models.Model):
    id = models.IntegerField(primary_key=True)
    addon_master = models.ForeignKey(AddonMaster, on_delete=models.PROTECT, null=True, related_name='parent_assignment')
    parent_addon_master = models.ForeignKey(AddonMaster, on_delete=models.PROTECT, null=True, related_name='child_assignment')

    def __str__(self):
        return f"{self.addon_master.title} @ {self.parent_addon_master.title}"

class Addon(SoftDeleteObject, PerCompanyModel):
    addon_id = models.IntegerField(null=True)
    expiration_time = models.DateTimeField(editable=True, null=True, blank=True)
    addon_master = models.ForeignKey(AddonMaster, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f"{self.addon_master.title} purchased by: {self.company.name}"
