from rest_framework.exceptions import APIException


class TagRegisteredValueException(APIException):
    status_code = 400
    default_detail = '入力したタグ名は、既に登録されています。'
    default_code = 'tag_registered_value'


class CorporateNumberNotUniqueException(APIException):
    status_code = 400
    default_detail = '入力した法人番号は、既に登録されています。'
    default_code = 'corporate_number_not_unique'


class CsvRegisterLimitException(APIException):
    status_code = 400
    default_code = 'corporate_number_not_unique'


class CannotUpdateContactException(APIException):
    status_code = 400
    default_detail = 'この取引先担当者は「配信中」ステータスの配信メールの宛先に含まれているため入力内容を更新することができません。'
    default_code = 'cannot_update_contact_with_email_sending'


class CannotDeleteContactException(APIException):
    status_code = 400
    default_detail = 'この取引先担当者は「配信中」ステータスの配信メールの宛先に含まれているため削除することができません'
    default_code = 'cannot_delete_contact_with_email_sending'
