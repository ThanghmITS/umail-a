from rest_framework.exceptions import APIException

class CannotDeleteException(APIException):
    status_code = 400
    default_detail = 'アイテムの削除に失敗しました。'
    default_code = 'cannnot_delete'

class CannotUpdateException(APIException):
    status_code = 400
    default_detail = 'アイテムの更新に失敗しました。'
    default_code = 'cannnot_update'

class CannotActivateException(APIException):
    status_code = 400
    default_detail = 'アイテムの有効化に失敗しました。'
    default_code = 'cannnot_activate'

class CannotDeactivateException(APIException):
    status_code = 400
    default_detail = 'ユーザーの無効化に失敗しました。'
    default_code = 'cannnot_deactivate'

class CannotDeleteUserException(APIException):
    status_code = 400
    default_detail = 'ユーザーの削除に失敗しました。'
    default_code = 'cannnot_delete_user'
