from rest_framework.exceptions import APIException

class TagRemoveException(APIException):
    status_code = 400
    default_detail = 'タグの削除に失敗しました'
    default_code = 'tag_remove_failed'
