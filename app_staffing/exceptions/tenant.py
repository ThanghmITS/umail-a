from rest_framework.exceptions import APIException


class TenantRegisterPasswordException(APIException):
    status_code = 400
    default_detail = 'パスワードが一致しません。パスワードを正しく入力してください。'
    default_code = 'tenant_register_password'


class TenantFormerMasterUserReRegisteringException(APIException):
    status_code = 400
    default_detail = '入力されたメールアドレスは過去にマスターユーザーとして登録されていたため、登録をすることができません。'
    default_code = 'tenant_former_master_user_re_registering'


class TenantCannotLoginInactiveUserOrWrongPasswordException(APIException):
    status_code = 400
    default_detail = '会員情報の登録に失敗しました。メールアドレスとパスワードを再度お確かめください。'
    default_code = 'tenant_cannot_login_inactive_user_or_Wrong_password'
