import os
import shutil
import pytz
import jpholiday
from django.core.cache import cache

from freezegun import freeze_time

from django.core.files.uploadedfile import SimpleUploadedFile  # For file mocking.

from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APITestCase

from app_staffing.constants.email import ScheduledEmailSettingFileStatus
from app_staffing.exceptions.scheduled_email import ScheduledEmailAlreadySentException, ScheduledEmailPastDateException, \
    ScheduledEmailDeliveryTimeIsNull, EmailDomainBlocked, ScheduledEmailAttachmentNotExist, \
    ScheduledEmailIncorrectPassword
from app_staffing.models import User, ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailStatus, ScheduledEmailSendType, Addon
from app_staffing.models.email import DRAFT, QUEUED, ScheduledEmailSearchCondition, SMTPServer, ScheduledEmailSetting, ScheduledEmailTarget
from app_staffing.tests.factories.company import CompanyFactory

from app_staffing.tests.views.base import APICRUDTestMixin, TestPostErrorMixin, MultiTenancyTestMixin, \
    ActionViewMTMixin, TestQueryParamErrorMixin
from app_staffing.tests.factories.email import create_scheduled_mail_data, ScheduledEmailSettingFactory
from app_staffing.tests.views.validators.scheduled_emails import IsScheduledEmail, IsScheduledEmailSummary, \
    IsScheduledEmailAttachment, IsScheduledEmailPreview
from app_staffing.tests.helpers.setup import MediaFolderSetupMixin
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, NORMAL_USER_NAME, GET_DEFAULT_COMPANY, \
    NORMAL_USER_PASSWORD

import datetime

from django.conf import settings

from unittest.mock import Mock, patch
from django.test import override_settings

from app_staffing.exceptions.validation import CannotDeleteException
from app_staffing.exceptions.base import BulkOperationLimitException, TemplateNameRegisteredException, \
    TemplateRegisterLimitException, TemplateNameBlankException, SenderDoesNotExistException, \
    IsSenderBelongToAnotherCompany
from app_staffing.utils.utils import generate_day_to_date
from app_staffing.exceptions.scheduled_email import ScheduledEmailTargetNotSelectedException

TEST_FILE_PATH = 'app_staffing/tests/__temp__/SendMailAttachment.txt'

TEST_MAIL_ID = '185c2a9b-ec13-4194-bd85-386de64f5ee4'  # This must corresponds to the fixture data.
TEST_MAIL_ATTACHMENT_ID = 'e57a0a67-a005-4cf7-9f26-62d089bfdd32'  # This must corresponds to the fixture data.

TEST_MAIL_SENDER_ADDRESS = 'testMailSender@example.com'
TEST_MAIL_SENDER_FIRST_NAME = 'First Name'
TEST_MAIL_SUBJECT = 'This is a Test Email'
TEST_MAIL_TEXT = 'This is a main content of the email'
TEST_MAIL_ATTACHMENT_BODY = 'file content'
TEST_MAIL_ATTACHMENT_TYPE = 'text/plain'
MAIL_FIXTURE_ATTACHMENT_DIR_PATH = "email/attachments/scheduled_email/185c2a9b-ec13-4194-bd85-386de64f5ee4"
MAIL_FIXTURE_ATTACHMENT_PATH = MAIL_FIXTURE_ATTACHMENT_DIR_PATH + "/sample.txt"


class ScheduledEmailTestDataMixin(object):
    """An Additional Mixin for Multi-tenancy tests of ScheduledEmail """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.email1, cls.attachment1 = create_scheduled_mail_data(
            company=cls.company_1,
            sender_address="sender@tenantA.com",
            sender_first_name="senderA",
            sender_signature="",
            subject="Mail A",
            content="yo",
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.email2, cls.attachment2 = create_scheduled_mail_data(
            company=cls.company_2,
            sender_address="sender@tenantB.com",
            sender_first_name="senderB",
            sender_signature="",
            subject="Mail B",
            content="yo",
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.email1.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.email1.save()
        cls.email2.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.email2.save()


class ScheduledEmailViewTestCase(APICRUDTestMixin, APITestCase, TestQueryParamErrorMixin):
    EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"

    fixtures = [
        'app_staffing/tests/fixtures/payjp/company.json',
        'app_staffing/tests/fixtures/payjp/users.json',
        'app_staffing/tests/fixtures/payjp/emails.json',
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    resource_id = EMAIL_ID
    resource_validator_class = IsScheduledEmail
    resource_validator_class_for_list = IsScheduledEmailSummary

    # Used in TestQueryParamErrorMixin
    filter_params = {
        'sender_id': '185c2a9b-ec13-4194-bd85-386de64f5ee4',
        'subject': 'Dummy subject',
        'status': 'Status A',
        'create_user_id': '185c2a9b-ec13-4194-bd85-386de64f5ee4',
        }

    skip_tests = ('put',)
    update_date_to_send = ''

    @classmethod
    def setUpTestData(cls):
        date_to_send= generate_day_to_date('iso_format')
        date_to_send_utc = generate_day_to_date('utc_format')
        cls.post_test_cases = [
            ('full', {
                "subject": "This is a complete email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Hi",
                "text_format": "text",
                "date_to_send": date_to_send,  # We can also accept UTC format.
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3", "291dc792-d5a0-43fa-a500-e9e2273beaec"],
            }),
            ('minimum', {
                "subject": "This is a draft email",
                "status": "draft",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Hi",
                "text_format": "text",
            })
        ]
        cls.patch_test_cases = [
            ('full', {
                "subject": "An Update Schedule email",
                "status": "draft",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": date_to_send,  # We can also accept UTC format.
                "additional_date_to_send1": "2021-03-16T14:00:00+09:00",
                "additional_date_to_send2": "2021-03-17T14:00:00+09:00",
                "additional_date_to_send3": "2021-03-18T14:00:00+09:00",
                "additional_date_to_send4": "2021-03-19T14:00:00+09:00",
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3", "291dc792-d5a0-43fa-a500-e9e2273beaec"],
                "search_conditions": {"searchtype": "job"},
            }),
            ('UTC time', {
                "subject": "A New Subject",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": date_to_send_utc
            }),
            ('minimum', {
                "subject": "A New Subject",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
            }),
            ('queued', {
                "subject": "An Update Schedule email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": date_to_send,  # We can also accept UTC format.
                "additional_date_to_send1": "2021-03-16T14:00:00+09:00",
                "additional_date_to_send2": "2021-03-17T14:00:00+09:00",
                "additional_date_to_send3": "2021-03-18T14:00:00+09:00",
                "additional_date_to_send4": "2021-03-19T14:00:00+09:00",
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3", "291dc792-d5a0-43fa-a500-e9e2273beaec"],
                "search_conditions": {"searchtype": "personnel"},
            }),
        ]

    # PAYJP_TEST_COMPANY_ID = '3efd25d0-989a-4fba-aa8d-b91708760eb9'

    def setUp(self):
        super().setUp()
        self.update_date_to_send = generate_day_to_date('iso_format')

        os.makedirs(settings.MEDIA_ROOT + '/' + MAIL_FIXTURE_ATTACHMENT_DIR_PATH, exist_ok=True)
        shutil.copy2('app_staffing/tests/files/sample.txt', settings.MEDIA_ROOT + '/' + MAIL_FIXTURE_ATTACHMENT_PATH)

    def test_patch_api(self):
        email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        email_instance.status = 'tasks'
        email_instance.save()

        mock = Mock()
        with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
            super().test_patch_api()
            email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
            self.assertEqual(email_instance.status, '')
            self.assertEqual(mock.delete_task.call_count, 1)

    def test_delete_api(self):
        email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        email_instance.status = 'tasks'
        email_instance.save()

        mock = Mock()
        with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
            super().test_delete_api()
            self.assertEqual(mock.delete_task.call_count, 1)

    def test_patch_api_without_job(self):
        mock = Mock()
        with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
            super().test_patch_api()
            self.assertEqual(mock.delete_task.call_count, 0)

    def test_delete_api_without_job(self):
        email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        email_instance.status = 'tasks'
        email_instance.scheduled_email_status_id = 5
        email_instance.save()

        mock = Mock()
        with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
            super().test_delete_api()
            self.assertEqual(mock.delete_task.call_count, 0)

    def test_edit_dilvery_email_case(self):
        data = {

            "text_format":"text",
            "sender":"6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "subject":"test duplicate",
            "text":"test description duplicate test",
            "send_copy_to_sender":False,
            "target_contacts":[
                    "78630567-72ce-49a3-84e6-8947b5b057f3"
                                ],
            "send_type":"other",
            "search_conditions":{
                        "searchtype":"other"
                        },
            "date_to_send":generate_day_to_date('iso_format'),
            "additional_date_to_send1":"2022-03-10 14:00:00.000+05:30",
            "status":"queued"

        }

        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        email_instance = ScheduledEmail.objects.get(id=response.data["id"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], email_instance.text)
        self.assertEqual(data["text_format"],email_instance.text_format)
        self.assertEqual(data["subject"], email_instance.subject)
        self.assertTrue(ScheduledEmailTarget.objects.filter(email_id=response.data["id"], contact_id='78630567-72ce-49a3-84e6-8947b5b057f3').exists())


    def test_over_max_length(self):
        data = {
            "subject": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "text": """
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                cccあいうえお
            """,
        }
        response = self.client.post(path='/app_staffing/scheduled_mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['subject']), settings.LENGTH_VALIDATIONS['scheduled_email']['subject']['message'])
        self.assertEqual(str(response.data['text']), settings.LENGTH_VALIDATIONS['scheduled_email']['text']['message'])

        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['subject']), settings.LENGTH_VALIDATIONS['scheduled_email']['subject']['message'])
        self.assertEqual(str(response.data['text']), settings.LENGTH_VALIDATIONS['scheduled_email']['text']['message'])

    def test_bulk_delete_limit_error(self):
        data = {'source': [
            'sample_uuid_xxxx_yyyy_1',
            'sample_uuid_xxxx_yyyy_2',
            'sample_uuid_xxxx_yyyy_3',
            'sample_uuid_xxxx_yyyy_4',
            'sample_uuid_xxxx_yyyy_5',
            'sample_uuid_xxxx_yyyy_6',
            'sample_uuid_xxxx_yyyy_7',
            'sample_uuid_xxxx_yyyy_8',
            'sample_uuid_xxxx_yyyy_9',
            'sample_uuid_xxxx_yyyy_10',
            'sample_uuid_xxxx_yyyy_11',
        ]}
        response = self.client.delete(path='/app_staffing/scheduled_mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

    def test_bulk_delete_sending_status(self):
        resource_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
        mail = ScheduledEmail.objects.get(id=resource_id)
        mail.scheduled_email_status_id = 3
        mail.save()
        data = {'source': [resource_id]}
        response = self.client.delete(path='/app_staffing/scheduled_mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeleteException.default_detail)

    @override_settings(SCHEDULED_EMAIL_DEFAULT_LIMIT_TARGET_COUNT=1)
    def test_target_count_limit(self):
        data = {
            "subject": "An Update Schedule email",
            "status": "queued",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "Updated",
            "text_format": "text",
            "date_to_send": self.update_date_to_send,  # We can also accept UTC format.
            "additional_date_to_send1": "2021-03-16T14:00:00+09:00",
            "additional_date_to_send2": "2021-03-17T14:00:00+09:00",
            "additional_date_to_send3": "2021-03-18T14:00:00+09:00",
            "additional_date_to_send4": "2021-03-19T14:00:00+09:00",
            "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3", "291dc792-d5a0-43fa-a500-e9e2273beaec"],
            "search_conditions": {"searchtype": "personnel"},
        }

        # 配信可能の上限値に引っかかって、予約不可
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], settings.SCHEDULED_EMAIL_TARGET_COUNT_LIMIT_ERROR_MESSAGE.format('1'))

        # アドオン購入によって上限緩和され、予約可能になる
        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID)
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delivery_interval(self):
        data = {
            "subject": "An Update Schedule email",
            "status": "queued",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "Updated",
            "text_format": "text",
            "date_to_send": self.update_date_to_send,  # We can also accept UTC format.
            "additional_date_to_send1": "2021-01-14T14:20:00+09:00",
            "additional_date_to_send2": "2021-01-14T14:30:00+09:00",
            "additional_date_to_send3": "2021-01-14T14:40:00+09:00",
            "additional_date_to_send4": "2021-01-14T14:50:00+09:00",
            "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3", "291dc792-d5a0-43fa-a500-e9e2273beaec"],
            "search_conditions": {"searchtype": "personnel"},
        }

        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID)
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_send_limit_is_correct_after_addon_purchase(self):
        # NOTE(joshua-hashimoto): アドオンを2回購入
        email = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        company_id = email.company.id
        addons = Addon.objects.filter(company_id=company_id)
        addon_id = settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID
        Addon.objects.create(company_id=company_id, addon_master_id=addon_id, addon_id=addons.count() + 1)
        Addon.objects.create(company_id=company_id, addon_master_id=addon_id, addon_id=addons.count() + 2)
        url = api_reverse("scheduled_email_detail", kwargs={"pk": self.EMAIL_ID})
        response = self.client.get(url)
        response_data = response.data
        self.assertEqual(response_data["send_limit"], 3000)

    def test_not_null_value(self):
        response = self.client.post(path='/app_staffing/scheduled_mails', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['text_format']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['text']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['subject']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['sender']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])

        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data={}, format='json')
        self.assertEqual(str(response.data['text_format']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['text']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['subject']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['sender']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])

        data = {
            "text_format": None,
            "text": None,
            "subject": None,
            "sender": None,
        }

        response = self.client.post(path='/app_staffing/scheduled_mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['text_format']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['text']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['subject']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['sender']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])

        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(str(response.data['text_format']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['text']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['subject']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])
        self.assertEqual(str(response.data['sender']), settings.NOT_NULL_VALIDATIONS['scheduled_email']['message'])

    def test_target_not_selected(self):
        scheduled_email_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

        scheduled_email = ScheduledEmail.objects.get(id=scheduled_email_id)
        scheduled_email.scheduled_email_status_id = 2 # 配信待ち
        scheduled_email.save()

        data = {
            "subject": "An Update Schedule email",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "Updated",
            "text_format": "text",
            "date_to_send": self.update_date_to_send,
            "target_contacts": [], # 宛先件数を0件で更新
            "search_conditions": {"searchtype": "personnel"},
        }

        response = self.client.patch(path=f'/app_staffing/scheduled_mails/{scheduled_email_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], ScheduledEmailTargetNotSelectedException.default_detail)


class ScheduledEmailViewErrorTestCase(TestPostErrorMixin, APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]

    base_url = '/app_staffing/scheduled_mails'  # Override by child class.

    update_date_to_send = ''

    post_test_cases = [
        (
            'invalid target format',
            {
                "subject": "This is a complete email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Hi",
                "text_format": "text",
                "date_to_send": "2019-05-15T05:00:00Z",
                "target_contacts": [1],  # This.
            },
            ["target_contacts"]
        ),
        (
            'specifying non existing target',
            {
                "subject": "This is a complete email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Hi",
                "text_format": "text",
                "date_to_send": "2019-05-15T05:00:00Z",
                "target_contacts": ["12345678-d5a0-43fa-a500-e9e2273beaec"]  # This.
            },
            ["target_contacts"]
        ),
        (
            'email exists at the same time ',
            {
                "subject": "This is a complete email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Hi",
                "text_format": "text",
                "date_to_send": "2019-05-14T05:58:46Z",  # This.
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"]
            },
            []
        ),
        (
            'holiday', {
                "subject": "An Update Schedule email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": "2021-03-14T14:00:00+09:00",  # We can also accept UTC format.
                "additional_date_to_send1": "2021-03-14T14:00:00+09:00",
                "additional_date_to_send2": "2021-03-14T14:00:00+09:00",
                "additional_date_to_send3": "2021-03-14T14:00:00+09:00",
                "additional_date_to_send4": "2021-03-14T14:00:00+09:00",
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"],
            },
            []
        ),
        (
            'not worktime', {
                "subject": "An Update Schedule email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": "2019-03-15T00:00:00+09:00",  # We can also accept UTC format.
                "additional_date_to_send1": "2021-03-16T04:00:00+09:00",
                "additional_date_to_send2": "2021-03-17T07:00:00+09:00",
                "additional_date_to_send3": "2021-03-18T21:00:00+09:00",
                "additional_date_to_send4": "2021-03-19T20:00:00+09:00",
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"],
            },
            []
        ),
        (
            'not available minutes', {
                "subject": "An Update Schedule email",
                "status": "queued",
                "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
                "text": "Updated",
                "text_format": "text",
                "date_to_send": "2019-03-15T00:10:00+09:00",  # We can also accept UTC format.
                "additional_date_to_send1": "2021-03-16T04:20:00+09:00",
                "additional_date_to_send2": "2021-03-17T07:30:00+09:00",
                "additional_date_to_send3": "2021-03-18T21:40:00+09:00",
                "additional_date_to_send4": "2021-03-19T20:50:00+09:00",
                "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"],
            },
            []
        ),
    ]

    def setUp(self):
        super().setUp()
        self.update_date_to_send = generate_day_to_date('python_format')
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_post_not_allowed_period_mail(self):

        data = {
            "subject": "Post Schedule email",
            "status": "queued",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "Post",
            "text_format": "text",
            "date_to_send": "2025-03-14T10:00:00+09:00",
            "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"],
        }
        response = self.client.post(path='/app_staffing/scheduled_mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('1ヶ月以上先の予約を行うことはできません。配信時刻を変更してください' in response.data['detail'])

    def test_set_date_to_send_for_sent_mails(self):
        data = {
            "subject": "Update date_to_sent for sent mails",
            "sender": "1dae6212-0ff4-40e4-86e1-bd73664a8b13",
            "text": "Update date_to_sent for sent mails",
            "text_format": "text",
            "date_to_send": self.update_date_to_send,
        }
        response = self.client.patch(path='/app_staffing/scheduled_mails/5737b0a8-5a99-4254-8798-b8c4a475cc2f', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(ScheduledEmailAlreadySentException.default_detail in response.data['detail'])

    def test_set_date_to_send_before_current_time(self):
        data = {
            "subject": "Update date_to_sent before current time",
            "sender": "1dae6212-0ff4-40e4-86e1-bd73664a8b13",
            "text": "Update date_to_sent for sent mails",
            "text_format": "text",
            "date_to_send": "2020-03-11T10:00:00+09:00",
        }
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(ScheduledEmailPastDateException.default_detail in response.data['detail'])

    def test_date_to_send_is_none_when_updating_status_to_draft(self):
        data = {
            "subject": "Update Status To Draft",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "update status to draft",
            "text_format": "text",
            "status": DRAFT,
        }
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data['date_to_send'])

class ScheduledEmailMultiTenantTestCase(ScheduledEmailTestDataMixin, MultiTenancyTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    test_patterns = ('list', 'get', 'post', 'patch', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.resource_id_for_tenant_1 = cls.email1.id
        cls.resource_id_for_tenant_2 = cls.email2.id

        available_contact_ids_for_tenant_1 = [str(target.contact.id) for target in cls.email1.targets.all()]
        available_contact_ids_for_tenant_2 = [str(target.contact.id) for target in cls.email2.targets.all()]

        update_date_to_send = generate_day_to_date('iso_format')

        cls.post_data_for_tenant_1 = {
            "subject": "This is a scheduled email for tenant A",
            "status": "draft",
            "sender": str(cls.user1.id),
            "text": "Hi",
            "date_to_send": update_date_to_send,  # We can also accept UTC format.
            "target_contacts": available_contact_ids_for_tenant_1,
            "send_copy_to_sender": True,
            "text_format": 'text'
        }

        cls.post_data_for_tenant_2 = {
            "subject": "This is a scheduled email for tenant B",
            "status": "draft",
            "sender": str(cls.user2.id),
            "text": "Hi",
            "date_to_send": update_date_to_send,  # We can also accept UTC format.
            "target_contacts": available_contact_ids_for_tenant_2,
            "send_copy_to_sender": False,
            "text_format": 'text'
        }

        cls.patch_data_for_tenant_1 = {
            "status": "queued",
            "target_contacts": available_contact_ids_for_tenant_1,
            "subject": "This is a scheduled email for tenant A",
            "sender": str(cls.user1.id),
            "date_to_send": update_date_to_send,  # We can also accept UTC format.
            "text": "Hi",
            "text_format": 'text'
        }

        cls.patch_data_for_tenant_2 = {
            "status": "queued",
            "target_contacts": available_contact_ids_for_tenant_2,
            "date_to_send": update_date_to_send,  # We can also accept UTC format.
            "subject": "This is a scheduled email for tenant B",
            "sender": str(cls.user2.id),
            "text": "Hi",
            "text_format": 'text'
        }


class ScheduledEmailAttachmentViewTestCase(MediaFolderSetupMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"
    EMAIL_ATTACHMENT_ID = 'e57a0a67-a005-4cf7-9f26-62d089bfdd32'
    TEXT_FILE_BODY = 'This is a test file'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # Setting up files for POST test
        if not os.path.exists(os.path.dirname(TEST_FILE_PATH)):
            os.makedirs(os.path.dirname(TEST_FILE_PATH))

        with open(file=TEST_FILE_PATH, mode='w') as fp:
            fp.write(cls.TEXT_FILE_BODY)

        # Check Fixture Conditions
        assert ScheduledEmail.objects.count() == 2
        assert ScheduledEmailAttachment.objects.count() == 2
        assert ScheduledEmailAttachment.objects.filter(email=cls.EMAIL_ID).count() == 1
        assert str(ScheduledEmailAttachment.objects.filter(email=cls.EMAIL_ID)[0].id) == cls.EMAIL_ATTACHMENT_ID

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(path=os.path.dirname(TEST_FILE_PATH))
        super().tearDownClass()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        # Create Actual Attachment Files for the test. (Overriding fixture data)
        self.attachment_instance = ScheduledEmailAttachment.objects.get(email=self.EMAIL_ID)
        self.attachment_instance.file = SimpleUploadedFile('sample.txt', bytes(self.TEXT_FILE_BODY, encoding='utf-8'))
        self.attachment_instance.save()

    def test_post_attachment(self):
        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        IsScheduledEmailAttachment(response.data).validate()

    def test_post_attachment_with_url_setting(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)
        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        IsScheduledEmailAttachment(response.data).validate()
        # validate scheduled email
        schedule_email = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        self.assertIsNotNone(schedule_email.password)
        self.assertIsNotNone(schedule_email.expired_date)
        self.assertEqual(schedule_email.file_type, ScheduledEmailSettingFileStatus.URL)

    def test_email_preview_with_url_setting(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)
        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )
        response = self.client.get(
            path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/preview'
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        IsScheduledEmailPreview(response.data).validate()
        self.assertIsNotNone(response.data.get("password"))
        self.assertIsNotNone(response.data.get("url_list"))
        self.assertEqual(response.data.get("file_type"), ScheduledEmailSettingFileStatus.URL)

    def test_should_return_400_when_incorrect_password(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            attachment_res = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        password = 'Zxcvbnm99q'
        schedule_email = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        schedule_email.password = password
        schedule_email.save()
        url = api_reverse("scheduled_email_authorize_download", kwargs={"pk": self.EMAIL_ID}) + "?password=XbOlNBYGjD"
        response = self.client.get(url)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(response.data.get('detail'), ScheduledEmailIncorrectPassword.default_detail)

    def test_should_return_list_url_file_when_correct_password(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            attachment_res = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        password = 'Zxcvbnm99q'
        schedule_email = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        schedule_email.password = password
        schedule_email.save()
        url = api_reverse("scheduled_email_authorize_download", kwargs={"pk": self.EMAIL_ID}) + f'?password={password}'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get("attachments"))

    def test_should_return_200_when_without_password(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            attachment_res = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        url = api_reverse("scheduled_email_authorize_download", kwargs={"pk": attachment_res.data.get("attached_mail_id")})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)

    def test_should_return_404_when_attachment_is_not_exist(self):
        company = CompanyFactory()
        ScheduledEmailSettingFactory(company=company, file_type=ScheduledEmailSettingFileStatus.URL)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            attachment_res = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )
        fake_id = "0154b60e-cef6-4122-b8c7-6d190515582d"
        url = api_reverse("scheduled_email_authorize_download", kwargs={"pk": fake_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get('detail'), ScheduledEmailAttachmentNotExist.default_detail)


    def test_post_attachment_not_more_than_10(self):
        for _ in range(9):
            with open(file=TEST_FILE_PATH, mode='r') as fp:
                data = {
                    'file': fp
                }
                content_type = 'multipart/form-data'
                headers = {
                    'HTTP_CONTENT_TYPE': content_type,
                    'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

                response = self.client.post(
                    headers=headers,
                    path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                    data=data
                )
                self.assertEqual(response.status_code,status.HTTP_201_CREATED)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['detail'], settings.FILE_ATTACHMENT_ERROR_MESSAGE)

    def test_post_attachment_and_delete_tasks(self):
        email_instance = self.attachment_instance.email
        email_instance.status = 'tasks'
        email_instance.save()

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            mock = Mock()
            with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
                response = self.client.post(
                    headers=headers,
                    path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                    data=data
                )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        IsScheduledEmailAttachment(response.data).validate()

        email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        self.assertEqual(email_instance.status, '')
        self.assertEqual(mock.delete_task.call_count, 1)

    def test_post_attachments_with_same_name(self):
        for i in range(3):
            with open(file=TEST_FILE_PATH, mode='r') as fp:
                data = {
                    'file': fp
                }

                content_type = 'multipart/form-data'
                filename = 'SendMailAttachment.txt'
                headers = {
                    'HTTP_CONTENT_TYPE': content_type,
                    'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="{}'.format(filename)
                }

                response = self.client.post(
                    headers=headers,
                    path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                    data=data
                )

            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            IsScheduledEmailAttachment(response.data).validate()
            self.assertEqual(response.data['name'], 'SendMailAttachment.txt')

    def test_get_attachment_info(self):
        response = self.client.get(path=f'/app_staffing/scheduled_mail_attachments/{TEST_MAIL_ATTACHMENT_ID}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        IsScheduledEmailAttachment(response.data).validate()

    def test_delete_attachment(self):
        response = self.client.delete(path=f'/app_staffing/scheduled_mail_attachments/{TEST_MAIL_ATTACHMENT_ID}')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_attachment_and_delete_tasks(self):
        email_instance = self.attachment_instance.email
        email_instance.status = 'tasks'
        email_instance.save()

        mock = Mock()
        with patch('app_staffing.views.scheduled_email.CloudTasks', return_value=mock):
            response = self.client.delete(path=f'/app_staffing/scheduled_mail_attachments/{TEST_MAIL_ATTACHMENT_ID}')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        email_instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        self.assertEqual(email_instance.status, '')
        self.assertEqual(mock.delete_task.call_count, 1)

    def test_get_attachment_related_to_a_specific_mail(self):
        response = self.client.get(path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(str(response.data['results'][0]['attached_mail_id']), self.EMAIL_ID)

    def test_download_attachment(self):
        response = self.client.get(
            path=f'/app_staffing/scheduled_mail_attachments/{TEST_MAIL_ATTACHMENT_ID}',
            HTTP_ACCEPT='application/octet-stream',
        )
        text = b''.join(response.streaming_content).decode(encoding='utf-8')
        self.assertEqual(self.TEXT_FILE_BODY, text)

    def test_download_same_name_attachments(self):
        attachment_ids = []
        for i in range(3):
            with open(file=TEST_FILE_PATH, mode='r') as fp:
                data = {
                    'file': fp
                }

                content_type = 'multipart/form-data'
                filename = 'SendMailAttachment.txt'
                headers = {
                    'HTTP_CONTENT_TYPE': content_type,
                    'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="{}'.format(filename)
                }

                response = self.client.post(
                    headers=headers,
                    path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                    data=data
                )
                attachment_ids.append(response.data['id'])

        for a_id in attachment_ids:
            response = self.client.get(
                path=f'/app_staffing/scheduled_mail_attachments/{a_id}',
                HTTP_ACCEPT='application/octet-stream',
            )
            text = b''.join(response.streaming_content).decode(encoding='utf-8')
            self.assertEqual(self.TEXT_FILE_BODY, text)


class ScheduledEmailAttachmentMultiTenantTestCase(ScheduledEmailTestDataMixin, MultiTenancyTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    base_url = '/app_staffing/scheduled_mail_attachments'
    test_patterns = ('get', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.resource_id_for_tenant_1 = cls.attachment1.id
        cls.resource_id_for_tenant_2 = cls.attachment2.id

    # Note, the endpoint of attachments is different by method,
    # So Add extra tests.
    def test_get_attachment_list(self):
        # Check if a user can only see data that belong to the same tenant.
        base_url = '/app_staffing/scheduled_mails'
        response1 = self.client_1.get(
            path=f'{base_url}/{self.email1.id}/attachments',
            format='json'
        )
        response2 = self.client_2.get(
            path=f'{base_url}/{self.email2.id}/attachments',
            format='json'
        )
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)
        self.assertEqual(1, len(response1.data['results']))
        self.assertEqual(1, len(response2.data['results']))

    def test_post_attachment(self):
        base_url = '/app_staffing/scheduled_mails'

        content_type = 'multipart/form-data'
        headers = {
            'HTTP_CONTENT_TYPE': content_type,
        }

        response1 = self.client_1.post(
            headers=headers,
            path=f'{base_url}/{self.email1.id}/attachments',
            data={'file': SimpleUploadedFile(name='file1', content=b'abc', content_type='text/plain')}
        )

        response2 = self.client_2.post(
            headers=headers,
            path=f'{base_url}/{self.email2.id}/attachments',
            data={'file': SimpleUploadedFile(name='file2', content=b'def', content_type='text/plain')}
        )

        self.assertEqual(status.HTTP_201_CREATED, response1.status_code)
        self.assertEqual(status.HTTP_201_CREATED, response2.status_code)

        # Check if a user can NOT see the created data that belongs to other tenants.
        # We need to do this to make sure that 2 created resources are separated by the tenant barrier.
        created_resource_id_1 = response1.data["id"]
        created_resource_id_2 = response2.data["id"]

        response3 = self.client_1.get(path=f'{self.base_url}/{created_resource_id_2}', format='json')
        response4 = self.client_2.get(path=f'{self.base_url}/{created_resource_id_1}', format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)


class SheduledEmailPreviewViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduledHtmlMail/scheduled_emails.json'
    ]

    TEST_INSTANCE_ID = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'
    TEST_HTML_INSTANCE_ID = '5737b0a8-5a99-4254-8798-b8c4a475cc3f'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_email_preview(self):
        response = self.client.get(
            path=f'/app_staffing/scheduled_mails/{self.TEST_INSTANCE_ID}/preview'
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        IsScheduledEmailPreview(response.data).validate()
        self.assertTrue('blah\nblah' in response.data['body'])

    def test_html_email_preview(self):
        response = self.client.get(
            path=f'/app_staffing/scheduled_mails/{self.TEST_HTML_INSTANCE_ID}/preview'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        IsScheduledEmailPreview(response.data).validate()
        self.assertTrue('所属取引先' in response.data['body'])
        self.assertTrue('取引先担当者 様' in response.data['body'])
        self.assertTrue('<b>ここはそのまま表示される</b><br>' in response.data['body'])
        self.assertTrue('&lt;script&gt;scriptタグは許可されていないのでサニタイズされる&lt;/script&gt;' in response.data['body'])
        self.assertTrue('<pre>***********' in response.data['body'])
        self.assertTrue('An Admin of example.com' in response.data['body'])


class ScheduledEmailPreviewMultiTenantTestCase(ScheduledEmailTestDataMixin, ActionViewMTMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    action_types = ('get',)
    action_name = 'preview'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.resource_id_for_tenant_1 = cls.email1.id
        cls.resource_id_for_tenant_2 = cls.email2.id


class ScheduledEmailCopyMultiTenantTestCase(ScheduledEmailTestDataMixin, ActionViewMTMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    action_types = ('post',)
    action_name = 'copy'
    success_code_for_post = status.HTTP_201_CREATED

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.resource_id_for_tenant_1 = cls.email1.id
        cls.resource_id_for_tenant_2 = cls.email2.id


class ScheduledEmailCopyViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]

    EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"
    EMAIL_ATTACHMENT_ID = 'e57a0a67-a005-4cf7-9f26-62d089bfdd32'

    MOCKED_TIME = '2018-01-01'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Check fixture conditions.
        assert ScheduledEmail.objects.count() == 2
        cls.original_email_instance = ScheduledEmail.objects.get(id__exact=cls.EMAIL_ID)
        assert ScheduledEmailAttachment.objects.filter(email=cls.original_email_instance).exists()
        assert str(ScheduledEmailAttachment.objects.get(email=cls.original_email_instance).id) == cls.EMAIL_ATTACHMENT_ID

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    @freeze_time(MOCKED_TIME)
    def test_copying_email(self):
        response = self.client.post(path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/copy')
        # Validate Status Code.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Validate Response Format.
        validator = IsScheduledEmail(response.data)
        validator.validate()
        # Get New instance id and check if it exists in the database.
        instance_id = response.data['id']
        instance = ScheduledEmail.objects.get(pk=instance_id)
        # Check if the new instance's subject and status are updated.
        self.assertEqual(instance.subject, 'A sample scheduled email')  # Same as the fixture value.
        self.assertEqual(instance.scheduled_email_status.name, DRAFT)
        # Check if the sent_date and the date_to_send are cleared. (null)
        self.assertFalse(instance.date_to_send)
        self.assertFalse(instance.sent_date)
        # Check if the user info has set to a new value.
        self.assertEqual(response.data['created_user'], User.objects.get(username=TEST_USER_NAME).id)
        self.assertEqual(response.data['modified_user'], User.objects.get(username=TEST_USER_NAME).id)
        # Check if the time info has updated.
        self.assertEqual(response.data['created_time'], f'{self.MOCKED_TIME}T09:00:00+09:00')
        self.assertEqual(response.data['modified_time'], f'{self.MOCKED_TIME}T09:00:00+09:00')
        # Check if the attachments are copied.
        self.assertEqual(self.original_email_instance.targets.count(), instance.targets.count())
        for target in self.original_email_instance.targets.all():
            self.assertTrue(target.contact_id in [obj.contact_id for obj in instance.targets.all()])
        # Check if the target_contacts are copied.
        self.assertEqual(self.original_email_instance.attachments.count(), instance.attachments.count())
        for attachment in self.original_email_instance.attachments.all():
            self.assertTrue(attachment.file in [obj.file for obj in instance.attachments.all()])


class ScheduledEmailViewPermissionTestCaseWithNormalUser(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    resource_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_delete(self):
        response = self.client.delete(path=self.base_url + "/{0}".format(self.resource_id), data={})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class ScheduledEmailFilterViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/dateRangeFilter/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_search_by_date_range(self):
        response = self.client.get(path=f'/app_staffing/scheduled_mails?date_to_send_gte=2019-05-14&date_to_send_lte=2019-05-15')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        gte = datetime.date(2019, 5, 14)
        lt = datetime.date(2019, 5, 16)
        for email in response.data['results']:
            tdatetime = datetime.datetime.strptime(email['date_to_send'], '%Y-%m-%dT%H:%M:%S+09:00')
            tdate = datetime.date(tdatetime.year, tdatetime.month, tdatetime.day)
            self.assertTrue(gte <= tdate)
            self.assertTrue(tdate < lt)

    def test_search_by_subject_and_text(self):
        response = self.client.get(path=f'/app_staffing/scheduled_mails?subject=e%20m%20a%20i%20l&text=e%20m%20a%20i%20l')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['subject'], 'A sample scheduled email')
        self.assertEqual(response.data['results'][0]['text'], 'This is a text of an email.')

        # subject='配信　メール'で検索を実施
        response = self.client.get(path=f'/app_staffing/scheduled_mails?subject=%E3%83%A1%E3%83%BC%E3%83%AB%E3%80%80%E9%85%8D%E4%BF%A1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['subject'], 'サンプル配信メール')

    def test_get_full_conditions(self):
        data = {
            'send_type': 'job',
            'subject': 'A sample scheduled email',
            'text': 'This is a text of an email.',
            'attachments': '2',
            'status': 'draft',
            'sender_id': '1dae6212-0ff4-40e4-86e1-bd73664a8b13',
            'date_to_send_gte': '2019-05-01',
            'date_to_send_lte': '2019-05-31',
            'sent_date_gte': '2019-05-01',
            'sent_date_lte': '2019-05-31',
            'text_format': 'html',
            'open_count_gt': 9,
            'open_count_lt': 11,
            'send_total_count_gt': 99,
            'send_total_count_lt': 101,
            'created_user': '1dae6212-0ff4-40e4-86e1-bd73664a8b13',
            'modified_user': '1dae6212-0ff4-40e4-86e1-bd73664a8b13',
        }
        response = self.client.get(path="/app_staffing/scheduled_mails", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['subject'], 'A sample scheduled email')

    def test_get_single_condition(self):
        cases = [
            {
                'data' : {
                    'subject': 'non existent subject',
                },
                'count': 0,
            },
            {
                'data' : {
                    'text': 'non existent text',
                },
                'count': 0,
            },
            {
                'data' : {
                    'attachments': '1',
                },
                'count': 1,
            },
            {
                'data' : {
                    'attachments': '2',
                },
                'count': 1,
            },
            {
                'data' : {
                    'attachments': '1,2',
                },
                'count': 2,
            },
            {
                'data' : {
                    'sender_id': '1dae6212-0ff4-40e4-86e1-bd73664a8b13,2dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 3,
            },
            {
                'data' : {
                    'sender_id': '2dae6212-0ff4-40e4-86e1-bd73664a8b13,3dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 0,
            },
            {
                'data' : {
                    'date_to_send_gte': '2100-01-01',
                },
                'count': 0,
            },
            {
                'data' : {
                    'date_to_send_lte': '1900-01-01',
                },
                'count': 0,
            },
            {
                'data' : {
                    'sent_date_gte': '2100-01-01',
                },
                'count': 0,
            },
            {
                'data' : {
                    'sent_date_lte': '1900-01-01',
                },
                'count': 0,
            },
            {
                'data' : {
                    'text_format': 'text',
                },
                'count': 2,
            },
            {
                'data' : {
                    'text_format': 'html',
                },
                'count': 1,
            },
            {
                'data' : {
                    'open_count_gt': 10000,
                },
                'count': 0,
            },
            {
                'data' : {
                    'open_count_lt': 1,
                },
                'count': 2,
            },
            {
                'data' : {
                    'send_total_count_gt': 10000,
                },
                'count': 0,
            },
            {
                'data' : {
                    'send_total_count_lt': 1,
                },
                'count': 0,
            },
            {
                'data' : {
                    'created_user': '1dae6212-0ff4-40e4-86e1-bd73664a8b13,2dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 3,
            },
            {
                'data' : {
                    'created_user': '2dae6212-0ff4-40e4-86e1-bd73664a8b13,3dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 0,
            },
            {
                'data' : {
                    'modified_user': '1dae6212-0ff4-40e4-86e1-bd73664a8b13,2dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 3,
            },
            {
                'data' : {
                    'modified_user': '2dae6212-0ff4-40e4-86e1-bd73664a8b13,3dae6212-0ff4-40e4-86e1-bd73664a8b13',
                },
                'count': 0,
            },
        ]
        for c in cases:
            response = self.client.get(path="/app_staffing/scheduled_mails", data=c.get('data'), format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data['results']), c.get('count'))

class ScheduledEmailFilterInClauseViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_status_conditions(self):
        ScheduledEmail.objects.create(
            subject="draft",
            scheduled_email_status=ScheduledEmailStatus.objects.get(id=1),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="queued",
            scheduled_email_status=ScheduledEmailStatus.objects.get(id=2),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="sending",
            scheduled_email_status=ScheduledEmailStatus.objects.get(id=3),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="sent",
            scheduled_email_status=ScheduledEmailStatus.objects.get(id=4),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="error",
            scheduled_email_status=ScheduledEmailStatus.objects.get(id=5),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )

        testCases = [
            # 1件指定(非not)
            {
                "data": {"status": "draft"},
                "expected": ["draft"],
            },
            {
                "data": {"status": "queued"},
                "expected": ["queued"],
            },
            {
                "data": {"status": "sending"},
                "expected": ["sending"],
            },
            {
                "data": {"status": "sent"},
                "expected": ["sent"],
            },
            {
                "data": {"status": "error"},
                "expected": ["error"],
            },
            # 1件指定(not)
            {
                "data": {"status": "not:draft"},
                "expected": ["queued", "sending", "sent", "error"],
            },
            {
                "data": {"status": "not:queued"},
                "expected": ["draft", "sending", "sent", "error"],
            },
            {
                "data": {"status": "not:sending"},
                "expected": ["draft", "queued", "sent", "error"],
            },
            {
                "data": {"status": "not:sent"},
                "expected": ["draft", "queued", "sending", "error"],
            },
            {
                "data": {"status": "not:error"},
                "expected": ["draft", "queued", "sending", "sent"],
            },
            # 2件指定(非not)
            {
                "data": {"status": "draft,queued"},
                "expected": ["draft", "queued"],
            },
            {
                "data": {"status": "queued,sending"},
                "expected": ["queued", "sending"],
            },
            {
                "data": {"status": "sending,sent"},
                "expected": ["sending", "sent"],
            },
            {
                "data": {"status": "sent,error"},
                "expected": ["sent", "error"],
            },
            {
                "data": {"status": "draft,error"},
                "expected": ["draft", "error"],
            },
            # 2件指定(not)
            {
                "data": {"status": "not:draft,not:queued"},
                "expected": ["sending", "sent", "error"],
            },
            {
                "data": {"status": "not:queued,not:sending"},
                "expected": ["draft", "sent", "error"],
            },
            {
                "data": {"status": "not:sending,not:sent"},
                "expected": ["draft", "queued", "error"],
            },
            {
                "data": {"status": "not:sent,not:error"},
                "expected": ["draft", "queued", "sending"],
            },
            {
                "data": {"status": "not:draft,not:error"},
                "expected": ["queued", "sending", "sent"],
            },
            # 3件指定(非not)
            {
                "data": {"status": "draft,queued,sending"},
                "expected": ["draft", "queued", "sending"],
            },
            {
                "data": {"status": "queued,sending,sent"},
                "expected": ["queued", "sending", "sent"],
            },
            {
                "data": {"status": "sending,sent,error"},
                "expected": ["sending", "sent", "error"],
            },
            {
                "data": {"status": "draft,queued,error"},
                "expected": ["draft", "queued", "error"],
            },
            {
                "data": {"status": "draft,sending,error"},
                "expected": ["draft", "sending", "error"],
            },
            # 3件指定(not)
            {
                "data": {"status": "not:draft,not:queued,not:sending"},
                "expected": ["sent", "error"],
            },
            {
                "data": {"status": "not:queued,not:sending,not:sent"},
                "expected": ["draft", "error"],
            },
            {
                "data": {"status": "not:sending,not:sent,not:error"},
                "expected": ["draft", "queued"],
            },
            {
                "data": {"status": "not:draft,not:sent,not:error"},
                "expected": ["queued", "sending"],
            },
            {
                "data": {"status": "not:queued,not:sending,not:error"},
                "expected": ["draft", "sent"],
            },
            # 4件指定(非not)
            {
                "data": {"status": "draft,queued,sending,sent"},
                "expected": ["draft", "queued", "sending", "sent"],
            },
            {
                "data": {"status": "draft,queued,sending,error"},
                "expected": ["draft", "queued", "sending", "error"],
            },
            {
                "data": {"status": "draft,queued,sent,error"},
                "expected": ["draft", "queued", "sent", "error"],
            },
            {
                "data": {"status": "draft,sending,sent,error"},
                "expected": ["draft", "sending", "sent", "error"],
            },
            {
                "data": {"status": "queued,sending,sent,error"},
                "expected": ["queued", "sending", "sent", "error"],
            },
            # 4件指定(not)
            {
                "data": {"status": "not:queued,not:sending,not:sent,not:error"},
                "expected": ["draft"],
            },
            {
                "data": {"status": "not:draft,not:sending,not:sent,not:error"},
                "expected": ["queued"],
            },
            {
                "data": {"status": "not:draft,not:queued,not:sent,not:error"},
                "expected": ["sending"],
            },
            {
                "data": {"status": "not:draft,not:queued,not:sending,not:error"},
                "expected": ["sent"],
            },
            {
                "data": {"status": "not:draft,not:queued,not:sending,not:sent"},
                "expected": ["error"],
            },
            # 5件指定(非not)
            {
                "data": {"status": "draft,queued,sending,sent,error"},
                "expected": ["draft", "queued", "sending", "sent", "error"],
            },
            # 5件指定(not)
            {
                "data": {"status": "not:draft,not:queued,not:sending,not:sent,not:error"},
                "expected": [],
            },
            # 非notとnotを混合指定
            {
                "data": {"status": "draft,not:draft"},
                "expected": ["draft", "queued", "sending", "sent", "error"],
            },
            {
                "data": {"status": "draft,not:queued"},
                "expected": ["draft", "sending", "sent", "error"],
            },
            {
                "data": {"status": "draft,not:draft,not:queued"},
                "expected": ["draft", "sending", "sent", "error"],
            },
            {
                "data": {"status": "draft,queued,not:draft,not:queued"},
                "expected": ["draft", "queued", "sending", "sent", "error"],
            },
        ]

        for testCase in testCases:
            response = self.client.get(path="/app_staffing/scheduled_mails", data=testCase["data"], format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["subject"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

    def test_get_send_type_conditions(self):
        ScheduledEmail.objects.create(
            subject="job",
            scheduled_email_send_type=ScheduledEmailSendType.objects.get(id=1),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="personnel",
            scheduled_email_send_type=ScheduledEmailSendType.objects.get(id=2),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )
        ScheduledEmail.objects.create(
            subject="other",
            scheduled_email_send_type=ScheduledEmailSendType.objects.get(id=3),
            sender=User.objects.get(username=TEST_USER_NAME),
            company=GET_DEFAULT_COMPANY(),
        )

        testCases = [
            # 1件指定(非not)
            {
                "data": {"send_type": "job"},
                "expected": ["job"],
            },
            {
                "data": {"send_type": "personnel"},
                "expected": ["personnel"],
            },
            {
                "data": {"send_type": "other"},
                "expected": ["other"],
            },
            # 1件指定(not)
            {
                "data": {"send_type": "not:personnel"},
                "expected": ["job", "other"],
            },
            {
                "data": {"send_type": "not:job"},
                "expected": ["personnel", "other"],
            },
            {
                "data": {"send_type": "not:other"},
                "expected": ["job", "personnel"],
            },
            # 2件指定(非not)
            {
                "data": {"send_type": "job,personnel"},
                "expected": ["job", "personnel"],
            },
            {
                "data": {"send_type": "personnel,other"},
                "expected": ["personnel", "other"],
            },
            {
                "data": {"send_type": "other,job"},
                "expected": ["other", "job"],
            },
            # 2件指定(not)
            {
                "data": {"send_type": "not:job,not:personnel"},
                "expected": ["other"],
            },
            {
                "data": {"send_type": "not:personnel,not:other"},
                "expected": ["job"],
            },
            {
                "data": {"send_type": "not:other,not:job"},
                "expected": ["personnel"],
            },
            # 3件指定(非not)
            {
                "data": {"send_type": "job,personnel,other,"},
                "expected": ["job", "personnel", "other"],
            },
            # 3件指定(not)
            {
                "data": {"send_type": "not:job,not:personnel,not:other,"},
                "expected": [],
            },
            # 非notとnotを混合指定
            {
                "data": {"send_type": "job,not:job"},
                "expected": ["job", "personnel", "other"],
            },
            {
                "data": {"send_type": "job,not:personnel"},
                "expected": ["job", "other"],
            },
            {
                "data": {"send_type": "job,not:job,not:personnel"},
                "expected": ["job", "other"],
            },
            {
                "data": {"send_type": "job,personnel,not:personnel"},
                "expected": ["job", "personnel", "other"],
            },
        ]

        for testCase in testCases:
            response = self.client.get(path="/app_staffing/scheduled_mails", data=testCase["data"], format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["subject"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

class SheduledEmailPreviewViewSizeOverTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduledHtmlMail/scheduled_emails.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    TEST_INSTANCE_ID = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'

    tmp_size = settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        setattr(settings, "SCHEDULED_EMAIL_MAX_BYTE_SIZE", 1)

    def tearDown(self):
        setattr(settings, "SCHEDULED_EMAIL_MAX_BYTE_SIZE", self.tmp_size)

    def test_email_preview(self):
        response = self.client.get(
            path=f'/app_staffing/scheduled_mails/{self.TEST_INSTANCE_ID}/preview'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['over_max_byte_size'])
        IsScheduledEmailPreview(response.data).validate()

    def test_email_preview(self):
        instance = ScheduledEmail.objects.get(id=self.TEST_INSTANCE_ID)
        scheduled_email_byte_size = instance.total_byte_size
        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=6)
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_attachment_max_size=True)
        setattr(settings, "SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE", scheduled_email_byte_size)

        response = self.client.get(
            path=f'/app_staffing/scheduled_mails/{self.TEST_INSTANCE_ID}/preview'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['over_max_byte_size'])
        IsScheduledEmailPreview(response.data).validate()


class ScheduledEmailAttachmentViewSizeOverTestCase(MediaFolderSetupMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"
    EMAIL_ATTACHMENT_ID = 'e57a0a67-a005-4cf7-9f26-62d089bfdd32'
    TEXT_FILE_BODY = 'This is a test file'

    tmp_size = settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # Setting up files for POST test
        if not os.path.exists(os.path.dirname(TEST_FILE_PATH)):
            os.makedirs(os.path.dirname(TEST_FILE_PATH))

        with open(file=TEST_FILE_PATH, mode='w') as fp:
            fp.write(cls.TEXT_FILE_BODY)

        # Check Fixture Conditions
        assert ScheduledEmail.objects.count() == 2
        assert ScheduledEmailAttachment.objects.count() == 2
        assert ScheduledEmailAttachment.objects.filter(email=cls.EMAIL_ID).count() == 1
        assert str(ScheduledEmailAttachment.objects.filter(email=cls.EMAIL_ID)[0].id) == cls.EMAIL_ATTACHMENT_ID

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(path=os.path.dirname(TEST_FILE_PATH))
        super().tearDownClass()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        # Create Actual Attachment Files for the test. (Overriding fixture data)
        self.attachment_instance = ScheduledEmailAttachment.objects.get(email=self.EMAIL_ID)
        self.attachment_instance.file = SimpleUploadedFile('sample.txt', bytes(self.TEXT_FILE_BODY, encoding='utf-8'))
        self.attachment_instance.save()
        setattr(settings, "SCHEDULED_EMAIL_MAX_BYTE_SIZE", 1)

    def tearDown(self):
        setattr(settings, "SCHEDULED_EMAIL_MAX_BYTE_SIZE", self.tmp_size)

    def test_post_attachment_fail(self):

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), settings.MAX_BYTE_SIZE_ERROR_MESSAGE)

    def test_post_attachment_success(self):

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
            scheduled_email_byte_size = instance.total_byte_size + len(self.TEXT_FILE_BODY)
            setattr(settings, "SCHEDULED_EMAIL_MAX_BYTE_SIZE", scheduled_email_byte_size)
            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_attachment_with_addon(self):
        instance = ScheduledEmail.objects.get(id=self.EMAIL_ID)
        scheduled_email_byte_size = instance.total_byte_size + len(self.TEXT_FILE_BODY)
        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=6)
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_attachment_max_size=True)
        setattr(settings, "SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE", scheduled_email_byte_size)

        with open(file=TEST_FILE_PATH, mode='r') as fp:
            data = {
                'file': fp
            }

            content_type = 'multipart/form-data'
            headers = {
                'HTTP_CONTENT_TYPE': content_type,
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="SendMailAttachment.txt'}

            response = self.client.post(
                headers=headers,
                path=f'/app_staffing/scheduled_mails/{self.EMAIL_ID}/attachments',
                data=data
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ScheduledEmailViewRelatedSearchConditionDeleteTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/scheduled_email_search_conditions.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    resource_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_not_delete(self):
        self.assertEqual(ScheduledEmailSearchCondition.objects.all().count(), 2)
        response = self.client.delete(path=self.base_url + "/{0}".format(self.resource_id), data={})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ScheduledEmailSearchCondition.objects.all().count(), 1)
        self.assertEqual(ScheduledEmailSearchCondition.objects.filter(email_id=self.resource_id).count(), 0)


class ScheduledEmailOpenHistoryViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company.json',
        'app_staffing/tests/fixtures/payjp/users.json',
        'app_staffing/tests/fixtures/payjp/emails.json',
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_open_history.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    SCHEDULED_EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_default(self):
        url = api_reverse("scheduled_email_open_history", kwargs={"pk": self.SCHEDULED_EMAIL_ID})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_added_property_exists(self):
        url = api_reverse("scheduled_email_open_history", kwargs={"pk": self.SCHEDULED_EMAIL_ID})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertTrue("is_open_count_extra_period_available" in data)

    def test_is_open_count_extra_period_available_is_false(self):
        url = api_reverse("scheduled_email_open_history", kwargs={"pk": self.SCHEDULED_EMAIL_ID})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        is_open_count_extra_period_available = data["is_open_count_extra_period_available"]
        self.assertFalse(is_open_count_extra_period_available)

    def test_is_open_count_extra_period_available_is_true(self):
        # 必要なアドオンを作成
        user = User.objects.get(username=TEST_USER_NAME)
        company = user.company
        addon_master_id = settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID
        Addon.objects.create(company=company, addon_master_id=addon_master_id)
        # APIのテスト
        url = api_reverse("scheduled_email_open_history", kwargs={"pk": self.SCHEDULED_EMAIL_ID})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        is_open_count_extra_period_available = data["is_open_count_extra_period_available"]
        self.assertTrue(is_open_count_extra_period_available)


class ScheduledEmailSettingViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    username = 'hogehoge'
    hostname = 'example.com'
    port_number = 587
    password = 'fugafuga0123'
    connection_type = settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_DEFAULT

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_default(self):
        response = self.client.get(path='/app_staffing/scheduled_email_setting')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['is_open_count_available'])
        self.assertFalse(response.data['use_open_count'])
        self.assertFalse(response.data['is_use_attachment_max_size_available'])
        self.assertFalse(response.data['use_attachment_max_size'])
        self.assertEqual(response.data['target_count_addon_purchase_count'], 0)

    def test_update(self):
        data = {
            'username': self.username,
            'hostname': self.hostname,
            'port_number': self.port_number,
            'password': self.password,
            'connection_type': self.connection_type,
            'use_open_count': True,
            'use_attachment_max_size': True
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['is_open_count_available'])
        self.assertTrue(response.data['use_open_count'])
        self.assertFalse(response.data['is_use_attachment_max_size_available'])
        self.assertTrue(response.data['use_attachment_max_size'])

        smtpserver = SMTPServer.objects.get(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(smtpserver.username, self.username)
        self.assertEqual(smtpserver.hostname, self.hostname)
        self.assertEqual(smtpserver.port_number, self.port_number)
        self.assertEqual(smtpserver.password, self.password)
        self.assertEqual(smtpserver.use_tls, False)
        self.assertEqual(smtpserver.use_ssl, False)

    def test_over_max_length(self):
        data = {
            'username': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'hostname': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'port_number': 999999,
            'password': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'connection_type': self.connection_type,
            'use_open_count': True
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['username']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['username']['message'])
        self.assertEqual(str(response.data['hostname']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['hostname']['message'])
        self.assertEqual(str(response.data['port_number']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['port_number']['message'])

    def test_not_null_value(self):
        data = {
            'username': None,
            'hostname': None,
            'port_number': None,
            'password': None,
            'connection_type': None,
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['username']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['hostname']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['port_number']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['password']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['connection_type']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])

        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['username']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['hostname']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['port_number']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['password']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
        self.assertEqual(str(response.data['connection_type']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/scheduled_email_setting')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        data = {
            'username': self.username,
            'hostname': self.hostname,
            'port_number': self.port_number,
            'password': self.password,
            'connection_type': self.connection_type,
            'use_open_count': True,
            'use_attachment_max_size': True
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_tls(self):
        data = {
            'username': self.username,
            'hostname': self.hostname,
            'port_number': self.port_number,
            'password': self.password,
            'connection_type': settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_TLS,
            'use_open_count': True,
            'use_attachment_max_size': True
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['is_open_count_available'])
        self.assertTrue(response.data['use_open_count'])
        self.assertFalse(response.data['is_use_attachment_max_size_available'])
        self.assertTrue(response.data['use_attachment_max_size'])

        smtpserver = SMTPServer.objects.get(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(smtpserver.username, self.username)
        self.assertEqual(smtpserver.hostname, self.hostname)
        self.assertEqual(smtpserver.port_number, self.port_number)
        self.assertEqual(smtpserver.password, self.password)
        self.assertEqual(smtpserver.use_tls, True)
        self.assertEqual(smtpserver.use_ssl, False)

    def test_update_ssl(self):
        data = {
            'username': self.username,
            'hostname': self.hostname,
            'port_number': self.port_number,
            'password': self.password,
            'connection_type': settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_SSL,
            'use_open_count': True,
            'use_attachment_max_size': True
        }
        response = self.client.patch(path='/app_staffing/scheduled_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(response.data['is_open_count_available'])
        self.assertTrue(response.data['use_open_count'])
        self.assertFalse(response.data['is_use_attachment_max_size_available'])
        self.assertTrue(response.data['use_attachment_max_size'])

        smtpserver = SMTPServer.objects.get(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(smtpserver.username, self.username)
        self.assertEqual(smtpserver.hostname, self.hostname)
        self.assertEqual(smtpserver.port_number, self.port_number)
        self.assertEqual(smtpserver.password, self.password)
        self.assertEqual(smtpserver.use_tls, False)
        self.assertEqual(smtpserver.use_ssl, True)

class ScheduledEmailConnectionViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_connection_test_fail(self):
        data = {'username': 'hogehoge', 'hostname': 'example.com', 'port_number': 587, 'password': 'fugafuga0123', 'connection_type': 1}
        mock = Mock(return_value=False)
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_connection_test_fail_exception(self):
        data = {'username': 'hogehoge', 'hostname': 'example.com', 'port_number': 587, 'password': 'fugafuga0123', 'connection_type': 1}
        mock = Mock(side_effect=Exception('Boom!'))
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_connection_test_success(self):
        data = {'username': 'hogehoge', 'hostname': 'example.com', 'port_number': 587, 'password': 'fugafuga0123', 'connection_type': 1}
        mock = Mock(return_value=True)
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_over_max_length(self):
        data = {
            'username': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'hostname': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'port_number': 999999,
            'password': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'connection_type': False,
        }
        mock = Mock(return_value=True)
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['username']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['username']['message'])
            self.assertEqual(str(response.data['hostname']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['hostname']['message'])
            self.assertEqual(str(response.data['port_number']), settings.LENGTH_VALIDATIONS['scheduled_email_setting']['port_number']['message'])

    def test_not_null_value(self):
        data = {
            'username': None,
            'hostname': None,
            'port_number': None,
            'password': None,
            'connection_type': None,
        }
        mock = Mock(return_value=True)
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data={}, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['username']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['hostname']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['port_number']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['password']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['connection_type']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])

        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['username']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['hostname']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['port_number']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['password']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])
            self.assertEqual(str(response.data['connection_type']), settings.NOT_NULL_VALIDATIONS['scheduled_email_setting']['message'])

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {'username': 'hogehoge', 'hostname': 'example.com', 'port_number': 587, 'password': 'fugafuga0123', 'connection_type': 1}
        mock = Mock(return_value=True)
        with patch('app_staffing.views.scheduled_email.ScheduledEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_invalid_domain(self):
        domain = 'gmail.com'
        data = {'username': 'hogehoge', 'hostname': domain, 'port_number': 587, 'password': 'fugafuga0123', 'connection_type': 1}
        response = self.client.post(path='/app_staffing/scheduled_email_connection', data=data, format='json')
        message_error = EmailDomainBlocked(domain)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(message_error.default_detail['detail'], response.data['detail'])

class ScheduledEmailViewTargetNotSelectedTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]

    base_url = '/app_staffing/scheduled_mails'
    resource_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

    data = {
        "subject": "An Update Schedule email",
        "status": "queued",
        "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
        "text": "Updated",
        "target_contacts": [],  # 宛先未選択
    }

    def setUp(self):
        self.data['date_to_send'] = generate_day_to_date('utc_format')
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_not_update(self):
        response = self.client.patch(path=self.base_url + "/{0}".format(self.resource_id), data=self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ScheduledEmailAttachmentSizeLimitViewTestCase(MediaFolderSetupMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company.json',
        'app_staffing/tests/fixtures/payjp/users.json',
        'app_staffing/tests/fixtures/payjp/emails.json',
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_limit_not_set(self):
        url = api_reverse("scheduled_email_attachment_size_limit")
        response = self.client.get(url)
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(data["is_attachment_size_extended"])

    def test_limit_set(self):
        user = User.objects.get(username=TEST_USER_NAME)
        company = user.company
        addon_id = settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID
        Addon.objects.create(company=company, addon_master_id=addon_id)
        url = api_reverse("scheduled_email_attachment_size_limit")
        response = self.client.get(url)
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(data["is_attachment_size_extended"])

class ScheduledEmailUpdateTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]

    base_url = '/app_staffing/scheduled_mails'  # Override by child class.

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_set_date_to_send_null_while_set_status_to_queued(self):
        data = {
            "subject": "Update Status To queued",
            "sender": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "text": "update status to queued",
            "text_format": "text",
            "status": QUEUED,
            "target_contacts": ["78630567-72ce-49a3-84e6-8947b5b057f3"],
        }
        response = self.client.patch(path='/app_staffing/scheduled_mails/185c2a9b-ec13-4194-bd85-386de64f5ee4', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(ScheduledEmailDeliveryTimeIsNull.default_detail in response.data['detail'])

class ScheduledEmailOrderingViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/order/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
    ]

    SCHEDULED_EMAIL_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    SCHEDULED_EMAIL_2 = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'
    SCHEDULED_EMAIL_3 = '5737b0a8-5a99-4254-8798-b8c4a475cc3f'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_order_by_sent_date_asc(self):
        data = {
            "page": 1,
            "page_size": 100,
            "ordering": "sent_date",
        }
        response = self.client.get(path="/app_staffing/scheduled_mails", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['id'], self.SCHEDULED_EMAIL_1)
        self.assertEqual(response.data['results'][1]['id'], self.SCHEDULED_EMAIL_2)
        self.assertEqual(response.data['results'][2]['id'], self.SCHEDULED_EMAIL_3)

    def test_order_by_sent_date_desc(self):
        data = {
            "page": 1,
            "page_size": 100,
            "ordering": "-sent_date",
        }
        response = self.client.get(path="/app_staffing/scheduled_mails", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][2]['id'], self.SCHEDULED_EMAIL_1)
        self.assertEqual(response.data['results'][1]['id'], self.SCHEDULED_EMAIL_2)
        self.assertEqual(response.data['results'][0]['id'], self.SCHEDULED_EMAIL_3)

    def test_order_by_modified_time_asc(self):
        data = {
            "page": 1,
            "page_size": 100,
            "ordering": "modified_time",
        }
        response = self.client.get(path="/app_staffing/scheduled_mails", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['id'], self.SCHEDULED_EMAIL_1)
        self.assertEqual(response.data['results'][1]['id'], self.SCHEDULED_EMAIL_2)
        self.assertEqual(response.data['results'][2]['id'], self.SCHEDULED_EMAIL_3)

    def test_order_by_modified_time_desc(self):
        data = {
            "page": 1,
            "page_size": 100,
            "ordering": "-modified_time",
        }
        response = self.client.get(path="/app_staffing/scheduled_mails", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][2]['id'], self.SCHEDULED_EMAIL_1)
        self.assertEqual(response.data['results'][1]['id'], self.SCHEDULED_EMAIL_2)
        self.assertEqual(response.data['results'][0]['id'], self.SCHEDULED_EMAIL_3)


class ScheduledEmailTemplateViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    base_url = '/app_staffing/scheduled_mails/template'

    cache_keys = f'{settings.CACHE_SCHEDULED_EMAIL_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_1957ab0f-b47c-455a-a7b7-4453cfffd05e'

    default = [
        {
            'title': 'hello world 1',
            'subject': 'hello worl 1',
            'text': 'hello world 1',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        }
    ]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    post_data = {
        'templates': [
            {
                'title': 'hello world 1',
                'subject': 'hello world 1',
                'text': 'hello world 1',
                'text_format': 'text',
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            },
            {
                'title': 'hello world 2',
                'subject': 'hello world 2',
                'text': 'hello world 2',
                'text_format': 'text',
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            },
        ]
    }

    post_data_over_default_limit = { 'templates': [
        {
            'title': 'hello world 1',
            'subject': 'hello world 1',
            'text': 'hello world 1',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        },
        {
            'title': 'hello world 2',
            'subject': 'hello world 2',
            'text': 'hello world 2',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        },
        {
            'title': 'hello world 3',
            'subject': 'hello world 3',
            'text': 'hello world 3',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        },
        {
            'title': 'hello world 4',
            'subject': 'hello world 4',
            'text': 'hello world 4',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        }
    ] }

    invalid_data = {'templates': [
        {
            'title': 'hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1',
            'subject': '''hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1
                        hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1
                        hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1
                        hello world 1 hello world 1 hello world 1 hello world 1 hello world 1 hello world 1''',
            'text': 'hello world 1',
            'text_format': 'text',
            'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
        },
    ]}

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_keys, self.default)

    def test_get_scheduled_email_template(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

    def test_post_scheduled_email_template(self):
        response = self.client.post(path=self.base_url, data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_keys)), 2)

    def test_over_max_length(self):
        response = self.client.post(path=self.base_url, data=self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['title']), settings.LENGTH_VALIDATIONS['scheduled_email_template']['title']['message'])
        self.assertEqual(str(response.data['subject']), settings.LENGTH_VALIDATIONS['scheduled_email_template']['subject']['message'])

    def test_not_null_value(self):
        post_data = { 'templates': [
            {
                'title': None,
                'subject': None,
                'text': None,
                'text_format': None,
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            }
        ]}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), TemplateNameBlankException.default_detail)

        post_data = { 'templates': [
            {
                'title': 'test',
                'subject': None,
                'text': None,
                'text_format': None,
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            }
        ]}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['subject']), settings.NOT_NULL_VALIDATIONS['scheduled_email_template']['message'])
        self.assertEqual(str(response.data['text']), settings.NOT_NULL_VALIDATIONS['scheduled_email_template']['message'])
        self.assertEqual(str(response.data['text_format']), settings.NOT_NULL_VALIDATIONS['scheduled_email_template']['message'])

    def test_post_scheduled_email_template_over_limit(self):
        response = self.client.post(path=self.base_url, data=self.post_data_over_default_limit, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

    # patchやdeleteは利用せず、postで上書きする
    def test_update_by_post(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

        response = self.client.post(path=self.base_url, data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_keys)), 2)
        self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

        post_data = { 'templates': []}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_keys)), 0)

    def test_duplicate_template_title(self):
        post_data = { 'templates': [
            {
                'title': 'hello world 4',
                'subject': 'hello world 4',
                'text': 'hello world 4',
                'text_format': 'text',
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            },
            {
                'title': 'hello world 4',
                'subject': 'hello world 4',
                'text': 'hello world 4',
                'text_format': 'text',
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            }
        ]}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateNameRegisteredException.default_detail)

    def test_sender_does_not_exist(self):
        post_data = { 'templates': [
            {
                'title': 'hello world 4',
                'subject': 'hello world 4',
                'text': 'hello world 4',
                'text_format': 'text',
                'sender': '26976828-a205-44a3-aeac-df3e9b6c1491'
            }
        ]}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], SenderDoesNotExistException.default_detail)

    def test_sender_belong_to_another_company(self):
        post_data = { 'templates': [
            {
                'title': 'hello world 4',
                'subject': 'hello world 4',
                'text': 'hello world 4',
                'text_format': 'text',
                'sender': '9dd08e5f-fab1-4ffb-bfdf-b9436376ccd1'
            }
        ]}
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], IsSenderBelongToAnotherCompany.default_detail)

    def test_post_normal_user_can_not_change_sender(self):
        post_data = { 'templates': [
            {
                'title': 'hello world 4',
                'subject': 'hello world 4',
                'text': 'hello world 4',
                'text_format': 'text',
                'sender': '67449fa2-771d-4ffa-bab3-fa160fdf716f'
            }
        ]}
        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path=self.base_url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['templates'][0]['subject'], post_data['templates'][0]['subject'])
        self.assertEqual(response.data['templates'][0]['text'], post_data['templates'][0]['text'])
        self.assertEqual(response.data['templates'][0]['title'], post_data['templates'][0]['title'])
        self.assertEqual(response.data['templates'][0]['text_format'], post_data['templates'][0]['text_format'])
        self.assertEqual(str(response.data['templates'][0]['sender']), 'd31034e2-ca12-4a6f-b1dc-0be092d1ac5d')
        self.assertNotEqual(str(response.data['templates'][0]['sender']), post_data['templates'][0]['sender'])

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        response = self.client.post(path=self.base_url, data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
