from datetime import datetime

from django.conf import settings
from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.exceptions.user import (
    UserTokenInvalidException,
    UserTokenDoesNotExistException,
)
from app_staffing.models.organization import Company
from app_staffing.models.user import User


class UserAuthTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/company_attribute.json',
        'app_staffing/tests/fixtures/auth/users.json',
    ]

    base_url = '/api-token-auth/'

    ACTIVE_USER_USERNAME = 'apiuser@example.com'

    INACTIVE_USER_USERNAME = 'user@example.com'

    ACTIVE_USER_PASSWORD = 'ChangeMe'

    INACTIVE_USER_PASSWORD = 'ChangeMe'

    def test_login_user_inactive(self):
        data = {
            "username": self.INACTIVE_USER_USERNAME,
            "password": self.INACTIVE_USER_PASSWORD,
        }
        response = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'ユーザーが無効化されています。管理者以上のユーザーまでお問い合わせください。')

    def test_login_user_active(self):
        data = {
            "username": self.ACTIVE_USER_USERNAME,
            "password": self.ACTIVE_USER_PASSWORD,
        }
        response = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_on_multi_browser(self):
        data = {
            "username": self.ACTIVE_USER_USERNAME,
            "password": self.ACTIVE_USER_PASSWORD,
        }
        response_login_1 = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response_login_1.status_code, status.HTTP_200_OK)
        token = response_login_1.data['token']

        response_login_2 = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response_login_2.status_code, status.HTTP_200_OK)

        response_profile = self.client.get(path='/app_staffing/my_profile', **{'HTTP_AUTHORIZATION': f'Token {token}'})
        self.assertEqual(response_profile.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_profile.data['detail'], UserTokenDoesNotExistException().default_detail['detail'])

    def test_authentication_user_inactive(self):
        data = {
            "username": self.ACTIVE_USER_USERNAME,
            "password": self.ACTIVE_USER_PASSWORD,
        }
        response_login = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response_login.status_code, status.HTTP_200_OK)
        token = response_login.data['token']

        user_id = response_login.data['user_id']
        user = User.objects.get(id=user_id)
        user.is_active = False
        user.save()

        response_profile = self.client.get(path='/app_staffing/my_profile',  **{'HTTP_AUTHORIZATION': f'Token {token}'})
        self.assertEqual(response_profile.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_profile.data['detail'], UserTokenInvalidException().default_detail)

    def test_authentication_user_company_deactivated_time(self):
        now = datetime.now()

        data = {
            "username": self.ACTIVE_USER_USERNAME,
            "password": self.ACTIVE_USER_PASSWORD,
        }
        response_login = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response_login.status_code, status.HTTP_200_OK)
        token = response_login.data['token']

        user_id = response_login.data['user_id']
        user = User.objects.get(id=user_id)
        company = Company.objects.get(id=user.company.id)
        company.deactivated_time = now
        company.save()

        response_profile = self.client.get(path='/app_staffing/my_profile',  **{'HTTP_AUTHORIZATION': f'Token {token}'})
        self.assertEqual(response_profile.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response_profile.data['detail'], UserTokenInvalidException().default_detail)

    def test_logout_user(self):
        data = {
            "username": self.ACTIVE_USER_USERNAME,
            "password": self.ACTIVE_USER_PASSWORD,
        }
        response_login = self.client.post(path=self.base_url, data=data)
        self.assertEqual(response_login.status_code, status.HTTP_200_OK)
        token = response_login.data['token']

        url_logout = self.base_url + 'logout'
        response_logout = self.client.get(path=url_logout, **{'HTTP_AUTHORIZATION': f'Token {token}'})
        self.assertEqual(response_logout.status_code, status.HTTP_200_OK)
        self.assertEqual(response_logout.data['message'], settings.MESSAGE_LOGOUT_SUCCESS)
