import random
import os

from app_staffing.models.organization import CompanyAttribute
from app_staffing.models.user import MASTER
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse

from app_staffing.models import User, Company

from django.core.signing import dumps
from django.conf import settings

import payjp
from unittest.mock import patch
from django.core import mail

from app_staffing.tests.settings import DEFAULT_COMPANY_ID, API_USER_ID, TEST_USER_NAME, TEST_USER_PASSWORD
from django.shortcuts import reverse

from app_staffing.exceptions.tenant import TenantRegisterPasswordException

import secrets

from app_staffing.exceptions.base import (
    InvalidateTelValueException,
    TelValueIsTooLongException
)


class TenantSiteKeyAPIViewTestCase(APITestCase):
    sitekey = settings.RE_CAPTCHA_SITE_KEY

    def test_api_can_fetch_recaptcha_sitekey(self):
        url = api_reverse("tenant_sitekey")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = response.data
        self.assertEqual(response_data["sitekey"], self.sitekey)


class TenantRegisterViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/tenant/company.json',
        'app_staffing/tests/fixtures/tenant/users.json',
    ]

    def test_post(self):
        email = 'tenantregistertest@example.com'
        password = 'Password0123%.'
        post_data = {'email': email, 'password': password, 'password_confirm': password}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['current_step'], settings.TENANT_REGISTER_STEP_MY_COMPANY)
        self.assertFalse(response.data['is_completed'])
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(['tenantregistertest@example.com'], email.to)
        self.assertEqual('[コモレビ]会員情報登録完了のご連絡', email.subject)

    def test_user_forgot_the_email(self):
        email = 'other_A.admin@other_A.com'
        password = 'password0123'
        post_data = {'email': email, 'password': password, 'password_confirm': password}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['current_step'], settings.TENANT_REGISTER_STEP_MY_COMPANY)
        self.assertFalse(response.data['is_completed'])
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(['other_A.admin@other_A.com'], email.to)
        self.assertEqual('[コモレビ]会員情報登録完了のご連絡', email.subject)

    def test_post_completed(self):
        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        company_attribute.tenant_register_current_step = settings.TENANT_REGISTER_STEP_COMPLETED
        company_attribute.save()
        email = 'admin@example.com'
        password = 'password0123'
        post_data = {'email': email, 'password': password, 'password_confirm': password}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['is_registered'])

    def test_post_password_error(self):
        email = 'tenantregistertest@example.com'
        password = 'password0123'
        post_data = {'email': email, 'password': password, 'password_confirm': 'hogehoge'}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TenantRegisterPasswordException().default_detail)

    def test_post_deleted_user(self):
        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        company_attribute.tenant_register_current_step = settings.TENANT_REGISTER_STEP_COMPLETED
        company_attribute.save()

        email = 'user@example.com'
        password = 'Password0123.'
        post_data = {'email': email, 'password': password, 'password_confirm': password}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(email=email)
        user.is_hidden = True
        user.is_active = False
        user.username = secrets.token_urlsafe(16)
        user.email = secrets.token_urlsafe(16)
        user.save()

        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['current_step'], settings.TENANT_REGISTER_STEP_MY_COMPANY)
        self.assertFalse(response.data['is_completed'])
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(['user@example.com'], email.to)
        self.assertEqual('[コモレビ]会員情報登録完了のご連絡', email.subject)

    def test_post_password_invalid(self):
        email = 'user_invalid_password@example.com'
        password = 'INVALID_PASSWORD'
        post_data = {'email': email, 'password': password, 'password_confirm': password}
        response = self.client.post(path=reverse('tenant_register'), data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TenantMyCompanyViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/tenant/company.json',
        'app_staffing/tests/fixtures/tenant/users.json',
    ]

    def test_get(self):
        auth_token = dumps(API_USER_ID)
        response = self.client.get(path=reverse('tenant_my_company') + '?auth_token=' + auth_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['address'], '東京都渋谷区恵比寿')
        self.assertEqual(response.data['building'], 'ほげビル')
        self.assertEqual(response.data['capital_man_yen'], 2000)
        self.assertEqual(response.data['domain_name'], 'tenant_A.com')
        self.assertEqual(response.data['establishment_date'], '2000-01-01')
        self.assertEqual(response.data['has_distribution'], True)
        self.assertEqual(response.data['has_haken'], False)
        self.assertEqual(response.data['has_invoice_system'], False)
        self.assertEqual(response.data['has_p_mark_or_isms'], False)
        self.assertEqual(response.data['name'], 'Tenant A')

    def test_post(self):
        auth_token = dumps(API_USER_ID)
        response = self.client.post(path=reverse('tenant_my_company'), data={
            'auth_token': auth_token,
            'name': 'Test Update Tennant A'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        tenant_a = Company.objects.get(id=DEFAULT_COMPANY_ID)
        self.assertEqual(tenant_a.name, 'Test Update Tennant A')


class TenantMyProfileViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/tenant/company.json',
        'app_staffing/tests/fixtures/tenant/users.json',
    ]

    def mock_my_profile_pre_step(self):
        # mock pre my_profile step
        user = User.objects.get(pk=API_USER_ID)
        company_attribute = CompanyAttribute.objects.get(company=user.company)
        company_attribute.tenant_register_current_step = settings.TENANT_REGISTER_STEP_MY_PROFILE
        company_attribute.save()

    def test_get(self):
        auth_token = dumps(API_USER_ID)
        response = self.client.get(path=reverse('tenant_my_profile') + '?auth_token=' + auth_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['first_name'], '')
        self.assertEqual(response.data['last_name'], '')
        self.assertEqual(response.data['email'], 'admin@example.com')
        self.assertEqual(response.data['user_service_id'], 'user_4214987350')

    def test_post(self):
        auth_token = dumps(API_USER_ID)
        self.mock_my_profile_pre_step()
        response = self.client.post(path=reverse('tenant_my_profile'), data={
            'auth_token': auth_token,
            'first_name': 'hoge',
            'user_service_id': 'my_user123',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.get(id=API_USER_ID)
        self.assertEqual(user.first_name, 'hoge')
        self.assertEqual(user.user_service_id, 'my_user123')
        company_attribute = CompanyAttribute.objects.get(company=user.company)
        self.assertEqual(company_attribute.tenant_register_current_step, settings.TENANT_REGISTER_STEP_COMPLETED)

    def test_post_invalid_value(self):
        auth_token = dumps(API_USER_ID)
        self.mock_my_profile_pre_step()

        base_data = {
            'auth_token': auth_token,
            'first_name': 'hoge',
        }

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**base_data, **{
            'tel1': 'A',
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 'B',
            'tel3': 3,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
            'tel3': 'C',
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**base_data, **{
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**base_data, **{
            'tel3': 3,
            'tel1': 1,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        """test update my profile with tel longer than 15"""
        data = {**base_data, **{
            'tel1': 12345,
            'tel2': 12345,
            'tel3': 123456,
        }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), TelValueIsTooLongException().default_detail)

        # Test validate for user_service_id
        data = {**base_data, **{'user_service_id': 'user@123', }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['regex_error']
        self.assertEqual(str(response.data['user_service_id']), msg)

        value_here = 'here'
        data = {**base_data, **{'user_service_id': value_here, }}
        response = self.client.post(path=reverse('tenant_my_profile'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['value_not_allowed'].format(value_here)
        self.assertEqual(str(response.data['user_service_id']), msg)

    def test_post_with_invalid_registration_flow(self):
        auth_token = dumps(API_USER_ID)
        response = self.client.post(path=reverse('tenant_my_profile'), data={
            'auth_token': auth_token,
            'first_name': 'hoge',
            'user_service_id': 'my_user123',
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get("detail"), settings.TENANT_REGISTER_FINISH_ERROR_MESSAGE)


class TenantCurrentStepViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/tenant/company.json',
        'app_staffing/tests/fixtures/tenant/users.json',
    ]

    def test_get(self):
        auth_token = dumps(API_USER_ID)
        response = self.client.get(path=reverse('tenant_current_step') + '?auth_token=' + auth_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['current_step'], 2)
        self.assertEqual(response.data['is_completed'], False)


class TenantErrorPatternTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/tenant/company.json',
        'app_staffing/tests/fixtures/tenant/users.json',
    ]

    PASSWORD = "ChangeMe"

    def setUp(self):
        """
        NOTE(joshua-hashimoto):
        masterロールのユーザーがいくら増えても、その中からランダムに値を取得する。
        こうすることで、包括的にテストが通過することができる。
        特定のユーザーでテストが通過しないのであれば、テストかデータどちらかが間違っているといえる。
        """
        self.master_user = random.choice(User.objects.filter(user_role__name="master"))
        self.unusable_user = random.choice(User.objects.filter(company=self.master_user.company).exclude(id=self.master_user.id))

    def test_cannot_re_register_former_master_user(self):
        is_logged_in = self.client.login(username=self.master_user.email, password=self.PASSWORD)
        self.assertTrue(is_logged_in)
        account_delete_url = api_reverse("account")
        account_delete_response = self.client.delete(account_delete_url)
        self.assertEqual(account_delete_response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.logout()
        url = api_reverse("tenant_register")
        password = 'password0123'
        post_data = {'email': self.master_user.email, 'password': password, 'password_confirm': password}
        response = self.client.post(url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_caanot_register_nor_login_when_user_is_inactive(self):
        is_logged_in = self.client.login(username=self.master_user.email, password=self.PASSWORD)
        self.assertTrue(is_logged_in)
        inactive_user_url = api_reverse("users")
        post_data = {
            "source": [str(self.unusable_user.pk)],
            "column": "is_active",
            "value": False
        }
        response = self.client.patch(inactive_user_url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        url = api_reverse("tenant_register")
        post_data = {'email': self.unusable_user.email, 'password': self.PASSWORD, 'password_confirm': self.PASSWORD}
        response = self.client.post(url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_register_nor_login_when_active_user_password_is_wrong(self):
        url = api_reverse("tenant_register")
        password = 'password012345'
        post_data = {'email': self.unusable_user.email, 'password': password, 'password_confirm': password}
        response = self.client.post(url, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
