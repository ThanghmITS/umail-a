from datetime import datetime
from rest_framework import status
from rest_framework.test import APITestCase
from freezegun import freeze_time
from app_staffing.models import User, EmailNotificationRule

from app_staffing.tests.views.validators.generals import IsPagenatedResponse
from app_staffing.tests.views.validators.notifications import IsEmailNotificationRule
from app_staffing.tests.factories.notifications import EmailNotificationConditionFactory, EmailNotificationRuleFactory
from app_staffing.tests.views.base import APICRUDTestMixin, PerUserViewAccessControlTestMixin, MultiTenancyTestMixin, \
    TestQueryParamErrorMixin
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from django.http import QueryDict
from django.conf import settings

BASE_URL = '/app_staffing/notifications/share-emails'

data_for_post = {
    'name': 'Notification Rule X',
    'master_rule': 'all',
    'conditions': [
        {
            'extraction_type': 'keyword',
            'target_type': 'subject',
            'condition_type': 'include',
            'value': 'newbie'
        },
        {
            'extraction_type': 'keyword',
            'target_type': 'content',
            'condition_type': 'exclude',
            'value': 'experienced'
        }
    ]
}

data_for_patch = {
    'name': 'Notification Rule X Updated',
    'master_rule': 'any',
    'conditions': [
        {
            'extraction_type': 'keyword',
            'target_type': 'content',
            'condition_type': 'exclude',
            'value': 'python'
        },
        {
            'extraction_type': 'keyword',
            'target_type': 'subject',
            'condition_type': 'include',
            'value': 'ruby'
        },
        {
            'extraction_type': 'keyword',
            'target_type': 'from_address',
            'condition_type': 'include',
            'value': 'user@example.com'
        }
    ]
}


class EmailNotificationCRUDTestCase(APICRUDTestMixin, APITestCase, TestQueryParamErrorMixin):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    base_url = '/app_staffing/notifications/share-emails'

    list_response_validator_class = IsPagenatedResponse
    resource_validator_class = IsEmailNotificationRule

    post_test_cases = [
        ('fully_qualified', data_for_post)
    ]
    patch_test_cases = [
        ('fully_qualified', data_for_patch)
    ]

    @classmethod
    def setUpTestData(cls):
        user = User.objects.get(username=TEST_USER_NAME)
        rule = EmailNotificationRuleFactory.create(created_user=user, modified_user=user)
        EmailNotificationConditionFactory.create_batch(3, rule=rule)

    def setUp(self):
        super().setUp()
        self.resource_id = EmailNotificationRule.objects.all()[0].id

    def test_over_max_length(self):
        data = {
            'name': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'master_rule': 'all',
            'conditions': [
                {
                    'extraction_type': 'keyword',
                    'target_type': 'subject',
                    'condition_type': 'include',
                    'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc'
                },
                {
                    'extraction_type': 'keyword',
                    'target_type': 'content',
                    'condition_type': 'exclude',
                    'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc'
                }
            ]
        }
        response = self.client.post(path='/app_staffing/notifications/share-emails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['notification']['name']['message'])
        self.assertEqual(str(response.data['conditions'][0]), settings.LENGTH_VALIDATIONS['notification']['conditions']['message'])
        self.assertEqual(str(response.data['conditions'][1]), settings.LENGTH_VALIDATIONS['notification']['conditions']['message'])

        response = self.client.patch(path=f'/app_staffing/notifications/share-emails/{EmailNotificationRule.objects.all()[0].id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['notification']['name']['message'])
        self.assertEqual(str(response.data['conditions'][0]), settings.LENGTH_VALIDATIONS['notification']['conditions']['message'])
        self.assertEqual(str(response.data['conditions'][1]), settings.LENGTH_VALIDATIONS['notification']['conditions']['message'])


class EmailNotificationOrderingTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    @classmethod
    def setUpTestData(cls):
        user = User.objects.get(username=TEST_USER_NAME)
        # Create 10 rules, and each rule has 10 conditions.
        rules = EmailNotificationRuleFactory.create_batch(10, created_user=user, modified_user=user)
        conditions = [EmailNotificationConditionFactory.create(rule=rule) for rule in rules]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_ordering(self):
        response = self.client.get(path=BASE_URL)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        rules = response.data['results']

        for i in range(len(rules) - 1):
            # Check if the ordering of rule is based on created time (asc)
            self.assertTrue(rules[i]['created_time'] > rules[i + 1]['created_time'])
            for j in range(len(rules[i]['conditions']) - 1):
                # Check if the conditions of each rules are sorted by registered order.
                # Note: Value must be a string and represents sequence number. (created via factory)
                self.assertTrue(int(rules[i]['conditions'][j]['value']) < int(rules[i]['conditions'][j + 1]['value']))


class EmailNotificationAccessControlTestCase(PerUserViewAccessControlTestMixin, APITestCase):
    base_url = '/app_staffing/notifications/share-emails'
    data_for_post = {
        'name': 'Notification Rule X',
        'conditions': [{
            'extraction_type': 'keyword',
            'target_type': 'subject',
            'condition_type': 'include',
            'value': 'newbie'
        }]
    }


class EmailNotificationMultiTenacyTestCase(MultiTenancyTestMixin, APITestCase):
    """ A multi-tenancy test for EmailNotification resources.
    Note: This API provides a per-user view, So basically, we do not need to do this test.
    """
    base_url = '/app_staffing/notifications/share-emails'
    test_patterns = ('list', 'get', 'post', 'patch', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # Note, this API provides per-user view, so we need to specify created_user for test data.
        cls.rule1 = EmailNotificationRuleFactory(name='A Rule on Tenant A', company=cls.company_1, created_user=cls.user1)
        cls.rule2 = EmailNotificationRuleFactory(name='A Rule on Tenant B', company=cls.company_2, created_user=cls.user2)
        EmailNotificationConditionFactory(rule=cls.rule1)
        EmailNotificationConditionFactory(rule=cls.rule2)
        cls.resource_id_for_tenant_1 = cls.rule1.id
        cls.resource_id_for_tenant_2 = cls.rule2.id

        cls.post_data_for_tenant_1 = {
            'name': 'Notification Rule A',
            'conditions': [{
                'extraction_type': 'keyword',
                'target_type': 'subject',
                'condition_type': 'include',
                'value': 'newbie'
            }]
        }

        cls.post_data_for_tenant_2 = {
            'name': 'Notification Rule B',
            'conditions': [{
                'extraction_type': 'keyword',
                'target_type': 'subject',
                'condition_type': 'include',
                'value': 'veteran'
            }]
        }

        cls.patch_data_for_tenant_1 = {
            'name': 'Notification Rule A Updated',
            'conditions': [
                {
                    'extraction_type': 'keyword',
                    'target_type': 'content',
                    'condition_type': 'exclude',
                    'value': 'python'
                }
            ]
        }

        cls.patch_data_for_tenant_2 = {
            'name': 'Notification Rule B Updated',
            'conditions': [
                {
                    'extraction_type': 'keyword',
                    'target_type': 'content',
                    'condition_type': 'exclude',
                    'value': 'ruby'
                }
            ]
        }


class EmailNotificationRuleListViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/email_notification_rules.json',
        'app_staffing/tests/fixtures/email_notification_conditions.json',
    ]

    query_param_inactive_filter = '?inactive_filter=use_filter'
    query_param_name = '?name=test'
    query_param_master_rule = '?master_rule=all'
    query_param_rules = '?rules=%5B%7B%22target_type%22%3A%22subject%22%2C%22condition_type%22%3A%22include%22%2C%22value%22%3A%22hoge%22%7D%5D'
    query_param_all = '?inactive_filter=use_filter&master_rule=all&name=test&rules=%5B%7B%22target_type%22%3A%22subject%22%2C%22condition_type%22%3A%22include%22%2C%22value%22%3A%22hoge%22%7D%2C%7B%22target_type%22%3A%22content%22%2C%22condition_type%22%3A%22include%22%2C%22value%22%3A%22fuga%22%7D%5D'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_contact_emails(self):
        response = self.client.get(path='/app_staffing/notifications/share-emails' + self.query_param_inactive_filter, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)
        self.assertEqual(response.data['results'][0]['is_active'], True)
        self.assertEqual(response.data['results'][1]['is_active'], True)

        response = self.client.get(path='/app_staffing/notifications/share-emails' + self.query_param_name, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)
        self.assertEqual(response.data['results'][0]['name'], 'test notification rule')
        self.assertEqual(response.data['results'][1]['name'], 'test notification rule')

        response = self.client.get(path='/app_staffing/notifications/share-emails' + self.query_param_master_rule, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)
        self.assertEqual(response.data['results'][0]['master_rule'], 'all')
        self.assertEqual(response.data['results'][1]['master_rule'], 'all')

        response = self.client.get(path='/app_staffing/notifications/share-emails' + self.query_param_rules, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)
        self.assertEqual(response.data['results'][0]['conditions'][0]['target_type'], 'subject')
        self.assertEqual(response.data['results'][0]['conditions'][0]['condition_type'], 'include')
        self.assertEqual(response.data['results'][0]['conditions'][0]['value'], 'hogehoge')
        self.assertEqual(response.data['results'][1]['conditions'][0]['target_type'], 'subject')
        self.assertEqual(response.data['results'][1]['conditions'][0]['condition_type'], 'include')
        self.assertEqual(response.data['results'][1]['conditions'][0]['value'], 'hogehoge')

        response = self.client.get(path='/app_staffing/notifications/share-emails' + self.query_param_all, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['is_active'], True)
        self.assertEqual(response.data['results'][0]['name'], 'test notification rule')
        self.assertEqual(response.data['results'][0]['master_rule'], 'all')
        self.assertEqual(response.data['results'][0]['conditions'][0]['target_type'], 'subject')
        self.assertEqual(response.data['results'][0]['conditions'][0]['condition_type'], 'include')
        self.assertEqual(response.data['results'][0]['conditions'][0]['value'], 'hogehoge')
        self.assertEqual(response.data['results'][0]['conditions'][1]['target_type'], 'content')
        self.assertEqual(response.data['results'][0]['conditions'][1]['condition_type'], 'include')
        self.assertEqual(response.data['results'][0]['conditions'][1]['value'], 'fugafuga')

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/notifications/share-emails', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class EmailNotificationRuleBulkDeleteTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/email_notification_rules.json',
        'app_staffing/tests/fixtures/email_notification_conditions.json',
    ]

    delete_item_ids = [
        '02724441-8967-4008-AB1D-600035D5A161',
        '02724441-8967-4008-AB1D-600035D5A162'
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_contact_emails(self):
        response = self.client.get(path='/app_staffing/notifications/share-emails', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 3)

        response = self.client.delete(path='/app_staffing/notifications/share-emails', data={'source': self.delete_item_ids}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/notifications/share-emails', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

class SystemNotificationListViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/system_notifications.json',
        'app_staffing/tests/fixtures/user_system_notifications.json',
    ]
    fake_current_date = datetime(2021, 10, 9)

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_notifications(self):
        with freeze_time(self.fake_current_date):
            response = self.client.get(path='/app_staffing/system_notifications', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4)
        self.assertEqual(response.data['results'][0]['is_checked'], False)
        self.assertEqual(response.data['results'][0]['title'], 'これはテストデータですよ7')
        self.assertEqual(response.data['results'][1]['is_checked'], False)
        self.assertEqual(response.data['results'][1]['title'], 'これはテストデータですよ3')
        self.assertEqual(response.data['results'][2]['is_checked'], False)
        self.assertEqual(response.data['results'][2]['title'], 'これはテストデータですよ2')
        self.assertEqual(response.data['results'][3]['is_checked'], True)
        self.assertEqual(response.data['results'][3]['title'], 'これはテストデータですよ')

    def test_get_notifications_unread(self):
        with freeze_time(self.fake_current_date):
            response = self.client.get(path='/app_staffing/system_notifications?unread=True', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 3)
        self.assertEqual(response.data['results'][0]['is_checked'], False)
        self.assertEqual(response.data['results'][0]['title'], 'これはテストデータですよ7')
        self.assertEqual(response.data['results'][1]['is_checked'], False)
        self.assertEqual(response.data['results'][1]['title'], 'これはテストデータですよ3')
        self.assertEqual(response.data['results'][2]['is_checked'], False)
        self.assertEqual(response.data['results'][2]['title'], 'これはテストデータですよ2')

    def test_post_and_get(self):
        with freeze_time(self.fake_current_date):
            response = self.client.post(path='/app_staffing/system_notifications', data={'source': [2,3]}, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['is_checked'], True)
        self.assertEqual(response.data[1]['is_checked'], True)

        with freeze_time(self.fake_current_date):
            response = self.client.get(path='/app_staffing/system_notifications', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4)
        self.assertEqual(response.data['results'][0]['is_checked'], False)
        self.assertEqual(response.data['results'][0]['title'], 'これはテストデータですよ7')
        self.assertEqual(response.data['results'][1]['is_checked'], True)
        self.assertEqual(response.data['results'][1]['title'], 'これはテストデータですよ3')
        self.assertEqual(response.data['results'][2]['is_checked'], True)
        self.assertEqual(response.data['results'][2]['title'], 'これはテストデータですよ2')
        self.assertEqual(response.data['results'][3]['is_checked'], True)
        self.assertEqual(response.data['results'][3]['title'], 'これはテストデータですよ')

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/system_notifications', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
