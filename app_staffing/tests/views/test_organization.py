from freezegun import freeze_time
from django.utils import timezone
from app_staffing.models.organization import Company, CompanyAttribute, ContactTagAssignment, DisplaySetting

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.models import Organization, Contact, OrganizationComment, OrganizationCategory, OrganizationCountry, OrganizationEmployeeNumber, ContactComment, User, OrganizationBranch,\
    ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference, ContactPersonnelSkillPreference, ContactPreference, \
    CompanyAttribute, Plan, PlanPaymentError, Tag, ExceptionalOrganization
from app_staffing.views import OrganizationCsvUploadView, ContactCsvUploadView
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, NORMAL_USER_NAME, NORMAL_USER_PASSWORD, DEFAULT_COMPANY_ID, GET_DEFAULT_COMPANY, ANOTHER_TENANT_NORMAL_USER_NAME, ANOTHER_TENANT_NORMAL_USER_PASSWORD

from app_staffing.tests.views.base import APICRUDTestMixin, SubTestMixin, CommentApiTestMixin, CsvApiTestMixin, \
    MultiTenancyTestMixin, MultiTenancySetupMixin, ReferenceViewMultiTenancyMixin, CsvDownloadViewMultiTenancyTestMixin, \
    TestQueryParamErrorMixin
from app_staffing.tests.factories.organization import OrganizationFactory, ContactFactory,\
    OrganizationCommentFactory, ContactCommentFactory, TagFactory
from app_staffing.tests.views.validators.generals import IsPagenatedResponse
from app_staffing.tests.views.validators.organizations import IsLocation, IsCompany, \
    IsOrganizationSummary, IsOrganization, IsOrganizationStats, IsComment, IsContact, IsTag

from django.core.cache import cache
from django.conf import settings
from django.shortcuts import reverse

from uuid import UUID

from rest_framework.authtoken.models import Token
import datetime
from freezegun import freeze_time
from django.test import override_settings

from app_staffing.exceptions.base import TemplateNameBlankException, TemplateNameRegisteredException, TemplateRegisterLimitException, BulkOperationLimitException
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.exceptions.organization import CorporateNumberNotUniqueException

import json
import copy

from app_staffing.serializers.organization import AlreadyRegisteredEmailException
from app_staffing.exceptions.base import (
    InvalidateTelValueException,
    InvalidateFaxValueException,
    TelValueIsTooLongException,
    FaxValueIsTooLongException
)

class LocationsViewTestCase(APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/locations.json'
    ]

    base_url = '/app_staffing/locations'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_locations_list(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        IsPagenatedResponse(response.data).validate()
        results = response.data["results"]

        for entry in results:
            validator = IsLocation(entry)
            validator.validate()


class MyCompanyViewTestCase(APITestCase):
    """A Test Case for my Company View"""

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/company.json',
    ]

    base_patch_data = {
      "name": "Tenant A",
      "domain_name": "tenant_A.com",
      "address": "東京都渋谷区恵比寿",
      "capital_man_yen": 2000,
      "establishment_date": "2000-01-01",
      "has_p_mark_or_isms": False,
      "has_invoice_system": False,
      "has_haken": False,
      "has_distribution": True,
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_company_info(self):
        response = self.client.get(path='/app_staffing/my_company', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data, dict))

        validator = IsCompany(response.data)
        validator.validate()

    def test_update_company_info(self):
        new_company_name = 'An Updated Company'
        new_domain_name = 'newexample.com'
        new_capital = 10000
        patch_data = {
            'name': new_company_name,
            'domain_name': new_domain_name,
            'capital_man_yen': new_capital
        }
        patch_data = {**self.base_patch_data, **patch_data}

        response = self.client.patch(path='/app_staffing/my_company', data=patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        validator = IsCompany(response.data)
        validator.validate()

        self.assertEqual(response.data['name'], new_company_name)
        self.assertEqual(response.data['domain_name'], new_domain_name)
        self.assertEqual(response.data['capital_man_yen'], new_capital)

    def test_put_is_prohibited(self):
        new_company_name = 'An Updated Company'
        put_data = {'name': new_company_name}

        response = self.client.put(path='/app_staffing/my_company', data=put_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_is_prohibited(self):
        response = self.client.delete(path='/app_staffing/my_company', format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_over_max_length(self):
        data = {
            "name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "building": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "domain_name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "capital_man_yen": 11111111111111,
            "capital_man_yen_required_for_transactions": 11111111111111,
            "establishment_year": 1111,
        }
        data = {**self.base_patch_data, **data}
        response = self.client.patch(path='/app_staffing/my_company', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['company']['name']['message'])
        self.assertEqual(str(response.data['domain_name']), settings.LENGTH_VALIDATIONS['company']['domain_name']['message'])
        self.assertEqual(str(response.data['building']), settings.LENGTH_VALIDATIONS['company']['building']['message'])
        self.assertEqual(str(response.data['capital_man_yen']), settings.LENGTH_VALIDATIONS['company']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.LENGTH_VALIDATIONS['company']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.LENGTH_VALIDATIONS['company']['establishment_year']['message'])

    def test_under_min_value(self):
        data = {
            "capital_man_yen": 0,
            "capital_man_yen_required_for_transactions": 0,
            "establishment_year": 0,
        }
        data = {**self.base_patch_data, **data}
        response = self.client.patch(path='/app_staffing/my_company', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['capital_man_yen']), settings.VALUE_RANGE_VALIDATIONS['company']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.VALUE_RANGE_VALIDATIONS['company']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.VALUE_RANGE_VALIDATIONS['company']['establishment_year']['message'])

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        data = {}
        response = self.client.patch(path='/app_staffing/my_company', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_normal_user_cannot_patch(self):
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        data = {}
        response = self.client.patch(path='/app_staffing/my_company', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_not_null_value(self):
        response = self.client.patch(path='/app_staffing/my_company', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['establishment_date']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['address']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['domain_name']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['capital_man_yen']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_p_mark_or_isms']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_invoice_system']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_haken']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_distribution']), settings.NOT_NULL_VALIDATIONS['company']['message'])

        data = {
            "name": None,
            "domain_name": None,
            "address": None,
            "capital_man_yen": None,
            "establishment_date": None,
            "has_p_mark_or_isms": None,
            "has_invoice_system": None,
            "has_haken": None,
            "has_distribution": None,
        }
        response = self.client.patch(path='/app_staffing/my_company', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['establishment_date']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['address']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['domain_name']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['capital_man_yen']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_p_mark_or_isms']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_invoice_system']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_haken']), settings.NOT_NULL_VALIDATIONS['company']['message'])
        self.assertEqual(str(response.data['has_distribution']), settings.NOT_NULL_VALIDATIONS['company']['message'])

class OrganizationViewTestCase(APICRUDTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_branches.json',
    ]

    base_url = '/app_staffing/organizations'
    resource_id = '749303ca-7701-463e-83a8-8a5a35df1646'
    resource_validator_class_for_list = IsOrganizationSummary
    resource_validator_class = IsOrganization

    # Used in TestQueryParamErrorMixin
    filter_params = {
        'name': 'Trump',
        'address': 'Tokyo',
        "building": "building",
        "domain_name": "example.com",
        "settlement_month": 1,
        'corporate_number': '1234567890',
        'country': 'Japan',
        'category': 'Cat A',
        "establishment_year": 10,
        'is_blacklisted': False,
        'old_id': 1,
    }

    post_test_cases = [
        ('full', {
            "name": "An Example Company",
            "corporate_number": 1234567890123,  # Max 13 digit in japan.
            "category": "client",
            "employee_number": "very_low",
            "contract": True,
            "country": "JP",
            "address": "東京都港区東麻布X-X-X",
            "building": "building",
            "domain_name": "example.com",
            "tel1": "111",
            "tel2": "222",
            "tel3": "333",
            "fax1": "811",
            "fax2": "822",
            "fax3": "833",
            "settlement_month": 1,
            "capital_man_yen": 1000,
            "capital_man_yen_required_for_transactions": 2000,
            "establishment_date": "2001-01-01",
            "establishment_year": 10,
            "score": 3,
            "has_p_mark_or_isms": True,
            "has_invoice_system": True,
            "has_haken": True,
            "has_distribution": True,
            "p_mark_or_isms": True,
            "invoice_system": True,
            "haken": True,
            "is_blacklisted": False,
            "initial_comment": {"content": "test comment", "is_important": True},
            "branches": [
                {
                    "name": "支店A",
                    "address": "支店A address",
                    "tel1": "01",
                    "tel2": "02",
                    "tel3": "03",
                    "fax1": "11",
                    "fax2": "12",
                    "fax3": "13",
                },
                {
                    "name": "支店B",
                },
            ]
        }),
        ('minimum', {"name": "An Example Company", "category": "client", "employee_number": "very_low", "score": 3}),
    ]

    # Used after post
    get_after_post_test_cases = [
        ('full', {
            "address": "東京都港区東麻布X-X-X",
            "branch_address": "支店A address",
            "branch_tel1": "01",
            "branch_tel2": "02",
            "branch_tel3": "03",
            "branch_fax1": "11",
            "branch_fax2": "12",
            "branch_fax3": "13",
            "branch_name": "支店A",
            "capital_man_yen_gt": 999,
            "capital_man_yen_lt": 1001,
            "capital_man_yen_required_for_transactions_gt": 1999,
            "capital_man_yen_required_for_transactions_lt": 2001,
            "category": "prospective,approached,exchanged,client",
            "contract": True,
            "corporate_number": 1234567890123,
            "country": "JP,CN,OTHER,KR",
            "domain_name": "example.com",
            "employee_number": "very_low,low,middle,semi_middle,high,very_high",
            "establishment_date_gte": "2001-01-01",
            "establishment_date_lte": "2001-01-01",
            "establishment_year_gt": 9,
            "establishment_year_lt": 11,
            "tel1": "111",
            "tel2": "222",
            "tel3": "333",
            "fax1": "811",
            "fax2": "822",
            "fax3": "833",
            "has_distribution": True,
            "ignore_blocklist_filter": "use_blocklist_filter",
            "ignore_filter": "use_filter",
            "license": "has_invoice_system,has_haken,has_p_mark_or_isms",
            "license_required_for_transactions": "p_mark_or_isms,invoice_system,haken",
            "name": "An Example Company",
            "score_gte": 3,
            "score_eq": 3,
            "settlement_month": "1,2,3,4,5,6,7,8,10,9,12,11",
            "page": 1,
            "page_size": 1,
            "created_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "comment_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "modified_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05e"
        }),
        ('adress_not_exist', {
            "address": "東京都港区東麻布Z-Z-Z",
            "page_size": 0,
        }),
        ('branch_address_not_exist', {
            "branch_address": "支店Z address",
            "page_size": 0,
        }),
        ('branch_tel1_not_exist', {
            "branch_tel1": "81",
            "page_size": 0,
        }),
        ('branch_tel2_not_exist', {
            "branch_tel2": "82",
            "page_size": 0,
        }),
        ('branch_tel3_not_exist', {
            "branch_tel3": "83",
            "page_size": 0,
        }),
        ('branch_fax1_not_exist', {
            "branch_fax1": "91",
            "page_size": 0,
        }),
        ('branch_fax2_not_exist', {
            "branch_fax2": "92",
            "page_size": 0,
        }),
        ('branch_fax3_not_exist', {
            "branch_fax3": "93",
            "page_size": 0,
        }),
        ('branch_name_not_exist', {
            "branch_name": "支店Z",
            "page_size": 0,
        }),
        ('capital_man_yen_gt_not_exist', {
            "capital_man_yen_gt": 2001,
            "page_size": 0,
        }),
        ('capital_man_yen_lt_not_exist', {
            "capital_man_yen_lt": 149,
            "page_size": 0,
        }),
        ('capital_man_yen_required_for_transactions_gt_not_exist', {
            "capital_man_yen_required_for_transactions_gt": 2001,
            "page_size": 0,
        }),
        ('capital_man_yen_required_for_transactions_lt_not_exist', {
            "capital_man_yen_required_for_transactions_lt": 99,
            "page_size": 0,
        }),
        ('corporate_number_not_exist', {
            "corporate_number": 9876543210987,
            "page_size": 0,
        }),
        ('domain_name_not_exist', {
            "domain_name": "example.org",
            "page_size": 0,
        }),
        ('employee_number_not_exist', {
            "employee_number": "very_high",
            "page_size": 0,
        }),
        ('establishment_date_gte_not_exist', {
            "establishment_date_gte": "2002-01-01",
            "page_size": 0,
        }),
        ('establishment_date_lte_not_exist', {
            "establishment_date_lte": "2000-01-01",
            "page_size": 0,
        }),
        ('establishment_year_lt_not_exist', {
            "establishment_year_gt": 11,
            "page_size": 0,
        }),
        ('establishment_year_lt_not_exist', {
            "establishment_year_lt": 9,
            "page_size": 0,
        }),
        ('tel1_not_exist', {
            "tel1": "444",
            "page_size": 0,
        }),
        ('tel2_not_exist', {
            "tel2": "555",
            "page_size": 0,
        }),
        ('tel3_not_exist', {
            "tel3": "666",
            "page_size": 0,
        }),
        ('fax1_not_exist', {
            "fax1": "844",
            "page_size": 0,
        }),
        ('fax2_not_exist', {
            "fax2": "855",
            "page_size": 0,
        }),
        ('fax3_not_exist', {
            "fax3": "866",
            "page_size": 0,
        }),
        ('name_not_exist', {
            "name": "Non-existent company",
            "page_size": 0,
        }),
        ('score_gte_not_exist', {
            "score_gte": 4,
            "page_size": 0,
        }),
        ('score_eq_not_exist', {
            "score_eq": 4,
            "page_size": 0,
        }),
        ('created_user_not_exist', {
            "created_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05f",
            "page_size": 0,
        }),
        ('comment_user_not_exist', {
            "comment_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05f",
            "page_size": 0,
        }),
        ('smodified_user_not_exist', {
            "modified_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05f",
            "page_size": 0,
        }),
        ('contract_false', {
            "contract": False,
            "page_size": 1,
        }),
        ('contract_true', {
            "contract": True,
            "page_size": 2,
        }),
        ('has_distribution_false', {
            "has_distribution": False,
            "page_size": 0,
        }),
        ('has_distribution_true', {
            "has_distribution": True,
            "page_size": 1,
        }),
        ('license_all', {
            "license": "has_invoice_system,has_haken,has_p_mark_or_isms",
            "page_size": 1,
        }),
        ('license_not_specified', {
            "license": "",
            "page_size": 2,
        }),
        ('license_required_for_transactions_all', {
            "license_required_for_transactions": "p_mark_or_isms,invoice_system,haken",
            "page_size": 1,
        }),
        ('license_required_for_transactions_not_specified', {
            "license_required_for_transactions": "",
            "page_size": 2,
        }),
    ]

    patch_test_cases = [
        ('full', {
            "name": "An Updated Company",
            "cooporate_number": 3214567890123,  # Max 13 digit in japan.
            "category": "prospective",
            "employee_number": "very_low",
            "contract": False,
            "country": "CN",
            "address": "大阪府のどこか",
            "building": "building",
            "domain_name": "example.com",
            "settlement_month": 1,
            "capital_man_yen": 2000,
            "establishment_year": 10,
            "score": 3,
            "is_blacklisted": True,
            "branches": [
                {
                    "id": "749303ca-7701-463e-83a8-8a5a35df1647",
                    "name": "支店C",
                },
                {
                    "name": "支店D",
                },
            ]
        }),
        ('minimum', {"name": "An New Name of an Example Company", "category": "prospective", "score": 3}),
    ]

    skip_tests = ('put',)

    def test_post_with_comment(self):
        data = {"name": "An Example Company With Comment", "category": "client", "employee_number": "very_low", "domain_name": "example.com", "country": "JP", "score": 3, "settlement_month": 1, "initial_comment": {"content": "test comment", "is_important": True}}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comment = OrganizationComment.objects.get(organization_id=response.data['id'])
        self.assertEqual(comment.content, 'test comment')
        self.assertEqual(comment.is_important, True)

    def test_post_with_branch(self):
        data = {
            "name": "An Example Company With Branch",
            "category": "client",
            "employee_number": "very_low",
            "country": "JP",
            "score": 3,
            "branches": [{"name": "支店A"}, {"name": "支店B"}, {"name": "支店C"}]
        }
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        branches = OrganizationBranch.objects.filter(organization_id=response.data['id'])
        self.assertEqual(len(branches), 3)

    def test_patch_with_branch(self):
        data = {
            "name": "An Example Company With Branch",
            "category": "client",
            "employee_number": "very_low",
            "country": "JP",
            "score": 3,
            "branches": [{"id": "749303ca-7701-463e-83a8-8a5a35df1647", "name": "支店D"}, {"name": "支店B"}]
        }
        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        branches = OrganizationBranch.objects.filter(organization_id=response.data['id'])
        self.assertEqual(len(branches), 2)

        updated = OrganizationBranch.objects.get(id='749303ca-7701-463e-83a8-8a5a35df1647')
        self.assertEqual(updated.name, "支店D")

    def test_bulk_delete(self):
        data = { 'source': ['749303ca-7701-463e-83a8-8a5a35df1646']}
        response = self.client.delete(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_bulk_delete_limit_error(self):
        data = { 'source': [
            'sample_uuid_xxxx_yyyy_1',
            'sample_uuid_xxxx_yyyy_2',
            'sample_uuid_xxxx_yyyy_3',
            'sample_uuid_xxxx_yyyy_4',
            'sample_uuid_xxxx_yyyy_5',
            'sample_uuid_xxxx_yyyy_6',
            'sample_uuid_xxxx_yyyy_7',
            'sample_uuid_xxxx_yyyy_8',
            'sample_uuid_xxxx_yyyy_9',
            'sample_uuid_xxxx_yyyy_10',
            'sample_uuid_xxxx_yyyy_11',
        ]}
        response = self.client.delete(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

    def test_post_over_max_length(self):
        data = {
            "name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "category": "client",
            "employee_number": "very_low",
            "contract": True,
            "domain_name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "country": "JP",
            "score": 3,
            "corporate_number": 11111111111111,
            "address": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "building": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "tel1": "11111",
            "tel2": "11111",
            "tel3": "111111",
            "fax1": "11111",
            "fax2": "11111",
            "fax3": "111111",
            "capital_man_yen": 11111111111111,
            "capital_man_yen_required_for_transactions": 11111111111111,
            "establishment_year": 1111,
            "branches": [{
                "name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
                "address": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
                "building": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
                "tel1": "11111",
                "tel2": "11111",
                "tel3": "111111",
                "fax1": "11111",
                "fax2": "11111",
                "fax3": "111111",
            }]
        }
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['organization']['name']['message'])
        self.assertEqual(str(response.data['domain_name']), settings.LENGTH_VALIDATIONS['organization']['domain_name']['message'])
        self.assertEqual(str(response.data['corporate_number']), settings.LENGTH_VALIDATIONS['organization']['corporate_number']['message'])
        self.assertEqual(str(response.data['building']), settings.LENGTH_VALIDATIONS['organization']['building']['message'])
        self.assertEqual(str(response.data['capital_man_yen']), settings.LENGTH_VALIDATIONS['organization']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.LENGTH_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.LENGTH_VALIDATIONS['organization']['establishment_year']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['organization']['tel']['message'])
        self.assertEqual(str(response.data['fax']), settings.LENGTH_VALIDATIONS['organization']['fax']['message'])
        self.assertEqual(str(response.data['branches'][0]['name']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['name']['message'])
        self.assertEqual(str(response.data['branches'][0]['building']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['building']['message'])
        self.assertEqual(str(response.data['branches'][0]['tel']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['tel']['message'])
        self.assertEqual(str(response.data['branches'][0]['fax']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['fax']['message'])

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['organization']['name']['message'])
        self.assertEqual(str(response.data['domain_name']), settings.LENGTH_VALIDATIONS['organization']['domain_name']['message'])
        self.assertEqual(str(response.data['corporate_number']), settings.LENGTH_VALIDATIONS['organization']['corporate_number']['message'])
        self.assertEqual(str(response.data['building']), settings.LENGTH_VALIDATIONS['organization']['building']['message'])
        self.assertEqual(str(response.data['capital_man_yen']), settings.LENGTH_VALIDATIONS['organization']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.LENGTH_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.LENGTH_VALIDATIONS['organization']['establishment_year']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['organization']['tel']['message'])
        self.assertEqual(str(response.data['fax']), settings.LENGTH_VALIDATIONS['organization']['fax']['message'])
        self.assertEqual(str(response.data['branches'][0]['name']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['name']['message'])
        self.assertEqual(str(response.data['branches'][0]['building']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['building']['message'])
        self.assertEqual(str(response.data['branches'][0]['tel']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['tel']['message'])
        self.assertEqual(str(response.data['branches'][0]['fax']), settings.LENGTH_VALIDATIONS['organization']['branches']['columns']['fax']['message'])

    def test_ignore_filter(self):
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=True,
        )
        Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=False,
        )
        Organization.objects.create(
            name="has_invoice_system_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=False,
            has_haken=True,
            has_distribution=True,
        )
        Organization.objects.create(
            name="has_invoice_system_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=False,
            has_haken=True,
            has_distribution=False,
        )
        Organization.objects.create(
            name="has_haken_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=False,
            has_distribution=True,
        )
        Organization.objects.create(
            name="has_haken_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=False,
            has_distribution=False,
        )
        Organization.objects.create(
            name="capital_man_yen_under_has_distribution_true",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=True,
        )
        Organization.objects.create(
            name="capital_man_yen_under_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=False,
        )
        Organization.objects.create(
            name="establishment_date_has_distribution_true",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            establishment_date=datetime.datetime.now().strftime("%Y-%m-%d"),
            has_distribution=True,
        )
        Organization.objects.create(
            name="establishment_date_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            establishment_date=datetime.datetime.now().strftime("%Y-%m-%d"),
            has_distribution=False,
        )
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 7) # 商流を「抜ける」の5件 + fixturesの2件(関連パラメータがNullのため条件外)

        data = {
            "ignore_filter": "ignore_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 12) # fixtureの2件と上段で定義した10件

    def test_exceptional_organizations(self):
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        organizations = Organization.objects.all()
        for organization in organizations:
            organization.has_p_mark_or_isms = False
            organization.save()

        organization = Organization.objects.create(
            name="exceptional_organization",
            category='client',
            score=3,
            company=company,
            capital_man_yen=10000,
            has_p_mark_or_isms=False,
            has_invoice_system=False,
            has_haken=False,
        )
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        ExceptionalOrganization.objects.create(
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=GET_DEFAULT_COMPANY())
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_ignore_blocklist_filter(self):
        Organization.objects.create(
            name="is_blacklisted_false",
            category='client',
            score=3,
            company=GET_DEFAULT_COMPANY(),
            is_blacklisted=False,
        )
        Organization.objects.create(
            name="is_blacklisted_true",
            category='client',
            score=3,
            company=GET_DEFAULT_COMPANY(),
            is_blacklisted=True,
        )
        data = {
            "ignore_blocklist_filter": "use_blocklist_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        data = {
            "ignore_blocklist_filter": "ignore_blocklist_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4)

    def test_not_null_value(self):
        response = self.client.post(path='/app_staffing/organizations', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['category']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['score']), settings.NOT_NULL_VALIDATIONS['organization']['message'])

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['category']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['score']), settings.NOT_NULL_VALIDATIONS['organization']['message'])

        data = {
            "name": None,
            "category": None,
            "score": None,
        }
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['category']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['score']), settings.NOT_NULL_VALIDATIONS['organization']['message'])

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['category']), settings.NOT_NULL_VALIDATIONS['organization']['message'])
        self.assertEqual(str(response.data['score']), settings.NOT_NULL_VALIDATIONS['organization']['message'])

    def test_under_min_value(self):
        data = {
            "name": "An Example Company",
            "category": "client",
            "score": 3,
            "capital_man_yen": 0,
            "capital_man_yen_required_for_transactions": 0,
            "establishment_year": 0,
        }
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['capital_man_yen']), settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.VALUE_RANGE_VALIDATIONS['organization']['establishment_year']['message'])

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['capital_man_yen']), settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen']['message'])
        self.assertEqual(str(response.data['capital_man_yen_required_for_transactions']), settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['message'])
        self.assertEqual(str(response.data['establishment_year']), settings.VALUE_RANGE_VALIDATIONS['organization']['establishment_year']['message'])

    def test_invalid_tel_value(self):
        base_data = {
            "name": "An Example Company",
            "category": "client",
            "score": 3,
        }

        data = {**base_data, **{
            'tel1': 'A',
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 'B',
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
            'tel3': 'C',
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel3': 3,
            'tel1': 1,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        """Test cannot create or update organization with tel longer than 15"""
        data = {**base_data, **{
            'tel1': 12345,
            'tel2': 12345,
            'tel3': 123456
        }}
        """Test create organization"""
        response = self.client.post(
            path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), TelValueIsTooLongException().default_detail)
        """Test update organization"""
        response = self.client.patch(
            path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), TelValueIsTooLongException().default_detail)

    def test_invalid_fax_value(self):
        base_data = {
            "name": "An Example Company",
            "category": "client",
            "score": 3,
        }

        data = {**base_data, **{
            'fax1': 'A',
            'fax2': 2,
            'fax3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'fax1': 1,
            'fax2': 'B',
            'fax3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'fax1': 1,
            'fax2': 2,
            'fax3': 'C',
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'fax1': 1,
            'fax2': 2,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'fax2': 2,
            'fax3': 3,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'fax3': 3,
            'fax1': 1,
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        """Test cannot create or update organization with fax longer than 15"""
        data = {**base_data, **{
            'fax1': 12345,
            'fax2': 12345,
            'fax3': 123456
        }}
        """Test create organization"""
        response = self.client.post(
            path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['fax']), FaxValueIsTooLongException().default_detail)
        """Test update organization"""
        response = self.client.patch(
            path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['fax']), FaxValueIsTooLongException().default_detail)

    def test_invalid_branch_tel_value(self):
        base_data = {
            "name": "An Example Company",
            "category": "client",
            "score": 3,
        }

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel1': 'A',
                'tel2': 2,
                'tel3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel1': 1,
                'tel2': 'B',
                'tel3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel1': 1,
                'tel2': 2,
                'tel3': 'C',
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel1': 1,
                'tel2': 2,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel2': 2,
                'tel3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel3': 3,
                'tel1': 1,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        """Test cannot create or update organization with tel longer than 15"""
        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'tel1': 12345,
                'tel2': 12345,
                'tel3': 123456
            }]
        }}
        """Test create organization"""
        response = self.client.post(
            path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['branches'][0]['tel']), TelValueIsTooLongException().default_detail)
        """Test update organization"""
        response = self.client.patch(
            path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['branches'][0]['tel']), TelValueIsTooLongException().default_detail)

    def test_invalid_branch_fax_value(self):
        base_data = {
            "name": "An Example Company",
            "category": "client",
            "score": 3,
        }

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax1': 'A',
                'fax2': 2,
                'fax3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax1': 1,
                'fax2': 'B',
                'fax3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax1': 1,
                'fax2': 2,
                'fax3': 'C',
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax1': 1,
                'fax2': 2,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax2': 2,
                'fax3': 3,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax3': 3,
                'fax1': 1,
            }]
        }}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'FAXの欄は全て半角数字で入力してください。')

        """Test cannot create or update organization with fax longer than 15"""
        data = {**base_data, **{
            "branches": [{
                "name": "An Example Branch",
                'fax1': 12345,
                'fax2': 12345,
                'fax3': 123456
            }]
        }}
        """Test create organization"""
        response = self.client.post(
            path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['branches'][0]['fax']), FaxValueIsTooLongException().default_detail)
        """Test update organization"""
        response = self.client.patch(
            path='/app_staffing/organizations/749303ca-7701-463e-83a8-8a5a35df1646', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['branches'][0]['fax']), FaxValueIsTooLongException().default_detail)

    def test_get_category_conditions(self):
        # fixture 側を全て除外しておく
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 1000000
        company.save()

        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        Organization.objects.create(
            name="prospective",
            organization_category=OrganizationCategory.objects.get(id=1),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="approached",
            organization_category=OrganizationCategory.objects.get(id=2),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="exchanged",
            organization_category=OrganizationCategory.objects.get(id=3),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="client",
            organization_category=OrganizationCategory.objects.get(id=4),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )

        testCases = [
            # 1件指定(非not)
            {
                "data": {"category": "prospective"},
                "expected": ["prospective"],
            },
            {
                "data": {"category": "approached"},
                "expected": ["approached"],
            },
            {
                "data": {"category": "exchanged"},
                "expected": ["exchanged"],
            },
            {
                "data": {"category": "client"},
                "expected": ["client"],
            },
            # 1件指定(not)
            {
                "data": {"category": "not:prospective"},
                "expected": ["approached", "client", "exchanged"],
            },
            {
                "data": {"category": "not:approached"},
                "expected": ["client", "exchanged", "prospective"],
            },
            {
                "data": {"category": "not:exchanged"},
                "expected": ["approached", "client", "prospective"],
            },
            {
                "data": {"category": "not:client"},
                "expected": ["approached", "exchanged", "prospective"],
            },
            # 2件指定(非not)
            {
                "data": {"category": "prospective,approached"},
                "expected": ["approached", "prospective"],
            },
            {
                "data": {"category": "approached,exchanged"},
                "expected": ["approached", "exchanged"],
            },
            {
                "data": {"category": "exchanged,client"},
                "expected": ["client", "exchanged"],
            },
            {
                "data": {"category": "client,prospective"},
                "expected": ["client", "prospective"],
            },
            # 2件指定(not)
            {
                "data": {"category": "not:prospective,not:approached"},
                "expected": ["client", "exchanged"],
            },
            {
                "data": {"category": "not:approached,not:exchanged"},
                "expected": ["client", "prospective"],
            },
            {
                "data": {"category": "not:exchanged,not:client"},
                "expected": ["approached", "prospective"],
            },
            {
                "data": {"category": "not:client,not:prospective"},
                "expected": ["approached", "exchanged"],
            },
            # 3件指定(非not)
            {
                "data": {"category": "prospective,approached,exchanged"},
                "expected": ["approached", "exchanged", "prospective"],
            },
            {
                "data": {"category": "approached,exchanged,client"},
                "expected": ["approached", "client", "exchanged"],
            },
            {
                "data": {"category": "exchanged,client,prospective"},
                "expected": ["client", "exchanged", "prospective"],
            },
            {
                "data": {"category": "client,prospective,approached"},
                "expected": ["approached", "client", "prospective"],
            },
            # 3件指定(not)
            {
                "data": {"category": "not:prospective,not:approached,not:exchanged"},
                "expected": ["client"],
            },
            {
                "data": {"category": "not:approached,not:exchanged,not:client"},
                "expected": ["prospective"],
            },
            {
                "data": {"category": "not:exchanged,not:client,not:prospective"},
                "expected": ["approached"],
            },
            {
                "data": {"category": "not:client,not:prospective,not:approached"},
                "expected": ["exchanged"],
            },
            # 4件指定(非not)
            {
                "data": {"category": "prospective,approached,exchanged,client"},
                "expected": ["approached", "client", "exchanged", "prospective"],
            },
            # 4件指定(not)
            {
                "data": {"category": "not:client,not:prospective,not:approached,not:exchanged"},
                "expected": [],
            },
            # 非notとnotを混合指定
            {
                "data": {"category": "prospective,not:prospective"},
                "expected": ["approached", "client", "exchanged", "prospective"],
            },
            {
                "data": {"category": "prospective,not:approached"},
                "expected": ["client", "exchanged", "prospective"],
            },
            {
                "data": {"category": "prospective,not:prospective,not:approached"},
                "expected": ["client", "exchanged", "prospective"],
            },
            {
                "data": {"category": "prospective,approached,not:approached"},
                "expected": ["approached", "client", "exchanged", "prospective"],
            },
        ]

        for testCase in testCases:
            response = self.client.get(path=self.base_url, data=testCase["data"], format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["name"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

    def test_get_country_conditions(self):
        # fixture 側を全て除外しておく
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 1000000
        company.save()

        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        Organization.objects.create(
            name="JP",
            organization_country=OrganizationCountry.objects.get(id=1),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="KR",
            organization_country=OrganizationCountry.objects.get(id=2),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="CN",
            organization_country=OrganizationCountry.objects.get(id=3),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )
        Organization.objects.create(
            name="OTHER",
            organization_country=OrganizationCountry.objects.get(id=4),
            score=3,
            company=GET_DEFAULT_COMPANY(),
        )

        testCases = [
            # 1件指定(非not)
            {
                "data": {"country": "JP"},
                "expected": ["JP"],
            },
            {
                "data": {"country": "KR"},
                "expected": ["KR"],
            },
            {
                "data": {"country": "CN"},
                "expected": ["CN"],
            },
            {
                "data": {"country": "OTHER"},
                "expected": ["OTHER"],
            },
            # 1件指定(not)
            {
                "data": {"country": "not:JP"},
                "expected": ["KR", "CN", "OTHER"],
            },
            {
                "data": {"country": "not:KR"},
                "expected": ["JP", "CN", "OTHER"],
            },
            {
                "data": {"country": "not:CN"},
                "expected": ["JP", "KR", "OTHER"],
            },
            {
                "data": {"country": "not:OTHER"},
                "expected": ["JP", "KR", "CN"],
            },
            # 2件指定(非not)
            {
                "data": {"country": "JP,KR"},
                "expected": ["JP", "KR"],
            },
            {
                "data": {"country": "KR,CN"},
                "expected": ["KR", "CN"],
            },
            {
                "data": {"country": "CN,OTHER"},
                "expected": ["CN", "OTHER"],
            },
            {
                "data": {"country": "OTHER,JP"},
                "expected": ["OTHER", "JP"],
            },
            # 2件指定(not)
            {
                "data": {"country": "not:JP,not:KR"},
                "expected": ["CN", "OTHER"],
            },
            {
                "data": {"country": "not:KR,not:CN"},
                "expected": ["JP", "OTHER"],
            },
            {
                "data": {"country": "not:CN,not:OTHER"},
                "expected": ["JP", "KR"],
            },
            {
                "data": {"country": "not:OTHER,not:JP"},
                "expected": ["CN", "KR"],
            },
            # 3件指定(非not)
            {
                "data": {"country": "JP,KR,CN"},
                "expected": ["JP", "KR", "CN"],
            },
            {
                "data": {"country": "KR,CN,OTHER"},
                "expected": ["KR", "CN", "OTHER"],
            },
            {
                "data": {"country": "CN,OTHER,JP"},
                "expected": ["JP", "CN", "OTHER"],
            },
            {
                "data": {"country": "OTHER,JP,KR"},
                "expected": ["JP", "KR", "OTHER"],
            },
            # 3件指定(not)
            {
                "data": {"country": "not:JP,not:KR,not:CN"},
                "expected": ["OTHER"],
            },
            {
                "data": {"country": "not:KR,not:CN,not:OTHER"},
                "expected": ["JP"],
            },
            {
                "data": {"country": "not:CN,not:OTHER,not:JP"},
                "expected": ["KR"],
            },
            {
                "data": {"country": "not:OTHER,not:JP,not:KR"},
                "expected": ["CN"],
            },
            # 4件指定(非not)
            {
                "data": {"country": "JP,KR,CN,OTHER"},
                "expected": ["JP", "KR", "CN", "OTHER"],
            },
            # 4件指定(not)
            {
                "data": {"country": "not:JP,not:KR,not:CN,not:OTHER"},
                "expected": [],
            },
            # 非notとnotを混合指定
            {
                "data": {"country": "JP,not:JP"},
                "expected": ["JP", "KR", "CN", "OTHER"],
            },
            {
                "data": {"country": "JP,not:KR"},
                "expected": ["JP", "CN", "OTHER"],
            },
            {
                "data": {"country": "JP,not:JP,not:KR"},
                "expected": ["JP", "CN", "OTHER"],
            },
            {
                "data": {"country": "KR,JP,not:JP"},
                "expected": ["JP", "KR", "CN", "OTHER"],
            },
        ]

        for testCase in testCases:
            response = self.client.get(path=self.base_url, data=testCase["data"], format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["name"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

class OrganizationNameViewTestCase(APITestCase):
    """A Test Case for organization name View"""

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_branches.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_organization_names(self):
        response = self.client.get(path='/app_staffing/organizations/names', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)


class OrganizationCsvViewTestCase(CsvApiTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',  # For foreign key relationships.
    ]

    _url = '/app_staffing/csv/organizations'

    _EXPECTED_HEADER = [
        '法人番号',
        '取引先名',
        '取引先ステータス',
        '取引先評価',
        '国籍',
        '設立年月',
        '決算期',
        '住所(市区町村・町名・番地)',
        '住所(建物)',
        'TEL',
        'FAX',
        'URL',
        '社員数',
        '商流',
        '請負',
        '資本金',
        '保有資格 > Pマーク／ISMS',
        '保有資格 > インボイス登録事業者',
        '保有資格 > 労働者派遣事業',
        '取引先支店名',
        '取引先支店住所(市区町村・町名・番地)',
        '取引先支店住所(建物)',
        '取引先支店TEL',
        '取引先支店FAX',
        '取引に必要な設立年数',
        '取引に必要な資本金',
        '取引に必要な資格 > Pマーク／ISMS',
        '取引に必要な資格 > インボイス登録事業者',
        '取引に必要な資格 > 労働者派遣事業',
        'ブロックリスト',
    ]  # Warning. the order of fields are important.

    _EXPECTED_FIRST_FIELDS = [
        '456',
        'A Blacklisted Company.',
        '見込み客',
        '2',
        'JP',
        '',
        '1',
        '',
        '',
        '03-1234-5678',
        '03-1234-5679',
        '',
        '~10名',
        '',
        '0',
        '150',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '10',
        '100',
        '0',
        '0',
        '0',
        '1',
    ]


class OrganizationCsvDownloadWithSearchConditionViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path='/app_staffing/csv/organizations?name=Example')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        rows = list(response.streaming_content)

        entries = rows[1:]
        self.assertEqual(len(entries), 1)

        for index, entry in enumerate(entries):
            fields = entry.decode('utf-8').replace('\r\n', '').split(',')
            self.assertEqual(fields[0], '123')


@override_settings(CSV_DOWNLOAD_LIMIT=3)
class OrganizationCsvDownloadViewWithIndexTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/csvDownload/organizations.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path='/app_staffing/csv/organizations?index=0')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company0')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company1')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company2')

        response = self.client.get(path='/app_staffing/csv/organizations?index=1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company3')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company4')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company5')

        response = self.client.get(path='/app_staffing/csv/organizations?index=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company6')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company7')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company8')

        response = self.client.get(path='/app_staffing/csv/organizations?index=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[1], 'An Example Company9')


class OrganizationMultiTenantTestCase(MultiTenancyTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    base_url = '/app_staffing/organizations'
    test_patterns = ('list', 'get', 'post', 'patch', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.organization1 = OrganizationFactory(
            name='An Organization of tenant A', company=cls.company_1,
        )
        cls.organization2 = OrganizationFactory(
            name='An Organization of tenant B', company=cls.company_2,
        )
        cls.resource_id_for_tenant_1 = cls.organization1.id
        cls.resource_id_for_tenant_2 = cls.organization2.id

        # Note: Make sure that posting data regards to the same company will not conflict each other,
        # like raising duplication error.
        cls.post_data_for_tenant_1 = {
            "name": "A Posted Company",
            "corporate_number": 1234567890123,  # Max 13 digit in japan.
            "category": "client",
            "employee_number": "very_low",
            "contract": True,
            "country": "JP",
            "address": "東京都港区東麻布X-X-X",
            "building": "building",
            "domain_name": "example.com",
            "settlement_month": 1,
            "capital_man_yen": 1000,
            "capital_man_yen_required_for_transactions": 1000,
            "establishment_year": 10,
            "score": 3,
            "is_blacklisted": False,
        }

        cls.post_data_for_tenant_2 = {
            "name": "A Posted Company",
            "corporate_number": 1234567890123,  # Max 13 digit in japan.
            "category": "client",
            "employee_number": "very_low",
            "contract": True,
            "country": "JP",
            "address": "東京都港区東麻布X-X-X",
            "building": "building",
            "domain_name": "example.com",
            "settlement_month": 1,
            "capital_man_yen": 1000,
            "capital_man_yen_required_for_transactions": 1000,
            "establishment_year": 100,
            "score": 3,
            "is_blacklisted": False,
        }

        cls.patch_data_for_tenant_1 = {
            "name": "A Patch Company",
            "category": "client",
            "score": 3,
            "address": "東京都千代田区X-X-X",
        }

        cls.patch_data_for_tenant_2 = {
            "name": "A Patch Company",
            "category": "client",
            "score": 3,
            "is_blacklisted": True,
        }


class OrganizationCsvViewMultiTenantTestCase(CsvDownloadViewMultiTenancyTestMixin, APITestCase):
    _base_url = '/app_staffing/csv/organizations'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.organization1 = OrganizationFactory(
            name='An Organization of tenant A', company=cls.company_1,
        )
        cls.organization2 = OrganizationFactory(
            name='An Organization of tenant B', company=cls.company_2,
        )


class OrganizationCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_comments.json',
    ]

    related_resource_id = '749303ca-7701-463e-83a8-8a5a35df1646'
    comment_id = '4ae633ba-5551-40a7-b352-6e4ae7d1ecc7'
    other_users_comment_id = '20c0a96b-ef06-44e0-8e67-1ef670169f7f'
    updating_comment_id = "4ae633ba-5551-40a7-b352-6e4ae7d1ecc8"

    base_url = f'/app_staffing/organizations/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}

    def test_over_max_length(self):
        data = {
            "content": """
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                ccc
            """,
            "is_important": False
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['content']), settings.LENGTH_VALIDATIONS['comment']['content']['message'])

        response = self.client.patch(path=f'{self.base_url}/4ae633ba-5551-40a7-b352-6e4ae7d1ecc7', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['content']), settings.LENGTH_VALIDATIONS['comment']['content']['message'])

    def test_get_comments(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_over_comment_limit(self):
        data = {
            "content": "test comment",
            "is_important": True
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CommentImportantLimitException().default_detail)

    def test_updating_important_comment_content(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-09T16:42:27.484000+09:00")
        self.assertTrue(updated_comment["is_important"])

    def test_updating_important_comment_content_and_is_important(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
            "is_important": False,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-09T16:42:27.484000+09:00")
        self.assertFalse(updated_comment["is_important"])

    def test_delete_comment(self):
        url = self.base_url + "/" + self.updating_comment_id
        self.assertEqual(OrganizationComment.objects.filter(id=self.updating_comment_id).count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(OrganizationComment.objects.filter(id=self.updating_comment_id).count(), 0)

    def test_comment_of_another_user_can_be_updated(self):
        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670169f7f"
        post_data = {
            "content": "updated Another User's comment",
            "is_important": True,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-10T16:42:27.484000+09:00")
        self.assertTrue(updated_comment["is_important"])

    def test_comment_of_another_user_can_be_deleted(self):
        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670169f7f"
        post_data = {
            "content": "updated Another User's comment",
        }
        self.assertEqual(OrganizationComment.objects.filter(id="20c0a96b-ef06-44e0-8e67-1ef670169f7f").count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(OrganizationComment.objects.filter(id="20c0a96b-ef06-44e0-8e67-1ef670169f7f").count(), 0)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {
            "content": "test comment",
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
            "is_important": True,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670169f7f"
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670169f7f"
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class OrganizationCommentViewMultiTenantTestCase(ReferenceViewMultiTenancyMixin, APITestCase):
    base_url = '/app_staffing/organizations'
    suffix = 'comments'
    test_patterns = ('list', 'post')
    is_paginated = False
    # Note: we won't test PATCH/DELETE to /app_staffing/organizations/{organization_id}/comments/{comment_id}
    # Because those two requests are controlled by user id and already tested as OrganizationCommentViewTestCase.

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.organization1 = OrganizationFactory(
            name='An Organization of tenant A', company=cls.company_1,
        )
        cls.organization2 = OrganizationFactory(
            name='An Organization of tenant B', company=cls.company_2,
        )
        # Create Comments.
        OrganizationCommentFactory(organization=cls.organization1, company=cls.company_1)
        OrganizationCommentFactory(organization=cls.organization2, company=cls.company_2)

        cls.resource_id_for_tenant_1 = cls.organization1.id
        cls.resource_id_for_tenant_2 = cls.organization2.id

    def test_can_not_list_other_company_s_data(self):
        # Check if a user can only access data that belong to the same tenant.
        response1 = self.client_1.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_2), format='json')
        response2 = self.client_2.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_1), format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)


class ContactViewTestCase(APICRUDTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_scores.json',
        'app_staffing/tests/fixtures/contact_preferences.json',
        'app_staffing/tests/fixtures/tags.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
    ]
    base_url = '/app_staffing/contacts'
    resource_id = '78630567-72ce-49a3-84e6-8947b5b057f3'
    resource_validator_class = IsContact

    # Used in TestQueryParamErrorMixin
    filter_params = {
        'last_name': 'Trump',
        'first_name': 'Trump',
        'email': 'trump@example.com',
        'organization__name': 'Trump Corp',
        'staff': '78630567-72ce-49a3-84e6-8947b5b057f3',
        'last_visit_gte': '2019-04-01',
        'last_visit_lte': '2020-04-01',
        'old_id': 1,
        'score_gte': 1,
        'cc_addresses__email': 'rump@example.com',
    }

    post_test_cases = [
        ('full', {
            "last_name": "An Example Contact",
            "first_name": "An Example Contact",
            "email": "contact@example.com",
            "tel1": "012",
            "tel2": "3456",
            "tel3": "7890",
            "position": "test position",
            "department": "test department",
            "staff": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "last_visit": "2119-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "tags": ['1118d509-d56b-4e19-b2ca-201bbc5f4218', '2218d509-d56b-4e19-b2ca-201bbc5f4218'],
            "category": "heart",
            "jobtypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "jobskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "job_koyou_proper": True
            },
        }),
        ('minimum', {
            "last_name": "An Example Contact 2",
            "email": "contact2@example.com",
            "organization": "5468c29f-7b35-4540-b9d0-c1807d945734",
            "last_visit": "2019-04-08",
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }),
    ]

    # Used after post
    get_after_post_test_cases = [
        ('full', {
            "last_name": "An Example Contact",
            "first_name": "An Example Contact",
            "organization__name": "An Example Company",
            "email": "contact@example.com",
            "tel1": "012",
            "tel2": "3456",
            "tel3": "7890",
            "position": "test position",
            "department": "test department",
            "staff": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "last_visit_gte": "2119-04-08",
            "last_visit_lte": "2120-04-08",
            "cc_addresses__email": "contact_cc1@example.com",
            "tags_and": "1118d509-d56b-4e19-b2ca-201bbc5f4218,2218d509-d56b-4e19-b2ca-201bbc5f4218",
            "category_eq": "heart",
            "preference": "job,personnel",
            "wants_location": "wants_location_hokkaido_japan",
            "ignore_blocklist_filter": "use_blocklist_filter",
            "ignore_filter": "use_filter",
            "page": 1,
            "page_size": 1,
            "created_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "modified_user": "1957ab0f-b47c-455a-a7b7-4453cfffd05e",
        }),
        ('last_name_not_exist', {
            "last_name": "Not Exist Last Name",
            "page_size": 0,
        }),
        ('first_name_not_exist', {
            "first_name": "Not Exist First Name",
            "page_size": 0,
        }),
        ('organization_name_not_exist', {
            "organization__name": "Not Exist Oganization Name",
            "page_size": 0,
        }),
        ('email_not_exist', {
            "email": "NotExist@email.com",
            "page_size": 0,
        }),
        ('tel1_not_exist', {
            "tel1": "444",
            "page_size": 0,
        }),
        ('tel2_not_exist', {
            "tel2": "555",
            "page_size": 0,
        }),
        ('tel3_not_exist', {
            "tel3": "666",
            "page_size": 0,
        }),
        ('position_not_exist', {
            "position": "Not Exist Position",
            "page_size": 0,
        }),
        ('department_not_exist', {
            "department": "Not Exist Department",
            "page_size": 0,
        }),
        ('staff_not_exist', {
            "staff": "1957ab0f-b47c-455a-a7b7-4453cfffd05f",
            "page_size": 0,
        }),
        ('last_visit_gte_not_exist', {
            "last_visit_gte": "3000-01-01",
            "page_size": 0,
        }),
        ('last_visit_lte_not_exist', {
            "last_visit_lte": "1000-01-01",
            "page_size": 0,
        }),
        ('cc_addresses__email_not_exist', {
            "cc_addresses__email": "NotExist@email.com",
            "page_size": 0,
        }),
        ('tags_and_not_exist', {
            "tags_and": "1118d509-d56b-4e19-b2ca-201bbc5f4218,3218d509-d56b-4e19-b2ca-201bbc5f4218",
            "page_size": 0,
        }),
        ('tags_or_not_exist', {
            "tags_or": "3118d509-d56b-4e19-b2ca-201bbc5f4218,4218d509-d56b-4e19-b2ca-201bbc5f4218",
            "page_size": 0,
        }),
        ('category_eq_heart', {
            "category_eq": "heart",
            "page_size": 1,
        }),
        ('category_eq_frown', {
            "category_eq": "frown",
            "page_size": 0,
        }),
        ('category_not_eq_heart', {
            "category_not_eq": "heart",
            "page_size": 6,
        }),
        ('category_not_eq_frown', {
            "category_not_eq": "frown",
            "page_size": 7,
        }),
        ('preference_job', {
            "preference": "job",
            "page_size": 1,
        }),
        ('preference_personnel', {
            "preference": "personnel",
            "page_size": 1,
        }),
        ('created_user_not_exist', {
            "created_user": "2957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "page_size": 0,
        }),
        ('modified_user_not_exist', {
            "modified_user": "2957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "page_size": 0,
        }),
        ('modified_user_not_exist', {
            "modified_user": "2957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "page_size": 0,
        }),
        ('comment_user_not_exist', {
            "comment_user": "2957ab0f-b47c-455a-a7b7-4453cfffd05e",
            "page_size": 0,
        }),
    ]

    patch_test_cases = [
        ('full', {
            "last_name": "An Updated",
            "first_name": "Example Contact",
            "email": "contact2@example.com",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc3@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "jobskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "job_koyou_proper": True
            },
        }),
        ('minimum', {
            "last_name": "An Updated",
            "email": "contact2@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }),
        ('m2mField', {
            "last_name": "An Updated",
            "email": "contact2@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc3@example.com"],
        }),
        ('related_fields', {
            "last_name": "An Updated",
            "email": "contact2@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "score": 5,
            "tags": ["6618d509-d56b-4e19-b2ca-201bbc5f4218"],
        }),
    ]

    skip_tests = ('put', 'list')

    def test_bulk_delete(self):
        data = { 'source': ['78630567-72ce-49a3-84e6-8947b5b057f3']}
        response = self.client.delete(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_bulk_delete_limit_error(self):
        data = { 'source': [
            'sample_uuid_xxxx_yyyy_1',
            'sample_uuid_xxxx_yyyy_2',
            'sample_uuid_xxxx_yyyy_3',
            'sample_uuid_xxxx_yyyy_4',
            'sample_uuid_xxxx_yyyy_5',
            'sample_uuid_xxxx_yyyy_6',
            'sample_uuid_xxxx_yyyy_7',
            'sample_uuid_xxxx_yyyy_8',
            'sample_uuid_xxxx_yyyy_9',
            'sample_uuid_xxxx_yyyy_10',
            'sample_uuid_xxxx_yyyy_11',
        ]}
        response = self.client.delete(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

    def test_over_max_length(self):
        data = {
            "last_name": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "email": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com",
            "cc_mails": [
                "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com",
            ],
            "tel1": "11111",
            "tel2": "11111",
            "tel3": "111111",
            "position": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
            "department": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc",
        }
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['contact']['name']['message'])
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['contact']['email']['message'])
        self.assertEqual(str(response.data['cc_mails']), settings.LENGTH_VALIDATIONS['contact']['cc_mails']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['contact']['tel']['message'])
        self.assertEqual(str(response.data['position']), settings.LENGTH_VALIDATIONS['contact']['position']['message'])
        self.assertEqual(str(response.data['department']), settings.LENGTH_VALIDATIONS['contact']['department']['message'])

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['contact']['name']['message'])
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['contact']['email']['message'])
        self.assertEqual(str(response.data['cc_mails']), settings.LENGTH_VALIDATIONS['contact']['cc_mails']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['contact']['tel']['message'])
        self.assertEqual(str(response.data['position']), settings.LENGTH_VALIDATIONS['contact']['position']['message'])
        self.assertEqual(str(response.data['department']), settings.LENGTH_VALIDATIONS['contact']['department']['message'])

    def test_ignore_filter(self):
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        org1 = Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org1',
            email='org1@example.com',
            organization=org1,
            company=company,
        )
        org2 = Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org2',
            email='org2@example.com',
            organization=org2,
            company=company,
        )
        org3 = Organization.objects.create(
            name="has_invoice_system_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=False,
            has_haken=True,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org3',
            email='org3@example.com',
            organization=org3,
            company=company,
        )
        org4 = Organization.objects.create(
            name="has_invoice_system_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=False,
            has_haken=True,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org4',
            email='org4@example.com',
            organization=org4,
            company=company,
        )
        org5 = Organization.objects.create(
            name="has_haken_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=False,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org5',
            email='org5@example.com',
            organization=org5,
            company=company,
        )
        org6 = Organization.objects.create(
            name="has_haken_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=False,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org6',
            email='org6@example.com',
            organization=org6,
            company=company,
        )
        org7 = Organization.objects.create(
            name="capital_man_yen_under_has_distribution_true",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org7',
            email='org7@example.com',
            organization=org7,
            company=company,
        )
        org8 = Organization.objects.create(
            name="capital_man_yen_under_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org8',
            email='org8@example.com',
            organization=org8,
            company=company,
        )
        org9 = Organization.objects.create(
            name="establishment_date_has_distribution_true",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            establishment_date=datetime.datetime.now().strftime("%Y-%m-%d"),
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org9',
            email='org9@example.com',
            organization=org9,
            company=company,
        )
        org10 = Organization.objects.create(
            name="establishment_date_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            establishment_date=datetime.datetime.now().strftime("%Y-%m-%d"),
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org10',
            email='org10@example.com',
            organization=org10,
            company=company,
        )
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 10) # 商流を「抜ける」の5件 + fixturesの5件(関連パラメータがNullのため条件外)

        data = {
            "ignore_filter": "ignore_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 15) # fixtureの5件と上段で定義した10件

    def test_exceptional_organizations(self):
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        organizations = Organization.objects.all()
        for organization in organizations:
            organization.has_p_mark_or_isms = False
            organization.save()

        organization = Organization.objects.create(
            name="exceptional_organization",
            category='client',
            score=3,
            company=company,
            capital_man_yen=10000,
            has_p_mark_or_isms=False,
            has_invoice_system=False,
            has_haken=False,
        )
        Contact.objects.create(
            last_name='exceptional_organization',
            email='exceptional_organization@example.com',
            organization=organization,
            company=company,
        )
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        ExceptionalOrganization.objects.create(
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_ignore_blocklist_filter(self):
        company = GET_DEFAULT_COMPANY()
        company.capital_man_yen_required_for_transactions = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        organizations = Organization.objects.all()
        for organization in organizations:
            organization.has_p_mark_or_isms = False
            organization.save()

        org_false = Organization.objects.create(
            name="is_blacklisted_false",
            category='client',
            score=3,
            company=company,
            is_blacklisted=False,
        )
        Contact.objects.create(
            last_name='is_blacklisted_false',
            email='is_blacklisted_false@example.com',
            organization=org_false,
            company=company,
        )
        org_true = Organization.objects.create(
            name="is_blacklisted_true",
            category='client',
            score=3,
            company=company,
            is_blacklisted=True,
        )
        Contact.objects.create(
            last_name='is_blacklisted_true',
            email='is_blacklisted_true@example.com',
            organization=org_true,
            company=company,
        )
        data = {
            "ignore_blocklist_filter": "use_blocklist_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

        data = {
            "ignore_blocklist_filter": "ignore_blocklist_filter",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_not_null_value(self):
        response = self.client.post(path='/app_staffing/contacts', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['last_name']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['organization']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['email']), settings.NOT_NULL_VALIDATIONS['contact']['message'])

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['last_name']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['organization']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['email']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        data = {
            "last_name": None,
            "organization": None,
            "email": None,
        }
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['last_name']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['organization']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['email']), settings.NOT_NULL_VALIDATIONS['contact']['message'])

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['last_name']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['organization']), settings.NOT_NULL_VALIDATIONS['contact']['message'])
        self.assertEqual(str(response.data['email']), settings.NOT_NULL_VALIDATIONS['contact']['message'])

    def test_over_limit_cc_mails(self):
        data = {
            "last_name": "An Over limit CC_mails",
            "email": "contact1@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["a1@example.com,a2@example.com,a3@example.com,a4@example.com,a5@example.com"],
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data["email"] = "contact2@example.com"
        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data["cc_mails"][0] += ",a6@example.com"
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'CCに指定できるアドレスは5件までです。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'CCに指定できるアドレスは5件までです。')

    def test_invalid_tel_value(self):
        base_data = {
            "last_name": "An Over limit CC_mails",
            "email": "contact1@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }

        data = {**base_data, **{
            'tel1': 'A',
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 'B',
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
            'tel3': 'C',
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel3': 3,
            'tel1': 1,
        }}
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        """Test cannot create or update contact with tel longer than 15"""
        data = {**base_data, **{
            'tel1': 12345,
            'tel2': 12345,
            'tel3': 123456
        }}
        """Test create organization"""
        response = self.client.post(
            path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), TelValueIsTooLongException().default_detail)
        """Test update organization"""
        response = self.client.patch(
            path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['tel']), TelValueIsTooLongException().default_detail)

    def test_over_limit_tags(self):
        data = {
            "last_name": "An Over limit CC_mails",
            "email": "contact1@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "tags": [
                "1018d509-d56b-4e19-b2ca-201bbc5f4218",
                "2018d509-d56b-4e19-b2ca-201bbc5f4218",
                "3018d509-d56b-4e19-b2ca-201bbc5f4218",
                "4018d509-d56b-4e19-b2ca-201bbc5f4218",
                "5018d509-d56b-4e19-b2ca-201bbc5f4218",
                "6018d509-d56b-4e19-b2ca-201bbc5f4218",
                "7018d509-d56b-4e19-b2ca-201bbc5f4218",
                "8018d509-d56b-4e19-b2ca-201bbc5f4218",
                "9018d509-d56b-4e19-b2ca-201bbc5f4218",
                "1118d509-d56b-4e19-b2ca-201bbc5f4218",
            ],
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data["email"] = "contact2@example.com"
        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data["tags"].append("2218d509-d56b-4e19-b2ca-201bbc5f4218")
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '選択できるタグは10件までです。')

        response = self.client.patch(path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '選択できるタグは10件までです。')

    def test_illegal_format_email(self):
        data = {
            "last_name": "Illegal Format Email",
            "email": "contact1.example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }
        response = self.client.post(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['email']), "[ErrorDetail(string='有効なメールアドレスを入力してください。', code='invalid')]")

    def test_removed_tag_not_match(self):
        tag = Tag.objects.create(company_id=DEFAULT_COMPANY_ID)
        contact_a = Contact.objects.get(id='78630567-72ce-49a3-84e6-8947b5b057f3')
        contact_b = Contact.objects.get(id='aa8f0a2b-acc5-4908-b4d0-918078a9373a')
        assign_a = ContactTagAssignment.objects.create(company_id=DEFAULT_COMPANY_ID, tag=tag, contact=contact_a)
        assign_b = ContactTagAssignment.objects.create(company_id=DEFAULT_COMPANY_ID, tag=tag, contact=contact_b)

        data_and = {
            "tags_and": str(tag.id)
        }
        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        data_or = {
            "tags_or": str(tag.id)
        }
        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        assign_a.delete()

        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

        assign_b.delete()

        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

    def test_update_already_registered_email(self):
        data = {
            "last_name": "An Over limit CC_mails",
            "email": "sample2@example.com",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "tags": [
                "1018d509-d56b-4e19-b2ca-201bbc5f4218",
                "2018d509-d56b-4e19-b2ca-201bbc5f4218",
            ],
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }
        response = self.client.patch(
            path='/app_staffing/contacts/78630567-72ce-49a3-84e6-8947b5b057f3', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), AlreadyRegisteredEmailException().default_detail)

    def test_list_all_contact(self):
        """Get all data contact in model"""
        obj_summary = Contact.objects.all()

        list_keys = [
            'id',
            'display_name',
            'email',
            'cc_mails',
            'position',
            'department',
            'organization__name',
            'staff__name',
            'last_visit',
            'created_time',
            'modified_time',
            'comments',
            'tel1',
            'tel2',
            'tel3',
            'tags',
            'tag_objects',
            'contactpreference',
            'category',
            'is_ignored',
            'contactjobtypepreferences',
            'contactpersonneltypepreferences',
            'organization',
        ]

        response = self.client.get(
            path='/app_staffing/contacts', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        self.assertEqual(len(obj_summary), len(results))
        for data in results:
            self.assertEqual(list_keys, list(data.keys()))


class ContactEmailViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_contact_emails(self):
        response = self.client.get(path='/app_staffing/contacts/emails', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 5)


class ContactCsvViewTestCase(CsvApiTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',  # For foreign key relationships.
        'app_staffing/tests/fixtures/contacts.json',
    ]

    _url = '/app_staffing/csv/contacts'

    _EXPECTED_HEADER = [
        '取引先担当者名(姓)',
        '取引先担当者名(名)',
        '所属取引先',
        'メールアドレス > TO',
        'メールアドレス > CC',
        'TEL',
        '役職',
        '部署',
        '自社担当者(メールアドレス)',
        '最終商談日',
        'タグ',
        '相性',
    ]  # Warning. the order of fields are important.

    _EXPECTED_FIRST_FIELDS = [
        'A sample user 2',
        'sample name 2',
        'An Example Company',
        'sample2@example.com',
        '',
        '03-9876-5432',
        'test position',
        'test department',
        '',
        '2100-04-08',
        '',
        '',
    ]


class ContactFullCsvViewTestCase(CsvApiTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',  # For foreign key relationships.
        'app_staffing/tests/fixtures/contacts.json',
    ]

    _url = '/app_staffing/csv/contacts_full'

    _EXPECTED_HEADER = [
        '取引先担当者名(姓)',
        '取引先担当者名(名)',
        '所属取引先',
        'メールアドレス > TO',
        'メールアドレス > CC',
        'TEL',
        '役職',
        '部署',
        '自社担当者(メールアドレス)',
        '最終商談日',
        'タグ',
        '相性',
        '希望エリア > 北海道',
        '希望エリア > 東北',
        '希望エリア > 関東',
        '希望エリア > 中部',
        '希望エリア > 東海',
        '希望エリア > 関西',
        '希望エリア > 四国',
        '希望エリア > 中国',
        '希望エリア > 九州',
        '希望エリア > その他',
        '案件配信 > 開発 > 職種詳細 > デザイナー',
        '案件配信 > 開発 > 職種詳細 > フロントエンド',
        '案件配信 > 開発 > 職種詳細 > バックエンド',
        '案件配信 > 開発 > 職種詳細 > PM・ディレクター',
        '案件配信 > 開発 > 職種詳細 > その他',
        '案件配信 > 開発 > スキル詳細 > 要件定義',
        '案件配信 > 開発 > スキル詳細 > 基本設計',
        '案件配信 > 開発 > スキル詳細 > 詳細設計',
        '案件配信 > 開発 > スキル詳細 > 製造',
        '案件配信 > 開発 > スキル詳細 > テスト・検証',
        '案件配信 > 開発 > スキル詳細 > 保守・運用',
        '案件配信 > 開発 > スキル詳細 > 未経験',
        '案件配信 > インフラ > 職種詳細 > サーバー',
        '案件配信 > インフラ > 職種詳細 > ネットワーク',
        '案件配信 > インフラ > 職種詳細 > セキュリティ',
        '案件配信 > インフラ > 職種詳細 > データベース',
        '案件配信 > インフラ > 職種詳細 > 情報システム',
        '案件配信 > インフラ > 職種詳細 > その他',
        '案件配信 > インフラ > スキル詳細 > 要件定義',
        '案件配信 > インフラ > スキル詳細 > 基本設計',
        '案件配信 > インフラ > スキル詳細 > 詳細設計',
        '案件配信 > インフラ > スキル詳細 > 構築',
        '案件配信 > インフラ > スキル詳細 > テスト・検証',
        '案件配信 > インフラ > スキル詳細 > 監視',
        '案件配信 > インフラ > スキル詳細 > 保守・運用',
        '案件配信 > インフラ > スキル詳細 > 未経験',
        '案件配信 > その他 > 職種詳細 > 営業・事務',
        '案件配信 > その他 > 職種詳細 > 基地局',
        '案件配信 > その他 > 職種詳細 > コールセンター・サポートデスク',
        '案件配信 > その他 > 職種詳細 > その他',
        '案件配信 > 商流制限',
        '要員配信 > 開発 > 職種詳細 > デザイナー',
        '要員配信 > 開発 > 職種詳細 > フロントエンド',
        '要員配信 > 開発 > 職種詳細 > バックエンド',
        '要員配信 > 開発 > 職種詳細 > PM・ディレクター',
        '要員配信 > 開発 > 職種詳細 > その他',
        '要員配信 > 開発 > スキル詳細 > 要件定義',
        '要員配信 > 開発 > スキル詳細 > 基本設計',
        '要員配信 > 開発 > スキル詳細 > 詳細設計',
        '要員配信 > 開発 > スキル詳細 > 製造',
        '要員配信 > 開発 > スキル詳細 > テスト・検証',
        '要員配信 > 開発 > スキル詳細 > 保守・運用',
        '要員配信 > 開発 > スキル詳細 > 未経験',
        '要員配信 > インフラ > 職種詳細 > サーバー',
        '要員配信 > インフラ > 職種詳細 > ネットワーク',
        '要員配信 > インフラ > 職種詳細 > セキュリティ',
        '要員配信 > インフラ > 職種詳細 > データベース',
        '要員配信 > インフラ > 職種詳細 > 情報システム',
        '要員配信 > インフラ > 職種詳細 > その他',
        '要員配信 > インフラ > スキル詳細 > 要件定義',
        '要員配信 > インフラ > スキル詳細 > 基本設計',
        '要員配信 > インフラ > スキル詳細 > 詳細設計',
        '要員配信 > インフラ > スキル詳細 > 構築',
        '要員配信 > インフラ > スキル詳細 > テスト・検証',
        '要員配信 > インフラ > スキル詳細 > 監視',
        '要員配信 > インフラ > スキル詳細 > 保守・運用',
        '要員配信 > インフラ > スキル詳細 > 未経験',
        '要員配信 > その他 > 職種詳細 > 営業・事務',
        '要員配信 > その他 > 職種詳細 > 基地局',
        '要員配信 > その他 > 職種詳細 > コールセンター・サポートデスク',
        '要員配信 > その他 > 職種詳細 > その他',
        '要員配信 > 希望雇用形態 > プロパー',
        '要員配信 > 希望雇用形態 > フリーランス',
        '要員配信 > 商流制限',
    ]  # Warning. the order of fields are important.

    _EXPECTED_FIRST_FIELDS = [
        'A sample user 2',
        'sample name 2',
        'An Example Company',
        'sample2@example.com',
        '',
        '03-9876-5432',
        'test position',
        'test department',
        '',
        '2100-04-08',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '',
        '',
        '',
        ]


class ContactCsvDownloadWithSearchConditionViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path='/app_staffing/csv/contacts?email=free')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        rows = list(response.streaming_content)

        entries = rows[1:]
        self.assertEqual(len(entries), 1)

        for index, entry in enumerate(entries):
            fields = entry.decode('utf-8').replace('\r\n', '').split(',')
            self.assertEqual(fields[0], "A Contact that has no relationships to our company's staff")


@override_settings(CSV_DOWNLOAD_LIMIT=3)
class ContactCsvDownloadViewWithIndexTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/csvDownload/organizations.json',
        'app_staffing/tests/fixtures/csvDownload/contacts.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path='/app_staffing/csv/contacts?index=0')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 0')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 1')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 2')

        response = self.client.get(path='/app_staffing/csv/contacts?index=1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 3')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 4')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 5')

        response = self.client.get(path='/app_staffing/csv/contacts?index=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 3)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 6')
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 7')
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 8')

        response = self.client.get(path='/app_staffing/csv/contacts?index=3')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        rows = list(response.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[0], 'A sample user 9')


class ContactFullCsvDownloadWithSearchConditionViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path='/app_staffing/csv/contacts_full?email=free')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        rows = list(response.streaming_content)

        entries = rows[1:]
        self.assertEqual(len(entries), 1)

        for index, entry in enumerate(entries):
            fields = entry.decode('utf-8').replace('\r\n', '').split(',')
            self.assertEqual(fields[0], "A Contact that has no relationships to our company's staff")


class ContactViewMultiTenantTestCase(MultiTenancyTestMixin, APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
    ]

    base_url = '/app_staffing/contacts'
    test_patterns = ('list', 'get', 'post', 'patch', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contact1 = ContactFactory(
            company=cls.company_1,
            email='contactA@example.com',
            created_user=cls.user1
        )
        cls.contact2 = ContactFactory(
            company=cls.company_2,
            email='contactB@example.com',
            created_user=cls.user2
        )
        cls.resource_id_for_tenant_1 = cls.contact1.id
        cls.resource_id_for_tenant_2 = cls.contact2.id

        cls.tag1 = TagFactory(
            company=cls.company_1,
            value='PHP'
        )

        cls.tag2 = TagFactory(
            company=cls.company_1,
            value='ES2015'
        )

        # Note: Make sure that posting data regards to the same company will not conflict each other,
        # like raising duplication error.
        cls.post_data_for_tenant_1 = {
            "last_name": "An Example",
            "first_name": "Contact",
            "email": "contact@example.com",
            "position": "test position",
            "department": "test department",
            "staff": cls.user1.id,
            "last_visit": '2019-04-08',
            "organization": cls.contact1.organization.id,
            "cc_mails": ["contact_cc1@example.com,contact_cc2@example.com"],
            "score": 3,
            "tags": [cls.tag1.id, cls.tag2.id],
            "jobtypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "jobskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "job_koyou_proper": True
            },
        }

        cls.post_data_for_tenant_2 = {
            "last_name": "An Example",
            "first_name": "Contact",
            "email": "contact@example.com",
            "position": "test position",
            "department": "test department",
            "staff": cls.user2.id,
            "last_visit": '2019-04-08',
            "organization": cls.contact2.organization.id,
            "cc_mails": ["contact_cc1@example.com,contact_cc2@example.com"],
            "score": 2,
            "tags": [cls.tag1.id, cls.tag2.id],
            "jobtypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "jobskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_kihon": True,
                "infra_kihon": True
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "job_koyou_proper": True
            },
        }

        cls.patch_data_for_tenant_1 = {
            "last_name": "An Example",
            "organization": cls.contact1.organization.id,
            "email": "contact@example.com",
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }

        cls.patch_data_for_tenant_2 = {
            "last_name": "An Example",
            "organization": cls.contact2.organization.id,
            "email": "contact@example.com",
            "cc_mails": ["contact_cc3@example.com"],
            "jobtypepreference": {},
            "jobskillpreference": {},
            "personneltypepreference": {},
            "personnelskillpreference": {},
            "preference": {},
        }


class ContactCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',  # For foreign key relationships.
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_comments.json',
    ]

    related_resource_id = '78630567-72ce-49a3-84e6-8947b5b057f3'
    comment_id = '39037c2e-4f95-47c2-b222-1beddb5ef0c7'
    other_users_comment_id = 'dd466574-10ce-4fef-bb97-7d1d8d806706'
    updating_comment_id = "dd466574-10ce-4fef-bb97-7d1d8d806707"

    base_url = f'/app_staffing/contacts/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}

    def test_over_max_length(self):
        data = {
            "content": """
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                ccc
            """,
            "is_important": False
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['content']), settings.LENGTH_VALIDATIONS['comment']['content']['message'])

        response = self.client.patch(path=f'{self.base_url}/{self.updating_comment_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['content']), settings.LENGTH_VALIDATIONS['comment']['content']['message'])

    def test_get_comments(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_over_comment_limit(self):
        data = {
            "content": "test comment",
            "is_important": True
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CommentImportantLimitException().default_detail)

    def test_updating_important_comment_content(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_updating_important_comment_content_and_is_important(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
            "is_important": False,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")
        self.assertFalse(updated_comment["is_important"])

    def test_delete_comment(self):
        url = self.base_url + "/" + self.updating_comment_id
        self.assertEqual(ContactComment.objects.filter(id=self.updating_comment_id).count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ContactComment.objects.filter(id=self.updating_comment_id).count(), 0)

    def test_comment_of_another_user_can_be_updated(self):
        url = self.base_url + "/dd466574-10ce-4fef-bb97-7d1d8d806706"
        post_data = {
            "content": "updated Another User's comment",
            "is_important": True,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")
        self.assertTrue(updated_comment["is_important"])

    def test_comment_of_another_user_can_be_deleted(self):
        url = self.base_url + "/dd466574-10ce-4fef-bb97-7d1d8d806706"
        post_data = {
            "content": "updated Another User's comment",
        }
        self.assertEqual(ContactComment.objects.filter(id="dd466574-10ce-4fef-bb97-7d1d8d806706").count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ContactComment.objects.filter(id="dd466574-10ce-4fef-bb97-7d1d8d806706").count(), 0)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {
            "content": "test comment",
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
            "is_important": True,
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/dd466574-10ce-4fef-bb97-7d1d8d806706"
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/dd466574-10ce-4fef-bb97-7d1d8d806706"
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class ContactCommentViewMultiTenantTestCase(ReferenceViewMultiTenancyMixin, APITestCase):
    base_url = '/app_staffing/contacts'
    suffix = 'comments'
    test_patterns = ('list', 'post')
    is_paginated = False
    # Note: we won't test PATCH/DELETE to /app_staffing/comments/{contact_id}/comments/{comment_id}
    # Because those two requests are controlled by user id and already tested as ContactCommentViewTestCase.

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        contact_1 = ContactFactory(company=cls.company_1)
        contact_2 = ContactFactory(company=cls.company_2)
        # Create Comments.
        ContactCommentFactory(contact=contact_1, company=cls.company_1)
        ContactCommentFactory(contact=contact_2, company=cls.company_2)

        cls.resource_id_for_tenant_1 = contact_1.id
        cls.resource_id_for_tenant_2 = contact_2.id

    def test_can_not_list_other_contact_s_data(self):
        # Check if a user can only access data that belong to the same tenant.
        response1 = self.client_1.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_2), format='json')
        response2 = self.client_2.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_1), format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)


class ContactReferenceObjectTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    TARGET_ORGANIZATION_ID = '749303ca-7701-463e-83a8-8a5a35df1646'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Check fixture conditions
        assert Organization.objects.get(pk=cls.TARGET_ORGANIZATION_ID)
        assert Contact.objects.filter(organization=cls.TARGET_ORGANIZATION_ID)

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_delete_protection(self):
        # Check if the deletion of the organization which is referenced by some contacts will be blocked.
        response = self.client.delete(path=f'/app_staffing/organizations/{self.TARGET_ORGANIZATION_ID}')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Organization.objects.filter(id=self.TARGET_ORGANIZATION_ID).exists())
        self.assertFalse(Contact.objects.filter(organization_id=self.TARGET_ORGANIZATION_ID).exists())


class ContactViewFilterTestCase(SubTestMixin, APITestCase):
    """Check if we can search nested fields"""
    TOTAL_COUNT_OF_CONTACT = 5
    base_url = '/app_staffing/contacts'

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    @classmethod
    def setUpClass(cls):
        """Assert fixture conditions for this test."""
        super().setUpClass()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        assert len(Contact.objects.all()) == self.TOTAL_COUNT_OF_CONTACT

    def test_filter_related_organization_name(self):

        test_patterns = [
            ('full', '?organization__name=A Blacklisted Company', 1),
            ('partial', '?organization__name=Blacklisted', 1),
            ('none', '?organization__name=', self.TOTAL_COUNT_OF_CONTACT)
        ]
        self.execute_sub_test(test_patterns)

    def test_filter_related_user(self):

        test_patterns = [
            ('full', '?staff=d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', 1),
            ('none', '?staff=', self.TOTAL_COUNT_OF_CONTACT)
        ]
        self.execute_sub_test(test_patterns)

class ContactViewFilterWantsLocationTestCase(SubTestMixin, APITestCase):
    base_url = '/app_staffing/contacts'

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_wants_location_filter(self):
        organization = Organization.objects.create(
            name="test organization",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=GET_DEFAULT_COMPANY(),
            organization_category_id=4 # client
        )

        con = Contact.objects.create(
            last_name='hokkaido',
            email='hokkaido@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_hokkaido_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='touhoku',
            email='touhoku@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_touhoku_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='chubu',
            email='chubu@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_chubu_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='toukai',
            email='toukai@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_toukai_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='kansai',
            email='kansai@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_kansai_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='shikoku',
            email='shikoku@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_shikoku_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='chugoku',
            email='chugoku@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_chugoku_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='kyushu',
            email='kyushu@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_kyushu_japan = True
        contact_pref.save()

        con = Contact.objects.create(
            last_name='other',
            email='other@example.com',
            organization=organization,
            company=GET_DEFAULT_COMPANY(),
        )
        contact_pref = ContactPreference.objects.get(contact=con)
        contact_pref.wants_location_other_japan = True
        contact_pref.save()

        testCases = [
            # 1件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan"},
                "expected": ["hokkaido"],
            },
            {
                "data": {"wants_location": "wants_location_touhoku_japan"},
                "expected": ["touhoku"],
            },
            {
                "data": {"wants_location": "wants_location_chubu_japan"},
                "expected": ["chubu"],
            },
            {
                "data": {"wants_location": "wants_location_toukai_japan"},
                "expected": ["toukai"],
            },
            {
                "data": {"wants_location": "wants_location_kansai_japan"},
                "expected": ["kansai"],
            },
            {
                "data": {"wants_location": "wants_location_shikoku_japan"},
                "expected": ["shikoku"],
            },            {
                "data": {"wants_location": "wants_location_chugoku_japan"},
                "expected": ["chugoku"],
            },
            {
                "data": {"wants_location": "wants_location_kyushu_japan"},
                "expected": ["kyushu"],
            },
            {
                "data": {"wants_location": "wants_location_other_japan"},
                "expected": ["other"],
            },
            # 1件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan"},
                "expected": ["touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_touhoku_japan"},
                "expected": ["hokkaido", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chubu_japan"},
                "expected": ["hokkaido", "touhoku", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_toukai_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_kansai_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_shikoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chugoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_kyushu_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_other_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu"],
            },
            # 2件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan"},
                "expected": ["hokkaido", "touhoku"],
            },
            {
                "data": {"wants_location": "wants_location_chubu_japan,wants_location_toukai_japan"},
                "expected": ["chubu", "toukai"],
            },
            {
                "data": {"wants_location": "wants_location_kansai_japan,wants_location_shikoku_japan"},
                "expected": ["kansai", "shikoku"],
            },
            {
                "data": {"wants_location": "wants_location_chugoku_japan,wants_location_kyushu_japan"},
                "expected": ["chugoku", "kyushu"],
            },
            {
                "data": {"wants_location": "wants_location_other_japan,wants_location_hokkaido_japan"},
                "expected": ["other", "hokkaido"],
            },
            # 2件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan"},
                "expected": ["chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chubu_japan,not:wants_location_toukai_japan"},
                "expected": ["hokkaido", "touhoku", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_kansai_japan,not:wants_location_shikoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chugoku_japan,not:wants_location_kyushu_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_other_japan,not:wants_location_hokkaido_japan"},
                "expected": ["touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu"],
            },
            # 3件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan"},
                "expected": ["hokkaido", "touhoku", "chubu"],
            },
            {
                "data": {"wants_location": "wants_location_chubu_japan,wants_location_toukai_japan,wants_location_kansai_japan"},
                "expected": ["chubu", "toukai", "kansai"],
            },
            {
                "data": {"wants_location": "wants_location_kansai_japan,wants_location_shikoku_japan,wants_location_chugoku_japan"},
                "expected": ["kansai", "shikoku", "chugoku"],
            },
            {
                "data": {"wants_location": "wants_location_chugoku_japan,wants_location_kyushu_japan,wants_location_other_japan"},
                "expected": ["chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "wants_location_other_japan,wants_location_hokkaido_japan,wants_location_touhoku_japan"},
                "expected": ["other", "hokkaido", "touhoku"],
            },
            # 3件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan"},
                "expected": ["toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chubu_japan,not:wants_location_toukai_japan,not:wants_location_kansai_japan"},
                "expected": ["hokkaido", "touhoku", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_kansai_japan,not:wants_location_shikoku_japan,not:wants_location_chugoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chugoku_japan,not:wants_location_kyushu_japan,not:wants_location_other_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku"],
            },
            {
                "data": {"wants_location": "not:wants_location_other_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan"},
                "expected": ["chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu"],
            },
            # 4件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai"],
            },
            {
                "data": {"wants_location": "wants_location_kansai_japan,wants_location_shikoku_japan,wants_location_chugoku_japan,wants_location_kyushu_japan"},
                "expected": ["kansai", "shikoku", "chugoku", "kyushu"],
            },
            {
                "data": {"wants_location": "wants_location_other_japan,wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_kansai_japan"},
                "expected": ["other", "hokkaido", "touhoku", "kansai"],
            },
            # 4件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan"},
                "expected": ["toukai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_chubu_japan,not:wants_location_toukai_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan"},
                "expected": ["hokkaido", "touhoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "not:wants_location_other_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chugoku_japan"},
                "expected": ["chubu", "toukai", "kansai", "shikoku", "kyushu"],
            },
            # 5件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan,wants_location_shikoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "shikoku"],
            },
            # 5件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan"},
                "expected": ["toukai", "chugoku", "kyushu", "other"],
            },
            # 6件指定(非not)
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan,wants_location_shikoku_japan,wants_location_other_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "shikoku", "other"],
            },
            # 6件指定(not)
            {
                "data": {"wants_location": "not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan,not:wants_location_other_japan"},
                "expected": ["toukai", "chugoku", "kyushu"],
            },
            # 7件指定(非not)
            {
                "data": {"wants_location": "wants_location_kansai_japan,wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan,wants_location_shikoku_japan,wants_location_other_japan"},
                "expected": ["kansai", "hokkaido", "touhoku", "chubu", "toukai", "shikoku", "other"],
            },
            # 7件指定(not)
            {
                "data": {"wants_location": "not:wants_location_toukai_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan,not:wants_location_other_japan"},
                "expected": ["chugoku", "kyushu"],
            },
            # 8件指定(非not)
            {
                "data": {"wants_location": "wants_location_kyushu_japan,wants_location_kansai_japan,wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan,wants_location_shikoku_japan,wants_location_other_japan"},
                "expected": ["kyushu", "kansai", "hokkaido", "touhoku", "chubu", "toukai", "shikoku", "other"],
            },
            # 8件指定(not)
            {
                "data": {"wants_location": "not:wants_location_chugoku_japan,not:wants_location_toukai_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan,not:wants_location_other_japan"},
                "expected": ["kyushu"],
            },
            # 9件指定(非not)
            {
                "data": {"wants_location": "wants_location_chugoku_japan,wants_location_kyushu_japan,wants_location_kansai_japan,wants_location_hokkaido_japan,wants_location_touhoku_japan,wants_location_chubu_japan,wants_location_toukai_japan,wants_location_shikoku_japan,wants_location_other_japan"},
                "expected": ["chugoku", "kyushu", "kansai", "hokkaido", "touhoku", "chubu", "toukai", "shikoku", "other"],
            },
            # 9件指定(not)
            {
                "data": {"wants_location": "not:wants_location_kyushu_japan,not:wants_location_chugoku_japan,not:wants_location_toukai_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan,not:wants_location_chubu_japan,not:wants_location_kansai_japan,not:wants_location_shikoku_japan,not:wants_location_other_japan"},
                "expected": [],
            },
            # 非notとnotを混合指定
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,not:wants_location_hokkaido_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,not:wants_location_touhoku_japan"},
                "expected": ["hokkaido", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,not:wants_location_hokkaido_japan,not:wants_location_touhoku_japan"},
                "expected": ["hokkaido", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },
            {
                "data": {"wants_location": "wants_location_hokkaido_japan,wants_location_touhoku_japan,not:wants_location_touhoku_japan"},
                "expected": ["hokkaido", "touhoku", "chubu", "toukai", "kansai", "shikoku", "chugoku", "kyushu", "other"],
            },

        ]

        for testCase in testCases:
            response = self.client.get(path=self.base_url, data=testCase["data"], format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["display_name"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

class ColumnSettingViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    cache_key = f'{settings.CACHE_COLUMN_SETTING_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_organization'
    defaultSelectedColumnKeys = ["hoge", "fuga"]
    updatedSelectedColumnKeys = ["inu", "neko"]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_key, self.defaultSelectedColumnKeys)

    def test_get_column_setting(self):
        response = self.client.get(path='/app_staffing/column_setting/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.defaultSelectedColumnKeys)

    def test_post_column_setting(self):
        response = self.client.post(path='/app_staffing/column_setting/organization', data={ 'selectedColumnKeys': self.updatedSelectedColumnKeys }, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(cache.get(self.cache_key), self.updatedSelectedColumnKeys)

class OrganizationCsvUploadViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_post_csv(self):
        with open(file='static/app_staffing/organizations.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(corporate_number=1234567890123).count(), 1)
        self.assertEqual(Organization.objects.filter(corporate_number=2345678901234).count(), 1)
        self.assertEqual(Organization.objects.filter(corporate_number=3456789012345).count(), 1)

        org = Organization.objects.get(corporate_number=1234567890123)
        self.assertEqual(org.name, "株式会社サンプルA")
        self.assertEqual(org.category, "approached")
        self.assertEqual(org.is_blacklisted, False)
        self.assertEqual(org.score, 3)
        self.assertEqual(org.country, "JP")
        self.assertEqual(org.establishment_date, datetime.date(2000, 1, 1))
        self.assertEqual(org.settlement_month, 1)
        self.assertEqual(org.address, "東京都千代田区霞ヶ関1-1-1")
        self.assertEqual(org.building, "千代田区ビル")
        self.assertEqual(org.tel1, "03")
        self.assertEqual(org.tel2, "0000")
        self.assertEqual(org.tel3, "0000")
        self.assertEqual(org.fax1, "03")
        self.assertEqual(org.fax2, "0001")
        self.assertEqual(org.fax3, "0000")
        self.assertEqual(org.domain_name, "https://cmrb_sample.jp1")
        self.assertEqual(org.employee_number, "low")
        self.assertEqual(org.contract, False)
        self.assertEqual(org.capital_man_yen, 1000)
        self.assertEqual(org.has_p_mark_or_isms, True)
        self.assertEqual(org.has_invoice_system, True)
        self.assertEqual(org.has_haken, True)
        self.assertEqual(org.has_distribution, False)
        self.assertEqual(org.establishment_year, 1)
        self.assertEqual(org.capital_man_yen_required_for_transactions, 1)
        self.assertEqual(org.p_mark_or_isms, False)
        self.assertEqual(org.invoice_system, False)
        self.assertEqual(org.haken, False)
        self.assertEqual(org.organization_category, OrganizationCategory.objects.get(name="approached"))
        self.assertEqual(org.organization_employee_number, OrganizationEmployeeNumber.objects.get(name="low"))
        self.assertEqual(org.organization_country, OrganizationCountry.objects.get(name="JP"))

        with open(file='static/app_staffing/organizations.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 1234567890123は法人番号として登録できません。既に登録されています。')
        self.assertEqual(response.data['errorMessages'][1], '2行目: 2345678901234は法人番号として登録できません。既に登録されています。')
        self.assertEqual(response.data['errorMessages'][2], '3行目: 3456789012345は法人番号として登録できません。既に登録されています。')

    def test_post_csv_date_format(self):
        with open(file='app_staffing/tests/files/organization_date_format.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organization_date_format.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(corporate_number=1234560000000).count(), 1)
        self.assertEqual(Organization.objects.get(corporate_number=1234560000000).establishment_date, datetime.date(2020, 1, 1))
        self.assertEqual(Organization.objects.filter(corporate_number=1234570000000).count(), 1)
        self.assertEqual(Organization.objects.get(corporate_number=1234570000000).establishment_date, datetime.date(2020, 1, 1))
        self.assertEqual(Organization.objects.filter(corporate_number=1234580000000).count(), 1)
        self.assertEqual(Organization.objects.get(corporate_number=1234580000000).establishment_date, datetime.date(2020, 1, 1))
        self.assertEqual(Organization.objects.filter(corporate_number=1234590000000).count(), 1)
        self.assertEqual(Organization.objects.get(corporate_number=1234590000000).establishment_date, datetime.date(2020, 1, 1))

    def test_post_only_required_csv(self):
        with open(file='app_staffing/tests/files/organizations_only_required.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_only_required.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(name='必須項目テスト会社名').count(), 1)

    def test_post_validation_error_csv(self):
        with open(file='app_staffing/tests/files/organization_validation_error.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organization_validation_error.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: 株式会社サンプルaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbは取引先名として登録できません。100文字以内で入力してください。',
            '1行目: 東京都千代田区霞ヶ関1-1-1千代田区ビルaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbは住所として登録できません。建物名を合わせて100文字以内で入力してください。',
            '1行目: 033333333-0000-0000はTELとして登録できません。半角ハイフンを含めて17文字以内で入力してください。',
            '1行目: 033333333-0001-0000はFAXとして登録できません。半角ハイフンを含めて17文字以内で入力してください。',
            '1行目: https://cmrb_sampleaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb.jp1はURLとして登録できません。50文字以内で入力してください。',
            '1行目: 10000000000000000は資本金として登録できません。9桁以内で入力してください。',
            '1行目: 10000は取引に必要な設立年数として登録できません。3桁以内で入力してください。',
            '1行目: 10000000000000000は取引に必要な資本金として登録できません。9桁以内で入力してください。'
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Organization.objects.filter().count(), 0)

    def test_post_multitenant_corporatenumber(self):
        # 他テナントで法人番号が使われていても登録できる
        another_company_id = "0639df05-8db5-40f2-a25d-6bcd3f8ae4ca"
        Organization.objects.create(company_id=another_company_id, score=1, corporate_number=1234567890123)
        Organization.objects.create(company_id=another_company_id, score=1, corporate_number=2345678901234)
        Organization.objects.create(company_id=another_company_id, score=1, corporate_number=3456789012345)

        with open(file='static/app_staffing/organizations.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(company_id=DEFAULT_COMPANY_ID, corporate_number=1234567890123).count(), 1)
        self.assertEqual(Organization.objects.filter(company_id=DEFAULT_COMPANY_ID, corporate_number=2345678901234).count(), 1)
        self.assertEqual(Organization.objects.filter(company_id=DEFAULT_COMPANY_ID, corporate_number=3456789012345).count(), 1)

    def test_post_csv_with_space(self):
        with open(file='app_staffing/tests/files/organizations_with_space.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_with_space.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        expeted_error_messages = [
            '1行目: 12345700 00000は法人番号として登録できません。13桁の数字で入力してください。',
            '1行目: 株式会社   SPACE3は取引先名として登録できません。スペースの入力はできません。',
            '1行目: 東京都千代田区  霞ヶ関1-1-1は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。',
            '2行目: 234568000 0000は法人番号として登録できません。13桁の数字で入力してください。',
            '2行目: 株式会社 サンプルSPACE1は取引先名として登録できません。スペースの入力はできません。',
            '2行目: 東京都渋谷区  恵比寿1-1-1は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。',
            '3行目: 34567 90000000は法人番号として登録できません。13桁の数字で入力してください。',
            '3行目: 株式会社    SPACE5は取引先名として登録できません。スペースの入力はできません。',
            '3行目: 東京都港 区六本木1-1-1は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'
        ]
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter().count(), 0)
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)

    def test_post_csv_with_blacklist(self):
        with open(file='app_staffing/tests/files/organizations_is_blacklisted.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_is_blacklisted.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(is_blacklisted=True).count(), 2)

    def test_post_require_items_null_csv(self):
        with open(file='app_staffing/tests/files/organizations_require_items_null.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_require_items_null.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: 取引先名は必須項目です。',
            '2行目: 取引先ステータスは必須項目です。',
            '3行目: 取引先評価は必須項目です。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Organization.objects.filter().count(), 0)

    def test_post_format_error_csv(self):
        with open(file='app_staffing/tests/files/organizations_format_error.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_format_error.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: 12345700-00000は法人番号として登録できません。13桁の数字で入力してください。',
            '1行目: 取引先名は必須項目です。',
            '1行目: 取引先ステータスは必須項目です。',
            '1行目: 取引先評価は必須項目です。',
            '2行目: 1は取引先ステータスとして登録できません。「見込み客」「アプローチ済」「情報交換済」「契約実績有」のいずれかを入力してください。',
            '2行目: 取引先評価は必須項目です。',
            '3行目: oは取引先評価として登録できません。「1」から「5」 (半角数字)のいずれかを入力してください。',
            '4行目: 0は取引先評価として登録できません。「1」から「5」 (半角数字)のいずれかを入力してください。',
            '5行目: 6は取引先評価として登録できません。「1」から「5」 (半角数字)のいずれかを入力してください。',
            '6行目: USは国籍として登録できません。「JP」「KR」「CN」「OTHER」のいずれかを入力してください。',
            '7行目: 20-01-01は設立年月として登録できません。yyyy-mmの形式で入力してください。',
            '8行目: 2000-001-001は設立年月として登録できません。yyyy-mmの形式で入力してください。',
            '9行目: 13は決算期として登録できません。「1」から「12」 (半角数字)のいずれかを入力してください。',
            '10行目: -0000-0000はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '11行目: 03-abc-0000はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '11行目: 03-abc-0000はTELとして登録できません。半角数字で入力してください。',
            '12行目: 03--0000はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '13行目: 03-0001-efgはFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '13行目: 03-0001-efgはFAXとして登録できません。半角数字で入力してください。',
            '14行目: 1~30名は社員数として登録できません。「~10名」「11~30名」「31~50名」「51~100名」「101~300名」「301名~」のいずれかを入力してください。',
            '15行目: 2は商流として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '16行目: 2は請負として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '17行目: oは資本金として登録できません。半角の整数で入力してください。',
            '18行目: 2は保有資格 > Pマーク／ISMSとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '19行目: 2は保有資格 > インボイス登録事業者として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '20行目: 2は保有資格 > 労働者派遣事業として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '21行目: oは取引に必要な設立年数として登録できません。半角の整数で入力してください。',
            '22行目: oは取引に必要な資本金として登録できません。半角の整数で入力してください。',
            '23行目: 2は取引に必要な資格 > Pマーク／ISMSとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '24行目: 2は取引に必要な資格 > インボイス登録事業者として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '25行目: 2は取引に必要な資格 > 労働者派遣事業として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '26行目: 2はブロックリストとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '27行目: カラム数が正しくありません。',
            '28行目: カラム数が正しくありません。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Organization.objects.filter().count(), 0)

    def test_post_illegal_format(self):
        with open(file='app_staffing/tests/files/organization_illegal_format.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organization_illegal_format.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: カラム数が正しくありません。')
        self.assertEqual(response.data['errorMessages'][1], '2行目: カラム数が正しくありません。')

    def test_post_with_display_setting(self):
        headers = {
            'HTTP_CONTENT_TYPE': 'multipart/form-data',
            'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organization_with_display_setting.csv"'
        }
        display_setting = DisplaySetting.objects.create(company_id=DEFAULT_COMPANY_ID, content=json.dumps({
            "organizations": {"require": ['corporate_number']},
        }))

        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 法人番号は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['country']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 国籍は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['establishment_date']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 設立年月は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['settlement_month']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 決算期は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['address']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 住所(市区町村・町名・番地)は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['tel']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: TELは必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['fax']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: FAXは必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['domain_name']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: URLは必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['employee_number']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 社員数は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['has_distribution']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 商流は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['contract']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 請負は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['capital_man_yen']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 資本金は必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": ['license']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 保有資格 > Pマーク／ISMSは必須項目です。')

        display_setting.content = json.dumps({"organizations": {"require": []}})
        display_setting.save()
        with open(file='app_staffing/tests/files/organization_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/organizations/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Organization.objects.filter(name='株式会社サンプルA').count(), 1)

    def test_post_future_last_visit_date(self):
        target_datetime = datetime.datetime(year=2020, month=1, day=2)

        with freeze_time(target_datetime):
            with open(file='app_staffing/tests/files/organizations_future_establishment_yyyymm.csv', mode='rb') as csv:
                headers = {
                    'HTTP_CONTENT_TYPE': 'multipart/form-data',
                    'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_future_establishment_yyyymm.csv"'
                }

                response = self.client.post(
                    headers=headers,
                    path='/app_staffing/organizations/csv_upload',
                    data={ 'file': csv }
                )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            expeted_error_messages = [
                '1行目: 2030-01は設立年月として登録できません。未来日付の登録は不可です。',
            ]
            self.assertEqual(response.data['errorMessages'], expeted_error_messages)
            self.assertEqual(Organization.objects.filter().count(), 0)

    def test_post_with_branch(self):
        with open(file='app_staffing/tests/files/organizations_with_branch.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations_with_branch.csv"'
            }
            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data['errorMessages']), 0)
        self.assertEqual(Organization.objects.filter(corporate_number=1234567890123).count(), 1)
        org = Organization.objects.get(corporate_number=1234567890123)
        branches = OrganizationBranch.objects.filter(organization_id=org.id)
        self.assertEqual(len(branches), 2)

        branchA = OrganizationBranch.objects.get(organization_id=org.id, name="A")
        self.assertEqual(branchA.address, "C")
        self.assertEqual(branchA.building, "E")
        self.assertEqual(branchA.tel1, "0")
        self.assertEqual(branchA.tel2, "1")
        self.assertEqual(branchA.tel3, "2")
        self.assertEqual(branchA.fax1, "6")
        self.assertEqual(branchA.fax2, "7")
        self.assertEqual(branchA.fax3, "8")

        branchB = OrganizationBranch.objects.get(organization_id=org.id, name="B")
        self.assertEqual(branchB.address, "D")
        self.assertEqual(branchB.building, "F")
        self.assertEqual(branchB.tel1, "3")
        self.assertEqual(branchB.tel2, "4")
        self.assertEqual(branchB.tel3, "5")
        self.assertEqual(branchB.fax1, "9")
        self.assertEqual(branchB.fax2, "0")
        self.assertEqual(branchB.fax3, "1")

class ContactCsvUploadViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/contact_csv_upload.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_post_csv(self):
        with open(file='static/app_staffing/contacts.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contacts.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'], [])
        self.assertEqual(Contact.objects.filter(email='sample-a@example.com').count(), 1)
        self.assertEqual(Contact.objects.filter(email='sample-b@example.com').count(), 1)
        self.assertEqual(Contact.objects.filter(email='sample-c@example.com').count(), 1)

        sample_contact = Contact.objects.get(email='sample-a@example.com')
        self.assertEqual(sample_contact.contactpreference.wants_location_kanto_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_kansai_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_hokkaido_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_touhoku_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_chubu_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_kyushu_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_other_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_chugoku_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_shikoku_japan, True)
        self.assertEqual(sample_contact.contactpreference.wants_location_toukai_japan, True)
        self.assertEqual(sample_contact.contactpreference.job_koyou_proper, True)
        self.assertEqual(sample_contact.contactpreference.job_koyou_free, True)
        self.assertEqual(sample_contact.contactpreference.job_syouryu, 1)
        self.assertEqual(sample_contact.contactpreference.personnel_syouryu, 1)
        self.assertEqual(sample_contact.contactjobtypepreferences.count(), 15)
        self.assertEqual(sample_contact.contactjobskillpreferences.count(), 15)
        self.assertEqual(sample_contact.contactpersonneltypepreferences.count(), 15)
        self.assertEqual(sample_contact.contactpersonnelskillpreferences.count(), 15)
        self.assertEqual(sample_contact.categories.all()[0].category, 'heart')
        self.assertEqual(sample_contact.tag_assignment.count(), 3)

    def test_post_csv_date_format(self):
        with open(file='app_staffing/tests/files/contact_date_format.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_date_format.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'], [])
        self.assertEqual(Contact.objects.filter(email='sample-a@example.com').count(), 1)
        self.assertEqual(Contact.objects.get(email='sample-a@example.com').last_visit, datetime.date(2020, 12, 31))
        self.assertEqual(Contact.objects.filter(email='sample-b@example.com').count(), 1)
        self.assertEqual(Contact.objects.get(email='sample-b@example.com').last_visit, datetime.date(2020, 12, 31))

    def test_post_preference_error_csv(self):
        with open(file='app_staffing/tests/files/contact_preference_error.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_preference_error.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Contact.objects.filter(email='sample-c@example.com').count(), 0)
        error_messages = [
            '1行目: 案件配信 > 開発の職種詳細とスキル詳細は必ず1つ選択してください',
            '1行目: 案件配信 > インフラの職種詳細とスキル詳細は必ず1つ選択してください',
            '1行目: 要員配信 > 開発の職種詳細とスキル詳細は必ず1つ選択してください',
            '1行目: 要員配信 > インフラの職種詳細とスキル詳細は必ず1つ選択してください',
            '1行目: 希望エリアは必ず1つ選択してください',
            '1行目: 案件配信 > 商流制限は必ず1つ選択してください',
            '1行目: 要員配信 > 商流制限は必ず1つ選択してください',
            '1行目: 要員配信 > 希望雇用形態は必ず1つ選択してください'
        ]
        self.assertEqual(response.data['errorMessages'], error_messages)

    def test_post_validation_error_csv(self):
        with open(file='app_staffing/tests/files/contact_validation_error.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_validation_error.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: サンプルA姓aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbサンプルA名は取引先担当者名として登録できません。姓名合わせて50文字以内で入力してください。',
            '1行目: sample-aaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.comはメールアドレス > TOとして登録できません。100文字以内で入力してください。',
            '1行目: メールアドレス > CCはカンマを含めて500文字以内で入力してください。',
            '1行目: 0100000000-2345-6789はTELとして登録できません。半角ハイフンを含めて17文字以内で入力してください。',
            '1行目: サンプル役職aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbは役職として登録できません。50文字以内で入力してください。',
            '1行目: サンプル部署aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbは部署として登録できません。50文字以内で入力してください。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)

    def test_post_csv_with_space(self):
        with open(file='app_staffing/tests/files/contact_with_space.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_with_space.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        expeted_error_messages = [
            '1行目: サンプルA姓 SPACE1は取引先担当者名(姓)として登録できません。スペースの入力はできません。',
            '2行目: サンプル  B姓SPACE2は取引先担当者名(姓)として登録できません。スペースの入力はできません。',
            '3行目: サンプル   C姓SPACE3は取引先担当者名(姓)として登録できません。スペースの入力はできません。',
        ]
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Contact.objects.filter(last_name='サンプルA姓SPACE1').count(), 0)
        self.assertEqual(Contact.objects.filter(last_name='サンプルB姓SPACE2').count(), 0)
        self.assertEqual(Contact.objects.filter(last_name='サンプルC姓SPACE3').count(), 0)

    def test_post_staff_not_assigned(self):
        with open(file='app_staffing/tests/files/contact_staff_not_assigned.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_staff_not_assigned.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Contact.objects.filter(email='sample-a@example.com').count(), 1)
        self.assertEqual(Contact.objects.get(email='sample-a@example.com').staff, None)

    def test_post_email_duplicated(self):
        with open(file='app_staffing/tests/files/contact_email_duplicated.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_email_duplicated.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '3行目: sample-a@example.comはメールアドレス > TOとして登録できません。値がファイル内で重複しています。')

    def test_post_already_registered(self):
        with open(file='static/app_staffing/contacts.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contacts.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        with open(file='static/app_staffing/contacts.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contacts.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: sample-a@example.comはメールアドレス > TOとして登録できません。既に登録されています。')

    def test_post_illegal_format(self):
        with open(file='app_staffing/tests/files/contact_illegal_format.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_illegal_format.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: カラム数が正しくありません。')
        self.assertEqual(response.data['errorMessages'][1], '2行目: カラム数が正しくありません。')

    def test_post_with_display_setting(self):
        headers = {
            'HTTP_CONTENT_TYPE': 'multipart/form-data',
            'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_with_display_setting.csv"'
        }
        display_setting = DisplaySetting.objects.create(company_id=DEFAULT_COMPANY_ID, content=json.dumps({
            "contacts": {"require": ['email_cc']},
        }))
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: メールアドレス > CCは必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['tel']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: TELは必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['position']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 役職は必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['department']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 部署は必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['staff']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 自社担当(メールアドレス)は必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['tag']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: タグは必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['category']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['errorMessages'][0], '1行目: 相性は必須項目です。')

        display_setting.content = json.dumps({"contacts": {"require": ['']}})
        display_setting.save()
        with open(file='app_staffing/tests/files/contact_with_display_setting.csv', mode='rb') as csv:
            response = self.client.post(headers=headers, path='/app_staffing/contacts/csv_upload', data={ 'file': csv })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Contact.objects.filter(email='sample-a@example.com').count(), 1)

    def test_post_require_items_null_csv(self):
        with open(file='app_staffing/tests/files/contact_require_items_null.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_require_items_null.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: 取引先担当者名(姓)は必須項目です。',
            '1行目: 所属取引先は必須項目です。',
            '1行目: メールアドレス > TOは必須項目です。'
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Contact.objects.filter().count(), 0)

    def test_post_illegal_format_items_csv(self):
        with open(file='app_staffing/tests/files/contact_illegal_format_items.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_illegal_format_items.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: no organizationは未登録の取引先です。先に取引先の登録を行ってください。',
            '1行目: sample-a.example.comはメールアドレス > TOとして登録できません。有効なメールアドレスを入力してください。',
            '1行目: sample-b.example.comはメールアドレス > CCとして登録できません。有効なメールアドレスを入力してください。',
            '1行目: sample-c@exampleはメールアドレス > CCとして登録できません。有効なメールアドレスを入力してください。',
            '1行目: sample-user.example.comに紐づくユーザーが見当たりません。再度ユーザーのメールアドレスをご確認ください。',
            '1行目: 2020=01=01は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。',
            '1行目: GOODは相性として登録できません。「良い」「悪い」のいずれかを入力してください。',
            '3行目: -2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '4行目: 1--3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '5行目: 1-2-はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '6行目: a-2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '6行目: a-2-3はTELとして登録できません。半角数字で入力してください。',
            '7行目: 1-b-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '7行目: 1-b-3はTELとして登録できません。半角数字で入力してください。',
            '8行目: 1-2-cはTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
            '8行目: 1-2-cはTELとして登録できません。半角数字で入力してください。',
            '9行目: 1-23はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Contact.objects.filter().count(), 0)

    def test_post_over_limit_tags_ccmails_csv(self):
        with open(file='app_staffing/tests/files/contact_over_limit_tags_ccmails.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_over_limit_tags_ccmails.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '2行目: メールアドレス > CCの登録可能数上限5件を超えています。',
            '2行目: タグの登録可能数上限10件を超えています。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Contact.objects.filter().count(), 0)

    def test_post_contact_illegal_preference_csv(self):
        with open(file='app_staffing/tests/files/contact_illegal_preference_error.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_illegal_preference_error.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        expeted_error_messages = [
            '1行目: 2は希望エリア > 北海道として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 東北として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 関東として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 中部として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 東海として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 関西として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 四国として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 中国として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > 九州として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は希望エリア > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > 職種詳細 > デザイナーとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > 職種詳細 > フロントエンドとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > 職種詳細 > バックエンドとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > 職種詳細 > PM・ディレクターとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 要件定義として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 基本設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 詳細設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 製造として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > テスト・検証として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 保守・運用として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 開発 > スキル詳細 > 未経験として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > サーバーとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > ネットワークとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > セキュリティとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > データベースとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > 情報システムとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 要件定義として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 基本設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 詳細設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 構築として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > テスト・検証として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 保守・運用として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 監視として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > インフラ > スキル詳細 > 未経験として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > その他 > 職種詳細 > 営業・事務として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > その他 > 職種詳細 > 基地局として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > その他 > 職種詳細 > コールセンター・サポートデスクとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > その他 > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は案件配信 > 商流制限として登録できません。「なし」「エンド直・元請直まで」「1次請まで」「2次請まで」のいずれかを入力してください。',
            '1行目: 2は要員配信 > 開発 > 職種詳細 > デザイナーとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > 職種詳細 > フロントエンドとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > 職種詳細 > バックエンドとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > 職種詳細 > PM・ディレクターとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 要件定義として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 基本設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 詳細設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 製造として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > テスト・検証として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 保守・運用として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 開発 > スキル詳細 > 未経験として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > サーバーとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > ネットワークとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > セキュリティとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > データベースとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > 情報システムとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 要件定義として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 基本設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 詳細設計として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 構築として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > テスト・検証として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 保守・運用として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 監視として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > インフラ > スキル詳細 > 未経験として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > その他 > 職種詳細 > 営業・事務として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > その他 > 職種詳細 > 基地局として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > その他 > 職種詳細 > コールセンター・サポートデスクとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > その他 > 職種詳細 > その他として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 希望雇用形態 > プロパーとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 希望雇用形態 > フリーランスとして登録できません。「0」と「1」(半角数字)または空欄のみ有効です。',
            '1行目: 2は要員配信 > 商流制限として登録できません。「なし」「自社所属まで」「1社先所属まで」「2社先所属まで」のいずれかを入力してください。',
        ]
        self.assertEqual(response.data['errorMessages'], expeted_error_messages)
        self.assertEqual(Contact.objects.filter().count(), 0)

    def test_post_future_last_visit_date(self):
        target_datetime = datetime.datetime(year=2020, month=1, day=2)

        with freeze_time(target_datetime):
            with open(file='app_staffing/tests/files/contact_future_last_visit_date.csv', mode='rb') as csv:
                headers = {
                    'HTTP_CONTENT_TYPE': 'multipart/form-data',
                    'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_future_last_visit_date.csv"'
                }

                response = self.client.post(
                    headers=headers,
                    path='/app_staffing/contacts/csv_upload',
                    data={ 'file': csv }
                )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            expeted_error_messages = [
                '3行目: 2020-01-03は最終商談日として登録できません。未来日付の登録は不可です。',
            ]
            self.assertEqual(response.data['errorMessages'], expeted_error_messages)
            self.assertEqual(Contact.objects.filter().count(), 0)

    def test_post_null_staff(self):
        with open(file='app_staffing/tests/files/contact_null_staff.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contact_null_staff.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Contact.objects.filter().count(), 1)
        self.assertEqual(Contact.objects.filter()[0].staff, None)


class OrganizationViewFilterTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organizationSearchFilter/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizationSearchFilter/organizations.json',
        'app_staffing/tests/fixtures/organizationSearchFilter/exceptional_organizations.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get(self):
        response = self.client.get(path='/app_staffing/organizations')
        self.assertEqual(4, response.data["count"])
        expected_ids = [
            "749303ca-7701-463e-83a8-8a5a35df1646",
            "5468c29f-7b35-4540-b9d0-c1807d945736",
            "5468c29f-7b35-4540-b9d0-c1807d945735",
            "5468c29f-7b35-4540-b9d0-c1807d945737",
        ]
        for result in response.data["results"]:
            self.assertTrue(result["id"] in expected_ids)

    def test_get_pmark_or_isms_or_haken(self):
        response = self.client.get(path='/app_staffing/organizations?license=has_p_mark_or_isms')
        self.assertEqual(response.data["count"], 1)
        self.assertEqual("749303ca-7701-463e-83a8-8a5a35df1646", response.data["results"][0]["id"])

        response = self.client.get(path='/app_staffing/organizations?license=has_haken')
        self.assertEqual(response.data["count"], 1)
        self.assertEqual("5468c29f-7b35-4540-b9d0-c1807d945735", response.data["results"][0]["id"])

        response = self.client.get(path='/app_staffing/organizations?license=has_p_mark_or_isms%2Chas_haken')
        self.assertEqual(response.data["count"], 2)
        self.assertEqual("749303ca-7701-463e-83a8-8a5a35df1646", response.data["results"][0]["id"])
        self.assertEqual("5468c29f-7b35-4540-b9d0-c1807d945735", response.data["results"][1]["id"])


class CommentTemplateViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    resource_names = ["organization", "contact"]
    cache_keys = [
        f'{settings.CACHE_COMMENT_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_organization',
        f'{settings.CACHE_COMMENT_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_contact',
    ]
    default = [{'title': 'hoge', 'content': 'hogehoge'}]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    post_data = { 'templates': [{'title': 'hoge', 'content': 'hogehoge'}, {'title': 'fuga', 'content': 'fugafuga'}] }

    post_data_over_default_limit = { 'templates': [
        {'title': 'hoge', 'content': 'hogehoge'},
        {'title': 'fuga', 'content': 'fugafuga'},
        {'title': 'hoge2', 'content': 'hogehoge2'},
        {'title': 'fuga2', 'content': 'fugafuga2'},
    ] }

    invalid_data = {'templates': [{
        'title': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
        'content': """
            aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
            aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
            aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
            aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
            aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
            ccc
        """,
    }]}

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        for cache_key in self.cache_keys:
            cache.set(cache_key, self.default)

    def test_get_comment_template(self):
        for resource_name in self.resource_names:
            response = self.client.get(path=f'/app_staffing/comment_template/{resource_name}', format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data, self.default_with_total_count)

    def test_post_comment_template(self):
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)

    def test_over_max_length(self):
        for resource_name in self.resource_names:
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.invalid_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['title']), settings.LENGTH_VALIDATIONS['comment_template']['title']['message'])
            self.assertEqual(str(response.data['content']), settings.LENGTH_VALIDATIONS['comment_template']['content']['message'])

    def test_not_null_value(self):
        for resource_name in self.resource_names:
            post_data = { 'templates': [{}]}
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['detail']), TemplateNameBlankException.default_detail)

            post_data = { 'templates': [{'title': None, 'content': None}]}
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['detail']), TemplateNameBlankException.default_detail)

            post_data = { 'templates': [{'title': 'test', 'content': None}]}
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['content']), settings.NOT_NULL_VALIDATIONS['comment_template']['message'])

    def test_post_comment_template_over_limit(self):
        for resource_name in self.resource_names:
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data_over_default_limit, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

    # patchやdeleteは利用せず、postで上書きする
    def test_update_by_post(self):
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.get(path=f'/app_staffing/comment_template/{resource_name}', format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data, self.default_with_total_count)

            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)
            self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

            post_data = { 'templates': []}
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 0)

    def test_duplicate_template_title(self):
        for resource_name in self.resource_names:
            post_data = { 'templates': [
                {'title': 'fuga', 'content': 'fuga1'},
                {'title': 'fuga', 'content': 'fuga2'},
            ]}
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data['detail'], TemplateNameRegisteredException.default_detail)

    def test_post_template_same_name(self):
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)
            self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)
            self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

    def test_post_template_same_name_multi_tenancy(self):
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)
            self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

        # 別テナントのユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        for i, resource_name in enumerate(self.resource_names):
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(len(cache.get(self.cache_keys[i])), 2)
            self.assertEqual(response.data, {'templates': self.post_data['templates'], 'total_available_count': 3})

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        for resource_name in self.resource_names:
            response = self.client.get(path=f'/app_staffing/comment_template/{resource_name}', format='json')
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        for resource_name in self.resource_names:
            response = self.client.post(path=f'/app_staffing/comment_template/{resource_name}', data=self.post_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class MyCompanyViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]
    base_url = '/app_staffing/my_company'

    @classmethod
    def setUpClass(cls):
        # Assert fixture conditions..
        super().setUpClass()
        assert User.objects.get(pk="d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", username=NORMAL_USER_NAME, is_user_admin=False)

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_get(self):
        response = self.client.get(path=self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_not_update(self):
        response = self.client.patch(path=self.base_url, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ContactViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_scores.json',
        'app_staffing/tests/fixtures/contact_preferences.json',
        'app_staffing/tests/fixtures/tags.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
    ]
    base_url = '/app_staffing/contacts'
    resource_id = '78630567-72ce-49a3-84e6-8947b5b057f3'

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_delete(self):
        response = self.client.delete(path=self.base_url + "/{0}".format(self.resource_id), data={})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class OrganizationViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
    ]
    base_url = '/app_staffing/organizations'
    resource_id = '749303ca-7701-463e-83a8-8a5a35df1646'

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_delete(self):
        response = self.client.delete(path=self.base_url + "/{0}".format(self.resource_id), data={})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class OrganizationCsvUploadViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_post_csv(self):
        with open(file='static/app_staffing/organizations.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="organizations.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/organizations/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ContactCsvUploadViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/contact_csv_upload.json',
    ]

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_post_csv(self):
        with open(file='static/app_staffing/contacts.csv', mode='rb') as csv:
            headers = {
                'HTTP_CONTENT_TYPE': 'multipart/form-data',
                'HTTP_CONTENT_DISPOSITION': 'form-data; name="file"; filename="contacts.csv"'
            }

            response = self.client.post(
                headers=headers,
                path='/app_staffing/contacts/csv_upload',
                data={ 'file': csv }
            )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ColumnSettingViewPermissionTestCaseWithNormalUser(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_column_setting(self):
        response = self.client.get(path='/app_staffing/column_setting/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DisplaySettingViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    patch_params = {
        "organizations": {"search": ['test'], "table": ['test'], "require": ['name', 'category']},
        "contacts": {"search": ['test'], "table": ['test'], "require": ['name', 'organization', 'email_to']},
        "shared_emails": {"search": ['test'], "table": ["staff", "date_range"], "require": ['test']},
        "scheduled_mails": {"search": ['test'], "table": ['test'], "require": ['test']},
        "users": {"search": ['test'], "table": ['test'], "require": ['test']}
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_column_setting(self):
        response = self.client.get(path='/app_staffing/display_setting', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['content_hash'], settings.DISPLAY_SETTING_DEFAULT)

    def test_patch_column_setting(self):
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': self.patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        get_response = self.client.get(path='/app_staffing/display_setting', format='json')
        self.assertEqual(get_response.data['content_hash'], self.patch_params)

    def test_patch_all_column_setting(self):
        patch_params = {
            "organizations": {
                "search": [],
                "table": [],
                "require": [
                    "name",
                    "category",
                    "corporate_number",
                    "score",
                    "country",
                    "establishment_date",
                    "settlement_month",
                    "address",
                    "tel",
                    "fax",
                    "domain_name",
                    "employee_number",
                    "has_distribution",
                    "contract",
                    "capital_man_yen",
                    "license"
                ],
                "page_size": 10
            },
            "contacts": {
                "search": [],
                "table": [],
                "require": [
                    "name",
                    "organization",
                    "email_to",
                    "department",
                    "email_cc",
                    "tel",
                    "position",
                    "staff",
                    "last_visit",
                    "tag",
                    "category"
                ],
                "page_size": 10
            },
            "shared_emails": {
                "search": [],
                "table": [],
                "require": [],
                "page_size": 10
            },
            "scheduled_mails": {
                "search": [],
                "table": [],
                "require": [],
                "page_size": 10
            },
            "users": {
                "search": [],
                "table": [],
                "require": [],
                "page_size": 10
            },
        }

        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        get_response = self.client.get(path='/app_staffing/display_setting', format='json')
        self.assertEqual(get_response.data['content_hash'], patch_params)

    def test_patch_minimum_column_setting(self):
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': settings.DISPLAY_SETTING_DEFAULT}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        get_response = self.client.get(path='/app_staffing/display_setting', format='json')
        self.assertEqual(get_response.data['content_hash'], settings.DISPLAY_SETTING_DEFAULT)

    def test_patch_must_require_column_setting_organizations(self):
        patch_params = copy.deepcopy(settings.DISPLAY_SETTING_DEFAULT)

        patch_params["organizations"]["require"] = ["name"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["organizations"]["require"] = ["category"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["organizations"]["require"] = ["test"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["organizations"]["require"] = []
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_must_require_column_setting_contacts(self):
        patch_params = copy.deepcopy(settings.DISPLAY_SETTING_DEFAULT)

        patch_params["contacts"]["require"] = ["name", "organization"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["contacts"]["require"] = ["name", "email_to"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["contacts"]["require"] = ["organization", "email_to"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["contacts"]["require"] = ["test"]
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        patch_params["contacts"]["require"] = []
        response = self.client.patch(path='/app_staffing/display_setting', data={'content': patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UserDisplaySettingViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    patch_params = {
        "organizations": {"search": ['test'], "table": ['test'], "require": ['test'], "page_size": 10},
        "contacts": {"search": ['test'], "table": ['test'], "require": ['test'], "page_size": 10},
        "shared_emails": {"search": ['test'], "table": ["staff", "date_range"], "require": ['test'], "page_size": 10},
        "scheduled_mails": {"search": ['test'], "table": ['test'], "require": ['test'], "page_size": 10},
        "users": {"search": ['test'], "table": ['test'], "require": ['test'], "page_size": 10}
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_column_setting(self):
        response = self.client.get(path='/app_staffing/user_display_setting', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['content_hash'], settings.USER_DISPLAY_SETTING_DEFAULT)

    def test_patch_column_setting(self):
        response = self.client.patch(path='/app_staffing/user_display_setting', data={'content': self.patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        get_response = self.client.get(path='/app_staffing/user_display_setting', format='json')
        self.assertEqual(get_response.data['content_hash'], self.patch_params)

    def test_partial_update_display_setting(self):
        new_user_page_size = 100
        new_user_search_list = []
        update_data = {
            'users': {
                'page_size': new_user_page_size,
                'search': new_user_search_list,
            }
        }
        url = reverse('user_display_setting')
        # NOTE(joshua-hashimoto): 最初にpatch_paramsをデフォルト値として設定
        initial_response = self.client.patch(url, data={'content': self.patch_params}, format='json')
        initial_user_display = initial_response.data['content_hash']
        self.assertEqual(initial_user_display['users']['page_size'], 10)
        self.assertEqual(initial_user_display['users']['search'], ['test'])
        self.assertEqual(initial_user_display['organizations']['page_size'], 10)
        self.assertEqual(initial_user_display['organizations']['search'], ['test'])
        # NOTE(joshua-hashimoto): テストしたい更新
        response = self.client.patch(url, data={'content': update_data}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # NOTE(joshua-hashimoto): APIから最新のユーザー表示項目制御の値を取得
        get_response = self.client.get(url, format='json')
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)

        content_hash = get_response.data['content_hash']
        self.assertEqual(content_hash['users']['page_size'], 100)
        self.assertEqual(content_hash['users']['search'], [])
        self.assertEqual(content_hash['organizations']['page_size'], 10)
        self.assertEqual(content_hash['organizations']['search'], ['test'])

    def test_unauth_user_cannot_get_column_setting(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/user_display_setting', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch_column_setting(self):
        self.client.logout()
        response = self.client.patch(path='/app_staffing/user_display_setting', data={'content': self.patch_params}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class CustomAuthTokenViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/company_attribute.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def test_get_new_token(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token = response.data['token']

        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_token = response.data['token']

        self.assertNotEqual(token, new_token)
        header = {'HTTP_AUTHORIZATION': f'Token {new_token}'}
        response = self.client.get(path='/app_staffing/organizations', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_no_token(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        token = Token.objects.get(key=response.data['token'])
        token.delete()

        header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
        response = self.client.get(path='/app_staffing/organizations', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_is_not_active(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        token = Token.objects.get(key=response.data['token'])
        token.created = datetime.datetime.now() - datetime.timedelta(days=1)
        token.save()

        user = User.objects.get(id=token.user_id)
        user.is_active = False
        user.save()

        header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
        response = self.client.get(path='/app_staffing/organizations', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_token_expired(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        token = Token.objects.get(key=response.data['token'])
        token.created = datetime.datetime.now() - datetime.timedelta(days=1)
        token.save()

        header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
        response = self.client.get(path='/app_staffing/organizations', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_generate_token_with_company_deactivated(self):
        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        company.deactivated_time = datetime.datetime.now()
        company.save()
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_auth_with_company_deactivated(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        company.deactivated_time = datetime.datetime.now()
        company.save()

        token = Token.objects.get(key=response.data['token'])
        header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
        response = self.client.get(path='/app_staffing/organizations', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_trial_expired(self):
        freezer = freeze_time('2021-12-16')
        freezer.start()
        try:
            response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            token = Token.objects.get(key=response.data['token'])
            token.created = datetime.datetime.now() - datetime.timedelta(hours=1)
            token.save()

            company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
            company_attribute.trial_expiration_date = (datetime.datetime.now() - datetime.timedelta(days=1)).date()
            company_attribute.save()

            header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
            response = self.client.get(path='/app_staffing/organizations', format='json', **header)
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        finally:
            freezer.stop()

    def test_plan_postponement_expired(self):
        freezer = freeze_time('2021-12-16')
        freezer.start()
        try:
            response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            token = Token.objects.get(key=response.data['token'])
            token.created = datetime.datetime.now() - datetime.timedelta(hours=1)
            token.save()

            plan = Plan.objects.create(
                company_id=DEFAULT_COMPANY_ID,
                plan_master_id=1,
                is_active=True,
                payment_date=(datetime.datetime.now() - datetime.timedelta(days=10)).date()
            )
            PlanPaymentError.objects.create(
                company_id=DEFAULT_COMPANY_ID,
                plan=plan
            )

            header = {'HTTP_AUTHORIZATION': f'Token {token.key}'}
            response = self.client.get(path='/app_staffing/organizations', format='json', **header)
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        finally:
            freezer.stop()

    def test_over_length_username(self):
        freezer = freeze_time('2021-12-16')
        freezer.start()
        username = 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc'
        try:
            response = self.client.post(path='/api-token-auth/', data={'username': username, 'password': TEST_USER_PASSWORD}, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['username']), settings.LENGTH_VALIDATIONS['user']['email']['message'])
        finally:
            freezer.stop()

    def test_null_email(self):
        freezer = freeze_time('2021-12-16')
        freezer.start()
        try:
            response = self.client.post(path='/api-token-auth/', data={}, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['username']), '必須項目です。')
        finally:
            freezer.stop()

    def test_unmatch_username_password(self):
        freezer = freeze_time('2021-12-16')
        freezer.start()
        try:
            response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': 'UNMATCH_PASSWORD'}, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(str(response.data['non_field_errors']), "[ErrorDetail(string='提供された認証情報でログインできません。', code='authorization')]")
        finally:
            freezer.stop()

class AuthorizedActionViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_authorized_action(self):
        response = self.client.get(path='/app_staffing/authorized_action', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['authorized_action'], settings.ROLE_AUTHORIZED_ACTIONS['admin'])

    def test_update_role(self):
        response = self.client.patch(path='/app_staffing/users/1957ab0f-b47c-455a-a7b7-4453cfffd05e', data={'role': 'member'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/authorized_action', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['authorized_action'], settings.ROLE_AUTHORIZED_ACTIONS['member'])

class OrganizationViewCorporateNumberValidationTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_cannot_register(self):
        data = {"corporate_number": 123, "name": "An Example Company", "category": "client", "country": "JP", "score": 3, "domain_name": "example.com", "settlement_month": 1}
        response = self.client.post(path='/app_staffing/organizations', data=data, format='json')
        self.assertEqual(str(response.data["detail"]), CorporateNumberNotUniqueException().default_detail)

    def test_cannot_update(self):
        data = {"corporate_number": 123, "name": "An Example Company", "category": "client", "score": 3}
        response = self.client.patch(path='/app_staffing/organizations/5468c29f-7b35-4540-b9d0-c1807d945734', data=data, format='json')
        self.assertEqual(str(response.data["detail"]), CorporateNumberNotUniqueException().default_detail)

class OrganizationBranchViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_branches.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_delete(self):
        branches = OrganizationBranch.objects.filter(id='749303ca-7701-463e-83a8-8a5a35df1647')
        self.assertEqual(len(branches), 1)
        response = self.client.delete(path='/app_staffing/organizations/branches/749303ca-7701-463e-83a8-8a5a35df1647', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        branches = OrganizationBranch.objects.filter(id='749303ca-7701-463e-83a8-8a5a35df1647')
        self.assertEqual(len(branches), 0)

class ContactListViewIgnoreFilterTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/searchIgnoreFilter/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/searchIgnoreFilter/organizations.json',
        'app_staffing/tests/fixtures/searchIgnoreFilter/contacts.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_ignore_filter(self):
        response = self.client.get(path='/app_staffing/contacts?ignore_blocklist_filter=use_blocklist_filter&ignore_filter=use_filter')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        response2 = self.client.get(path='/app_staffing/contacts?ignore_blocklist_filter=ignore_blocklist_filter&ignore_filter=use_filter')
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response2.data['results']), 3)

        response3 = self.client.get(path='/app_staffing/contacts?ignore_blocklist_filter=ignore_blocklist_filter&ignore_filter=ignore_filter')
        self.assertEqual(response3.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response3.data['results']), 7)

        response4 = self.client.get(path='/app_staffing/contacts?ignore_blocklist_filter=use_blocklist_filter&ignore_filter=ignore_filter')
        self.assertEqual(response4.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response4.data['results']), 6)

class OrganizationListViewIgnoreFilterTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/searchIgnoreFilter/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/searchIgnoreFilter/organizations.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_ignore_filter(self):
        response = self.client.get(path='/app_staffing/organizations?ignore_blocklist_filter=use_blocklist_filter&ignore_filter=use_filter')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        response2 = self.client.get(path='/app_staffing/organizations?ignore_blocklist_filter=ignore_blocklist_filter&ignore_filter=use_filter')
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response2.data['results']), 3)

        response3 = self.client.get(path='/app_staffing/organizations?ignore_blocklist_filter=ignore_blocklist_filter&ignore_filter=ignore_filter')
        self.assertEqual(response3.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response3.data['results']), 7)

        response4 = self.client.get(path='/app_staffing/organizations?ignore_blocklist_filter=use_blocklist_filter&ignore_filter=ignore_filter')
        self.assertEqual(response4.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response4.data['results']), 6)

class LogoutViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/company_attribute.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def test_logout_no_token(self):
        response = self.client.get(path='/api-token-auth/logout', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_logout_user(self):
        response = self.client.post(path='/api-token-auth/', data={'username': TEST_USER_NAME, 'password': TEST_USER_PASSWORD}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token = response.data['token']

        header = {'HTTP_AUTHORIZATION': f'Token {token}'}
        response = self.client.get(path='/api-token-auth/logout', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_logout_user_with_invalid_token(self):
        token = 'invalid_token'
        header = {'HTTP_AUTHORIZATION': f'Token {token}'}
        response = self.client.get(path='/api-token-auth/logout', format='json', **header)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class ContactListViewOrderingTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/order/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_order_by_capital_man_yen_asc(self):
        data = {
            "page": 1,
            "page_size": 10,
            "ordering": "capital_man_yen",
        }
        response = self.client.get(path="/app_staffing/organizations", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['capital_man_yen'], 500)
        self.assertEqual(response.data['results'][1]['capital_man_yen'], 1000)
        self.assertEqual(response.data['results'][2]['capital_man_yen'], 1500)
        self.assertEqual(response.data['results'][3]['capital_man_yen'], 2000)

    def test_order_by_capital_man_yen_desc(self):
        data = {
            "page": 1,
            "page_size": 10,
            "ordering": "-capital_man_yen",
        }
        response = self.client.get(path="/app_staffing/organizations", data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][3]['capital_man_yen'], 500)
        self.assertEqual(response.data['results'][2]['capital_man_yen'], 1000)
        self.assertEqual(response.data['results'][1]['capital_man_yen'], 1500)
        self.assertEqual(response.data['results'][0]['capital_man_yen'], 2000)

class OrganizationCsvDownloadWithOrderViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        Organization.objects.create(name='test1', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)
        Organization.objects.create(name='test2', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)
        Organization.objects.create(name='test3', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)
        Organization.objects.create(name='test4', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=2)
        Organization.objects.create(name='test5', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=2)
        Organization.objects.create(name='test6', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=2)

        response_csv = self.client.get(path='/app_staffing/csv/organizations?ordering=organization_category__order')
        self.assertEqual(response_csv.status_code, status.HTTP_200_OK)
        rows = list(response_csv.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 6)

        response_list = self.client.get(path='/app_staffing/organizations?ordering=organization_category__order')

        self.assertEqual(len(entries), len(response_list.data['results']))
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][0]['name'])
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][1]['name'])
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][2]['name'])
        self.assertEqual(entries[3].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][3]['name'])
        self.assertEqual(entries[4].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][4]['name'])
        self.assertEqual(entries[5].decode('utf-8').replace('\r\n', '').split(',')[1], response_list.data['results'][5]['name'])


class ContactCsvDownloadWithOrderViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        organization = Organization.objects.create(name='test1', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)

        Contact.objects.create(last_name='test1', email='test1@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-01-01')
        Contact.objects.create(last_name='test2', email='test2@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-01-01')
        Contact.objects.create(last_name='test3', email='test3@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-01-01')
        Contact.objects.create(last_name='test4', email='test4@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-02-01')
        Contact.objects.create(last_name='test5', email='test5@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-02-01')
        Contact.objects.create(last_name='test6', email='test6@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization, last_visit='2021-02-01')

        response_csv = self.client.get(path='/app_staffing/csv/contacts?ordering=last_visit')
        self.assertEqual(response_csv.status_code, status.HTTP_200_OK)
        rows = list(response_csv.streaming_content)
        entries = rows[1:]
        self.assertEqual(len(entries), 6)

        response_list = self.client.get(path='/app_staffing/contacts?ordering=last_visit')

        self.assertEqual(len(entries), len(response_list.data['results']))
        self.assertEqual(entries[0].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][0]['email'])
        self.assertEqual(entries[1].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][1]['email'])
        self.assertEqual(entries[2].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][2]['email'])
        self.assertEqual(entries[3].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][3]['email'])
        self.assertEqual(entries[4].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][4]['email'])
        self.assertEqual(entries[5].decode('utf-8').replace('\r\n', '').split(',')[3], response_list.data['results'][5]['email'])

class OrganizationCsvUploadViewValidationTestCase(APITestCase, OrganizationCsvUploadView):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    maxDiff = None

    def test_valid_corporate_number(self):
        testCases = [
            {
                'value': '1234567890123',
                'required_items': {},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [],
                'expectedErr': [],
            },
            {
                'value': '1234567890123',
                'required_items': {},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [1234567890123],
                'expectedErr': ['2行目: 1234567890123は法人番号として登録できません。既に登録されています。'],
            },
            {
                'value': '1234567890123',
                'required_items': {},
                'corporate_numbers_in_csv': ['1234567890123'],
                'already_registered_numbers': [],
                'expectedErr': ['2行目: 1234567890123は法人番号として登録できません。値がファイル内で重複しています。'],
            },
            {
                'value': '',
                'required_items': {},
                'corporate_numbers_in_csv': [''],
                'already_registered_numbers': [],
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'corporate_number'},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [],
                'expectedErr': ['2行目: 法人番号は必須項目です。'],
            },
            {
                'value': 'A234567890123',
                'required_items': {},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [],
                'expectedErr': ['2行目: A234567890123は法人番号として登録できません。13桁の数字で入力してください。'],
            },
            {
                'value': '12345678901234',
                'required_items': {},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [],
                'expectedErr': ['2行目: 12345678901234は法人番号として登録できません。13桁の数字で入力してください。'],
            },
            {
                'value': '123456789012',
                'required_items': {},
                'corporate_numbers_in_csv': [],
                'already_registered_numbers': [],
                'expectedErr': ['2行目: 123456789012は法人番号として登録できません。13桁の数字で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_corporate_number(1, testCase['value'], errorMessages, testCase['required_items'], testCase['corporate_numbers_in_csv'], testCase['already_registered_numbers'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_name(self):
        testCases = [
            {
                'value': '取引先A',
                'expectedErr': [],
            },
            {
                'value': '取引先A ',
                'expectedErr': ['2行目: 取引先A は取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': ' 取引先A',
                'expectedErr': ['2行目:  取引先Aは取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': '取引先 A',
                'expectedErr': ['2行目: 取引先 Aは取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': '取引先A　',
                'expectedErr': ['2行目: 取引先A　は取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': '　取引先A',
                'expectedErr': ['2行目: 　取引先Aは取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': '取引先　A',
                'expectedErr': ['2行目: 取引先　Aは取引先名として登録できません。スペースの入力はできません。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは取引先名として登録できません。100文字以内で入力してください。'],
            },
            {
                'value': '',
                'expectedErr': ['2行目: 取引先名は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_name(1, "取引先名", testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['organization']['name']['max'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_category(self):
        testCases = [
            {
                'value': '見込み客',
                'expectedErr': [],
            },
            {
                'value': 'アプローチ済',
                'expectedErr': [],
            },
            {
                'value': '情報交換済',
                'expectedErr': [],
            },
            {
                'value': '契約実績有',
                'expectedErr': [],
            },
            {
                'value': '契約実績有り',
                'expectedErr': ['2行目: 契約実績有りは取引先ステータスとして登録できません。「見込み客」「アプローチ済」「情報交換済」「契約実績有」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'expectedErr': ['2行目: 取引先ステータスは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_category(1, testCase['value'], errorMessages)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_score(self):
        testCases = [
            {
                'value': '1',
                'expectedErr': [],
            },
            {
                'value': '2',
                'expectedErr': [],
            },
            {
                'value': '3',
                'expectedErr': [],
            },
            {
                'value': '4',
                'expectedErr': [],
            },
            {
                'value': '5',
                'expectedErr': [],
            },
            {
                'value': '0',
                'expectedErr': ['2行目: 0は取引先評価として登録できません。「1」から「5」 (半角数字)のいずれかを入力してください。'],
            },
            {
                'value': '6',
                'expectedErr': ['2行目: 6は取引先評価として登録できません。「1」から「5」 (半角数字)のいずれかを入力してください。'],
            },
            {
                'value': '',
                'expectedErr': ['2行目: 取引先評価は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_score(1, testCase['value'], errorMessages)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_country(self):
        testCases = [
            {
                'value': 'JP',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'KR',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'CN',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'OTHER',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'US',
                'required_items': {},
                'expectedErr': ['2行目: USは国籍として登録できません。「JP」「KR」「CN」「OTHER」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'country'},
                'expectedErr': ['2行目: 国籍は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_country(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_establishment_date(self):
        testCases = [
            {
                'value': '2020-01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2020-01-01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '20-01-01',
                'required_items': {},
                'expectedErr': ['2行目: 20-01-01は設立年月として登録できません。yyyy-mmの形式で入力してください。'],
            },
            {
                'value': '20-01',
                'required_items': {},
                'expectedErr': ['2行目: 20-01は設立年月として登録できません。yyyy-mmの形式で入力してください。'],
            },
            {
                'value': '2022/01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2022/01/01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '２０２０-０１',
                'required_items': {},
                'expectedErr': ['2行目: ２０２０-０１は設立年月として登録できません。yyyy-mmの形式で入力してください。'],
            },
            {
                'value': '2220-01',
                'required_items': {},
                'expectedErr': ['2行目: 2220-01は設立年月として登録できません。未来日付の登録は不可です。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'establishment_date'},
                'expectedErr': ['2行目: 設立年月は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_establishment_date(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_settlement_month(self):
        testCases = [
            {
                'value': '1',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '3',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '4',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '5',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '6',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '7',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '8',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '9',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '10',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '11',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '12',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '13',
                'required_items': {},
                'expectedErr': ['2行目: 13は決算期として登録できません。「1」から「12」 (半角数字)のいずれかを入力してください。'],
            },
            {
                'value': '１',
                'required_items': {},
                'expectedErr': ['2行目: １は決算期として登録できません。「1」から「12」 (半角数字)のいずれかを入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'settlement_month'},
                'expectedErr': ['2行目: 決算期は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_settlement_month(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_adress_building(self):
        testCases = [
            {
                'address_value': 'A市',
                'building_value': 'Bビル',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'address_value': '',
                'building_value': 'Bビル',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'address_value': 'A市',
                'building_value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'address_value': ' A市',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目:  A市は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': 'A 市',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目: A 市は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': 'A市 ',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目: A市 は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '　A市',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目: 　A市は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': 'A　市',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目: A　市は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': 'A市　',
                'building_value': '',
                'required_items': {},
                'expectedErr': ['2行目: A市　は住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': ' Bビル',
                'required_items': {},
                'expectedErr': ['2行目:  Bビルは住所(建物)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': 'B ビル',
                'required_items': {},
                'expectedErr': ['2行目: B ビルは住所(建物)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': 'Bビル ',
                'required_items': {},
                'expectedErr': ['2行目: Bビル は住所(建物)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': '　Bビル',
                'required_items': {},
                'expectedErr': ['2行目: 　Bビルは住所(建物)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': 'B　ビル',
                'required_items': {},
                'expectedErr': ['2行目: B　ビルは住所(建物)として登録できません。スペースの入力はできません。'],
            },
            {
                'address_value': '',
                'building_value': 'Bビル　',
                'required_items': {},
                'expectedErr': ['2行目: Bビル　は住所(建物)として登録できません。スペースの入力はできません。'],
            },            {
                'address_value': '',
                'building_value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'address_value': '',
                'building_value': 'Bビル',
                'required_items': {'address'},
                'expectedErr': ['2行目: 住所(市区町村・町名・番地)は必須項目です。'],
            },
            {
                'address_value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'building_value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {'address'},
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは住所として登録できません。建物名を合わせて100文字以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_adress_building(1, testCase['address_value'], testCase['building_value'], errorMessages, testCase['required_items'], settings.LENGTH_VALIDATIONS['organization']['building']['max'], False)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_tel(self):
        testCases = [
            {
                'value': '1-2-3',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '1-23',
                'required_items': {},
                'expectedErr': ['2行目: 1-23はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '12-3',
                'required_items': {},
                'expectedErr': ['2行目: 12-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '123',
                'required_items': {},
                'expectedErr': ['2行目: 123はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '-2-',
                'required_items': {},
                'expectedErr': ['2行目: -2-はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '1--2',
                'required_items': {},
                'expectedErr': ['2行目: 1--2はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': 'A-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: A-2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: A-2-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-B-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-B-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-B-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-C',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-CはTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-CはTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1B2',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1B2はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1B2はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '１-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: １-2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: １-2-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-２-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-２-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-２-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-３',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-３はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-３はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1２3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1２3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1２3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1ー2ー3',
                'required_items': {},
                'expectedErr': ['2行目: 1ー2ー3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '11111-11111-111111',
                'required_items': {},
                'expectedErr': ['2行目: 11111-11111-111111はTELとして登録できません。半角ハイフンを含めて17文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'tel'},
                'expectedErr': ['2行目: TELは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_tel(1, testCase['value'], errorMessages, testCase['required_items'], settings.LENGTH_VALIDATIONS['organization']['tel']['max'], False)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_fax(self):
        testCases = [
            {
                'value': '1-2-3',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '1-23',
                'required_items': {},
                'expectedErr': ['2行目: 1-23はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '12-3',
                'required_items': {},
                'expectedErr': ['2行目: 12-3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '123',
                'required_items': {},
                'expectedErr': ['2行目: 123はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '-2-',
                'required_items': {},
                'expectedErr': ['2行目: -2-はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '1--2',
                'required_items': {},
                'expectedErr': ['2行目: 1--2はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': 'A-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: A-2-3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: A-2-3はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-B-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-B-3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-B-3はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-C',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-CはFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-CはFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1B2',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1B2はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1B2はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '１-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: １-2-3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: １-2-3はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-２-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-２-3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-２-3はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-３',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-３はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-３はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1２3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1２3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1２3はFAXとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1ー2ー3',
                'required_items': {},
                'expectedErr': ['2行目: 1ー2ー3はFAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '11111-11111-111111',
                'required_items': {},
                'expectedErr': ['2行目: 11111-11111-111111はFAXとして登録できません。半角ハイフンを含めて17文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'fax'},
                'expectedErr': ['2行目: FAXは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_fax(1, testCase['value'], errorMessages, testCase['required_items'], settings.LENGTH_VALIDATIONS['organization']['fax']['max'], False)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_domain_name(self):
        testCases = [
            {
                'value': 'http://sample123.com',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'http://sample１23.com',
                'required_items': {},
                'expectedErr': ['2行目: http://sample１23.comはURLとして登録できません。半角英数字記号で入力してください。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {},
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcはURLとして登録できません。50文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'domain_name'},
                'expectedErr': ['2行目: URLは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_domain_name(1, testCase['value'], errorMessages, testCase['required_items'], settings.LENGTH_VALIDATIONS['organization']['domain_name']['max'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_employee_number(self):
        testCases = [
            {
                'value': '~10名',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '11~30名',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '31~50名',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '51~100名',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '101~300名',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '301名~',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '401名~',
                'required_items': {},
                'expectedErr': ['2行目: 401名~は社員数として登録できません。「~10名」「11~30名」「31~50名」「51~100名」「101~300名」「301名~」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'employee_number'},
                'expectedErr': ['2行目: 社員数は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_employee_number(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_boolean_item(self):
        testCases = [
            {
                'value': '1',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '0',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2',
                'required_items': {},
                'expectedErr': ['2行目: 2は商流として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。'],
            },
            {
                'value': '０',
                'required_items': {},
                'expectedErr': ['2行目: ０は商流として登録できません。「0」と「1」(半角数字)または空欄のみ有効です。'],
            },
            {
                'value': '',
                'required_items': {'has_distribution'},
                'expectedErr': ['2行目: 商流は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_boolean_item(1, '商流', testCase['value'], errorMessages, testCase['required_items'], 'has_distribution', None)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_capital_man_yen(self):
        testCases = [
            {
                'value': '100',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '1',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '0',
                'required_items': {},
                'expectedErr': ['2行目: 0は資本金として登録できません。資本金には1以上の数値を入力してください。'],
            },
            {
                'value': '１',
                'required_items': {},
                'expectedErr': ['2行目: １は資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '1.2',
                'required_items': {},
                'expectedErr': ['2行目: 1.2は資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'A',
                'required_items': {},
                'expectedErr': ['2行目: Aは資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'あ',
                'required_items': {},
                'expectedErr': ['2行目: あは資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'capital_man_yen'},
                'expectedErr': ['2行目: 資本金は必須項目です。'],
            },
            {
                'value': '1111111111',
                'required_items': {},
                'expectedErr': ['2行目: 1111111111は資本金として登録できません。9桁以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_capital_man_yen(1, testCase['value'], errorMessages, testCase['required_items'], settings.LENGTH_VALIDATIONS['organization']['capital_man_yen']['max'], settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen']['min'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_establishment_year(self):
        testCases = [
            {
                'value': '100',
                'expectedErr': [],
            },
            {
                'value': '1',
                'expectedErr': [],
            },
            {
                'value': '0',
                'expectedErr': ['2行目: 0は取引に必要な設立年数として登録できません。取引に必要な設立年数には1以上の数値を入力してください。'],
            },
            {
                'value': '１',
                'expectedErr': ['2行目: １は取引に必要な設立年数として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '1.2',
                'expectedErr': ['2行目: 1.2は取引に必要な設立年数として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'A',
                'expectedErr': ['2行目: Aは取引に必要な設立年数として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'あ',
                'expectedErr': ['2行目: あは取引に必要な設立年数として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '',
                'expectedErr': [],
            },
            {
                'value': '1111',
                'required_items': {},
                'expectedErr': ['2行目: 1111は取引に必要な設立年数として登録できません。3桁以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_establishment_year(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['organization']['establishment_year']['max'], settings.VALUE_RANGE_VALIDATIONS['organization']['establishment_year']['min'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_capital_man_yen_required_for_transactions(self):
        testCases = [
            {
                'value': '100',
                'expectedErr': [],
            },
            {
                'value': '1',
                'expectedErr': [],
            },
            {
                'value': '0',
                'expectedErr': ['2行目: 0は取引に必要な資本金として登録できません。取引に必要な資本金には1以上の数値を入力してください。'],
            },
            {
                'value': '１',
                'expectedErr': ['2行目: １は取引に必要な資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '1.2',
                'expectedErr': ['2行目: 1.2は取引に必要な資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'A',
                'expectedErr': ['2行目: Aは取引に必要な資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': 'あ',
                'expectedErr': ['2行目: あは取引に必要な資本金として登録できません。半角の整数で入力してください。'],
            },
            {
                'value': '',
                'expectedErr': [],
            },
            {
                'value': '1111111111',
                'expectedErr': ['2行目: 1111111111は取引に必要な資本金として登録できません。9桁以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            actual = self._valid_capital_man_yen_required_for_transactions(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['max'], settings.VALUE_RANGE_VALIDATIONS['organization']['capital_man_yen_required_for_transactions']['min'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_valid_branchs(self):
        testCases = [
            {
                'name_values': 'TestNameA',
                'addresses_values': 'A市',
                'buildings_values': 'Aビル',
                'tel_values': '1-2-3',
                'fax_values': '1-2-3',
                'expectedErr': [],
            },
            {
                'name_values': 'TestNameA,TestNameB',
                'addresses_values': 'A市,B市',
                'buildings_values': 'Aビル,Bビル',
                'tel_values': '1-2-3,4-5-6',
                'fax_values': '1-2-3,4-5-6',
                'expectedErr': [],
            },
            {
                'name_values': 'TestNameA,TestNameB',
                'addresses_values': '',
                'buildings_values': '',
                'tel_values': '',
                'fax_values': '',
                'expectedErr': [],
            },
            {
                'name_values': 'TestNameA,TestNameB',
                'addresses_values': 'A市',
                'buildings_values': 'Aビル',
                'tel_values': '1-2-3',
                'fax_values': '1-2-3',
                'expectedErr': [
                    '2行目: A市は取引先支店住所(市区町村・町名・番地)として登録できません。取引先支店名と取引先支店住所(市区町村・町名・番地)の値の数が一致しません。',
                    '2行目: Aビルは取引先支店住所(建物)として登録できません。取引先支店名と取引先支店住所(建物)の値の数が一致しません。',
                    '2行目: 1-2-3は取引先支店TELとして登録できません。取引先支店名と取引先支店TELの値の数が一致しません。',
                    '2行目: 1-2-3は取引先支店FAXとして登録できません。取引先支店名と取引先支店FAXの値の数が一致しません。',
                ],
            },
            {
                'name_values': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'addresses_values': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'buildings_values': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'tel_values': '11111-11111-111111',
                'fax_values': '11111-11111-111111',
                'expectedErr': [
                    '2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは取引先支店名として登録できません。100文字以内で入力してください。',
                    '2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは取引先支店住所として登録できません。建物名を合わせて100文字以内で入力してください。',
                    '2行目: 11111-11111-111111は取引先支店TELとして登録できません。半角ハイフンを含めて17文字以内で入力してください。',
                    '2行目: 11111-11111-111111は取引先支店FAXとして登録できません。半角ハイフンを含めて17文字以内で入力してください。',
                ],
            },
            {
                'name_values': 'TestNameA',
                'addresses_values': 'A 市',
                'buildings_values': 'A ビル',
                'tel_values': '1-2',
                'fax_values': '1-2',
                'expectedErr': [
                    '2行目: A 市は取引先支店住所(市区町村・町名・番地)として登録できません。スペースの入力はできません。',
                    '2行目: A ビルは取引先支店住所(建物)として登録できません。スペースの入力はできません。',
                    '2行目: 1-2は取引先支店TELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2は取引先支店FAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'
                ],
            },
            {
                'name_values': 'TestNameA',
                'addresses_values': '',
                'buildings_values': '',
                'tel_values': 'A-2-3',
                'fax_values': 'A-2-3',
                'expectedErr': [
                    '2行目: A-2-3は取引先支店TELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: A-2-3は取引先支店TELとして登録できません。半角数字で入力してください。',
                    '2行目: A-2-3は取引先支店FAXとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: A-2-3は取引先支店FAXとして登録できません。半角数字で入力してください。',
                ],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            branches_hash = {}
            actual = self._valid_branchs(
                1,
                testCase['name_values'],
                testCase['addresses_values'],
                testCase['buildings_values'],
                testCase['tel_values'],
                testCase['fax_values'],
                errorMessages,
                settings.LENGTH_VALIDATIONS['organization']['branches']['columns'],
                branches_hash,
                'dummy-org-hash',
            )
            self.assertEqual(errorMessages, testCase['expectedErr'])

class ContactCsvUploadViewValidationTestCase(APITestCase, ContactCsvUploadView):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]

    maxDiff = None

    def test_get_error_name(self):
        testCases = [
            {
                'last_name_value': '姓A',
                'first_name_value': '名B',
                'expectedErr': [],
            },
            {
                'last_name_value': '姓A ',
                'first_name_value': '名B',
                'expectedErr': ['2行目: 姓A は取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': ' 姓A',
                'first_name_value': '名B',
                'expectedErr': ['2行目:  姓Aは取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓 A',
                'first_name_value': '名B',
                'expectedErr': ['2行目: 姓 Aは取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '名B ',
                'expectedErr': ['2行目: 名B は取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': ' 名B',
                'expectedErr': ['2行目:  名Bは取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '名 B',
                'expectedErr': ['2行目: 名 Bは取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A　',
                'first_name_value': '名B',
                'expectedErr': ['2行目: 姓A　は取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '　姓A',
                'first_name_value': '名B',
                'expectedErr': ['2行目: 　姓Aは取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓　A',
                'first_name_value': '名B',
                'expectedErr': ['2行目: 姓　Aは取引先担当者名(姓)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '名B　',
                'expectedErr': ['2行目: 名B　は取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '　名B',
                'expectedErr': ['2行目: 　名Bは取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '名　B',
                'expectedErr': ['2行目: 名　Bは取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '姓A',
                'first_name_value': '名　B',
                'expectedErr': ['2行目: 名　Bは取引先担当者名(名)として登録できません。スペースの入力はできません。'],
            },
            {
                'last_name_value': '',
                'first_name_value': '',
                'expectedErr': ['2行目: 取引先担当者名(姓)は必須項目です。'],
            },
            {
                'last_name_value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'first_name_value': '',
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは取引先担当者名として登録できません。姓名合わせて50文字以内で入力してください。'],
            },
            {
                'last_name_value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb',
                'first_name_value': 'c',
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは取引先担当者名として登録できません。姓名合わせて50文字以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_name(1, testCase['last_name_value'], testCase['first_name_value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['name']['max'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_organization(self):
        testCases = [
            {
                'value': '取引先A',
                'org': 'Not None',
                'expectedErr': [],
            },
            {
                'value': '取引先A',
                'org': None,
                'expectedErr': ['2行目: 取引先Aは未登録の取引先です。先に取引先の登録を行ってください。'],
            },
            {
                'value': '',
                'org': None,
                'expectedErr': ['2行目: 所属取引先は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_organization(1, testCase['value'], testCase['org'], errorMessages)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_mail_to(self):
        testCases = [
            {
                'value': 'test-mail@sample.com',
                'expectedErr': [],
            },
            {
                'value': 'test-mail.sample.com',
                'expectedErr': ['2行目: test-mail.sample.comはメールアドレス > TOとして登録できません。有効なメールアドレスを入力してください。'],
            },
            {
                'value': 'test-mail@sample',
                'expectedErr': ['2行目: test-mail@sampleはメールアドレス > TOとして登録できません。有効なメールアドレスを入力してください。'],
            },
            {
                'value': '',
                'expectedErr': ['2行目: メールアドレス > TOは必須項目です。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com',
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.comはメールアドレス > TOとして登録できません。100文字以内で入力してください。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_mail_to(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['email']['max'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_mail_cc(self):
        testCases = [
            {
                'value': 'test-mail@sample.com',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'test1-mail@sample.com,test2-mail@sample.com',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'test-mail.sample.com',
                'required_items': {},
                'expectedErr': ['2行目: test-mail.sample.comはメールアドレス > CCとして登録できません。有効なメールアドレスを入力してください。'],
            },
            {
                'value': 'test-mail@sample',
                'required_items': {},
                'expectedErr': ['2行目: test-mail@sampleはメールアドレス > CCとして登録できません。有効なメールアドレスを入力してください。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com',
                'required_items': {},
                'expectedErr': ['2行目: メールアドレス > CCはカンマを含めて500文字以内で入力してください。'],
            },
            {
                'value': 'test1@sample.com,test2@sample.com,test3@sample.com,test4@sample.com,test5@sample.com',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'test1@sample.com,test2@sample.com,test3@sample.com,test4@sample.com,test5@sample.com,test6@sample.com',
                'required_items': {},
                'expectedErr': ['2行目: メールアドレス > CCの登録可能数上限5件を超えています。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'email_cc'},
                'expectedErr': ['2行目: メールアドレス > CCは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_mail_cc(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['cc_mails']['max'], testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_tel(self):
        testCases = [
            {
                'value': '1-2-3',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '1-23',
                'required_items': {},
                'expectedErr': ['2行目: 1-23はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '12-3',
                'required_items': {},
                'expectedErr': ['2行目: 12-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '123',
                'required_items': {},
                'expectedErr': ['2行目: 123はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '-2-',
                'required_items': {},
                'expectedErr': ['2行目: -2-はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '1--2',
                'required_items': {},
                'expectedErr': ['2行目: 1--2はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': 'A-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: A-2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: A-2-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-B-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-B-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-B-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-C',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-CはTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-CはTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1B2',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1B2はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1B2はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '１-2-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: １-2-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: １-2-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-２-3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-２-3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-２-3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1-2-３',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1-2-３はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1-2-３はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1２3',
                'required_items': {},
                'expectedErr': [
                    '2行目: 1２3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。',
                    '2行目: 1２3はTELとして登録できません。半角数字で入力してください。'
                ],
            },
            {
                'value': '1ー2ー3',
                'required_items': {},
                'expectedErr': ['2行目: 1ー2ー3はTELとして登録できません。半角ハイフンを含め適切なフォーマットで入力してください。'],
            },
            {
                'value': '11111-11111-111111',
                'required_items': {},
                'expectedErr': ['2行目: 11111-11111-111111はTELとして登録できません。半角ハイフンを含めて17文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'tel'},
                'expectedErr': ['2行目: TELは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_tel(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['tel']['max'], testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_position(self):
        testCases = [
            {
                'value': 'positionA',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {},
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは役職として登録できません。50文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'position'},
                'expectedErr': ['2行目: 役職は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_position(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['position']['max'], testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_department(self):
        testCases = [
            {
                'value': 'departmentA',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {},
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcは部署として登録できません。50文字以内で入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'department'},
                'expectedErr': ['2行目: 部署は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_department(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['contact']['department']['max'], testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_staff(self):
        testCases = [
            {
                'value': 'sample@sample.com',
                'staff': 'Not None',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'sample@sample.com',
                'staff': None,
                'required_items': {},
                'expectedErr': ['2行目: sample@sample.comに紐づくユーザーが見当たりません。再度ユーザーのメールアドレスをご確認ください。'],
            },
            {
                'value': '',
                'staff': None,
                'required_items': {'staff'},
                'expectedErr': ['2行目: 自社担当(メールアドレス)は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_staff(1, testCase['value'], testCase['staff'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_last_visit(self):
        testCases = [
            {
                'value': '2020-01-01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2020-01',
                'required_items': {},
                'expectedErr': ['2行目: 2020-01は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '20-01-01',
                'required_items': {},
                'expectedErr': ['2行目: 20-01-01は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '２０２０-０１-０１',
                'required_items': {},
                'expectedErr': ['2行目: ２０２０-０１-０１は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '2020/01/01',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '2020/01',
                'required_items': {},
                'expectedErr': ['2行目: 2020/01は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '20/01/01',
                'required_items': {},
                'expectedErr': ['2行目: 20/01/01は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '２０２０/０１/０１',
                'required_items': {},
                'expectedErr': ['2行目: ２０２０/０１/０１は最終商談日として登録できません。yyyy-mm-ddの形式で入力してください。'],
            },
            {
                'value': '2120-01-01',
                'required_items': {},
                'expectedErr': ['2行目: 2120-01-01は最終商談日として登録できません。未来日付の登録は不可です。'],
            },
            {
                'value': '2120/01/01',
                'required_items': {},
                'expectedErr': ['2行目: 2120/01/01は最終商談日として登録できません。未来日付の登録は不可です。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'last_visit'},
                'expectedErr': ['2行目: 最終商談日は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_last_visit(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_tags(self):
        testCases = [
            {
                'value': 'TAG1',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'TAG1,TAG2,TAG3,TAG4,TAG5,TAG6,TAG7,TAG8,TAG9,TAG10',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': 'TAG1,TAG2,TAG3,TAG4,TAG5,TAG6,TAG7,TAG8,TAG9,TAG10,TAG11',
                'required_items': {},
                'expectedErr': ['2行目: タグの登録可能数上限10件を超えています。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {},
                'expectedErr': ['2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcはタグとして登録できません。タグ名はそれぞれ50文字以内で入力してください。'],
            },
            {
                'value': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc,aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbc',
                'required_items': {},
                'expectedErr': [
                    '2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcはタグとして登録できません。タグ名はそれぞれ50文字以内で入力してください。',
                    '2行目: aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbcはタグとして登録できません。タグ名はそれぞれ50文字以内で入力してください。',
                ],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'tag'},
                'expectedErr': ['2行目: タグは必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_tags(1, testCase['value'], errorMessages, settings.LENGTH_VALIDATIONS['tag']['value']['max'], testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_category(self):
        testCases = [
            {
                'value': '良い',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '悪い',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '普通',
                'required_items': {},
                'expectedErr': ['2行目: 普通は相性として登録できません。「良い」「悪い」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'required_items': {},
                'expectedErr': [],
            },
            {
                'value': '',
                'required_items': {'category'},
                'expectedErr': ['2行目: 相性は必須項目です。'],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_category(1, testCase['value'], errorMessages, testCase['required_items'])
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_job_syouryu(self):
        testCases = [
            {
                'value': 'なし',
                'expectedErr': [],
            },
            {
                'value': 'エンド直・元請直まで',
                'expectedErr': [],
            },
            {
                'value': '1次請まで',
                'expectedErr': [],
            },
            {
                'value': '2次請まで',
                'expectedErr': [],
            },
            {
                'value': '3次請まで',
                'expectedErr': ['2行目: 3次請までは案件配信 > 商流制限として登録できません。「なし」「エンド直・元請直まで」「1次請まで」「2次請まで」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'expectedErr': [],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_job_syouryu(1, testCase['value'], errorMessages)
            self.assertEqual(errorMessages, testCase['expectedErr'])

    def test_get_error_personnel_syouryu(self):
        testCases = [
            {
                'value': 'なし',
                'expectedErr': [],
            },
            {
                'value': '自社所属まで',
                'expectedErr': [],
            },
            {
                'value': '1社先所属まで',
                'expectedErr': [],
            },
            {
                'value': '2社先所属まで',
                'expectedErr': [],
            },
            {
                'value': '3社先所属まで',
                'expectedErr': ['2行目: 3社先所属までは要員配信 > 商流制限として登録できません。「なし」「自社所属まで」「1社先所属まで」「2社先所属まで」のいずれかを入力してください。'],
            },
            {
                'value': '',
                'expectedErr': [],
            },
        ]

        for testCase in testCases:
            errorMessages = []
            self._get_error_personnel_syouryu(1, testCase['value'], errorMessages)
            self.assertEqual(errorMessages, testCase['expectedErr'])


class OrganizationSubCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_comments.json',
    ]

    related_resource_id = '749303ca-7701-463e-83a8-8a5a35df1646'
    comment_id = '20c0a96b-ef06-44e0-8e67-1ef670269f8f'
    other_users_comment_id = '20c0a96b-ef06-44e0-8e67-1ef670169f7f'
    updating_comment_id = "20c0a96b-ef06-44e0-8e67-1ef670369f8f"

    base_url = f'/app_staffing/organizations/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}

    def test_post_comment(self):
        new_content = "A new comment"

        response = self.client.post(path=f'{self.base_url}/{self.comment_id}/sub-comments', data={'content': new_content})
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.resource_validator_class(response.data).validate()
        self.assertEqual(response.data.get('content'), new_content)
        self.assertEqual(response.data.get('created_user__name'), TEST_USER_NAME)
        self.assertEqual(str(response.data.get('parent')), self.comment_id)

    def test_get_sub_comments(self):
        response = self.client.get(path=f'{self.base_url}/{self.comment_id}/sub-comments', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_updating_important_sub_comment_content(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-09T16:42:27.484000+09:00")

    def test_updating_important_sub_comment_content_and_is_important(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-09T16:42:27.484000+09:00")

    def test_delete_sub_comment(self):
        url = self.base_url + "/" + self.updating_comment_id
        self.assertEqual(OrganizationComment.objects.filter(id=self.updating_comment_id).count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(OrganizationComment.objects.filter(id=self.updating_comment_id).count(), 0)

    def test_sub_comment_of_another_user_can_be_updated(self):
        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670179f7f"
        post_data = {
            "content": "updated Another User's comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-04-10T16:42:27.484000+09:00")

    def test_comment_of_another_user_can_be_deleted(self):
        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670179f7f"
        post_data = {
            "content": "updated Another User's comment",
        }
        self.assertEqual(OrganizationComment.objects.filter(id="20c0a96b-ef06-44e0-8e67-1ef670179f7f").count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(OrganizationComment.objects.filter(id="20c0a96b-ef06-44e0-8e67-1ef670179f7f").count(), 0)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {
            "content": "test comment",
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670179f7f"
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/20c0a96b-ef06-44e0-8e67-1ef670179f7f"
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ContactSubCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',  # For foreign key relationships.
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_comments.json',
    ]

    related_resource_id = '78630567-72ce-49a3-84e6-8947b5b057f3'
    comment_id = '39037c2e-4f95-47c2-b222-1beddb8ef0c8'
    other_users_comment_id = 'dd466574-10ce-4fef-bb97-7d1d8d806706'
    updating_comment_id = "39037c2e-4f95-47c2-b222-1beddb4ef0c7"

    base_url = f'/app_staffing/contacts/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}


    def test_post_comment(self):
        new_content = "A new comment"

        response = self.client.post(path=f'{self.base_url}/{self.comment_id}/sub-comments', data={'content': new_content})
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.resource_validator_class(response.data).validate()
        self.assertEqual(response.data.get('content'), new_content)
        self.assertEqual(response.data.get('created_user__name'), TEST_USER_NAME)
        self.assertEqual(str(response.data.get('parent')), self.comment_id)

    def test_get_sub_comments(self):
        response = self.client.get(path=f'{self.base_url}/{self.comment_id}/sub-comments', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_updating_important_sub_comment_content(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_updating_important_sub_comment_content_and_is_important(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_delete_sub_comment(self):
        url = self.base_url + "/" + self.updating_comment_id
        self.assertEqual(ContactComment.objects.filter(id=self.updating_comment_id).count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ContactComment.objects.filter(id=self.updating_comment_id).count(), 0)

    def test_sub_comment_of_another_user_can_be_updated(self):
        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb7ef0c7"
        post_data = {
            "content": "updated Another User's comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_comment_of_another_user_can_be_deleted(self):
        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb7ef0c7"
        post_data = {
            "content": "updated Another User's comment",
        }
        self.assertEqual(ContactComment.objects.filter(id="39037c2e-4f95-47c2-b222-1beddb7ef0c7").count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ContactComment.objects.filter(id="39037c2e-4f95-47c2-b222-1beddb7ef0c7").count(), 0)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {
            "content": "test comment",
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb7ef0c7"
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb7ef0c7"
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
