import json
import os
from rest_framework.authtoken.models import Token

from django.conf import settings
from django.core.cache import cache
from django.core.signing import dumps
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.exceptions.base import (BulkOperationLimitException, InvalidateTelValueException,
                                          TelValueIsTooLongException, RequiredFieldsException)
from app_staffing.exceptions.user import (UserLoginCanNotInActiveException, UserRegisterActivateLimitException, MaxSizeValidatorException,
                                          UserLoginCanNotDeleteException,
                                          FileExtensionValidatorException, DeleteFileError,
                                          PermissionDeleteAvatarException,
                                          UserDoesNotExistException, DeleteAvatarDefault,
                                          UserUpdateActivateLimitException)
from app_staffing.exceptions.validation import (CannotActivateException, CannotDeactivateException,
                                                CannotDeleteException, CannotUpdateException,
                                                CannotDeleteUserException)
from app_staffing.models import (CompanyAttribute, ScheduledEmail, User, UserRole)
from app_staffing.models.organization import (Contact, ContactCategory,
                                              Organization, UserDisplaySetting)
from app_staffing.tests.factories.user import AppUserFactory, UserRoleFactory
from app_staffing.tests.settings import (ADMIN_USER_NAME, ADMIN_USER_PASSWORD,
                                         DEFAULT_COMPANY_ID, GET_DEFAULT_COMPANY,
                                         NORMAL_USER_NAME, NORMAL_USER_PASSWORD,
                                         TEST_USER_NAME, TEST_USER_PASSWORD)
from app_staffing.tests.views.base import (APICRUDTestMixin, MultiTenancyTestMixin)
from app_staffing.tests.views.validators.generals import IsPagenatedResponse
from app_staffing.tests.views.validators.users import IsUser, IsUserProfile
from app_staffing.utils.user import get_extras_file, get_uploaded_file
from app_staffing.utils.utils import generate_image_filename

extras_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'files')


class UserViewCRUDTestCase(APICRUDTestMixin, APITestCase):
    # TODO: this view have permission classes, so add 3 test conditions :
    #  access user is userAdmin / createdUser / otherUsers
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
    ]
    base_url = '/app_staffing/users'
    resource_id = '1957ab0f-b47c-455a-a7b7-4453cfffd05e'  # Correspond to Test user ID.
    user_id = '67449fa2-771d-4ffa-bab3-fa160fdf716f'
    id_suffix = '/{0}'
    list_response_validator_class = IsPagenatedResponse
    resource_validator_class = IsUser

    post_test_cases = [
        ('fully_qualified', {
            'last_name': 'NEW',
            'first_name': 'USER',
            'email': 'newuser@example.com',
            'email_signature': "*********\n A New User *********",
            'tel1': '0120',
            'tel2': '456',
            'tel3': '7890',
            'is_user_admin': False,
            'password': 'changeMe0123.'
        }),
        ('migration', {  # Remove after the migration from the old system has finished.
            'email': 'olduser@example.com',
            'email_signature': 'test',
            'password': 'changeMe0123.',
            'old_id': 12,
        })
    ]
    patch_test_cases = [
        ('minimum', {'is_user_admin': True}),
        ('email', {'email': 'newemail@example.com'}),
        ('migration', {  # Remove after the migration from the old system has finished.
            'old_id': 12,  # This patch will not happen, but checking whether this attr is available.
        })
    ]

    skip_tests = ('put',)  # Specify a tuple of SkipChoices.

    cache_key_base = f'{settings.CACHE_SEARCH_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_{user_id}_'
    cache_key_contact = cache_key_base + 'contact'
    cache_key_contact_mail_preference = cache_key_base + 'contact_mail_preference'
    cache_key_organization = cache_key_base + 'organization'
    cache_key_scheduled_email = cache_key_base + 'scheduled_email'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_update_password(self):
        new_password = 'passwordChanged0123.'
        response = self.client.patch(path='/app_staffing/users/1957ab0f-b47c-455a-a7b7-4453cfffd05e', data={'password': new_password})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check If the password has updated correctly.
        instance = User.objects.get(username=TEST_USER_NAME)
        self.assertTrue(instance.check_password(new_password))

    def test_bulk_update(self):
        instance = User.objects.get(pk=self.user_id)
        response = self.client.patch(path='/app_staffing/users', data={ 'source': [instance.id], 'column': 'is_active', 'value': False }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        instance.refresh_from_db()
        self.assertFalse(instance.is_active)

    def test_bulk_update_limit_error(self):
        data = {
            'source': [
                'sample_uuid_xxxx_yyyy_1',
                'sample_uuid_xxxx_yyyy_2',
                'sample_uuid_xxxx_yyyy_3',
                'sample_uuid_xxxx_yyyy_4',
                'sample_uuid_xxxx_yyyy_5',
                'sample_uuid_xxxx_yyyy_6',
                'sample_uuid_xxxx_yyyy_7',
                'sample_uuid_xxxx_yyyy_8',
                'sample_uuid_xxxx_yyyy_9',
                'sample_uuid_xxxx_yyyy_10',
                'sample_uuid_xxxx_yyyy_11',
            ],
            'column': 'is_active',
            'value': False
        }
        response = self.client.patch(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

    def test_bulk_delete_limit_error(self):
        data = { 'source': [
            'sample_uuid_xxxx_yyyy_1',
            'sample_uuid_xxxx_yyyy_2',
            'sample_uuid_xxxx_yyyy_3',
            'sample_uuid_xxxx_yyyy_4',
            'sample_uuid_xxxx_yyyy_5',
            'sample_uuid_xxxx_yyyy_6',
            'sample_uuid_xxxx_yyyy_7',
            'sample_uuid_xxxx_yyyy_8',
            'sample_uuid_xxxx_yyyy_9',
            'sample_uuid_xxxx_yyyy_10',
            'sample_uuid_xxxx_yyyy_11',
        ]}
        response = self.client.delete(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

    def test_invite_user_again_ok(self):
        response = self.client.post(path='/app_staffing/users', data={'email': 'apiuser@example.com','sendAgain': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invite_user_error(self):
        response = self.client.post(path='/app_staffing/users', data={'email': 'apiuser@example.com'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_over_max_length(self):
        data = {
            'first_name': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'last_name': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'password': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'tel1': 11111,
            'tel2': 11111,
            'tel3': 111111,
            'email': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com',
            'email_signature': """
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                ccc
            """,
        }
        response = self.client.post(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['user']['tel']['message'])
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['user']['email']['message'])
        self.assertEqual(str(response.data['email_signature']), settings.LENGTH_VALIDATIONS['user']['email_signature']['message'])

        instance = User.objects.get(username=TEST_USER_NAME)
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['user']['name']['message'])
        self.assertEqual(str(response.data['password']), settings.LENGTH_VALIDATIONS['user']['password']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['user']['tel']['message'])
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['user']['email']['message'])
        self.assertEqual(str(response.data['email_signature']), settings.LENGTH_VALIDATIONS['user']['email_signature']['message'])

    def test_deactivate(self):
        instance = User.objects.get(pk=self.user_id)
        UserDisplaySetting.objects.create(user=instance, content=json.dumps(settings.USER_DISPLAY_SETTING_DEFAULT))
        cache.set(self.cache_key_contact, 'test')
        cache.set(self.cache_key_contact_mail_preference, 'test')
        cache.set(self.cache_key_organization, 'test')
        cache.set(self.cache_key_scheduled_email, 'test')
        organization = Organization.objects.create(name='test1', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)
        contact = Contact.objects.create(last_name='test1', email='test1@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization)
        ContactCategory.objects.create(user=instance, contact=contact, category='heart', company_id=DEFAULT_COMPANY_ID)
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        instance.refresh_from_db()
        self.assertIn('(無効ユーザー)', instance.display_name)
        self.assertFalse(UserDisplaySetting.objects.filter(user=instance).exists())
        self.assertFalse(hasattr(instance, 'userdisplaysetting'))
        self.assertFalse(cache.get(self.cache_key_contact))
        self.assertFalse(cache.get(self.cache_key_contact_mail_preference))
        self.assertFalse(cache.get(self.cache_key_organization))
        self.assertFalse(cache.get(self.cache_key_scheduled_email))
        self.assertFalse(ContactCategory.objects.filter(user=instance).exists())

    def test_hide(self):
        instance = User.objects.get(pk=self.user_id)
        UserDisplaySetting.objects.create(user=instance, content=json.dumps(settings.USER_DISPLAY_SETTING_DEFAULT))
        cache.set(self.cache_key_contact, 'test')
        cache.set(self.cache_key_contact_mail_preference, 'test')
        cache.set(self.cache_key_organization, 'test')
        cache.set(self.cache_key_scheduled_email, 'test')
        organization = Organization.objects.create(name='test1', company_id=DEFAULT_COMPANY_ID, score=1, organization_category_id=1)
        contact = Contact.objects.create(last_name='test1', email='test1@example.com', company_id=DEFAULT_COMPANY_ID, organization=organization)
        ContactCategory.objects.create(user=instance, contact=contact, category='heart', company_id=DEFAULT_COMPANY_ID)

        response = self.client.delete(path=f'/app_staffing/users/{instance.id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        instance.refresh_from_db()
        self.assertIn('(削除済ユーザー)', instance.display_name)
        self.assertFalse(UserDisplaySetting.objects.filter(user=instance).exists())
        self.assertFalse(hasattr(instance, 'userdisplaysetting'))
        self.assertFalse(cache.get(self.cache_key_contact))
        self.assertFalse(cache.get(self.cache_key_contact_mail_preference))
        self.assertFalse(cache.get(self.cache_key_organization))
        self.assertFalse(cache.get(self.cache_key_scheduled_email))
        self.assertFalse(ContactCategory.objects.filter(user=instance).exists())

    def test_get_full_conditions(self):
        data = {
            'last_name': 'TEST',
            'first_name': 'USER ADMIN',
            'email': 'useradmin@example.com',
            'tel1': '090',
            'tel2': '1234',
            'tel3': '5678',
            'last_login_gte': '2017-01-01',
            'last_login_lte': '2019-01-01',
            'role': 'admin',
            'inactive_filter': 'use_filter',
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_single_condition(self):
        cases = [
            {
                'data' : {
                    'last_name': 'non-existent last name',
                },
                'count': 0,
            },
            {
                'data' : {
                    'first_name': 'non-existent first name',
                },
                'count': 0,
            },
            {
                'data' : {
                    'email': 'non-existent email',
                },
                'count': 0,
            },
            {
                'data' : {
                    'tel1': '999',
                },
                'count': 0,
            },
            {
                'data' : {
                    'tel2': '999',
                },
                'count': 0,
            },
            {
                'data' : {
                    'tel3': '999',
                },
                'count': 0,
            },
            {
                'data' : {
                    'last_login_gte': '2100-01-01',
                },
                'count': 0,
            },
            {
                'data' : {
                    'last_login_lte': '1900-01-01',
                },
                'count': 0,
            },
        ]
        for c in cases:
            response = self.client.get(path=self.base_url, data=c.get('data'), format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data['results']), c.get('count'))

    def test_get_inactive_filter(self):
        User.objects.create(
            first_name='active',
            username='active@example.com',
            company=GET_DEFAULT_COMPANY(),
            is_active=True,
            user_service_id='user_2389589420',
        )
        User.objects.create(
            first_name='non_active',
            username='non_active@example.com',
            company=GET_DEFAULT_COMPANY(),
            is_active=False,
            user_service_id='user_2389589421',
        )
        data = {
            'first_name': 'active',
            'inactive_filter': 'use_filter',
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        data = {
            'first_name': 'active',
            'inactive_filter': 'ignore_filter',
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_create_user(self):
        data = {
            'email': 'new_user@example.com',
            'role': 'manager',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '招待済みのメールアドレスです。')

    def test_not_null_values(self):
        response = self.client.post(path=self.base_url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        print(response.data)
        self.assertEqual(str(response.data['username']), "[ErrorDetail(string='この項目は必須です。', code='required')]")
        self.assertEqual(str(response.data['password']), "[ErrorDetail(string='この項目は必須です。', code='required')]")

    def test_patch_invalidate_value(self):
        instance = User.objects.get(username=TEST_USER_NAME)

        base_data = {
            'email': 'patch@example.com',
            'role': 'admin',
        }

        data = {**base_data, **{
            'tel1': 1,
            'tel2': 2,
        }}
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        data = {**base_data, **{
            'tel3': 3,
            'tel1': 1,
        }}
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), 'TELの欄は全て半角数字で入力してください。')

        """Test update user with tel longer than 15"""
        data = {**base_data, **{
            'tel1': 12345,
            'tel2': 12345,
            'tel3': 123456,
        }}
        response = self.client.patch(
            path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), TelValueIsTooLongException().default_detail)

    def test_patch_validate_user_service_id(self):
        instance = User.objects.get(username=TEST_USER_NAME)

        # validate max length
        response = self.client.patch(
            path=f'/app_staffing/users/{instance.id}', data={'user_service_id': 'user_0123456789123'}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['user_service_id']), settings.LENGTH_VALIDATIONS['user']['user_service_id']['message'])

        # validate regex - only allow: a-z, A-Z and _
        response = self.client.patch(
            path=f'/app_staffing/users/{instance.id}', data={'user_service_id': '#user01'}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['regex_error']
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate keyword, not allow value: 'here', 'card', 'comment', 'board'
        value = 'board'
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data={'user_service_id': value}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['value_not_allowed'].format(value)
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate null value
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data={'user_service_id': None}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['null_value']
        self.assertEqual(str(response.data['user_service_id'][0]), msg)

    def test_patch_duplicate_user_service_id(self):
        # prepare data
        User.objects.create(username='user_test2@example.com', company=GET_DEFAULT_COMPANY(), user_service_id='user_02')
        u = User.objects.create(username='user_test1@example.com', company=GET_DEFAULT_COMPANY(), user_service_id='user_01')

        # update user with user_service_id existed
        response = self.client.patch(
            path=f'/app_staffing/users/{u.id}',
            data={'user_service_id': 'user_02', 'pk': f'{u.id}'},
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['unique_error']
        self.assertEqual(str(response.data['detail']), msg)

    def test_patch_password(self):
        instance = User.objects.get(username=TEST_USER_NAME)

        data = {
            'email': 'patch_password@example.com',
            'role': 'master',
        }
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_instance = User.objects.get(id=instance.id)
        self.assertEqual(instance.password, updated_instance.password)

        data = {
            'email': 'patch_password@example.com',
            'role': 'admin',
            'password': 'updatedPassword123.',
        }
        response = self.client.patch(path=f'/app_staffing/users/{instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_instance = User.objects.get(id=instance.id)
        self.assertNotEqual(instance.password, updated_instance.password)

    def test_cannot_create_user_master(self):
        data = {
            'email': 'new_user@example.com',
            'role': 'master',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        instance = User.objects.get(username=data['email'])
        response = self.client.delete(path=f'/app_staffing/users/{instance.id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        data = {
            'email': instance.email,
            'role': 'master',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '招待済みのメールアドレスです。')

    def test_can_create_user_admin(self):
        data = {
            'email': 'new_user@example.com',
            'role': 'admin',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        instance = User.objects.get(username=data['email'])
        response = self.client.delete(path=f'/app_staffing/users/{instance.id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        data = {
            'email': instance.email,
            'role': 'admin',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_cannot_update_duplicate_email_master(self):
        data = {
            'email': 'new_user@example.com',
            'role': 'master',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        instance = User.objects.get(username=data['email'])
        response = self.client.delete(path=f'/app_staffing/users/{instance.id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        target_instance = User.objects.get(username=TEST_USER_NAME)
        data = {
            'email': instance.email,
            'role': 'master',
        }
        response = self.client.patch(path=f'/app_staffing/users/{target_instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '登録済みのメールアドレスです。')

    def test_can_update_duplicate_email_admin(self):
        data = {
            'email': 'new_user@example.com',
            'role': 'admin',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        instance = User.objects.get(username=data['email'])
        response = self.client.delete(path=f'/app_staffing/users/{instance.id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        target_instance = User.objects.get(username=TEST_USER_NAME)
        data = {
            'email': instance.email,
            'role': 'admin',
        }
        response = self.client.patch(path=f'/app_staffing/users/{target_instance.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invite_deleted_user(self):
        response = self.client.post(path='/app_staffing/users', data={'email': 'user@example.com', 'role': 'admin'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.delete(path=f'/app_staffing/users/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.post(path='/app_staffing/users', data={'email': 'user@example.com', 'role': 'admin'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_role_conditions(self):
        User.objects.create(
            first_name='admin',
            last_name='testsearch',
            username='admin@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=1),
            is_active=True,
            user_service_id='user_1823584923',
        )
        User.objects.create(
            first_name='manager',
            last_name='testsearch',
            username='manager@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=2),
            is_active=True,
            user_service_id='user_1823584924',
        )
        User.objects.create(
            first_name='leader',
            last_name='testsearch',
            username='leader@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=3),
            is_active=True,
            user_service_id='user_1823584925',
        )
        User.objects.create(
            first_name='member',
            last_name='testsearch',
            username='member@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=4),
            is_active=True,
            user_service_id='user_1823584926',
        )
        User.objects.create(
            first_name='guest',
            last_name='testsearch',
            username='guest@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=5),
            is_active=True,
            user_service_id='user_1823584927',
        )
        User.objects.create(
            first_name='master',
            last_name='testsearch',
            username='master@example.com',
            company=GET_DEFAULT_COMPANY(),
            user_role=UserRole.objects.get(id=6),
            is_active=True,
            user_service_id='user_1823584928',
        )

        testCases = [
            # 1件指定(非not)
            {
                "data": {"role": "admin"},
                "expected": ["admin"],
            },
            {
                "data": {"role": "manager"},
                "expected": ["manager"],
            },
            {
                "data": {"role": "leader"},
                "expected": ["leader"],
            },
            {
                "data": {"role": "member"},
                "expected": ["member"],
            },
            {
                "data": {"role": "guest"},
                "expected": ["guest"],
            },
            {
                "data": {"role": "master"},
                "expected": ["master"],
            },
            # 1件指定(not)
            {
                "data": {"role": "not:admin"},
                "expected": ["manager", "leader", "member", "guest", "master"],
            },
            {
                "data": {"role": "not:manager"},
                "expected": ["admin", "leader", "member", "guest", "master"],
            },
            {
                "data": {"role": "not:leader"},
                "expected": ["admin", "manager", "member", "guest", "master"],
            },
            {
                "data": {"role": "not:member"},
                "expected": ["admin", "manager", "leader", "guest", "master"],
            },
            {
                "data": {"role": "not:guest"},
                "expected": ["admin", "manager", "leader", "member", "master"],
            },
            {
                "data": {"role": "not:master"},
                "expected": ["admin", "manager", "leader", "member", "guest"],
            },
            # 2件指定(非not)
            {
                "data": {"role": "admin,manager"},
                "expected": ["admin", "manager"],
            },
            {
                "data": {"role": "manager,leader"},
                "expected": ["manager", "leader"],
            },
            {
                "data": {"role": "leader,member"},
                "expected": ["leader", "member"],
            },
            {
                "data": {"role": "member,guest"},
                "expected": ["member", "guest"],
            },
            {
                "data": {"role": "guest,master"},
                "expected": ["guest", "master"],
            },
            # 2件指定(not)
            {
                "data": {"role": "not:admin,not:manager"},
                "expected": ["leader", "member", "guest", "master"],
            },
            {
                "data": {"role": "not:manager,not:leader"},
                "expected": ["admin", "member", "guest", "master"],
            },
            {
                "data": {"role": "not:leader,not:member"},
                "expected": ["admin", "manager", "guest", "master"],
            },
            {
                "data": {"role": "not:member,not:guest"},
                "expected": ["admin", "manager", "leader", "master"],
            },
            {
                "data": {"role": "not:guest,not:master"},
                "expected": ["admin", "manager", "leader", "member"],
            },
            # 3件指定(非not)
            {
                "data": {"role": "admin,manager,leader"},
                "expected": ["admin", "manager", "leader"],
            },
            {
                "data": {"role": "manager,leader,member"},
                "expected": ["manager", "leader", "member"],
            },
            {
                "data": {"role": "leader,member,guest"},
                "expected": ["leader", "member", "guest"],
            },
            {
                "data": {"role": "member,guest,master"},
                "expected": ["member", "guest", "master"],
            },
            # 3件指定(not)
            {
                "data": {"role": "not:admin,not:manager,not:leader"},
                "expected": ["member", "guest", "master"],
            },
            {
                "data": {"role": "not:manager,not:leader,not:member"},
                "expected": ["admin", "guest", "master"],
            },
            {
                "data": {"role": "not:leader,not:member,not:guest"},
                "expected": ["admin", "manager", "master"],
            },
            {
                "data": {"role": "not:member,not:guest,not:master"},
                "expected": ["admin", "manager", "leader"],
            },
            # 4件指定(非not)
            {
                "data": {"role": "admin,manager,leader,member"},
                "expected": ["admin", "manager", "leader", "member"],
            },
            {
                "data": {"role": "manager,leader,member,guest"},
                "expected": ["manager", "leader", "member", "guest"],
            },
            {
                "data": {"role": "leader,member,guest,master"},
                "expected": ["leader", "member", "guest", "master"],
            },
            # 4件指定(not)
            {
                "data": {"role": "not:admin,not:manager,not:leader,not:member"},
                "expected": ["guest", "master"],
            },
            {
                "data": {"role": "not:manager,not:leader,not:member,not:guest"},
                "expected": ["admin", "master"],
            },
            {
                "data": {"role": "not:leader,not:member,not:guest,not:master"},
                "expected": ["admin", "manager"],
            },
            # 5件指定(非not)
            {
                "data": {"role": "admin,manager,leader,member,guest"},
                "expected": ["admin", "manager", "leader", "member", "guest"],
            },
            {
                "data": {"role": "manager,leader,member,guest,master"},
                "expected": ["manager", "leader", "member", "guest", "master"],
            },
            # 5件指定(not)
            {
                "data": {"role": "not:admin,not:manager,not:leader,not:member,not:guest"},
                "expected": ["master"],
            },
            {
                "data": {"role": "not:manager,not:leader,not:member,not:guest,not:master"},
                "expected": ["admin"],
            },
            # 6件指定(非not)
            {
                "data": {"role": "admin,manager,leader,member,guest,master"},
                "expected": ["admin", "manager", "leader", "member", "guest", "master"],
            },
            # 6件指定(not)
            {
                "data": {"role": "not:admin,not:manager,not:leader,not:member,not:guest,not:master"},
                "expected": [],
            },
        ]

        for testCase in testCases:
            # fixtureのデータを回避したいので last_name の条件を追加
            data = {"role":  testCase["data"]["role"], "last_name": "testsearch"}
            response = self.client.get(path=self.base_url, data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            actual = [res["first_name"] for res in response.data["results"]]
            self.assertEqual(sorted(actual), sorted(testCase["expected"]))

    def test_delete_api(self):
        response = self.client.delete(path=self.base_url + self.id_suffix.format(self.user_id), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class UserRegisterViewTestCase(APICRUDTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]
    base_url = '/app_staffing/users/register'
    resource_id = dumps('1957ab0f-b47c-455a-a7b7-4453cfffd05e')  # Correspond to Test user ID.

    skip_tests = ('put', 'post', 'list', 'delete',)  # Specify a tuple of SkipChoices.

    id_suffix = '/{0}'
    list_response_validator_class = IsPagenatedResponse
    resource_validator_class = IsUser

    patch_test_cases = [
        ('normal', {'last_name': 'Kamioka', 'password':'passwordChanged0123.'}),
    ]

    def setUp(self):
        CompanyAttribute.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            user_registration_limit=100
        )
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_update_password(self):
        new_password = 'passwordChanged0123.'
        response = self.client.patch(path=self.base_url + '/' + self.resource_id, data={'password': new_password}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check If the password has updated correctly.
        instance = User.objects.get(username=TEST_USER_NAME)
        self.assertTrue(instance.check_password(new_password))

    def test_cannnot_activate(self):
        attribute = CompanyAttribute.objects.get(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')
        attribute.user_registration_limit = 1
        attribute.save()
        new_password = 'cannotactivate0123'
        response = self.client.patch(path=self.base_url + '/' + self.resource_id, data={'password': new_password}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], UserRegisterActivateLimitException().default_detail)

    def test_update_user_service_id_success(self):
        new_user_service_id = 'my_user'
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': new_user_service_id}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check If the user_service_id has updated correctly.
        instance = User.objects.get(username=TEST_USER_NAME)
        self.assertEqual(instance.user_service_id, new_user_service_id)

    def test_validate_user_service_id(self):
        # validate max length
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': 'user_0123456789123'}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['user_service_id']), settings.LENGTH_VALIDATIONS['user']['user_service_id']['message'])

        # validate regex - only allow: a-z, A-Z and _
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': 'user-01'}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['regex_error']
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate keyword, not allow value: 'here', 'card', 'comment', 'board'
        value = 'comment'
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': value}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['value_not_allowed'].format(value)
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate null value
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': None}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['null_value']
        self.assertEqual(str(response.data['user_service_id'][0]), msg)

    def test_register_with_duplicate_user_service_id(self):
        # prepare data
        user_service_id = 'user_123'
        User.objects.create(username='user_id123@example.com', company=GET_DEFAULT_COMPANY(), user_service_id=user_service_id)

        # register user with user_service_id existed
        response = self.client.patch(
            path=self.base_url + '/' + self.resource_id, data={'user_service_id': user_service_id}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['unique_error']
        self.assertEqual(str(response.data['detail']), msg)


class UserViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]
    base_url = '/app_staffing/users'

    # Used in TestQueryParamErrorMixin
    filter_params = {
        'id': '1957ab0f-b47c-455a-a7b7-4453cfffd05e',
        'first_name': 'Trump',
        'last_name': 'Donald',
        'email': 'trump@example.com',
        'is_user_admin': True,
        'is_active': True,
        'old_id': 1,
    }

    @classmethod
    def setUpClass(cls):
        # Assert fixture conditions..
        super().setUpClass()
        assert User.objects.get(pk="d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", username=NORMAL_USER_NAME, is_user_admin=False)
        assert User.objects.get(pk="1dae6212-0ff4-40e4-86e1-bd73664a8b13")

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_not_create_user(self):
        response = self.client.post(path=self.base_url, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_get_list(self):
        response = self.client.get(path=self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_self_item(self):
        response = self.client.get(path=self.base_url + '/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_others_item(self):
        response = self.client.get(path=self.base_url + '/1dae6212-0ff4-40e4-86e1-bd73664a8b13')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_not_update_self_item(self):
        response = self.client.patch(path=self.base_url + '/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_not_update_others_item(self):
        response = self.client.patch(path=self.base_url + '/1dae6212-0ff4-40e4-86e1-bd73664a8b13', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_not_delete_self(self):
        response = self.client.delete(path=self.base_url + '/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_not_delete_others(self):
        response = self.client.delete(path=self.base_url + '/1dae6212-0ff4-40e4-86e1-bd73664a8b13', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class UserViewPermissionTestCaseWithAdminUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]
    user_id = '67449fa2-771d-4ffa-bab3-fa160fdf716f'

    @classmethod
    def setUpClass(cls):
        # Assert fixture conditions..
        super().setUpClass()
        assert User.objects.get(pk="d31034e2-ca12-4a6f-b1dc-0be092d1ac5d")
        assert User.objects.get(pk="1dae6212-0ff4-40e4-86e1-bd73664a8b13", username=ADMIN_USER_NAME, is_user_admin=True)

    def setUp(self):
        self.client.login(username=ADMIN_USER_NAME, password=ADMIN_USER_PASSWORD)

    def test_can_create_user(self):
        response = self.client.post(path='/app_staffing/users', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_can_get_list(self):
        response = self.client.get(path='/app_staffing/users')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_self_item(self):
        response = self.client.get(path='/app_staffing/users/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_others_item(self):
        response = self.client.get(path='/app_staffing/users/1dae6212-0ff4-40e4-86e1-bd73664a8b13')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_update_self_item(self):
        response = self.client.patch(path='/app_staffing/users/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_update_others_item(self):
        response = self.client.patch(path='/app_staffing/users/1dae6212-0ff4-40e4-86e1-bd73664a8b13', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_delete_self(self):
        response = self.client.delete(path='/app_staffing/users/d31034e2-ca12-4a6f-b1dc-0be092d1ac5d', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_can_delete_others(self):
        response = self.client.delete(path=f'/app_staffing/users/{self.user_id}', data={})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class MyProfileViewTestCase(APITestCase):
    """A Test case for an API of email object."""

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    test_data_for_update = {
        'first_name': 'TEST',
        'last_name': 'USER',
        'email': 'wont_be_updated@example.com',  # This must be ignored. (read-only field)
        'email_signature': "*********\n A New Signature *********",
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.user_avatar_url = api_reverse('users_avatar')

    def test_get_my_profile(self):
        """Test a single object retrieval."""
        response = self.client.get(path='/app_staffing/my_profile')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if there is enough attributes and no extra attributes.
        validator = IsUserProfile(response.data)
        validator.validate()

    def test_patch_my_profile(self):
        """Test a partial update."""

        new_data = self.test_data_for_update

        response = self.client.patch(path='/app_staffing/my_profile', data=new_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if there is enough attributes and no extra attributes.
        validator = IsUserProfile(response.data)
        validator.validate()

        # Check if data has been successfully accepted except email field.
        self.assertEqual(response.data['first_name'], new_data['first_name'])
        self.assertEqual(response.data['last_name'], new_data['last_name'])
        self.assertEqual(response.data['email_signature'], new_data['email_signature'])

        # Assert that the email field (read-only) is not updated at all.
        self.assertNotEqual(response.data['email'], new_data['email'])

    def test_patch_new_password(self):
        new_password = 'MyNewExtremelyIncrediblePassword0123.!@'
        data = {
            'first_name': 'TEST',
            'last_name': 'USER',
            'password': new_password
        }

        response = self.client.patch(path='/app_staffing/my_profile', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if there is enough attributes and no extra attributes.
        validator = IsUserProfile(response.data)
        validator.validate()

        # Check If the password has updated correctly.
        instance = User.objects.get(username=TEST_USER_NAME)
        self.assertTrue(instance.check_password(new_password))

    def test_put_is_prohibited(self):
        response = self.client.put(path='/app_staffing/my_profile', data=self.test_data_for_update)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_over_max_length(self):
        data = {
            'first_name': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'last_name': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'password': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'tel1': 11111,
            'tel2': 11111,
            'tel3': 111111,
            'email': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb@example.com',
            'email_signature': """
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbb
                ccc
            """,
        }
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['name']), settings.LENGTH_VALIDATIONS['user']['name']['message'])
        self.assertEqual(str(response.data['password']), settings.LENGTH_VALIDATIONS['user']['password']['message'])
        self.assertEqual(str(response.data['tel']), settings.LENGTH_VALIDATIONS['user']['tel']['message'])
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['user']['email']['message'])
        self.assertEqual(str(response.data['email_signature']), settings.LENGTH_VALIDATIONS['user']['email_signature']['message'])

    def test_invalid_value(self):
        data = {**self.test_data_for_update, **{
            'tel1': 'A',
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**self.test_data_for_update, **{
            'tel1': 1,
            'tel2': 'B',
            'tel3': 3,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**self.test_data_for_update, **{
            'tel1': 1,
            'tel2': 2,
            'tel3': 'C',
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**self.test_data_for_update, **{
            'tel1': 1,
            'tel2': 2,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**self.test_data_for_update, **{
            'tel2': 2,
            'tel3': 3,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        data = {**self.test_data_for_update, **{
            'tel3': 3,
            'tel1': 1,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), InvalidateTelValueException().default_detail)

        """test update my profile with tel longer than 15"""
        data = {**self.test_data_for_update, **{
            'tel1': 12345,
            'tel2': 12345,
            'tel3': 123456,
        }}
        response = self.client.patch(path='/app_staffing/my_profile', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['tel']), TelValueIsTooLongException().default_detail)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/my_profile')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        response = self.client.patch(path='/app_staffing/my_profile', data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_valid_patch_delete_my_profile_avatar(self):
        f = get_uploaded_file(get_extras_file('avatar.png', extras_dir))
        data = {'avatar': f}
        response = self.client.patch(path='/app_staffing/my_profile', data=data)
        validator = IsUserProfile(response.data)
        validator.validate()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        instance = response.wsgi_request.user
        self.assertIn(str(instance.id), validator['avatar'])
        self.assertIn(generate_image_filename(instance, 'avatar.png'), validator['avatar'])

        resp = self.client.delete(self.user_avatar_url, data={'email': instance.email})
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(resp.data['detail'], settings.USER_DELETE_AVATAR_SUCCESS)
        self.assertIn(instance.avatar.name, ['', None])

    def test_invalid_gif_allowed_extension(self):
        f = get_uploaded_file(get_extras_file('avatar.gif', extras_dir))
        data = {'avatar': f}
        response = self.client.patch(path='/app_staffing/my_profile', data=data)
        self.assertEqual(response.status_code, FileExtensionValidatorException().status_code)
        self.assertEqual(response.data['detail'], FileExtensionValidatorException().default_detail)

    def test_invalid_max_size(self):
        f = get_uploaded_file(get_extras_file('avatar_invalid_max_size.png', extras_dir))
        data = {'avatar': f}
        response = self.client.patch(path='/app_staffing/my_profile', data=data)
        self.assertEqual(response.status_code, MaxSizeValidatorException().status_code)
        self.assertEqual(response.data['detail'], MaxSizeValidatorException().default_detail)

    def test_invalid_delete_avatar_not_email_field(self):
        response = self.client.delete(self.user_avatar_url)
        self.assertEqual(response.status_code, RequiredFieldsException('email').status_code)
        self.assertEqual(response.data['email'], RequiredFieldsException('email').default_detail['email'])

    def test_invalid_delete_avatar_user_does_not_exist(self):
        response = self.client.delete(self.user_avatar_url, data={'email': 'useravatar@doesnotexist.com'})
        self.assertEqual(response.status_code, UserDoesNotExistException().status_code)
        self.assertEqual(response.data['detail'], UserDoesNotExistException().default_detail)

    def test_delete_avatar_default(self):
        resp = self.client.delete(self.user_avatar_url, data={'email': 'apiuser@example.com'})
        self.assertEqual(resp.status_code, DeleteAvatarDefault().status_code)
        self.assertEqual(resp.data['detail'], DeleteAvatarDefault().default_detail)

    def test_invalid_delete_avatar(self):
        resp = self.client.delete(self.user_avatar_url, data={'email': 'useravatar@delete.com'})
        self.assertEqual(resp.status_code, DeleteFileError().status_code)
        self.assertEqual(resp.data['detail'], DeleteFileError().default_detail)

    def test_invalid_delete_avatar_by_role(self):
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        resp = self.client.delete(self.user_avatar_url, data={'email': 'apiuser@example.com'})
        self.assertEqual(resp.status_code, PermissionDeleteAvatarException().status_code)
        self.assertEqual(resp.data['detail'], PermissionDeleteAvatarException().default_detail)


    def test_validate_user_service_id(self):
        # validate max length
        response = self.client.patch(
            path='/app_staffing/my_profile', data={'user_service_id': 'user_0123456789123'}, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['user_service_id']), settings.LENGTH_VALIDATIONS['user']['user_service_id']['message'])

        # validate regex - only allow: a-z, A-Z and _
        response = self.client.patch(path='/app_staffing/my_profile', data={'user_service_id': 'user(01)'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['regex_error']
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate keyword, not allow value: 'here', 'card', 'comment', 'board'
        value = 'card'
        response = self.client.patch(path='/app_staffing/my_profile', data={'user_service_id': value}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['value_not_allowed'].format(value)
        self.assertEqual(str(response.data['user_service_id']), msg)

        # validate null value
        response = self.client.patch(path='/app_staffing/my_profile', data={'user_service_id': None}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['null_value']
        self.assertEqual(str(response.data['user_service_id'][0]), msg)

    def test_register_with_duplicate_user_service_id(self):
        # prepare data
        user_service_id = 'test_user01'
        User.objects.create(username='user_id123@example.com', company=GET_DEFAULT_COMPANY(), user_service_id=user_service_id)

        # update profile with user_service_id existed
        response = self.client.patch(path='/app_staffing/my_profile', data={'user_service_id': user_service_id}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['unique_error']
        self.assertEqual(str(response.data['detail']), msg)


class UserViewMultiTenantTestCase(MultiTenancyTestMixin, APITestCase):
    base_url = '/app_staffing/users'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user_role_member = UserRoleFactory(id=4, name='member', order=4)
        # Since this is a user view and there is already 2 users for this test (Generated in superclass,)
        # So we will not create test resources here.
        cls.resource_id_for_tenant_1 = cls.user1.id
        cls.resource_id_for_tenant_2 = cls.user2.id
        cls.post_data_for_tenant_1 = {
            'last_name': 'TENANT A',
            'first_name': 'USER 2',
            'email': 'newuser@tenantA.com',  # Note, this email must be unique even in multi tenant environment.
            'email_signature': "*********\n A New User *********",
            'role': 'member',
            'password': 'changeMe0123.'
        }

        cls.post_data_for_tenant_2 = {
            'last_name': 'TENANT B',
            'first_name': 'USER 2',
            'email': 'newuser@tenantB.com',  # Note, this email must be unique even in multi tenant environment.
            'email_signature': "*********\n A New User *********",
            'role': 'member',
            'password': 'changeMe0123.'
        }
        cls.user3 = AppUserFactory(company=cls.company_1, email='user_c@tenantC.com', username='user@tenantC.com', user_role=cls.user_role_admin, user_service_id='user_123456788')
        cls.user4 = AppUserFactory(company=cls.company_2, email='user_d@tenantD.com', username='user@tenantD.com', user_role=cls.user_role_admin, user_service_id='user_123456788')
        cls.resource_id_for_tenant_3 = cls.user3.id
        cls.resource_id_for_tenant_4 = cls.user4.id

        cls.patch_data_for_tenant_1 = {'password': 'greatPassword0123#$'}
        cls.patch_data_for_tenant_2 = {'password': 'awesomePassword0123%^'}

    def test_delete_data_separation(self):
        if 'delete' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        if not (self.resource_id_for_tenant_1 and self.resource_id_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 ids of resources to class attribute to execute a delete test.')

        # Check if a user can NOT delete data that belongs to other tenants.
        response1 = self.client_1.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', format='json')
        response2 = self.client_2.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)

        # Check if a user can only delete data that belongs to same tenant.
        response3 = self.client_1.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_3}', format='json')
        response4 = self.client_2.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_4}', format='json')
        self.assertEqual(status.HTTP_204_NO_CONTENT, response3.status_code)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response4.status_code)

    def test_list_data_separation(self):
        """This test checks data separation between different company's resource.
        Note: We need to pass basic CRUD operations to execute this test correctly.
        """
        if 'list' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if a user can only see data that belong to the same tenant.
        response1 = self.client_1.get(path=self.base_url, format='json')
        response2 = self.client_2.get(path=self.base_url, format='json')
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)
        self.assertEqual(2, len(response1.data['results']))
        self.assertEqual(2, len(response2.data['results']))

class UserCannotDeleteTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/test_user_delete/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/userCannotDeactivate/contacts.json',
        'app_staffing/tests/fixtures/userCannotDeactivate/scheduled_emails.json',
        'app_staffing/tests/fixtures/userCannotDeactivate/emails.json',
    ]

    ADMIN_USER_ID = '1dae6212-0ff4-40e4-86e1-bd73664a8b13'
    ANOTHER_USER_ID = 'e9b04508-398d-4d21-9a4c-921fac05e742'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        self.client.login(username='useradminn@example.com', password=ADMIN_USER_PASSWORD)

    def test_cannot_deactivate(self):
        response = self.client.patch(path='/app_staffing/users/' + self.ADMIN_USER_ID, data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeactivateException.default_detail)

        ScheduledEmail.objects.filter(sender_id=self.ADMIN_USER_ID).delete()
        response = self.client.patch(path='/app_staffing/users/' + self.ADMIN_USER_ID, data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_hide(self):
        response = self.client.delete(path='/app_staffing/users/' + self.ADMIN_USER_ID, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeleteUserException.default_detail)

        ScheduledEmail.objects.filter(sender_id=self.ADMIN_USER_ID).delete()
        response = self.client.patch(path='/app_staffing/users/' + self.ADMIN_USER_ID, data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_activate(self):
        user = User.objects.get(id=self.ANOTHER_USER_ID)
        user.is_active = False
        user.save()
        CompanyAttribute.objects.create(company_id=DEFAULT_COMPANY_ID,user_registration_limit=1)
        response = self.client.patch(path='/app_staffing/users/' + self.ANOTHER_USER_ID, data={'is_active': True}, format='json')
        self.assertEqual(response.data['detail'], UserUpdateActivateLimitException.default_detail)

    def test_can_activate(self):
        user = User.objects.get(id=self.ANOTHER_USER_ID)
        user.is_active = False
        user.save()
        CompanyAttribute.objects.create(company_id=DEFAULT_COMPANY_ID,user_registration_limit=6)
        response = self.client.patch(path='/app_staffing/users/' + self.ANOTHER_USER_ID, data={'is_active': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.filter(company_id=DEFAULT_COMPANY_ID, is_active=True).count(), 6)

    def test_cannot_bulk_deactivate(self):
        data = {
            'source': [self.ADMIN_USER_ID],
            'column': 'is_active',
            'value': False
        }
        response = self.client.patch(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeactivateException.default_detail)

        ScheduledEmail.objects.filter(sender_id=self.ADMIN_USER_ID).delete()
        response = self.client.patch(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_bulk_hide(self):
        data = {
            'source': [self.ADMIN_USER_ID],
        }
        response = self.client.delete(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeleteUserException.default_detail)

        ScheduledEmail.objects.filter(sender_id=self.ADMIN_USER_ID).delete()
        response = self.client.delete(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_can_bulk_activate(self):
        user = User.objects.get(id=self.ANOTHER_USER_ID)
        user.is_active = False
        user.save()
        CompanyAttribute.objects.create(company_id=DEFAULT_COMPANY_ID,user_registration_limit=6)
        data = {
            'source': [self.ANOTHER_USER_ID],
            'column': 'is_active',
            'value': True
        }
        response = self.client.patch(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_bulk_activate(self):
        user = User.objects.get(id=self.ANOTHER_USER_ID)
        user.is_active = False
        user.save()
        CompanyAttribute.objects.create(company_id=DEFAULT_COMPANY_ID,user_registration_limit=1)
        data = {
            'source': [self.ANOTHER_USER_ID],
            'column': 'is_active',
            'value': True
        }
        response = self.client.patch(path='/app_staffing/users', data=data, format='json')
        self.assertEqual(response.data['detail'], UserUpdateActivateLimitException.default_detail)

class SortUserByRoleTestCase(APITestCase):
    # TODO: this view have permission classes, so add 3 test conditions :
    #  access user is userAdmin / createdUser / otherUsers
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/sortUserByRole/users.json',
    ] # Correspond to Test user ID.

    base_url = '/app_staffing/users'
    list_response_validator_class = IsPagenatedResponse
    resource_validator_class = IsUser

    skip_tests = ('put',)  # Specify a tuple of SkipChoices.

    cache_key_base = f'{settings.CACHE_SEARCH_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_1957ab0f-b47c-455a-a7b7-4453cfffd05e_'
    cache_key_contact = cache_key_base + 'contact'
    cache_key_contact_mail_preference = cache_key_base + 'contact_mail_preference'
    cache_key_organization = cache_key_base + 'organization'
    cache_key_scheduled_email = cache_key_base + 'scheduled_email'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_sort_users_by_user_role_asc(self):
        data = {
            'inactive_filter': 'use_filter',
            'page': 1,
            'page_size': 10,
            'ordering': 'user_role__order',
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['results'][0]['role'], 'guest')
        self.assertTrue(response.data['results'][1]['role'], 'member')
        self.assertTrue(response.data['results'][2]['role'], 'leader')
        self.assertTrue(response.data['results'][3]['role'], 'manager')
        self.assertTrue(response.data['results'][4]['role'], 'admin')
        self.assertTrue(response.data['results'][5]['role'], 'master')

    def test_sort_users_by_user_role_desc(self):
        data = {
            'inactive_filter': 'use_filter',
            'page': 1,
            'page_size': 10,
            'ordering': '-user_role__order',
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['results'][5]['role'], 'guest')
        self.assertTrue(response.data['results'][4]['role'], 'member')
        self.assertTrue(response.data['results'][3]['role'], 'leader')
        self.assertTrue(response.data['results'][2]['role'], 'manager')
        self.assertTrue(response.data['results'][1]['role'], 'admin')
        self.assertTrue(response.data['results'][0]['role'], 'master')


class CanNotDeActiveUpdateUserView(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
        'app_staffing/tests/fixtures/authtokens.json',
    ]

    base_url = '/app_staffing/users'
    user_id = '1957ab0f-b47c-455a-a7b7-4453cfffd05e'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_not_deactive_user_when_login_list_view(self):
        instance = User.objects.get(pk=self.user_id)

        response = self.client.patch(path=f'{self.base_url}', data={'source': [
                                     instance.id], 'column': 'is_active', 'value': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data['detail'], UserLoginCanNotInActiveException.default_detail)

    def test_can_not_deactive_user_when_login_detail_view(self):
        instance = User.objects.get(username=TEST_USER_NAME)
        response = self.client.patch(
            path=f'/app_staffing/users/{instance.id}', data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data['detail'], UserLoginCanNotInActiveException.default_detail)


class CanNotDeleteUserView(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
        'app_staffing/tests/fixtures/authtokens.json',
    ]

    base_url = '/app_staffing/users'
    user_id = '1957ab0f-b47c-455a-a7b7-4453cfffd05e'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_can_not_delete_user_when_login_list_view(self):
        instance = User.objects.get(pk=self.user_id)

        response = self.client.delete(path=f'{self.base_url}', data={'source': [
                                     instance.id], 'column': 'is_active', 'value': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], UserLoginCanNotDeleteException.default_detail)

    def test_can_not_delete_user_when_login_detail_view(self):
        instance = User.objects.get(username=TEST_USER_NAME)
        response = self.client.delete(
            path=f'/app_staffing/users/{instance.id}', data={'is_active': False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], UserLoginCanNotDeleteException.default_detail)
