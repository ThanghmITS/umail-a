from django.test import TestCase
from django.conf import settings
from datetime import datetime, timedelta, date
from app_staffing.models import Plan, Addon, PurchaseHistory, CompanyAttribute, PlanPaymentError, Company
from app_staffing.models.user import User
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from app_staffing.tests.helpers.mock_payjp_api import MockingPayjpAPIHelper
from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.email import ScheduledEmailFactory
from app_staffing.tests.factories.user import AppUserFactory
from app_staffing.tests.factories.organization import OrganizationFactory, ContactFactory, ContactJobTypePreferenceFactory, ContactPersonnelTypePreferenceFactory
from rest_framework import status
from rest_framework.test import APITestCase
from dateutil.relativedelta import relativedelta
import payjp
import pytz
from unittest.mock import patch
from django.core import mail
from freezegun import freeze_time

from app_staffing.utils.expiration_time import get_payment_date
from app_staffing.services.stats import OrganizationStats, ContactStats, ScheduledEmailStats

class AdminBatchPaymentViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/admin/users.json',
        'app_staffing/tests/fixtures/admin/company.json',
        'app_staffing/tests/fixtures/admin/plan.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    base_url = '/admin/batch_payment/'

    TENANT_A = '3efd25d0-989a-4fba-aa8d-b91708760eb9'
    TENANT_B = '3efd25d0-989a-4fba-aa8d-b91708760eb8'
    TENANT_C = '3efd25d0-989a-4fba-aa8d-b91708760eb7'
    TENANT_D = '3efd25d0-989a-4fba-aa8d-b91708760eb6'
    TENANT_E = '3efd25d0-989a-4fba-aa8d-b91708760eb5'
    TENANT_F = '3efd25d0-989a-4fba-aa8d-b91708760eb4'
    TENANT_G = '3efd25d0-989a-4fba-aa8d-b91708760eb3'
    TENANT_H = '3efd25d0-989a-4fba-aa8d-b91708760eb2'
    TENANT_I = '3efd25d0-989a-4fba-aa8d-b91708760eb1'
    
    @classmethod
    def mock_Charge_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        # 不正なクレジットカードの例外を発生させる
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb8': # TENANT_B
            raise Exception('No such customer')
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb3': # TENANT_G
            raise Exception('expired_card')
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb2': # TENANT_H
            raise Exception('card_declined')

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 0
        data = []
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb9':
            # TENANT_A
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb9',
                    'amount': 33000,
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb4':
            # TENANT_B
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb4',
                    'amount': 55550,
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb3':
            # TENANT_G
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb3',
                    'failure_code': 'expired_card',
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb2':
            # TENANT_H
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb2',
                    'failure_code': 'card_declined',
            }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')

    @classmethod
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def setUpClass(cls):
        super().setUpClass()
        payjp.api_key = settings.PAYJP_API_KEY
        # テナントAの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_A)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_A)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントFの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_F)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_F)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントGの顧客情報とクレジットカード情報(エラー)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_G)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_G)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000004012',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass
    
        # テナントHの顧客情報とクレジットカード情報(エラー)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_H)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_H)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass
    
    @classmethod
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def tearDownClass(cls):
        super().tearDownClass()
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(cls.TENANT_A)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_F)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_G)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_H)
        customer.delete()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

        now = datetime.now()
        # テナントA：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_A,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_A,
            user_registration_limit=5,
        )
        # テナントB：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_B,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=2)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_B,
            user_registration_limit=5,
        )
        # テナントC：支払い対象外
        Plan.objects.create(
            company_id=self.TENANT_C,
            plan_master_id=1,
            is_active=True,
            payment_date=(now + timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_C,
            user_registration_limit=5,
        )
        # テナントE：プランが2つ存在する(データ異常)
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=3)).date(),
        )
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=3)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_E,
            user_registration_limit=5,
        )
        # テナントF：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_F,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=4)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_F,
            user_registration_limit=10,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=1,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=2,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=2,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=4,
        )
        # テナントG：支払い対象だがクレジットカード情報が不正
        Plan.objects.create(
            company_id=self.TENANT_G,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=5)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_G,
            user_registration_limit=5,
        )
        # テナントH：支払い対象だがクレジットカード情報が不正
        Plan.objects.create(
            company_id=self.TENANT_H,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=6)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_H,
            user_registration_limit=5,
        )
        # テナントI：退会しているため支払い対象外
        Plan.objects.create(
            company_id=self.TENANT_I,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=7)).date(),
        )
        company_i = Company.objects.get(id=self.TENANT_I)
        company_i.deactivated_time = now
        company_i.save()
        CompanyAttribute.objects.create(
            company_id=self.TENANT_I,
            user_registration_limit=5,
        )

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    def test_payment(self):
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        planA_payment_date = get_payment_date(plans[0].payment_date)
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        planB_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        planF_payment_date = get_payment_date(plans[0].payment_date)
        now = datetime.now()

        # テナントA
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_A})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        # テナントAのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planA_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planA_payment_date.day)

        # テナントAの支払いが行われていることを確認する
        payjp.api_key = settings.PAYJP_API_KEY
        charges = payjp.Charge.all(customer=self.TENANT_A, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 33000)

        # テナントAのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_A)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) * 1.1 = 33000
        self.assertEqual(purchase_histories[0].price, 33000)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=1)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)


        # テナントB
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_B})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントBのプランが更新されていないことを確認する(トランザクションが切られているためロールバックされる)
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planB_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planB_payment_date.day)

        # テナントBの支払いが行われていないことを確認する(クレジットカードが登録されていないためエラーになる)
        charges = payjp.Charge.all(customer=self.TENANT_B, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_B).exists())


        # テナントC
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_C})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントCの支払いが行われていないことを確認する(プランの期限が切れていないため対象にならない)
        charges = payjp.Charge.all(customer=self.TENANT_C, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)


        # テナントD
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_D})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントDの支払いが行われていないことを確認する(プラン情報がない)
        charges = payjp.Charge.all(customer=self.TENANT_D, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)


        # テナントE
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_E})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントEの支払いが行われていないことを確認する(プラン情報が複数存在する)
        charges = payjp.Charge.all(customer=self.TENANT_E, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)


        # テナントF
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_F})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        # テナントFのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planF_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planF_payment_date.day)

        # テナントFの支払いが行われていることを確認する
        charges = payjp.Charge.all(customer=self.TENANT_F, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        # プランの料金(30000) + アドオンの料金(500 + 500 + 500 + 4000 = 5500) + ユーザー追加料金(5000 * 5 = 15000) = 50500
        # 50500 * 1.1 = 55550
        self.assertEqual(charges['data'][0]['amount'], 55550)

        # テナントFのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_F)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) + アドオンの料金(500 + 500 + 500 + 4000 = 5500) + ユーザー追加料金(5000 * 5 = 15000) = 50500
        # 50500 * 1.1 = 55550
        self.assertEqual(purchase_histories[0].price, 55550)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=4)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)


        # テナントG
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_G})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)
        
        # テナントGの支払いが行われていないことを確認する(カード情報が不正)
        charges = payjp.Charge.all(customer=self.TENANT_G, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['failure_code'], 'expired_card')

        # テナントGのエラー情報が保存されていることを確認する
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_G).exists())


        # テナントH
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_H})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントHの支払いが行われていないことを確認する(カード情報が不正)
        charges = payjp.Charge.all(customer=self.TENANT_H, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['failure_code'], 'card_declined')

        # テナントHのエラー情報が保存されていることを確認する
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_H).exists())

        # テナントI
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_I})
        self.assertNotEqual(status.HTTP_200_OK, response.status_code)

        # テナントIの支払いが行われていないことを確認する(退会テナント)
        charges = payjp.Charge.all(customer=self.TENANT_I, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)

        emails = [email for email in mail.outbox]
        self.assertEqual(len(emails), 5)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[0].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[1].subject)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[2].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[3].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[4].subject)

        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', emails[0].message().as_string())
        user = User.objects.get(company_id=self.TENANT_F)
        self.assertIn(user.display_name, emails[0].message().as_string())

        self.assertIn('お支払いの処理に失敗いたしましたのでご連絡いたします。', emails[1].message().as_string())
        user = User.objects.get(company_id=self.TENANT_B)
        self.assertIn(user.display_name, emails[1].message().as_string())

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    def test_payment_only_A(self):
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        planA_payment_date = get_payment_date(plans[0].payment_date)
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        planB_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_C, is_active=True)
        planC_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_E, is_active=True)
        planE_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        planF_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_G, is_active=True)
        planG_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_H, is_active=True)
        planH_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_I, is_active=True)
        planI_payment_date = plans[0].payment_date # 更新されない
        now = datetime.now()

        # テナントA
        response = self.client.post(self.base_url, data={'company_id':self.TENANT_A})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        # テナントAのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planA_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planA_payment_date.day)

        # テナントAの支払いが行われていることを確認する
        payjp.api_key = settings.PAYJP_API_KEY
        charges = payjp.Charge.all(customer=self.TENANT_A, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 33000)

        # テナントAのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_A)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) * 1.1 = 33000
        self.assertEqual(purchase_histories[0].price, 33000)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=1)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)


        # テナントB
        # テナントBのプランが更新されていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planB_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planB_payment_date.day)


        # テナントC
        # テナントCの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_C, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planC_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planC_payment_date.day)

        # テナントD
        # テナントDの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_D, is_active=True)
        self.assertEqual(len(plans), 0)


        # テナントE
        # テナントEの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_E, is_active=True)
        self.assertEqual(len(plans), 2)
        self.assertEqual(plans[0].payment_date.month, planE_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planE_payment_date.day)


        # テナントF
        # テナントFのプランが更新されていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planF_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planF_payment_date.day)


        # テナントG
        # テナントGの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_G, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planG_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planG_payment_date.day)


        # テナントH
        # テナントHの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_H, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planH_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planH_payment_date.day)


        # テナントI
        # テナントIの支払いが行われていないことを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_I, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planI_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planI_payment_date.day)

        emails = [email for email in mail.outbox]
        self.assertEqual(len(emails), 1)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[0].subject)

        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', emails[0].message().as_string())
        user = User.objects.get(company_id=self.TENANT_A)
        self.assertIn(user.display_name, emails[0].message().as_string())

class AdminBatchCalculateStatsViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/admin/users.json',
        'app_staffing/tests/fixtures/admin/company.json',
        'app_staffing/tests/fixtures/organization_master_data.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    base_url = '/admin/batch_calculate_stats/'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_calc_organization_stat(self):
        today = datetime.today()

        this_month = datetime(year=today.year, month=today.month, day=1)
        one_month_ago = this_month - relativedelta(months=1)
        two_month_ago = this_month - relativedelta(months=2)
        company = CompanyFactory.create()
        with freeze_time(two_month_ago):
            OrganizationFactory.create_batch(size=1, company=company, organization_category_id=1)
            OrganizationFactory.create_batch(size=2, company=company, organization_category_id=2)
            OrganizationFactory.create_batch(size=3, company=company, organization_category_id=3)
            OrganizationFactory.create_batch(size=4, company=company, organization_category_id=4)

        with freeze_time(one_month_ago):
            OrganizationFactory.create_batch(size=5, company=company, organization_category_id=1)
            OrganizationFactory.create_batch(size=6, company=company, organization_category_id=2)
            OrganizationFactory.create_batch(size=7, company=company, organization_category_id=3)
            OrganizationFactory.create_batch(size=8, company=company, organization_category_id=4)

        with freeze_time(this_month):
            OrganizationFactory.create_batch(size=9, company=company, organization_category_id=1)
            OrganizationFactory.create_batch(size=10, company=company, organization_category_id=2)
            OrganizationFactory.create_batch(size=11, company=company, organization_category_id=3)
            OrganizationFactory.create_batch(size=12, company=company, organization_category_id=4)

        # target_year_month 未指定時は先月が集計対象になる
        data = {
            'company_id': company.id,
            'is_target_organization': 'true',
        }
        expected = {
            'labels': ['{year}/{month}'.format(
                year=one_month_ago.year,
                month=one_month_ago.month,
            )],
            'values': {
                'prospective': [6],
                'approached': [8],
                'exchanged': [10],
                'client': [12],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = OrganizationStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)

        # target_year_month を指定すると過去の月を対象にできる
        print(two_month_ago.strftime("%Y/%m"))
        data = {
            'company_id': company.id,
            'target_year_month': two_month_ago.strftime("%Y/%m"),
            'is_target_organization': 'true',
        }
        expected = {
            'labels': [
                '{year}/{month}'.format(
                    year=two_month_ago.year,
                    month=two_month_ago.month,
                ),
                '{year}/{month}'.format(
                    year=one_month_ago.year,
                    month=one_month_ago.month,
                ),
            ],
            'values': {
                'prospective': [1, 6],
                'approached': [2, 8],
                'exchanged': [3, 10],
                'client': [4, 12],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = OrganizationStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_contact_stat(self):
        today = datetime.today()

        this_month = datetime(year=today.year, month=today.month, day=1)
        one_month_ago = this_month - relativedelta(months=1)
        two_month_ago = this_month - relativedelta(months=2)

        company = CompanyFactory.create()
        organization = OrganizationFactory(name='テスト取引先', company=company)

        with freeze_time(two_month_ago):
            contact_1 = ContactFactory(email='sampleuser1@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_2 = ContactFactory(email='sampleuser2@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_3 = ContactFactory(email='sampleuser3@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            contact_4 = ContactFactory(email='sampleuser4@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_4, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_5 = ContactFactory(email='sampleuser5@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_5, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_5, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_6 = ContactFactory(email='sampleuser6@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_6, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            ContactFactory(email='sampleuser7@example.com', company=company, organization=organization)

        with freeze_time(one_month_ago):
            contact_8 = ContactFactory(email='sampleuser8@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_8, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_9 = ContactFactory(email='sampleuser9@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_9, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_9, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_9, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_9, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_9, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_10 = ContactFactory(email='sampleuser10@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_10, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_10, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_10, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_10, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            contact_11 = ContactFactory(email='sampleuser11@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_11, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_11, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_11, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_12 = ContactFactory(email='sampleuser12@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_12, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_12, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_13 = ContactFactory(email='sampleuser13@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_13, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            ContactFactory(email='sampleuser14@example.com', company=company, organization=organization)

        with freeze_time(this_month):
            contact_15 = ContactFactory(email='sampleuser15@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_15, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_16 = ContactFactory(email='sampleuser16@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_16, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_16, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_16, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_16, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_16, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_17 = ContactFactory(email='sampleuser17@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_17, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_17, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_17, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=company, contact=contact_17, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            contact_18 = ContactFactory(email='sampleuser18@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_18, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_18, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=company, contact=contact_18, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_19 = ContactFactory(email='sampleuser19@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_19, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=company, contact=contact_19, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_20 = ContactFactory(email='sampleuser20@example.com', company=company, organization=organization)
            ContactJobTypePreferenceFactory(company=company, contact=contact_20, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            ContactFactory(email='sampleuser21@example.com', company=company, organization=organization)

        # target_year_month 未指定時は先月が集計対象になる
        data = {
            'company_id': company.id,
            'is_target_contact': 'true',
        }
        expected = {
            'labels': ['{year}/{month}'.format(
                year=one_month_ago.year,
                month=one_month_ago.month,
            )],
            'values': {
                'jobs': {
                    'development': [12],
                    'infrastructure': [10],
                    'others': [8],
                },
                'personnel': {
                    'development': [6],
                    'infrastructure': [4],
                    'others': [2],
                },
                'announcement': [14],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = ContactStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)

        # target_year_month を指定すると過去の月を対象にできる
        data = {
            'company_id': company.id,
            'target_year_month': two_month_ago.strftime("%Y/%m"),
            'is_target_contact': 'true',
        }
        expected = {
            'labels': [
                '{year}/{month}'.format(
                    year=two_month_ago.year,
                    month=two_month_ago.month,
                ),
                '{year}/{month}'.format(
                    year=one_month_ago.year,
                    month=one_month_ago.month,
                ),
            ],
            'values': {
                'jobs': {
                    'development': [6, 12],
                    'infrastructure': [5, 10],
                    'others': [4, 8],
                },
                'personnel': {
                    'development': [3, 6],
                    'infrastructure': [2, 4],
                    'others': [1, 2],
                },
                'announcement': [7, 14],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = ContactStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_scheduled_eamil_stat(self):
        today = datetime.today()

        this_month = datetime(year=today.year, month=today.month, day=1)
        one_month_ago = this_month - relativedelta(months=1)
        two_month_ago = this_month - relativedelta(months=2)

        company = CompanyFactory.create()
        user = AppUserFactory(company=company, user_service_id='user_5461398165')
        with freeze_time(two_month_ago):
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=two_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=two_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=two_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=two_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=two_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=two_month_ago)

        with freeze_time(one_month_ago):
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=one_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=one_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=one_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=one_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=one_month_ago)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=one_month_ago)

        with freeze_time(this_month):
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=this_month)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=1, sender=user, sent_date=this_month)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=2, sender=user, sent_date=this_month)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=this_month)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=this_month)
            ScheduledEmailFactory(company=company, scheduled_email_send_type_id=3, sender=user, sent_date=this_month)

        # target_year_month 未指定時は先月が集計対象になる
        data = {
            'company_id': company.id,
            'is_target_scheduled_email': 'true',
        }
        expected = {
            'labels': ['{year}/{month}'.format(
                year=one_month_ago.year,
                month=one_month_ago.month,
            )],
            'values': {
                'jobs': [1],
                'personnel': [3],
                'announcement': [2],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = ScheduledEmailStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)

        # target_year_month を指定すると過去の月を対象にできる
        data = {
            'company_id': company.id,
            'target_year_month': two_month_ago.strftime("%Y/%m"),
            'is_target_scheduled_email': 'true',
        }
        expected = {
            'labels': [
                '{year}/{month}'.format(
                    year=two_month_ago.year,
                    month=two_month_ago.month,
                ),
                '{year}/{month}'.format(
                    year=one_month_ago.year,
                    month=one_month_ago.month,
                ),
            ],
            'values': {
                'jobs': [3, 1],
                'personnel': [2, 3],
                'announcement': [1, 2],
            }
        }
        response = self.client.post(self.base_url, data=data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        actual = ScheduledEmailStats(company_id=company.id).get_stats()
        self.assertDictEqual(expected, actual)
