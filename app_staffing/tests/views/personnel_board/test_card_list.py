from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.board.constants import CARD_LIST_ARRAY
from app_staffing.board.models import PersonnelCardList
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD

class TestCardListAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/card_list.json',
        'app_staffing/tests/fixtures/personnel_board/card.json',
        'app_staffing/tests/fixtures/personnel_board/assignee.json',
    ]

    base_url = '/app_staffing/board/personnel/lists'
    card_list_id_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_list_id_2 = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'

    card_id_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_list_api(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 7)

        for index in range(len(response.data)):
            self.assertEqual(response.data[index]['title'], CARD_LIST_ARRAY[index])

    def test_create_list_board(self):
        response = self.client.post(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        card_list = PersonnelCardList.objects.all().count()
        self.assertEqual(card_list, 7)

    def test_archive_card_list_board(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_list_id_2}/archive', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        obj = PersonnelCardList.objects.get(pk=self.card_list_id_2).personnel_items.last()
        self.assertEqual(obj.is_archived, True)

    def test_move_card_list_board(self):
        count_card_list_1 = PersonnelCardList.objects.get(pk=self.card_list_id_1).personnel_items.all().count()
        count_card_list_2 = PersonnelCardList.objects.get(pk=self.card_list_id_2).personnel_items.all().count()
        response = self.client.patch(path=f'{self.base_url}/{self.card_list_id_2}/move', data={'listId': self.card_list_id_1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        count_card_list = PersonnelCardList.objects.get(pk=self.card_list_id_1).personnel_items.all().count()
        self.assertEqual((count_card_list_1+count_card_list_2), count_card_list)

    def test_action_card_list_not_allowed(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_list_id_2}/xyz', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_list_card(self):
        url = f"/app_staffing/board/personnel/lists/{self.card_id_1}"
        res = self.client.get(url)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_filter_list_card(self):
        url = f"/app_staffing/board/personnel/lists/{self.card_id_1}"
        data_filter = '?assigneeId= \
            &priority=館 \
            &endLte=2022-01-01 \
            &endGte=2022-01-01 \
            &firstName=a \
            &lastName=ada \
            &lastNameInitial=d \
            &firstNameInitial=c \
            &operatePeriodGte=2022-01-01 \
            &operatePeriodLte=2022-01-01 \
            &affiliation=dad \
            &skill=abc abcc \
            &cardListId= \
            &gender=male \
            &trainStation=dada \
            &priceLte=123456789 \
            &priceGte=1000000000 \
            &parallel=asdasd \
            &request=ada \
            &image=true \
            &skillSheet=false \
            &contactHigherId= \
            &contactLowerId= \
            &organizationHigher=test1 \
            &organizationLower=test2 \
            &description=da \
            &contractStart= \
            &contractEnd= \
            &contractPriceLte=2000 \
            &contractPriceGte=1000'
        res = self.client.get(url + data_filter)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_filter_list_card_with_assignee(self):
        url = f"/app_staffing/board/personnel/lists/{self.card_id_1}"
        user_assign_id = '6dba67ff-4bb7-4210-afec-f6fbec1c5fcb'
        data_filter = f'?assigneeId={user_assign_id}'
        res = self.client.get(url + data_filter)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        user_assign_res_data_id = str(res.data['results'][0]['assignees'][0]['id'])
        self.assertEqual(user_assign_res_data_id, user_assign_id)

    def test_filter_list_card_with_affiliation(self):
        url = f"/app_staffing/board/personnel/lists/{self.card_id_1}"
        user_assign_id = '6dba67ff-4bb7-4210-afec-f6fbec1c5fcb'
        data_affiliation = 'proper'
        data_filter = f'?assigneeId={user_assign_id}&affiliation={data_affiliation}'
        res = self.client.get(url + data_filter)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        user_assign_res_data_id = str(res.data['results'][0]['assignees'][0]['id'])
        self.assertEqual(user_assign_res_data_id, user_assign_id)
        affiliation_res_data = str(res.data['results'][0]['affiliation'])
        self.assertEqual(affiliation_res_data, data_affiliation)

    def test_filter_list_card_with_affiliation(self):
        url = f"/app_staffing/board/personnel/lists/{self.card_id_1}"

        user_assign_id = '6dba67ff-4bb7-4210-afec-f6fbec1c5fcb'
        card_list_id = '185c2a9b-ec13-4194-bd85-386de64f5ee4'

        data_filter = f'?assigneeId={user_assign_id} \
            &cardListId={card_list_id}'
        res = self.client.get(url + data_filter)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        user_assign_res_data_id = str(res.data['results'][0]['assignees'][0]['id'])
        self.assertEqual(user_assign_res_data_id, user_assign_id)
