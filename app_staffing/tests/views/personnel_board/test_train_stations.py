from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD


class TestTrainstationListAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/train_station_list.json',
    ]
    base_url = f"/app_staffing/board/personnel/train_stations"

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_list_api(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 30)

    def test_filter_list_api(self):
        data= {
            "name": "大沼公園",
        }
        response = self.client.get(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['id'], 9)
        self.assertEqual(response.data['results'][0]['name'], '大沼公園')
        self.assertEqual(response.data['results'][0]['location_key'], 'wants_location_hokkaido_japan')
        self.assertEqual(response.data['results'][0]['area'], '北海道')
        self.assertEqual(len(response.data['results']), 1)
