from app_staffing.tests.factories.user import AppUserFactory
from app_staffing.tests.views.personnel_board import PersonnelBoardTestBase
from app_staffing.board.models import Personnel, PersonnelAssignee
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, ANOTHER_TENANT_NORMAL_USER_NAME, \
    ANOTHER_TENANT_NORMAL_USER_PASSWORD
from rest_framework.test import APITestCase
from app_staffing.board.exceptions import PersonnelCannotDeleteDueToContractException

from rest_framework import status


class CardOrderUpdateAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/card_list.json',
        'app_staffing/tests/fixtures/personnel_board/card.json',
    ]

    base_url = '/app_staffing/board/personnel/cards'

    card_list_id_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_list_id_2 = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'

    card_id_1_card_list_1_order_0 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_id_2_card_list_1_order_1 = '49d343a3-9520-43ec-aa61-46ffd7729af8'
    card_id_3_card_list_1_order_2 = '7fde8493-8aa4-41f6-808f-1ec6c9d4c25d'
    card_id_4_card_list_2_order_0 = '171ec336-a0b0-4a0a-8c48-0b7ab9148f73'
    card_id_5_card_list_2_order_1 = 'deb03711-5965-4592-aa8c-f8e277e5a8d8'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_move_card_from_card_list_1_to_card_list_2(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        }, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        cards = Personnel.objects.filter(card_list_id=self.card_list_id_2).order_by('order')
        self.assertEqual(str(cards[0].id), self.card_id_4_card_list_2_order_0)
        self.assertEqual(str(cards[1].id), self.card_id_2_card_list_1_order_1)
        self.assertEqual(str(cards[2].id), self.card_id_5_card_list_2_order_1)

    def test_move_card_from_top_to_bottom(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_1_card_list_1_order_0}/position', data={
            'list_id': self.card_list_id_1,
            'position': 2
        }, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        cards = Personnel.objects.filter(card_list_id=self.card_list_id_1).order_by('order')
        self.assertEqual(str(cards[0].id), self.card_id_2_card_list_1_order_1)
        self.assertEqual(str(cards[1].id), self.card_id_1_card_list_1_order_0)
        self.assertEqual(str(cards[2].id), self.card_id_3_card_list_1_order_2)

    def test_move_card_from_bottom_to_top(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_3_card_list_1_order_2}/position', data={
            'list_id': self.card_list_id_1,
            'position': 0
        }, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        cards = Personnel.objects.filter(card_list_id=self.card_list_id_1).order_by('order')
        self.assertEqual(str(cards[0].id), self.card_id_3_card_list_1_order_2)
        self.assertEqual(str(cards[1].id), self.card_id_1_card_list_1_order_0)
        self.assertEqual(str(cards[2].id), self.card_id_2_card_list_1_order_1)

    def test_another_user_not_same_company_can_not_move_card(self):
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class CardActionApiTestView(PersonnelBoardTestBase):

    def setUp(self):
        super().setUp()
        self.url = f"/app_staffing/board/personnel/cards"
        self.data = dict(
            list_id=self.card_list.id,
            first_name_initial="f",
            last_name_initial="l"
        )

    def test_create_card_successful(self):
        res = self.client.post(self.url, data=self.data, format="json")
        cards = Personnel.objects.all()
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(res.data.get("id"))
        self.assertEqual(cards.count(), 2)
        self.assertEqual(cards[1].first_name_initial, self.data.get("first_name_initial"))
        self.assertEqual(cards[1].last_name_initial, self.data.get("last_name_initial"))

    def test_update_card_successful(self):
        url = f"/app_staffing/board/personnel/cards/{self.card.id}"
        user = AppUserFactory(
            username='Another User',
            first_name='User',
            last_name='Another',
            email='another@example.com'
        )
        data = dict(
            first_name="updated first name",
            last_name="updated last name",
            age=22,
            gender="female",
            affiliation="freelancer",
            assignees=[user.id]
        )
        res = self.client.patch(url, data=data, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        cards = Personnel.objects.filter(id=self.card.id)
        assignees = PersonnelAssignee.objects.filter(card_id=self.card.id).count()
        self.assertEqual(assignees, 1)
        self.verify(
            res, cards, input_data=data,
            expected_keys=["first_name", "last_name", "age", "gender", "affiliation"]
        )

    def test_update_card_wrong_when_invalid_data(self):
        url = f"/app_staffing/board/personnel/cards/{self.card.id}"
        data = dict(
            first_name="updated first name",
            last_name="updated last name",
            age=22,
            gender="female",
            affiliation="invalid"
        )
        res = self.client.patch(url, data=data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_card_successful(self):
        url = f"/app_staffing/board/personnel/cards/{self.card.id}"
        res = self.client.delete(url)
        cards = Personnel.objects.all()
        self.assertEqual(res.status_code, 204)
        self.assertEqual(cards.count(), 1)

    def test_delete_card_fail_due_to_contract(self):
        self.mock_project_contract()
        url = f"/app_staffing/board/personnel/cards/{self.card.id}"
        res = self.client.delete(url)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data.get("detail"), PersonnelCannotDeleteDueToContractException.default_detail)
