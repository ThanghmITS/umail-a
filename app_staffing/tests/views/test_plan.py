from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from django.conf import settings

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID
from app_staffing.tests.helpers.mock_payjp_api import MockingPayjpAPIHelper
from app_staffing.models import Plan, PlanMaster, CompanyAttribute, PlanPaymentError, PurchaseHistory, User, CardPaymentError
from app_staffing.exceptions.payment import PaymentCardNotRegisteredException

import payjp
from unittest.mock import Mock, patch, MagicMock

from datetime import datetime, timedelta

from django.core import mail

class PlanListViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 1
        data = [{
            'amount': 33000,
        }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')

    @classmethod
    def mock_Charge_create_card_declined(cls, api_key=None, payjp_account=None, headers=None, **params):
        raise payjp.error.CardError('ERROR', None,'card_declined', json_body={'error':{'code':'card_declined'}})

    def setUp(self):
        user = User.objects.get(username=TEST_USER_NAME)
        user.user_role_id = 6
        user.save()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
    
    def test_get(self):
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5
        )
        url = api_reverse('plan')
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_get.data), 6)
        self.assertEqual(response_get.data['plan_master_id'], 1)
        self.assertEqual(response_get.data['user_registration_limit'], 5)
        self.assertEqual(response_get.data['default_user_count'], 5)
        self.assertEqual(response_get.data['current_user_count'], 6)
    
    def test_get_trial(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5
        )
        url = api_reverse('plan')
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_get.data), 6)
        self.assertEqual(response_get.data['plan_master_id'], None)
        self.assertEqual(response_get.data['user_registration_limit'], 5)
        self.assertEqual(response_get.data['default_user_count'], 5)
        self.assertEqual(response_get.data['current_user_count'], 6)

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_post(self):
        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        now = datetime.now()

        url = api_reverse('plan')
        response_post = self.client.post(url, data={'plan_master_id': 1})
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Plan.objects.filter(company_id=DEFAULT_COMPANY_ID,plan_master_id=1,is_active=True).count(), 1)

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]プラン購入完了のご連絡', email.subject)
        self.assertIn('プランの購入が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        self.assertEquals(company_attribute.user_registration_limit, 5)

        # 請求に成功したかの確認
        charges = payjp.Charge.all(customer=DEFAULT_COMPANY_ID, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 33000)

        purchase_histories = PurchaseHistory.objects.filter(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 33000)

        # 使用した顧客情報を削除
        customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", MagicMock(side_effect=[payjp.error.CardError('ERROR', None,'card_declined', json_body={'error':{'code':'card_declined'}}), None]))
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_post_with_backup(self):
        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)

        # 支払い時にエラーを起こすカードを登録
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass

        # 有効なカードを登録
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass

        now = datetime.now()

        url = api_reverse('plan')
        response_post = self.client.post(url, data={'plan_master_id': 1})
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Plan.objects.filter(company_id=DEFAULT_COMPANY_ID,plan_master_id=1,is_active=True).count(), 1)

        self.assertEqual(len(mail.outbox), 2)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]プラン購入完了のご連絡', email.subject)
        self.assertIn('プランの購入が完了いたしましたのでご連絡いたします。', email.message().as_string())
        email = mail.outbox[1]
        self.assertEqual('[コモレビ] 予備クレジットカードでの決済完了のご連絡', email.subject)
        self.assertIn('予備クレジットカードでの決済が行われましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        self.assertEquals(company_attribute.user_registration_limit, 5)

        # 請求に成功したかの確認
        charges = payjp.Charge.all(customer=DEFAULT_COMPANY_ID, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 33000)

        purchase_histories = PurchaseHistory.objects.filter(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 33000)

        # 使用した顧客情報を削除
        customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", mock_Charge_create_card_declined)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_post_failed(self):
        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)

        # 支払い時にエラーを起こすカードを登録
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass

        # 支払い時にエラーを起こすカードを登録
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass

        now = datetime.now()

        url = api_reverse('plan')
        response = self.client.post(url, data={'plan_master_id': 1})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), settings.PAYJP_CHARGE_ERROR_MESSAGES['card_declined'])
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

        purchase_histories = PurchaseHistory.objects.filter(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(len(purchase_histories), 0)

        # 使用した顧客情報を削除
        customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_unauth_user_cannot_post(self):
        self.client.logout()

        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        url = api_reverse('plan')
        response_post = self.client.post(url, data={'plan_master_id': 1})
        self.assertEqual(response_post.status_code, status.HTTP_401_UNAUTHORIZED)

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_no_content)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_card_not_registered(self):
        url = api_reverse('plan')
        response_post = self.client.post(url, data={'plan_master_id': 1})
        self.assertEqual(response_post.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_post.data["detail"], PaymentCardNotRegisteredException.default_detail)
        self.assertEqual(Plan.objects.filter(company_id=DEFAULT_COMPANY_ID,plan_master_id=1,is_active=True).count(), 0)

class AddUserCountViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 1
        data = [{
            'amount': 3000,
        }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')

    def mock_Customer_delete(self, **params):
        return

    def setUp(self):
        user = User.objects.get(username=TEST_USER_NAME)
        user.user_role_id = 6
        user.save()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_post(self):
        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        now = datetime.now()

        # トライアル期間中はユーザー追加はできない
        company_attribute = CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(now + timedelta(days=2)).date()
        )

        url = api_reverse('add_user_count')
        response_get = self.client.post(url)
        self.assertEqual(response_get.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

        # トライアル終了していれば追加可能
        company_attribute.trial_expiration_date = (now - timedelta(days=2)).date()
        company_attribute.save()

        # プラン情報の作成
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=now.date()
        )

        url = api_reverse('add_user_count')
        response_get = self.client.post(url)
        self.assertEqual(response_get.status_code, status.HTTP_201_CREATED)

        # ユーザ数が加算されていることの確認
        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(company_attribute.user_registration_limit, 6)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]ユーザー上限購入完了のご連絡', email.subject)
        self.assertIn('ユーザー上限の購入が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        # 請求に成功したかの確認
        payjp.api_key = settings.PAYJP_API_KEY
        charges = payjp.Charge.all(customer=DEFAULT_COMPANY_ID, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 3000)

        purchase_histories = PurchaseHistory.objects.filter(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 3300)

        # 使用した顧客情報を削除
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)    
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def test_unauth_user_cannot_post(self):
        self.client.logout()

        # 顧客情報とカード情報の作成
        payjp.api_key = settings.PAYJP_API_KEY
        try:
            customer = payjp.Customer.retrieve(DEFAULT_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.create(id=DEFAULT_COMPANY_ID)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        now = datetime.now()

        # トライアル期間中はユーザー追加はできない
        company_attribute = CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(now - timedelta(days=2)).date()
        )

        # プラン情報の作成
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=now.date()
        )

        url = api_reverse('add_user_count')
        response_get = self.client.post(url)
        self.assertEqual(response_get.status_code, status.HTTP_401_UNAUTHORIZED)

class RemoveUserCountViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def setUp(self):
        user = User.objects.get(username=TEST_USER_NAME)
        user.user_role_id = 6
        user.save()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
    
    def test_post_user_limit_cannot_delete(self):
        # プラン情報の作成
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=6
        )

        url = api_reverse('remove_user_count')
        response_post = self.client.post(url)
        self.assertEqual(response_post.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response_post.data['detail']), 'ユーザー登録上限数の削除に失敗しました。')

        # 最大ユーザー登録数がアクティブユーザーの総数を下回わらない
        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(company_attribute.user_registration_limit, 6)
        self.assertEqual(len(mail.outbox), 0)
        
    def test_post(self):
        # プラン情報の作成
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=7
        )

        url = api_reverse('remove_user_count')
        response_post = self.client.post(url)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)

        # ユーザ数が減算されていることの確認
        company_attribute = CompanyAttribute.objects.get(company_id=DEFAULT_COMPANY_ID)
        self.assertEqual(company_attribute.user_registration_limit, 6)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]ユーザー上限解約完了のご連絡', email.subject)
        self.assertIn('ユーザー上限の解約が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        # プランに設定されているデフォルト値以下には減らせない
        response_post = self.client.post(url)
        self.assertEqual(response_post.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response_post.data['detail']), 'ユーザー登録上限数の削除に失敗しました。')

    def test_unauth_user_cannot_post(self):
        self.client.logout()

        # プラン情報の作成
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=6
        )

        url = api_reverse('remove_user_count')
        response_post = self.client.post(url)
        self.assertEqual(response_post.status_code, status.HTTP_401_UNAUTHORIZED)

class PlanSummaryViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
    
    def test_get(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(datetime.now() + timedelta(days=2)).date()
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 0)
        self.assertEqual(response_get.data['remaining_days'], '2')
    
    def test_get_trial_expired(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(datetime.now() - timedelta(days=2)).date()
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 0)
        self.assertEqual(response_get.data['remaining_days'], None)
    
    def test_get_trial_expire_today(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=datetime.now().date()
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 0)
        self.assertEqual(response_get.data['remaining_days'], '0')
    
    def test_get_plan_registered(self):
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=(datetime.now() + timedelta(days=31)).date()
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=None
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 1)
        self.assertEqual(response_get.data['remaining_days'], '40')
        self.assertEqual(response_get.data['payment_error_exists'], False)
    
    def test_get_plan_payment_error(self):
        plan = Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=datetime.now().date()
        )
        PlanPaymentError.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan=plan
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=None
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 1)
        self.assertEqual(response_get.data['remaining_days'], '9')
        self.assertEqual(response_get.data['payment_error_exists'], True)
    
    def test_get_plan_payment_error_over_postponement(self):
        plan = Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=(datetime.now() - timedelta(days=10)).date()
        )
        PlanPaymentError.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan=plan
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=None
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 1)
        self.assertEqual(response_get.data['remaining_days'], None)
        self.assertEqual(response_get.data['payment_error_exists'], True)

    def test_get_card_payment_error(self):
        plan = Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=datetime.now().date()
        )
        CardPaymentError.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan=plan,
            card_id="main_card_id",
            card_type="main"
        )
        CardPaymentError.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan=plan,
            card_id="sub_card_id",
            card_type="sub"
        )
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=None
        )
        url = api_reverse('plan_summary')
        response_get = self.client.get(url)

        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertEqual(response_get.data['plan_id'], 1)
        self.assertEqual(response_get.data['remaining_days'], '9')
        self.assertEqual(list(response_get.data['payment_error_card_ids']), ["main_card_id", "sub_card_id"])
