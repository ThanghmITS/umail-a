from app_staffing.models.user import User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID
from app_staffing.models import Company
from django.core import mail

class AccountViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/account/users.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_delete(self):
        url = api_reverse('account')
        response_delete = self.client.delete(url)
        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(Company.objects.get(id=DEFAULT_COMPANY_ID).deactivated_time)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]退会完了のご連絡', email.subject)
        self.assertIn('モレビの退会が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())
