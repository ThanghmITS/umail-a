from schematics.types import (BooleanType, IntType, StringType, UUIDType, EmailType,
                              UTCDateTimeType, ListType, ModelType, DateType, DictType)
from schematics import Model
from app_staffing.tests.views.validators.preferences import IsContactPreference


# Validators for API View tests.
class IsLocation(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)
    country = StringType(required=True)
    deleted_at = UTCDateTimeType(required=False)


class IsCompany(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)
    domain_name = StringType(required=True)
    address = StringType(required=False)
    building = StringType(required=False)
    capital_man_yen = IntType(required=True)
    establishment_date = DateType(required=False)
    settlement_month = IntType(required=False)
    has_invoice_system = BooleanType(required=False)
    has_haken = BooleanType(required=False)
    has_distribution = BooleanType(required=False)
    capital_man_yen_required_for_transactions = IntType(required=False)
    establishment_year = IntType(required=False)
    exceptional_organizations = ListType(field=StringType, required=False)
    exceptional_organization_names = ListType(field=StringType, required=False)
    has_p_mark_or_isms = BooleanType(required=False)
    p_mark_or_isms = BooleanType(required=False)
    invoice_system = BooleanType(required=False)
    haken = BooleanType(required=False)
    employee_number = StringType(required=False)
    contract = BooleanType(required=False)
    score = IntType(required=False)
    modified_user__name = StringType(required=False)
    modified_time = UTCDateTimeType(required=False)


class IsContactSummary(Model):
    id = UUIDType(required=True)
    last_name = StringType(required=True)
    first_name = StringType(required=False)
    display_name = StringType(required=True)
    email = EmailType(required=True)
    position = StringType(required=True)
    department = StringType(required=True)

class IsCommentSummary(Model):
    created_user__name = StringType(required=True)
    created_time = StringType(required=True)
    content = StringType(required=True)

class IsOrganizationBranchSummary(Model):
    name = StringType(required=True)
    address = StringType(required=False)
    building = StringType(required=False)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    fax1 = StringType(required=False)
    fax2 = StringType(required=False)
    fax3 = StringType(required=False)

class IsOrganizationSummary(Model):
    id = UUIDType(required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    corporate_number = IntType(required=False)  # Temporally disabling for migrations
    name = StringType(required=True)
    category = StringType(required=True)
    employee_number = StringType(required=True)
    contract = BooleanType(required=False)
    country = StringType(required=True)
    address = StringType(required=False)  # Temporally disabling for migrations
    building = StringType(required=False)
    domain_name = StringType(required=False)
    settlement_month = IntType(required=False)
    capital_man_yen = IntType(required=False)  # Temporally disabling for migrations
    capital_man_yen_required_for_transactions = IntType(required=False)  # Temporally disabling for migrations
    establishment_year = IntType(required=False)  # Temporally disabling for migrations
    score = IntType(required=False)
    is_blacklisted = BooleanType(required=True)
    old_id = IntType(required=False)  # Need for migration process.
    comments = ListType(field=ModelType(model_spec=IsCommentSummary), required=True)
    establishment_date = DateType(required=False)
    is_ignored = BooleanType(required=False)
    has_p_mark_or_isms = BooleanType(required=False)
    p_mark_or_isms = BooleanType(required=False)
    has_invoice_system = BooleanType(required=False)
    has_haken = BooleanType(required=False)
    has_distribution = BooleanType(required=False)
    invoice_system = BooleanType(required=False)
    haken = BooleanType(required=False)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    fax1 = StringType(required=False)
    fax2 = StringType(required=False)
    fax3 = StringType(required=False)
    branches = ListType(field=ModelType(model_spec=IsOrganizationBranchSummary), required=True)

class IsOrganizationBranch(Model):
    id = UUIDType(required=True)
    organization = UUIDType(required=True)
    name = StringType(required=True)
    address = StringType(required=False)
    building = StringType(required=False)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    fax1 = StringType(required=False)
    fax2 = StringType(required=False)
    fax3 = StringType(required=False)

class IsOrganization(Model):
    id = UUIDType(required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    corporate_number = IntType(required=False)  # Temporally disabling for migrations
    name = StringType(required=True)
    category = StringType(required=True)
    employee_number = StringType(required=True)
    contract = BooleanType(required=False)
    country = StringType(required=True)
    address = StringType(required=False)  # Temporally disabling for migrations
    building = StringType(required=False)
    domain_name = StringType(required=False)
    settlement_month = IntType(required=False)
    capital_man_yen = IntType(required=False)  # Temporally disabling for migrations
    capital_man_yen_required_for_transactions = IntType(required=False)  # Temporally disabling for migrations
    establishment_year = IntType(required=False)  # Temporally disabling for migrations
    score = IntType(required=False)
    is_blacklisted = BooleanType(required=True)
    old_id = IntType(required=False)  # Need for migration process.
    related_contacts = ListType(field=ModelType(model_spec=IsContactSummary), required=True)  # Read only set.
    establishment_date = DateType(required=False)
    has_p_mark_or_isms = BooleanType(required=False)
    p_mark_or_isms = BooleanType(required=False)
    has_invoice_system = BooleanType(required=False)
    has_haken = BooleanType(required=False)
    has_distribution = BooleanType(required=False)
    invoice_system = BooleanType(required=False)
    haken = BooleanType(required=False)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    fax1 = StringType(required=False)
    fax2 = StringType(required=False)
    fax3 = StringType(required=False)
    organization_branches = ListType(field=ModelType(model_spec=IsOrganizationBranch), required=True)

class IsComment(Model):
    id = UUIDType(required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_user__avatar = StringType(required=False, default=None)
    created_time = UTCDateTimeType(required=True)
    content = StringType(required=True)
    is_important = BooleanType(required=True)
    modified_user = UUIDType()
    modified_user__name = StringType()
    modified_time = UTCDateTimeType()
    has_subcomment = BooleanType()
    deleted_at = UTCDateTimeType()
    parent = UUIDType()
    total_sub_comment = IntType()
    sub_comment_users_avatar = ListType(field=StringType, required=False)
    parent_content = StringType()
    parent_created_user_name = StringType()
    parent_modified_user_name = StringType()
    parent_created_time = StringType()
    parent_modified_time = StringType()


class IsContact(Model):
    id = UUIDType(required=True)
    last_name = StringType(required=True)
    first_name = StringType(required=False)
    display_name = StringType(required=True)
    email = EmailType(required=True)
    position = StringType(required=False)
    department = StringType(required=False)
    organization = UUIDType(required=True)
    organization_branch = UUIDType(required=False)
    organization__name = StringType(required=True)
    organization_branch__name = StringType(required=True)
    organization__is_blacklisted = BooleanType(required=True)
    score = IntType(required=False)
    staff = UUIDType(required=False)
    staff__name = StringType(required=True)
    last_visit = DateType(required=True)
    tags = ListType(required=True, field=UUIDType(required=True))
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    old_id = IntType(required=False)  # Need for migration process.
    cc_mails = ListType(required=True, field=StringType(required=True))
    comments = ListType(field=ModelType(model_spec=IsCommentSummary), required=True)
    contactjobtypepreferences = ListType(required=True, field=StringType(required=True))
    contactjobskillpreferences = ListType(required=True, field=StringType(required=True))
    contactpersonneltypepreferences = ListType(required=True, field=StringType(required=True))
    contactpersonnelskillpreferences = ListType(required=True, field=StringType(required=True))
    contactpreference = ModelType(IsContactPreference, required=True)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    tag_objects = ListType(DictType(StringType(required=False), required=False), required=False)
    category = StringType(required=False)
    is_ignored = BooleanType(required=True)


class IsOrganizationRegisterTrend(Model):
    labels = ListType(field=StringType, required=True)
    counts = ListType(field=IntType, required=True)


class IsOrganizationTrend(Model):
    register = ModelType(IsOrganizationRegisterTrend, required=True)


class IsOrganizationStats(Model):
    trend = ModelType(IsOrganizationTrend, required=True)


class IsTag(Model):
    id = UUIDType(required=True)
    value = StringType(required=True)
    internal_value = StringType(required=True)
    color = StringType(required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    is_skill = BooleanType(required=True)
