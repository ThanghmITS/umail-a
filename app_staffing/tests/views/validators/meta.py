from schematics.types import IntType
from schematics import Model


class IsVersionInfo(Model):
    revision = IntType(required=True)
