
from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.tests.views.base import APICRUDTestMixin, TestQueryParamErrorMixin
from app_staffing.tests.views.validators.organizations import IsContact

from app_staffing.exceptions.organization import (
    CannotDeleteContactException,
    CannotUpdateContactException
)


class ContactScheduledEmailViewTestCase(APICRUDTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/organizations.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contacts.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_cc.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_preferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/type_preferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/skill_preferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/scheduled_emails.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_jobskillpreferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_jobtypepreferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_personnelskillpreferences.json',
        'app_staffing/tests/fixtures/contactScheduledEmail/contact_personneltypepreferences.json',
    ]
    base_url = '/app_staffing/contacts'
    resource_id = '78630567-72ce-49a3-84e6-8947b5b057f3'
    resource_validator_class = IsContact

    skip_tests = ('put', 'post', 'patch', 'delete', 'list')

    def test_bulk_delete_contact_with_scheduled_email_sending(self):
        data = {'source': [self.resource_id]}
        response = self.client.delete(path='/app_staffing/contacts', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeleteContactException().default_detail)

    def test_delete_contact_with_scheduled_email_sending(self):
        url = self.base_url + '/' + self.resource_id
        response = self.client.delete(path=url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CannotDeleteContactException().default_detail)

    def test_update_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_name_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample1 user",
            "first_name": "sample1 name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_mail_to_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample3@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_mail_cc_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc3@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_jobskill_preference_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": False
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_jobtype_preference_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": False
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_personnelskill_preference_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": False,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_personneltype_preference_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": False
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_preference_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "749303ca-7701-463e-83a8-8a5a35df1646",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": False,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 4,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)

    def test_update_company_contact_with_scheduled_email_sending(self):
        data = {
            "last_name": "A sample user",
            "first_name": "sample name 1",
            "email": "sample@example.com",
            "tel": "012-7890-3456",
            "position": "test position",
            "department": "test department",
            "staff": "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
            "last_visit": "2019-04-08",
            "organization": "5468c29f-7b35-4540-b9d0-c1807d945734",
            "cc_mails": ["contact_cc1@example.com"],
            "score": 1,
            "tags": ["3318d509-d56b-4e19-b2ca-201bbc5f4218", "4418d509-d56b-4e19-b2ca-201bbc5f4218", "5518d509-d56b-4e19-b2ca-201bbc5f4218"],
            "jobtypepreference": {
                "dev_designer": True,
                "dev_front": True,
                "dev_server": True,
                "dev_pm": True,
                "dev_other": True,
                "infra_server": True,
                "infra_network": True,
                "infra_security": True,
                "infra_database": True,
                "infra_sys": True,
                "infra_other": True,
                "other_eigyo": True,
                "other_kichi": True,
                "other_support": True,
                "other_other": True
            },
            "jobskillpreference": {
                "dev_youken": True,
                "dev_kihon": True,
                "dev_syousai": True,
                "dev_seizou": True,
                "dev_test": True,
                "infra_youken": True
            },
            "personneltypepreference": {
                "dev_designer": True,
                "infra_server": True
            },
            "personnelskillpreference": {
                "dev_youken": True,
            },
            "preference": {
                "wants_location_hokkaido_japan": True,
                "wants_location_touhoku_japan": True,
                "wants_location_kanto_japan": True,
                "wants_location_kansai_japan": True,
                "wants_location_chubu_japan": True,
                "wants_location_kyushu_japan": True,
                "wants_location_other_japan": True,
                "wants_location_chugoku_japan": True,
                "wants_location_shikoku_japan": True,
                "wants_location_toukai_japan": True,
                "job_koyou_proper": True,
                "job_koyou_free": True,
                "job_syouryu": 1,
                "personnel_syouryu": 1,
            },
        }
        url = self.base_url + '/' + self.resource_id
        response = self.client.patch(
            path=url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(response.data['detail']), CannotUpdateContactException().default_detail)
