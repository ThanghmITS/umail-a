from datetime import datetime
from unittest import skip
from freezegun import freeze_time
from dateutil.parser import parse

from django.core.files.uploadedfile import SimpleUploadedFile  # For file mocking.
from django.test import override_settings
from app_staffing.exceptions.scheduled_email import EmailDomainBlocked
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APITestCase

from app_staffing.exceptions.base import (BulkOperationLimitException, RequestTimeoutException, LoginIncorrectException)
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.models import Email, EmailComment, User
from app_staffing.models.email import INBOUND, Email, EmailAttachment
from app_staffing.tests.factories.email import (SharedEmailAttachmentFactory,  SharedEmailFactory)
from app_staffing.tests.helpers.setup import (MediaFolderSetupMixin, reset_estimators, setup_estimators)
from app_staffing.tests.helpers.skip import skipUnlessIntegrated
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from app_staffing.tests.views.base import (CommentApiTestMixin, MultiTenancyTestMixin, SubTestMixin, TestQueryParamErrorMixin)
from app_staffing.tests.views.validators.emails import (IsEmail, IsEmailDetail, IsLabeledEmail, IsSummarizedEmail, IsUnLabeledEmail)
from app_staffing.tests.views.validators.generals import IsPagenatedResponse
from app_staffing.tests.views.validators.organizations import IsComment

from unittest.mock import Mock, patch
from django.conf import settings

from app_staffing.exceptions.base import BulkOperationLimitException
from app_staffing.utils.email import get_due_date_of_shared_email
from app_staffing.tests.views.validators.recommendations import IsRecommendationResult

FIXTURE_TEST_EMAIL_ID = 'bcda5407-54ad-4637-bd5c-1d830f4e9fb5'


class EmailViewTestCase(APITestCase):
    """A Test case for an API of email object."""

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json'
    ]
    # TODO: Insert Sample data.

    _labeled_email = {
        'mail_id': 99999,
        'from_address': 'test_user@example.com',
        'direction': 'inbound',
        'subject': 'Test Mail Creation (labeled)',
        'body': 'This is an sample of labeled email entity created by an unit test.',
        'received_date': '2018-10-25 14:57:32',
        'attachment_names': 'resume.docs career.xls',
        'category': 'person',
    }

    _unlabeled_email = {
        'mail_id': 11111,
        'from_address': 'test_user@example.com',
        'direction': 'inbound',
        'subject': 'Test Mail Creation (unlabeled)',
        'body': 'This is an sample of unlabeled email entity created by an unit test.',
        'received_date': '2018-10-25 14:57:32',
        'attachment_names': 'resume.docs career.xls',
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    @skip("Requirements will be changed.")
    def test_email_creation_labeled(self):
        # Test with fully qualified data.
        input_data = self._labeled_email

        response = self.client.post(path='/app_staffing/emails', data=input_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        validator = IsLabeledEmail(response.data)
        validator.validate()

    @skip("Requirements will be changed.")
    def test_email_creation_unlabeled(self):
        # Test with fully qualified data.
        input_data = self._unlabeled_email

        response = self.client.post(path='/app_staffing/emails', data=input_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        validator = IsUnLabeledEmail(response.data)
        validator.validate()

    @skip("Requirements will be changed.")
    def test_email_creation_with_success_on_exists_mode(self):
        input_data = self._labeled_email
        response = self.client.post(path='/app_staffing/emails?success_on_exists=True', data=input_data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        validator = IsLabeledEmail(response.data)
        validator.validate()

    @skip("Requirements will be changed.")
    def test_email_list(self):
        response = self.client.get(path='/app_staffing/emails', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data, list))

        for entry in response.data:
            validator = IsEmail(entry)
            validator.validate()

    def test_duplicate_post(self):
        # Test with fully qualified data.
        input_data = self._labeled_email

        response = self.client.post(path='/app_staffing/emails', data=input_data, format='json')
        response = self.client.post(path='/app_staffing/emails', data=input_data, format='json')  # Do it again.

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # TODO: Define an Error response data and assert.

    @skip("Requirements will be changed.")
    def test_duplicate_post_with_success_on_exists_param(self):
        input_data = self._labeled_email

        response = self.client.post(path='/app_staffing/emails', data=input_data, format='json')
        response = self.client.post(path='/app_staffing/emails?success_on_exists=True', data=input_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    @skip("Requirements will be changed.")
    def test_get_email(self):
        """Test a single object retrieval."""
        target_uuid = '90ba907b-3102-4b71-a76d-640a0db3cfdb'  # Make sure that this object is defined in the test fixtures.
        response = self.client.get(path='/app_staffing/emails/{0}'.format(target_uuid))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        validator = IsUnLabeledEmail(response.data)
        validator.validate()

    @skip("Requirements will be changed.")
    def test_delete_email(self):
        """Test of email deletion.
        In this application, emails won't be deleted except under some rare condition, like old data purging,
         so no deletion test is available in a view test currently.
        """
        pass


class SharedEmailViewTestCase(APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_attachments.json'
    ]

    base_url = '/app_staffing/share-mails'

    # Used in TestQueryParamErrorMixin
    filter_params = {
        'from_name': 'Trump',
        'from_address': 'trump@example.com',
        'subject': 'Subject keyword',
        'subject_AND': 'Another subject keyword',
        'text': 'Content keyword',
        'text_AND': 'Another content keyword',
        'has_attachments': False,
        'estimated_category': 'dummy',
        'sent_date_gte': '1970-01-01',
        'sent_date_lte': '2020-12-31',
    }
    TEXT_FILE_BODY = 'This is a test file'

    @classmethod
    def setUpClass(cls):
        """ Assert if fixture data qualifies conditions. """
        super().setUpClass()

        # Assert fixture data.
        cls.shared_email_records = Email.objects.filter(direction=INBOUND, shared=True)
        cls.normal_user_records = Email.objects.all().exclude(direction=INBOUND, shared=True)

        assert (len(cls.shared_email_records) > 0), "No shared email entry found."
        assert (len(cls.normal_user_records) > 0), "This test requires an email that is created by a normal user."

        # Assert the existence of fixtures for a detail view.
        Email.objects.get(id=FIXTURE_TEST_EMAIL_ID)

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        # Mocking attachments
        # Create Actual Attachment Files for the test. (Overriding fixture data)
        self.attachment_instance = EmailAttachment.objects.get(email=FIXTURE_TEST_EMAIL_ID)
        self.attachment_instance.file = SimpleUploadedFile('sample.txt', bytes(self.TEXT_FILE_BODY, encoding='utf-8'))
        self.attachment_instance.save()

    def test_get_email_list(self):
        """Check if the endpoint returns a list of summarized email, only which is created by a batch"""
        url = api_reverse("shared_emails")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Assert data forms.
        IsPagenatedResponse(response.data).validate()
        results = response.data["results"]
        self.assertEqual(len(results), len(self.shared_email_records))

        for entry in results:
            validator = IsSummarizedEmail(entry)
            validator.validate()

    def test_get_email_detail(self):
        response = self.client.get(path='/app_staffing/share-mails/{0}'.format(FIXTURE_TEST_EMAIL_ID))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data, dict))

        validator = IsEmailDetail(response.data)
        validator.validate()

    def test_shared_email_list_comment(self):
        user = User.objects.get(username=TEST_USER_NAME)
        company = user.company
        email = company.email_set.filter(shared=True).first()
        for idx in range(1, 3):
            EmailComment.objects.create(
                company=company,
                email=email,
                content="example comment{index}".format(index=idx),
                is_important=True
            )
        for idx in range(3, 5):
            EmailComment.objects.create(
                company=company,
                email=email,
                content="example comment{index}".format(index=idx),
                is_important=False
            )
        all_email_comments = EmailComment.objects.all()
        self.assertEqual(all_email_comments.count(), 4)
        url = api_reverse('shared_emails')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        commented_shared_emails = list(filter(lambda result: result['id'] == str(email.id), results))
        commented_shared_email = commented_shared_emails[0] if len(commented_shared_emails) else None
        self.assertEqual(len(commented_shared_email['comments']), 2)

    def test_shared_email_list_comment_maximum_comments(self):
        user = User.objects.get(username=TEST_USER_NAME)
        company = user.company
        email = company.email_set.filter(shared=True).first()
        for idx in range(1, 7):
            EmailComment.objects.create(
                company=company,
                email=email,
                content="example comment{index}".format(index=idx),
                is_important=True
            )
        for idx in range(7, 9):
            EmailComment.objects.create(
                company=company,
                email=email,
                content="example comment{index}".format(index=idx),
                is_important=False
            )
        all_email_comments = EmailComment.objects.all()
        self.assertEqual(all_email_comments.count(), 8)
        url = api_reverse('shared_emails')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        commented_shared_emails = list(filter(lambda result: result['id'] == str(email.id), results))
        commented_shared_email = commented_shared_emails[0] if len(commented_shared_emails) else None
        self.assertEqual(len(commented_shared_email['comments']), 5)

    def test_shared_email_list_histories(self):
        sender = User.objects.get(username=TEST_USER_NAME)
        sender_company = sender.company
        shared_email = sender_company.email_set.filter(shared=True).first()
        receiver = User.objects.filter(company=sender_company).exclude(id=sender.id).order_by('?').first()
        post_data = {
            'staff_in_charge_id': str(receiver.id)
        }
        forward_url = api_reverse('shared_email_forward', kwargs={'pk': shared_email.id})
        forward_response = self.client.post(forward_url, data=post_data, format="json")
        self.assertEqual(forward_response.status_code, status.HTTP_200_OK)
        shared_emails_list_url = api_reverse('shared_emails')
        response = self.client.get(shared_emails_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        commented_shared_emails = list(filter(lambda result: result['id'] == str(shared_email.id), results))
        commented_shared_email = commented_shared_emails[0] if len(commented_shared_emails) else None
        histories = commented_shared_email['histories']
        self.assertEqual(len(histories), 1)

    def test_shared_email_list_histories_maximum(self):
        sender = User.objects.get(username=TEST_USER_NAME)
        sender_company = sender.company
        shared_email = sender_company.email_set.filter(shared=True).first()
        receiver = User.objects.filter(company=sender_company).exclude(id=sender.id).order_by('?').first()
        post_data = {
            'staff_in_charge_id': str(receiver.id)
        }
        forward_url = api_reverse('shared_email_forward', kwargs={'pk': shared_email.id})
        for _ in range(8):
            forward_response = self.client.post(forward_url, data=post_data, format="json")
            self.assertEqual(forward_response.status_code, status.HTTP_200_OK)
        shared_emails_list_url = api_reverse('shared_emails')
        response = self.client.get(shared_emails_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.data['results']
        commented_shared_emails = list(filter(lambda result: result['id'] == str(shared_email.id), results))
        commented_shared_email = commented_shared_emails[0] if len(commented_shared_emails) else None
        histories = commented_shared_email['histories']
        self.assertEqual(len(histories), 5)

    def test_bulk_delete_limit_error(self):
        data = { 'source': [
            'sample_uuid_xxxx_yyyy_1',
            'sample_uuid_xxxx_yyyy_2',
            'sample_uuid_xxxx_yyyy_3',
            'sample_uuid_xxxx_yyyy_4',
            'sample_uuid_xxxx_yyyy_5',
            'sample_uuid_xxxx_yyyy_6',
            'sample_uuid_xxxx_yyyy_7',
            'sample_uuid_xxxx_yyyy_8',
            'sample_uuid_xxxx_yyyy_9',
            'sample_uuid_xxxx_yyyy_10',
            'sample_uuid_xxxx_yyyy_11',
        ]}
        response = self.client.delete(path='/app_staffing/share-mails', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], BulkOperationLimitException().default_detail)

class SharedEmailViewOrderingTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailViewOrdering/emails.json',
    ]

    @classmethod
    def setUpClass(cls):
        """Assert fixture conditions for this test."""
        super().setUpClass()
        assert len(Email.objects.all()) >= 3

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_email_list_order(self):
        """Confirm that the order of the email list is descending to sent_date."""
        date_format = '%Y-%m-%dT%H:%M:%SZ'

        response = self.client.get(path='/app_staffing/share-mails')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert data forms.
        IsPagenatedResponse(response.data).validate()

        email_list = response.data["results"]

        for index, entry in enumerate(email_list):
            if index + 1 > len(email_list) - 1:
                return

            self.assertTrue(
                datetime.strptime(email_list[index]['sent_date'], date_format)
                > datetime.strptime(email_list[index + 1]['sent_date'], date_format)
            )


class SharedEmailViewFilterTestCase(SubTestMixin, APITestCase):
    """Test filtering and searching via query parameters.
    Note: In an actual environment, query parameters will be URI encoded. For example,
    1byte space => %20
    2byte space (Japanese) => %E3%80%80
    However, API client for tests supports both URI encoded and not encoded data, so encoding test params is not mandatory.
    """
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailViewFilter/emails.json',
    ]

    EXPECTED_TOTAL_COUNT_OF_EMAIL = 5
    EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_PERSON = 1
    EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_JOB = 1

    base_url = '/app_staffing/share-mails'
    fake_current_date = datetime(2018, 12, 15)

    @classmethod
    def setUpClass(cls):
        """Assert fixture conditions for this test."""
        super().setUpClass()
        assert len(Email.objects.filter(direction=INBOUND, shared=True)) == cls.EXPECTED_TOTAL_COUNT_OF_EMAIL
        assert len(Email.objects.filter(from_name='デルタ カンパニー', direction=INBOUND, shared=True)) == 2
        assert len(Email.objects.filter(estimated_category='person', direction=INBOUND,
                                        shared=True)) == cls.EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_PERSON
        assert len(Email.objects.filter(estimated_category='job', direction=INBOUND,
                                        shared=True)) == cls.EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_JOB

        assert Email.objects.get(from_name='Alpha Inc', direction=INBOUND, shared=True)
        assert Email.objects.get(from_address='staff@beta.com', direction=INBOUND, shared=True)
        assert Email.objects.get(subject='I have an Intel', direction=INBOUND, shared=True)
        assert Email.objects.get(from_name='デルタ カンパニー',
                                 text='文字列の部分一致が正しく検出されるかテストしてください。',
                                 direction=INBOUND,
                                 shared=True)
        assert Email.objects.get(from_name='デルタ カンパニー',
                                 subject='デルタ カンパニーからのもう１つのメール。',
                                 direction=INBOUND,
                                 shared=True)

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_filter_from_name(self):

        test_patterns = [
            ('full', '?from_name=Alpha Inc', 1),
            ('partial', '?from_name=Alpha', 1),
            ('none', '?from_name=', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_filter_from_address(self):

        test_patterns = [
            ('full', '?from_address=staff@beta.com', 1),
            ('partial', '?from_address=beta', 1),
            ('none', '?from_address=', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_filter_subject(self):

        test_patterns = [
            ('full', '?subject=I have an Intel', 1),
            ('partial', '?subject=Intel', 1),
            ('AND search', '?subject_AND=I Intel', 1),
            ('none', '?subject=', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_filter_text(self):

        test_patterns = [
            ('full', '?text=文字列の部分一致が正しく検出されるかテストしてください。', 1),
            ('partial', '?text=正しく検出', 1),
            ('AND search', '?text_AND=文字列 検出', 1),
            ('AND search with a 2byte separator', '?text_AND=文字列　検出', 1),
            ('none', '?text=', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_filter_estimated_category(self):
        test_patterns = [
            ('person', '?estimated_category=person', self.EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_PERSON),
            ('job', '?estimated_category=job', self.EXPECTED_TOTAL_COUNT_OF_EMAIL_OF_JOB),
            ('invalid', '?estimated_category=person job', 0),
            ('none', '?estimated_category=', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_complex_pattern(self):

        test_patterns = [
            ('two match', '?from_name=デルタ カンパニー', 2),
            ('combined param', '?from_name=デルタ カンパニー&subject=【日本語テスト】', 1),
            ('completely nothing', '', self.EXPECTED_TOTAL_COUNT_OF_EMAIL)
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_search(self):
        """
        Check if when we enter several words with space as a separator,
         we will find an email which has both words.
        """
        test_patterns = [
            ('specify field (found)', '?search=文字列&fields=text', 1),
            ('specify field (Not found field)', '?search=WRONG_WORD&fields=text', 0),
            ('specify field (AND condition fulfilled)', '?search=文字列 検出&fields=text', 1),
            ('specify field (AND condition not fulfilled)', '?search=文字列 存在しない文字列&fields=text', 0),
            ('specify multiple field (found)', '?search=文字列&fields=from_name,text', 1),
            ('specify multiple field (not found)', '?search=WRONG_WORD&fields=from_name,text', 0),
            ('specify multiple field (AND condition fulfilled)', '?search=文字列 検出&fields=from_name,text', 1),
            ('specify multiple field (AND condition not fulfilled)', '?search=文字列 WRONG_WORD&fields=from_name,text', 0),
        ]
        with freeze_time(self.fake_current_date):
            self.execute_sub_test(test_patterns)

    def test_invalid_field_search(self):
        response = self.client.get(path='{0}{1}'.format(self.base_url, '?search=文字列&fields=WRONGFIELD'))
        self.assertEqual(response.status_code, 400)


class SharedEmailViewFilterApiTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organizationSearchFilter/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailViewFilter/emails.json',
        'app_staffing/tests/fixtures/sharedEmailViewFilter/contacts.json',
        'app_staffing/tests/fixtures/organizationSearchFilter/organizations.json',
        'app_staffing/tests/fixtures/organizationSearchFilter/exceptional_organizations.json',

    ]

    base_url = '/app_staffing/share-mails'
    fake_current_date = datetime(2018, 12, 15)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_exclusion_company_email(self):
        with freeze_time(self.fake_current_date):
            response = self.client.get(path='/app_staffing/share-mails')

        self.assertEqual(1, response.data["count"])
        self.assertEqual("7a4b6b4d-042c-42c2-9487-e0e0cd19e424", response.data["results"][0]["id"])


class SharedEmailViewMultiTenantTestCase(MultiTenancyTestMixin, APITestCase):
    base_url = '/app_staffing/share-mails'
    test_patterns = ('list', 'get')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        email_for_tenant_1 = SharedEmailFactory(company=cls.company_1)
        email_for_tenant_2 = SharedEmailFactory(company=cls.company_2)
        cls.resource_id_for_tenant_1 = email_for_tenant_1.id
        cls.resource_id_for_tenant_2 = email_for_tenant_2.id


@skipUnlessIntegrated
class EmailRecommendationViewTestCase(APITestCase):
    """A Test case for an API of email object."""

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json'   # This Must contains labeled data in this test.

    ]

    @classmethod
    def setUpClass(cls):
        # We need to standby a trained estimator before running the test.
        super().setUpClass()
        setup_estimators()

    @classmethod
    def tearDown(self):
        reset_estimators()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_related_email(self):
        """Test a single object retrieval."""
        target_uuid = '90ba907b-3102-4b71-a76d-640a0db3cfdb'  # Make sure that this object is defined in the test fixtures.
        response = self.client.get(path='/app_staffing/emails/{0}/related/jobs'.format(target_uuid))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check response format first.
        validator = IsRecommendationResult(response.data)
        validator.validate()


class SharedEmailAttachmentTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_attachments.json'
    ]

    ATTACHMENT_ID = '4ab91419-0705-45cf-8d3f-7a0adb559610'
    MOCK_ATTACHMENT_BODY = 'Mock Data'

    @classmethod
    def setUpTestData(cls):

        # Check Fixture conditions
        cls.test_instance = EmailAttachment.objects.get(pk=cls.ATTACHMENT_ID)

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        # Mocking attachment file
        self.test_instance.file = SimpleUploadedFile('sample.txt', bytes(self.MOCK_ATTACHMENT_BODY, encoding='utf-8'))
        self.test_instance.save()

    def test_download_attachment(self):
        response = self.client.get(
            path=f'/app_staffing/share-mails_attachments/{self.ATTACHMENT_ID}',
            HTTP_ACCEPT='application/octet-stream'
        )
        text = b''.join(response.streaming_content).decode(encoding='utf-8')

        self.assertEqual(self.MOCK_ATTACHMENT_BODY, text)

    def test_delete_is_invalid(self):
        response = self.client.delete(path=f'/app_staffing/share-mails_attachments/{self.ATTACHMENT_ID}')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class SharedEmailAttachmentMultiTenancyTest(MultiTenancyTestMixin, APITestCase):
    base_url = '/app_staffing/share-mails_attachments'
    test_patterns = ('get',)

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        attachment_for_tenant_1 = SharedEmailAttachmentFactory(company=cls.company_1)
        attachment_for_tenant_2 = SharedEmailAttachmentFactory(company=cls.company_2)
        cls.resource_id_for_tenant_1 = attachment_for_tenant_1.id
        cls.resource_id_for_tenant_2 = attachment_for_tenant_2.id


class EmailForwardViewTestCase(MediaFolderSetupMixin, APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_attachments.json'
    ]
    TEXT_FILE_BODY = 'This is a test file'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Check Fixture conditions
        instance = Email.objects.get(pk=FIXTURE_TEST_EMAIL_ID)
        instance.staff_in_charge = None

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

        # Mocking attachments
        # Create Actual Attachment Files for the test. (Overriding fixture data)
        self.attachment_instance = EmailAttachment.objects.get(email=FIXTURE_TEST_EMAIL_ID)
        self.attachment_instance.file = SimpleUploadedFile('sample.txt', bytes(self.TEXT_FILE_BODY, encoding='utf-8'))
        self.attachment_instance.save()

    def tearDown(self):
        self.attachment_instance.file.delete()

    def test_forward_email(self):
        param = {'staff_in_charge_id': '1957ab0f-b47c-455a-a7b7-4453cfffd05e'}
        response = self.client.post(path=f'/app_staffing/share-mails/{FIXTURE_TEST_EMAIL_ID}/forward', data=param, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check the Database record after update.
        self.assertEqual(
            Email.objects.get(pk=FIXTURE_TEST_EMAIL_ID).staff_in_charge.id,
            User.objects.get(username=TEST_USER_NAME).id
        )

        # Note: Checking the sent email content will be executed in a service test.


class SharedEmailSettingViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_default(self):
        response = self.client.get(path='/app_staffing/shared_email_setting')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['allow_self_domain'])

    def test_over_max_length(self):
        data = {
            'from_email': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
            'password': 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc',
        }
        response = self.client.patch(path='/app_staffing/shared_email_setting', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['from_email']), settings.LENGTH_VALIDATIONS['shared_email_setting']['from_email']['message'])


class SharedEmailConnectionViewTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self) -> None:
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_connection_test_fail(self):
        data = { 'from_email': 'hogehoge@example.com', 'password': 'Fugafuga0123%$', 'protocol': 'imap+ssl'}
        mock = Mock(return_value=False)
        with patch('app_staffing.views.email.SharedEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/shared_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_connection_test_fail_exception(self):
        data = { 'from_email': 'hogehoge@example.com', 'password': 'Fugafuga0123%$', 'protocol': 'imap+ssl'}
        mock = Mock(side_effect=Exception('Boom!'))
        with patch('app_staffing.views.email.SharedEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/shared_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_connection_test_success(self):
        data = { 'from_email': 'hogehoge@example.com', 'password': 'Fugafuga0123%$', 'protocol': 'imap+ssl'}
        mock = Mock(return_value=True)
        with patch('app_staffing.views.email.SharedEmailConnectionView.check_server_connection', mock):
            response = self.client.post(path='/app_staffing/shared_email_connection', data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_domain(self):
        domain = 'gmail.com'
        data = { 'from_email': 'daanchoi@gmail.com', 'password': 'Fugafuga0123%$', 'protocol': 'imap+ssl'}
        response = self.client.post(path='/app_staffing/shared_email_connection', data=data, format='json')
        message_error = EmailDomainBlocked(domain)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(message_error.default_detail['detail'], response.data['detail'])

class EmailCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_comments.json',
    ]

    related_resource_id = '90ba907b-3102-4b71-a76d-640a0db3cfdb'
    comment_id = '39037c2e-4f95-47c2-b222-1beddb5ef0c7'
    other_users_comment_id = 'dd466574-10ce-4fef-bb97-7d1d8d806706'

    base_url = f'/app_staffing/share-mails/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}


class EmailSubCommentViewTestCase(CommentApiTestMixin, APITestCase, TestQueryParamErrorMixin):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_comments.json',
    ]

    related_resource_id = '90ba907b-3102-4b71-a76d-640a0db3cfdb'
    comment_id = '39037c2e-4f95-47c2-b222-1beddb6ef0c7'
    other_users_comment_id = 'dd466574-10ce-4fef-bb97-7d1d8d806706'
    updating_comment_id = "39037c2e-4f95-47c2-b222-1beddb7ef0c7"

    base_url = f'/app_staffing/share-mails/{related_resource_id}/comments'
    resource_validator_class = IsComment

    # Override default pagination_params in TestQueryParamErrorMixin
    pagination_params = {}


    def test_post_comment(self):
        new_content = "A new comment"

        response = self.client.post(path=f'{self.base_url}/{self.comment_id}/sub-comments', data={'content': new_content})
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.resource_validator_class(response.data).validate()
        self.assertEqual(response.data.get('content'), new_content)
        self.assertEqual(response.data.get('created_user__name'), TEST_USER_NAME)
        self.assertEqual(str(response.data.get('parent')), self.comment_id)

    def test_get_sub_comments(self):
        response = self.client.get(path=f'{self.base_url}/{self.comment_id}/sub-comments', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_updating_important_sub_comment_content(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_updating_important_sub_comment_content_and_is_important(self):
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_delete_sub_comment(self):
        url = self.base_url + "/" + self.updating_comment_id
        self.assertEqual(EmailComment.objects.filter(id=self.updating_comment_id).count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(EmailComment.objects.filter(id=self.updating_comment_id).count(), 0)

    def test_sub_comment_of_another_user_can_be_updated(self):
        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb9ef0c7"
        post_data = {
            "content": "updated Another User's comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_comment = response.data
        self.assertEqual(updated_comment["content"], post_data["content"])
        self.assertEqual(updated_comment["created_time"], "2019-05-12T16:14:53.526000+09:00")

    def test_comment_of_another_user_can_be_deleted(self):
        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb9ef0c7"
        post_data = {
            "content": "updated Another User's comment",
        }
        self.assertEqual(EmailComment.objects.filter(id="39037c2e-4f95-47c2-b222-1beddb9ef0c7").count(), 1)
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(EmailComment.objects.filter(id="39037c2e-4f95-47c2-b222-1beddb9ef0c7").count(), 0)

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_post(self):
        self.client.logout()
        data = {
            "content": "test comment",
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        post_data = {
            "content": "updated comment",
        }
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb9ef0c7"
        response = self.client.patch(url, data=post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        url = self.base_url + "/" + self.updating_comment_id
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        url = self.base_url + "/39037c2e-4f95-47c2-b222-1beddb9ef0c7"
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class EmailAddressCheckViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.url = api_reverse('email_address_check')

    def test_valid_email_TO(self):
        data = {'email': 'ng.test.example@gmail.com'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'], 'メールアドレス(TO)は有効です。')

    def test_valid_email_CC(self):
        data = {'cc_emails': 'dannv@its-global.vn,ng.test.example@gmail.com'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'], 'メールアドレス(CC)は有効です。')

    def test_invalid_email_TO(self):
        data = {'email': 'test.example1@local.com'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(data['email'], response.data['detail'])

    def test_invalid_email_CC(self):
        data = {'cc_emails': 'ng.test.example@gmail.com,test.example1@local.com,test.example1@abc'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('test.example1@local.com', response.data['detail'])
        self.assertIn('test.example1@abc', response.data['detail'])

    def test_invalid_non_field(self):
        data = {}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], 'メールアドレスを正しく入力してください。')

    def test_invalid_not_email_field(self):
        data = {'mail': 'ng.test.example@gmail.com'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], 'メールアドレスを正しく入力してください。')

    @override_settings(EMAIL_HOST='example.com', SMTP_TIMEOUT=1)
    def test_timeout_exception(self):
        data = {'email': 'ng.test.example@gmail.com'}
        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, RequestTimeoutException().status_code)
        self.assertEqual(str(response.data['detail']), RequestTimeoutException().default_detail)

    @override_settings(EMAIL_HOST_PASSWORD='wrong_password')
    def test_login_incorrect_exception(self):
        if settings.EMAIL_HOST not in ['localhost', '127.0.0.1']:
            data = {'email': 'ng.test.example@gmail.com'}
            response = self.client.post(self.url, data, format='json')
            self.assertEqual(response.status_code, LoginIncorrectException().status_code)
            self.assertEqual(str(response.data['detail']), LoginIncorrectException().default_detail)


class SharedEmailWithExpirationDateApiTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailWithExpirationDate/emails.json',
    ]

    base_url = '/app_staffing/share-mails'
    fake_current_date = datetime(2022, 4, 25)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_list_shared_email_with_expiration_date(self):
        url = api_reverse("shared_emails")
        with freeze_time(self.fake_current_date):
            response = self.client.get(path=url)
            due_date = get_due_date_of_shared_email()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 7)

        # because result is sorted by sent_date (desc) => get sent_date from last record, compare with expiration date
        oldest_sent_date = parse(response.data["results"][-1]["sent_date"])
        self.assertTrue(due_date <= oldest_sent_date)

    def test_detail_shared_email_with_expiration_date(self):
        # with shared email has sent_date valid
        url = api_reverse("shared_email", kwargs={"pk": "90ba907b-3102-4b71-a76d-640a0db3cf16"})
        with freeze_time(self.fake_current_date):
            response = self.client.get(path=url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        # with shared email has sent_date invalid (sent_date expired)
        url = api_reverse("shared_email", kwargs={"pk": "3efd25d0-989a-4fba-aa8d-b91708760eb1"})
        with freeze_time(self.fake_current_date):
            response = self.client.get(path=url)
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class SharedEmailComment(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.user = User.objects.get(username=TEST_USER_NAME)
        self.shared_email = SharedEmailFactory(company=self.user.company)

    def test_patch_comment_over_comment_limit(self):
        # prepare 6 comment with unpin (is_important = False)
        url_comment = api_reverse('email_comments', kwargs={'parent_pk': self.shared_email.id})
        for i in range(6):
            comment = {'content': f'comment {i}', 'is_important': False}
            self.client.post(url_comment, data=comment, format='json')

        email_comment_qs = EmailComment.objects.filter(email=self.shared_email).only('id')
        comment_ids = [item.id for item in email_comment_qs]

        # Pin 3 comments (update is_important = True)
        url_pin_1 = api_reverse('email_comment', kwargs={'parent_pk': self.shared_email.id, 'pk': comment_ids[0]})
        response = self.client.patch(url_pin_1, data={'id': comment_ids[0], "is_important": True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url_pin_2 = api_reverse('email_comment', kwargs={'parent_pk': self.shared_email.id, 'pk': comment_ids[1]})
        response = self.client.patch(url_pin_2, data={'id': comment_ids[1], "is_important": True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url_pin_3 = api_reverse('email_comment', kwargs={'parent_pk': self.shared_email.id, 'pk': comment_ids[2]})
        response = self.client.patch(url_pin_3, data={'id': comment_ids[2], "is_important": True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Fourth Pin ==> Wrong
        url_pin_4 = api_reverse('email_comment', kwargs={'parent_pk': self.shared_email.id, 'pk': comment_ids[3]})
        response = self.client.patch(url_pin_4, data={'id': comment_ids[3], "is_important": True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CommentImportantLimitException().default_detail)

        # Unpin a comment
        response = self.client.patch(url_pin_2, data={'id': comment_ids[1], "is_important": False}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # continue Pin => success
        response = self.client.patch(url_pin_4, data={'id': comment_ids[3], "is_important": True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_comment_over_comment_limit(self):
        # prepare 3 comment with pin (is_important = True)
        url_comment = api_reverse('email_comments', kwargs={'parent_pk': self.shared_email.id})
        for i in range(3):
            comment = {'content': f'comment {i}', 'is_important': True}
            self.client.post(url_comment, data=comment, format='json')

        # continue post comment with pin => Wrong
        response = self.client.post(url_comment, data={'content': f'comment 4', 'is_important': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], CommentImportantLimitException().default_detail)

        # unpin a comment
        comment = EmailComment.objects.filter(email=self.shared_email).first()
        comment.is_important = False
        comment.save()

        # post comment with pin
        response = self.client.post(url_comment, data={'content': f'comment 4', 'is_important': True}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
