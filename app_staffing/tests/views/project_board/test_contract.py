from datetime import datetime, timedelta

import pytz
from django.conf import settings
from rest_framework import status

from app_staffing.board.models import ProjectContract
from app_staffing.tests.factories.contract import ProjectContractFactory
from app_staffing.tests.factories.card import PersonnelFactory
from app_staffing.tests.factories.organization import OrganizationFactory, ContactFactory
from app_staffing.tests.factories.personnel_card_list import PersonnelCardListFactory
from app_staffing.tests.views.project_board import ProjectBoardTestBase


class ContractProjectActionTest(ProjectBoardTestBase):

    def setUp(self):
        super().setUp()
        self.start = datetime.now(pytz.timezone(settings.TIME_ZONE))
        self.end = datetime.now(pytz.timezone(settings.TIME_ZONE)) + timedelta(1)
        self.start_utc = self.start.strftime("%Y-%m-%dT%H:%M:%S%z")
        self.end_utc = self.end.strftime("%Y-%m-%dT%H:%M:%S%z")
        self.url = f"/app_staffing/board/project/contracts"
        self.org = OrganizationFactory()
        self.contact = ContactFactory()
        self.card_list = PersonnelCardListFactory()
        self.personnel = PersonnelFactory(card_list=self.card_list)
        self.data = dict(
            personnel=self.personnel.id,
            cardId=self.project.id,
            higherOrganization=self.org.id,
            higherContact=self.contact.id,
            lowerOrganization=self.org.id,
            lowerContact=self.contact.id,
        )

    def test_create_contract_successful(self):
        res = self.client.post(self.url, data=self.data, format='json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.data.get('peronnel'), res.data.get('peronnel'))
        self.assertEqual(self.data.get('higherOrganization'), res.data.get('higher_organization'))
        self.assertEqual(self.data.get('higherContact'), res.data.get('higher_contact'))
        self.assertEqual(self.data.get('lowerOrganization'), res.data.get('lower_organization'))
        self.assertEqual(self.data.get('lowerContact'), res.data.get('lower_contact'))

    def test_get_contract_successful(self):
        mock_contract = \
            ProjectContractFactory(project=self.project, higher_organization_id=self.org.id, higher_contact_id=self.contact.id,
                            lower_organization_id=self.org.id, lower_contact_id=self.contact.id, personnel_id=self.personnel.id)
        res = self.client.get(f"{self.url}/{mock_contract.id}")
        self.assertEqual(str(mock_contract.id), res.data.get('id'))
        self.assertEqual(mock_contract.personnel_id, res.data.get('personnel'))
        self.assertEqual(mock_contract.higher_organization_id, res.data.get('higher_organization'))
        self.assertEqual(mock_contract.higher_contact_id, res.data.get('higher_contact'))
        self.assertEqual(mock_contract.lower_organization_id, res.data.get('lower_organization'))
        self.assertEqual(mock_contract.lower_contact_id, res.data.get('lower_contact'))

    def test_update_contract_successful(self):
        mock_contracts = ProjectContractFactory(project=self.project)
        data = dict(detail="new detail", lower_organization=self.org.id)
        res = self.client.patch(f"{self.url}/{mock_contracts.id}", data=data, format='json')
        rows = ProjectContract.objects.filter(id=mock_contracts.id)
        self.verify(res, rows, input_data=data, expected_keys=["personnel", "lower_organization_id"])

    def test_delete_contract_successful(self):
        mock_contracts = ProjectContractFactory(project=self.project)
        res = self.client.delete(f"{self.url}/{mock_contracts.id}")
        rows = ProjectContract.objects.filter(project_id=self.project.id)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(rows.count(), 0)
