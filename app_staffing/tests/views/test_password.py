from rest_framework.test import APITestCase
from rest_framework import status

from django.core import mail
from django.conf import settings

from app_staffing.models.user import User

from app_staffing.tests.settings import \
    TEST_USER_NAME, TEST_USER_PASSWORD

import re

class PasswordResetEmailViewCRUDTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]
    base_url = '/app_staffing/password'

    def test_send_password_reset_email(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        response = self.client.post(path='/app_staffing/password', data={'email': email})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'result': 'ok'})

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[コモレビ]パスワードの再設定のご案内')
        self.assertTrue("/passwordChange" in mail.outbox[0].message().as_string())
        self.assertIn('以下のリンクより、パスワードを再設定することができます。', mail.outbox[0].message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, mail.outbox[0].message().as_string())

    def test_email_does_not_exist(self):
        response = self.client.post(path='/app_staffing/password', data={'email': 'hoge@aaa.com'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'result': 'AccountDoesNotExist'})
        self.assertEqual(len(mail.outbox), 0)

    def test_over_length_email(self):
        email = 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc'
        response = self.client.post(path='/app_staffing/password', data={'email': email})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['email']), settings.LENGTH_VALIDATIONS['user']['email']['message'])
        self.assertEqual(len(mail.outbox), 0)

    def test_null_email(self):
        response = self.client.post(path='/app_staffing/password', data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['email']), '必須項目です。')
        self.assertEqual(len(mail.outbox), 0)

class PasswordChangeViewCRUDTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]
    base_url = '/app_staffing/password'

    def test_valid_token(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]

        response = self.client.get(path='/app_staffing/password_change/{0}'.format(token))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_token(self):
        response = self.client.get(path='/app_staffing/password_change/{0}'.format('1234'))
        self.assertEqual(response.status_code, status.HTTP_410_GONE)

    def test_change_password(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]
        new_password = 'ChangeMe123@#&'

        # パスワード変更
        response = self.client.patch(path='/app_staffing/password_change/{0}'.format(token), data={'password': new_password, 'password_confirm': new_password})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # パスワード変更したら、セッションが切断される
        response = self.client.get(path='/app_staffing/authorized_action', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # 前のパスワードではログインできない
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        response = self.client.get(path='/app_staffing/authorized_action', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # 新しいパスワードでログインできる
        self.client.login(username=TEST_USER_NAME, password=new_password)
        response = self.client.get(path='/app_staffing/authorized_action', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # パスワード変更済みなのに、再度パスワード変更画面に行ったらエラーになる
        response = self.client.get(path='/app_staffing/password_change/{0}'.format(token))
        self.assertEqual(response.status_code, status.HTTP_410_GONE)

    def test_change_password_with_invalid_token(self):
        new_password = 'ChangeMe123@#&'

        response = self.client.patch(path='/app_staffing/password_change/{0}'.format('12345'), data={'password': new_password, 'password_confirm': new_password})
        self.assertEqual(response.status_code, status.HTTP_410_GONE)

    def test_change_password_with_unmatch_password(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]
        new_password1 = 'ghife97e'
        new_password2 = '1hife97e'

        response = self.client.patch(path='/app_staffing/password_change/{0}'.format(token), data={'password': new_password1, 'password_confirm': new_password2})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_expires_token(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]

        from django.conf import settings
        from django.core.cache import cache

        cache_key = f'{settings.CACHE_PASSWORD_RESET_KEY_BASE}_{token}'
        cache.delete(cache_key)

        response = self.client.get(path='/app_staffing/password_change/{0}'.format(token))
        self.assertEqual(response.status_code, status.HTTP_410_GONE)

    def test_over_length_password(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]
        new_password = 'aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc'

        response = self.client.patch(path='/app_staffing/password_change/{0}'.format(token), data={'password': new_password, 'password_confirm': new_password})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['password']), settings.LENGTH_VALIDATIONS['user']['password']['message'])

    def test_null_password(self):
        email = User.objects.get(username=TEST_USER_NAME).email
        self.client.post(path='/app_staffing/password', data={'email': email})
        token = re.findall('passwordChange\/(.*)\"', mail.outbox[0].message().as_string())[0]

        response = self.client.patch(path='/app_staffing/password_change/{0}'.format(token), data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['password']), '必須項目です。')
        self.assertEqual(str(response.data['password_confirm']), '必須項目です。')
