from rest_framework import status
from rest_framework.test import APITestCase
from app_staffing.tests.views.validators.meta import IsVersionInfo

EXPECTED_REVISION_NUMBER = 100


class LocationsViewTestCase(APITestCase):

    # Note: Non-authorized user can access this view.
    def test_locations_list(self):
        with self.settings(REVISION_NUMBER=EXPECTED_REVISION_NUMBER):
            response = self.client.get(path='/app_staffing/version', format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            IsVersionInfo(response.data).validate()
            self.assertEqual(EXPECTED_REVISION_NUMBER, response.data['revision'])
