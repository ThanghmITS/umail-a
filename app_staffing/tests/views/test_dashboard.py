import pytz
import json
from datetime import datetime
from dateutil.relativedelta import relativedelta
from freezegun import freeze_time

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from django.conf import settings

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD


class DashboardStatisViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/account/users.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/organization_stats.json',
        'app_staffing/tests/fixtures/contact_stats.json',
        'app_staffing/tests/fixtures/scheduled_email_stats.json',
        'app_staffing/tests/fixtures/shared_email_stats.json',
        'app_staffing/tests/fixtures/staff_in_charge_stats.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)  # Setup login with account and password

    def test_get_dashboard(self):
        url = api_reverse('dashboard_stats')

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data_response = json.loads(response.content)  # Convert response data

        current_time = datetime.now(pytz.timezone(settings.TIME_ZONE))  # Current time by current timezone
        current_time_str = current_time.strftime(settings.DASHBOARD_DATETIME_FORMAT)

        scan_time = current_time + relativedelta(months=1)  # Refresh time 1 month after user login (refresh_time)
        scan_time_str = scan_time.strftime(settings.DASHBOARD_DATETIME_FORMAT)

        calculate_time = data_response['dashboard_time']['calculate_time']
        refresh_time = data_response['dashboard_time']['refresh_time']

        print(current_time_str, calculate_time)

        self.assertEqual(calculate_time, current_time_str)
        self.assertEqual(refresh_time, scan_time_str)

    def test_get_stats_dashboard(self):
        response_get_datetime = datetime(year=2021, month=12, day=1)

        expecteddata = {
            "contact_stats": {
		        "labels": ["2021/6", "2021/7", "2021/8", "2021/9", "2021/10", "2021/11"],
        		"values": {
			        "jobs": {
				        "development": [61, 71, 81, 91, 101, 111],
				        "infrastructure": [62, 72, 82, 92, 102, 112],
				        "others": [63, 73, 83, 93, 103, 113]
			        },
			        "personnel": {
				        "development": [64, 74, 84, 94, 104, 114],
				        "infrastructure": [65, 75, 85, 95, 105, 115],
        				"others": [66, 76, 86, 96, 106, 116]
        			},
		        	"announcement": [67, 77, 87, 97, 107, 117]
		        }
	        },
            "organization_stats": {
		        "labels": ["2021/6", "2021/7", "2021/8", "2021/9", "2021/10", "2021/11"],
        		"values": {
		        	"prospective": [61, 71, 81, 91, 101, 111],
    			    "approached": [62, 72, 82, 92, 102, 112],
        			"exchanged": [63, 73, 83, 93, 103, 113],
			        "client": [64, 74, 84, 94, 104, 114]
		        }
	        },
	        "scheduled_email_stats": {
		        "labels": ["2021/6", "2021/7", "2021/8", "2021/9", "2021/10", "2021/11"],
        		"values": {
			        "jobs": [61, 71, 81, 91, 101, 111],
			        "personnel": [62, 72, 82, 92, 102, 112],
			        "announcement": [63, 73, 83, 93, 103, 113]
		        }
	        },
            "shared_email_stats": {
                "2021/6": {
                    "top_values": [22, 21, 20, 19, 18, 17],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
                "2021/7": {
                    "top_values": [10, 9, 4, 3, 3, 1],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
                "2021/8": {
                    "top_values": [14, 4, 2, 1, 1, 3],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
                "2021/9": {
                    "top_values": [15, 14, 11, 6, 5, 4],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
                "2021/10": {
                    "top_values": [5, 4, 4, 3, 2, 6],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
                "2021/11": {
                    "top_values": [9, 7, 7, 5, 5, 4],
                    "from_address": ["a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com", "e@gmail.com",
                                     "other"],
                    "from_name": ["a", "b", "c", "d", "e", "other"],
                },
            },
            "staff_in_charge_stats": {
                "2021/6": {
                    "top_values": [22, 21, 20, 19, 18, 17],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
                "2021/7": {
                    "top_values": [10, 9, 4, 3, 3, 1],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
                "2021/8": {
                    "top_values": [14, 4, 2, 1, 1, 3],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
                "2021/9": {
                    "top_values": [15, 14, 11, 6, 5, 4],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
                "2021/10": {
                    "top_values": [5, 4, 4, 3, 2, 6],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
                "2021/11": {
                    "top_values": [9, 7, 7, 5, 5, 4],
                    "staff": ["Top 01", "Top 02", "Top 03", "Top 04", "Top 05", "other"],
                },
            }
        }

        with freeze_time(response_get_datetime):
            response = self.client.get(path='/app_staffing/statistics/dashboard', format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data["contact_stats"], expecteddata["contact_stats"])
            self.assertEqual(response.data["organization_stats"], expecteddata["organization_stats"])
            self.assertEqual(response.data["scheduled_email_stats"], expecteddata["scheduled_email_stats"])
            self.assertEqual(response.data["shared_email_stats"], expecteddata["shared_email_stats"])
            self.assertEqual(response.data["staff_in_charge_stats"], expecteddata["staff_in_charge_stats"])

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        response = self.client.get(path='/app_staffing/statistics/dashboard', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
