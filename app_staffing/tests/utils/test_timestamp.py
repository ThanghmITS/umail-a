from time import sleep
from django.test import TestCase
from app_staffing.utils.timestamp import LogExecutionTime

from logging import getLogger

logger = getLogger(__name__)


@LogExecutionTime(logger=logger)
def sleep_1_sec():
    sleep(1.1)  # Add + 0.1 for test robustness, because Python's timedelta.seconds will round-down seconds.


class LogExecutionTimeTestCase(TestCase):

    def test_logging_execution_time(self):
        with self.assertLogs(logger=logger) as log:
            sleep_1_sec()

        message = log.records[0].message
        self.assertEqual('The operation sleep_1_sec took 1 seconds.', message)
