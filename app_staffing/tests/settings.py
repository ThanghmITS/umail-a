"""
A setting module for unit tests.
"""
from app_staffing.models.organization import Company

# Constants related to fixtures.
DEFAULT_COMPANY_ID = '3efd25d0-989a-4fba-aa8d-b91708760eb1' # depends on tests/fixtures/company.json
GET_DEFAULT_COMPANY = lambda: Company.objects.get(id=DEFAULT_COMPANY_ID)

TEST_USER_NAME = 'apiuser'
TEST_USER_PASSWORD = 'ChangeMe'

MIGRATION_USER_NAME = 'migration'
MIGRATION_USER_PASSWORD = 'n2W6eKoa'

NORMAL_USER_NAME = 'user@example.com'
NORMAL_USER_PASSWORD = 'ChangeMe'

ADMIN_USER_NAME = 'useradmin@example.com'
ADMIN_USER_PASSWORD = 'n2W6eKoa'

## Users for multi-tenancy tests.
ANOTHER_TENANT_NORMAL_USER_NAME = 'user@tenant_B.com'
ANOTHER_TENANT_NORMAL_USER_PASSWORD = 'ChangeMe'

ANOTHER_TENANT_ADMIN_USER_NAME = 'useradmin@tenant_B.com'
ANOTHER_TENANT_ADMIN_USER_PASSWORD = 'n2W6eKoa'

API_USER_ID = '6dba67ff-4bb7-4210-afec-f6fbec1c5fcb'

TEST_PERSONNEL_ID = 'deb03711-5965-4592-aa8c-f8e277e5a8d8'
TEST_SKILL_SHEET_ID = '9f03a195-e5e4-4ba5-87c8-93cd0afa32fe'
