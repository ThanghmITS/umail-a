import re
import os
import shutil

from django.test import TestCase
from django.core.mail import EmailMessage
from django.core import mail
from django.conf import settings

from app_staffing.models import ScheduledEmail, ScheduledEmailSetting
from app_staffing.models.email import SMTPServer, ScheduledEmailTarget
from app_staffing.models.organization import Contact, Organization
from app_staffing.services.email.send_mail import _create_email_instance, _generate_emails,\
    send_mails, put_mails, EMAIL_TEMPLATE_NAME, DUMMY_CONTACT_LAST_NAME, DUMMY_CONTACT_FIRST_NAME, DUMMY_ORGANIZATION_NAME
from app_staffing.models.addon import Addon

from app_staffing.tests.helpers.setup import MediaFolderSetupMixin
from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.email import SMTPServerFactory, create_scheduled_mail_data, TEST_FILE_NAME, ScheduledEmailSettingFactory
from app_staffing.tests.factories.addon import AddonFactory

from unittest.mock import Mock, patch, call
import datetime

TEST_FILE_PATH = 'app_staffing/tests/__temp__/SendMailAttachment.txt'
TEST_MAIL_SENDER_ADDRESS = 'testMailSender@example.com'
TEST_MAIL_SENDER_FIRST_NAME = 'First Name'
TEST_MAIL_SENDER_SIGNATURE = "***********\n A Signature\n ***********\n"
TEST_MAIL_SUBJECT = 'This is a Test Email'
TEST_MAIL_TEXT = 'This is a main content of the email'
TEST_MAIL_ATTACHMENT_BODY = 'file content'
TEST_MAIL_ATTACHMENT_TYPE = 'text/plain'
TEST_SHARED_EMAIL_ADDRESS = 'testsharedmail@example.com'
TEST_MAIL_TEXT_FORMAT = 'text'
SMTP_INFO = {'host': 'smtp@example.com', 'port': 25, 'user': 'user@example.com', 'password': 'password', 'tls': False, 'ssl': False}

class SendMailServiceTestCase(MediaFolderSetupMixin, TestCase):
    attachment = None
    valid_contacts_num = 1  # Depends on Test Data.

    fixtures = [
        'app_staffing/tests/fixtures/addon_datas.json'
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        if not os.path.exists(os.path.dirname(TEST_FILE_PATH)):
            os.makedirs(os.path.dirname(TEST_FILE_PATH))

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)
        ScheduledEmailSettingFactory.create(company=cls.company, use_open_count=True)
        AddonFactory.create(company=cls.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID)

        scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
            text_format=TEST_MAIL_TEXT_FORMAT,
        )

        cls.scheduled_mail_id = scheduled_mail.id

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(path=os.path.dirname(TEST_FILE_PATH))
        cls.attachment.file.delete()
        super().tearDownClass()

    def setUp(self):
        # Load the scheduled email instance from DB before every test to avoid the side effects of each test cases.
        self.instance = ScheduledEmail.objects.get(id=self.scheduled_mail_id)
        self.attachments = [
            {'filename': attachment.file.name} for attachment in self.instance.attachments.all()
        ]

    # open_countを使用しない
    # remove_promotionを使用しない
    def test_put_mail_with_text_format(self):
        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [],
            'content': "\nThis is a main content of the email\n",
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }
        payload = {
            'type': 'normal',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'total': 1,
            'smtp': SMTP_INFO,
            'contexts': [
                {
                    'subject': 'This is a Test Email',
                    'header': "Receiver's Corporation\nReceiver1 FirstName1 様\n",
                    'footer': "",
                    'to': 'receiver1@example.com',
                    'cc': ['receiver1_cc1@example.com', 'receiver1_cc2@example.com'],
                    'from': 'testMailSender@example.com',
                    'format': 'text',
                    'attachments': []
                }
            ],
            'content': "\nThis is a main content of the email\n",
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }

        self.assertEqual(result['has_error'], False)

        print('===========Preview the content of the Email============')
        print(mock.create_task.call_args_list)
        print('===========          Preview end           ============')

        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
            call(payload,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

    def test_put_mail_for_sender_and_share_option(self):
        self.instance.send_copy_to_sender = True
        self.instance.send_copy_to_share = True

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [
                {
                    'subject': '配信メール控え: This is a Test Email',
                    'header': '所属取引先\n取引先担当者 様\n',
                    'footer': '',
                    'to': 'testMailSender@example.com',
                    'cc': [],
                    'from': settings.DEFAULT_FROM_EMAIL,
                    'format': 'text',
                    'attachments': []
                },
                {
                    'subject': '配信メール控え: This is a Test Email',
                    'header': '所属取引先\n取引先担当者 様\n',
                    'footer': '',
                    'to': 'testsharedmail@example.com',
                    'cc': [],
                    'from': 'testMailSender@example.com',
                    'format': 'text',
                    'attachments': []
                }
            ],
            'content': '\nThis is a main content of the email\n',
            'sig': '\n***********\n A Signature\n ***********\n\n',
        }

        payload = {
            'type': 'normal',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'total': 1,
            'smtp': SMTP_INFO,
            'contexts': [
                {
                    'subject': 'This is a Test Email',
                    'header': "Receiver's Corporation\nReceiver1 FirstName1 様\n",
                    'footer': "",
                    'to': 'receiver1@example.com',
                    'cc': ['receiver1_cc1@example.com', 'receiver1_cc2@example.com'],
                    'from': 'testMailSender@example.com',
                    'format': 'text',
                    'attachments': []
                }
            ],
            'content': "\nThis is a main content of the email\n",
            'sig': "\n***********\n A Signature\n ***********\n\n"
        }

        self.assertEqual(result['has_error'], False)

        print('===========Preview the content of the Email============')
        print(mock.create_task.call_args_list)
        print('===========          Preview end           ============')

        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
            call(payload,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

    def test_put_html_mail_withno_open_count_remove_promotion(self):
        # open_count, remove_promotionについてAddOnは購入しているが、使用する設定になってない
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_open_count = False
        scheduled_email_setting.use_remove_promotion = False

        self.instance.text_format = 'html'
        self.instance.save()

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None, scheduled_email_setting=scheduled_email_setting, is_open_count_available=True, is_use_remove_promotion_available=True)

        self.assertEqual(result['has_error'], False)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [],
            'content': '\n<br />\nThis is a main content of the email<br />\n<br />' + settings.SCHEDULED_EMAIL_PROMOTION_MESSAGE,
            'sig': '\n<pre>***********\n A Signature\n ***********\n</pre>\n'
        }
        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)

        call_args, _ = mock.create_task.call_args_list[1]
        payload = call_args[0]

        message = payload['contexts'][0]['header'] + payload['content'] + payload['sig'] + payload['contexts'][0]['footer'] 
        format = payload['contexts'][0]['format']
        contact = self.instance.targets.select_related("contact", "contact__organization").filter(
            contact__organization__is_blacklisted=False,
        ).first().contact
        self.assertEqual(format, 'html')
        self.assertTrue(contact.organization.name in message)
        self.assertTrue(contact.last_name in message)
        self.assertTrue(contact.first_name in message)
        self.assertTrue(contact.display_name in message)
        self.assertTrue('***********\n A Signature\n ***********\n' in message)
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in message)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in message)

    def test_put_mail_with_open_count_and_remove_promotion(self):
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_open_count = True
        scheduled_email_setting.use_remove_promotion = True
        scheduled_email_setting.save()

        self.instance.text_format = 'html'
        self.instance.save()

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None, scheduled_email_setting=scheduled_email_setting, is_open_count_available=True, is_use_remove_promotion_available=True)

        self.assertEqual(result['has_error'], False)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [],
            'content': '\n<br />\nThis is a main content of the email<br />\n<br />',
            'sig': '\n<pre>***********\n A Signature\n ***********\n</pre>\n',
        }
        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)

        call_args, _ = mock.create_task.call_args_list[1]
        payload = call_args[0]

        message = payload['contexts'][0]['header'] + payload['content'] + payload['sig'] + payload['contexts'][0]['footer'] 
        format = payload['contexts'][0]['format']
        contact = self.instance.targets.select_related("contact", "contact__organization").filter(
            contact__organization__is_blacklisted=False,
        ).first().contact
        self.assertEqual(format, 'html')
        self.assertTrue(contact.organization.name in message)
        self.assertTrue(contact.last_name in message)
        self.assertTrue(contact.first_name in message)
        self.assertTrue(contact.display_name in message)
        self.assertTrue('***********\n A Signature\n ***********\n' in message)
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' in message)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' not in message)

    def test_put_mail_that_not_allowed_open_count_and_remove_promotion(self):
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        # 使用は可能だが、配信メールを送る時チェックしない場合
        scheduled_email_setting.use_open_count = True
        scheduled_email_setting.use_remove_promotion = True
        scheduled_email_setting.save()

        self.instance.text_format = 'html'
        self.instance.save()

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None, scheduled_email_setting=scheduled_email_setting)

        self.assertEqual(result['has_error'], False)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [],
            'content': '\n<br />\nThis is a main content of the email<br />\n<br />' + settings.SCHEDULED_EMAIL_PROMOTION_MESSAGE,
            'sig': '\n<pre>***********\n A Signature\n ***********\n</pre>\n',
        }
        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)

        call_args, _ = mock.create_task.call_args_list[1]
        payload = call_args[0]

        message = payload['contexts'][0]['header'] + payload['content'] + payload['sig'] + payload['contexts'][0]['footer'] 
        format = payload['contexts'][0]['format']
        contact = self.instance.targets.select_related("contact", "contact__organization").filter(
            contact__organization__is_blacklisted=False,
        ).first().contact
        self.assertEqual(format, 'html')
        self.assertTrue(contact.organization.name in message)
        self.assertTrue(contact.last_name in message)
        self.assertTrue(contact.first_name in message)
        self.assertTrue(contact.display_name in message)
        self.assertTrue('***********\n A Signature\n ***********\n' in message)
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in message)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in message)



    def test_put_mail_with_error(self):
        """
        WARNING: The outbox attribute is a special attribute that is created only when the locmem email backend is used.
        So this test only runs in a local environment.
        """
        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            # テストスピードを上げるためにsleepはモック化
            with patch('time.sleep', return_value=mock):
                mock.create_task.side_effect = Exception('Boom!')
                result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['has_error'], True)
        self.assertEqual(mock.create_task.call_count, 5)

    def test_put_mail_with_tls(self):
        smtp_server = SMTPServer.objects.get(company_id=self.instance.company.id)
        smtp_server.use_tls = True
        smtp_server.save()

        smtp_info = {
            'host': 'smtp@example.com',
            'port': 25,
            'user': 'user@example.com',
            'password': 'password',
            'tls': True,
            'ssl': False
        }

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': smtp_info,
            'contexts': [],
            'content': '\nThis is a main content of the email\n',
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }
        payload = {
            'type': 'normal',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'total': 1,
            'smtp': smtp_info,
            'contexts': [
                {
                    'subject': 'This is a Test Email',
                    'header': "Receiver's Corporation\nReceiver1 FirstName1 様\n",
                    'footer': "",
                    'to': 'receiver1@example.com',
                    'cc': ['receiver1_cc1@example.com', 'receiver1_cc2@example.com'],
                    'from': 'testMailSender@example.com',
                    'format': 'text',
                    'attachments': []
                }
            ],
            'content': '\nThis is a main content of the email\n',
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }

        self.assertEqual(result['has_error'], False)

        print('===========Preview the content of the Email============')
        print(mock.create_task.call_args_list)
        print('===========          Preview end           ============')

        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
            call(payload,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

    def test_put_mail_with_ssl(self):
        smtp_server = SMTPServer.objects.get(company_id=self.instance.company.id)
        smtp_server.use_ssl = True
        smtp_server.save()

        smtp_info = {
            'host': 'smtp@example.com',
            'port': 25,
            'user': 'user@example.com',
            'password': 'password',
            'tls': False,
            'ssl': True
        }

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': smtp_info,
            'contexts': [],
            'content': '\nThis is a main content of the email\n',
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }
        payload = {
            'type': 'normal',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'total': 1,
            'smtp': smtp_info,
            'contexts': [
                {
                    'subject': 'This is a Test Email',
                    'header': "Receiver's Corporation\nReceiver1 FirstName1 様\n",
                    'footer': "",
                    'to': 'receiver1@example.com',
                    'cc': ['receiver1_cc1@example.com', 'receiver1_cc2@example.com'],
                    'from': 'testMailSender@example.com',
                    'format': 'text',
                    'attachments': []
                }
            ],
            'content': '\nThis is a main content of the email\n',
            'sig': "\n***********\n A Signature\n ***********\n\n",
        }

        self.assertEqual(result['has_error'], False)

        print('===========Preview the content of the Email============')
        print(mock.create_task.call_args_list)
        print('===========          Preview end           ============')

        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
            call(payload,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

    def test_put_mail_send_count_cloud_task(self):
        smtp_server = SMTPServer.objects.get(company_id=self.instance.company.id)
        smtp_server.send_count_per_task = 2
        smtp_server.save()

        # 9名のターゲットを作成、デフォルト1名なので合計で10名のターゲット
        organization = Organization.objects.get(name="Receiver's Corporation")
        for i in range(1, 10):
            contact = Contact.objects.create(
                last_name=f'Receiver_{i}',
                first_name=f'FirstName_{i}',
                email=f'receiver_send_count_test_{i}@example.com',
                organization=organization,
                company=self.instance.company,
            )
            ScheduledEmailTarget.objects.create(
                email=self.instance,
                contact=contact,
                company=self.instance.company,
            )

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['has_error'], False)

        call_args_list = mock.create_task.call_args_list
        print('=========== Preview the content of the call ============')
        print(call_args_list)
        print('=========== Preview end ============')

        self.assertEquals(len(call_args_list), 6)

        for c in call_args_list:
            if c[0][0]['type'] == 'normal':
                # send_count_per_task = 2なので、contextsには2名分のデータしか入らない
                self.assertEquals(len(c[0][0]['contexts']), 2)

    def test_put_html_mail_content_auto_br(self):
        # open_count, remove_promotionについてAddOnは購入しているが、使用する設定になってない
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_open_count = False
        scheduled_email_setting.use_remove_promotion = False

        self.instance.text_format = 'html'
        self.instance.text = 'hogehoge\nfugafuga\n'
        self.instance.save()

        mock = Mock()
        with patch('app_staffing.services.email.send_mail.CloudTasks', return_value=mock):
            result = put_mails(scheduled_email_instance=self.instance, shared_email_address=None, scheduled_email_setting=scheduled_email_setting, is_open_count_available=True, is_use_remove_promotion_available=True)

        self.assertEqual(result['has_error'], False)

        payload_for_copy = {
            'type': 'copy',
            'email_id': str(self.instance.id),
            'attachments': self.attachments,
            'smtp': SMTP_INFO,
            'contexts': [],
            'content': '\n<br />\nhogehoge<br>fugafuga<br><br />\n<br />' + settings.SCHEDULED_EMAIL_PROMOTION_MESSAGE,
            'sig': '\n<pre>***********\n A Signature\n ***********\n</pre>\n'
        }
        calls = [
            call(payload_for_copy,self.instance.date_to_send, str(self.instance.id)),
        ]
        mock.create_task.assert_has_calls(calls)

        call_args, _ = mock.create_task.call_args_list[1]
        payload = call_args[0]

        message = payload['contexts'][0]['header'] + payload['content'] + payload['sig'] + payload['contexts'][0]['footer'] 
        format = payload['contexts'][0]['format']
        self.assertEqual(format, 'html')
        self.assertTrue('hogehoge<br>fugafuga<br>' in message)
