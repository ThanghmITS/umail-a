import datetime
from unittest.mock import MagicMock, patch

from django.test import TestCase

from django_mailbox.models import Message, MessageAttachment
from app_staffing.services.email.store_email import _get_file_name, store_email, extract_message_data, \
    save_email, save_attachments, EmailDataError, _get_original_body, get_header

from app_staffing.models import Email, EmailAttachment, Company
from app_staffing.models.email import INBOUND
from app_staffing.tests.helpers.setup import AttachmentPathMockingMixin, MediaFolderSetupMixin
from app_staffing.tests.settings import DEFAULT_COMPANY_ID
from django.test import override_settings

EMAIL_FIXTURE_ID = "90ba907b-3102-4b71-a76d-640a0db3cfdb"
FIXTURE_ATTACHMENT_NAME = "testData.txt"
MOCKED_TIME = '2018-01-01'


class EmailStoreTestCase(AttachmentPathMockingMixin, MediaFolderSetupMixin, TestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/django_mailbox_messages.json',
        'app_staffing/tests/fixtures/django_mailbox_message_attachments.json',
    ]
    maxDiff = None

    @classmethod
    def setUpTestData(cls):  # This is essential for attachment test.
        super().setUpTestData()
        # Checking fixture conditions.
        cls.original_message = Message.objects.get(pk=1)
        cls.original_message_attachments = MessageAttachment.objects.filter(message_id__exact=cls.original_message)
        cls.company = Company.objects.get(id=DEFAULT_COMPANY_ID)  # Depends on fixture data.

        assert cls.original_message_attachments[0].get_filename() == 'text1.txt'  # Depends on fixture data.

    def test_copy_email(self):
        """Check if a whole operation success.
        Note: This test only checks parameter passing works.
        """
        instance = store_email(sender=None, message=self.original_message, company=self.company, ignore_domain=None)

        # Checking the content of the App Email model.
        # Note: Expected data is generated from the original message body (Base64 encoded.)
        self.assertEqual("Test a Signal", instance.subject)
        self.assertEqual("<7ddeb290-c007-f0ad-fc30-a813408b8306@example.com>", instance.message_id)
        self.assertEqual("TestMailBox <test@example.com>", instance.from_header)
        self.assertEqual("TestMailBox", instance.from_name)
        self.assertEqual("test@example.com", instance.from_address)
        self.assertEqual("receiver@example.com", instance.to_header)
        self.assertEqual("Reply Here <replyme@example.com>", instance.reply_to_header)
        # Note: Original CC header is encoded in body.
        self.assertCountEqual(
            ['cc1@example.com', 'CC2 <cc2@example.com>'],
            [value for value in instance.cc_values]
        )
        self.assertEqual("Testing a signal handler.", instance.text)
        self.assertEqual("", instance.html)
        self.assertEqual(
            datetime.datetime(2019, 3, 5, 16, 44, 2, tzinfo=datetime.timezone(datetime.timedelta(0, 32400))),
            instance.sent_date
        )
        # Followings are metadata for this app.
        self.assertTrue(instance.has_attachments)
        self.assertEqual(True, instance.shared)
        self.assertTrue(instance.has_attachments)
        self.assertIsNone(instance.category)
        self.assertIsNone(instance.estimated_category)
        self.assertIsNone(instance.staff_in_charge)

    def test_copy_email_with_ignore_domain(self):
        instance = store_email(sender=None, message=self.original_message, company=self.company, ignore_domain='example.com')
        self.assertEquals(instance, None)

    def test_copy_email_with_similar_ignore_domain(self):
        original_message = Message.objects.get(pk=1)
        # from_addressの値はbodyから取得しているので、Bodyを上書きする
        # body内のFromに設定されている値は'test@hoge.example.com'
        original_message.body = 'UmV0dXJuLVBhdGg6IDx0ZXN0QGV4YW1wbGUuY29tPg0KWC1PcmlnaW5hbC1UbzogdGVzdEBleGFtcGxlLmNvbQ0KRGVsaXZlcmVkLVRvOiB0ZXN0QGV4YW1wbGUuY29tDQpSZWNlaXZlZDogZnJvbSBUZXN0UEMubG9jYWwgKFhYWFgudjQuZW5hYmxlci5uZS5qcCBbMTkyLjE2OC4xLjEwXSkNCglieSBleGFtcGxlLmNvbSAoUG9zdGZpeCkgd2l0aCBFU01UUFNBIGlkIFhYWFhYWFhYDQoJZm9yIDx0ZXN0QGV4YW1wbGUuY29tPjsgVHVlLCAgNSBNYXIgMjAxOSAxNjo0NDowMiArMDkwMCAoSlNUKQ0KVG86IHJlY2VpdmVyQGV4YW1wbGUuY29tDQpDYzogY2MxQGV4YW1wbGUuY29tLCBDQzIgPGNjMkBleGFtcGxlLmNvbT4NCkZyb206IFRlc3RNYWlsQm94IDx0ZXN0QGhvZ2UuZXhhbXBsZS5jb20+DQpSZXBseS1UbzogUmVwbHkgSGVyZSA8cmVwbHltZUBleGFtcGxlLmNvbT4NClN1YmplY3Q6IFRlc3QgYSBTaWduYWwNCk1lc3NhZ2UtSUQ6IDxlY2Q2ZWFiNy1kZTNkLTliZDctZjFiZS02ZjA0ZTA3ZTc5OWNAZXhhbXBsZS5jb20+DQpEYXRlOiBUdWUsIDUgTWFyIDIwMTkgMTY6NDQ6MDIgKzA5MDANClVzZXItQWdlbnQ6IE1vemlsbGEvNS4wIChNYWNpbnRvc2g7IEludGVsIE1hYyBPUyBYIDEwLjE0OyBydjo2MC4wKQ0KIEdlY2tvLzIwMTAwMTAxIFRodW5kZXJiaXJkLzYwLjUuMg0KTUlNRS1WZXJzaW9uOiAxLjANCkNvbnRlbnQtVHlwZTogdGV4dC9wbGFpbjsgY2hhcnNldD11dGYtODsgZm9ybWF0PWZsb3dlZA0KQ29udGVudC1UcmFuc2Zlci1FbmNvZGluZzogN2JpdA0KQ29udGVudC1MYW5ndWFnZTogZW4tVVMNCg0KVGVzdGluZyBhIHNpZ25hbCBoYW5kbGVyLg=='

        instance = store_email(sender=None, message=original_message, company=self.company, ignore_domain='example.com')
        self.assertEqual("<7ddeb290-c007-f0ad-fc30-a813408b8306@example.com>", instance.message_id)
        self.assertEqual("test@hoge.example.com", instance.from_address)

    def test_copy_email_with_not_ignored(self):
        original_message = Message.objects.get(pk=1)
        original_message.message_id = f'<7ddeb290-c007-f0ad-fc30-a813408b8306@prd-k8s-cron-sendmail>'

        instance = store_email(sender=None, message=original_message, company=self.company, ignore_domain='example.com')
        self.assertEqual("Test a Signal", instance.subject)
        self.assertEqual("<7ddeb290-c007-f0ad-fc30-a813408b8306@prd-k8s-cron-sendmail>", instance.message_id)
        self.assertEqual("TestMailBox <test@example.com>", instance.from_header)
        self.assertEqual("TestMailBox", instance.from_name)
        self.assertEqual("test@example.com", instance.from_address)
        self.assertEqual("receiver@example.com", instance.to_header)
        self.assertEqual("Reply Here <replyme@example.com>", instance.reply_to_header)
        self.assertCountEqual(
            ['cc1@example.com', 'CC2 <cc2@example.com>'],
            [value for value in instance.cc_values]
        )
        self.assertEqual("Testing a signal handler.", instance.text)
        self.assertEqual("", instance.html)
        self.assertEqual(
            datetime.datetime(2019, 3, 5, 16, 44, 2, tzinfo=datetime.timezone(datetime.timedelta(0, 32400))),
            instance.sent_date
        )
        self.assertTrue(instance.has_attachments)
        self.assertEqual(True, instance.shared)
        self.assertTrue(instance.has_attachments)
        self.assertIsNone(instance.category)
        self.assertIsNone(instance.estimated_category)
        self.assertIsNone(instance.staff_in_charge)


    def test_copy_email_with_exception(self):
        """Check if copying email will not be aborted by errors when SKIP_ON_ERRORS option is enabled. """
        mock = MagicMock(side_effect=ValueError)

        with patch('app_staffing.services.email.store_email.save_email', mock):
            with self.settings(SKIP_ON_ERRORS=True):
                with self.assertLogs('', level='WARNING'):  # Also, checking if some warning has logged.
                    store_email(sender=None, message=self.original_message, company=self.company, ignore_domain=None)

    def test_extract_message(self):

        expected = {  # This value depends on a fixture data. (Base64 encoded body)
            'message_id': '<7ddeb290-c007-f0ad-fc30-a813408b8306@example.com>',
            'shared': True,
            'direction': 'inbound',
            'from_header': 'TestMailBox <test@example.com>',
            'from_name': 'TestMailBox',
            'from_address': 'test@example.com',
            'reply_to_header': "Reply Here <replyme@example.com>",
            'cc_header': 'cc1@example.com, CC2 <cc2@example.com>',
            'to_header': 'receiver@example.com',
            'subject': 'Test a Signal', 'text': 'Testing a signal handler.',
            'html': '',
            'sent_date': datetime.datetime(2019, 3, 5, 16, 44, 2, tzinfo=datetime.timezone(datetime.timedelta(0, 32400))),
            'attachments': [{'name': 'text1.txt', 'file': self.original_message_attachments[0].document.file}]
        }

        original_message = Message.objects.get(pk=1)
        actual = extract_message_data(original_message, 'test_company_id_for logging')

        # Check attributes.
        self.assertEqual(expected, actual)

    def test_error_behavior_of_extract_message(self):

        original_message = Message.objects.get(pk=1)
        original_message.body = "Wrong_body."

        with self.assertRaises(EmailDataError):
            with self.assertLogs('app_staffing', level='WARNING'):
                extract_message_data(original_message, 'test_company_id_for logging')

        # TODO: Assert log Detail.

    def test_save_email(self):
        """Check if test_save method works."""

        # Test data definition
        message_id = "SampleStrings"
        shared = True
        direction = "inbound"
        from_header = "Test User <test@example.com>"
        from_name = "Test User"
        from_address = "test@example.com"
        to_header = 'receiver@example.com'
        reply_to_header = "Reply Here <replyme@example.com>"
        cc_header = "cc1@example.com,cc2@example.com"
        subject = "This is a test mail generated in an unit test."
        text = "Sample text"
        html = "<p>Sample text</p>"
        sent_date = datetime.datetime(2019, 3, 8, 15, 00, 0, tzinfo=datetime.timezone(datetime.timedelta(0, 32400)))

        data = {
            'message_id': message_id,
            'shared': shared,
            'direction': direction,
            'from_header': from_header,
            'from_name': from_name,
            'from_address': from_address,
            'to_header': to_header,
            "reply_to_header": reply_to_header,
            'cc_header': cc_header,
            'subject': subject,
            'text': text,
            'html': html,
            'sent_date': sent_date
        }

        created_instance = save_email(data=data, company=self.company)

        # Check actual data stored in a database.
        db_instance = Email.objects.get(id=created_instance.id)

        self.assertEqual(message_id, db_instance.message_id)
        self.assertEqual(INBOUND, db_instance.direction)
        self.assertEqual(from_header, db_instance.from_header)
        self.assertEqual(reply_to_header, db_instance.reply_to_header)
        self.assertEqual(from_name, db_instance.from_name)
        self.assertEqual(from_address, db_instance.from_address)
        self.assertEqual(to_header, db_instance.to_header)
        self.assertCountEqual(cc_header.split(','), [value for value in db_instance.cc_values])
        self.assertEqual(subject, db_instance.subject)
        self.assertEqual(text, db_instance.text)
        self.assertEqual(html, db_instance.html)
        self.assertEqual(sent_date, db_instance.sent_date)
        self.assertFalse(db_instance.has_attachments)
        self.assertEqual(None, db_instance.created_user)
        # should not change basically.

    def test_save_email_with_wrong_data(self):
        """Check if test_save method works."""

        # Test data definition
        message_id = True  # Wrong, not strings.
        shared = "yES!"  # Wrong, not boolean
        direction = True  # Wrong, not strings.
        to_header = True  # Wrong, not strings.
        from_header = True  # Wrong, not strings.
        from_name = True  # Wrong, not strings.
        from_address = True  # Wrong, not strings.
        reply_to_header = True  # Wrong, not strings.
        cc_header = True  # Wrong, not strings.
        subject = True  # Wrong, not strings.
        text = True  # Wrong, not strings.
        html = True  # Wrong, not strings.
        sent_date = True  # Wrong, not Datetime.

        data = {
            'message_id': message_id,
            'shared': shared,
            'direction': direction,
            'from_header': from_header,
            'from_name': from_name,
            'from_address': from_address,
            'to_header': to_header,
            'cc_header': cc_header,
            'reply_to_header': reply_to_header,
            'subject': subject,
            'text': text,
            'html': html,
            'sent_date': sent_date,
            'attachments': None
        }

        expect_error_log = "WARNING:root:Could not convert django-mailbox data to app data. Skipping Email importation. Details:{'message_id': [ErrorDetail(string='Not a valid string.', code='invalid')], 'shared': [ErrorDetail(string='Must be a valid boolean.', code='invalid')], 'direction': [ErrorDetail(string='\"True\"は有効な選択肢ではありません。', code='invalid_choice')], 'from_header': [ErrorDetail(string='Not a valid string.', code='invalid')], 'reply_to_header': [ErrorDetail(string='Not a valid string.', code='invalid')], 'from_name': [ErrorDetail(string='Not a valid string.', code='invalid')], 'from_address': [ErrorDetail(string='有効なメールアドレスを入力してください。', code='invalid')], 'to_header': [ErrorDetail(string='Not a valid string.', code='invalid')], 'cc_header': [ErrorDetail(string='Not a valid string.', code='invalid')], 'subject': [ErrorDetail(string='Not a valid string.', code='invalid')], 'text': [ErrorDetail(string='Not a valid string.', code='invalid')], 'html': [ErrorDetail(string='Not a valid string.', code='invalid')], 'sent_date': [ErrorDetail(string='日時の形式が違います。以下のどれかの形式にしてください: YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]。', code='invalid')]}, Company: 3efd25d0-989a-4fba-aa8d-b91708760eb1"

        # Assuming SKIP_ON_ERROR environment variable is set to True.
        with self.settings(SKIP_ON_ERRORS=True):
            with self.assertLogs('', level='WARNING') as logs:
                with self.assertRaises(EmailDataError):
                    print("#####################")
                    print(data)
                    print("#####################")
                    print(self.company)
                    print("#####################")
                    save_email(data=data, company=self.company)
            self.assertEqual(expect_error_log, logs.output[0])

    def test_copy_email_with_html_body(self):
        original_message = Message.objects.get(pk=1)
        # HTML形式のメール本文
        original_message.body = 'UmV0dXJuLVBhdGg6IDw+ClgtT3JpZ2luYWwtVG86IHN1enVraUBoLWJhc2lzLmNvLmpwCkRlbGl2ZXJlZC1Ubzogc3V6dWtpQGgtYmFzaXMuY28uanAKUmVjZWl2ZWQ6IGJ5IGgtYmFzaXMuY28uanAgKFBvc3RmaXgpCglpZCBBQTUxRTIwN0Y1RkY7IEZyaSwgIDIgSnVsIDIwMjEgMTc6MjA6MDQgKzA5MDAgKEpTVCkKRGF0ZTogRnJpLCAgMiBKdWwgMjAyMSAxNzoyMDowNCArMDkwMCAoSlNUKQpGcm9tOiBNQUlMRVItREFFTU9OQGgtYmFzaXMuY28uanAgKE1haWwgRGVsaXZlcnkgU3lzdGVtKQpTdWJqZWN0OiBVbmRlbGl2ZXJlZCBNYWlsIFJldHVybmVkIHRvIFNlbmRlcgpUbzogc3V6dWtpQGgtYmFzaXMuY28uanAKQXV0by1TdWJtaXR0ZWQ6IGF1dG8tcmVwbGllZApNSU1FLVZlcnNpb246IDEuMApDb250ZW50LVR5cGU6IG11bHRpcGFydC9yZXBvcnQ7IHJlcG9ydC10eXBlPWRlbGl2ZXJ5LXN0YXR1czsKCWJvdW5kYXJ5PSIzRTY1MDIwN0Y1RkIuMTYyNTIxNDAwNC9oLWJhc2lzLmNvLmpwIgpDb250ZW50LVRyYW5zZmVyLUVuY29kaW5nOiA4Yml0Ck1lc3NhZ2UtSWQ6IDwyMDIxMDcwMjA4MjAwNC5BQTUxRTIwN0Y1RkZAaC1iYXNpcy5jby5qcD4KCi0tM0U2NTAyMDdGNUZCLjE2MjUyMTQwMDQvaC1iYXNpcy5jby5qcApDb250ZW50LURlc2NyaXB0aW9uOiBOb3RpZmljYXRpb24KQ29udGVudC1UeXBlOiB0ZXh0L3BsYWluOyBjaGFyc2V0PXVzLWFzY2lpCgpUaGlzIGlzIHRoZSBtYWlsIHN5c3RlbSBhdCBob3N0IGgtYmFzaXMuY28uanAuCgpJJ20gc29ycnkgdG8gaGF2ZSB0byBpbmZvcm0geW91IHRoYXQgeW91ciBtZXNzYWdlIGNvdWxkIG5vdApiZSBkZWxpdmVyZWQgdG8gb25lIG9yIG1vcmUgcmVjaXBpZW50cy4gSXQncyBhdHRhY2hlZCBiZWxvdy4KCkZvciBmdXJ0aGVyIGFzc2lzdGFuY2UsIHBsZWFzZSBzZW5kIG1haWwgdG8gcG9zdG1hc3Rlci4KCklmIHlvdSBkbyBzbywgcGxlYXNlIGluY2x1ZGUgdGhpcyBwcm9ibGVtIHJlcG9ydC4gWW91IGNhbgpkZWxldGUgeW91ciBvd24gdGV4dCBmcm9tIHRoZSBhdHRhY2hlZCByZXR1cm5lZCBtZXNzYWdlLgoKICAgICAgICAgICAgICAgICAgIFRoZSBtYWlsIHN5c3RlbQoKPGNjY0BleGFtcGxlLmNvbT46IE5hbWUgc2VydmljZSBlcnJvciBmb3IgbmFtZT1leGFtcGxlLmNvbSB0eXBlPU1YOiBNYWxmb3JtZWQKICAgIG9yIHVuZXhwZWN0ZWQgbmFtZSBzZXJ2ZXIgcmVwbHkKCi0tM0U2NTAyMDdGNUZCLjE2MjUyMTQwMDQvaC1iYXNpcy5jby5qcApDb250ZW50LURlc2NyaXB0aW9uOiBEZWxpdmVyeSByZXBvcnQKQ29udGVudC1UeXBlOiBtZXNzYWdlL2RlbGl2ZXJ5LXN0YXR1cwoKUmVwb3J0aW5nLU1UQTogZG5zOyBoLWJhc2lzLmNvLmpwClgtUG9zdGZpeC1RdWV1ZS1JRDogM0U2NTAyMDdGNUZCClgtUG9zdGZpeC1TZW5kZXI6IHJmYzgyMjsgc3V6dWtpQGgtYmFzaXMuY28uanAKQXJyaXZhbC1EYXRlOiBGcmksICAyIEp1bCAyMDIxIDE3OjIwOjAzICswOTAwIChKU1QpCgpGaW5hbC1SZWNpcGllbnQ6IHJmYzgyMjsgY2NjQGV4YW1wbGUuY29tCk9yaWdpbmFsLVJlY2lwaWVudDogcmZjODIyO2NjY0BleGFtcGxlLmNvbQpBY3Rpb246IGZhaWxlZApTdGF0dXM6IDUuNC40CkRpYWdub3N0aWMtQ29kZTogWC1Qb3N0Zml4OyBOYW1lIHNlcnZpY2UgZXJyb3IgZm9yIG5hbWU9ZXhhbXBsZS5jb20gdHlwZT1NWDoKICAgIE1hbGZvcm1lZCBvciB1bmV4cGVjdGVkIG5hbWUgc2VydmVyIHJlcGx5CgotLTNFNjUwMjA3RjVGQi4xNjI1MjE0MDA0L2gtYmFzaXMuY28uanAKQ29udGVudC1EZXNjcmlwdGlvbjogVW5kZWxpdmVyZWQgTWVzc2FnZQpDb250ZW50LVR5cGU6IG1lc3NhZ2UvcmZjODIyCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDhiaXQKClJldHVybi1QYXRoOiA8c3V6dWtpQGgtYmFzaXMuY28uanA+ClJlY2VpdmVkOiBmcm9tIHN1enVraW5vbWFjYm9vay1wcm8ubG9jYWwgKGgxNzUtMTc3LTA0MS0wMjYuY2F0djAyLml0c2NvbS5qcCBbMTc1LjE3Ny40MS4yNl0pCglieSBoLWJhc2lzLmNvLmpwIChQb3N0Zml4KSB3aXRoIEVTTVRQQSBpZCAzRTY1MDIwN0Y1RkI7CglGcmksICAyIEp1bCAyMDIxIDE3OjIwOjAzICswOTAwIChKU1QpCkNvbnRlbnQtVHlwZTogdGV4dC9odG1sOyBjaGFyc2V0PSJ1dGYtOCIKTUlNRS1WZXJzaW9uOiAxLjAKQ29udGVudC1UcmFuc2Zlci1FbmNvZGluZzogYmFzZTY0ClN1YmplY3Q6IHBpa2FwaWthCkZyb206IHN1enVraUBoLWJhc2lzLmNvLmpwClRvOiBzaGludGFyby5zLmIuekBnbWFpbC5jb20KQ2M6IGNjY0BleGFtcGxlLmNvbQpEYXRlOiBGcmksIDAyIEp1bCAyMDIxIDA4OjIwOjAzIC0wMDAwCk1lc3NhZ2UtSUQ6IAogPDE2MjUyMTQwMDMyNC42NjcyNy4xMDUwMDE2Nzg5NTcxOTE4NDA1NUBzdXp1a2lub21hY2Jvb2stcHJvLmxvY2FsPgoKRFFvOGNISmxQZzBLYlc5bmRXMXZaM1VOQ25SbGMzUWc1b3VGNWIyVDZJQ0ZJT2FubUEwS1BDOXdjbVUrRFFvOFluSStEUXB3YVd0aApjR2xyWVR4aWNqNE5DanhpY2o0TkNqeHdjbVUrS2lvcUtpb3FLaW9xS2lvTkNpQkJiaUJCWkcxcGJpQnZaaUJsZUdGdGNHeGxMbU52CmJTQm9iMmRsYUc5blpXaHZaMlZvYjJkbERRcDFjMlZ5WVdSdGFXNUFaWGhoYlhCc1pTNWpiMjBOQ25WelpYSmhaRzFwYmtCbGVHRnQKY0d4bExtTnZiUTBLZFhObGNtRmtiV2x1UUdWNFlXMXdiR1V1WTI5dERRcDFjMlZ5WVdSdGFXNUFaWGhoYlhCc1pTNWpiMjBOQ25WegpaWEpoWkcxcGJrQmxlR0Z0Y0d4bExtTnZiUTBLS2lvcUtpb3FLaW9xS2lvTkN1S0F1K09CaXVXVmorV1FpT09CbStPQWdlT0JsT2FQCmtPYWhpT09CcStPQnBPT0JoT09CcHVPQnIyMWhhV3pqZ2F2amdhYmpnWlRvdjVUa3Y2SGpncExqZ1lycG9aampnWVRqZ1pmamdiN2oKZ1puamdJSU5DdUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVQpnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdRMEs1cUNxNWJ5UDVMeWE1NlMrNDRPWTQ0T3I0NEs1CjQ0T1o0NEtrNDRLMzQ0SzVEUXBvZEhSd2N6b3ZMMmd0WW1GemFYTXVZMjh1YW5BdkRRcHRZV2xzNzd5YWMyVnpRR2d0WW1GemFYTXUKWTI4dWFuQU5DdWFMaGVXOWsrKzhtdWEzc2VlVXNBMEs0b0M3SU9PQml1YUFwZU9CanVPQnJ1V2d0T1dRaU9PQnIrT0FnZVdMbmVlVQpzTys4aURBM01ETXlNRFV5TnpFejc3eUo0NEcrNDRHbjQ0R1U2WUNqNTdXaDQ0R1A0NEdnNDRHVjQ0R0VEUW9OQ3VPQWtPYWRzZVM2CnJPT0NxdU9EbGVPQ28rT0N1ZU9Ba1EwSzQ0Q1NNVFV3TFRBd01UTU5DdWFkc2VTNnJPbUR2ZWE0aStpd3QrV011dWFCdGVhdmxPV3YKdnpNdE16Z3RNeUFOQ3VPRG1PT0RxK09DdWVPRG1lT0NwT09DdCtPQ3VlT0RrK09EcXcwSzQ0Q1E1YVNuNlppcTQ0S3E0NE9WNDRLago0NEs1NDRDUkRRcmpnSkkxTkRFdE1EQTFNK1drcCttWXF1VzZuT1drcCttWXF1VzRndVM0cmVXa3J1V011dWFjck9lVXVqUXRNaTB4Ck1nMEs1cDJ4NklxZDVhU242WmlxNDRPVDQ0T3JPRVlOQ3VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1UKZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnZUtVZ2VLVWdlS1VnVHd2Y0hKbApQZzBLRFFvPQoKLS0zRTY1MDIwN0Y1RkIuMTYyNTIxNDAwNC9oLWJhc2lzLmNvLmpwLS0K'

        instance = store_email(sender=None, message=original_message, company=self.company, ignore_domain='example.com')
        self.assertIn("<pre>\rmogumogu\rtest 担当者 様\r</pre>", instance.html)

    def test_copy_email_with_no_date(self):
        original_message = Message.objects.get(pk=1)
        # body内のDateが設定されていない
        original_message.body = 'UmV0dXJuLVBhdGg6IDxtZWxlcnJvci0xMTQ1OC0zMTY1ODRAbWVsbWVsLnRodW5kZXItYmlyZC5iaXo+ClgtT3JpZ2luYWwtVG86IHNlc0BoLWJhc2lzLmNvLmpwCkRlbGl2ZXJlZC1Ubzogc2hhcmVfdW1haWxAaC1iYXNpcy5jby5qcApSZWNlaXZlZDogYnkgaC1iYXNpcy5jby5qcCAoUG9zdGZpeCkKCWlkIDY2N0IzMjFGM0E1NTsgTW9uLCAxNiBBdWcgMjAyMSAwOTowMDoyNCArMDkwMCAoSlNUKQpEZWxpdmVyZWQtVG86IHNlc0BoLWJhc2lzLmNvLmpwClJlY2VpdmVkOiBmcm9tIG1haWwyLnRodW5kZXItYmlyZC5iaXogKG1haWwyLnRodW5kZXItYmlyZC5iaXogWzE1My4xMjAuOTMuMTU1XSkKCWJ5IGgtYmFzaXMuY28uanAgKFBvc3RmaXgpIHdpdGggRVNNVFAgaWQgNTk0M0QyMUYzQTRBCglmb3IgPHNlc0BoLWJhc2lzLmNvLmpwPjsgTW9uLCAxNiBBdWcgMjAyMSAwOTowMDoyNCArMDkwMCAoSlNUKQpSZWNlaXZlZDogZnJvbSAxOTIuMTY4LjAuNiAodW5rbm93biBbMTkyLjE2OC4wLjFdKQoJYnkgbWFpbDIudGh1bmRlci1iaXJkLmJpeiAoUG9zdGZpeCkgd2l0aCBFU01UUEEgaWQgM0Q1NEJBMkRCRQoJZm9yIDxzZXNAaC1iYXNpcy5jby5qcD47IE1vbiwgMTYgQXVnIDIwMjEgMDk6MDA6MjQgKzA5MDAgKEpTVCkKVG86IHNlc0BoLWJhc2lzLmNvLmpwClN1YmplY3Q6ID0/aXNvLTIwMjItanA/Qj9HeVJDSVZwS1FEeFNRMjFPVHpCR04yOGtSeVE1SVNvaGR6UndTMXdsYWlWaUlUd2JLRUk9Pz0KCT0/aXNvLTIwMjItanA/Qj9HeVJDSlVnaFd5VkhKU01sYkNVdkpUOGhQQnNvUWk4YkpFSXhQMDFSUTJZYktFSlhSVUliSkVJbE54c29RZz09Pz0KCT0/aXNvLTIwMjItanA/Qj9HeVJDSlRrbFJpVmdNbjQ5SkJzb1FpOGJKRUlsS0NWekpVbEVQaUZhSlRrbFVTVWtKV2tsYXhzb1FnPT0/PQoJPT9pc28tMjAyMi1qcD9CP0d5UkNKVHNsY3lVNUlWc2JLRUk9Pz0KRnJvbTogPT9pc28tMjAyMi1qcD9CP0d5UkNKVGtsVVNVa0pXa2xheVU3SlhNbE9TRWhKVklsWlNFOEpWNGxjeVU5SldvYktFST0/PQoJPT9pc28tMjAyMi1qcD9CP0d5UkNKV1VoUENVM0pXY2xjenQyTmtoSmRCc29RZz09Pz0gPHNhbGVzQHNwaXJhbHNlbnNlLmpwPgpNZXNzYWdlLUlkOiA8UWRtYWlsLjEuMi42Yl82NTgxMWIxNTQ1ZGM4ODZmYzRjYTI0ZDYyN2M1OTkyNmU3YTRkM2Q2QGl0bS1hc3AuY29tPgpDb250ZW50LVR5cGU6IHRleHQvcGxhaW47IGNoYXJzZXQ9Imlzby0yMDIyLWpwIgpDb250ZW50LVRyYW5zZmVyLUVuY29kaW5nOiA3Yml0Ck1JTUUtVmVyc2lvbjogMS4wClgtUEhQLVFkbWFpbDogdmVyc2lvbi0xLjIuNmIgVGhlX01JVF9MaWNlbnNlIGh0dHA6Ly9oYWw0NTYubmV0L3FkbWFpbCBQSFB2ZXIgNS42LjQKCXNlbmQtYnkgU01UUAoKGyRCM3Q8MDJxPFIlWCVrJTklWSUkJTclORsoQgoKGyRCJDRDNEV2PFRNTRsoQgoKGyRCJCQkRCRiJCpAJE9DJEskSiRDJEYkKiRqJF4kOSEjGyhCChskQiU5JVElJCVpJWslOyVzJTkzdDwwMnE8UhsoQkhTGyRCO3Y2SEl0JEckNCQ2JCQkXiQ5ISMbKEIKChskQkpAPFI8UjB3JEhMPjtJOHI0OSRyJDUkOyRGJCQkPyRAJCQkP0p9JEskKkF3JGokNSQ7JEYkJCQ/JEAkJCRGJCokaiReJDkhIxsoQgoKGyRCOD06XyEiMjw1LTBGN28kRz9NOmAkckpnPTgkNyRGJCokaiReJDkhIxsoQgobJEI4dUpkPFQkTkp9JCwkJCRpJEMkNyRjJCQkXiQ3JD8kaSEiJDREczBGJE4kWyRJJGgkbSQ3JC8kKjRqJCQkJCQ/JDckXiQ5ISMbKEIKIAobJEIiKDFENkg2JkRMJE4lYSE8JWslIiVJJWwlOSRyOm5ALiQkJD8kNyReJDckPyEjGyhCCiAgGyRCOiM4ZSEiMEY3bz5wSnMhIj9NOmAkTiQ0PlIycCRLJEQkJCRGJE8wSjI8JE4lYSE8JWslIiVJJWwlOSReJEcbKEIKICAbJEIkNE8iTW0kciQqNGokJCQkJD8kNyReJDkhIxsoQgoKICBFLW1haWw6c2FsZXNAc3BpcmFsc2Vuc2UuanAKCioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKgobJEIiI0pnPTg/JjxvGyhCChskQiVHJSMlbCUvJT8hPBsoQgoKGyRCIiMwRjdvRmJNRhsoQgobJEI4PTpfMT9NUUNmJE4bKEJXRUIbJEIlNSE8JVMlOSRLJCokJCRGJUclIyVsJS8lPyE8JEgkNyRGJTclOSVGJWAyfj0kJEs3SCRvJEMkRiQkJD8kQCQtJF4kOSEjGyhCCgobJEIhWjZxQk5FKiRKNkhMM0ZiTUYhWxsoQgobJEIhJiVXJW0lOCUnJS8lSDRJTX02SEwzGyhCKBskQiU5JTElOCVlITwlazRJTX0kYjReJGAbKEIpChskQiEmTVc3b0RqNUEkKiRoJFNAXzdXJEtJLE1XJEolSSUtJWUlYSVzJUg6bkAuGyhCChskQiEmMytILyVhJXMlUCE8JGQlRyU2JSQlSiE8JEgkTj5wSnNPIjdIGyhCChskQiEmPFJGYjghPlolYSVzJVAhPCRIJE5ENEAwNkhMMxsoQgoKGyRCIVo0cEtcPnBKcyFbGyhCChskQj5sPWohJ087S1xMWiIoNHBLXCVqJWIhPCVIJW8hPCUvGyhCChskQj9NP3QhJxsoQjEbJEJMPhsoQgobJEJDMTJBISclOSUtJWs4KzlnJCQbKEIKGyRCQDY7OyEnGyhCMTQwaC0xODBoChskQjJURi8hJxsoQjkbJEI3biEhMX5BakNMGyhCChskQjt+NFYhJxsoQjEwOjAwfjE5OjAwChskQkl+QXUhJztkSX4bKEIKGyRCPiZOLiEnJSglcyVJIipKQDxSGyhCChskQj1qQjAhJzhmPFI9akIwJF4kPyRPOGY8UhsoQjEbJEI8UjI8PWpCMCReJEcbKEIKGyRCISEhISEhIigbKEIxGyRCPFIyPCROSn0+bDlnGyhCKBskQiVVJWohPCVpJXMlOSRiNF4kYBsoQikbJEIxRDZIO1kxZ0hxJEskRiQ0QlAxfiRyJCo0aiQkJCQkPyQ3JF4kOSEjGyhCChskQkxMQ0whJxsoQjEbJEIycxsoQigbJEJKQDxSRjFAShsoQikKGyRCO1lKJyEnGyhCNDAbJEJGfCU1JSQlSBsoQigbJEJFdjduS3ZEeSRhGyhCLxskQk1iITk3bhsoQjEwGyRCRnw7WUonJCQbKEIpCgobJEIhWkksP1wlOSUtJWshJjdQODMhWxsoQgobJEIhJjMrSC8lRyUjJWwlLyU3JWclczdQODMbKEIKGyRCISYlbyUkJWQhPCVVJWwhPCVgISIyaExMQSswXD9eISIlNSUkJUglXiVDJVc6bkAuN1A4MxsoQgobJEIhJhsoQkhUTUwbJEIhIhsoQkNTUxskQiRONHBBQ0UqJEpDTjwxGyhCChskQiEmGyhCQmFja2xvZxskQiRKJEkkck14TVEkNyQ/GyhCV2lraSgbJEIlXiE8JS8lQCUmJXMbKEIpGyRCN1A4MxsoQgobJEIhJiVXJW0lOCUnJS8lSDRJTX02SEwzJEs9YCQ6JGsbKEJXQlMbJEIhIiUsJXMlQSVjITwlSDpuQC4bKEIKGyRCISYlYSE8JWskZCUzJV8lZSVLJTEhPCU3JWclcyVEITwlayRyTVEkJCQ/MzBJdCRIJE4lUyU4JU0lOUUqJEpKOD5PJEckTiRkJGokSCRqGyhCChskQiEmGyhCTWljcm9zb2Z0T2ZmaWNlGyRCJHJNeE1RJDckPzt2TDNFKiRKJTklLSVrGyhCCgobJEIhWjQ/N14lOSUtJWshJjdQODMhWxsoQgobJEIhJhsoQkFXUxskQiROQ048MRsoQgoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioKGyRCPlw6WSRPMjw1LUM0RXY8VCReJEckKkxkJCQ5ZyRvJDskLyRAJDUkJCEjGyhCCgobJEIhWj4uPVAhWxsoQgpNT0IbJEIhJxsoQjA3MC01MzcwLTM4NDAKRS1tYWlsGyRCIScbKEJrb2lkZUBzcGlyYWxzZW5zZS5qcAoKGyRCIVpAUDg2IVsbKEIKTU9CGyRCIScbKEIwNzAtNjQ4NC04MjA1CkUtbWFpbBskQiEnGyhCaXNoaWhhcmFAc3BpcmFsc2Vuc2UuanAKChskQjFENkg2JkRMGyhCRS1tYWlsGyRCIScbKEJzYWxlc0BzcGlyYWxzZW5zZS5qcAoKGyRCMEY3b0ZiTUYkSzgrOWckJkp9JCwkJCRpJEMkNyRjJCQkXiQ3JD8kaSEiQCdIcyQ0PlIycCQvJEAkNSQkISMbKEIKIAobJEIiKEV2MEY3bz5wSnMlYSE8JWskLCQ0SVRNVyROSn0kTyEiJCo8aj90JEckOSQsJDRPIk1tJCQkPyRAJDEkXiQ5JGgkJhsoQgogIBskQiRoJG0kNyQvJCo0aiQkJCQkPyQ3JF4kOSEjGyhCCioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKgobJEI0SkMxJW8lcyU5JUYlQyVXJEclOSUtJWslNyE8JUg6bkAuISobKEIKGyRCIXklOSUtJWslNyE8JUglYSE8JSshPCF5GyhCCmh0dHBzOi8vc2tpbGxzaGVldC5zcGlyYWxzZW5zZS5qcAoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioKGyRCJTklUSUkJWklayU7JXMlOTN0PDAycTxSGyhCChskQiFjPTs9aiFkGyhCChskQiIpGyhCMTUxLTAwNTEKGyRCISFFbDV+RVQ9QkMrNmhAaUJMJXZDKxsoQjQtMTYtNgobJEIhISUoJVUlbyVzS0w7MkY7GyhCMzAxClRFTBskQiEnGyhCMDMtNjQzNC05MzM1CgobJEIwbEhMR0k4Lzt2Nkg1djJESFY5ZhsoQiAgIBskQkhMGyhCMTMtMzEwODEwChskQk0tTkE/JjZIPlIycDV2MkRIVjlmISEbKEIxMy0bJEIlZhsoQi0zMDYxMjUKCkUtbWFpbBskQiEnGyhCc2FsZXNAc3BpcmFsc2Vuc2UuanAKVVJMGyRCIScbKEJodHRwOi8vc3BpcmFsc2Vuc2UuanAvCgoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqChskQiVhITwlayUiJUklbCU5SlE5OSEiR1s/LkRkO18kTyQzJEEkaSQrJGkbKEIKKEVtYWlsIGFkZHJlc3MgY2hhbmdlLCBVbnN1YnNjcmliZSBmcm9tIHRoZSBtYWlsaW5nIGxpc3QgRnJvbSBoZXJlKQpodHRwczovL3d3dy5tZWxzdG9wLmNvbS9tZWxzdG9wLzE5NjAwMjcyMi9jMlZ6UUdndFltRnphWE11WTI4dWFuQWpJM0poYm1SaGJTTWpNalUyTVEqKgo='

        instance = store_email(sender=None, message=original_message, company=self.company, ignore_domain='example.com')
        now = datetime.datetime.now()
        self.assertEqual(instance.sent_date.year, now.year)
        self.assertEqual(instance.sent_date.month, now.month)
        self.assertEqual(instance.sent_date.day, now.day)


class EmailDecodeTestCase(AttachmentPathMockingMixin, MediaFolderSetupMixin, TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):  # This is essential for attachment test.
        super().setUpTestData()
        # Checking fixture conditions.
        cls.original_message = Message.objects.get(pk=1)
        cls.original_message_attachments = MessageAttachment.objects.filter(message_id__exact=cls.original_message)
        assert cls.original_message_attachments[0].get_filename() == 'text1.txt'  # Depends on fixture data.

    fixtures = [
        'app_staffing/tests/fixtures/django_mailbox_messages.json',
        'app_staffing/tests/fixtures/django_mailbox_message_attachments.json',
    ]

    def test_decoding_unknown_8bit_subject(self):
        mock = MagicMock()  # This is a mock for the email content.
        mock.return_value = open(file='app_staffing/tests/services/email/data/unknown_8bit_subject', mode='r').read()

        original_message = Message.objects.get(pk=1)  #  Don't care about the original values, because the content is patched up.
        with patch('app_staffing.services.email.store_email._get_original_body', mock):
            actual = extract_message_data(original_message, 'test_company_id_for logging')
            self.assertEqual('【直案件/東京23区内】AWS・GCP/インフラクラウド基盤構築要員募集', actual['subject'])

    def test_rescue_filename_that_lost_original_filename(self):
        mock = MagicMock()
        mock.return_value = None
        attachment = MessageAttachment.objects.get(message_id__exact=self.original_message)
        attachment.get_filename = mock

        expected = 'Document.txt'  # If original file name is not defined,
        # filename will be "Document" + "stored file extension"
        actual = _get_file_name(attachment)

        self.assertEqual(expected, actual)

    def test_rescue_filename_that_lost_both_original_filename_and_path(self):
        mock = MagicMock()
        mock.return_value = None
        attachment = MessageAttachment.objects.get(message_id__exact=self.original_message)
        attachment.get_filename = mock
        attachment.document.file.name = None

        # The filename will be "ファイルを正しく取り込めませんでした" when we cant get neither file path and file name.
        actual = _get_file_name(attachment)
        expected = 'ファイルを正しく取り込めませんでした'


class EmailSignalAttachmentTestCase(AttachmentPathMockingMixin, MediaFolderSetupMixin, TestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/django_mailbox_messages.json',
        'app_staffing/tests/fixtures/django_mailbox_message_attachments.json',
        'app_staffing/tests/fixtures/emails.json'
    ]

    @classmethod
    def setUpTestData(cls):
        cls.company = Company.objects.get(id=DEFAULT_COMPANY_ID)  # Depends on fixture data.

    def test_save_attachments(self):
        original_attachment = MessageAttachment.objects.get(pk=1)
        att_name = original_attachment.get_filename()
        att_file = original_attachment.document.file

        attachment = {'name': att_name, 'file': att_file}

        # This is not an email that correspond to an original message,
        # But it is OK to test this method.
        email = Email.objects.get(pk=EMAIL_FIXTURE_ID)
        save_attachments(email=email, attachments=[attachment], company=self.company)

        created_attachments = EmailAttachment.objects.filter(email_id__exact=email.id)
        self.assertEqual(len(created_attachments), 1)
        created_attachment = created_attachments[0]

        # Data assertion
        self.assertEqual(att_name, created_attachment.name)
        self.assertEqual(None, created_attachment.created_user)  # A batch user name is defined in settings.py,
        # should not change basically.
        self.assertEqual(INBOUND, created_attachment.category)

class EmailAttachmentSizeOverTestCase(AttachmentPathMockingMixin, MediaFolderSetupMixin, TestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/django_mailbox_messages.json',
        'app_staffing/tests/fixtures/django_mailbox_message_attachments.json',
    ]
    maxDiff = None

    @classmethod
    def setUpTestData(cls):  # This is essential for attachment test.
        super().setUpTestData()
        cls.original_message = Message.objects.get(pk=1)
        cls.original_message_attachments = MessageAttachment.objects.filter(message_id__exact=cls.original_message)
        cls.company = Company.objects.get(id=DEFAULT_COMPANY_ID)

        assert cls.original_message_attachments[0].get_filename() == 'text1.txt'

    @override_settings(SHARED_EMAIL_MAX_BYTE_SIZE=1)
    def test_attachment_size_over(self):
        with self.assertLogs(logger='', level='ERROR') as cm:
            instance = store_email(sender=None, message=self.original_message, company=self.company, ignore_domain=None)
            self.assertIsNone(instance)
            self.assertEqual(len(cm.output), 1)
            self.assertIn('attachment size over', cm.output[0])

class EmailSelfDecodeTestCase(AttachmentPathMockingMixin, MediaFolderSetupMixin, TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/email_encoding/django_mailbox_messages.json',
        'app_staffing/tests/fixtures/django_mailbox_message_attachments.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_self_decode(self):
        original_message = Message.objects.get(pk=1)

        decoded_mail = _get_original_body(original_message)
        headers = get_header(decoded_mail)
        self.assertEqual('Outsystems=?iso-2022-jp?B?GyRCRXkkck1RJCQkPyVtITwlMyE8JUkzKxsoQg==?= 発支援', headers.get('Subject'))

        actual = extract_message_data(original_message, 'test_company_id_for logging')
        self.assertEqual('Outsystems等を用いたローコード開発支援', actual['subject'])

        original_message = Message.objects.get(pk=2)

        decoded_mail = _get_original_body(original_message)
        headers = get_header(decoded_mail)
        self.assertEqual(
            '=?iso-2022-jp?Q?=1B=24B0F7o=3EpJs=1B=28B=5B220426=2D01_=1B=24?= =?iso-2022-jp?Q?B418xD=23=1B=28B_=2D_=1B=24BBg5=2CLO=257=259=25F=25?= =?iso-2022-jp?Q?=600=5C9T=24KH=3C=24=26=1B=28BPMO=1B=24B=3BY1g=1B=28?= B]',
            headers.get('Subject')
        )
        self.assertEqual('"営業@シャタ� �システムズ" <eigyo@shataisys.com>', headers.get('From'))

        actual = extract_message_data(original_message, 'test_company_id_for logging')
        self.assertEqual('案件情報[220426-01 官公庁 - 大規模システム移行に伴うPMO支援]', actual['subject'])
        self.assertEqual('営業@シャタイシステムズ<eigyo@shataisys.com>', actual['from_header'])
