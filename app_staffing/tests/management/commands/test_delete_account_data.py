from django.test import TestCase

from django.core.management import call_command
from django.conf import settings
from django.core import mail

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID
from app_staffing.models import User, CompanyAttribute

from app_staffing.models.addon import Addon
from app_staffing.models.email import Email, EmailAttachment, EmailComment, MailboxMapping, SMTPServer, ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailOpenHistory, ScheduledEmailSearchCondition, ScheduledEmailSendError, ScheduledEmailSetting, ScheduledEmailTarget, SharedEmailSetting, SharedEmailTransferHistory
from app_staffing.models.notification import EmailNotificationCondition, EmailNotificationRule, UserSystemNotification
from app_staffing.models.organization import Contact, ContactCC, ContactCategory, ContactComment, ContactScore, ContactTagAssignment, DisplaySetting, ExceptionalOrganization, Organization, OrganizationBranch, OrganizationComment, Tag, UserDisplaySetting
from app_staffing.models.plan import Plan, PlanPaymentError
from app_staffing.models.preferences import ContactJobSkillPreference, ContactJobTypePreference, ContactPersonnelSkillPreference, ContactPersonnelTypePreference, ContactPreference
from app_staffing.models.purchase_history import PurchaseHistory
from app_staffing.models.stats import ContactStat, OrganizationStat, ScheduledEmailStat


from datetime import datetime, timedelta

class DeleteAccountDataCommandTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/delete_account_data/company.json',
        'app_staffing/tests/fixtures/delete_account_data/users.json',
        'app_staffing/tests/fixtures/system_notifications.json',
    ]

    COMPANY_A = '3efd25d0-989a-4fba-aa8d-b91708760eb1'
    COMPANY_B = '0639df05-8db5-40f2-a25d-6bcd3f8ae4ca'

    COMPANY_A_HIDDEN_USER = '6dba67ff-4bb7-4210-afec-f6fbec1c5fcb'
    COMPANY_B_HIDDEN_USER = '5dba67ff-4bb7-4210-afec-f6fbec1c5fcb'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        basedate = datetime.now().date()

        hidden_user_a = User.objects.get(id=cls.COMPANY_A_HIDDEN_USER)
        hidden_user_b = User.objects.get(id=cls.COMPANY_B_HIDDEN_USER)

        organization_a = Organization.objects.create(company_id=cls.COMPANY_A, score=1)
        organization_b = Organization.objects.create(company_id=cls.COMPANY_B, score=1)

        OrganizationBranch.objects.create(company_id=cls.COMPANY_A, organization=organization_a)
        OrganizationBranch.objects.create(company_id=cls.COMPANY_B, organization=organization_b)

        contact_a = Contact.objects.create(company_id=cls.COMPANY_A, organization=organization_a)
        contact_b = Contact.objects.create(company_id=cls.COMPANY_B, organization=organization_b)

        scheduledemail_a = ScheduledEmail.objects.create(company_id=cls.COMPANY_A, sender=hidden_user_a)
        scheduledemail_b = ScheduledEmail.objects.create(company_id=cls.COMPANY_B, sender=hidden_user_b)

        email_a = Email.objects.create(company_id=cls.COMPANY_A, subject='hoge')
        email_b = Email.objects.create(company_id=cls.COMPANY_B, subject='fuga')

        tag_a = Tag.objects.create(company_id=cls.COMPANY_A)
        tag_b = Tag.objects.create(company_id=cls.COMPANY_B)

        plan_a = Plan.objects.create(company_id=cls.COMPANY_A)
        plan_b = Plan.objects.create(company_id=cls.COMPANY_B)

        rule_a = EmailNotificationRule.objects.create(company_id=cls.COMPANY_A)
        rule_b = EmailNotificationRule.objects.create(company_id=cls.COMPANY_B)

        SharedEmailTransferHistory.objects.create(email=email_a, receiver=hidden_user_a, sender=hidden_user_a)
        SharedEmailTransferHistory.objects.create(email=email_b, receiver=hidden_user_b, sender=hidden_user_b)

        EmailNotificationCondition.objects.create(rule=rule_a, order=1)
        EmailNotificationCondition.objects.create(rule=rule_b, order=1)

        UserDisplaySetting.objects.create(user=hidden_user_a)
        UserDisplaySetting.objects.create(user=hidden_user_b)

        UserSystemNotification.objects.create(user=hidden_user_a, system_notification_id=1)
        UserSystemNotification.objects.create(user=hidden_user_b, system_notification_id=1)

        ScheduledEmailSearchCondition.objects.create(email=scheduledemail_a)
        ScheduledEmailSearchCondition.objects.create(email=scheduledemail_b)

        ScheduledEmailSendError.objects.create(email=scheduledemail_a, contact=contact_a)
        ScheduledEmailSendError.objects.create(email=scheduledemail_b, contact=contact_b)

        ScheduledEmailOpenHistory.objects.create(email=scheduledemail_a, contact=contact_a)
        ScheduledEmailOpenHistory.objects.create(email=scheduledemail_b, contact=contact_b)

        SMTPServer.objects.create(company_id=cls.COMPANY_A, port_number=457)
        SMTPServer.objects.create(company_id=cls.COMPANY_B, port_number=457)

        ScheduledEmailStat.objects.create(company_id=cls.COMPANY_A, target_date=basedate)
        ScheduledEmailStat.objects.create(company_id=cls.COMPANY_B, target_date=basedate)

        ContactStat.objects.create(company_id=cls.COMPANY_A, target_date=basedate)
        ContactStat.objects.create(company_id=cls.COMPANY_B, target_date=basedate)

        OrganizationStat.objects.create(company_id=cls.COMPANY_A, target_date=basedate)
        OrganizationStat.objects.create(company_id=cls.COMPANY_B, target_date=basedate)

        PurchaseHistory.objects.create(company_id=cls.COMPANY_A, plan=plan_a, period_start=basedate, period_end=basedate, price=100, method=1)
        PurchaseHistory.objects.create(company_id=cls.COMPANY_B, plan=plan_b, period_start=basedate, period_end=basedate, price=100, method=1)

        DisplaySetting.objects.create(company_id=cls.COMPANY_A)
        DisplaySetting.objects.create(company_id=cls.COMPANY_B)

        ContactPersonnelSkillPreference.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactPersonnelSkillPreference.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactPersonnelTypePreference.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactPersonnelTypePreference.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactJobSkillPreference.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactJobSkillPreference.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactJobTypePreference.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactJobTypePreference.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactCategory.objects.create(company_id=cls.COMPANY_A, contact=contact_a, user=hidden_user_a)
        ContactCategory.objects.create(company_id=cls.COMPANY_B, contact=contact_b, user=hidden_user_b)

        ContactComment.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactComment.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactScore.objects.create(company_id=cls.COMPANY_A, contact=contact_a, user=hidden_user_a, score=3)
        ContactScore.objects.create(company_id=cls.COMPANY_B, contact=contact_b, user=hidden_user_b, score=3)

        ContactCC.objects.create(company_id=cls.COMPANY_A, contact=contact_a)
        ContactCC.objects.create(company_id=cls.COMPANY_B, contact=contact_b)

        ContactTagAssignment.objects.create(company_id=cls.COMPANY_A, contact=contact_a, tag=tag_a)
        ContactTagAssignment.objects.create(company_id=cls.COMPANY_B, contact=contact_b, tag=tag_b)

        ExceptionalOrganization.objects.create(company_id=cls.COMPANY_A, organization=organization_a)
        ExceptionalOrganization.objects.create(company_id=cls.COMPANY_B, organization=organization_b)

        OrganizationComment.objects.create(company_id=cls.COMPANY_A, organization=organization_a)
        OrganizationComment.objects.create(company_id=cls.COMPANY_B, organization=organization_b)

        ScheduledEmailSetting.objects.create(company_id=cls.COMPANY_A)
        ScheduledEmailSetting.objects.create(company_id=cls.COMPANY_B)

        ScheduledEmailTarget.objects.create(company_id=cls.COMPANY_A, contact=contact_a, email=scheduledemail_a)
        ScheduledEmailTarget.objects.create(company_id=cls.COMPANY_B, contact=contact_b, email=scheduledemail_b)

        ScheduledEmailAttachment.objects.create(company_id=cls.COMPANY_A, email=scheduledemail_a, name='hoge')
        ScheduledEmailAttachment.objects.create(company_id=cls.COMPANY_B, email=scheduledemail_b, name='fuga')

        SharedEmailSetting.objects.create(company_id=cls.COMPANY_A)
        SharedEmailSetting.objects.create(company_id=cls.COMPANY_B)

        EmailComment.objects.create(company_id=cls.COMPANY_A, email=email_a)
        EmailComment.objects.create(company_id=cls.COMPANY_B, email=email_b)

        EmailAttachment.objects.create(company_id=cls.COMPANY_A, email=email_a)
        EmailAttachment.objects.create(company_id=cls.COMPANY_B, email=email_b)

        Addon.objects.create(company_id=cls.COMPANY_A)
        Addon.objects.create(company_id=cls.COMPANY_B)

    def test_execute(self):
        with self.assertRaises(SystemExit) as command:
            call_command(command_name='delete_account_data')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)
        
        # データが削除されていることを確認
        self.assertEqual(User.objects.filter(company_id=self.COMPANY_A).count(), 1)
        self.assertEqual(Organization.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(OrganizationBranch.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(Contact.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(Email.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(Tag.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(Plan.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(EmailNotificationRule.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(SharedEmailTransferHistory.objects.filter(email__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(EmailNotificationCondition.objects.filter(rule__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(UserDisplaySetting.objects.filter(user__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(UserSystemNotification.objects.filter(user__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailSearchCondition.objects.filter(email__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailSendError.objects.filter(email__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailOpenHistory.objects.filter(email__company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(SMTPServer.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailStat.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactStat.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(OrganizationStat.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(PurchaseHistory.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(DisplaySetting.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactPersonnelSkillPreference.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactPersonnelTypePreference.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactJobSkillPreference.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactJobTypePreference.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactPreference.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactCategory.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactComment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactScore.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactCC.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ContactTagAssignment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ExceptionalOrganization.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(OrganizationComment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailSetting.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailTarget.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(ScheduledEmailAttachment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(SharedEmailSetting.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(EmailComment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(EmailAttachment.objects.filter(company_id=self.COMPANY_A).count(), 0)
        self.assertEqual(Addon.objects.filter(company_id=self.COMPANY_A).count(), 0)

        # 別テナントのデータは削除されていないことを確認
        self.assertEqual(User.objects.filter(company_id=self.COMPANY_B).count(), 2)
        self.assertEqual(Organization.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(OrganizationBranch.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(Contact.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmail.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(Email.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(Tag.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(Plan.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(EmailNotificationRule.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(SharedEmailTransferHistory.objects.filter(email__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(EmailNotificationCondition.objects.filter(rule__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(UserDisplaySetting.objects.filter(user__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(UserSystemNotification.objects.filter(user__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailSearchCondition.objects.filter(email__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailSendError.objects.filter(email__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailOpenHistory.objects.filter(email__company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(SMTPServer.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailStat.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactStat.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(OrganizationStat.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(PurchaseHistory.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(DisplaySetting.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactPersonnelSkillPreference.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactPersonnelTypePreference.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactJobSkillPreference.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactJobTypePreference.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactPreference.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactCategory.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactComment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactScore.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactCC.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ContactTagAssignment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ExceptionalOrganization.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(OrganizationComment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailSetting.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailTarget.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(ScheduledEmailAttachment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(SharedEmailSetting.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(EmailComment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(EmailAttachment.objects.filter(company_id=self.COMPANY_B).count(), 1)
        self.assertEqual(Addon.objects.filter(company_id=self.COMPANY_B).count(), 1)

