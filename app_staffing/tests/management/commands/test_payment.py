from django.core.management import call_command
from django.test import TestCase
from django.conf import settings
from datetime import datetime, timedelta, date
from app_staffing.models import Plan, Addon, PurchaseHistory, CompanyAttribute, PlanPaymentError, CardPaymentError, Company
from app_staffing.models.user import User
from app_staffing.tests.helpers.mock_payjp_api import MockingPayjpAPIHelper
from dateutil.relativedelta import relativedelta
import payjp
import pytz
from unittest.mock import patch
from django.core import mail
from freezegun import freeze_time

from app_staffing.utils.expiration_time import get_payment_date

class PaymentTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company_for_batch.json',
        'app_staffing/tests/fixtures/payjp/plan.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/payjp/master_users.json',
    ]

    TENANT_A = '3efd25d0-989a-4fba-aa8d-b91708760eb9'
    TENANT_B = '3efd25d0-989a-4fba-aa8d-b91708760eb8'
    TENANT_C = '3efd25d0-989a-4fba-aa8d-b91708760eb7'
    TENANT_D = '3efd25d0-989a-4fba-aa8d-b91708760eb6'
    TENANT_E = '3efd25d0-989a-4fba-aa8d-b91708760eb5'
    TENANT_F = '3efd25d0-989a-4fba-aa8d-b91708760eb4'
    TENANT_G = '3efd25d0-989a-4fba-aa8d-b91708760eb3'
    TENANT_H = '3efd25d0-989a-4fba-aa8d-b91708760eb2'
    TENANT_I = '3efd25d0-989a-4fba-aa8d-b91708760eb1'

    @classmethod
    def mock_Charge_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        # 不正なクレジットカードの例外を発生させる
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb8': # TENANT_B
            raise Exception('No such customer')
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb3': # TENANT_G
            raise Exception('expired_card')
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb2': # TENANT_H
            raise Exception('card_declined')

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 0
        data = []
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb9':
            # TENANT_A
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb9',
                    'amount': 33000,
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb4':
            # TENANT_B
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb4',
                    'amount': 55550,
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb3':
            # TENANT_G
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb3',
                    'failure_code': 'expired_card',
            }]
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb2':
            # TENANT_H
            cnt = 1
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb2',
                    'failure_code': 'card_declined',
            }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')

    @classmethod
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def setUpClass(cls):
        super().setUpClass()
        payjp.api_key = settings.PAYJP_API_KEY
        # テナントAの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_A)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_A)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントFの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_F)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_F)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントGの顧客情報とクレジットカード情報(エラー)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_G)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_G)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000004012',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass
    
        # テナントHの顧客情報とクレジットカード情報(エラー)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_H)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_H)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

    @classmethod
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def tearDownClass(cls):
        super().tearDownClass()
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(cls.TENANT_A)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_F)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_G)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_H)
        customer.delete()
    
    def setUp(self):
        now = datetime.now()
        # テナントA：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_A,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_A,
            user_registration_limit=5,
        )
        # テナントB：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_B,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=2)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_B,
            user_registration_limit=5,
        )
        # テナントC：支払い対象外
        Plan.objects.create(
            company_id=self.TENANT_C,
            plan_master_id=1,
            is_active=True,
            payment_date=(now + timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_C,
            user_registration_limit=5,
        )
        # テナントE：プランが2つ存在する(データ異常)
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=3)).date(),
        )
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=3)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_E,
            user_registration_limit=5,
        )
        # テナントF：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_F,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=4)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_F,
            user_registration_limit=10,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=1,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=2,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=2,
        )
        Addon.objects.create(
            company_id=self.TENANT_F,
            addon_master_id=4,
        )
        # テナントG：支払い対象だがクレジットカード情報が不正
        Plan.objects.create(
            company_id=self.TENANT_G,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=5)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_G,
            user_registration_limit=5,
        )
        # テナントH：支払い対象だがクレジットカード情報が不正
        Plan.objects.create(
            company_id=self.TENANT_H,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=6)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_H,
            user_registration_limit=5,
        )
        # テナントI：退会しているため支払い対象外
        Plan.objects.create(
            company_id=self.TENANT_I,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=7)).date(),
        )
        company_i = Company.objects.get(id=self.TENANT_I)
        company_i.deactivated_time = now
        company_i.save()
        CompanyAttribute.objects.create(
            company_id=self.TENANT_I,
            user_registration_limit=5,
        )

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    def test_command_payment(self):
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        planA_payment_date = get_payment_date(plans[0].payment_date)
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        planB_payment_date = plans[0].payment_date # 更新されない
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        planF_payment_date = get_payment_date(plans[0].payment_date)

        now = datetime.now()
        with self.assertLogs(logger='', level='ERROR') as cm:
            with self.assertRaises(SystemExit) as command:
                call_command('payment')
        
        company_id_and_errors = [
            {'company_id': self.TENANT_I, 'error': 'company deactivated'},
            {'company_id': self.TENANT_H, 'error': 'card_declined'},
            {'company_id': self.TENANT_G, 'error': 'expired_card'},
            {'company_id': self.TENANT_E, 'error': 'multiple active plans exists'},
            {'company_id': self.TENANT_D, 'error': 'no active plan'},
            {'company_id': self.TENANT_C, 'error': 'no active plan'},
            {'company_id': self.TENANT_B, 'error': 'No such customer'},
        ]

        self.assertEqual(command.exception.code, 0)
        self.assertEqual(len(cm.output), len(company_id_and_errors))

        for i in range(0, len(cm.output)):
            company_id_and_error_found = False
            for company_id_and_error in company_id_and_errors:
                if company_id_and_error['company_id'] in cm.output[i] and company_id_and_error['error'] in cm.output[i]:
                    company_id_and_error_found = True

            if not company_id_and_error_found:
                self.fail('company id or error message does not found in error log')

        # テナントAのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planA_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planA_payment_date.day)

        # テナントAの支払いが行われていることを確認する
        payjp.api_key = settings.PAYJP_API_KEY
        charges = payjp.Charge.all(customer=self.TENANT_A, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['amount'], 33000)

        # テナントAのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_A)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) * 1.1 = 33000
        self.assertEqual(purchase_histories[0].price, 33000)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=1)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)

        # テナントBのプランが更新されていないことを確認する(トランザクションが切られているためロールバックされる)
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planB_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planB_payment_date.day)

        # テナントBの支払いが行われていないことを確認する(クレジットカードが登録されていないためエラーになる)
        charges = payjp.Charge.all(customer=self.TENANT_B, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_B).exists())

        # テナントCの支払いが行われていないことを確認する(プランの期限が切れていないため対象にならない)
        charges = payjp.Charge.all(customer=self.TENANT_C, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)

        # テナントDの支払いが行われていないことを確認する(プラン情報がない)
        charges = payjp.Charge.all(customer=self.TENANT_D, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)

        # テナントEの支払いが行われていないことを確認する(プラン情報が複数存在する)
        charges = payjp.Charge.all(customer=self.TENANT_E, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)

        # テナントFのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planF_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planF_payment_date.day)

        # テナントFの支払いが行われていることを確認する
        charges = payjp.Charge.all(customer=self.TENANT_F, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        # プランの料金(30000) + アドオンの料金(500 + 500 + 500 + 4000 = 5500) + ユーザー追加料金(5000 * 5 = 15000) = 50500
        # 50500 * 1.1 = 55550
        self.assertEqual(charges['data'][0]['amount'], 55550)

        # テナントFのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_F)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) + アドオンの料金(500 + 500 + 500 + 4000 = 5500) + ユーザー追加料金(5000 * 5 = 15000) = 50500
        # 50500 * 1.1 = 55550
        self.assertEqual(purchase_histories[0].price, 55550)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=4)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)
        
        # テナントGの支払いが行われていないことを確認する(カード情報が不正)
        charges = payjp.Charge.all(customer=self.TENANT_G, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['failure_code'], 'expired_card')

        # テナントGのエラー情報が保存されていることを確認する
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_G).exists())

        # テナントHの支払いが行われていないことを確認する(カード情報が不正)
        charges = payjp.Charge.all(customer=self.TENANT_H, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        self.assertEqual(charges['data'][0]['failure_code'], 'card_declined')

        # テナントHのエラー情報が保存されていることを確認する
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_H).exists())

        # テナントIの支払いが行われていないことを確認する(退会テナント)
        charges = payjp.Charge.all(customer=self.TENANT_I, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 0)

        emails = [email for email in mail.outbox]
        self.assertEqual(len(emails), 5)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[0].subject)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[1].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[2].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[3].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[4].subject)

        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', emails[0].message().as_string())
        user = User.objects.get(company_id=self.TENANT_F)
        self.assertIn(user.display_name, emails[0].message().as_string())

        self.assertIn('お支払いの処理に失敗いたしましたのでご連絡いたします。', emails[2].message().as_string())
        user = User.objects.get(company_id=self.TENANT_H)
        self.assertIn(user.display_name, emails[2].message().as_string())

class PaymentWithBackupCardTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company_for_batch_with_backup_card.json',
        'app_staffing/tests/fixtures/payjp/plan.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/payjp/master_users_with_backup_card.json',
    ]

    TENANT_J = '3efd25d0-989a-4fba-aa8d-b91708760eb0'
    TENANT_K = '3efd25d0-989a-4fba-aa8d-b91708760ea9'

    @classmethod
    def mock_Charge_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        # 不正なクレジットカードの例外を発生させる
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb0': # TENANT_J
            if params['card'] == 'mock_card_id_main':
                raise Exception('card_declined')
        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760ea9': # TENANT_K
            if params['card'] == 'mock_card_id_main':
                raise Exception('card_declined')
            if params['card'] == 'mock_card_id_backup':
                raise Exception('card_declined')

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 0
        data = []
        if params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760eb0':
            # TENANT_J
            cnt = 2
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760ea9',
                    'failure_code': 'card_declined',
            },
            {
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb0',
                    'amount': 33000,
            }]

        elif params['customer'] == '3efd25d0-989a-4fba-aa8d-b91708760ea9':
            # TENANT_K
            cnt = 2
            data = [{
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760ea9',
                    'failure_code': 'card_declined',
            },
            {
                    'customer': '3efd25d0-989a-4fba-aa8d-b91708760ea9',
                    'failure_code': 'expired_card',
            }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')

    @classmethod
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def setUpClass(cls):
        super().setUpClass()
        payjp.api_key = settings.PAYJP_API_KEY

        # テナントJの顧客情報とクレジットカード情報(１枚目がエラー、２枚目が有効)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_J)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_J)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=True)
        except Exception:
            pass
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass

        # テナントKの顧客情報とクレジットカード情報(２枚目ともエラー)登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_J)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_J)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=True)
        except Exception:
            pass
        res_post_token = payjp.Token.create(
            card={
                'number' : '4000000000004012',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'], default=False)
        except Exception:
            pass
    
    @classmethod
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve_with_backup_card)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def tearDownClass(cls):
        super().tearDownClass()
        customer = payjp.Customer.retrieve(cls.TENANT_J)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_K)
        customer.delete()
    
    def setUp(self):
        now = datetime.now()
        # テナントJ：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_J,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_J,
            user_registration_limit=5,
        )
        # テナントK：支払い対象
        Plan.objects.create(
            company_id=self.TENANT_K,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=1)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_K,
            user_registration_limit=5,
        )

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve_with_backup_card)
    @patch("payjp.resource.Charge.create", mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    def test_command_payment(self):
        plans = Plan.objects.filter(company_id=self.TENANT_J, is_active=True)
        planJ_payment_date = get_payment_date(plans[0].payment_date)
        plans = Plan.objects.filter(company_id=self.TENANT_K, is_active=True)
        planK_payment_date = plans[0].payment_date # 更新されない

        now = datetime.now()
        with self.assertLogs(logger='', level='ERROR') as cm:
            with self.assertRaises(SystemExit) as command:
                call_command('payment')
        
        company_id_and_errors = [
            {'company_id': self.TENANT_J, 'error': 'card_declined'},
            {'company_id': self.TENANT_K, 'error': 'card_declined'},
            {'company_id': self.TENANT_K, 'error': 'expired_card'},
        ]

        self.assertEqual(command.exception.code, 0)
        self.assertEqual(len(cm.output), len(company_id_and_errors))

        for i in range(0, len(cm.output)):
            company_id_and_error_found = False
            for company_id_and_error in company_id_and_errors:
                if company_id_and_error['company_id'] in cm.output[i] and company_id_and_error['error'] in cm.output[i]:
                    company_id_and_error_found = True

            if not company_id_and_error_found:
                self.fail('company id or error message does not found in error log')

        # テナントJのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_J, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, planJ_payment_date.month)
        self.assertEqual(plans[0].payment_date.day, planJ_payment_date.day)

        # テナントJの支払いが行われていることを確認する
        payjp.api_key = settings.PAYJP_API_KEY
        charges = payjp.Charge.all(customer=self.TENANT_J, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 2)
        self.assertEqual(charges['data'][0]['failure_code'], 'card_declined')
        self.assertEqual(charges['data'][1]['amount'], 33000)

        # テナントJのPaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.TENANT_J)
        self.assertEqual(len(purchase_histories), 1)
        # プランの料金(30000) * 1.1 = 33000
        self.assertEqual(purchase_histories[0].price, 33000)
        self.assertEqual(purchase_histories[0].period_start, (now - timedelta(days=1)).date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)

        # テナントJのカードエラーが作られていることを確認する
        self.assertTrue(CardPaymentError.objects.filter(company_id=self.TENANT_J, card_id="mock_card_id_main", card_type='main').exists())

        # テナントKの支払いが行われていないことを確認する(カード情報が不正)
        charges = payjp.Charge.all(customer=self.TENANT_K, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 2)
        self.assertEqual(charges['data'][0]['failure_code'], 'card_declined')
        self.assertEqual(charges['data'][1]['failure_code'], 'expired_card')

        # テナントKのエラー情報が保存されていることを確認する
        self.assertTrue(PlanPaymentError.objects.filter(company_id=self.TENANT_K).exists())

        # テナントKのカードエラーが作られていることを確認する
        self.assertTrue(CardPaymentError.objects.filter(company_id=self.TENANT_K, card_id="mock_card_id_main", card_type='main').exists())
        self.assertTrue(CardPaymentError.objects.filter(company_id=self.TENANT_K, card_id="mock_card_id_backup", card_type='sub').exists())

        emails = [email for email in mail.outbox]
        self.assertEqual(len(emails), 3)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', emails[0].subject)
        self.assertEqual('[コモレビ] 予備クレジットカードでの決済完了のご連絡', emails[1].subject)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', emails[2].subject)

        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', emails[0].message().as_string())
        user = User.objects.get(company_id=self.TENANT_J)
        self.assertIn(user.display_name, emails[0].message().as_string())

        self.assertIn('予備クレジットカードでの決済が行われましたのでご連絡いたします。', emails[1].message().as_string())
        user = User.objects.get(company_id=self.TENANT_J)
        self.assertIn(user.display_name, emails[1].message().as_string())

        self.assertIn('お支払いの処理に失敗いたしましたのでご連絡いたします。', emails[2].message().as_string())
        user = User.objects.get(company_id=self.TENANT_K)
        self.assertIn(user.display_name, emails[2].message().as_string())

class NextPaymentDateTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company_for_batch.json',
        'app_staffing/tests/fixtures/payjp/plan.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    TENANT_A = '3efd25d0-989a-4fba-aa8d-b91708760eb9'
    TENANT_B = '3efd25d0-989a-4fba-aa8d-b91708760eb8'
    TENANT_C = '3efd25d0-989a-4fba-aa8d-b91708760eb7'
    TENANT_D = '3efd25d0-989a-4fba-aa8d-b91708760eb6'
    TENANT_E = '3efd25d0-989a-4fba-aa8d-b91708760eb5'
    TENANT_F = '3efd25d0-989a-4fba-aa8d-b91708760eb4'

    @classmethod
    def mock_Charge_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        return True

    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        return payjp.resource.Charge.construct_from({
            'count': 1,
            'data': [{
                'customer': '3efd25d0-989a-4fba-aa8d-b91708760eb9',
                'amount': 33000,
            }]
        }, 'mock_api_key')

    @classmethod
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def setUpClass(cls):
        super().setUpClass()
        payjp.api_key = settings.PAYJP_API_KEY
        # テナントAの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_A)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_A)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントBの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_B)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_B)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントCの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_C)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_C)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントDの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_D)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_D)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントEの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_E)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_E)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass

        # テナントFの顧客情報とクレジットカード情報登録
        try:
            customer = payjp.Customer.retrieve(cls.TENANT_F)
        except Exception:
            customer = payjp.Customer.create(id=cls.TENANT_F)
        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        try:
            customer.cards.create(card=res_post_token['id'])
        except Exception:
            pass
    
    @classmethod
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def tearDownClass(cls):
        super().tearDownClass()
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(cls.TENANT_A)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_B)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_C)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_D)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_E)
        customer.delete()
        customer = payjp.Customer.retrieve(cls.TENANT_F)
        customer.delete()
    
    def setUp(self):
        # テナントA
        Plan.objects.create(
            company_id=self.TENANT_A,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2021, 10, 31),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_A,
            user_registration_limit=5,
        )
        # テナントB
        Plan.objects.create(
            company_id=self.TENANT_B,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2021, 11, 30),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_B,
            user_registration_limit=5,
        )
        # テナントC
        Plan.objects.create(
            company_id=self.TENANT_C,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2021, 12, 30),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_C,
            user_registration_limit=5,
        )
        # テナントD
        Plan.objects.create(
            company_id=self.TENANT_D,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2022, 1, 30),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_D,
            user_registration_limit=5,
        )
        # テナントE
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2022, 2, 28),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_E,
            user_registration_limit=5,
        )
        # テナントF：支払い対象だがクレジットカード情報が不正
        Plan.objects.create(
            company_id=self.TENANT_F,
            plan_master_id=1,
            is_active=True,
            payment_date=date(2022, 3, 28),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_F,
            user_registration_limit=5,
        )

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    def test_command_payment(self):
        freezer = freeze_time('2022-04-01')
        freezer.start()
        try:
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('payment')
        finally:
            freezer.stop()
        
        self.assertEqual(command.exception.code, 0)
        self.assertEqual(len(cm.output), 3)

        company_ids = [
            '3efd25d0-989a-4fba-aa8d-b91708760eb1',
            '3efd25d0-989a-4fba-aa8d-b91708760eb2',
            '3efd25d0-989a-4fba-aa8d-b91708760eb3'
        ]

        for i in range(0, 3):
            self.assertIn('no active plan', cm.output[i])

            company_id_found = False
            for company_id in company_ids:
                if company_id in cm.output[i]:
                    company_id_found = True

            if not company_id_found:
                self.fail('company id does not found in error log')


        # 決済日は以下のように遷移していく
        # 10/31 -> 11/30 -> 12/30 -> 1/30 -> 2/28 -> 3/28 -> 4/28...
        # 詳細は下記のアプリ設計シートを参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=1421658702

        # テナントAのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_A, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2021, 11, 30))

        # テナントBのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_B, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2021, 12, 30))

        # テナントCのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_C, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2022, 1, 30))

        # テナントDのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_D, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2022, 2, 28))

        # テナントEのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_E, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2022, 3, 28))

        # テナントFのプランが更新されていることを確認する
        plans = Plan.objects.filter(company_id=self.TENANT_F, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date, date(2022, 4, 28))
