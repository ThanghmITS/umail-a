from django.core.management import call_command
from django.test import TestCase
from django.conf import settings
from datetime import datetime, timedelta, date
from app_staffing.models import Plan, Company, CompanyAttribute
from dateutil.relativedelta import relativedelta

class FindPaymentOverdueTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/findPaymentOverdue/company.json',
        'app_staffing/tests/fixtures/findPaymentOverdue/plan.json',
    ]

    TENANT_A = '3efd25d0-989a-4fba-aa8d-b91708760eb9'
    TENANT_B = '3efd25d0-989a-4fba-aa8d-b91708760eb8'
    TENANT_C = '3efd25d0-989a-4fba-aa8d-b91708760eb7'
    TENANT_D = '3efd25d0-989a-4fba-aa8d-b91708760eb6'
    TENANT_E = '3efd25d0-989a-4fba-aa8d-b91708760eb5'

    def setUp(self):
        now = datetime.now()
        # テナントA：対象外
        Plan.objects.create(
            company_id=self.TENANT_A,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=9)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_A,
            user_registration_limit=5,
        )
        # テナントB：対象
        Plan.objects.create(
            company_id=self.TENANT_B,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=10)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_B,
            user_registration_limit=5,
        )
        # テナントC：対象
        Plan.objects.create(
            company_id=self.TENANT_C,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=11)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_C,
            user_registration_limit=5,
        )
        # テナントD：退会しているため対象外
        Plan.objects.create(
            company_id=self.TENANT_D,
            plan_master_id=1,
            is_active=True,
            payment_date=(now - timedelta(days=10)).date(),
        )
        company_d = Company.objects.get(id=self.TENANT_D)
        company_d.deactivated_time = now
        company_d.save()
        CompanyAttribute.objects.create(
            company_id=self.TENANT_D,
            user_registration_limit=5,
        )
        # テナントE：非活性のため対象外
        Plan.objects.create(
            company_id=self.TENANT_E,
            plan_master_id=1,
            is_active=False,
            payment_date=(now - timedelta(days=10)).date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.TENANT_E,
            user_registration_limit=5,
        )

    def test_command_payment(self):
        with self.assertLogs(logger='', level='INFO') as cm:
            with self.assertRaises(SystemExit) as command:
                call_command('find_payment_overdue')
        
        
        self.assertEqual(command.exception.code, 0)
        self.assertEqual(len(cm.output), 2)
        print(cm.output[0])

        company_name_list = [
            "Tenant B",
            "Tenant C",
        ]
        for c in company_name_list:
            if not c in cm.output[0]:
                self.fail('company:{0} does not found in log'.format(c))

        company_name_list = [
            "Tenant A",
            "Tenant D",
            "Tenant E",
        ]
        for c in company_name_list:
            if c in cm.output[0]:
                self.fail('company:{0} founds in log'.format(c))
