from django.test import TestCase

from django.core.management import call_command
from django.conf import settings
from django.core import mail

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID
from app_staffing.models import User, CompanyAttribute

from datetime import datetime, timedelta

class FreeTrialCommandTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user = User.objects.get(username=TEST_USER_NAME)
        user.user_role_id = 6
        user.save()

    def test_calculate_stats(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(datetime.now() - timedelta(days=1)).date(),
        )

        with self.assertRaises(SystemExit) as command:
            call_command(command_name='free_trial')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]無料トライアル終了のご連絡', email.subject)
        self.assertIn('無料トライアルが終了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

    def test_calculate_stats_not_target_past_date(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(datetime.now() - timedelta(days=2)).date(),
        )

        with self.assertRaises(SystemExit) as command:
            call_command(command_name='free_trial')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)
        self.assertEqual(len(mail.outbox), 0)

    def test_calculate_stats_not_target_future_date(self):
        CompanyAttribute.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            user_registration_limit=5,
            trial_expiration_date=(datetime.now() + timedelta(days=1)).date(),
        )

        with self.assertRaises(SystemExit) as command:
            call_command(command_name='free_trial')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)
        self.assertEqual(len(mail.outbox), 0)
