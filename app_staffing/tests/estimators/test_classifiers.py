from django.test import TestCase

from app_staffing.models import DummyModel
from app_staffing.models.email import Email, EMAIL_CATEGORIES
from app_staffing.estimators.classifiers import EmailClassifier
from app_staffing.tests.helpers.skip import skipUnlessIntegrated

FIXTURE_LABELED_MAIL_ID = '2c199464-49ac-4e6a-9ac2-408df2925d03'
FIXTURE_ESTIMATED_BUT_UNLABELED_MAIL_ID = '90ba907b-3102-4b71-a76d-640a0db3cfdb'


@skipUnlessIntegrated
class EmailClassifierTestCase(TestCase):
    """Test cases for estimator.
    WARNING: Run remote server or use stub to run this test.
    """
    _model_class = Email
    _estimator_class = EmailClassifier

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',  # This Must contains both labeled and unlabeled data.
    ]

    _available_categories = [tpl[0] for tpl in EMAIL_CATEGORIES]

    def _clean(self):
        self._model_class.objects.all().delete()

    def setUp(self):
        self.service = self._estimator_class()
        self.service.reset()

    def tearDown(self):
        self._clean()

    def test_batch_train(self):
        self.service.batch_train()

    def test_batch_predict(self):
        self.service.batch_train()
        self.service.batch_predict()

    def test_train(self):
        instance = self._model_class.objects.get(pk=FIXTURE_LABELED_MAIL_ID)  # Make sure that this instance is labeled one.
        self.service.train(instance=instance)

    def test_predict(self):
        self.service.batch_train()

        instance = self._model_class.objects.get(pk=FIXTURE_ESTIMATED_BUT_UNLABELED_MAIL_ID)
        label, result = self.service.predict(instance)

        # TODO: Use schema matching like Cerberus to validate the result format.
        self.assertIn(label, self._available_categories)
        self.assertTrue(isinstance(result, list))
        self.assertTrue(isinstance(result[0], tuple))
        self.assertTrue(isinstance(result[0][0], str))
        self.assertTrue(isinstance(result[0][1], float))

    def test_reset(self):
        self.service.reset()

    def test_predict_with_no_train_data(self):
        instance = self._model_class.objects.get(pk=FIXTURE_ESTIMATED_BUT_UNLABELED_MAIL_ID)
        with self.assertRaises(ValueError):  # TODO: Propose a custom exception class for jubakit official.
            label, result = self.service.predict(instance)

    def test_predict_with_wrong_data(self):
        instance = DummyModel()
        instance.name = 'test'
        instance.save()

        self.service.batch_train()
        with self.assertRaises(AssertionError):  # TODO: Use a custom exception class.
            label, result = self.service.predict(instance)
