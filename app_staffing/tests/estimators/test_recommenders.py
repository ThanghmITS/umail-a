from django.test import TestCase

from app_staffing.models.email import Email
from app_staffing.estimators.recommenders import JobEmailRecommender
from app_staffing.tests.helpers.skip import skipUnlessIntegrated

FIXTURE_LABELED_MAIL_ID = '64b83ee3-9830-459c-b264-0a63d3419238'
FIXTURE_UNLABELED_MAIL_ID = '90ba907b-3102-4b71-a76d-640a0db3cfdb'


@skipUnlessIntegrated
class JobEmailRecommenderTestCase(TestCase):
    """Test cases for estimator.
    WARNING: Run remote server or use stub to run this test.
    """
    _model_class = Email
    _estimator_class = JobEmailRecommender

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',  # This Must contains both labeled and unlabeled data.
    ]

    def _clean(self):
        self._model_class.objects.all().delete()

    def setUp(self):
        self.service = self._estimator_class()
        self.service.reset()

    def tearDown(self):
        self._clean()

    def test_train(self):
        instance = self._model_class.objects.get(pk=FIXTURE_UNLABELED_MAIL_ID)
        self.service.train(instance)

    def test_predict(self):
        # Register multiple objects before getting a recommendation.
        for instance in self._model_class.objects.all():
            self.service.train(instance)

        # Run the main test logic.
        instance = self._model_class.objects.get(pk=FIXTURE_UNLABELED_MAIL_ID)
        result = self.service.predict(instance=instance)

        # Validate the result.
        self.assertTrue(isinstance(result, list))
        for item in result:
            self.assertTrue(isinstance(item['id'], str))
            self.assertTrue(isinstance(item['score'], float))

    def test_remove(self):
        """This test assumes that the train / predict method is working."""
        # Register the instance to the remote server first.
        instance = self._model_class.objects.get(pk=FIXTURE_UNLABELED_MAIL_ID)
        self.service.train(instance)

        # Ensure that the instance is registered to the remote server.
        similar_entries_on_server = self.service.predict(instance)
        assert len(similar_entries_on_server) >= 1, "Could not complete this test because train or predict method is broken."

        # Then, Remove test starts.
        result = self.service.remove(instance)
        self.assertTrue(result)

        # Ensure that instance is no longer exists on the remote server.
        similar_entries_on_server = self.service.predict(instance)
        self.assertEqual(len(similar_entries_on_server), 0)

    def test_batch_train(self):
        self.service.batch_train()
        # TODO: assert that there are only new job data after the training process.

    def test_batch_remove(self):
        """This test assumes that the batch_train method is working.
        WARNING: Training data (Django model fixtures) entry's timestamp must be old enough to detect as old data.
        """
        self.service.batch_train()
        result = self.service.batch_remove()  # Delete old entries.

        # Validate the result.
        self.assertIsInstance(result, list)
        self.assertTrue(len(result) >= 1)  # This assertion will fail if training data is newer than DAYS_TO_KEEP_RECOMMENDATION.
        for item in result:
            self.assertIsInstance(item, dict)
            self.assertIsInstance(item['id'], str)
            self.assertIsInstance(item['result'], bool)

    # TODO: Implement error cases.
