from app_staffing.board.models import PersonnelCheckList, ProjectCheckList
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelCheckListFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    title = faker.text()

    class Meta:
        model = PersonnelCheckList


class ProjectCheckListFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    title = faker.text()

    class Meta:
        model = ProjectCheckList
