from app_staffing.board.models import PersonnelDynamicRow, ProjectDynamicRow
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelDynamicRowFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    title = faker.text()
    content = faker.text()

    class Meta:
        model = PersonnelDynamicRow

class ProjectDynamicRowFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    title = faker.text()
    content = faker.text()

    class Meta:
        model = ProjectDynamicRow
