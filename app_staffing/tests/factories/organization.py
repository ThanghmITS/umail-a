import factory
import faker

from factory import fuzzy

from app_staffing.models import User, Tag, ContactTagAssignment
from app_staffing.models.organization import Organization, OrganizationComment,\
    Contact, ContactComment, ContactScore, CLIENT, PROSPECTIVE, VERY_LOW
from app_staffing.models.preferences import ContactJobTypePreference, ContactPersonnelTypePreference, ContactPreference, ENTRUST_PROHIBITED, ENTRUST_NO_LIMITATION

from app_staffing.tests.factories.company import CompanyFactory

# A factory module that generates Organization related data for unit tests of Scheduled email.
# (send mail, mail marketing)

fake = faker.Faker()


# A Factory Mixin to update existing ContactPreference instead of creating new one.
class UpdateDefaultProfileMixin(object):

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        contact = kwargs.pop('contact')
        obj, created = model_class.objects.update_or_create(contact=contact, defaults=kwargs)
        return obj


# Users for tests.
class ApiUser(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = "apiuser"
    email = "apiuser@example.com"
    company = CompanyFactory()


class AnotherUser(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = "anotheruser"
    email = "anotheruser@example.com"
    company = CompanyFactory()


# Definitions for Organization
class GoodOrganization(factory.DjangoModelFactory):
    class Meta:
        model = Organization

    corporate_number = None
    name = 'A Good Company (Not blacklisted)'
    category = CLIENT
    employee_number = VERY_LOW
    contract = True
    country = 'JP'
    address = 'Good place'
    building = 'building'
    capital_man_yen = 10000
    capital_man_yen_required_for_transactions = None
    establishment_year = None
    score = 3
    is_blacklisted = False
    company = CompanyFactory()


class BadOrganization(factory.DjangoModelFactory):
    class Meta:
        model = Organization

    corporate_number = None
    name = 'Bad Company (blacklisted)'
    category = CLIENT
    employee_number = VERY_LOW
    contract = True
    country = 'JP'
    address = 'Bad place'
    building = 'building'
    capital_man_yen = 100
    capital_man_yen_required_for_transactions = None
    establishment_year = 0
    score = 1
    is_blacklisted = True
    company = CompanyFactory()


class OrganizationFactory(factory.DjangoModelFactory):
    corporate_number = None
    name = fake.company()
    category = fuzzy.FuzzyChoice(choices=[CLIENT, PROSPECTIVE])
    employee_number = VERY_LOW
    contract = False
    country = 'JP'
    address = '1-2-3 Tokyo Minatoku Japan'
    building = 'building'
    capital_man_yen = fuzzy.FuzzyInteger(100, 10000000)
    capital_man_yen_required_for_transactions = None
    establishment_year = 3
    score = 2
    is_blacklisted = False
    company = CompanyFactory()

    class Meta:
        model = Organization


class OrganizationCommentFactory(factory.DjangoModelFactory):
    organization = factory.SubFactory(OrganizationFactory, company=factory.SelfAttribute('..company'))
    content = fake.text()

    class Meta:
        model = OrganizationComment


class SmallOrganization(factory.DjangoModelFactory):
    """An Organization which dont care how much our capital is."""
    class Meta:
        model = Organization

    corporate_number = None
    name = 'A small Company (requires tiny capitals for transaction)'
    category = PROSPECTIVE
    employee_number = VERY_LOW
    contract = False
    country = 'CN'
    address = 'Beijing'
    building = 'building'
    capital_man_yen = 100
    capital_man_yen_required_for_transactions = 0
    establishment_year = None
    score = 4
    is_blacklisted = False
    company = CompanyFactory()


class TooBigOrganization(factory.DjangoModelFactory):
    """An Organization which requires strong capital to our company."""
    class Meta:
        model = Organization

    corporate_number = None
    name = 'Too big organization (requires huge capitals for transaction)'
    category = PROSPECTIVE
    employee_number = VERY_LOW
    contract = False
    country = 'CN'
    address = 'Shanghai'
    building = 'building'
    capital_man_yen = 100000000
    capital_man_yen_required_for_transactions = 100000000
    establishment_year = None
    score = 5
    is_blacklisted = False
    company = CompanyFactory()


# Definition for Contact.
class ContactFactory(factory.DjangoModelFactory):
    class Meta:
        model = Contact

    last_name = fake.last_name()
    first_name = fake.first_name()
    email = fake.email()
    position = 'test position'
    department = 'test department'
    staff = None
    last_visit = '2019-04-08'
    created_user = None
    company = CompanyFactory()
    organization = factory.SubFactory(
        OrganizationFactory,
        company=factory.SelfAttribute('..company'), created_user=factory.SelfAttribute('..created_user')
    )


class ContactCommentFactory(factory.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory, company=factory.SelfAttribute('..company'))
    content = fake.text()

    class Meta:
        model = ContactComment


class AbstractContact(factory.DjangoModelFactory):
    class Meta:
        model = Contact
        abstract = True

    position = None
    department = None
    staff = None
    last_visit = None
    company = CompanyFactory()


class BlacklistedCompanysContact(AbstractContact):
    last_name = 'A Contact who belongs to a blacklisted company'
    email = 'blacklisted@example.com'
    organization = factory.SubFactory(BadOrganization)


class RefuseAdsContact(AbstractContact):
    last_name = 'Do not want to receive marketing email'
    email = 'noMailPlz@example.com'
    organization = factory.SubFactory(GoodOrganization)


class AllOkContact(AbstractContact):
    last_name = 'Wants all item'
    email = 'allOk@example.com'
    organization = factory.SubFactory(GoodOrganization)


class AllNgContact(AbstractContact):
    last_name = 'Wants no item'
    email = 'allNg@example.com'
    organization = factory.SubFactory(GoodOrganization)


class JobOnlyContact(AbstractContact):
    last_name = 'Wants job item only'
    email = 'jobItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class PersonnelOnlyContact(AbstractContact):
    last_name = 'Wants person item only'
    email = 'PersonItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class InfraOnlyContact(AbstractContact):
    last_name = 'Wants Infra item only'
    email = 'InfraItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class DevelopmentOnlyContact(AbstractContact):
    last_name = 'Wants Development item only'
    email = 'DevelopmentItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class SalesOnlyContact(AbstractContact):
    last_name = 'Wants Sales item only'
    email = 'SalesItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class ProperOnlyContact(AbstractContact):
    last_name = 'Wants Proper item only'
    email = 'ProperItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class IndividualOnlyContact(AbstractContact):
    last_name = 'Wants Individual item only'
    email = 'IndividialItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class SkilledPersonOnlyContact(AbstractContact):
    last_name = 'Wants Skilled person only'
    email = 'SkilledPersonOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class PersonnelReEntrustProhibitedContact(AbstractContact):
    last_name = 'Personnel ReEntrust Prohibited'
    email = 'NonReEntrustPersonOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class JobReEntrustProhibitedContact(AbstractContact):
    last_name = 'Job ReEntrust Prohibited'
    email = 'NonReEntrustJobOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class AcceptsPersonViaWorkerDispatchContract(AbstractContact):
    last_name = 'Worker Dispatch Contract item only'
    email = 'WorkerDispatchItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class AcceptsPersonViaQuasiMandateContract(AbstractContact):
    last_name = 'Quasi Mandate Contract item only'
    email = 'QuasiMandateContractItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class LocationKantoOnlyContact(AbstractContact):
    last_name = 'Wants kanto Japan item only'
    email = 'KantoLocationItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class LocationKansaiOnlyContact(AbstractContact):
    last_name = 'Wants Kansai Japan item only'
    email = 'KansaiLocationItemOnly@example.com'
    organization = factory.SubFactory(GoodOrganization)


class RichContact(AbstractContact):
    last_name = 'Who requires strong capital to our company. (man yen)'
    email = 'superrich@example.com'
    organization = factory.SubFactory(TooBigOrganization)


class NotRichContact(AbstractContact):
    last_name = 'Who dont care how much our capital is.'
    email = 'notsorich@example.com'
    organization = factory.SubFactory(SmallOrganization)


class ScoreFactory(factory.DjangoModelFactory):
    score = 5  # override this when you use.
    contact = factory.SubFactory(AllOkContact)  # override this when you use.
    user = factory.SubFactory(ApiUser)  # override this when you use.
    company = CompanyFactory()

    class Meta:
        model = ContactScore


class TagFactory(factory.DjangoModelFactory):
    value = 'ChangeMe'  # override this when you use.
    company = CompanyFactory()

    class Meta:
        model = Tag
        django_get_or_create = ('value',)


class ContactTagAssignmentFactory(factory.DjangoModelFactory):
    contact = factory.SubFactory(AllOkContact)  # override this when you use.
    tag = factory.SubFactory(TagFactory)  # override this when you use.
    company = CompanyFactory()

    class Meta:
        model = ContactTagAssignment


# Definition for contact profile
class AllOkProfile(UpdateDefaultProfileMixin, factory.DjangoModelFactory):
    class Meta:
        model = ContactPreference

    contact = factory.SubFactory(AllOkContact)
    company = CompanyFactory()

    wants_location_kanto_japan = True
    wants_location_kansai_japan = True


class AllNgProfile(UpdateDefaultProfileMixin, factory.DjangoModelFactory):  # In a very rare case, this pattern exists.
    class Meta:
        model = ContactPreference

    contact = factory.SubFactory(AllNgContact)
    company = CompanyFactory()

    wants_location_kanto_japan = False
    wants_location_kansai_japan = False


class RichManProfile(AllOkProfile):
    contact = factory.SubFactory(RichContact)


class NotRichManProfile(AllOkProfile):
    contact = factory.SubFactory(NotRichContact)


class RefuseAdsProfile(AllOkProfile):
    contact = factory.SubFactory(RefuseAdsContact)


class BlacklistedProfile(AllOkProfile):
    contact = factory.SubFactory(BlacklistedCompanysContact)


class JobOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(JobOnlyContact)


class PersonnelOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(PersonnelOnlyContact)


class InfraProjectOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(InfraOnlyContact)


class DevelopmentProjectOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(DevelopmentOnlyContact)


class SalesProjectOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(SalesOnlyContact)


class ProperOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(ProperOnlyContact)


class IndividualOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(IndividualOnlyContact)


class PersonnelReEntrustProhibitedProfile(AllOkProfile):
    contact = factory.SubFactory(PersonnelReEntrustProhibitedContact)


class SkilledPersonOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(SkilledPersonOnlyContact)


class JobReEntrustProhibitedProfile(AllOkProfile):
    contact = factory.SubFactory(JobReEntrustProhibitedContact)


class WorkerDispatchPersonOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(AcceptsPersonViaWorkerDispatchContract)


class QuasiMandatePersonOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(AcceptsPersonViaQuasiMandateContract)


class KantoOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(LocationKantoOnlyContact)
    wants_location_kansai_japan = False


class KansaiOnlyProfile(AllOkProfile):
    contact = factory.SubFactory(LocationKansaiOnlyContact)
    wants_location_kanto_japan = False

class ContactJobTypePreferenceFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactJobTypePreference

    company = CompanyFactory()
    contact = None
    type_preference = None

class ContactPersonnelTypePreferenceFactory(factory.DjangoModelFactory):
    class Meta:
        model = ContactPersonnelTypePreference

    company = CompanyFactory()
    contact = None
    type_preference = None
