import factory
from app_staffing.models import User, UserRole

from app_staffing.tests.factories.company import CompanyFactory

class UserRoleFactory(factory.DjangoModelFactory):
    id = 1
    name = 'admin'
    order = 1

    class Meta:
        model = UserRole

class AppUserFactory(factory.DjangoModelFactory):
    user_service_id = 'user_123456789'
    username = 'Send Mail User'
    first_name = 'First Name'
    last_name = 'Last Name'
    email = 'sender@example.com'
    email_signature = '=======Signature====='
    password = 'ajke9gk!ks'
    is_superuser = False
    is_user_admin = True
    is_active = True
    company = CompanyFactory()

    class Meta:
        model = User
