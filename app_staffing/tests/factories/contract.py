from app_staffing.board.models import ProjectContract, PersonnelContract
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelContractFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    detail = 'detail'

    class Meta:
        model = PersonnelContract



class ProjectContractFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    class Meta:
        model = ProjectContract
