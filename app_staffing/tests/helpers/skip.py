from unittest import skipUnless, skipIf
from django.conf import settings

skipUnlessIntegrated = skipUnless(  # Use this object as a decorator.
    condition=settings.ENABLE_INTEGRATION_TEST,
    reason="This test only runs if you enable integration test feature in the settings."
)

skipIfIntegrated = skipIf(  # Use this object as a decorator.
    condition=settings.ENABLE_INTEGRATION_TEST,
    reason="This test only runs in a local environment."
)
