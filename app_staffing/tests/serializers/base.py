from abc import ABCMeta

from django.test import TestCase


class SerializerTestCase(TestCase):
    """A Base Abstract Class for TestCase of Serializers. (subclass of Django rest framework Serializer.)"""
    __metaclass__ = ABCMeta
    _serializer_class = None  # Override by child class!

    @property
    def _model_class(cls):  # Lazy evaluation of a class member.
        return cls._serializer_class.Meta.model
