from app_staffing.serializers import MyProfileSerializer
from app_staffing.tests.serializers.base import SerializerTestCase


class MyProfileSerializerTestCase(SerializerTestCase):
    _serializer_class = MyProfileSerializer
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json'
    ]

    def test_profile_representation(self):
        """This test confirms that the serializer only shows the limited number of fields of user."""
        expected_fields = ['first_name', 'last_name', 'email', 'email_signature', 'tel1', 'tel2', 'tel3', 'role',
                           'modified_time', 'modified_user__name', 'user_service_id', 'avatar']

        user = self._model_class.objects.all()[0]
        serialized_data = self._serializer_class(user).data

        # Assert that serialized data has all expected fields.
        for fields in expected_fields:
            self.assertTrue(fields in serialized_data.keys())

        # Assert that serialized data is not having extra fields.
        self.assertEqual(len(serialized_data.keys()), len(expected_fields))

    def test_profile_update(self):
        """Check if this serializer supports partial update and ignores read-only fields."""

        instance = self._model_class.objects.all()[0]

        user_input_data = {
            'first_name': 'Taro',
            'last_name': 'Yamada',
            'email': 'changed@example.com',
            'email_signature': 'test',
            'role': 'admin'
        }

        serializer = self._serializer_class(instance=instance, data=user_input_data, partial=True)
        serializer.is_valid(raise_exception=True)
        updated_instance = serializer.save()

        # Assert that all fields except read-only fields has been updated.
        self.assertEqual(updated_instance.first_name, user_input_data['first_name'])
        self.assertEqual(updated_instance.last_name, user_input_data['last_name'])

        # Assert that read-only fields are not updated at all.
        self.assertNotEqual(updated_instance.email, user_input_data['email'])
