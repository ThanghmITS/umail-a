from django.test import TestCase

from app_staffing.models import EmailNotificationRule, EmailNotificationCondition
from app_staffing.serializers import EmailNotificationRuleSerializer
from app_staffing.tests.factories.company import CompanyFactory


class EmailNotificationRuleSerializerTestCase(TestCase):
    data_for_post_test = {
        'name': 'Nested Rule',
        'master_rule': 'all',
        'conditions': [
            {
                'extraction_type': 'keyword',
                'target_type': 'subject',
                'condition_type': 'include',
                'value': 'newbie'
            },
            {
                'extraction_type': 'keyword',
                'target_type': 'content',
                'condition_type': 'exclude',
                'value': 'experienced'
            },
        ],
    }

    data_for_update_test = {
        'name': 'Updated Nested Rule',
        'master_rule': 'any',
        'conditions': [
            {
                'extraction_type': 'keyword',
                'target_type': 'content',
                'condition_type': 'exclude',
                'value': 'python'
            },
            {
                'extraction_type': 'keyword',
                'target_type': 'subject',
                'condition_type': 'include',
                'value': 'ruby'
            },
            {
                'extraction_type': 'keyword',
                'target_type': 'from_address',
                'condition_type': 'include',
                'value': 'user@example.com'
            },
        ],
    }

    def test_nested_create_and_update(self):
        # Execute creation test.
        # Add company info to separate data between tenants.
        company = CompanyFactory()
        # Execute creation test.
        data_for_post = {"company": company.id, **self.data_for_post_test}
        serializer = EmailNotificationRuleSerializer(data=data_for_post)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()

        # Assert created instance info.
        self.assertTrue(EmailNotificationRule.objects.get(id=instance.id))
        self.assertEqual(EmailNotificationCondition.objects.filter(rule_id=instance.id).count(),
                         len(self.data_for_post_test['conditions']))
        self.assertEqual(EmailNotificationRule.objects.get(id=instance.id).name, 'Nested Rule')
        self.assertEqual(EmailNotificationRule.objects.get(id=instance.id).master_rule, 'all')

        # Assert the order of conditions
        self.assertEqual(EmailNotificationCondition.objects.get(rule_id=instance.id, order=0).value, 'newbie')
        self.assertEqual(EmailNotificationCondition.objects.get(rule_id=instance.id, order=1).value, 'experienced')

        # Then, execute update test.
        data_for_update = {**self.data_for_update_test}
        serializer_for_update = EmailNotificationRuleSerializer(instance=instance, data=data_for_update, partial=True)
        serializer_for_update.is_valid(raise_exception=True)
        serializer_for_update.save()

        # Assert updated instance info.
        self.assertTrue(EmailNotificationRule.objects.get(id=instance.id))
        self.assertEqual(EmailNotificationCondition.objects.filter(rule_id=instance.id).count(),
                         len(self.data_for_update_test['conditions']))
        self.assertEqual(EmailNotificationRule.objects.get(id=instance.id).name, 'Updated Nested Rule')
        self.assertEqual(EmailNotificationRule.objects.get(id=instance.id).master_rule, 'any')

        # Assert the order of conditions
        self.assertEqual(EmailNotificationCondition.objects.get(rule_id=instance.id, order=0).value, 'python')
        self.assertEqual(EmailNotificationCondition.objects.get(rule_id=instance.id, order=1).value, 'ruby')
        self.assertEqual(EmailNotificationCondition.objects.get(rule_id=instance.id, order=2).value, 'user@example.com')
