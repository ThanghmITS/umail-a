from dateutil.relativedelta import relativedelta

from app_staffing.models import Contact, ContactJobTypePreference, ContactPersonnelTypePreference, ContactStat
from app_staffing.services.stats.base import Statistics

from django.db.models import Count, Q
from django.conf import settings

class ContactStats(Statistics):
    def calc_stats(self, target_date):
        company_id = self.get_company_id()
        from_date, to_datetime = self.get_target_from_to(target_date)

        result_job = ContactJobTypePreference.objects.select_related(
            'contact', 'contact__organization', 'type_preference'
        ).filter(
            company_id=company_id,
            contact__created_time__lt=to_datetime,
        ).aggregate(
            dev=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other']),
                distinct=True
            ),
            infra=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other']),
                distinct=True
            ),
            other=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['other_eigyo', 'other_kichi', 'other_support', 'other_other']),
                distinct=True
            ),
        )

        result_personnel = ContactPersonnelTypePreference.objects.select_related(
            'contact', 'contact__organization', 'type_preference'
        ).filter(
            company_id=company_id,
            contact__created_time__lt=to_datetime,
        ).aggregate(
            dev=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other']),
                distinct=True
            ),
            infra=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other']),
                distinct=True
            ),
            other=Count(
                'contact_id',
                filter=Q(type_preference__name__in=['other_eigyo', 'other_kichi', 'other_support', 'other_other']),
                distinct=True
            ),
        )

        result_announcement = Contact.objects.filter(
            company_id=company_id,
            created_time__lt=to_datetime,
        ).count()

        contact_stat = ContactStat.objects.filter(
            company_id=company_id,
            target_date=from_date,
        )

        if contact_stat:
            contact_stat[0].jobtype_dev=result_job['dev']
            contact_stat[0].jobtype_infra=result_job['infra']
            contact_stat[0].jobtype_other=result_job['other']
            contact_stat[0].personneltype_dev=result_personnel['dev']
            contact_stat[0].personneltype_infra=result_personnel['infra']
            contact_stat[0].personneltype_other=result_personnel['other']
            contact_stat[0].other=result_announcement
            contact_stat[0].save()
        else:
            ContactStat.objects.create(
                company_id=company_id,
                target_date=from_date,
                jobtype_dev=result_job['dev'],
                jobtype_infra=result_job['infra'],
                jobtype_other=result_job['other'],
                personneltype_dev=result_personnel['dev'],
                personneltype_infra=result_personnel['infra'],
                personneltype_other=result_personnel['other'],
                other=result_announcement,
            )

    def get_stats(self):
        company_id = self.get_company_id()
        base_datetime = self.get_base_datetime()
        from_datetime = base_datetime - relativedelta(months=self.get_month_range())
        labels = []
        values = {
            'jobs': {
                'development': [],
                'infrastructure': [],
                'others': [],
            },
            'personnel': {
                'development': [],
                'infrastructure': [],
                'others': [],
            },
            'announcement': [],
        }

        results =ContactStat.objects.filter(
            company_id=company_id,
            target_date__lt=base_datetime,
            target_date__gte=from_datetime
        ).order_by('target_date')[:settings.DASHBOARD_DISPLAY_MAX_COUNT]

        for result in results:
            labels.append(f'{result.target_date.year}/{result.target_date.month}')
            values['jobs']['development'].append(result.jobtype_dev)
            values['jobs']['infrastructure'].append(result.jobtype_infra)
            values['jobs']['others'].append(result.jobtype_other)
            values['personnel']['development'].append(result.personneltype_dev)
            values['personnel']['infrastructure'].append(result.personneltype_infra)
            values['personnel']['others'].append(result.personneltype_other)
            values['announcement'].append(result.other)

        return {
            'labels': labels,
            'values': values,
        }
