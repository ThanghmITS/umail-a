from abc import ABC
from datetime import datetime

from django.conf import settings

from dateutil.relativedelta import relativedelta

class Statistics(ABC):
    def __init__(self, company_id):
        self.__month_range = 6
        self.company_id = company_id
        if not self.company_id:
            raise ValueError('Please specify the ID of a company(tenant) for calculations.')

    def get_month_range(self):
        return self.__month_range

    def get_company_id(self):
        return self.company_id

    def get_base_datetime(self):
        today = datetime.today()
        return datetime(year=today.year, month=today.month, day=1)

    def get_target_from_to(self, target_date):
        from_date = to_date = None
        if target_date is None:
            # target_date が未指定の場合は (先月１日, 今月１日)を返す
            to_date = self.get_base_datetime()
            from_date = to_date - relativedelta(months=1)
        else:
            # target_date が指定されている場合は (指定年月の１日, 指定年月翌月の１日)を返す
            from_date = datetime(year=target_date.year, month=target_date.month, day=1)
            to_date = from_date + relativedelta(months=1)
        return from_date, to_date
