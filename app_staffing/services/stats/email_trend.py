from dateutil.relativedelta import relativedelta

from app_staffing.models.email import Email
from app_staffing.services.stats.base import Statistics


class EmailTrendStats(Statistics):
    _STATS_ID = 'RxEmail_Count_Trend'

    def _calc_stats(self, company_id, base_datetime):
        month_range = 6

        labels = [
            '{year}/{month}'.format(
                year=(base_datetime - relativedelta(months=x)).year,
                month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ) for x in reversed(range(1, month_range + 1))
        ]

        job_email_counts = [
            Email.objects.filter(
                has_attachments=False,
                sent_date__year=(base_datetime - relativedelta(months=x)).year,
                sent_date__month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ).count() for x in reversed(range(1, month_range + 1))
        ]

        personnel_email_counts = [
            Email.objects.filter(
                has_attachments=True,
                sent_date__year=(base_datetime - relativedelta(months=x)).year,
                sent_date__month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ).count() for x in reversed(range(1, month_range + 1))
        ]

        return {
            'labels': labels,
            'job_email_counts': job_email_counts,
            'personnel_email_counts': personnel_email_counts,
        }
