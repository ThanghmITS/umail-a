from dateutil.relativedelta import relativedelta
from django.db.models import Count, CharField, Value, F
from django.db.models.functions import Concat
from django.conf import settings

from app_staffing.models.email import Email
from app_staffing.models.stats import StaffInChargeStat
from app_staffing.services.stats.base import Statistics


class StaffInChargeStats(Statistics):
    _partition = "|"
    _empty_char = "_"

    def calc_stats(self, target_date):
        company_id = self.get_company_id()
        from_date, to_date = self.get_target_from_to(target_date)

        email_shared_queryset = Email.objects.filter(
            company_id=company_id,
            sent_date__gte=from_date,
            sent_date__lt=to_date,
            shared=True,
        ).select_related('staff_in_charge')

        queryset_group_by_staff = email_shared_queryset.filter(staff_in_charge__isnull=False).values(
            "staff_in_charge__id"
        ).annotate(
            count=Count("staff_in_charge__id"),
            full_name=Concat(
                F('staff_in_charge__last_name'), Value(' '), F('staff_in_charge__first_name'),
                output_field=CharField(),
            ),
        ).order_by("-count", "staff_in_charge__created_time")[:5]

        # prepare values for insert_or_update StaffInChargeStat
        top_values, staff_in_charge = [], []
        total_of_top, last_index = 0, -1
        for index, email in enumerate(queryset_group_by_staff):
            top_values.append(str(email.get("count")))
            staff_in_charge.append(email.get("full_name") or self._empty_char)
            total_of_top += email.get("count")
            last_index = index

        while last_index < 4:
            last_index += 1
            top_values.append("0")
            staff_in_charge.append(self._empty_char)

        top_values.append(str(email_shared_queryset.count() - total_of_top))
        staff_in_charge.append("other")

        StaffInChargeStat.objects.update_or_create(
            company_id=company_id, target_date=from_date,
            defaults={
                "top_values": self._partition.join(top_values),
                "staff_in_charge": self._partition.join(staff_in_charge),
            },
        )

    def get_stats(self):
        company_id = self.get_company_id()
        base_datetime = self.get_base_datetime()
        from_datetime = base_datetime - relativedelta(months=self.get_month_range())
        values = {}

        results = StaffInChargeStat.objects.filter(
            company_id=company_id,
            target_date__lt=base_datetime,
            target_date__gte=from_datetime,
        ).order_by("target_date")[:settings.DASHBOARD_DISPLAY_MAX_COUNT]

        for result in results:
            values[f"{result.target_date.year}/{result.target_date.month}"] = {
                "top_values": [int(value) for value in result.top_values.split(self._partition)],
                "staff": result.staff_in_charge.split(self._partition),
            }

        return values
