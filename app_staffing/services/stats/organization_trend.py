from dateutil.relativedelta import relativedelta

from app_staffing.models import Organization
from app_staffing.services.stats.base import Statistics


class OrganizationTrendStats(Statistics):
    _STATS_ID = 'Organization_Register_Trend'

    def _calc_stats(self, company_id, base_datetime):
        month_range = 6

        labels = [
            '{year}/{month}'.format(
                year=(base_datetime - relativedelta(months=x)).year,
                month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ) for x in reversed(range(1, month_range + 1))
        ]

        counts = [
            Organization.objects.filter(
                created_time__year=(base_datetime - relativedelta(months=x)).year,
                created_time__month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ).count() for x in reversed(range(1, month_range + 1))
        ]

        return {
            'labels': labels,
            'counts': counts
        }
