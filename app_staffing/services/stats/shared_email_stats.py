from dateutil.relativedelta import relativedelta
from django.db.models import Count, CharField, Value
from django.db.models.functions import Concat
from django.conf import settings

from app_staffing.models.user import User
from app_staffing.models.email import Email
from app_staffing.models import SharedEmailStat
from app_staffing.services.stats.base import Statistics


class SharedEmailStats(Statistics):
    _partition = "|"
    _empty_char = "_"
    _STATS_ID = 'Shared_Email_Stats'

    def _calc_stats(self, company_id, base_datetime):

        labels = [
            '{year}/{month}'.format(
                year=(base_datetime - relativedelta(months=x)).year,
                month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ) for x in reversed(range(1, self.get_month_range() + 1))
        ]

        return {
            'labels': labels,
            'values': [
                12,
                92,
                49,
                70,
                65,
                11,
            ],
        }

    def calc_stats(self, target_date):
        company_id = self.get_company_id()
        from_date, to_date = self.get_target_from_to(target_date)

        email_shared_queryset = Email.objects.filter(
            company_id=company_id,
            sent_date__gte=from_date,
            sent_date__lt=to_date,
            shared=True,
        )
        email_shared_queryset_group = email_shared_queryset.values(
            "from_address", "from_name",
        ).annotate(count=Count("from_address")).order_by("-count", "from_address")[:5]

        # prepare values for insert_or_update SharedEmailStat
        top_values, from_address, from_name = [], [], []
        total_of_top, last_index = 0, -1
        for index, email in enumerate(email_shared_queryset_group):
            top_values.append(str(email.get("count")))
            from_address.append(email.get("from_address") or self._empty_char)
            from_name.append(email.get("from_name") or self._empty_char)
            total_of_top += email.get("count")
            last_index = index

        while last_index < 4:
            last_index += 1
            top_values.append("0")
            from_address.append(self._empty_char)
            from_name.append(self._empty_char)

        top_values.append(str(email_shared_queryset.count() - total_of_top))
        from_address.append("other")
        from_name.append("other")

        SharedEmailStat.objects.update_or_create(
            company_id=company_id, target_date=from_date,
            defaults={
                "top_values": self._partition.join(top_values),
                "from_address": self._partition.join(from_address),
                "from_name": self._partition.join(from_name),
            },
        )

    def get_stats(self):
        company_id = self.get_company_id()
        base_datetime = self.get_base_datetime()
        from_datetime = base_datetime - relativedelta(months=self.get_month_range())
        values = {}

        results = SharedEmailStat.objects.filter(
            company_id=company_id,
            target_date__lt=base_datetime,
            target_date__gte=from_datetime
        ).order_by("target_date")[:settings.DASHBOARD_DISPLAY_MAX_COUNT]

        for result in results:
            month = f"{result.target_date.year}/{result.target_date.month}"
            addresses = result.from_address.split(self._partition)
            names = result.from_name.split(self._partition)

            user_names = User.objects.filter(email__in=addresses).values(
                "last_name", "first_name", "email",
            ).annotate(
                display_name=Concat("last_name", Value(" "), "first_name", output_field=CharField())
            ).order_by("email").values_list("display_name", "email")

            for index, value in enumerate(addresses):
                for display_name, email in user_names:
                    if email == value:
                        names[index] = display_name
                        break

            values[month] = {
                "top_values": [int(value) for value in result.top_values.split(self._partition)],
                "from_address": addresses,
                "from_name": names,
            }

        return values
