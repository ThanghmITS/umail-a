
from dateutil.relativedelta import relativedelta

from app_staffing.models import ScheduledEmailStat
from app_staffing.models.email import ScheduledEmail
from app_staffing.services.stats.base import Statistics
from django.db.models import Count, Q

from django.conf import settings

class ScheduledEmailStats(Statistics):
    def calc_stats(self, target_date):
        company_id = self.get_company_id()
        from_date, to_date = self.get_target_from_to(target_date)

        result = ScheduledEmail.objects.filter(
            company_id=company_id,
            sent_date__gte=from_date,
            sent_date__lt=to_date,
        ).aggregate(
            job=Count('id', filter=Q(scheduled_email_send_type_id=1)),
            personnel=Count('id', filter=Q(scheduled_email_send_type_id=2)),
            other=Count('id', filter=Q(scheduled_email_send_type_id=3)),
        )

        scheduled_email_stat = ScheduledEmailStat.objects.filter(
            company_id=company_id,
            target_date=from_date
        )

        if scheduled_email_stat:
            scheduled_email_stat[0].job = result['job']
            scheduled_email_stat[0].personnel = result['personnel']
            scheduled_email_stat[0].other = result['other']
            scheduled_email_stat[0].save()
        else:
            ScheduledEmailStat.objects.create(
                company_id=company_id,
                target_date=from_date,
                job=result['job'],
                personnel=result['personnel'],
                other=result['other'],
            )

    def get_stats(self):
        company_id = self.get_company_id()
        base_datetime = self.get_base_datetime()
        from_datetime = base_datetime - relativedelta(months=self.get_month_range())
        labels = []
        values = {
            'jobs': [],
            'personnel': [],
            'announcement': [],
        }

        results =ScheduledEmailStat.objects.filter(
            company_id=company_id,
            target_date__lt=base_datetime,
            target_date__gte=from_datetime
        ).order_by('target_date')[:settings.DASHBOARD_DISPLAY_MAX_COUNT]

        for result in results:
            labels.append(f'{result.target_date.year}/{result.target_date.month}')
            values['jobs'].append(result.job)
            values['personnel'].append(result.personnel)
            values['announcement'].append(result.other)

        return {
            'labels': labels,
            'values': values,
        }
