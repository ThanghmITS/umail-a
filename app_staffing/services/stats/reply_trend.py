from dateutil.relativedelta import relativedelta

from app_staffing.models.email import Email
from app_staffing.services.stats.base import Statistics


class EmailReplyTrendStats(Statistics):
    _STATS_ID = 'RxEmail_Reply_Trend'

    def _calc_stats(self, company_id, base_datetime):
        month_range = 6

        labels = [
            '{year}/{month}'.format(
                year=(base_datetime - relativedelta(months=x)).year,
                month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ) for x in reversed(range(1, month_range + 1))
        ]

        reply_counts = [
            Email.objects.filter(
                staff_in_charge__isnull=False,
                sent_date__year=(base_datetime - relativedelta(months=x)).year,
                sent_date__month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ).count() for x in reversed(range(1, month_range + 1))
        ]

        return {
            'labels': labels,
            'counts': reply_counts,
        }
