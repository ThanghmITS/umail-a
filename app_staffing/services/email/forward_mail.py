from django.conf import settings
from django.core.mail import EmailMessage
from django.core.files.storage import default_storage as storage

import logging


def create_message_of_original_email(mail_instance):
    """ This method will return an eml data that reproduces the original received email.
    :param mail_instance: An Email message to forward.
    :type mail_instance: app_staffing.models.email.Email
    :return: message
    :rtype: django.core.mail.EmailMessage
    """
    mail = mail_instance

    # Extract Header values and contents from received emails.
    from_address = mail.from_header
    reply_to = mail.reply_to_values
    # Use strip() to remove whitespaces around the CC addresses.
    cc_addresses = mail.cc_values
    subject = mail.subject
    content = mail.text
    attachments = [  # Note: This attachment model is EmailAttachment.
        {'filename': attachment.name, 'content': storage.open(attachment.file.name, "rb").read()}
        for attachment
        in mail.attachments.all()
    ]

    # Create a new email instance
    new_message = EmailMessage(
        from_email=from_address, reply_to=reply_to, to=[], cc=cc_addresses, subject=subject, body=content,
    )

    # Attach a files to a new email instance.
    for attachment in attachments:
        new_message.attach(filename=attachment['filename'], content=attachment['content'])

    return new_message


def create_an_forward_message(original_email, forward_to):
    to_addresses = [forward_to]
    subject = f'共有メール転送: {original_email.subject}'
    content = 'Web上での操作に基づき、返信対象となるメールを添付いたしました。\n' \
              '添付ファイルを開き、返信をお願いいたします。\n\n' \
              '※このメールは、システムにより自動送信しております。\n' \
              'また、Thunderbird, Outlook, iCloud以外のメーラーをお使いの場合、添付ファイルから返信できない可能性がございます。\n' \

    message = EmailMessage(
        to=to_addresses, subject=subject, body=content,
    )
    original_message = create_message_of_original_email(original_email)
    # Note: If you specify a mimetype of message/rfc822, content argument will also accept
    # django.core.mail.EmailMessage and email.message.Message.
    message.attach(filename='ClickToReply.eml', content=original_message, mimetype='message/rfc822')

    return message


# Note: make sure that EMAIL_BACKEND is defined in settings.py
def forward_mail(original_email, forward_to):
    logging.info(msg='Trying to forward Original Email: ID:{0}, To:{1}'.format(
        original_email.id, forward_to)
    )
    mail = create_an_forward_message(original_email, forward_to)
    logging.info(msg=f'Forwarding Email. From:{mail.from_email} Subject:{mail.subject} To:{mail.to}')
    mail.send()
