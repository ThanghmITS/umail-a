import os
from logging import getLogger

from django.conf import settings
from django_mailbox.models import MessageAttachment
from html2text import html2text
from rest_framework.serializers import ValidationError

from app_staffing.models.email import INBOUND
from app_staffing.serializers.base import ValidationErrorCodes
from app_staffing.serializers.email import EmailSerializer, EmailAttachmentSerializer
from app_staffing.utils.email import get_header, get_name_and_address_from_header, get_datetime

import datetime

import logging
from django_mailbox import utils

class DuplicationError(Exception):
    pass


class EmailDataError(Exception):
    pass


def remove_null_character(text):
    """Use this to avoid crashing when we store the broken text message to the database."""
    return text.replace('\x00', '')


def _get_original_body(instance):
    return instance.get_body().decode()


def _get_text_data(instance):
    if instance.html:
        return remove_null_character(html2text(instance.html))
    return remove_null_character(instance.text)


def _get_file_name(message_attachment):
    """
    :type message_attachment: django_mailbox.models.MessageAttachment
    :return:
    """
    original_name = message_attachment.get_filename()
    if not original_name:
        if message_attachment.document.file.name:
            alias = 'Document'
            extension = os.path.splitext(message_attachment.document.file.name)[1]
            new_name = f'{alias}{extension}'
            return new_name
        else:
            return 'ファイルを正しく取り込めませんでした'

    return original_name


# getするとデコードされた文字列が返って来るが、一部分のみ文字コードが違う場合、完全にデコードできてない場合がある
# その際はデコード前の値をraw_itemsから取得し、convert_header_to_unicode明示的にデコードし直す
def _get_decoded_item(name, headers):
    decoded_item = headers.get(name, '')
    if ('=?' in decoded_item and '?=' in decoded_item) or u"\ufffd" in decoded_item:
        for item in headers.raw_items():
            if item[0] == name:
                decoded_item = utils.convert_header_to_unicode(item[1])
                break
    return decoded_item


def _get_subject(headers):
    return _get_decoded_item('Subject', headers)


def _get_from_str(headers):
    return _get_decoded_item('From', headers)


def extract_message_data(original_message, log_message):
    """ Extract Message data for model Email from original model Message.

    :param original_message: A message object sent by the signal.
    :type original_message: django_mailbox.models.Message (subclass of django.db.models.Model)
    :return: converted_data
    :rtype: dict
    """
    instance = original_message

    try:
        decoded_mail = _get_original_body(instance)

        if not decoded_mail:
            raise RuntimeError(f'Could not decode original message: {instance}, {log_message}')

        headers = get_header(decoded_mail)
        from_str = _get_from_str(headers)  # Note: sometime django_mailbox lost Envelope headers.
        date_str = headers.get('Date', None)
        subject = _get_subject(headers)
        cc_header_str = headers.get('Cc', None)
        to_header_str = headers.get('To', None)
        reply_to_header_str = headers.get('Reply-To', None)

        from_name, from_address = get_name_and_address_from_header(from_str)
        sent_datetime = get_datetime(date_str) if date_str else datetime.datetime.now()
    except Exception as e:
        logging.warning('Could not decode the original message header.  Skipping copy.. Original Error Details:{0}, {1}'.format(e, log_message))
        raise EmailDataError('Could not decode the original message.')  # Omit subsequent processing.

    try:
        converted_data = {
            'message_id': instance.message_id,
            'direction': INBOUND,  # Append metadata to mark it as an received email.
            'shared': True,  # Append metadata to mark it as a shared email.
            'from_header': from_str,
            'from_name': from_name,
            'from_address': from_address,
            'reply_to_header': reply_to_header_str,
            'to_header': to_header_str,
            'cc_header': cc_header_str,
            'subject': subject,
            'text': _get_text_data(instance),
            'html': instance.html,
            'sent_date': sent_datetime,  # Get header value through EmailMessage.__getitem__
            'attachments': [_extract_attachment_data(attachment) for attachment in _get_related_attachments(instance.id)]
        }
        return converted_data
    except Exception as e:
        logging.warning("Could not access some original message's attribute. Skipping copy.. Original Error Details:{0}, {1}".format(e, log_message))
        raise EmailDataError("Could not access some original message's attribute.")


def _get_related_attachments(message_id):
    """
    :param message_id: A primary key value of the original message that might have attachments.
    :type message_id: int
    :return: attachments
    :rtype: queryset
    """

    attachments = MessageAttachment.objects.filter(message_id__exact=message_id)
    return attachments


def _extract_attachment_data(message_attachment):
    """
    :param message_attachment: an instance of django_mailbox.models.MessageAttachment
    :rtype: dict , { name: str, file: django.db.models.fields.files.FieldFile }
    """
    data = {
        'name': _get_file_name(message_attachment),
        'file': message_attachment.document
    }

    return data


def save_email(data, company):
    serializer = EmailSerializer(data={**data, 'company': company.id})
    serializer.is_valid()
    if serializer.errors:
        logging.warning('Could not convert django-mailbox data to app data. Skipping Email importation. '
                       'Details:{0}, Company: {1}'.format(serializer.errors, company.id))
        raise EmailDataError(f'Could not convert django-mailbox data to app data. Detail: {serializer.errors}')
    else:
        try:
            instance = serializer.save()
            # Override Created user from the outside of serializer.
            # This avoids editable=False of a model field as an irregular case.
        except ValidationError as e:
            for details in e.detail.values():
                for detail in details:
                    if detail.code == ValidationErrorCodes.UNIQUE:
                        raise DuplicationError
            raise e

        return instance


def save_attachments(email, attachments, company):
    """Create attachment record related to an original message.
    :param email: A created instance of Email model.
    :type email: app_staffing.models.Email
    :param attachments:  A List of Dict ('name': str, 'file': FieldFile)
    :param company: A Company (Tenant) model instance which this attachment should belongs to.
    :type company: app_staffing.models.Company
    :return: None
    """

    for attachment in attachments:
        data = {
            'email': email.id,
            'category': INBOUND,
            'name': attachment['name'],
            'file': attachment['file'],
            'company': company.id,
        }
        serializer = EmailAttachmentSerializer(data=data)
        serializer.is_valid()
        if serializer.errors:
            logging.warning('Could not convert django-mailbox attachment data to app attachments.'
                           'Skipping this attachment. Details:{0}, Company: {1}'.format(serializer.errors, company.id))
            if settings.SKIP_ON_ERRORS:
                continue
            else:
                raise EmailDataError('Could not save an email attachment. Details:{0}, Company: {1}'.format(serializer.errors, company.id))
        else:
            serializer.save()


def get_attachment_size(attachments):
    total_size = 0
    for attachment in attachments:
        if 'file' in attachment:
            total_size += attachment['file'].size
    return total_size

def is_attachment_size_over(total_size):
    return total_size > settings.SHARED_EMAIL_MAX_BYTE_SIZE


def store_email(company, message, ignore_domain, **kwargs):
    """[Main] Convert and store email messages of django-mailbox to App Email Model
    And extract headers from raw data to extract attributes which django-mailbox does not support.
    :param company: A company id which this email must belongs to.
    :type company: app_staffing.models.Company
    :param message: An original email message instance of django-mailbox.
    :type message: django_mailbox.models.Message (subclass of django.db.models.Model)
    """

    logging.info('Trying to store the original Message {0} to Email instance of myapp.., Company: {1}'.format(message.message_id, company.id))

    # Copy and Transform original message into Email model.
    try:
        log_message = 'CompanyID: {0}'.format(company.id)
        extracted_data = extract_message_data(message, log_message)
        # 自社ドメイン拒否設定されていて、送信元メールアドレスのドメインと共有メールアドレスのドメインが一致している場合
        if ignore_domain and extracted_data['from_address'].endswith(f'@{ignore_domain}'):
            is_scheduled_mail = False
            for judge_string in settings.SCHEDULED_EMAIL_JUDGE_STRINGS:
                if judge_string in extracted_data['message_id']:
                    is_scheduled_mail = True
                    break
            # 対象のメールが配信メールでない場合は、取り込まない
            if not is_scheduled_mail:
                return

        attachments = extracted_data.pop('attachments')

        # ファイル容量が設定値(25MB)を超えていた場合は、メールを保存せず破棄する
        attachment_size = get_attachment_size(attachments)
        if is_attachment_size_over(attachment_size):
            message.delete()
            logging.error(
                'store email : Company: {0} FromAddress: {1} attachment size over, total size = {2}, Message: {3}'.format(
                    company.id,
                    extracted_data['from_address'],
                    attachment_size,
                    message.message_id,
                )
            )
            return

        email = save_email(data=extracted_data, company=company)

        if email and attachments:
            email.has_attachments = True
            email.save()
            save_attachments(email=email, attachments=attachments, company=company)
    except DuplicationError:
        logging.warning('The email is already imported, skipping.., Message: {0}, Company: {1}'.format(message.message_id, company.id))
        return
    except Exception as e:
        if settings.SKIP_ON_ERRORS:
            if isinstance(e, EmailDataError):
                return
            else:
                logging.warning('Faced an unhandled error. Skipping.. Details:{0}, Message: {1}, Company: {2}'.format(e, message.message_id, company.id))
            return
        else:
            raise e
    return email
