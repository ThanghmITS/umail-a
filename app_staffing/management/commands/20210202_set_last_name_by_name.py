from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Contact

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set contact last name.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        contacts = Contact.objects.all()
        logger.info(f'contact count : {len(contacts)}')
        for contact in contacts:
            logger.info(f'contact : {contact.id}')
            contact.last_name = contact.name
            contact.save()
            logger.info(f'contact : {contact.id} saved')

        exit(0)
