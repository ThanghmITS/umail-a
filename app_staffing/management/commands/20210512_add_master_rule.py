from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import EmailNotificationRule

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set email notification master rule.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        rules = EmailNotificationRule.objects.all()
        total = len(rules)
        logger.info(f'total rule count : {total}')

        count = 0
        for rule in rules:
            logger.info(f'rule : {rule.id}')
            rule.master_rule = 'all'
            rule.save()

            logger.info(f'rule : {rule.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
