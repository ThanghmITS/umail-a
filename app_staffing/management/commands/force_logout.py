from django.core.management.base import BaseCommand
from rest_framework.authtoken.models import Token

class Command(BaseCommand):
    help = 'This command removes all auth-token  to let users to login again, after system released'

    def handle(self, *args, **options):
        Token.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('All auth-token data removal has successful.'))
        exit(0)
