from django.core.management import BaseCommand
from django.conf import settings
from django.db.models import Prefetch
from app_staffing.services.payment import exec_company_payment
from app_staffing.models import Company, Plan, Addon, CompanyAttribute
from app_staffing.models.user import User
from app_staffing.utils.timestamp import LogExecutionTime
from datetime import datetime
from django.template.loader import render_to_string

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()


class Command(BaseCommand):
    def _send_email(self, companies, subject_template_name, message_template_name):
        if companies:
            for company in companies:
                try:
                    master_users = company.user_set.all()
                    if len(master_users) == 1:
                        context = {
                            'url': settings.HOST_URL,
                            'display_name': master_users[0].display_name,
                        }
                        subject = render_to_string(subject_template_name, context)
                        message = render_to_string(message_template_name, context)
                        master_users[0].email_user(subject, '', html_message=message)
                except Exception as e:
                    logging.error('payment notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))

    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        now = datetime.now()
        companies = Company.objects.all().prefetch_related(
            Prefetch(
                'addon_set',
                queryset=Addon.objects.select_related('addon_master')
            )
        ).prefetch_related(
            Prefetch(
                'plan_set',
                queryset=Plan.objects.select_related('plan_master').filter(is_active=True, payment_date__lte=now)
            )
        ).prefetch_related(
            Prefetch(
                'companyattribute_set',
                queryset=CompanyAttribute.objects.all()
            )
        ).prefetch_related(
            Prefetch(
                'user_set',
                queryset=User.objects.filter(user_role_id=settings.USER_ROLE_MASTER_ID)
            )
        )

        payment_succeeded_companies = []
        payment_succeeded_with_backup_companies = []
        payment_failed_companies = []
        for company in companies:
            succeeded, succeeded_with_backup, failed = exec_company_payment(company)
            if succeeded:
                payment_succeeded_companies.append(succeeded)
            if succeeded_with_backup:
                payment_succeeded_with_backup_companies.append(succeeded_with_backup)
            if failed:
                payment_failed_companies.append(failed)

        # メインカードで支払いが成功した会社への追知
        self._send_email(
            payment_succeeded_companies,
            settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_SUBJECT,
            settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_MESSAGE,
        )

        # 予備カードで支払いが成功した会社への追知
        self._send_email(
            payment_succeeded_with_backup_companies,
            settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT,
            settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE,
        )

        # 支払いが失敗した会社への追知
        self._send_email(
            payment_failed_companies,
            settings.TEMPLATE_EMAIL_PAYMENT_FAILED_SUBJECT,
            settings.TEMPLATE_EMAIL_PAYMENT_FAILED_MESSAGE,
        )

        exit(0)
