from django.core.management.base import BaseCommand
from app_staffing.estimators.classifiers import EmailClassifier


class Command(BaseCommand):
    help = 'This command search unestimated record and label them with classifiers.'

    def handle(self, *args, **options):
        classifiers = [EmailClassifier()]

        for classifier in classifiers:
            classifier.batch_predict()
            self.stdout.write(self.style.SUCCESS('Successfully predicted data with classifier "%s"' % classifier.__class__.__name__))

        self.stdout.write(self.style.SUCCESS('All prediction has successful.'))
