import sqlalchemy
import uuid
import sys
from google.cloud import storage
import io
from flask import send_file, Response
import os
import time
import datetime
from datetime import timedelta

connection_name = os.environ.get('CONNECTION_NAME', 'CONNECTION_NAME is not set.')
db_password = os.environ.get('DB_PASSWORD', 'DB_PASSWORD is not set.')
db_name = "django"

db_user = "django"
driver_name = 'postgres+pg8000'
query_string = dict({"unix_sock": "/cloudsql/{}/.s.PGSQL.5432".format(connection_name)})

MAX_RETRY = 3
SLEEP = 1

bucket_name = os.environ.get('BUCKET_NAME', 'CONNECTION_NAME is not set.')
source_blob_name = "oc.png"

def ret_image():
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    image_binary= blob.download_as_bytes()

    return send_file(
        io.BytesIO(image_binary),
        mimetype='image/png',
        as_attachment=True,
        attachment_filename='%s' % source_blob_name
    )

def get_count(conn, query):
    exec_result = conn.execute(query)
    return list(exec_result)[0][0]

def entry_point(request):
    if not request.args or not 'eid' in request.args or not 'cid' in request.args:
        # eid, cidがないということは、send_email 側でid付与処理にバグが発生している可能性もあるためAlert出す方がいいかも
        print('args error')
        return ret_image()

    try:
        eid = uuid.UUID(request.args.get('eid'))
        cid = uuid.UUID(request.args.get('cid'))
    except Exception as e:
        print('uuid error: {0}'.format(e))
        return ret_image()

    db = sqlalchemy.create_engine(
        sqlalchemy.engine.url.URL(
            drivername=driver_name,
            username=db_user,
            password=db_password,
            database=db_name,
            query=query_string,
        ),
        pool_size=5,
        max_overflow=2,
        pool_timeout=30,
        pool_recycle=1800
    )

    try:
        with db.connect() as conn:
            open_history_count_query = "SELECT COUNT(1) FROM app_staffing_scheduledemailopenhistory WHERE email_id = '{0}' AND contact_id = '{1}'".format(eid, cid)
            open_history_count = get_count(conn, open_history_count_query)
            if open_history_count == 1:
                raise RuntimeError('already opened error: eid: {0}, cid: {1}'.format(eid, cid))

            # scheduled_emailからcompany_idを持ってくる
            get_company_id_query = "SELECT company_id FROM app_staffing_scheduledemail WHERE id = '{0}'".format(eid)
            company_id = get_count(conn, get_company_id_query)

            # company_idでscheduled_email_settingsテーブルを探して、use_open_count_extra_periodがTrue(開封数取得期間を延長する状態)かどうか判定
            get_use_open_count_extra_period_true_count_query = "SELECT COUNT(1) FROM app_staffing_scheduledemailsetting WHERE company_id = '{0}' AND use_open_count_extra_period = True".format(company_id)
            use_open_count_extra_period_true_count = get_count(conn, get_use_open_count_extra_period_true_count_query)

            # 両方ともにTrueの場合は -30days、そうでなければ-3daysする
            extra_period_days = 3
            if use_open_count_extra_period_true_count == 1:
                extra_period_days = 30

            # 対象期間内にメールが存在する場合のみ処理を進める
            dt_now = datetime.datetime.now()
            extra_period = (dt_now - timedelta(days=extra_period_days))
            extra_period_mail_count_query = "SELECT COUNT(1) FROM app_staffing_scheduledemail WHERE id = '{0}' AND '{1}' <= date_to_send".format(eid, extra_period)
            extra_period_mail_count = get_count(conn, extra_period_mail_count_query)
            if extra_period_mail_count != 1:
                raise RuntimeError('over extra period error: eid: {0}, cid: {1}'.format(eid, cid))

            with conn.begin() as transaction:
                conn.execute(
                    "INSERT INTO app_staffing_scheduledemailopenhistory (id, email_id, contact_id, opened_time) VALUES ('{0}', '{1}', '{2}', '{3}')".format(str(uuid.uuid4()), eid, cid, dt_now)
                )

                for i in range(MAX_RETRY + 1):
                    try:
                        conn.execute(
                            "UPDATE app_staffing_scheduledemail SET open_count=open_count+1 WHERE id = '{0}'".format(eid)
                        )
                        transaction.commit()
                        break
                    except Exception as e:
                        print('update execution error: {0}'.format(e))
                        if i == MAX_RETRY:
                            transaction.rollback()
                            raise e
                        print("sleep {0} sec".format(SLEEP))
                        time.sleep(SLEEP)

    except Exception as e:
        print('query execution error: {0}'.format(e))

    db.dispose()
    return ret_image()
