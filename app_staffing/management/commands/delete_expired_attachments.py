from django.core.management import BaseCommand

from app_staffing.services.email.scheduled_email import ScheduledEmailService
from datetime import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        today = datetime.today()
        ScheduledEmailService.delete_expired_attachments(today)
        exit(0)
