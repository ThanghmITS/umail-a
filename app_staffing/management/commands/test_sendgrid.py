from django.conf import settings
from django.core.management.base import BaseCommand
from app_staffing.models import User
from app_staffing.utils.timestamp import LogExecutionTime
from google.cloud.logging.handlers import ContainerEngineHandler
import sys
from django.core.mail import get_connection
from logging import getLogger
from urllib.parse import urlparse
from django.template.loader import render_to_string

logger = getLogger(__name__)
logger.propagate = False
logger.addHandler(ContainerEngineHandler(stream=sys.stdout))

class Command(BaseCommand):
    help = 'SendGrid疎通確認ツール' \
           'Option' \
           '  --email 送信先のemail。Umailに登録されているemailを指定すること。' \

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):

        settings.EMAIL_BACKEND = 'app_staffing.email_backend.DKIMBackend'

        email = options.get('email', 'hoge@example.com')

        user = User.objects.get(email=email)
        url = urlparse(settings.HOST_URL)
        context = {
            'protocol': url.scheme,
            'domain': '%s:%d' % (settings.HOST_NAME, url.port),
            'token': 'hoge',
            'display_name': user.display_name,
        }

        # パスワードリセット
        print("パスワードリセットメールの疎通確認中。。。")
        subject = render_to_string(settings.TEMPLATE_EMAIL_PASSWORD_RESET_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_PASSWORD_RESET_MESSAGE, context)
        user.email_user(subject, '', html_message=message)

        # テナント登録
        print("テナント登録メールの疎通確認中。。。")
        subject = render_to_string(settings.TEMPLATE_EMAIL_TENANT_REGISTER_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_TENANT_REGISTER_MESSAGE, context)
        user.email_user(subject, '', html_message=message)

        print("疎通OK！ 実在しているemailを指定した場合は、メールが届いているか確認してください！")

    def add_arguments(self, parser):
        parser.add_argument('--email', nargs='?', default='', type=str)
