from django.core.management import BaseCommand

from app_staffing.models import Company, Plan
from app_staffing.services.email.notify_mail import send_notification_emails
from app_staffing.utils.timestamp import LogExecutionTime

from django.conf import settings

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()



class Command(BaseCommand):
    help = 'This command scans received emails and send notifications to users based on the registered rules.'

    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        companies = Company.objects.all()
        plan_master_id_mapping = {}
        for active_plan in Plan.objects.filter(company__in=companies, is_active=True):
            plan_master_id_mapping[active_plan.company_id] = active_plan.plan_master_id
        for company in companies:
            try:
                # プランに加入していない or ベーシックプランの場合は共有メールを使用できない
                plan_master_id = plan_master_id_mapping.get(company.id)
                if not plan_master_id or plan_master_id == 1:
                    continue
                send_notification_emails(company=company)
            except Exception as e:
                logging.error('notification mail: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))
        exit(0)
