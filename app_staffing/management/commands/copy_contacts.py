from logging import getLogger

from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.db.transaction import atomic
from django.utils import timezone


from app_staffing.models import Company, Contact, ContactCC, ContactPreference, Organization, User,\
    ContactComment, OrganizationComment, Tag, ContactTagAssignment
from app_staffing.utils.timestamp import LogExecutionTime

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command copies Contact and all related resources between tenants'

    def add_arguments(self, parser):
        parser.add_argument('source_cid')
        parser.add_argument('dest_cid')

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        source_cid = options['source_cid']
        dest_cid = options['dest_cid']

        # Get company info
        try:
            source_company = Company.objects.get(id=source_cid)
            destination_company = Company.objects.get(id=dest_cid)
        except ObjectDoesNotExist:
            logger.error(msg='The company you specified is not found.')
            exit(1)

        logger.info(
            msg=f'Copying contact and related resources from the company:'
                f'{source_company.name} to the company {destination_company.name}'
        )

        # Initialize model managers.
        c1_contacts = Contact.objects.filter(company=source_company)
        c2_contacts = Contact.objects.filter(company=destination_company)
        c1_contact_ccs = ContactCC.objects.filter(company=source_company)
        c2_contact_ccs = ContactCC.objects.filter(company=destination_company)
        c1_contact_preferences = ContactPreference.objects.filter(company=source_company)
        c2_contact_preferences = ContactPreference.objects.filter(company=destination_company)
        c1_organizations = Organization.objects.filter(company=source_company)
        c2_organizations = Organization.objects.filter(company=destination_company)
        c1_c_comments = ContactComment.objects.filter(company=source_company)
        c2_c_comments = ContactComment.objects.filter(company=destination_company)
        c1_o_comments = OrganizationComment.objects.filter(company=source_company)
        c2_o_comments = OrganizationComment.objects.filter(company=destination_company)
        c1_tags = Tag.objects.filter(company=source_company)
        c2_tags = Tag.objects.filter(company=destination_company)
        c1_tag_assignment = ContactTagAssignment.objects.filter(company=source_company)
        c2_tag_assignment = ContactTagAssignment.objects.filter(company=destination_company)

        # Check preconditions
        logger.info(msg='Checking preconditions before copying data.'
                        'If the destination tenant(company) has the greater number of resources than the source tenant have,'
                        'This script treat the fact that copy has already done.'
                    )
        try:
            assert c2_contacts.count() == 0
            assert c2_contact_ccs.count() == 0
            assert c2_contact_preferences.count() == 0
            assert c2_organizations.count() == 0
            assert c2_c_comments.count() == 0
            assert c2_o_comments.count() == 0
            assert c2_tags.count() == 0  # Note: Tags can not remove completely from GUI if you use that tenant once.
            # In that case, remove all tags that belongs to tenant2 by SQL.
            assert c2_tag_assignment.count() == 0
        except AssertionError:
            logger.error('Contact and Organizations must be empty on the destination tenant')
            exit(2)

        # Setup the temporal data container to keep relationships between resources.
        # Key: An original ID (), Value: A new ID of copied object.

        # Update data and assert.
        now = timezone.now()
        with atomic():
            # Step1: Copy Tags and store relationships between new id and old id.
            logger.info("Copying Tags..")
            tag_id_mapping = {}
            for t in c1_tags:
                old_id = t.id
                t.id = None
                t.company = destination_company
                t.created_user = None
                t.created_time = now
                t.modified_user = None
                t.modified_time = now
                t.save()
                tag_id_mapping[old_id] = t.id

            # Step2: Copy Organizations + Organization Comments and store relationships between new id and old id.
            organization_id_mapping = {}
            logger.info("Copying Organizations and comments..")
            for o in c1_organizations:
                old_id = o.id
                comments = OrganizationComment.objects.filter(organization=o)

                o.id = None
                o.company = destination_company
                o.old_id = None
                o.save()  # This will create an new Organization.
                organization_id_mapping[old_id] = o.id  # New ID
                # New organization does not have any comments, so copy related comments.
                for oc in comments:
                    oc.id = None
                    oc.company = destination_company
                    oc.organization = o
                    oc.save()

            # Step3: Copy Contacts and ContactCCs and ContactPreference and ContactComments and TagAssignments.
            logger.info("Copying Contacts and CCs and Preferences and Comments and TagAssignments..")
            for c in c1_contacts:
                contact_email_ccs = ContactCC.objects.filter(contact=c)
                comments = ContactComment.objects.filter(contact=c)
                tag_assignments = ContactTagAssignment.objects.filter(contact=c)
                preference = ContactPreference.objects.get(contact=c)

                # Create a copied contact
                c.id = None
                c.company = destination_company
                c.old_id = None
                c.staff = None
                c.organization = Organization.objects.get(id=organization_id_mapping[c.organization.id])
                c.save()

                # Copy Related objects
                # Contact CC
                for emcc in contact_email_ccs:
                    emcc.id = None
                    emcc.company = destination_company
                    emcc.contact = c
                    emcc.save()

                # Contact Preference
                preference.id = None
                preference.company = destination_company
                preference.contact = c
                preference.save()

                # Contact Comments:
                for cc in comments:
                    cc.id = None
                    cc.company = destination_company
                    cc.contact = c
                    cc.save()

                # Contact Tag Assignments:
                for tas in tag_assignments:
                    tas.id = None
                    tas.company = destination_company
                    tas.contact = c
                    tas.tag = Tag.objects.get(id=tag_id_mapping[tas.tag.id])
                    tas.save()

            # Step4: Validate results:
            # Reload model managers again.

            c1_contacts = Contact.objects.filter(company=source_company)
            c2_contacts = Contact.objects.filter(company=destination_company)
            c1_contact_ccs = ContactCC.objects.filter(company=source_company)
            c2_contact_ccs = ContactCC.objects.filter(company=destination_company)
            c1_contact_preferences = ContactPreference.objects.filter(company=source_company)
            c2_contact_preferences = ContactPreference.objects.filter(company=destination_company)
            c1_organizations = Organization.objects.filter(company=source_company)
            c2_organizations = Organization.objects.filter(company=destination_company)
            c1_c_comments = ContactComment.objects.filter(company=source_company)
            c2_c_comments = ContactComment.objects.filter(company=destination_company)
            c1_o_comments = OrganizationComment.objects.filter(company=source_company)
            c2_o_comments = OrganizationComment.objects.filter(company=destination_company)
            c1_tags = Tag.objects.filter(company=source_company)
            c2_tags = Tag.objects.filter(company=destination_company)
            c1_tag_assignment = ContactTagAssignment.objects.filter(company=source_company)
            c2_tag_assignment = ContactTagAssignment.objects.filter(company=destination_company)

            logger.info("Validating Results..")
            # Assert the number of objects.
            logger.info(f'FromContacts: {c1_contacts.count()} -> ToContacts: {c2_contacts.count()}')
            assert c1_contacts.count() == c2_contacts.count()
            logger.info(f'FromContactCCs: {c1_contact_ccs.count()} -> ToContactCCs: {c2_contact_ccs.count()}')
            assert c1_contact_ccs.count() == c2_contact_ccs.count()
            logger.info(f'FromContactPreferences: {c1_contact_preferences.count()} -> ToContactPreferences: {c2_contact_preferences.count()}')
            assert c1_contact_preferences.count() == c2_contact_preferences.count()
            logger.info(f'FromOrganizations: {c1_organizations.count()} -> ToOrganizations: {c2_organizations.count()}')
            assert c1_organizations.count() == c2_organizations.count()
            logger.info(f'FromContactComments: {c1_c_comments.count()} -> ToFromContactComments: {c2_c_comments.count()}')
            assert c1_c_comments.count() == c2_c_comments.count()
            logger.info(f'FromOrganizationComments: {c1_o_comments.count()} -> ToOrganizationComments: {c2_o_comments.count()}')
            assert c1_o_comments.count() == c2_o_comments.count()
            logger.info(f'FromTags: {c1_tags.count()} -> ToTags: {c2_tags.count()}')
            assert c1_tags.count() == c2_tags.count()
            logger.info(f'FromTagAssignments: {c1_tag_assignment.count()} -> ToTagAssignments: {c2_tag_assignment.count()}')
            assert c1_tag_assignment.count() == c2_tag_assignment.count()

            # Verify relationships between objects.
            # Organization Comments:
            for oc in c1_o_comments:
                assert oc.organization.company == source_company
            for oc in c2_o_comments:
                assert oc.organization.company == destination_company

            # Contacts:
            for ct in c1_contacts:
                assert ct.organization.company == source_company
            for ct in c2_contacts:
                assert ct.organization.company == destination_company

            # Contact Preferences:
            for pref in c1_contact_preferences:
                assert pref.contact.company == source_company
            for pref in c2_contact_preferences:
                assert pref.contact.company == destination_company

            # CCs:
            for emcc in c1_contact_ccs:
                assert emcc.contact.company == source_company
            for emcc in c2_contact_ccs:
                assert emcc.contact.company == destination_company

            # Contact Comments:
            for cc in c1_c_comments:
                assert cc.contact.company == source_company
            for cc in c2_c_comments:
                assert cc.contact.company == destination_company

            # Tag assignments:
            for tas in c1_tag_assignment:
                assert tas.contact.company == source_company
                assert tas.tag.company == source_company
            for tas in c2_tag_assignment:
                assert tas.contact.company == destination_company
                assert tas.tag.company == destination_company

        # Commit runs here.
        logger.info("The operation has succesfully finished.")
        exit(0)
