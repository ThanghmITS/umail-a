from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Company, Plan, CompanyAttribute
from django.conf import settings
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
import pytz

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        for plan in Plan.objects.all():
            if plan.expiration_time:
                logger.info(f'plan : {plan.id}')
                payment_date = plan.expiration_time.astimezone(pytz.timezone(settings.TIME_ZONE)).date()
                logger.info(f'payment_date : {payment_date}')
                plan.payment_date = payment_date
                plan.save()

        exit(0)
