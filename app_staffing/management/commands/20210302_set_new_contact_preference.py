from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Organization, ContactPreference, Contact, ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference, ContactPersonnelSkillPreference, TypePreference, SkillPreference, User, Company

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set contact new preference.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        ContactJobTypePreference.objects.all().delete()
        ContactJobSkillPreference.objects.all().delete()
        ContactPersonnelTypePreference.objects.all().delete()
        ContactPersonnelSkillPreference.objects.all().delete()

        hokkaido = '北海道'
        touhoku = ['青森県','岩手県','秋田県','宮城県','山形県','福島県']
        chubu = ['新潟県','富山県','石川県','福井県','山梨県','長野県']
        toukai = ['岐阜県','静岡県','愛知県']
        shikoku = ['徳島県','香川県','愛媛県','高知県']
        chugoku = ['鳥取県','島根県','岡山県','広島県','山口県']
        kyushu = ['福岡県','佐賀県','長崎県','熊本県','大分県','宮崎県','鹿児島県','沖縄県']

        type_dev_items = ['dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other']
        skill_dev_items = ['dev_youken', 'dev_kihon', 'dev_syousai', 'dev_seizou', 'dev_test', 'dev_hosyu', 'dev_beginner']
        type_infra_items = ['infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other']
        skill_infra_items = ['infra_youken', 'infra_kihon', 'infra_syousai', 'infra_kouchiku', 'infra_test', 'infra_kanshi', 'infra_hosyu', 'infra_beginner']
        type_other_items = ['other_eigyo', 'other_kichi', 'other_support', 'other_other']

        typepreferences = TypePreference.objects.all()
        skillpreferences = SkillPreference.objects.all()

        organization_hash = {}
        for organization in Organization.objects.all():
            organization_hash[organization.id] = organization

        contact_hash = {}
        for contact in Contact.objects.all():
            contact_hash[contact.id] = contact

        company_hash = {}
        for company in Company.objects.all():
            company_hash[company.id] = company

        contact_preferences = ContactPreference.objects.all()
        total = len(contact_preferences)
        logger.info(f'contact_preference count : {total}')
        count = 0
        for contact_preference in contact_preferences:
            contact = contact_hash[contact_preference.contact_id]
            organization = organization_hash[contact.organization_id]
            company = company_hash[contact.company_id]

            logger.info(f'organization: {organization.id}, contact: {contact.id}')

            if contact_preference.willing_to_receive_ads == False:
                logger.info(f'contact: {contact.id} contact_preference skipped')
                count = count + 1
                logger.info(f'count: {count} / {total}')
                continue

            # contact_preference
            if organization.address and hokkaido in organization.address:
                contact_preference.wants_location_hokkaido_japan = True

            for location in touhoku:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_touhoku_japan = True

            for location in chubu:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_chubu_japan = True

            for location in toukai:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_toukai_japan = True

            for location in shikoku:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_shikoku_japan = True

            for location in chugoku:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_chugoku_japan = True

            for location in kyushu:
                if organization.address and location in organization.address:
                    contact_preference.wants_location_kyushu_japan = True

            # contactjobtypepreference, contactjobskillpreference
            if contact_preference.wants_job_introduction:
                cjt_array = []
                cjs_array = []
                if contact_preference.wants_develop_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_dev_items:
                            cjt = ContactJobTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cjt_array.append(cjt)
                    for skillpreference in skillpreferences:
                        if skillpreference.name in skill_dev_items:
                            if skillpreference.name == 'dev_beginner' and contact_preference.accepts_low_skilled_personnel == False:
                                continue

                            cjs = ContactJobSkillPreference(
                                contact=contact,
                                skill_preference=skillpreference,
                                company=company,
                            )
                            cjs_array.append(cjs)

                if contact_preference.wants_infra_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_infra_items:
                            cjt = ContactJobTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cjt_array.append(cjt)
                    for skillpreference in skillpreferences:
                        if skillpreference.name in skill_infra_items:
                            if skillpreference.name == 'infra_beginner' and contact_preference.accepts_low_skilled_personnel == False:
                                continue

                            cjs = ContactJobSkillPreference(
                                contact=contact,
                                skill_preference=skillpreference,
                                company=company,
                            )
                            cjs_array.append(cjs)

                if contact_preference.wants_sales_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_other_items:
                            cjt = ContactJobTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cjt_array.append(cjt)

                if contact_preference.wants_develop_items or contact_preference.wants_infra_items or contact_preference.wants_sales_items:
                    ContactJobTypePreference.objects.bulk_create(cjt_array)
                    ContactJobSkillPreference.objects.bulk_create(cjs_array)

                    if contact_preference.job_distribution_limit == 0:
                        contact_preference.job_syouryu = 4
                    else:
                        contact_preference.job_syouryu = 1

            # contactpersonneltypepreference, contactpersonnelskillpreference
            if contact_preference.wants_personnel_introduction:
                cpt_array = []
                cps_array = []
                if contact_preference.wants_develop_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_dev_items:
                            cpt = ContactPersonnelTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cpt_array.append(cpt)
                    for skillpreference in skillpreferences:
                        if skillpreference.name in skill_dev_items:
                            if skillpreference.name == 'dev_beginner' and contact_preference.accepts_low_skilled_personnel == False:
                                continue

                            cps = ContactPersonnelSkillPreference(
                                contact=contact,
                                skill_preference=skillpreference,
                                company=company,
                            )
                            cps_array.append(cps)

                if contact_preference.wants_infra_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_infra_items:
                            cpt = ContactPersonnelTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cpt_array.append(cpt)
                    for skillpreference in skillpreferences:
                        if skillpreference.name in skill_infra_items:
                            if skillpreference.name == 'infra_beginner' and contact_preference.accepts_low_skilled_personnel == False:
                                continue

                            cps = ContactPersonnelSkillPreference(
                                contact=contact,
                                skill_preference=skillpreference,
                                company=company,
                            )
                            cps_array.append(cps)

                if contact_preference.wants_sales_items:
                    for typepreference in typepreferences:
                        if typepreference.name in type_other_items:
                            cpt = ContactPersonnelTypePreference(
                                contact=contact,
                                type_preference=typepreference,
                                company=company,
                            )
                            cpt_array.append(cpt)

                if contact_preference.wants_develop_items or contact_preference.wants_infra_items or contact_preference.wants_sales_items:
                    ContactPersonnelTypePreference.objects.bulk_create(cpt_array)
                    ContactPersonnelSkillPreference.objects.bulk_create(cps_array)

                    if contact_preference.wants_proper_personnel:
                        contact_preference.job_koyou_proper = True

                    if contact_preference.wants_individual_personnel:
                        contact_preference.job_koyou_free = True

                    if contact_preference.personnel_distribution_limit == 0:
                        contact_preference.personnel_syouryu = 4
                    else:
                        contact_preference.personnel_syouryu = 1

            contact_preference.save()
            logger.info(f'contact: {contact.id} contact_preference saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
