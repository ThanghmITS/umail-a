from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import User

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set user new tel.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        users = User.objects.filter(tel__isnull=False)
        total = len(users)
        logger.info(f'total user count : {total}')

        count = 0
        for user in users:
            logger.info(f'user : {user.id}')
            logger.info(f'user tel : {user.tel}')

            replaced_tel = user.tel.replace('-', '').replace('ー', '')
            tel1, tel2, tel3 = replaced_tel[:3], replaced_tel[3:7], replaced_tel[7:11]

            logger.info(f'user tel1 : {tel1}')
            logger.info(f'user tel2 : {tel2}')
            logger.info(f'user tel3 : {tel3}')

            user.tel1 = tel1
            user.tel2 = tel2
            user.tel3 = tel3
            user.save()

            logger.info(f'user : {user.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
