from logging import getLogger
from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from app_staffing.models import Email, Company, EmailAttachment

logger = getLogger(__name__)

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source_cid')


    def handle(self, *args, **options):
        source_cid = options['source_cid']

        # Get company info
        try:
            source_company = Company.objects.get(id=source_cid)
        except ObjectDoesNotExist:
            logger.error(msg='The company you specified is not found.')
            exit(1)

        c_mail = EmailAttachment.objects.filter(company=source_company)
        for c in c_mail:
            c.delete()

        exit(0)
