from django.core.management.base import BaseCommand
from app_staffing.estimators.recommenders import JobEmailRecommender


class Command(BaseCommand):
    help = 'This command removes all old data from remote estimators.'

    def handle(self, *args, **options):
        estimators = [JobEmailRecommender()]

        for estimator in estimators:
            estimator.batch_remove()
            self.stdout.write(self.style.SUCCESS('Successfully removed old data from an estimator "%s"' % estimator.__class__.__name__))

        self.stdout.write(self.style.SUCCESS('All old data removal has successful.'))
