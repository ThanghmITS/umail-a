from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Company, Plan, CompanyAttribute
from django.conf import settings
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
import pytz

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        companies = Company.objects.filter(id__in=['0f78dc15-186e-4adb-87c5-6ba4187d8e66', '44a5720a-b3d4-4871-8efa-1e5ca7e7ffec'])
        now = datetime.now()
        for company in companies:
            charge_day = company.created_time.astimezone(pytz.timezone(settings.TIME_ZONE)).day
            try:
                # 翌月の同じ日を決済日にする
                expiration_time = (now + relativedelta(months=1)).replace(day=charge_day, hour=0, minute=0, second=0, microsecond=0)
            except Exception:
                # 翌月に同じ日が存在しない場合(今の決済日が31日で、翌月が30日までしかない場合)、翌月の末日を決済日とする
                expiration_time = now.replace(day=1) + relativedelta(months=2) - timedelta(days=1)

            Plan.objects.create(
                company=company,
                plan_master_id=1,
                is_active=True,
                expiration_time=expiration_time
            )

            CompanyAttribute.objects.create(
                company=company,
                user_registration_limit=100
            )

        exit(0)
