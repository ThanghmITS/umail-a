from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Contact

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set contact new tel.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        contacts = Contact.objects.filter(tel__isnull=False)
        total = len(contacts)
        logger.info(f'total contact count : {total}')

        count = 0
        for contact in contacts:
            logger.info(f'contact : {contact.id}')
            logger.info(f'contact tel : {contact.tel}')

            replaced_tel = contact.tel.replace('-', '').replace('ー', '')
            tel1, tel2, tel3 = replaced_tel[:3], replaced_tel[3:7], replaced_tel[7:11]

            logger.info(f'contact tel1 : {tel1}')
            logger.info(f'contact tel2 : {tel2}')
            logger.info(f'contact tel3 : {tel3}')

            contact.tel1 = tel1
            contact.tel2 = tel2
            contact.tel3 = tel3
            contact.save()

            logger.info(f'contact : {contact.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
