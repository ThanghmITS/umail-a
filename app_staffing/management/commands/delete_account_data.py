from django.core.management import BaseCommand

from app_staffing.models import Contact, Company, User

from datetime import timedelta, datetime
import pytz
from django.conf import settings
from app_staffing.models.addon import Addon
from app_staffing.models.email import Email, EmailAttachment, EmailComment, MailboxMapping, SMTPServer, ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailOpenHistory, ScheduledEmailSearchCondition, ScheduledEmailSendError, ScheduledEmailSetting, ScheduledEmailTarget, SharedEmailSetting, SharedEmailTransferHistory
from app_staffing.models.notification import EmailNotificationCondition, EmailNotificationRule, UserSystemNotification
from app_staffing.models.organization import ContactCC, ContactCategory, ContactComment, ContactScore, ContactTagAssignment, DisplaySetting, ExceptionalOrganization, Organization, OrganizationBranch, OrganizationComment, Tag, UserDisplaySetting
from app_staffing.models.plan import Plan, PlanPaymentError
from app_staffing.models.preferences import ContactJobSkillPreference, ContactJobTypePreference, ContactPersonnelSkillPreference, ContactPersonnelTypePreference, ContactPreference
from app_staffing.models.purchase_history import PurchaseHistory
from app_staffing.models.stats import ContactStat, OrganizationStat, ScheduledEmailStat

from app_staffing.models.user import MASTER
from django_mailbox.models import Message, Mailbox

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()

from app_staffing.utils.delete_account import delete_with_chunk

class Command(BaseCommand):

    def handle(self, *args, **options):
        from_date = datetime.now(pytz.timezone(settings.TIME_ZONE)) - timedelta(days=settings.ACCOUNT_DELETE_REMAINING_DAY)
        company_ids = Company.objects.filter(deactivated_time__lte=from_date).values_list('id', flat=True)
        self.remove_user_with_company_ids(company_ids, 'account')
        exit(0)

    def remove_user_with_company_ids(self, company_ids, type):
        for company_id in company_ids:
            try:
                logging.info(f'company: {company_id}, start delete all {type} data')
                self._delete_django_mailbox_data(company_id)
                self._delete_all_items(company_id)
                logging.info(f'company: {company_id}, end delete all {type} data')
            except Exception as e:
                logging.error(f'account delete: Company: {company_id} Exception occurred. ErrorDetails:{e}')

    def _delete_all_items(self, company_id):
        # 親子関係があるものに関しては、子を先に削除する
        delete_with_chunk(Addon.objects.filter(company_id=company_id))
        delete_with_chunk(Addon.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(EmailAttachment.objects.filter(company_id=company_id))
        delete_with_chunk(EmailAttachment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(EmailComment.objects.filter(company_id=company_id))
        delete_with_chunk(EmailComment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(SharedEmailSetting.objects.filter(company_id=company_id))
        delete_with_chunk(SharedEmailSetting.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailAttachment.objects.filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailAttachment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailTarget.objects.filter(company_id=company_id)) # not soft delete model
        delete_with_chunk(ScheduledEmailSetting.objects.filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailSetting.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(OrganizationComment.objects.filter(company_id=company_id))
        delete_with_chunk(OrganizationComment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ExceptionalOrganization.objects.filter(company_id=company_id))
        delete_with_chunk(ExceptionalOrganization.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactTagAssignment.objects.filter(company_id=company_id))
        delete_with_chunk(ContactTagAssignment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactCC.objects.filter(company_id=company_id))
        delete_with_chunk(ContactCC.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactScore.objects.filter(company_id=company_id))
        delete_with_chunk(ContactScore.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactComment.objects.filter(company_id=company_id))
        delete_with_chunk(ContactComment.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactCategory.objects.filter(company_id=company_id))
        delete_with_chunk(ContactCategory.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactPreference.objects.filter(company_id=company_id))
        delete_with_chunk(ContactPreference.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactJobTypePreference.objects.filter(company_id=company_id)) # not soft delete model
        delete_with_chunk(ContactJobSkillPreference.objects.filter(company_id=company_id)) # not soft delete model
        delete_with_chunk(ContactPersonnelTypePreference.objects.filter(company_id=company_id)) # not soft delete model
        delete_with_chunk(ContactPersonnelSkillPreference.objects.filter(company_id=company_id)) # not soft delete model
        delete_with_chunk(DisplaySetting.objects.filter(company_id=company_id))
        delete_with_chunk(DisplaySetting.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(PlanPaymentError.objects.filter(company_id=company_id))
        delete_with_chunk(PlanPaymentError.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(PurchaseHistory.objects.filter(company_id=company_id))
        delete_with_chunk(PurchaseHistory.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(OrganizationStat.objects.filter(company_id=company_id))
        delete_with_chunk(OrganizationStat.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ContactStat.objects.filter(company_id=company_id))
        delete_with_chunk(ContactStat.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailStat.objects.filter(company_id=company_id))
        delete_with_chunk(ScheduledEmailStat.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(SMTPServer.objects.filter(company_id=company_id))
        delete_with_chunk(SMTPServer.objects.deleted_set().filter(company_id=company_id))

        delete_with_chunk(ScheduledEmailOpenHistory.objects.filter(email__company_id=company_id))
        delete_with_chunk(ScheduledEmailOpenHistory.objects.deleted_set().filter(email__company_id=company_id))
        delete_with_chunk(ScheduledEmailSendError.objects.filter(email__company_id=company_id))
        delete_with_chunk(ScheduledEmailSendError.objects.deleted_set().filter(email__company_id=company_id))
        delete_with_chunk(ScheduledEmailSearchCondition.objects.filter(email__company_id=company_id))
        delete_with_chunk(ScheduledEmailSearchCondition.objects.deleted_set().filter(email__company_id=company_id))

        delete_with_chunk(UserSystemNotification.objects.filter(user__company_id=company_id))
        delete_with_chunk(UserSystemNotification.objects.deleted_set().filter(user__company_id=company_id))
        delete_with_chunk(UserDisplaySetting.objects.filter(user__company_id=company_id))
        delete_with_chunk(UserDisplaySetting.objects.deleted_set().filter(user__company_id=company_id))

        delete_with_chunk(EmailNotificationCondition.objects.filter(rule__company_id=company_id))
        delete_with_chunk(EmailNotificationCondition.objects.deleted_set().filter(rule__company_id=company_id))

        delete_with_chunk(SharedEmailTransferHistory.objects.filter(email__company_id=company_id)) # not soft delete model

        # 親を削除
        delete_with_chunk(EmailNotificationRule.objects.filter(company_id=company_id))
        delete_with_chunk(EmailNotificationRule.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(Plan.objects.filter(company_id=company_id))
        delete_with_chunk(Plan.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(Tag.objects.filter(company_id=company_id))
        delete_with_chunk(Tag.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(Email.objects.filter(company_id=company_id))
        delete_with_chunk(Email.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(ScheduledEmail.objects.filter(company_id=company_id))
        delete_with_chunk(ScheduledEmail.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(Contact.objects.filter(company_id=company_id))
        delete_with_chunk(Contact.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(OrganizationBranch.objects.filter(company_id=company_id))
        delete_with_chunk(OrganizationBranch.objects.deleted_set().filter(company_id=company_id))
        delete_with_chunk(Organization.objects.filter(company_id=company_id))
        delete_with_chunk(Organization.objects.deleted_set().filter(company_id=company_id))

        # ユーザは一番最後に削除
        delete_with_chunk(User.objects.filter(company_id=company_id).exclude(user_role__name=MASTER))

    def _delete_django_mailbox_data(self, company_id):
        mailbox_ids = list(MailboxMapping.objects.all_with_deleted().filter(company_id=company_id).values_list('mailbox_id', flat=True))
        delete_with_chunk(Message.objects.filter(mailbox_id__in=mailbox_ids))

        delete_with_chunk(MailboxMapping.objects.filter(company_id=company_id))
        delete_with_chunk(MailboxMapping.objects.deleted_set().filter(company_id=company_id))

        delete_with_chunk(Mailbox.objects.filter(id__in=mailbox_ids))
