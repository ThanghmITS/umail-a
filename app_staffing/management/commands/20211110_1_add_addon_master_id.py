from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Addon

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        addons = Addon.objects.all()
        total = len(addons)
        logger.info(f'total addon count : {total}')

        count = 0
        for addon in addons:
            logger.info(f'addon : {addon.id}')
            addon.addon_master_id = addon.addon_id
            addon.save()
            logger.info(f'addon : {addon.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
