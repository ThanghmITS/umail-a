import email
import six

from django.core.management.base import BaseCommand

from django_mailbox import utils
from django_mailbox import models
from django_mailbox.models import Message, MessageAttachment
from django_mailbox.utils import get_settings

from app_staffing.services.email.store_email import store_email
from app_staffing.utils.timestamp import LogExecutionTime

from django.conf import settings

from app_staffing.models import Plan
import time
import traceback
import django
from django.utils.timezone import now

import traceback
from email import message as _message
from email import charset as _charset
Charset = _charset.Charset

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()

from email.message import Message as EmailMessage
import mimetypes
import os.path
import uuid
from django.core.files.base import ContentFile
from email.header import decode_header

logger = logging.getLogger(__name__)

MAX_RETRY = 3

def _process_message(self, message):
    """ Copied from django-mailbox 4.7.1 for monkey patching.
    (A method of class Mailbox.)
    """
    msg = Message()
    settings = utils.get_settings()

    if settings['store_original_message']:
        self._process_save_original_message(message, msg)
    msg.mailbox = self
    if 'subject' in message:
        msg.subject = (
            utils.convert_header_to_unicode(message['subject'])[0:255]
        )
    if 'message-id' in message:
        msg.message_id = message['message-id'][0:255].strip()
    if 'from' in message:
        # Add a slicing to avoid "value too long" error.
        msg.from_header = utils.convert_header_to_unicode(message['from'])[0:255]
    if 'to' in message:
        msg.to_header = utils.convert_header_to_unicode(message['to'])
    elif 'Delivered-To' in message:
        msg.to_header = utils.convert_header_to_unicode(
            message['Delivered-To']
        )
    msg.save()
    message = self._get_dehydrated_message(message, msg)
    try:
        body = message.as_string()
    except KeyError as exc:
        # email.message.replace_header may raise 'KeyError' if the header
        # 'content-transfer-encoding' is missing
        logging.warning("Failed to parse message: %s", exc, )
        return None
    msg.set_body(body)
    if message['in-reply-to']:
        try:
            msg.in_reply_to = Message.objects.filter(
                message_id=message['in-reply-to'].strip()
            )[0]
        except IndexError:
            pass
    msg.save()
    return msg


def _convert_header_to_unicode(header):
    """Copied from django-mailbox 4.7.1 for monkey patching"""
    default_charset = get_settings()['default_charset']

    if six.PY2 and isinstance(header, six.text_type):
        return header

    def _decode(value, encoding):
        if isinstance(value, six.text_type):
            return value
        if not encoding or encoding == 'unknown-8bit':
            encoding = default_charset
        if encoding == 'windows-31j':  # Monkey patching
            encoding = 'cp932'
        try:  # Monkey patching (Error handling)
            return value.decode(encoding, 'replace')
        except LookupError:  # Monkey patching
            logging.warning(
                'Faced to unknown decoding. Trying to replace decoding with default_charset to avoid a crash,'
                ' but it will cause garbling.')
            return value.decode(default_charset, 'replace')

    try:
        return ''.join(
            [
                (
                    _decode(bytestr, encoding)
                ) for bytestr, encoding in email.header.decode_header(header)
            ]
        )
    except UnicodeDecodeError:
        logging.error(
            'Errors encountered decoding header %s into encoding %s.',
            header,
            default_charset,
        )
        return header.decode(default_charset, 'replace')

# https://github.com/python/cpython/blob/main/Lib/email/message.py#L303
def _set_payload(self, payload, charset=None):
    """Set the payload to the given value.
    Optional charset sets the message's default character set.  See
    set_charset() for details.
    """
    if hasattr(payload, 'encode'):
        if charset is None:
            self._payload = payload
            return
        if not isinstance(charset, Charset):
            charset = Charset(charset)

        try:
            payload = payload.encode(charset.output_charset)
        except Exception:
            # メールに記載されている文字コードでのエンコードに失敗した場合
            # Charsetとして使用できる全てのエンコードで総当たりする
            for c in _charset.CHARSETS:
                try:
                    charset = Charset(c)
                    if charset.output_charset:
                        payload = payload.encode(charset.output_charset)
                    else:
                        continue
                except Exception:
                    continue
                break

    if hasattr(payload, 'decode'):
        self._payload = payload.decode('ascii', 'surrogateescape')
    else:
        self._payload = payload
    if charset is not None:
        self.set_charset(charset)

# https://github.com/coddingtonbear/django-mailbox/blob/4.8.1/django_mailbox/models.py#L407
def get_new_mail(self, condition=None):
    """Connect to this transport and fetch new messages."""
    new_mail = []
    connection = None
    for i in range(MAX_RETRY + 1):
        try:
            connection = self.get_connection()
            break
        except Exception as e:
            logging.info('get connection in get mail: Location: {0} ErrorDetails: {1} Retry:{2}'.format(self.location, traceback.format_exc(), i))
            time.sleep(3)

    if not connection:
        return
    for message in connection.get_message(condition):
        msg = self.process_incoming_message(message)
        if not msg is None:
            yield msg
    self.last_polling = now()
    if django.VERSION >= (1, 5):  # Django 1.5 introduces update_fields
        self.save(update_fields=['last_polling'])
    else:
        self.save()

# https://github.com/coddingtonbear/django-mailbox/blob/4.8.1/django_mailbox/models.py#L249
def _get_dehydrated_message(self, msg, record):
    settings = utils.get_settings()

    new = EmailMessage()
    if msg.is_multipart():
        for header, value in msg.items():
            new[header] = value
        for part in msg.get_payload():
            new.attach(
                self._get_dehydrated_message(part, record)
            )
    elif (
        settings['strip_unallowed_mimetypes']
        and not msg.get_content_type() in settings['allowed_mimetypes']
    ):
        for header, value in msg.items():
            new[header] = value
        # Delete header, otherwise when attempting to  deserialize the
        # payload, it will be expecting a body for this.
        del new['Content-Transfer-Encoding']
        new[settings['altered_message_header']] = (
            'Stripped; Content type %s not allowed' % (
                msg.get_content_type()
            )
        )
        new.set_payload('')
    elif (
        (
            msg.get_content_type() not in settings['text_stored_mimetypes']
        ) or
        (
            # Content-DispositionがstrではなくHeaderオブジェクトとして渡ってくる可能性がある
            # (https://github.com/python/cpython/blob/3.6/Lib/email/_policybase.py#L288)
            # そのため、strオブジェクトに変換してチェックする
            'attachment' in str(msg.get('Content-Disposition', ''))
        )
    ):
        filename = None
        raw_filename = msg.get_filename()
        if raw_filename:
            filename = utils.convert_header_to_unicode(raw_filename)
        if not filename:
            extension = mimetypes.guess_extension(msg.get_content_type())
        else:
            _, extension = os.path.splitext(filename)
        if not extension:
            extension = '.bin'

        attachment = MessageAttachment()

        attachment.document.save(
            uuid.uuid4().hex + extension,
            ContentFile(
                six.BytesIO(
                    msg.get_payload(decode=True)
                ).getvalue()
            )
        )
        attachment.message = record
        for key, value in msg.items():
            attachment[key] = value
        attachment.save()

        placeholder = EmailMessage()
        placeholder[
            settings['attachment_interpolation_header']
        ] = str(attachment.pk)
        new = placeholder
    else:
        content_charset = msg.get_content_charset()
        if not content_charset:
            content_charset = 'ascii'
        try:
            # Make sure that the payload can be properly decoded in the
            # defined charset, if it can't, let's mash some things
            # inside the payload :-\
            msg.get_payload(decode=True).decode(content_charset)
        except LookupError:
            logger.warning(
                "Unknown encoding %s; interpreting as ASCII!",
                content_charset
            )
            msg.set_payload(
                msg.get_payload(decode=True).decode(
                    'ascii',
                    'ignore'
                )
            )
        except ValueError:
            logger.warning(
                "Decoding error encountered; interpreting %s as ASCII!",
                content_charset
            )
            msg.set_payload(
                msg.get_payload(decode=True).decode(
                    'ascii',
                    'ignore'
                )
            )
        new = msg
    return new

# https://github.com/coddingtonbear/django-mailbox/blob/4.8.1/django_mailbox/utils.py#L105
def get_body_from_message(message, maintype, subtype):
    """
    Fetchs the body message matching main/sub content type.
    """
    body = six.text_type('')
    for part in message.walk():
        if part.get('content-disposition', '').startswith('attachment;'):
            continue

        # content_dispositionの中身がデコードが必要な場合がある
        # (Content-Disposition: =?unknown-8bit?b?YXR0YWNobWVudDsgZmlsZW5hbWU9IuOCueOCreODq+OCt+ODvOODiF9ILlTvvIjnq4vpo5vvvIkueGxzeCI=?=)みたいになっている
        # かつ、unknoown_8bitの場合は文字コードが不明なので、
        # 全ての文字コードで総当たりにデコードを行う
        if part.get('content-disposition', '').startswith('=?') and part.get('content-disposition', '').endswith('?='):
            # decode_header関数を用いると、文字コード部分とバイト部分を分割して返却してくれる
            decoded = decode_header(part.get('content-disposition', ''))
            decoded_byte = decoded[0][0]
            decoded_charset = decoded[0][1]
            decoded_str = ''
            try:
                decoded_str = decoded_byte.decode(decoded_charset)
            except Exception:
                for c in _charset.CHARSETS:
                    try:
                        charset = Charset(c)
                        if charset.output_charset:
                            decoded_str = decoded_byte.decode(charset.output_charset)
                        else:
                            continue
                    except Exception:
                        continue
                    break
            # デコードした文字列が'attachment:'で開始していた場合はその部分は添付ファイルの内容になるので、
            # テキスト本文として返却しないようにする
            if decoded_str.startswith('attachment;'):
                continue
            
        if part.get_content_maintype() == maintype and \
                part.get_content_subtype() == subtype:
            charset = part.get_content_charset()
            this_part = part.get_payload(decode=True)

            if charset:
                try:
                    this_part = this_part.decode(charset, 'replace')
                except LookupError:
                    this_part = this_part.decode('ascii', 'replace')
                    logger.warning(
                        'Unknown encoding %s encountered while decoding '
                        'text payload.  Interpreting as ASCII with '
                        'replacement, but some data may not be '
                        'represented as the sender intended.',
                        charset
                    )
                except ValueError:
                    this_part = this_part.decode('ascii', 'replace')
                    logger.warning(
                        'Error encountered while decoding text '
                        'payload from an incorrectly-constructed '
                        'e-mail; payload was converted to ASCII with '
                        'replacement, but some data may not be '
                        'represented as the sender intended.'
                    )
            else:
                this_part = this_part.decode('ascii', 'replace')

            body += this_part

    return body

utils.convert_header_to_unicode = _convert_header_to_unicode  # Monkey patching.
utils.get_body_from_message = get_body_from_message  # Monkey patching.
models.Mailbox._process_message = _process_message  # Monkey patching.
models.Mailbox.get_new_mail = get_new_mail  # Monkey patching.
models.Mailbox._get_dehydrated_message = _get_dehydrated_message  # Monkey patching.
_message.Message.set_payload = _set_payload

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--dry-run',
            action='store_true',
            help='This will skip Downloading / Storing email.',
        )

    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        # Note: Import models dynamically so that we can load patched modules.
        _dry_run = options['dry_run']  # Django automatically casts - to _ .

        from app_staffing.models import MailboxMapping
        mailbox_mappings = []
        companies = []

        for mapping in MailboxMapping.objects.all():
            mailbox_mappings.append(mapping)
            companies.append(mapping.company)
        
        plan_master_id_mapping = {}
        for active_plan in Plan.objects.filter(company__in=companies, is_active=True):
            plan_master_id_mapping[active_plan.company_id] = active_plan.plan_master_id

        for mapping in mailbox_mappings:
            try:
                # プランに加入していない or ベーシックプランの場合は共有メールを使用できない
                plan_master_id = plan_master_id_mapping.get(mapping.company_id)
                if not plan_master_id or plan_master_id == 1:
                    continue

                if mapping.mailbox.active:
                    logging.info(f'Gathering messages for Mailbox: {mapping.mailbox.name}, Owner: {mapping.company}')

                    if not _dry_run:
                        ignore_domain = None
                        if hasattr(mapping.company, 'sharedemailsetting') and not mapping.company.sharedemailsetting.allow_self_domain:
                            splited_items = mapping.mailbox.from_email.split('@')
                            ignore_domain = splited_items[1]
                        messages = mapping.mailbox.get_new_mail()
                        for message in messages:
                            store_email(company=mapping.company, message=message, ignore_domain=ignore_domain)
                    else:
                        continue
            except Exception:
                logging.error('get mail: Company: {0} Exception occurred. ErrorDetails:{1}'.format(mapping.company.id, traceback.format_exc()))
        exit(0)
