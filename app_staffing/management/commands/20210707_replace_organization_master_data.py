from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Organization, OrganizationCategory, OrganizationCountry, OrganizationEmployeeNumber

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        organization_categories = OrganizationCategory.objects.all()
        organization_category_dict = {}
        for organization_category in organization_categories:
            organization_category_dict[organization_category.name] = organization_category
        
        organization_countries = OrganizationCountry.objects.all()
        organization_country_dict = {}
        for organization_country in organization_countries:
            organization_country_dict[organization_country.name] = organization_country

        organization_employee_numbers = OrganizationEmployeeNumber.objects.all()
        organization_employee_number_dict = {}
        for organization_employee_number in organization_employee_numbers:
            organization_employee_number_dict[organization_employee_number.name] = organization_employee_number

        organizations = Organization.objects.all()
        total = len(organizations)
        logger.info(f'total organization count : {total}')

        count = 0
        for organization in organizations:
            logger.info(f'organization : {organization.id}')
            logger.info(f'organization category : {organization.category}, country: {organization.country}, employee_number: {organization.employee_number}')

            changed = False
            if organization.category in organization_category_dict:
                organization.organization_category = organization_category_dict[organization.category]
                logger.info(f'organization organization_category : {organization.organization_category}')
                changed = True

            if organization.country in organization_country_dict:
                organization.organization_country = organization_country_dict[organization.country]
                logger.info(f'organization organization_country : {organization.organization_country}')
                changed = True

            if organization.employee_number in organization_employee_number_dict:
                organization.organization_employee_number = organization_employee_number_dict[organization.employee_number]
                logger.info(f'organization organization_employee_number : {organization.organization_employee_number}')
                changed = True

            if changed:
                organization.save()
                logger.info(f'organization : {organization.id} saved')
                count = count + 1
                logger.info(f'count: {count} / {total}')

        exit(0)
