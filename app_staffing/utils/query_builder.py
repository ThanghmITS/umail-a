from django.db.models import Q

def generate_or_query(base_query, query_params):
    queries = []
    for query_param in query_params:
        queries.append(Q(**query_param))
    or_query = queries.pop()
    for item in queries:
        or_query |= item
    return base_query.filter(or_query)
