from logging import getLogger
from google.cloud.logging.handlers import ContainerEngineHandler
import sys
import google.cloud.logging
from django.conf import settings

def get_info_logger(name):
    logger_stdout = getLogger(name)
    logger_stdout.propagate = False
    logger_stdout.addHandler(ContainerEngineHandler(stream=sys.stdout))
    return logger_stdout

def get_error_logger(name):
    logger_stderr = getLogger(name+'_err')
    logger_stderr.propagate = False
    logger_stderr.addHandler(ContainerEngineHandler(stream=sys.stderr))
    return logger_stderr

def use_cloud_logging():
    if settings.USE_CLOUD_LOGGING:
        client = google.cloud.logging.Client()
        client.setup_logging()
