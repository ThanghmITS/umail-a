import re
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings

from email.parser import Parser
from email.policy import default
from email.utils import parsedate_to_datetime

# WARNING: This pattern appends trailing space to detected name when both name and address has detected.
from_header_ptn = r'^(?P<name>.*)\s?<(?P<address>.+)>$'

from_header_missing_brackets = r'^(?P<address>.+)$'  # Rescue pattern.


def get_header(decoded_message):
    return Parser(policy=default).parsestr(decoded_message)


def get_name_and_address_from_header(from_header_string):
    """Get name and address from a header string.
    :param from_header_string: An original header strings of a received email.
    :type from_header_string: str
    :return: name, address
    :rtype: str, str
    """
    matcher = re.match(pattern=from_header_ptn, string=from_header_string)
    if matcher is None:  # Trying to Rescue un-official pattern.
        matcher = re.match(pattern=from_header_missing_brackets, string=from_header_string)
        if matcher is not None:  # if rescue success.
            return '', matcher.group('address')
        else:  # if rescue failed,
            return None, None
    else:
        return matcher.group('name').rstrip(), matcher.group('address')  # rstrip removes trailing spaces.


def get_datetime(date_string):
    if date_string:
        return parsedate_to_datetime(date_string)
    return None


def get_due_date_of_shared_email():
    now = datetime.now().astimezone(pytz.timezone(settings.TIME_ZONE))
    return now - relativedelta(months=settings.SHARED_EMAIL_EXPIRATION_MONTH)


def filter_email_by_sent_date_gte(queryset, sent_date=None):
    """
    :param queryset: Queryset - [require]
    :param sent_date: datetime - [optional] - if sent_date is None, get default from settings
    :return: Queryset
    """
    if not sent_date:
        sent_date = get_due_date_of_shared_email()

    return queryset.filter(sent_date__gte=sent_date)
