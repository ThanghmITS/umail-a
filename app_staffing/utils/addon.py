from django.conf import settings

from app_staffing.models import Addon, ScheduledEmailSetting


def comment_template_total_available_count(company):
    purchase_count = Addon.objects.filter(company=company, addon_master_id=settings.COMMENT_TEMPLATE_ADDON_ID).count()
    additional_count = settings.COMMENT_TEMPLATE_ADDON_ADDITIONAL_COUNT * purchase_count
    return settings.COMMENT_TEMPLATE_DEFAULT_LIMIT + additional_count


def search_template_total_available_count(company):
    purchase_count = Addon.objects.filter(company=company, addon_master_id=settings.SEARCH_TEMPLATE_ADDON_ID).count()
    additional_count = settings.SEARCH_TEMPLATE_ADDON_ADDITIONAL_COUNT * purchase_count
    return settings.SEARCH_TEMPLATE_DEFAULT_LIMIT + additional_count


def scheduled_email_limit_target_count(company):
    addon_purchase_count = Addon.objects.filter(company=company,
                                                addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID).count()
    return settings.SCHEDULED_EMAIL_DEFAULT_LIMIT_TARGET_COUNT * (addon_purchase_count + 1)


def scheduled_email_max_byte_size_and_error_message(company):
    is_addon_purchased = Addon.objects.filter(company=company,
                                              addon_master_id=settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID).exists()
    use_attachment_max_size = ScheduledEmailSetting.objects.filter(company=company,
                                                                   use_attachment_max_size=True).exists()

    max_byte_size = settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE
    max_byte_size_error_message = settings.MAX_BYTE_SIZE_ERROR_MESSAGE
    if is_addon_purchased and use_attachment_max_size:
        max_byte_size = settings.SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE
        max_byte_size_error_message = settings.MAX_BYTE_SIZE_WITH_ADDON_ERROR_MESSAGE
    return max_byte_size, max_byte_size_error_message


def scheduled_email_template_total_available_count(company):
    return settings.SCHEDULED_EMAIL_TEMPLATE_DEFAULT_LIMIT


def personnel_max_byte_size_image_and_error_message():
    max_byte_size = settings.PERSONNEL_MAX_BYTE_SIZE_IMAGE
    max_byte_size_error_message = settings.MAX_BYTE_SIZE_PERSONNEL_IMAGE_ERROR_MESSAGE
    return max_byte_size, max_byte_size_error_message


def personnel_max_byte_size_skillsheet_and_error_message():
    max_byte_size = settings.PERSONNEL_MAX_BYTE_SIZE_SKILL_SHEET
    max_byte_size_error_message = settings.MAX_BYTE_SIZE_PERSONNEL_SKILL_SHEET_ERROR_MESSAGE
    return max_byte_size, max_byte_size_error_message
