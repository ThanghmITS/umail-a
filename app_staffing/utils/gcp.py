from google.cloud import tasks_v2
from google.protobuf import timestamp_pb2
import json
from django.conf import settings
from app_staffing.utils.utils import random_name
import logging


class CloudTasks():
    def __init__(self, queue_name, worker_name):
        """Instantiates the cloud tasks client for this application.

        Args:
            queue_name (str): Your Cloud Tasks's Queue Name
        """

        self.client = tasks_v2.CloudTasksClient()

        self.project = settings.PROJECT
        self.queue = queue_name
        self.location = settings.LOCATION
        url = settings.WORKER_URL % worker_name

        self.parent = self.client.queue_path(self.project, self.location, self.queue)
        self.task = {
            "http_request": {
                "http_method": tasks_v2.HttpMethod.POST,
                "url": url,
                "oidc_token": {"service_account_email": settings.SERVICE_ACCOUNT_EMAIL_FOR_CLOUD_TASKS },
            }
        }

        self.task_name_suffix = random_name(4)
        self.task_no = 0

    def _get_task_full_name(self, task_name):
        return self.client.task_path(self.project, self.location, self.queue, task_name)

    def _get_task_name(self, task_name_prefix):
        self.task_no += 1
        return "%s_%s-%s" % (task_name_prefix, self.task_name_suffix, self.task_no)

    def list_tasks(self):
        return self.client.list_tasks(request={"parent": self.parent})

    def delete_task(self, task_name_prefix):
        task_name = self._get_task_full_name(task_name_prefix)
        for task in self.list_tasks():
            if task.name.startswith(task_name):
                self.client.delete_task(request={"name": task.name})
                logging.info("Deleted task {}".format(task.name))

    def create_task(self, payload, send_date, task_name_prefix = ''):
        task_name = self._get_task_name(task_name_prefix)
        payload['task_id'] = task_name

        if payload is None:
            raise RuntimeError('payload has not been specified.')

        if send_date is None:
            raise RuntimeError('send_date has not been specified.')

        if isinstance(payload, dict):
            payload = json.dumps(payload)
            self.task["http_request"]["headers"] = {"Content-type": "application/json"}

        # The API expects a payload of type bytes.
        converted_payload = payload.encode()
        self.task["http_request"]["body"] = converted_payload

        timestamp = timestamp_pb2.Timestamp()
        timestamp.FromDatetime(send_date)
        self.task["schedule_time"] = timestamp
        self.task["name"] = self._get_task_full_name(task_name)

        response = self.client.create_task(request={"parent": self.parent, "task": self.task})

        logging.info("Created task {}".format(response.name))

        return task_name
