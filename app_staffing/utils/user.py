import mimetypes
import os
import re
from typing import Any

from django.conf import settings
from django.core.files.uploadedfile import UploadedFile

from app_staffing.models import User
from app_staffing.models.user import MASTER
from app_staffing.exceptions.user import UserServiceIdAlreadyExistsException

mimetypes.init()


def get_master_user(company_id):
    return User.objects.get(company_id=company_id, user_role__name=MASTER)


def get_extras_file(name, extras_dir='') -> str:
    return os.path.join(extras_dir, name)


def get_uploaded_file(filename, content_type=None, upload_name=None) -> Any:
    if content_type is False:
        return UploadedFile(open(filename, 'rb'),
                            upload_name or os.path.basename(filename), None)
    return UploadedFile(
        open(filename, 'rb'),
        upload_name or os.path.basename(filename),
        content_type or mimetypes.guess_type(filename)[0]
    )


def validate_user_service_id(pk, company_id, user_service_id):
    if user_service_id is None:
        # TODO: open comment in next phase
        # return False, settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['null_value']
        return True, ""

    length_validate = settings.LENGTH_VALIDATIONS.get('user')
    if length_validate and length_validate.get('user_service_id'):
        max_length, min_length = length_validate['user_service_id']['max'], length_validate['user_service_id']['min']
        if len(str(user_service_id)) > max_length or min_length > len(str(user_service_id)):
            return False, length_validate['user_service_id']['message']

    value_validate = settings.VALUE_VALIDATIONS.get('user')
    if value_validate and value_validate.get('user_service_id'):
        regex = value_validate['user_service_id'].get('regex')
        if regex and not bool(re.match(regex, user_service_id)):
            return False, value_validate['user_service_id']['message']['regex_error']

        not_allowed = value_validate['user_service_id'].get('not_allowed')
        if not_allowed and user_service_id in not_allowed:
            return False, value_validate['user_service_id']['message']['value_not_allowed'].format(user_service_id)

        if User.is_user_service_id_exist(pk, company_id, user_service_id):
            return False, value_validate['user_service_id']['message']['unique_error']

    return True, ""


def check_user_service_id_duplicate(request, data):
    """
    raise Exception if user_service_id already exist in the company
    :param request: object - contain a User
    :param data: dict - contain user_service_id
    """
    pk = data.get('pk') or request.user.id
    if User.is_user_service_id_exist(pk, request.user.company_id, data.get('user_service_id')):
        raise UserServiceIdAlreadyExistsException()
