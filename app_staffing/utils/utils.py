import collections
import os
import random
import string
import datetime
import jpholiday
import pytz
import re
from copy import deepcopy
from django.conf import settings

from app_staffing.exceptions.base import (
    InvalidateTelValueException,
    InvalidateFaxValueException,
    TelValueIsTooLongException,
    FaxValueIsTooLongException
)
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator


def deep_update(base, update):
    base_items = deepcopy(base)
    update_items = deepcopy(update)
    for key, value in update_items.items():
        if isinstance(value, collections.Mapping) and key in base:
            base_items[key] = deep_update(base_items[key], value)
        else:
            base_items[key] = value
    return base_items

def random_name(n):
   randlst = [random.choice(string.ascii_letters + string.digits) for i in range(n)]
   return ''.join(randlst)


def generate_day_to_date(pattern) -> string:
    update_date_to_send = ''
    for i in range(1, 29):
        if pattern is 'utc_format':
            now = datetime.datetime.utcnow().replace(hour=5, minute=0, second=0, microsecond=0) + datetime.timedelta(
                days=i)
        else:
            now = datetime.datetime.now().astimezone(pytz.timezone(settings.TIME_ZONE)).replace(hour=14, minute=0,
                                                                                                second=0,
                                                                                                microsecond=0) + datetime.timedelta(
                days=i)

        if now.weekday() < 5 and jpholiday.is_holiday(now) is False:
            if pattern == 'python_format':
                update_date_to_send = now.strftime("%Y-%m-%d %H:%M:%S+09:00")
            elif pattern == 'iso_format':
                update_date_to_send = now.isoformat()
            elif pattern == 'utc_format':
                update_date_to_send = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            else:
                update_date_to_send = ''
            break

    return update_date_to_send


def validate_tel_values(validated_data, model_name):
    tel1 = validated_data.get('tel1')
    tel2 = validated_data.get('tel2')
    tel3 = validated_data.get('tel3')
    # tel123の少なくとも１つは入力されている場合は値を確認
    if tel1 or tel2 or tel3:
        full_tel = f'{tel1}-{tel2}-{tel3}'
        if not re.compile(r'(^\d+)-(\d+)-(\d+$)').search(full_tel):
            raise InvalidateTelValueException()
        # Check if the tel length is less than 15 + 2 (2 character '-')
        if len(full_tel) > settings.LENGTH_VALIDATIONS[model_name]['tel']['max'] + 2:
            raise TelValueIsTooLongException()
    return True


def validate_fax_values(validated_data, model_name):
    fax1 = validated_data.get('fax1')
    fax2 = validated_data.get('fax2')
    fax3 = validated_data.get('fax3')
    # fax123の少なくとも１つは入力されている場合は値を確認
    if fax1 or fax2 or fax3:
        full_fax = f'{fax1}-{fax2}-{fax3}'
        if not re.compile(r'(^\d+)-(\d+)-(\d+$)').search(full_fax):
            raise InvalidateFaxValueException()
        # Check if the fax length is less than 15 + 2 (2 character '-')
        if len(full_fax) > settings.LENGTH_VALIDATIONS[model_name]['fax']['max'] + 2:
            raise FaxValueIsTooLongException()
    return True


def generate_image_filename(instance, filename):
    base, extension = os.path.splitext(filename.lower())
    return f'users/avatar/{instance.id}{extension}'


def map_project_and_card_board_data(data, request, pk):
    data.update(company=request.user.company.id)
    if 'project' in request.path:
        data.update(project=pk)
    else:
        data.update(card=pk)
    return data
