from django.conf import settings

def get_search_template_cache_key(company_id, user_id, resource_name): 
    return f'{settings.CACHE_SEARCH_TEMPLATE_KEY_BASE}_{company_id}_{user_id}_{resource_name}'
