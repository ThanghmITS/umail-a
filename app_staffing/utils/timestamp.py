from contextlib import ContextDecorator
from django.utils import timezone


class LogExecutionTime(ContextDecorator):
    def __init__(self, logger):
        super().__init__()
        self.logger = logger
        self.start_time = None
        self.func_name = None

    def __enter__(self):
        self.start_time = timezone.now()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end_time = timezone.now()
        exec_time = self.end_time - self.start_time
        exec_sec = exec_time.seconds
        self.logger.info(f'The operation {self.func_name} took {exec_sec} seconds.')

    def __call__(self, func):
        self.func_name = func.__name__
        return super().__call__(func)
