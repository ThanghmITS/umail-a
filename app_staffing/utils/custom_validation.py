from uuid import UUID
import os
import re
import smtplib
import socket
import sys
from logging import getLogger
import unicodedata

import dns.resolver
from django.conf import settings
from google.cloud.logging.handlers import ContainerEngineHandler
from PIL import Image
from rest_framework import serializers

from app_staffing.exceptions.base import RequestTimeoutException, LoginIncorrectException
from app_staffing.exceptions.user import FileExtensionValidatorException, MaxSizeValidatorException

logger = getLogger(__name__)
logger.addHandler(ContainerEngineHandler(stream=sys.stdout))


def password_validator(value, validate_values) -> bool:
    return len(str(value)) > validate_values['max'] or\
        validate_values['min'] > len(str(value)) or\
        not bool(re.search(r"[a-zA-Z]", value)) or\
        not bool(re.search(r"[0-9]", value)) or\
        not bool(re.search(r"([`~!@#$%^&*()_+\-={}[\]¥|:;\"'<>,.?/])", value))


def get_array_of_dict_validation_messages(value, validate_values):
    messages = {}
    for i,d in enumerate(value):
        if (validate_values['column'] in d) and d[validate_values['column']] and len(d[validate_values['column']]) > validate_values['max']:
            messages[i] = validate_values['message']
    return messages


def get_array_validation_messages(key, validate_dict, data):
    message = validate_dict.get('message')
    max_length = validate_dict.get('max')
    if data.get(key) and len(data.get(key)) > max_length:
        return False, message
    return True, None


def validate_max_length(key, validate_dict, data):
    validate_type = validate_dict.get('type')
    message = validate_dict.get('message')
    max_length = validate_dict.get('max')
    if validate_type in ['str', 'digits']:
        if data.get(key) and len(str(data.get(key))) > max_length:
            return False, message
    elif validate_type == 'join_str':
        joined_str_length = ''
        for column in validate_dict['columns']:
            if column in data and data[column]:
                joined_str_length += str(data[column])
        if len(joined_str_length) > max_length:
            return False, message
    elif validate_type == 'array':
        if data.get(key):
            for item in data.get(key):
                if len(str(item)) > max_length:
                    return False, message
    elif validate_type == 'password':
        if data.get(key) and password_validator(data.get(key), validate_dict):
            return False, message
    elif validate_type == 'user_service_id' and isinstance(data.get(key), str):
        value = data.get(key)
        if len(str(value)) > validate_dict['max'] or validate_dict['min'] > len(str(value)):
            return False, message

    return True, None


def validate_value(items, value):
    # Check value match with regex
    if items.get('regex') and not bool(re.match(items.get('regex'), value)):
        return False, items['message']['regex_error']

    # Check value not allowed
    if items.get('not_allowed') and value in items.get('not_allowed') or items.get('allowed') and value not in items.get('allowed'):
        return False, items['message']['value_not_allowed'].format(value)

    return True, None


def check_east_asian_width(text):
    return any(unicodedata.east_asian_width(c) in 'FWA' for c in text)


def custom_validation(data, resource_name, raise_error=True):
    errors = {}
    # Check full size or half size character on text and change max text
    text = data.get("text", "")
    settings.LENGTH_VALIDATIONS["scheduled_email"]["text"]["max"] = 5000
    if text and not check_east_asian_width(text):
        settings.LENGTH_VALIDATIONS["scheduled_email"]["text"]["max"] = 10000
    for k, v in settings.LENGTH_VALIDATIONS[resource_name].items():
        if v['type'] == 'array_of_dict':
            if data.get(k):
                messages = get_array_of_dict_validation_messages(data.get(k), v)
                if messages:
                    errors[k] = messages
        elif v['type'] == 'array_of_multiple_types':
            if data.get(k):
                messages = {}
                for i, d in enumerate(data.get(k)):
                    for k2, v2 in v['columns'].items():
                        result, message = validate_max_length(k2, v2, d)
                        if not result:
                            if i not in messages:
                                messages[i] = {}
                            messages[i][k2] = message
                if messages:
                    errors[k] = messages
        elif v['type'] == 'enum':
            if data.get(k) and data.get(k) not in v['values']:
                message = v['message']
                if message:
                    errors[k] = message
        elif v['type'] == 'uuid':
            if data.get(k):
                try:
                    if not isinstance(data.get(k), UUID) and isinstance(data.get(k), str):
                        uuid_obj = UUID(data.get(k), version=v['version'])
                    elif not isinstance(data.get(k), UUID) and not isinstance(data.get(k), str):
                        raise ValueError
                except ValueError:
                    message = v['message']
                    if message:
                        errors[k] = message
        elif v['type'] == 'array_uuid':
            if data.get(k):
                result, message = get_array_validation_messages(k, v, data)
                if not result:
                    errors[k] = message
        elif v['type'] == 'dict':
            if data.get(k):
                messages = {}
                for k2, v2 in v['columns'].items():
                    result, message = validate_max_length(k2, v2, data.get(k))
                    if not result:
                        messages[k2] = message
                if messages:
                    errors[k] = messages
        else :
            result, message = validate_max_length(k, v, data)
            if not result:
                errors[k] = message

    if settings.VALUE_RANGE_VALIDATIONS.get(resource_name):
        for k, v in settings.VALUE_RANGE_VALIDATIONS.get(resource_name).items():
            if v['type'] == 'integer' and data.get(k) is not None: # Null可の要素をチェックから外す
                if (v.get('min') and int(data[k]) < v.get('min')) or (v.get('max') and int(data[k]) > v.get('max')):
                    errors[k] = v['message']
            elif v['type'] == 'dict':
                if data.get(k):
                    messages = {}
                    for k2, v2 in v['columns'].items():
                        if v2['type'] == 'integer' and data.get(k).get(k2) is not None:
                            if (v2.get('min') and int(data[k][k2]) < v2.get('min')) or (v2.get('max') and int(data[k][k2]) > v2.get('max')):
                                messages[k2] = v2['message']
                    if messages:
                        errors[k] = messages

    if settings.NOT_NULL_VALIDATIONS.get(resource_name):
        message = settings.NOT_NULL_VALIDATIONS.get(resource_name).get('message')
        for k in settings.NOT_NULL_VALIDATIONS.get(resource_name).get('required_items'):
            value = data.get(k)
            if value is None:
                errors[k] = message
            else:
                if isinstance(value, str) and len(value) < 1:
                    errors[k] = message

    if settings.VALUE_VALIDATIONS.get(resource_name):
        for k, v in settings.VALUE_VALIDATIONS.get(resource_name).items():
            if data.get(k):
                if 'type' in v.keys() and v['type'] == 'dict':
                    for k2, v2 in v['columns'].items():
                        _, msg = validate_value(v2, data.get(k).get(k2))
                elif data.get(k) is None:
                    continue
                else:
                    _, msg = validate_value(v, data.get(k))
                if msg:
                    errors[k] = msg

    if errors:
        if raise_error:
            raise serializers.ValidationError(errors)
        else:
            return errors


class FileValidator:
    def __init__(self, file):
        self.file = file

    def file_extension_validator(self, allowed_extensions: tuple = None) -> bool:
        if not allowed_extensions:
            allowed_extensions = settings.PROFILE_AVATAR_ALLOWED_EXTENSIONS
        _, ext = os.path.splitext(self.file.name)
        ext = ext.lower()
        if ext not in allowed_extensions:
            return False
        elif ext == '.gif':
            with Image.open(self.file) as image:
                return not getattr(image, 'is_animated', False)
        return True

    def max_size_validator(self, max_size: int = 0) -> bool:
        if max_size == 0:
            max_size = settings.PROFILE_AVATAR_MAX_SIZE
        return self.file.size <= max_size


def file_validation(value):
    file = FileValidator(value)
    if not file.file_extension_validator():
        raise FileExtensionValidatorException
    if not file.max_size_validator():
        raise MaxSizeValidatorException


def mail_address_check(mail_address):
    # メールアドレス構文チェック
    regex = re.compile(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)')
    if not re.fullmatch(regex, mail_address):
        return False

    # ドメインチェック
    mail_domain = re.search("(.*)(@)(.*)", mail_address).group(3) # ドメイン部分の取り出し
    try:
        records  = dns.resolver.query(mail_domain, 'MX')
        record = records[0].exchange
        record = str(record)
    except Exception as e:
        logger.info('None of DNS query names exist')
        return False

    # メールアドレス存在チェック
    localhost = socket.gethostname()

    check_email_exist_port = settings.CHECK_EMAIL_EXIST_PORT
    timeout = settings.SMTP_TIMEOUT
    is_tls = settings.EMAIL_USE_TLS
    is_ssl = settings.EMAIL_USE_SSL
    hostname = settings.EMAIL_HOST
    port_number = settings.EMAIL_PORT
    username = settings.EMAIL_HOST_USER
    password = settings.EMAIL_HOST_PASSWORD
    source_addr = f'{username}@{hostname}'

    # if hostname is equal to localhost or 127.0.0.1, it means smtp server not setup in .env
    if hostname not in ['localhost', '127.0.0.1']:
        try:
            if is_ssl:
                server = smtplib.SMTP_SSL(host=hostname, port=port_number, timeout=timeout)
            else:
                server = smtplib.SMTP(host=hostname, port=port_number, timeout=timeout)
        except (socket.error, smtplib.SMTPConnectError):
            raise RequestTimeoutException

        if is_tls:
            server.ehlo()
            server.starttls()
            server.ehlo()
        try:
            server.login(username, password)
        except smtplib.SMTPServerDisconnected:
            raise LoginIncorrectException
    else:
        try:
            server = smtplib.SMTP(timeout=timeout)
        except (socket.error, smtplib.SMTPConnectError):
            raise RequestTimeoutException
    try:
        server.set_debuglevel(0)
        server.connect(record, check_email_exist_port)
        server.helo(localhost)
        server.mail(source_addr)
        code, message = server.rcpt(str(mail_address))
        server.quit()
        return code == 250
    except (socket.error, smtplib.SMTPConnectError):
        raise RequestTimeoutException
    except Exception as e:
        logger.info(e)
        return False
