from django.template.loader import render_to_string
from app_staffing.utils.user import get_master_user

def send_system_mail(company_id, subject_file_name, message_file_name, context = {}):
    master_user = get_master_user(company_id)
    context_cp = context.copy()
    context_cp['display_name'] = master_user.display_name
    subject = render_to_string(subject_file_name, {})
    message = render_to_string(message_file_name, context_cp)
    master_user.email_user(subject, '', html_message=message)
