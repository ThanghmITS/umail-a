import datetime
import os

import jpholiday
import pytz
from django.conf import settings
from django.db.utils import IntegrityError
from django.db.transaction import atomic

from app_staffing.board.models import ProjectScheduledEmailHistory, PersonnelScheduledEmailHistory
from app_staffing.constants.email import ScheduledEmailSettingFileStatus
from app_staffing.models.organization import Company

from rest_framework.exceptions import ErrorDetail
from rest_framework.serializers import CharField, UUIDField, DateTimeField, ChoiceField, SerializerMethodField,\
    PrimaryKeyRelatedField, DictField
from rest_framework.serializers import ModelSerializer, ValidationError

from app_staffing.models import Email, EmailAttachment, ScheduledEmail, Contact, MailboxMapping, EmailComment, ScheduledEmailSetting
from app_staffing.models.email import (
    ScheduledEmailTarget,
    ScheduledEmailAttachment,
    SENT,
    ScheduledEmailSearchCondition,
    SharedEmailSetting,
    ScheduledEmailStatus,
    ScheduledEmailSendType,
    SharedEmailTransferHistory,
    ScheduledEmailSetting,
    SMTPServer,
    ScheduledEmailOpenHistory,
    ScheduledEmailSendError, DRAFT, SENDING, ERROR, QUEUED)
from app_staffing.serializers.base import AppResourceSerializer, BaseSubCommentSerializer, MultiTenantFieldsMixin, \
    ValidationErrorCodes, BaseCommentSerializer
from app_staffing.services.email.send_mail import render_body, render_header, render_footer, DUMMY_CONTACT_DISPLAY_NAME, DUMMY_ORGANIZATION_NAME, render_sig, render_url_attachment_template
from app_staffing.utils.addon import scheduled_email_max_byte_size_and_error_message
import json

from urllib.parse import urlparse
from django_mailbox.models import Mailbox

from django.conf import settings

from app_staffing.models.addon import Addon

from app_staffing.utils.custom_validation import custom_validation

from app_staffing.exceptions.scheduled_email import ScheduledEmailAlreadySentException, \
    ScheduledEmailAttachementSizeException, ScheduledEmailPastDateException, ScheduledEmailDeliveryTimeIsNull,\
    ScheduledEmailAttachementFileLimitException

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)



def is_exists(model_class, company):
    def _is_exists(instance_id):
        if model_class.objects.filter(company=company, id=instance_id).exists():
            return True
        return False
    return _is_exists


class EmailAttachmentSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = EmailAttachment
        fields = (
            'id', 'email', 'name', 'category', 'file',
            'created_time', 'modified_time',
            'created_user', 'modified_user', 'company',
        )
        extra_kwargs = {'company': {'write_only': True}}


class EmailAttachmentSummarySerializer(ModelSerializer):

    class Meta:
        model = EmailAttachment
        fields = ('id', 'name')
        read_only_fields = ('id', 'name')


class EmailCommentSerializer(BaseCommentSerializer):

    class Meta:
        model = EmailComment
        fields = (
            'id',
            'created_user',
            'created_user__name',
            'created_user__avatar',
            'created_time',
            'content',
            'company',
            'is_important',
            'modified_user',
            'modified_user__name',
            'modified_time',
            'has_subcomment',
            'deleted_at',
            'total_sub_comment',
            'sub_comment_users_avatar',
            'parent',
            'parent_content',
            'parent_created_user_name',
            'parent_modified_user_name',
            'parent_created_time',
            'parent_modified_time',
            )
        extra_kwargs = {'company': {'write_only': True}, 'sub_comment_users_avatar': {'read_only': True}, 'total_sub_comment': {'read_only': True}, 'parent': {'read_only': True}}


class EmailSubCommentSerializer(BaseSubCommentSerializer):

    class Meta:
        model = EmailComment
        fields = (
            'id',
            'created_user',
            'created_user__name',
            'created_user__avatar',
            'created_time',
            'content',
            'company',
            'is_important',
            'modified_user',
            'modified_user__name',
            'modified_time',
            'parent',
            )
        extra_kwargs = {'company': {'write_only': True}, 'is_important': {'read_only': True}}


class SharedEmailTransferHistorySerializer(ModelSerializer):
    email = SerializerMethodField()
    sender = SerializerMethodField()
    receiver = SerializerMethodField()

    class Meta:
        model = SharedEmailTransferHistory
        fields = (
            'email',
            'sender',
            'receiver',
            'transfer_date'
        )

    def get_email(self, obj):
        return obj.email.subject

    def get_sender(self, obj):
        return obj.sender.display_name

    def get_receiver(self, obj):
        return obj.receiver.display_name


class EmailSummarySerializer(ModelSerializer):
    staff_in_charge__name = SerializerMethodField()
    comments = EmailCommentSerializer(many=True, read_only=True, source='emailcomment_set')
    histories = SharedEmailTransferHistorySerializer(many=True, read_only=True, source="shared_history")
    attachments = EmailAttachmentSummarySerializer(many=True, read_only=True)

    def get_staff_in_charge__name(self, obj):
        return obj.staff_in_charge.display_name if obj.staff_in_charge else ''

    class Meta:
        model = Email
        fields = (
            'id',
            'from_name',
            'from_address',
            'subject',
            'text',
            'sent_date',
            'estimated_category',
            'has_attachments',
            'attachments',
            'staff_in_charge__name',
            'comments',
            'histories',
        )

    def create(self, validated_data):
        raise NotImplementedError('This serializer is for read only')

    def update(self, instance, validated_data):
        raise NotImplementedError('This serializer is for read only')


class EmailSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    """This serializer will give full functionality to access Email object, except few editable=False field."""
    class Meta:
        model = Email
        fields = '__all__'
        extra_kwargs = {'company': {'write_only': True}}

    @atomic
    def save(self, **kwargs):
        # Since the target model will raise Error based on digest message, we need to convert exceptions.
        try:
            instance = super().save(**kwargs)
            return instance
        except IntegrityError as e:
            if ('UNIQUE' in e.args[0]) or ('unique' in e.__str__()):  # For Sqlite3, and PostgreSQL respectively.
                raise ValidationError(detail={'non_field_errors': [ErrorDetail(code=ValidationErrorCodes.UNIQUE, string="既に同じ内容のメールがあります")]})
            else:
                raise e

    def create(self, validated_data):
        """Override create behavior. If label is given, set same value to estimated label."""
        instance = super().create(validated_data)
        label = validated_data.get('category')
        if label:
            instance.estimated_category = label
            instance.save()
        return instance

    def update(self, instance, validated_data):
        raise RuntimeError('This resource is immutable')


class EmailDetailSerializer(ModelSerializer):
    """An Email serializer for detail view.
    This serializer restricts field update except for few fields, So suitable for shared received mail views.
    """
    # model attribute plus following field.
    staff_in_charge__name = SerializerMethodField()
    staff_in_charge__id = SerializerMethodField()
    attachments = EmailAttachmentSummarySerializer(many=True, read_only=True)

    def get_staff_in_charge__name(self, obj):
        return obj.staff_in_charge.display_name if obj.staff_in_charge else ''

    def get_staff_in_charge__id(self, obj):
        return obj.staff_in_charge.id if obj.staff_in_charge else ''

    class Meta:
        model = Email
        fields = ('id', 'message_id', 'shared', 'from_name', 'from_address', 'subject', 'text', 'html', 'sent_date',
                  'estimated_category', 'category', 'attachments', 'staff_in_charge__name', 'staff_in_charge__id')
        read_only_fields = ('message_id', 'shared', 'from_name', 'from_address', 'subject', 'text', 'html', 'sent_date',
                            'estimated_category', 'attachments', 'staff_in_charge__name', 'staff_in_charge__id')

    @atomic
    def update(self, instance, validated_data):
        """Override create behavior. If label is given, set same value to estimated label."""
        instance = super().update(instance, validated_data)
        label = validated_data.get('category')
        if label:
            instance.estimated_category = label
            instance.save()
        return instance


class ScheduledEmailAttachmentSerializer(MultiTenantFieldsMixin, ModelSerializer):
    attached_mail_id = PrimaryKeyRelatedField(source='email', read_only=True)

    def create(self, validated_data):
        total_byte_size = 0

        if validated_data['email']:
            total_byte_size += validated_data['email'].total_byte_size

        total_byte_size += validated_data['file'].size

        email_attachments_row = ScheduledEmailAttachment.objects.filter(email=validated_data['email'].id)
        total_mail_attachments = email_attachments_row.count()
        if total_mail_attachments>=10:
            raise ScheduledEmailAttachementFileLimitException(settings.FILE_ATTACHMENT_ERROR_MESSAGE)

        max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message(validated_data['company'])
        if max_byte_size < total_byte_size:
            raise ScheduledEmailAttachementSizeException(max_byte_size_error_message)

        validated_data['name'] = validated_data['file'].name
        validated_data['size'] = validated_data['file'].size
        return super().create(validated_data)

    class Meta:
        model = ScheduledEmailAttachment
        # WARNING: Do not miss "file" field, or we can not upload / download files correctly.
        fields = ('id', 'name', 'attached_mail_id', 'file', 'company', "gcp_link", "status", "file_type")
        extra_kwargs = {'file': {'write_only': True}, 'company': {'write_only': True}}


class ScheduledEmailTargetSerializer(ModelSerializer):

    class Meta:
        model = ScheduledEmailTarget
        fields = ('id', 'email', 'contact')


class ScheduledEmailSummarySerializer(ModelSerializer):
    id = CharField(read_only=True)
    sender = UUIDField(read_only=True, source='sender.id')
    sender__name = CharField(read_only=True, source='sender.display_name')
    subject = CharField(read_only=True)
    text = CharField(read_only=True)
    attachments = ScheduledEmailAttachmentSerializer(many=True, read_only=True)
    date_to_send = DateTimeField(read_only=True)
    sent_date = DateTimeField(read_only=True)
    status = SerializerMethodField()
    send_type = SerializerMethodField()
    open_ratio = SerializerMethodField()
    open_count = SerializerMethodField()

    def get_status(self, obj):
        return obj.scheduled_email_status_name

    def get_send_type(self, obj):
        return obj.scheduled_email_send_type_name

    def get_open_ratio(self, obj):
        return obj.open_ratio

    def get_open_count(self, obj):
        return obj.open_count_format

    class Meta:
        model = ScheduledEmail
        fields = (
            'id',
            'sender',
            'sender__name',
            'subject',
            'text',
            'attachments',
            'status',
            'date_to_send',
            'sent_date',
            'send_total_count',
            'send_type',
            'text_format',
            'open_count',
            "created_time",
            "modified_time",
            "open_ratio",
            "password",
            "expired_date"
        )


class ScheduledEmailSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    # TODO: validate condition when status becomes queued.
    sender__name = SerializerMethodField()
    status = ChoiceField(choices=['draft', 'queued'])
    attachments = ScheduledEmailAttachmentSerializer(many=True, read_only=True)
    target_contacts = SerializerMethodField()
    search_condition = SerializerMethodField()
    search_conditions = DictField(write_only=True, required=False)
    status = SerializerMethodField()
    send_type = SerializerMethodField()
    send_limit = SerializerMethodField()
    project_id = UUIDField(required=False, allow_null=False)
    personnel_id = UUIDField(required=False, allow_null=False)

    def custom_validation(self, data):
        custom_validation(data, 'scheduled_email')

    def get_status(self, obj):
        return obj.scheduled_email_status_name

    def get_send_type(self, obj):
        return obj.scheduled_email_send_type_name

    def get_target_contacts(self, obj):
        return [target.contact_id for target in obj.targets.all()]

    def get_sender__name(self, obj):
        return obj.sender.display_name

    def get_search_condition(self, obj):
        if hasattr(obj, 'searchcondition') and obj.searchcondition.search_condition:
            return json.loads(obj.searchcondition.search_condition)
        else:
            return {}

    def get_send_limit(self, obj):
        send_limit_extension_addons = Addon.objects.filter(company=obj.company, addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID)
        default_send_limit = settings.SCHEDULED_EMAIL_DEFAULT_LIMIT_TARGET_COUNT
        send_limit = default_send_limit + (default_send_limit * send_limit_extension_addons.count())
        return send_limit

    class Meta:
        model = ScheduledEmail
        fields = '__all__'
        read_only_fields = ('sent_date',)
        extra_kwargs = {'company': {'write_only': True}}

    @staticmethod
    def _bulk_create_targets(scheduled_email, target_contact_ids):
        target_contact_count = Contact.objects.filter(id__in=target_contact_ids).count()
        if target_contact_count != len(target_contact_ids):
            # Check if all given contacts exist.
            is_contact_exists = is_exists(model_class=Contact, company=scheduled_email.company)
            missing_ids = [tid for tid in target_contact_ids if not is_contact_exists(tid)]
            if missing_ids:
                raise ValidationError(detail={'target_contacts': f'指定された次の宛先IDは存在しません。{missing_ids}'}, code='invalid')

        # If all contacts exist, create related records.
        targets = []
        for tid in target_contact_ids:
            targets.append(ScheduledEmailTarget(email=scheduled_email, contact_id=tid, company=scheduled_email.company))
        ScheduledEmailTarget.objects.bulk_create(targets)

    @staticmethod
    def _create_search_condition(instance, search_conditions):
        if search_conditions:
            if hasattr(instance, 'searchcondition'):
                instance.searchcondition.search_condition = json.dumps(search_conditions)
                instance.searchcondition.save()
            else:
                ScheduledEmailSearchCondition.objects.create(
                    email=instance,
                    created_user=instance.created_user,
                    modified_user=instance.modified_user,
                    search_condition=json.dumps(search_conditions)
                )

    def create(self, validated_data):
        if 'status' in self.initial_data:
            selected_scheduled_email_status = ScheduledEmailStatus.objects.get(name=self.initial_data['status'])
            validated_data['scheduled_email_status'] = selected_scheduled_email_status

        if 'send_type' in self.initial_data:
            selected_scheduled_email_send_type = ScheduledEmailSendType.objects.get(name=self.initial_data['send_type'])
            validated_data['scheduled_email_send_type'] = selected_scheduled_email_send_type

        target_contact_ids = self.initial_data.get('target_contacts', [])
        search_conditions = validated_data.pop('search_conditions', {})

        project_id, personnel_id = None, None
        if "project_id" in validated_data:
            project_id = validated_data.pop("project_id")
        if "personnel_id" in validated_data:
            personnel_id = validated_data.pop("personnel_id")

        instance = super().create(validated_data)

        # create scheduled card history
        ProjectScheduledEmailHistory.objects.create(email=instance, card_id=project_id) if project_id else None
        PersonnelScheduledEmailHistory.objects.create(email=instance, card_id=personnel_id) if personnel_id else None

        ScheduledEmailTarget.objects.filter(email=instance).delete()  # Delete related records first.
        self._bulk_create_targets(instance, target_contact_ids)
        self._create_search_condition(instance, search_conditions)

        return instance

    def update(self, instance, validated_data):
        # Prohibit update if the instance email has already sent.
        if instance.scheduled_email_status and instance.scheduled_email_status.name == SENT:
            raise ScheduledEmailAlreadySentException()

        if 'date_to_send' in self.initial_data:
            if instance.scheduled_email_status_name in [SENT, SENDING, ERROR]:
                raise ScheduledEmailAlreadySentException()
            now = datetime.datetime.now().astimezone(pytz.timezone(settings.TIME_ZONE))
            if validated_data['date_to_send'] < now:
                raise ScheduledEmailPastDateException()

        if 'status' in self.initial_data:
            if self.initial_data['status'] == QUEUED and 'date_to_send' not in validated_data:
                raise ScheduledEmailDeliveryTimeIsNull()
            selected_scheduled_email_status = ScheduledEmailStatus.objects.get(name=self.initial_data['status'])
            validated_data['scheduled_email_status'] = selected_scheduled_email_status
            if self.initial_data['status'] == DRAFT:
                validated_data['date_to_send'] = None

        if 'send_type' in self.initial_data:
            selected_scheduled_email_send_type = ScheduledEmailSendType.objects.get(name=self.initial_data['send_type'])
            validated_data['scheduled_email_send_type'] = selected_scheduled_email_send_type

        target_contact_ids = self.initial_data.get('target_contacts', [])
        search_conditions = validated_data.pop('search_conditions', {})
        updated_instance = super().update(instance, validated_data)

        ScheduledEmailTarget.objects.filter(email=instance).delete()  # Delete related records first.
        self._bulk_create_targets(updated_instance, target_contact_ids)
        self._create_search_condition(instance, search_conditions)

        return updated_instance

    @atomic
    def save(self, **kwargs):
        instance = super().save(**kwargs)
        # Delete ScheduledEmailTarget which are not refers to any ScheduledEmail after updating target_contacts.
        ScheduledEmailTarget.objects.filter(email__isnull=True).delete()  # Delete Orphans.
        return instance


class ScheduledEmailPreviewSerializer(ModelSerializer):
    sender_email = SerializerMethodField()
    body = SerializerMethodField()
    over_max_byte_size = SerializerMethodField()
    url_list = SerializerMethodField()
    file_type = SerializerMethodField()

    def get_sender_email(self, obj):

        sender_email = ''
        if obj.sender:
            sender_email = obj.sender.email

        return sender_email

    def get_body(self, obj):

        signature = None
        if obj.sender:
            signature = obj.sender.email_signature

        scheduled_email_setting = None
        try:
            scheduled_email_setting = ScheduledEmailSetting.objects.get(company=obj.company)
        except Exception:
            logger_stderr.error('company: {0} , scheduled email setting does not exists.'.format(obj.company_id))

        is_use_remove_promotion_available = Addon.objects.filter(company=obj.company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID).exists()

        content = obj.text
        if obj.text_format == 'html':
            content = content.replace('\n', '<br>')

        body = render_body(
            content=content,
            text_format=obj.text_format,
            scheduled_email_setting=scheduled_email_setting,
            is_use_remove_promotion_available=is_use_remove_promotion_available,
        )

        header = render_header(
            contact_display_name=DUMMY_CONTACT_DISPLAY_NAME,
            contact_organization_name=DUMMY_ORGANIZATION_NAME,
            text_format=obj.text_format,
        )

        footer = render_footer(
            text_format=obj.text_format,
            scheduled_email_setting=scheduled_email_setting,
        )

        sig = render_sig(
            sender_signature=signature,
            text_format=obj.text_format,
        )

        body_data = header + body + sig + footer

        url_list = ScheduledEmailAttachment.objects.filter(email_id=obj.id).values_list("gcp_link", flat=True)
        if obj.file_type == ScheduledEmailSettingFileStatus.URL and url_list:
            url_text = f"{settings.HOST_URL}/scheduledMail/files/download/{obj.id}"
            url_attachment = render_url_attachment_template(
                password=obj.password,
                url=url_text,
                text_format=obj.text_format,
            )
            body_data = header + body + url_attachment + sig + footer

        return body_data

    def get_over_max_byte_size(self, obj):
        is_addon_purchased = Addon.objects.filter(
            company=obj.company,
            addon_master_id=settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID
        ).exists()
        use_attachment_max_size = ScheduledEmailSetting.objects.filter(
            company=obj.company,
            use_attachment_max_size=True
        ).exists()
        max_byte_size = settings.SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE if is_addon_purchased and use_attachment_max_size else settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE
        return max_byte_size < obj.total_byte_size

    def get_url_list(self, obj):
        url_list = list(ScheduledEmailAttachment.objects.filter(email_id=obj.id).values_list("gcp_link", flat=True))
        return url_list

    def get_file_type(self, obj):
        has_url_setting = ScheduledEmailSetting.objects.filter(company=obj.company, file_type=ScheduledEmailSettingFileStatus.URL).exists()
        return ScheduledEmailSettingFileStatus.URL if has_url_setting else ScheduledEmailSettingFileStatus.FILE


    class Meta:
        model = ScheduledEmail
        fields = ('sender_email', 'subject', 'body', 'date_to_send', 'text_format', 'over_max_byte_size', 'password', 'url_list', 'file_type')
        read_only_fields = ('sender_email', 'subject', 'body', 'date_to_send', 'text_format', 'over_max_byte_size', 'password', 'url_list', 'file_type')


class SharedEmailSettingSerializer(ModelSerializer):
    from_email = SerializerMethodField()
    protocol = SerializerMethodField()
    password = SerializerMethodField()
    modified_user__name = SerializerMethodField()

    def custom_validation(self, data):
        custom_validation(data, 'shared_email_setting')

    def get_from_email(self, obj):
        from_email = ''
        mapping = MailboxMapping.objects.filter(company=obj.company)
        if mapping and len(mapping) == 1:
            from_email = mapping[0].mailbox.from_email
        return from_email

    def get_protocol(self, obj):
        protocol = ''
        mapping = MailboxMapping.objects.filter(company=obj.company)
        if mapping and len(mapping) == 1:
            uri = mapping[0].mailbox.uri
            parsed_url_items = urlparse(uri)
            protocol = parsed_url_items.scheme
        return protocol

    def get_password(self, obj):
        password = ''
        mapping = MailboxMapping.objects.filter(company=obj.company)
        if mapping and len(mapping) == 1:
            uri = mapping[0].mailbox.uri
            parsed_url_items = urlparse(uri)
            password = parsed_url_items.password
        return password

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def update(self, instance, validated_data):
        from_email = self.initial_data.get('from_email', None)
        password = self.initial_data.get('password', None)
        protocol = self.initial_data.get('protocol', None)
        splited_items = from_email.split('@')
        username = splited_items[0]
        domain = splited_items[1]
        uri = protocol + '://' + username + ':' + password + '@' + domain

        mapping = MailboxMapping.objects.filter(company=instance.company)
        if mapping:
            mailbox = mapping[0].mailbox
            mailbox.uri = uri
            mailbox.save()
        else:
            mailbox = Mailbox.objects.create(name=username, uri=uri, from_email=from_email, active=True)
            MailboxMapping.objects.create(company=instance.company, mailbox=mailbox)

        return super().update(instance, validated_data)

    class Meta:
        model = SharedEmailSetting
        fields = (
            'company',
            'allow_self_domain',
            'from_email',
            'protocol',
            'password',
            'modified_time',
            'modified_user',
            'modified_user__name',
        )


class ScheduledEmailOpenHistorySerializer(ModelSerializer):
    contact = SerializerMethodField()
    organization = SerializerMethodField()

    def get_contact(self, obj):
        return {'id': obj.contact.id, 'name': obj.contact.display_name}

    def get_organization(self, obj):
        return {'id': obj.contact.organization.id, 'name': obj.contact.organization.name}

    class Meta:
        model = ScheduledEmailOpenHistory
        fields = ('id', 'email', 'contact', 'organization', 'opened_time')


class ScheduledEmailSendErrorSerializer(ModelSerializer):
    contact = SerializerMethodField()
    organization = SerializerMethodField()

    def get_contact(self, obj):
        return {'id': obj.contact.id, 'name': obj.contact.display_name}

    def get_organization(self, obj):
        return {'id': obj.contact.organization.id, 'name': obj.contact.organization.name}

    class Meta:
        model = ScheduledEmailSendError
        fields = ('id', 'email', 'contact', 'organization')


class ScheduledEmailSettingSerializer(ModelSerializer):
    username = SerializerMethodField()
    hostname = SerializerMethodField()
    port_number = SerializerMethodField()
    password = SerializerMethodField()
    use_tls = SerializerMethodField()
    use_ssl = SerializerMethodField()
    is_open_count_available = SerializerMethodField()
    is_use_attachment_max_size_available = SerializerMethodField()
    is_open_count_extra_period_available = SerializerMethodField()
    modified_user__name = SerializerMethodField()
    is_use_remove_promotion_available = SerializerMethodField()
    target_count_addon_purchase_count = SerializerMethodField()
    is_use_delivery_interval_available = SerializerMethodField()
    connection_type = SerializerMethodField()

    def custom_validation(self, data):
        custom_validation(data, 'scheduled_email_setting')

    def get_username(self, obj):
        username = ''
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            username = smtpservers[0].username
        return username

    def get_hostname(self, obj):
        hostname = ''
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            hostname = smtpservers[0].hostname
        return hostname

    def get_port_number(self, obj):
        port_number = 0
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            port_number = smtpservers[0].port_number
        return port_number

    def get_password(self, obj):
        password = ''
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            password = smtpservers[0].password
        return password

    def get_use_tls(self, obj):
        use_tls = False
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            use_tls = smtpservers[0].use_tls
        return use_tls

    def get_use_ssl(self, obj):
        use_ssl = False
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers:
            use_ssl = smtpservers[0].use_ssl
        return use_ssl

    def get_connection_type(self, obj):
        smtpservers = SMTPServer.objects.filter(company=obj.company)
        if smtpservers and smtpservers[0].use_ssl:
            return settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_SSL
        elif smtpservers and smtpservers[0].use_tls:
            return settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_TLS
        else:
            return settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_DEFAULT

    def get_is_open_count_available(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID).exists()

    def get_is_open_count_extra_period_available(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID).exists()

    def get_is_use_remove_promotion_available(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID).exists()

    def get_is_use_attachment_max_size_available(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID).exists()

    def get_is_use_delivery_interval_available(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID).exists()

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def get_target_count_addon_purchase_count(self, obj):
        return Addon.objects.filter(company=obj.company, addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID).count()

    def update(self, instance, validated_data):
        username = self.initial_data.get('username', None)
        hostname = self.initial_data.get('hostname', None)
        port_number = self.initial_data.get('port_number', None)
        password = self.initial_data.get('password', None)

        connection_type = self.initial_data.get('connection_type', None)
        use_tls = connection_type and connection_type == settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_TLS
        use_ssl = connection_type and connection_type == settings.SCHEDULED_EMAIL_SETTING_CONNECTION_TYPE_SSL

        smtpserver = SMTPServer.objects.filter(company=instance.company)
        if smtpserver:
            smtpserver[0].username = username
            smtpserver[0].hostname = hostname
            smtpserver[0].port_number = port_number
            smtpserver[0].password = password
            smtpserver[0].use_tls = use_tls
            smtpserver[0].use_ssl = use_ssl
            smtpserver[0].save()
        else:
            SMTPServer.objects.update_or_create(
                username=username,
                hostname=hostname,
                password=password,
                use_tls=use_tls,
                use_ssl=use_ssl,
                port_number=port_number,
                company=instance.company
            )

        return super().update(instance, validated_data)

    class Meta:
        model = ScheduledEmailSetting
        fields = (
            'company',
            'is_open_count_available',
            'is_open_count_extra_period_available',
            'use_open_count',
            'use_open_count_extra_period',
            'username',
            'hostname',
            'port_number',
            'password',
            'use_tls',
            'use_ssl',
            'modified_time',
            'modified_user',
            'modified_user__name',
            'is_use_remove_promotion_available',
            'use_remove_promotion',
            'is_use_attachment_max_size_available',
            'use_attachment_max_size',
            'target_count_addon_purchase_count',
            'is_use_delivery_interval_available',
            'connection_type',
            'file_type',
        )
