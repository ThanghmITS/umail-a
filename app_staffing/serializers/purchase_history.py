from rest_framework.serializers import ModelSerializer, SerializerMethodField

from app_staffing.models import PurchaseHistory, AddonMaster
from django.conf import settings

class PurchaseHistorySerializer(ModelSerializer):
    plan_name = SerializerMethodField()
    option_names = SerializerMethodField()
    method_name = SerializerMethodField()

    def get_plan_name(self, obj):
        if obj.plan:
            return obj.plan.plan_master.title
        else:
            return ''

    def get_option_names(self, obj):
        option_names = []
        if obj.addon_ids:
            option_names.extend(AddonMaster.objects.filter(id__in=[int(addon_id) for addon_id in obj.addon_ids.split(',')]).values_list('title', flat=True))
        
        if obj.additional_options:
            option_names.extend([s for s in obj.additional_options.split(',')])

        return option_names
    
    def get_method_name(self, obj):
        if obj.method:
            return settings.PURCHASE_HISTORY_METHOD_NAMES[obj.method]
        else:
            return ''

    class Meta:
        model = PurchaseHistory
        fields = (
            "id",
            "period_start",
            "period_end",
            "plan_name",
            "option_names",
            "card_last4",
            "price",
            "created_time",
            "method_name",
        )
