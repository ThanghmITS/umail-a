from django.core.exceptions import ObjectDoesNotExist
from rest_framework.serializers import Serializer, BooleanField, CharField, UUIDField, SerializerMethodField, \
    IntegerField, ModelSerializer
from crum import get_current_user

from app_staffing.models import ContactPreference, Company, ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference,  \
    ContactPersonnelSkillPreference, TypePreference, SkillPreference

from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer


class ContactPreferenceSummarySerializer(Serializer):
    """ A light weight serializer for list views."""
    id = UUIDField(read_only=True, source='contact.id')
    name = CharField(read_only=True, source='contact.display_name')
    email = CharField(read_only=True, source='contact.email')
    org_name = CharField(read_only=True, source='contact.organization.name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = self.context['request'].user
        self.user_company_s_capital = Company.objects.get(pk=user.company.id).capital_man_yen
        self.current_user = get_current_user()


class ContactPreferenceModelSerializer(ModelSerializer):
    """ A model serializer for nested serializer."""

    class Meta:
        model = ContactPreference
        fields = (
            'wants_location_hokkaido_japan',
            'wants_location_touhoku_japan',
            'wants_location_kanto_japan',
            'wants_location_chubu_japan',
            'wants_location_toukai_japan',
            'wants_location_kansai_japan',
            'wants_location_shikoku_japan',
            'wants_location_chugoku_japan',
            'wants_location_kyushu_japan',
            'wants_location_other_japan'
        )



class ContactPreferenceSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    contact__last_name = SerializerMethodField()
    contact__first_name = SerializerMethodField()
    contact__display_name = SerializerMethodField()
    contact__email = SerializerMethodField()
    contact__organization__name = SerializerMethodField()
    contact__organization__is_blacklisted = SerializerMethodField()

    def get_contact__last_name(self, obj):
        return obj.contact.last_name

    def get_contact__first_name(self, obj):
        return obj.contact.first_name

    def get_contact__display_name(self, obj):
        return obj.contact.display_name

    def get_contact__email(self, obj):
        return obj.contact.email

    def get_contact__organization__name(self, obj):
        return obj.contact.organization.name

    def get_contact__organization__is_blacklisted(self, obj):
        return obj.contact.organization.is_blacklisted

    class Meta:
        model = ContactPreference
        fields = '__all__'
        extra_kwargs = {'company': {'write_only': True}}


class RequestContactJobTypePreferenceSerializer(Serializer):
    dev_designer = BooleanField(write_only=True, required=False)
    dev_front = BooleanField(write_only=True, required=False)
    dev_server = BooleanField(write_only=True, required=False)
    dev_pm = BooleanField(write_only=True, required=False)
    dev_other = BooleanField(write_only=True, required=False)
    infra_server = BooleanField(write_only=True, required=False)
    infra_network = BooleanField(write_only=True, required=False)
    infra_security = BooleanField(write_only=True, required=False)
    infra_database = BooleanField(write_only=True, required=False)
    infra_sys = BooleanField(write_only=True, required=False)
    infra_other = BooleanField(write_only=True, required=False)
    other_eigyo = BooleanField(write_only=True, required=False)
    other_kichi = BooleanField(write_only=True, required=False)
    other_support = BooleanField(write_only=True, required=False)
    other_other = BooleanField(write_only=True, required=False)


class RequestContactJobSkillPreferenceSerializer(Serializer):
    dev_youken = BooleanField(write_only=True, required=False)
    dev_kihon = BooleanField(write_only=True, required=False)
    dev_syousai = BooleanField(write_only=True, required=False)
    dev_seizou = BooleanField(write_only=True, required=False)
    dev_test = BooleanField(write_only=True, required=False)
    dev_hosyu = BooleanField(write_only=True, required=False)
    dev_beginner = BooleanField(write_only=True, required=False)
    infra_youken = BooleanField(write_only=True, required=False)
    infra_kihon = BooleanField(write_only=True, required=False)
    infra_syousai = BooleanField(write_only=True, required=False)
    infra_kouchiku = BooleanField(write_only=True, required=False)
    infra_test = BooleanField(write_only=True, required=False)
    infra_hosyu = BooleanField(write_only=True, required=False)
    infra_kanshi = BooleanField(write_only=True, required=False)
    infra_beginner = BooleanField(write_only=True, required=False)


class RequestContactPersonnelTypePreferenceSerializer(Serializer):
    dev_designer = BooleanField(write_only=True, required=False)
    dev_front = BooleanField(write_only=True, required=False)
    dev_server = BooleanField(write_only=True, required=False)
    dev_pm = BooleanField(write_only=True, required=False)
    dev_other = BooleanField(write_only=True, required=False)
    infra_server = BooleanField(write_only=True, required=False)
    infra_network = BooleanField(write_only=True, required=False)
    infra_security = BooleanField(write_only=True, required=False)
    infra_database = BooleanField(write_only=True, required=False)
    infra_sys = BooleanField(write_only=True, required=False)
    infra_other = BooleanField(write_only=True, required=False)
    other_eigyo = BooleanField(write_only=True, required=False)
    other_kichi = BooleanField(write_only=True, required=False)
    other_support = BooleanField(write_only=True, required=False)
    other_other = BooleanField(write_only=True, required=False)


class RequestContactPersonnelSkillPreferenceSerializer(Serializer):
    dev_youken = BooleanField(write_only=True, required=False)
    dev_kihon = BooleanField(write_only=True, required=False)
    dev_syousai = BooleanField(write_only=True, required=False)
    dev_seizou = BooleanField(write_only=True, required=False)
    dev_test = BooleanField(write_only=True, required=False)
    dev_hosyu = BooleanField(write_only=True, required=False)
    dev_beginner = BooleanField(write_only=True, required=False)
    infra_youken = BooleanField(write_only=True, required=False)
    infra_kihon = BooleanField(write_only=True, required=False)
    infra_syousai = BooleanField(write_only=True, required=False)
    infra_kouchiku = BooleanField(write_only=True, required=False)
    infra_test = BooleanField(write_only=True, required=False)
    infra_hosyu = BooleanField(write_only=True, required=False)
    infra_kanshi = BooleanField(write_only=True, required=False)
    infra_beginner = BooleanField(write_only=True, required=False)


class RequestContactPreferenceSerializer(Serializer):
    wants_location_kanto_japan = BooleanField(write_only=True, required=False)
    wants_location_kansai_japan = BooleanField(write_only=True, required=False)
    wants_location_hokkaido_japan = BooleanField(write_only=True, required=False)
    wants_location_touhoku_japan = BooleanField(write_only=True, required=False)
    wants_location_chubu_japan = BooleanField(write_only=True, required=False)
    wants_location_kyushu_japan = BooleanField(write_only=True, required=False)
    wants_location_other_japan = BooleanField(write_only=True, required=False)
    wants_location_chugoku_japan = BooleanField(write_only=True, required=False)
    wants_location_shikoku_japan = BooleanField(write_only=True, required=False)
    wants_location_toukai_japan = BooleanField(write_only=True, required=False)
    job_koyou_proper = BooleanField(write_only=True, required=False)
    job_koyou_free = BooleanField(write_only=True, required=False)
    job_syouryu = IntegerField(write_only=True, required=False)
    personnel_syouryu = IntegerField(write_only=True, required=False)
    personnel_country_japan = BooleanField(write_only=True, required=False)
    personnel_country_other = BooleanField(write_only=True, required=False)
    has_send_guide = BooleanField(write_only=True, required=False)
