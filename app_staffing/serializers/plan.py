from rest_framework.serializers import ModelSerializer, SerializerMethodField
from app_staffing.models import Plan, CompanyAttribute, User
from app_staffing.serializers.base import AppResourceSerializer, MultiTenantFieldsMixin
from datetime import timedelta, datetime
from django.core.exceptions import ObjectDoesNotExist
from dateutil.relativedelta import relativedelta
import pytz
from django.conf import settings
from app_staffing.utils.expiration_time import get_payment_date

class PlanSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    user_registration_limit = SerializerMethodField()
    default_user_count = SerializerMethodField()
    current_user_count = SerializerMethodField()
    expiration_date = SerializerMethodField()

    def get_user_registration_limit(self, obj):
        try:
            return CompanyAttribute.objects.get(company=obj.company).user_registration_limit
        except ObjectDoesNotExist:
            return None
    
    def get_default_user_count(self, obj):
        return obj.plan_master.default_user_count
    
    def get_expiration_date(self, obj):
        return obj.expiration_date
    
    def get_current_user_count(self, obj):
        return User.objects.filter(company=obj.company, is_active=True).count()
    
    def create(self, validated_data):
        validated_data['is_active'] = True

        now = datetime.now(pytz.timezone(settings.TIME_ZONE))
        validated_data['payment_date'] = get_payment_date(now.date())

        instance = super().create(validated_data)
        return instance

    class Meta:
        model = Plan
        fields = (
            "id",
            "expiration_date",
            "plan_master_id",
            "user_registration_limit",
            "default_user_count",
            "current_user_count",
        )
