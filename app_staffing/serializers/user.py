from django.conf import settings
from rest_framework import serializers
from rest_framework.serializers import CharField, ModelSerializer

from app_staffing.exceptions.user import DeleteFileError
from app_staffing.models import User
from app_staffing.models.user import User, UserRole
from app_staffing.serializers.base import MultiTenantFieldsMixin
from app_staffing.utils.custom_validation import custom_validation, file_validation
from app_staffing.utils.user_role import is_admin_or_master
from app_staffing.utils.user import check_user_service_id_duplicate


def setattr_avatar(instance, file):
    if file:
        file_validation(file)
        try:
            avatar = instance.avatar
            if avatar.name != '' and avatar.name is not None:
                avatar.storage.delete(name=avatar.name)
        except Exception:
            raise DeleteFileError
        setattr(instance, 'avatar', file)
    return instance


class UserSerializer(MultiTenantFieldsMixin, ModelSerializer):
    password = serializers.CharField(write_only=True)
    editable = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()
    modified_user__name = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def custom_validation(self, data):
        check_user_service_id_duplicate(self.context.get('request'), data)
        custom_validation(data, 'user')

    class Meta:
        model = User
        fields = ('id', 'display_name', 'username', 'last_name', 'first_name', 'email', 'last_login', 'is_active',
                  'is_user_admin', 'password', 'editable', 'old_id', 'company', 'role', 'email_signature', 'tel1',
                  'tel2', 'tel3', 'registed_at', 'modified_time', 'modified_user__name', 'user_service_id', 'avatar')
        extra_kwargs = {'company': {'write_only': True}}

    def create(self, validated_data):
        if 'role' in self.initial_data:
            selected_user_role = UserRole.objects.get(name=self.initial_data['role'])
            validated_data['user_role'] = selected_user_role
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        if 'role' in self.initial_data:
            selected_user_role = UserRole.objects.get(name=self.initial_data['role'])
            validated_data['user_role'] = selected_user_role
        new_password = validated_data.pop('password', None)
        if new_password:
            instance.set_password(new_password)
        instance = setattr_avatar(instance, self.initial_data.get('avatar'))
        return super().update(instance, validated_data)

    def get_editable(self, obj):  # TODO: need an unit test.
        try:
            user = self.context.get('request').user
            return obj == user or is_admin_or_master(user)
        except AttributeError:
            return False

    def get_role(self, obj):
        return obj.user_role_name

    def get_avatar(self, obj):
        if not obj.avatar:
            return None
        return obj.avatar.url


class MyProfileSerializer(ModelSerializer):
    """A serializer to see / edit own profile.
    Note: This is a wrapper of model User, but it only allows to edit specific fields.
    """
    password = CharField(max_length=1024, write_only=True)
    role = serializers.SerializerMethodField()
    modified_user__name = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def custom_validation(self, data):
        check_user_service_id_duplicate(self.context.get('request'), data)
        custom_validation(data, 'user')

    class Meta:
        model = User
        fields = ('last_name', 'first_name', 'email', 'email_signature', 'tel1', 'tel2', 'tel3', 'role', 'password',
                  'modified_time', 'modified_user__name', 'user_service_id', 'avatar')
        read_only_fields = ('email', 'role')

    def update(self, instance, validated_data):
        new_password = validated_data.pop('password', None)
        if new_password:
            instance.set_password(new_password)
        instance = setattr_avatar(instance, self.initial_data.get('avatar'))
        return super().update(instance, validated_data)

    def get_role(self, obj):
        return obj.user_role_name

    def get_avatar(self, obj):
        if not obj.avatar:
            return None
        return obj.avatar.url
