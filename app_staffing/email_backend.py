from django.core.mail.backends.smtp import EmailBackend
from django.conf import settings
import dkim

class DKIMBackend(EmailBackend):
    def _send(self, email_message):
        """A helper method that does the actual sending + DKIM signing."""
        if not email_message.recipients():
            return False
        try:
            message_string = email_message.message().as_string().encode('utf-8')
            signature = dkim.sign(message_string,
                                  settings.DKIM_SELECTOR.encode('utf-8'),
                                  settings.DKIM_DOMAIN.encode('utf-8'),
                                  settings.DKIM_PRIVATE_KEY.encode('utf-8'))
            self.connection.sendmail(email_message.from_email,
                    email_message.recipients(),
                    signature+message_string)
        except:
            if not self.fail_silently:
                raise
            return False
        return True
