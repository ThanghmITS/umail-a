from django.apps import AppConfig


class AppStaffingConfig(AppConfig):
    name = 'app_staffing'

    def ready(self):
        pass
