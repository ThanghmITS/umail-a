# Generated by Django 2.2.13 on 2021-03-22 05:26

import app_staffing.models.base
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0056_add_index_to_employee_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduledEmailSearchCondition',
            fields=[
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('modified_time', models.DateTimeField(auto_now=True)),
                ('email', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='searchcondition', serialize=False, to='app_staffing.ScheduledEmail')),
                ('search_condition', models.TextField(help_text='Search condition of targets of the email.')),
                ('created_user', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='created_scheduledemailsearchcondition', to=settings.AUTH_USER_MODEL)),
                ('modified_user', models.ForeignKey(default=None, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='modified_scheduledemailsearchcondition', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(app_staffing.models.base.AutoUserFieldsMixin, models.Model),
        ),
    ]
