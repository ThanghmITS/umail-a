# Generated by Django 2.2.20 on 2021-04-29 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0065_add_has_distribution_to_organization'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='has_distribution',
            field=models.BooleanField(default=False),
        ),
    ]
