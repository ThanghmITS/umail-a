from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('app_staffing', '0142_remove_contact_preference_column'),
    ]

    operations = [
        migrations.RunSQL(
            sql=r'CREATE INDEX "stf_schedMail_open_ratio_idx" ON "app_staffing_scheduledemail"(( CASE WHEN text_format = "text" THEN NULL WHEN text_format = ' + "'html'" +' THEN CASE WHEN send_total_count = 0 THEN NULL WHEN scheduled_email_status_id = 1 THEN NULL WHEN scheduled_email_status_id = 2 THEN NULL WHEN scheduled_email_status_id = 3 THEN NULL ELSE ROUND(open_count * 100.0 / send_total_count, 2) END END ));',
            reverse_sql=r'DROP INDEX "stf_schedMail_open_ratio_idx";'
        ),
        migrations.RunSQL(
            sql=r'CREATE INDEX "stf_schedMail_open_count_format_idx" ON "app_staffing_scheduledemail"(( CASE WHEN text_format = "text" THEN NULL WHEN text_format = ' + "'html'" +' THEN CASE WHEN send_total_count = 0 THEN NULL WHEN scheduled_email_status_id = 1 THEN NULL WHEN scheduled_email_status_id = 2 THEN NULL WHEN scheduled_email_status_id = 3 THEN NULL ELSE open_count END END ));',
            reverse_sql=r'DROP INDEX "stf_schedMail_open_count_format_idx";'
        ),
    ]
