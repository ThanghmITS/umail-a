# Generated by Django 2.2.18 on 2021-04-05 05:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0058_add_address_and_building_to_company'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='country',
            field=models.CharField(help_text='会社の国籍', max_length=8),
        ),
    ]
