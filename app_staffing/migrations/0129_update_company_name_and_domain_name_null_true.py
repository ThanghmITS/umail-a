# Generated by Django 2.2.25 on 2021-12-21 09:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0128_contract_to_organization'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='domain_name',
            field=models.CharField(blank=True, help_text='会社のURL', max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(blank=True, help_text='このAppを利用する会社名', max_length=128, null=True),
        ),
    ]
