# Generated by Django 2.2.24 on 2021-07-28 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0084_create_shared_email_transfer_history_table'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduledemail',
            name='open_count',
            field=models.IntegerField(default=0, help_text='開封数'),
        ),
    ]
