# Generated by Django 2.2.27 on 2022-04-14 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0145_add_send_count_per_task'),
    ]

    def update_user_service_id(apps, schema_editor):
        User = apps.get_model("app_staffing", "User")
        users = list(User.objects.all())
        initial_id = 1000000000
        for index, user in enumerate(users):
            user.user_service_id = f'user_{initial_id + index}'

        User.objects.bulk_update(users, ['user_service_id'])

    def update_user_service_id_reverse(apps, schema_editor):
        pass

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={},
        ),
        migrations.AddField(
            model_name='user',
            name='user_service_id',
            field=models.CharField(max_length=15, null=True),
        ),
        migrations.RunPython(update_user_service_id, update_user_service_id_reverse),
        migrations.AlterField(
            model_name='user',
            name='user_service_id',
            field=models.CharField(max_length=15, null=False),
        ),
        migrations.AlterUniqueTogether(
            name='user',
            unique_together={('user_service_id', 'company')},
        ),
    ]
