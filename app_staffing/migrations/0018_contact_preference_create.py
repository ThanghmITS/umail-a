# Created manually by Asayu123 on 2019-08-16 10:57 (JST)

from django.db import migrations, models


def create_missing_preferences(apps, schema_editor):
    Contact = apps.get_model('app_staffing', 'Contact')
    ContactPreference = apps.get_model('app_staffing', 'ContactPreference')

    target_contact = Contact.objects.filter(contactpreference__isnull=True)
    print(' ')  # feeding line
    for contact in target_contact:
        # Create preferences with default parameter.
        print(f'Creating a default preference for the contact <{contact.id}> : {contact.name}@{contact.organization.name}')
        ContactPreference(contact=contact).save()


class Migration(migrations.Migration):
    """This migration creates a contact preference to all contacts if contact does not have a preference object.
    In the newer version of the app, a preference will automatically be created when creating a Contact instance.
    """

    dependencies = [
        ('app_staffing', '0017_organization_simplify'),
    ]

    operations = [
        migrations.RunPython(create_missing_preferences, reverse_code=migrations.RunPython.noop)
    ]
