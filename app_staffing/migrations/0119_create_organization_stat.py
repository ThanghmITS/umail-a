# Generated by Django 2.2.24 on 2021-10-08 05:58

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0118_delete_plan_function_description_snippet'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizationStat',
            fields=[
                ('deleted_at', models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('target_date', models.DateField()),
                ('prospective', models.PositiveIntegerField(default=0)),
                ('approached', models.PositiveIntegerField(default=0)),
                ('exchanged', models.PositiveIntegerField(default=0)),
                ('client', models.PositiveIntegerField(default=0)),
                ('blocklist', models.PositiveIntegerField(default=0)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_staffing.Company')),
            ],
        ),
        migrations.AddConstraint(
            model_name='organizationstat',
            constraint=models.UniqueConstraint(condition=models.Q(deleted_at=None), fields=('company', 'target_date'), name='uniq_company_target_date'),
        ),
    ]
