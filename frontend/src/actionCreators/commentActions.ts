import { PayloadAction } from "~/models/reduxModel";
import {
    COMMENT_EDIT_CLEAR,
    COMMENT_NEW_CLEAR,
    PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS,
    PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS,
    WRITING_REPLY_COMMENTS,
} from "~/actions/actionTypes";

export const CommentActions = {
    replyAction: (payload: any): PayloadAction<any> => {
        return {
            type: WRITING_REPLY_COMMENTS,
            payload,
        };
    },
    newCommentExistAction: (): PayloadAction<undefined> => {
        return {
            type: PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS,
            payload: undefined,
        };
    },
    newCommentClearAction: (): PayloadAction<undefined> => {
        return {
            type: COMMENT_NEW_CLEAR,
            payload: undefined,
        };
    },
    editCommentExistAction: (): PayloadAction<undefined> => {
        return {
            type: PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS,
            payload: undefined,
        };
    },
    editCommentClearAction: (): PayloadAction<undefined> => {
        return {
            type: COMMENT_EDIT_CLEAR,
            payload: undefined,
        };
    },
};
