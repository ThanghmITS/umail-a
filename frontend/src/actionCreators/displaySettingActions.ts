import { DISPLAY_SETTING_LOADED } from "~/actions/actionTypes";
import { DISPLAY_SETTING_PAGE } from "~/components/Pages/pageIds";
import { PayloadAction } from "~/models/reduxModel";

export const DisplaySettingActions = {
    loadedAction: (payload: any): PayloadAction<any> => {
        return {
            type: DISPLAY_SETTING_PAGE + DISPLAY_SETTING_LOADED,
            payload,
        };
    },
};
