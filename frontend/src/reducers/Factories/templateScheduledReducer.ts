import {
  SCHEDULED_TEMPLATE_LOADED,
  SCHEDULED_TEMPLATE_LOADING,
  SCHEDULE_TEMPLATE_CREATING,
  SCHEDULE_TEMPLATE_CREATED
} from "../../actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";

const commentActionSuffix = "__COMMENTS";

export type TemplateScheduleInitialStateModel = {
  loading: boolean,
  message: string,
  errorMessage: string,
  data: any,
  fieldErrors: any,
  dataTemplate: any
};

export const defaultInitialState: TemplateScheduleInitialStateModel = {
  loading: false,
  message: "",
  errorMessage: "",
  data: {},
  fieldErrors: {},
  dataTemplate: {}
};

export const createTemplateCommentReducer = (
  pageId: string,
  initialState = defaultInitialState
) => {
  const templateScheduledReducer = (
      state = initialState,
      action: PayloadAction<TemplateScheduleInitialStateModel>
  ): TemplateScheduleInitialStateModel => {
      switch (action.type) {
              case pageId + SCHEDULED_TEMPLATE_LOADING:
                return {
                  ...state,
                  loading: true,
                  message: "",
                  errorMessage: "",
                };
            case pageId + SCHEDULED_TEMPLATE_LOADED:
                return {
                  ...state,
                  loading: false,
                  message: "",
                  errorMessage: "",
                  fieldErrors: {},
                  dataTemplate: action.payload.data,
                };

              case pageId + SCHEDULE_TEMPLATE_CREATING:
                return {
                    ...state,
                    message: "",
                    errorMessage: "",
                };
              case pageId + SCHEDULE_TEMPLATE_CREATED:
                return {
                    ...state,
                    dataTemplate: action.payload.data,
                };
          default:
              return { ...state };
      }
  };
  return templateScheduledReducer;
};

export default createTemplateCommentReducer;
