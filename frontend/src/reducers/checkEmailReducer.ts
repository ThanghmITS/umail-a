import {
  CHECKING_EMAIL_ADDRESS,
  CHECKED_EMAIL_ADDRESS,
  CHECK_ERROR_EMAIL_ADDRESS,
  CHECK_CLEAR_EMAIL_ADDRESS,
} from "~/actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";

export type CheckEmailInitialStateModel = {
  loading: boolean,
  message: string,
  errorMessage: string,
};

export const CheckEmailInitialState: CheckEmailInitialStateModel =
{
  loading: false,
  message: "",
  errorMessage: "",
};

export const checkEmailReducer = (pageId: string) => {
  const Reducer = (
    state = CheckEmailInitialState,
    action: PayloadAction<CheckEmailInitialStateModel>
  ): CheckEmailInitialStateModel => {
    switch (action.type) {
      case pageId + CHECKING_EMAIL_ADDRESS:
        return {
          ...state,
          loading: true,
          message: "",
          errorMessage: "",
        };
      case pageId + CHECKED_EMAIL_ADDRESS:
        return {
          ...state,
          loading: false,
          message: action.payload.message,
          errorMessage: "",
        };
      case pageId + CHECK_ERROR_EMAIL_ADDRESS:
        return {
          ...state,
          loading: false,
          message: "",
          errorMessage: action.payload.errorMessage,
        };
      case pageId + CHECK_CLEAR_EMAIL_ADDRESS:
        return {
          ...state,
          loading: false,
          message: "",
          errorMessage: "",
      };
      default:
        return { ...state };
    }
  };
  return Reducer;
};
