import {
    CANCELED,
    CLEAR,
    CLEAR_ERROR,
    CLEAR_SUCCESS,
    ERROR,
    LOADED,
    LOADING,
    PAYMENT_DELETE,
    PAYMENT_FETCH,
    PAYMENT_FORM,
    PAYMENT_UPDATE,
    VALIDATION_ERROR,
} from "~/actions/actionTypes";
import { PaymentFetchModel } from "~/models/paymentModel";
import { PayloadAction } from "~/models/reduxModel";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";

export type PaymentInfoInitialStateModel = {
    isLoading: boolean;
    paymentInfo?: PaymentFetchModel;
    errorMessage: string;
};

export const PaymentInfoState: PaymentInfoInitialStateModel = {
    isLoading: false,
    paymentInfo: undefined,
    errorMessage: "",
};

export const paymentInfo = (
    state = PaymentInfoState,
    action: PayloadAction<PaymentFetchModel>
): PaymentInfoInitialStateModel => {
    switch (action.type) {
        case PAYMENT_FETCH + LOADING:
            return {
                isLoading: true,
                paymentInfo: state.paymentInfo,
                errorMessage: "",
            };
        case PAYMENT_FETCH + LOADED:
            return {
                isLoading: false,
                paymentInfo: action.payload,
                errorMessage: "",
            };
        case PAYMENT_FETCH + VALIDATION_ERROR:
        case PAYMENT_FETCH + CANCELED:
        case PAYMENT_FETCH + ERROR:
            return {
                ...state,
                isLoading: false,
                errorMessage: ErrorMessages.payment.fetchInfo,
            };
        case PAYMENT_FETCH + CLEAR_ERROR:
            return {
                ...state,
                errorMessage: "",
            };
        case PAYMENT_FETCH + CLEAR:
            return { ...PaymentInfoState };
        default:
            return { ...state };
    }
};

export type PaymentFormInitialStateModel = {
    isLoading: boolean;
    successMessage: string;
    errorMessage: string;
};

export const PaymentFormInitialState: PaymentFormInitialStateModel = {
    isLoading: false,
    successMessage: "",
    errorMessage: "",
};

export const paymentForm = (
    state = PaymentFormInitialState,
    action: PayloadAction<any>
): PaymentFormInitialStateModel => {
    switch (action.type) {
        case PAYMENT_FORM + LOADING:
            return {
                isLoading: true,
                successMessage: "",
                errorMessage: "",
            };
        case PAYMENT_FORM + LOADED:
            return {
                isLoading: false,
                successMessage: SuccessMessages.payment.register,
                errorMessage: "",
            };
        case PAYMENT_FORM + VALIDATION_ERROR:
        case PAYMENT_FORM + CANCELED:
        case PAYMENT_FORM + ERROR:
            return {
                ...state,
                isLoading: false,
                successMessage: "",
                errorMessage: ErrorMessages.payment.register,
            };
        case PAYMENT_FORM + CLEAR_SUCCESS:
            return {
                ...state,
                successMessage: "",
            };
        case PAYMENT_FORM + CLEAR_ERROR:
            return {
                ...state,
                errorMessage: "",
            };
        case PAYMENT_FORM + CLEAR:
            return { ...PaymentFormInitialState };
        default:
            return { ...state };
    }
};

export type PaymentDeleteInitialStateModel = {
    isLoading: boolean;
    successMessage: string;
    errorMessage: string;
};

export const PaymentDeleteInitialState: PaymentDeleteInitialStateModel = {
    isLoading: false,
    successMessage: "",
    errorMessage: "",
};

export const paymentDelete = (
    state = PaymentDeleteInitialState,
    action: PayloadAction<any>
): PaymentDeleteInitialStateModel => {
    switch (action.type) {
        case PAYMENT_DELETE + LOADING:
            return {
                isLoading: true,
                successMessage: "",
                errorMessage: "",
            };
        case PAYMENT_DELETE + LOADED:
            return {
                isLoading: false,
                successMessage: SuccessMessages.payment.delete,
                errorMessage: "",
            };
        case PAYMENT_DELETE + VALIDATION_ERROR:
        case PAYMENT_DELETE + CANCELED:
        case PAYMENT_DELETE + ERROR:
            return {
                ...state,
                isLoading: false,
                successMessage: "",
                errorMessage: ErrorMessages.payment.delete,
            };
        case PAYMENT_DELETE + CLEAR_SUCCESS:
            return {
                ...state,
                successMessage: "",
            };
        case PAYMENT_DELETE + CLEAR_ERROR:
            return {
                ...state,
                errorMessage: "",
            };
        case PAYMENT_DELETE + CLEAR:
            return { ...PaymentDeleteInitialState };
        default:
            return { ...state };
    }
};

export type PaymentUpdateInitialStateModel = {
    isLoading: boolean;
    successMessage: string;
    errorMessage: string;
};

export const PaymentUpdateInitialState: PaymentUpdateInitialStateModel = {
    isLoading: false,
    successMessage: "",
    errorMessage: "",
};

export const paymentUpdate = (
    state = PaymentUpdateInitialState,
    action: PayloadAction<any>
): PaymentUpdateInitialStateModel => {
    switch (action.type) {
        case PAYMENT_UPDATE + LOADING:
            return {
                isLoading: true,
                successMessage: "",
                errorMessage: "",
            };
        case PAYMENT_UPDATE + LOADED:
            return {
                isLoading: false,
                successMessage: SuccessMessages.payment.update,
                errorMessage: "",
            };
        case PAYMENT_UPDATE + VALIDATION_ERROR:
        case PAYMENT_UPDATE + CANCELED:
        case PAYMENT_UPDATE + ERROR:
            return {
                ...state,
                isLoading: false,
                successMessage: "",
                errorMessage: ErrorMessages.payment.update,
            };
        case PAYMENT_UPDATE + CLEAR_SUCCESS:
            return {
                ...state,
                successMessage: "",
            };
        case PAYMENT_UPDATE + CLEAR_ERROR:
            return {
                ...state,
                errorMessage: "",
            };
        case PAYMENT_UPDATE + CLEAR:
            return { ...PaymentUpdateInitialState };
        default:
            return { ...state };
    }
};
