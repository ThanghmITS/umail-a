import {
    WRITING_NEW_COMMENTS,
    PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS,
    COMMENT_NEW_CLEAR,
    WRITING_EDIT_COMMENTS,
    WRITING_REPLY_COMMENTS,
    PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS,
    PARENT_CANNOT_SUBMIT_WHEN_REPLY_COMMENT_EXISTS,
    COMMENT_EDIT_CLEAR,
    CREATED,
    UPDATED,
    VALIDATION_ERROR,
} from "~/actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";

const commentActionSuffix = "__COMMENTS";

export type NewCommentInitialStateModel = {
    commentValue: string;
    commentError: string;
    error: any;
    method: string;
};

export const NewCommentInitialState: NewCommentInitialStateModel = {
    commentValue: "",
    commentError: "",
    error: undefined,
    method: "",
};

export const createNewCommentReducer = (
    pageId: string,
    initialState = NewCommentInitialState
) => {
    const newCommentReducer = (
        state = initialState,
        action: PayloadAction<NewCommentInitialStateModel>
    ): NewCommentInitialStateModel => {
        switch (action.type) {
            case COMMENT_NEW_CLEAR:
                return {
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case WRITING_NEW_COMMENTS:
                return {
                    commentValue: action.payload.commentValue,
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS:
                return {
                    ...state,
                    commentError: "コメント欄は空にしてください。",
                    error: undefined,
                };
            case pageId + commentActionSuffix + CREATED:
                return {
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case pageId + commentActionSuffix + VALIDATION_ERROR:
                if (action.payload.method !== 'create') {
                    return {
                        ...state,
                    }
                }
                const error = action.payload.error;
                let errorMessage = error.message
                if (error.field_errors && error.field_errors.detail) {
                    errorMessage = error.field_errors.detail
                } else if(error.field_errors && error.field_errors.content) {
                    errorMessage = error.field_errors.content
                }
                return {
                    ...state,
                    commentError: errorMessage,
                    error: undefined,
                    method: "",
                }
            default:
                return { ...state };
        }
    };
    return newCommentReducer;
};

export type EditCommentInitialStateModel = {
    commentValue: string;
    commentError: string;
    error: any;
    method: string;
};

export const EditCommentInitialState: EditCommentInitialStateModel = {
    commentValue: "",
    commentError: "",
    error: undefined,
    method: "",
};

export const createEditCommentReducer = (
    pageId: string,
    initialState = EditCommentInitialState
) => {
    const editCommentReducer = (
        state = initialState,
        action: PayloadAction<EditCommentInitialStateModel>
    ): EditCommentInitialStateModel => {
        switch (action.type) {
            case COMMENT_EDIT_CLEAR:
                return {
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case WRITING_EDIT_COMMENTS:
                return {
                    commentValue: action.payload.commentValue,
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case WRITING_REPLY_COMMENTS:
              return {
                    ...state,
                    commentValue: action.payload.commentValue,
                    commentError: "",
                    error: undefined,
                    method: "",
              };
            case PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS:
                return {
                    ...state,
                    commentError: "コメントは更新してください。",
                    error: undefined,
                };
            case pageId + commentActionSuffix + UPDATED:
                return {
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case pageId + commentActionSuffix + VALIDATION_ERROR:
                if (action.payload.method !== 'patch') {
                    return {
                        ...state,
                    }
                }
                const error = action.payload.error;
                let errorMessage = error.message
                if (error.field_errors && error.field_errors.detail) {
                    errorMessage = error.field_errors.detail
                } else if(error.field_errors && error.field_errors.content) {
                    errorMessage = error.field_errors.content
                }
                return {
                    ...state,
                    commentError: errorMessage,
                    error: undefined,
                    method: "",
                }
            default:
                return { ...state };
        }
    };
    return editCommentReducer;
};

export type ReplyCommentInitialStateModel = {
  commentValue: string;
  commentError: string;
  error: any;
  method: string;
};

export const ReplyCommentInitialState: ReplyCommentInitialStateModel = {
  commentValue: "",
  commentError: "",
  error: undefined,
  method: "",
};

export const createReplyCommentReducer = (
  pageId: string,
  initialState = ReplyCommentInitialState
) => {
  const replyCommentReducer = (
      state = initialState,
      action: PayloadAction<ReplyCommentInitialStateModel>
  ): ReplyCommentInitialStateModel => {
      switch (action.type) {
          case COMMENT_NEW_CLEAR:
              return {
                  ...state,
                  commentValue: "",
                  commentError: "",
                  error: undefined,
                  method: "",
              };
          case WRITING_REPLY_COMMENTS:
              return {
                  ...state,
                  commentValue: action.payload.commentValue,
                  commentError: "",
                  error: undefined,
                  method: "",
              };
          case PARENT_CANNOT_SUBMIT_WHEN_REPLY_COMMENT_EXISTS:
              return {
                  ...state,
                  commentError: "コメント欄は空にしてください。",
                  error: undefined,
              };
          case pageId + commentActionSuffix + CREATED:
              return {
                  ...state,
                  commentValue: "",
                  commentError: "",
                  error: undefined,
                  method: "",
              };
          case pageId + commentActionSuffix + VALIDATION_ERROR:
              if (action.payload.method !== 'create') {
                  return {
                      ...state,
                  }
              }
              const error = action.payload.error;
              let errorMessage = error.message
              if (error.field_errors && error.field_errors.detail) {
                  errorMessage = error.field_errors.detail
              } else if(error.field_errors && error.field_errors.content) {
                  errorMessage = error.field_errors.content
              }
              return {
                  ...state,
                  commentError: errorMessage,
                  error: undefined,
                  method: "",
              }
          default:
              return { ...state };
      }
  };
  return replyCommentReducer;
};
