// declare module "*.json" {
//     var json: { [key: string]: string | number | boolean | null };
//     export = json;
// }
declare module "*.json" {
    const value: any;
    export default value;
}
