import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import { PaginationRequestModel } from "~/models/requestModel";
import { PaginationResponseModel } from "~/models/responseModel";
import {
    TagBulkDeleteModel,
    TagDeleteResponseModel,
    TagPostModel,
    TagResponseModel,
    TagUpdateParamModel,
} from "~/models/tagModel";
import { DEFAULT_PAGE_SIZE } from "~/utils/constants";

const tag = (client: AxiosInstance) => {
    return {
        fetchTags(
            query: PaginationRequestModel
        ): Promise<AxiosResponse<PaginationResponseModel<TagResponseModel[]>>> {
            let url =
                Endpoint.tags +
                "?" +
                `page_size=${query.pageSize ?? DEFAULT_PAGE_SIZE}`;
            if (query.page) {
                url += "&page=" + query.page;
            }
            if (query.value) {
                url += "&value=" + query.value;
            }
            return client.get(url);
        },
        bulkDelete(
            postData: TagBulkDeleteModel
        ): Promise<AxiosResponse<string[]>> {
            const url = Endpoint.tags;
            return client.delete(url, { data: postData });
        },
        fetchTag(id: string): Promise<AxiosResponse<TagResponseModel>> {
            const url = Endpoint.tags + "/" + id;
            return client.get(url);
        },
        create(
            postData: TagPostModel
        ): Promise<AxiosResponse<TagResponseModel>> {
            const url = Endpoint.tags;
            return client.post(url, postData);
        },
        update({
            id,
            postData,
        }: TagUpdateParamModel): Promise<AxiosResponse<TagResponseModel>> {
            const url = Endpoint.tags + "/" + id;
            return client.patch(url, postData);
        },
        delete(id: string): Promise<AxiosResponse<TagDeleteResponseModel>> {
            const url = Endpoint.tags + "/" + id;
            return client.delete(url);
        },
    };
};

export default tag;
