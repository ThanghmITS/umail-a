import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    ScheduledEmailSettingBaseSettingFormModel,
    ScheduledEmailSettingRequestModel,
    ScheduledEmailSettingResponseModel,
} from "~/models/scheduledEmailSettingModel";

const scheduledEmailSetting = (client: AxiosInstance) => {
    return {
        fetchScheduledEmailSetting(): Promise<
            AxiosResponse<ScheduledEmailSettingResponseModel>
        > {
            const url = Endpoint.scheduledEmailSetting;
            return client.get(url);
        },
        testConnection(
            postData: ScheduledEmailSettingBaseSettingFormModel
        ): Promise<AxiosResponse<{}>> {
            const url = Endpoint.scheduledEmailConnection;
            return client.post(url, postData);
        },
        updateScheduledEmailSetting(
            postData: ScheduledEmailSettingRequestModel
        ): Promise<AxiosResponse<ScheduledEmailSettingResponseModel>> {
            const url = Endpoint.scheduledEmailSetting;
            return client.patch(url, postData);
        },
    };
};

export default scheduledEmailSetting;
