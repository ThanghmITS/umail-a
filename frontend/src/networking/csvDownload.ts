import { AxiosInstance } from "axios";
import { Endpoint } from "~/domain/api";
import { CsvDownloadRequestArgs } from "~/models/csvControlModel";

const csvDownload = (client: AxiosInstance) => {
    return {
        async organizationCsvDownload({
            index,
            queryString,
        }: CsvDownloadRequestArgs) {
            let url = Endpoint.organizationsCsv + "?index=" + index;
            if (queryString) {
                url += "&" + queryString;
            }
            return await client.get(url, { responseType: "text" });
        },
        async contactCsvDownload({
            index,
            queryString,
        }: CsvDownloadRequestArgs) {
            let url = Endpoint.contactsCsv + "?index=" + index;
            if (queryString) {
                url += "&" + queryString;
            }
            return await client.get(url, { responseType: "text" });
        },
        async contactCsvDownloadWithCondition({
            index,
            queryString,
        }: CsvDownloadRequestArgs) {
            let url = Endpoint.contactsFullCsv + "?index=" + index;
            if (queryString) {
                url += "&" + queryString;
            }
            return await client.get(url, { responseType: "text" });
        },
    };
};

export default csvDownload;
