import { AxiosInstance, AxiosResponse } from "axios";
import {
    BoardChecklistCreateFormModel,
    BoardChecklistCreateResponseModel,
    BoardChecklistItemCreateFormModel,
    BoardChecklistItemModel,
    BoardChecklistItemUpdateFormModel,
    BoardChecklistModel,
    BoardChecklistUpdateFormModel,
    BoardColumnModel,
    BoardCommentCreateFormModel,
    BoardCommentModel,
    BoardCommentUpdateFormModel,
    CardChangePositionRequestModel,
} from "~/models/boardCommonModel";
import {
    ProjectBoardCardCreateResponseModel,
    ProjectBoardContractCreateFromModel,
    ProjectBoardContractModel,
    ProjectBoardContractUpdateFormModel,
    ProjectBoardDetailModel,
    ProjectBoardFormModel,
    ProjectBoardListCardModel,
    ProjectBoardNewCardFormModel,
} from "~/models/projectBoardModel";
import {
    PaginationResponseModel,
    SuccessDetailResponse,
} from "~/models/responseModel";
import { OrganizationListModel }  from "~/models/organizationModel";
import { ContactListModel }  from "~/models/contactModel";

export const projectBoardBasePath = "board/project";

const listsOperations = (client: AxiosInstance) => {
    const path = (dynamicPath: string = ""): string => {
        return projectBoardBasePath + "/lists" + dynamicPath;
    };

    return {
        fetchLists(): Promise<AxiosResponse<BoardColumnModel[]>> {
            const url = path();
            return client.get(url);
        },
        fetchListCards(
            listId: string
        ): Promise<
            AxiosResponse<PaginationResponseModel<ProjectBoardListCardModel[]>>
        > {
            const url = path("/" + listId);
            return client.get(url);
        },
    };
};

const cardsOperations = (client: AxiosInstance) => {
    const path = (dynamicPath: string = ""): string => {
        return projectBoardBasePath + "/cards" + dynamicPath;
    };

    return {
        createCard(
            postData: ProjectBoardNewCardFormModel
        ): Promise<AxiosResponse<ProjectBoardCardCreateResponseModel>> {
            const url = path();
            return client.post(url, postData);
        },
        fetchCard(
            cardId: string
        ): Promise<AxiosResponse<ProjectBoardDetailModel>> {
            const url = path("/" + cardId);
            return client.get(url);
        },
        updateCard(
            cardId: string,
            postData: ProjectBoardFormModel
        ): Promise<AxiosResponse<ProjectBoardDetailModel>> {
            const url = path("/" + cardId);
            return client.patch(url, postData);
        },
        deleteCard(
            cardId: string
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + cardId);
            return client.delete(url);
        },
        changeCardPosition(
            cardId: string,
            postData: CardChangePositionRequestModel
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + cardId + "/position");
            return client.patch(url, postData);
        },
    };
};

const checklistsOperations = (client: AxiosInstance) => {
    const path = (dynamicPath: string = ""): string => {
        return projectBoardBasePath + "/checklists" + dynamicPath;
    };

    return {
        fetchChecklists(
            cardId: string
        ): Promise<AxiosResponse<BoardChecklistModel[]>> {
            const url = path("?projectId=" + cardId);
            return client.get(url);
        },
        createChecklist(
            postData: BoardChecklistCreateFormModel
        ): Promise<AxiosResponse<BoardChecklistCreateResponseModel>> {
            const url = path();
            return client.post(url, postData);
        },
        fetchChecklist(
            checklistId: string
        ): Promise<AxiosResponse<BoardChecklistModel>> {
            const url = path("/" + checklistId);
            return client.get(url);
        },
        updateChecklist(
            checklistId: string,
            postData: BoardChecklistUpdateFormModel
        ): Promise<AxiosResponse<BoardChecklistModel>> {
            const url = path("/" + checklistId);
            return client.patch(url, postData);
        },
        deleteChecklist(
            checklistId: string
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + checklistId);
            return client.delete(url);
        },
        fetchChecklistItems(
            checklistId: string
        ): Promise<AxiosResponse<BoardChecklistItemModel[]>> {
            const url = path("/" + checklistId + "/items");
            return client.get(url);
        },
        createChecklistItem(
            checklistId: string,
            postData: BoardChecklistItemCreateFormModel
        ): Promise<AxiosResponse<BoardChecklistItemModel>> {
            const url = path("/" + checklistId + "/items");
            return client.post(url, postData);
        },
        fetchChecklistItem(
            checklistId: string,
            itemId: string
        ): Promise<AxiosResponse<BoardChecklistItemModel>> {
            const url = path("/" + checklistId + "items" + itemId);
            return client.get(url);
        },
        updateChecklistItem(
            checklistId: string,
            itemId: string,
            postData: BoardChecklistItemUpdateFormModel
        ): Promise<AxiosResponse<BoardChecklistItemModel>> {
            const url = path("/" + checklistId + "items" + itemId);
            return client.patch(url, postData);
        },
        deleteChecklistItem(
            checklistId: string,
            itemId: string
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + checklistId + "items" + itemId);
            return client.delete(url);
        },
    };
};

const commentsOperations = (client: AxiosInstance) => {
    const path = (dynamicPath: string = ""): string => {
        return projectBoardBasePath + "/comments" + dynamicPath;
    };

    return {
        fetchComments(
            cardId: string
        ): Promise<AxiosResponse<BoardCommentModel[]>> {
            const url = path("/?cardId=" + cardId);
            return client.get(url);
        },
        createComment(
            postData: BoardCommentCreateFormModel
        ): Promise<AxiosResponse<BoardCommentModel>> {
            const url = path();
            return client.post(url, postData);
        },
        fetchComment(
            commentId: string
        ): Promise<AxiosResponse<BoardCommentModel>> {
            const url = path("/" + commentId);
            return client.get(url);
        },
        updateComment(
            commentId: string,
            postData: BoardCommentUpdateFormModel
        ): Promise<AxiosResponse<BoardCommentModel>> {
            const url = path("/" + commentId);
            return client.patch(url, postData);
        },
        deleteComment(
            commentId: string
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + commentId);
            return client.delete(url);
        },
        fetchSubComments(
            commentId: string
        ): Promise<AxiosResponse<BoardCommentModel[]>> {
            const url = path("/" + commentId + "/sub-comments");
            return client.get(url);
        },
    };
};

const contractsOperations = (client: AxiosInstance) => {
    const path = (dynamicPath: string = ""): string => {
        return projectBoardBasePath + "/contracts" + dynamicPath;
    };

    return {
        fetchContracts(
            cardId: string
        ): Promise<AxiosResponse<ProjectBoardContractModel[]>> {
            const url = path("/?cardId=" + cardId);
            return client.get(url);
        },
        createContract(
            postData: ProjectBoardContractCreateFromModel
        ): Promise<AxiosResponse<ProjectBoardContractModel>> {
            const url = path();
            return client.post(url, postData);
        },
        fetchContract(
            contractId: string
        ): Promise<AxiosResponse<ProjectBoardContractModel>> {
            const url = path("/" + contractId);
            return client.get(url);
        },
        updateContract(
            contractId: string,
            postData: ProjectBoardContractUpdateFormModel
        ): Promise<AxiosResponse<BoardCommentModel>> {
            const url = path("/" + contractId);
            return client.patch(url, postData);
        },
        deleteContract(
            contractId: string
        ): Promise<AxiosResponse<SuccessDetailResponse>> {
            const url = path("/" + contractId);
            return client.delete(url);
        },
        updateCardOrder(
          cardId: string,
          postData: {
              listId: string,
              position:number   
          }
        ): Promise<AxiosResponse<ProjectBoardDetailModel>>{
            const url = path("/" + cardId + "/position");
            return client.patch(url, postData);
        },
        fetchOrganizations(
        ): Promise<AxiosResponse<OrganizationListModel[]>> {
            const url = "/organizations";
            return client.get(url);
        },
        fetchContacts(
        ): Promise<AxiosResponse<ContactListModel[]>> {
            const url = "/contacts";
            return client.get(url);
        },
    };
};

const projectBoard = (client: AxiosInstance) => {
    return {
        ...listsOperations(client),
        ...cardsOperations(client),
        ...checklistsOperations(client),
        ...commentsOperations(client),
        ...contractsOperations(client),
    };
};

export default projectBoard;
