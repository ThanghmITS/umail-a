import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    PasswordResetRequestFormModel,
    PasswordResetResponseModel,
} from "~/models/authModel";

const account = (client: AxiosInstance) => {
    return {
        deleteAccount(): Promise<AxiosResponse<any>> {
            return client.delete(Endpoint.account);
        },
        passwordReset(
            postData: PasswordResetRequestFormModel
        ): Promise<AxiosResponse<PasswordResetResponseModel>> {
            const url = Endpoint.passwordResetMail;
            return client.post(url, postData);
        },
    };
};

export default account;
