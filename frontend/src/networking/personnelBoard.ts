import { AxiosInstance, AxiosResponse } from "axios";
import {
  BoardChecklistCreateFormModel,
  BoardChecklistCreateResponseModel,
  BoardChecklistItemCreateFormModel,
  BoardChecklistItemModel,
  BoardChecklistItemUpdateFormModel,
  BoardChecklistModel,
  BoardChecklistUpdateFormModel,
  BoardColumnModel,
  BoardCommentCreateFormModel,
  BoardCommentModel,
  BoardCommentUpdateFormModel,
  CardChangePositionRequestModel,
} from "~/models/boardCommonModel";
import {
  PersonnelBoardCardCreateResponseModel,
  PersonnelBoardContractCreateFormModel,
  PersonnelBoardContractModel,
  PersonnelBoardContractUpdateFormModel,
  PersonnelBoardDetailModel,
  PersonnelBoardFormModel,
  PersonnelBoardListCardModel,
  PersonnelBoardNewCardFormModel,
  PersonnelBoardTrainStationModel,
  PersonnelBoardSkillSheetModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import {
  PaginationResponseModel,
  SuccessDetailResponse,
} from "~/models/responseModel";
import { ContactListModel } from "~/models/contactModel";
import { OrganizationListModel } from "~/models/organizationModel";

export const personnelBoardBasePath = "board/personnel";

const listsOperations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/lists" + dynamicPath;
  };

  return {
    fetchLists(): Promise<AxiosResponse<BoardColumnModel[]>> {
      const url = path();
      return client.get(url);
    },
    fetchListCards(
      listId: string
    ): Promise<
      AxiosResponse<PaginationResponseModel<PersonnelBoardListCardModel[]>>
    > {
      const url = path("/" + listId);
      return client.get(url);
    },
    fetchListCardWithouArchives(
      listId: string,
      isArchived: boolean
    ): Promise<
      AxiosResponse<PaginationResponseModel<PersonnelBoardListCardModel[]>>
    > {
      const _isArchived = isArchived ? 'True' : 'False'

      const url = path("/" + listId + `?isArchived=${_isArchived}`);
      return client.get(url);
    },
    sortAllCards(
      query: string
    ): Promise<
      AxiosResponse<PaginationResponseModel<PersonnelBoardListCardModel[]>>
    > {
      const url = path("/" + query);
      return client.get(url);
    },
    moveAllCards(body: {
      listId: string;
      newListId: string;
    }): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + body.listId + "/move");
      return client.patch(url, { list_id: body.newListId });
    },
    archiveAllCards(
      listId: string
    ): Promise<AxiosResponse<SuccessDetailResponse[]>> {
      const url = path("/" + listId + "/archive");
      return client.patch(url);
    },
  };
};

const cardsOperations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/cards" + dynamicPath;
  };

  return {
    createCard(
      postData: PersonnelBoardNewCardFormModel
    ): Promise<AxiosResponse<PersonnelBoardCardCreateResponseModel>> {
      const url = path();
      return client.post(url, postData);
    },
    fetchCard(
      cardId: string
    ): Promise<AxiosResponse<PersonnelBoardDetailModel>> {
      const url = path("/" + cardId);
      return client.get(url);
    },
    updateCard(
      cardId: string,
      postData: PersonnelBoardFormModel
    ): Promise<AxiosResponse<PersonnelBoardDetailModel>> {
      const url = path("/" + cardId);
      return client.patch(url, postData);
    },
    deleteCard(cardId: string): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + cardId);
      return client.delete(url);
    },
    changeCardPosition(
      cardId: string,
      postData: CardChangePositionRequestModel
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + cardId + "/position");
      return client.patch(url, postData);
    },
    updateCardOrder(
      cardId: string,
      postData: {
        listId: string;
        position: number;
      }
    ): Promise<AxiosResponse<PersonnelBoardDetailModel>> {
      const url = path("/" + cardId + "/position");
      return client.patch(url, postData);
    },
    duplicateCard(
      body: PersonnelBoardCopyCardFormModel
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/copy");
      return client.post(url, body);
    },
  };
};

const checklistsOperations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/checklists" + dynamicPath;
  };

  return {
    fetchChecklists(
      cardId: string
    ): Promise<AxiosResponse<BoardChecklistModel[]>> {
      const url = path("?cardId=" + cardId);
      return client.get(url);
    },
    createChecklist(
      postData: BoardChecklistCreateFormModel
    ): Promise<AxiosResponse<BoardChecklistCreateResponseModel>> {
      const url = path();
      return client.post(url, postData);
    },
    fetchChecklist(
      checklistId: string
    ): Promise<AxiosResponse<BoardChecklistModel>> {
      const url = path("/" + checklistId);
      return client.get(url);
    },
    updateChecklist(
      checklistId: string,
      postData: BoardChecklistUpdateFormModel
    ): Promise<AxiosResponse<BoardChecklistModel>> {
      const url = path("/" + checklistId);
      return client.patch(url, postData);
    },
    deleteChecklist(
      checklistId: string
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + checklistId);
      return client.delete(url);
    },
    fetchChecklistItems(
      checklistId: string
    ): Promise<AxiosResponse<BoardChecklistItemModel[]>> {
      const url = path("/" + checklistId + "/items");
      return client.get(url);
    },
    createChecklistItem(
      checklistId: string,
      postData: BoardChecklistItemCreateFormModel
    ): Promise<AxiosResponse<BoardChecklistItemModel>> {
      const url = path("/" + checklistId + "/items");
      return client.post(url, postData);
    },
    fetchChecklistItem(
      checklistId: string,
      itemId: string
    ): Promise<AxiosResponse<BoardChecklistItemModel>> {
      const url = path("/" + checklistId + "items" + itemId);
      return client.get(url);
    },
    updateChecklistItem(
      checklistId: string,
      itemId: string,
      postData: BoardChecklistItemUpdateFormModel
    ): Promise<AxiosResponse<BoardChecklistItemModel>> {
      const url = path("/" + checklistId + "/items/" + itemId);
      return client.patch(url, postData);
    },
    deleteChecklistItem(
      checklistId: string,
      itemId: string
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + checklistId + "items" + itemId);
      return client.delete(url);
    },
  };
};

const commentsOperations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/cards" + dynamicPath;
  };

  return {
    fetchComments(cardId: string): Promise<AxiosResponse<BoardCommentModel[]>> {
      const url = path(`/${cardId}/comments`);
      return client.get(url);
    },
    createComment(
      postData: BoardCommentCreateFormModel
    ): Promise<AxiosResponse<BoardCommentModel>> {
      const url = path(`/${postData.cardId}/comments`);
      return client.post(url, postData);
    },
    fetchComment(commentId: string): Promise<AxiosResponse<BoardCommentModel>> {
      const url = path("/" + commentId);
      return client.get(url);
    },
    updateComment(
      commentId: string,
      postData: BoardCommentUpdateFormModel
    ): Promise<AxiosResponse<BoardCommentModel>> {
      const url = path(`/${postData.id}/comments/${commentId}`);
      return client.patch(url, postData);
    },
    deleteComment(postData: {
      cardId: string;
      commentId: string;
    }): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path(`/${postData.cardId}/comments/${postData.commentId}`);
      return client.delete(url);
    },
    fetchSubComments(
      commentId: string
    ): Promise<AxiosResponse<BoardCommentModel[]>> {
      const url = path("/" + commentId + "/sub-comments");
      return client.get(url);
    },
  };
};

const contractsOperations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/contracts" + dynamicPath;
  };

  return {
    fetchContracts(
      cardId: string
    ): Promise<AxiosResponse<PersonnelBoardContractModel[]>> {
      const url = path("/?cardId=" + cardId);
      return client.get(url);
    },
    createContract(
      postData: PersonnelBoardContractCreateFormModel
    ): Promise<AxiosResponse<PersonnelBoardContractModel>> {
      const url = path();
      return client.post(url, postData);
    },
    fetchContract(
      contractId: string
    ): Promise<AxiosResponse<PersonnelBoardContractModel>> {
      const url = path("/" + contractId);
      return client.get(url);
    },
    updateContract(
      contractId: string,
      postData: PersonnelBoardContractUpdateFormModel
    ): Promise<AxiosResponse<BoardCommentModel>> {
      const url = path("/" + contractId);
      return client.patch(url, postData);
    },
    deleteContract(
      contractId: string
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/" + contractId);
      return client.delete(url);
    },
    fetchContacts(): Promise<AxiosResponse<ContactListModel[]>> {
      const url = "/contacts";
      return client.get(url);
    },
    fetchOrganizations(): Promise<AxiosResponse<OrganizationListModel[]>> {
      const url = "/organizations";
      return client.get(url);
    },
  };
};

const trainStations = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/train_stations" + dynamicPath;
  };

  return {
    fetchTrainStations(
      name: string
    ): Promise<
      AxiosResponse<PaginationResponseModel<PersonnelBoardTrainStationModel[]>>
    > {
      const url = path("?name=" + name);
      return client.get(url);
    },
  };
};

const skillSheet = (client: AxiosInstance) => {
  const path = (dynamicPath: string = ""): string => {
    return personnelBoardBasePath + "/cards" + dynamicPath;
  };

  return {
    fetchSkillSheet(
      cardId: string
    ): Promise<AxiosResponse<PersonnelBoardSkillSheetModel[]>> {
      const url = path("/" + cardId + "/skill-sheets");
      return client.get(url);
    },
    deleteSkillSheet(
      fileId: string
    ): Promise<AxiosResponse<SuccessDetailResponse>> {
      const url = path("/skill-sheets/" + fileId);
      return client.delete(url);
    },
  };
};

const personnelBoard = (client: AxiosInstance) => {
  return {
    ...listsOperations(client),
    ...cardsOperations(client),
    ...checklistsOperations(client),
    ...commentsOperations(client),
    ...contractsOperations(client),
    ...trainStations(client),
    ...skillSheet(client),
  };
};

export default personnelBoard;
