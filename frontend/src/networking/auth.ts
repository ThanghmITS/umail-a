import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    AuthorizedActionsResponseModel,
    LoginAPIResponseModel,
    LoginFormModel,
} from "~/models/authModel";

const auth = (client: AxiosInstance) => {
    return {
        login(
            postData: LoginFormModel
        ): Promise<AxiosResponse<LoginAPIResponseModel>> {
            const url = "api-token-auth/";
            return client.post(url, postData, {
                baseURL: `${window.location.protocol}//${window.location.host}`,
            });
        },
        logout(): Promise<AxiosResponse<any>> {
            const url = "api-token-auth/logout";
            return client.get(url, {
                baseURL: `${window.location.protocol}//${window.location.host}`,
            });
        },
        fetchAuthorizedActions(): Promise<
            AxiosResponse<AuthorizedActionsResponseModel>
        > {
            const url = Endpoint.authorizedAction;
            return client.get(url);
        },
    };
};

export default auth;
