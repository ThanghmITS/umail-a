import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import { Endpoint } from "~/domain/api";
import {
    DisplaySettingChunkAPIModel,
    UserDisplaySettingAPIModel,
} from "~/models/displaySetting";
import { mockLoginResponseData } from "./authAPIMock";

export const mockDisplaySettingAPIData: DisplaySettingChunkAPIModel = {
    company: uuidv4(),
    content_hash: mockLoginResponseData.userDisplaySetting.content_hash,
    modified_time: "2022-04-01T16:02:11.674059+09:00",
    modified_user__name: "example@example.com",
};

export const mockUserDisplaySettingAPIData: UserDisplaySettingAPIModel = {
    user: uuidv4(),
    content_hash: mockLoginResponseData.userDisplaySetting.content_hash,
};

export const mockDisplaySettingSuccess = rest.get(
    `/app_staffing/${Endpoint.displaySetting}`,
    (req, res, ctx) => {
        const responseData = { ...mockDisplaySettingAPIData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUserDisplaySettingSuccess = rest.get(
    `/app_staffing/${Endpoint.userDisplaySetting}`,
    (req, res, ctx) => {
        const responseData = { ...mockUserDisplaySettingAPIData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const displaySettingAPIMock = [
    mockDisplaySettingSuccess,
    mockUserDisplaySettingSuccess,
];
