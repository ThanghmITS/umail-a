import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import { mockServerPath } from "../utils";

export const mockDeleteThumbnailSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.thumbnail),
    (req, res, ctx) => {
        const responseData = {
            detail: "プロフィール画像が削除されました。",
        };
        return res(ctx.status(204), ctx.json(responseData));
    }
);

export const thumbnailAPIMock = [mockDeleteThumbnailSuccessAPIRoute];
