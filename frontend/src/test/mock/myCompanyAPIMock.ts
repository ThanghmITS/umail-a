import { v4 as uuidv4 } from "uuid";
import { rest } from "msw";
import { MyCompanyModel, MyCompanyResponseModel } from "~/models/myCompany";
import { Endpoint } from "~/domain/api";
import { convertMyCompanyResponseModelToMyCompanyModel } from "~/hooks/useMyCompany";

export const testOrganization1 = {
    id: uuidv4(),
    name: "test1",
};

export const testOrganization2 = {
    id: uuidv4(),
    name: "テスト2",
};

export const mockMyCompanyAPIMockData: MyCompanyResponseModel = {
    address: "example_address",
    building: "example_building",
    capital_man_yen: 2000,
    capital_man_yen_required_for_transactions: 300,
    domain_name: "example.co.jp",
    establishment_date: "2022-01-01",
    establishment_year: 4,
    exceptional_organization_names: [
        testOrganization1.name,
        testOrganization2.name,
    ],
    exceptional_organizations: [testOrganization1.id, testOrganization2.id],
    haken: true,
    has_distribution: true,
    has_haken: true,
    has_invoice_system: true,
    has_p_mark_or_isms: true,
    id: uuidv4(),
    invoice_system: true,
    modified_time: "2022-04-06T14:57:30.233600+09:00",
    modified_user__name: "example@example.com",
    name: "Example",
    p_mark_or_isms: true,
    settlement_month: 0,
};

export const mockMyCompanyMockData: MyCompanyModel =
    convertMyCompanyResponseModelToMyCompanyModel(mockMyCompanyAPIMockData);

export const fetchMyCompanySuccess = rest.get(
    `/app_staffing/${Endpoint.myCompanyPath}`,
    (req, res, ctx) => {
        const responseData = { ...mockMyCompanyAPIMockData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const myCompanyAPIMock = [fetchMyCompanySuccess];
