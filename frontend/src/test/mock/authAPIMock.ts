import { rest } from "msw";
import { LoginAPIResponseModel, LoginResponseModel } from "~/models/authModel";
import { AUTHORIZED_ACTIONS_DEFAULT_VALUE } from "~/utils/constants";
import { generateRandomToken } from "~/utils/utils";

const randomToken = generateRandomToken();
const randomUserId = generateRandomToken();
const randomIntercomUserHash = generateRandomToken();

export const mockLoginAPIResponseData: LoginAPIResponseModel = {
    token: randomToken,
    user_id: randomUserId,
    display_name: "mock user",
    is_user_admin: true,
    role: "guest",
    authorized_actions: AUTHORIZED_ACTIONS_DEFAULT_VALUE,
    intercom_user_hash: randomIntercomUserHash,
    user_display_setting: {
        content_hash: {
            organizations: {
                page_size: 10,
                search: [],
                table: [],
                require: ["name", "category"],
            },
            contacts: {
                page_size: 10,
                search: [],
                table: [],
                require: ["name", "organization", "email_to"],
            },
            shared_emails: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
            scheduled_mails: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
            users: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
        },
    },
};

export const mockLoginResponseData: LoginResponseModel = {
    token: randomToken,
    userId: randomUserId,
    displayName: "mock user",
    isUserAdmin: true,
    role: "guest",
    authorizedActions: AUTHORIZED_ACTIONS_DEFAULT_VALUE,
    intercomUserHash: randomIntercomUserHash,
    userDisplaySetting: {
        content_hash: {
            organizations: {
                page_size: 10,
                search: [],
                table: [],
                require: ["name", "category"],
            },
            contacts: {
                page_size: 10,
                search: [],
                table: [],
                require: ["name", "organization", "email_to"],
            },
            shared_emails: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
            scheduled_mails: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
            users: {
                page_size: 10,
                search: [],
                table: [],
                require: [],
            },
        },
    },
};

export const mockLogin = rest.post("/api-token-auth/", (req, res, ctx) => {
    const responseData = { ...mockLoginAPIResponseData };
    return res(ctx.status(200), ctx.json(responseData));
});

export const mockLoginFailsWithInvalidCredentials = rest.post(
    "/api-token-auth/",
    (req, res, ctx) => {
        const responseData = {
            non_field_errors: ["提供された認証情報でログインできません。"],
        };
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockLogout = rest.get(
    "/api-token-auth/logout",
    (req, res, ctx) => {
        const responseData = { message: "ログアウトしました。" };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockLogoutFailsWithInvalidCredentials = rest.get(
    "/api-token-auth/logout",
    (req, res, ctx) => {
        const responseData = { detail: "不正なトークンです。" };
        return res(ctx.status(401), ctx.json(responseData));
    }
);

export const authAPIMock = [mockLogin, mockLogout];
