import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";
import {
    BoardAssigneeModel,
    BoardChecklistItemModel,
    BoardChecklistModel,
    BoardColumnModel,
    BoardCommentModel,
    BoardDynamicPropertyModel,
    BoardPeriodModel,
    BoardPriorityModel,
    BoardPriorityTitleModel,
    BoardPriorityValueModel,
    BoardScheduledEmailHistoryModel,
    BoardScheduledEmailTemplateModel,
    BoardScheduledEmailTemplateTextModel,
} from "~/models/boardCommonModel";
import {
    PersonnelBoardContractModel,
    PersonnelBoardDetailModel,
    PersonnelBoardListCardModel,
} from "~/models/personnelBoardModel";
import {
    PaginationResponseModel,
    SuccessDetailResponse,
} from "~/models/responseModel";
import { personnelBoardBasePath } from "~/networking/personnelBoard";
import { randString } from "~/utils/utils";
import { mockServerPath } from "../utils";
import _ from "lodash";
import { ScheduledEmailTextFormatValueModel } from "~/models/scheduledEmailModel";

const now = moment();

const path = (dynamicPath: string): string => {
    return mockServerPath("/" + personnelBoardBasePath) + dynamicPath;
};

export const createPersonnelBoardListResponse = (): BoardColumnModel => {
    return {
        id: uuidv4(),
        title: randString(),
    };
};

export const listsResponse: BoardColumnModel[] = [
    createPersonnelBoardListResponse(),
    createPersonnelBoardListResponse(),
];

export const mockFetchListsSuccessAPIRoute = rest.get(
    path("/lists"),
    (req, res, ctx) => {
        const responseData = [...listsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const createAssignee = (): BoardAssigneeModel => {
    return {
        id: uuidv4(),
        name: randString(),
        avatar: "",
    };
};

export const createPriority = (
    title?: BoardPriorityTitleModel,
    value?: BoardPriorityValueModel
): BoardPriorityModel => {
    return {
        title: title ?? "緊急",
        value: value ?? "urgent",
    };
};

export const createPeriod = (
    start?: string,
    end?: string,
    isFinished?: boolean
): BoardPeriodModel => {
    return {
        start: start ?? now.toLocaleString(),
        end: end ?? now.add({ days: 3 }).toLocaleString(),
        isFinished: !!isFinished,
    };
};

export const createChecklistItem = (
    content?: string,
    isFinished?: boolean
): BoardChecklistItemModel => {
    return {
        id: uuidv4(),
        content: content ?? randString(),
        isFinished: !!isFinished,
    };
};

export const createChecklist = (
    title?: string,
    itemCount: number = 2,
    showFinished?: boolean
): BoardChecklistModel => {
    return {
        id: uuidv4(),
        title: title ?? randString(),
        items: _.range(itemCount).map((count) => createChecklistItem()),
        showFinished: !!showFinished,
    };
};

export const createComment = (
    parentId?: string,
    content?: string,
    isImportant?: boolean
): BoardCommentModel => {
    return {
        id: uuidv4(),
        parentId: parentId ?? randString(),
        content: content ?? randString(),
        createdTime: now.toLocaleString(),
        createdUser: uuidv4(),
        modifiedTime: now.toString(),
        modifiedUser: uuidv4(),
        isImportant: !!isImportant,
    };
};

export const createPersonnelBoardListCardResponse = (
    checklistCount: number = 2,
    commentCount: number = 2
): PersonnelBoardListCardModel => {
    return {
        id: uuidv4(),
        order: 1,
        assignees: [createAssignee(), createAssignee()],
        priority: createPriority(),
        lastName: "example",
        firstName: "personnelBoard 1",
        lastNameInitial: "E",
        firstNameInitial: "1",
        age: 25,
        gender: "male",
        trainStation: "東京駅",
        affiliation: "freelance",
        skills: [uuidv4(), uuidv4()],
        period: createPeriod(),
        parallel: "parallel",
        price: 30,
        skillSheet: "",
        checklist: _.range(checklistCount).map((count) => createChecklist()),
        comments: _.range(commentCount).map((count) => createComment()),
        isArchived: false,
        image: '',
        operatePeriod:''
    };
};

export const cardsContent: PersonnelBoardListCardModel[] = _.range(2).map(
    (count) => createPersonnelBoardListCardResponse()
);

export const cardsResponse: PaginationResponseModel<
    PersonnelBoardListCardModel[]
> = {
    next: "",
    previous: "",
    count: cardsContent.length,
    results: cardsContent,
};

export const mockFetchListCardsSuccessAPIRoute = rest.get(
    path("/lists/:listId"),
    (req, res, ctx) => {
        const responseData = { ...cardsResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const cardCreateResponse = {
    id: uuidv4(),
};

export const mockCreateCardSuccessAPIRoute = rest.post(
    path("/cards"),
    (req, res, ctx) => {
        const responseData = { ...cardCreateResponse };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const createDynamicRow = (
    title?: string,
    content?: string,
    withId = false
): BoardDynamicPropertyModel => {
    const property: BoardDynamicPropertyModel = {
        title: title ?? randString(),
        content: content ?? randString(),
    };
    if (withId) {
        property.id = uuidv4();
    }
    return property;
};

export const createContract = (
    detail?: string,
    projectPeriod?: string[],
    higherOrganization?: string,
    higherContact?: string,
    lowerOrganization?: string,
    lowerContact?: string,
    widthId = false
): PersonnelBoardContractModel => {
    const obj: PersonnelBoardContractModel = {
        detail: detail ?? randString(),
        projectPeriod: projectPeriod ?? [
            now.toLocaleString(),
            now.add({ days: 3 }).toLocaleString(),
        ],
        higherOrganization: higherOrganization ?? uuidv4(),
        higherContact: higherContact ?? uuidv4(),
        lowerOrganization: lowerOrganization ?? uuidv4(),
        lowerContact: lowerContact ?? uuidv4(),
    };
    if (widthId) {
        obj.id = uuidv4();
    }
    return obj;
};

export const createScheduledEmailTemplate = (
    subject?: string,
    text?: BoardScheduledEmailTemplateTextModel,
    sendCopyToSender?: boolean,
    sendCopyToShare?: boolean,
    condition?: any
): BoardScheduledEmailTemplateModel => {
    return {
        subject: subject ?? randString(),
        text: text ?? { upper: randString(), lower: randString() },
        sendCopyToSender: !sendCopyToSender,
        sendCopyToShare: !!sendCopyToShare,
        condition: condition ?? {},
    };
};

export const createScheduledEmailHistory = (
    sendCompleteDate?: string,
    format?: ScheduledEmailTextFormatValueModel,
    sendCount?: number,
    openCount?: number
): BoardScheduledEmailHistoryModel => {
    return {
        id: uuidv4(),
        status: "draft",
        sendCompleteDate: sendCompleteDate ?? randString(),
        format: format ?? "text",
        sendCount: sendCount ?? 2,
        openCount: openCount ?? 2,
        openRatio: 0,
    };
};

export const createPersonnelBoardDetailResponse = (
    checklistCount: number = 2,
    commentCount: number = 2
): PersonnelBoardDetailModel => {
    return {
        id: uuidv4(),
        order: 1,
        listId: uuidv4(),
        assignees: [uuidv4(), uuidv4()],
        priority: createPriority(),
        period: createPeriod(
            undefined,
            now.add({ days: 2, months: 1 }).toLocaleString()
        ),
        lastName: "Doe",
        firstName: "John",
        lastNameInitial: "D",
        firstNameInitial: "J",
        age: 25,
        birthday: now.toLocaleString(),
        gender: "male",
        trainStation: "東京駅",
        affiliation: "freelance",
        skills: [uuidv4(), uuidv4()],
        operationPeriod: now.toLocaleString(),
        parallel: "parallel",
        price: 100,
        isNegotiable: false,
        request: "",
        dynamicRow: _.range(2).map((count) => createDynamicRow()),
        image: "",
        skillSheet: "",
        contracts: [],
        scheduledEmailTemplate: createScheduledEmailTemplate(),
        scheduledEmailHistory: _.range(2).map((count) =>
            createScheduledEmailHistory()
        ),
        checklist: _.range(checklistCount).map((count) => createChecklist()),
        comments: _.range(commentCount).map((count) => createComment()),
        isArchived: true,
    };
};

export const cardDetailResponse = createPersonnelBoardDetailResponse();

export const mockFetchCardSuccessAPIRoute = rest.get(
    path("/cards/:cardId"),
    (req, res, ctx) => {
        const responseData = { ...cardDetailResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateCardSuccessAPIRoute = rest.patch(
    path("/cards/:cardId"),
    (req, res, ctx) => {
        const responseData = { ...cardDetailResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockDeleteCardSuccessAPIRoute = rest.delete(
    path("/cards/:cardId"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "削除に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockChangeCardPositionSuccessAPIRoute = rest.patch(
    path("/cards/:cardId/position"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "変更に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const checklistsResponse: BoardChecklistModel[] = _.range(3).map(
    (count) => createChecklist()
);

export const mockFetchChecklistsSuccessAPIRoute = rest.get(
    path("/checklists"),
    (req, res, ctx) => {
        const responseData = [...checklistsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockCreateChecklistSuccessAPIRoute = rest.post(
    path("/checklists"),
    (req, res, ctx) => {
        const responseData = {
            id: uuidv4(),
        };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const checklistResponse: BoardChecklistModel = createChecklist();

export const mockFetchChecklistSuccessAPIRoute = rest.get(
    path("/checklists/:checklistId"),
    (req, res, ctx) => {
        const responseData = { ...checklistResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateChecklistSuccessAPIRoute = rest.patch(
    path("/checklists/:checklistId"),
    (req, res, ctx) => {
        const responseData = { ...checklistResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockDeleteChecklistSuccessAPIRoute = rest.delete(
    path("/checklists/:checklistId"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "削除に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const checklistItemsResponse = _.range(3).map((count) =>
    createChecklistItem()
);

export const mockFetchChecklistItemsSuccessAPIRoute = rest.get(
    path("/checklists/:checklistId/items"),
    (req, res, ctx) => {
        const responseData = [...checklistItemsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const checklistItemResponse = createChecklistItem();

export const mockCreateChecklistItemSuccessAPIRoute = rest.post(
    path("/checklists/:checklistId/items"),
    (req, res, ctx) => {
        const responseData = { ...checklistItemResponse };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockFetchChecklistItemSuccessAPIRoute = rest.get(
    path("/checklists/:checklistId/items/:itemId"),
    (req, res, ctx) => {
        const responseData = { ...checklistItemResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateChecklistItemSuccessAPIRoute = rest.patch(
    path("/checklists/:checklistId/items/:itemId"),
    (req, res, ctx) => {
        const responseData = { ...checklistItemResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockDeleteChecklistItemSuccessAPIRoute = rest.delete(
    path("/checklists/:checklistId/items/:itemId"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "削除に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const commentsResponse: BoardCommentModel[] = _.range(3).map((count) =>
    createComment()
);

export const mockFetchCommentsSuccessAPIRoute = rest.get(
    path("/comments"),
    (req, res, ctx) => {
        const responseData = [...commentsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const commentResponse: BoardCommentModel = createComment();

export const mockCreateCommentSuccessAPIRoute = rest.post(
    path("/comments"),
    (req, res, ctx) => {
        const responseData = { ...commentResponse };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockFetchCommentSuccessAPIRoute = rest.get(
    path("/comments/:commentId"),
    (req, res, ctx) => {
        const responseData = { ...commentResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateCommentSuccessAPIRoute = rest.patch(
    path("/comments/:commentId"),
    (req, res, ctx) => {
        const responseData = { ...commentResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockDeleteCommentSuccessAPIRoute = rest.delete(
    path("/comments/:commentId"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "削除に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const subCommentsResponse: BoardCommentModel[] = _.range(3).map(
    (count) => createComment(uuidv4())
);

export const mockFetchSubCommentsSuccessAPIRoute = rest.get(
    path("/comments/:commentId/sub-comments"),
    (req, res, ctx) => {
        const responseData = [...subCommentsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const contractsResponse: PersonnelBoardContractModel[] = _.range(3).map(
    (count) => createContract()
);

export const mockFetchContractsSuccessAPIRoute = rest.get(
    path("/contracts"),
    (req, res, ctx) => {
        const responseData = [...contractsResponse];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const contractResponse: PersonnelBoardContractModel = createContract();

export const mockCreateContractSuccessAPIRoute = rest.post(
    path("/contracts"),
    (req, res, ctx) => {
        const responseData = { ...contractResponse };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockFetchContractSuccessAPIRoute = rest.get(
    path("/contracts/:contractId"),
    (req, res, ctx) => {
        const responseData = { ...contractResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateContractSuccessAPIRoute = rest.patch(
    path("/contracts/:contractId"),
    (req, res, ctx) => {
        const responseData = { ...contractResponse };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockDeleteContractSuccessAPIRoute = rest.delete(
    path("/contracts/:contractId"),
    (req, res, ctx) => {
        const responseData: SuccessDetailResponse = {
            detail: "削除に成功しました", // TODO(joshua-hashimoto): 正しいものに修正する
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const personnelBoardAPIMock = [
    mockFetchListsSuccessAPIRoute,
    mockFetchListCardsSuccessAPIRoute,
    mockCreateCardSuccessAPIRoute,
    mockFetchCardSuccessAPIRoute,
    mockUpdateCardSuccessAPIRoute,
    mockDeleteCardSuccessAPIRoute,
    mockChangeCardPositionSuccessAPIRoute,
    mockFetchChecklistsSuccessAPIRoute,
    mockCreateChecklistSuccessAPIRoute,
    mockFetchChecklistSuccessAPIRoute,
    mockUpdateChecklistSuccessAPIRoute,
    mockDeleteChecklistSuccessAPIRoute,
    mockFetchChecklistItemsSuccessAPIRoute,
    mockCreateChecklistItemSuccessAPIRoute,
    mockFetchChecklistItemSuccessAPIRoute,
    mockUpdateChecklistItemSuccessAPIRoute,
    mockDeleteChecklistItemSuccessAPIRoute,
    mockFetchCommentsSuccessAPIRoute,
    mockCreateCommentSuccessAPIRoute,
    mockFetchCommentSuccessAPIRoute,
    mockUpdateCommentSuccessAPIRoute,
    mockDeleteCommentSuccessAPIRoute,
    mockFetchSubCommentsSuccessAPIRoute,
    mockFetchContractsSuccessAPIRoute,
    mockCreateContractSuccessAPIRoute,
    mockFetchContractSuccessAPIRoute,
    mockUpdateContractSuccessAPIRoute,
    mockDeleteContractSuccessAPIRoute,
];
