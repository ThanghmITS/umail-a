import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import { mockServerPath } from "../utils";
import { PasswordResetResponseModel } from "~/models/authModel";

export const mockDeleteAccountSuccessAPIRoute = rest.delete(
    mockServerPath("/" + Endpoint.account),
    (req, res, ctx) => {
        return res(ctx.status(204));
    }
);

export const mockDeleteAccountFailsWithBadRequest = rest.delete(
    mockServerPath("/" + Endpoint.account),
    (req, res, ctx) => {
        return res(ctx.status(400));
    }
);

export const mockPasswordResetSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.passwordResetMail),
    (req, res, ctx) => {
        const responseData: PasswordResetResponseModel = {
            result: "ok",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPasswordResetFailureAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.passwordResetMail),
    (req, res, ctx) => {
        const responseData: PasswordResetResponseModel = {
            result: "AccountDoesNotExist",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const accountAPIMock = [
    mockDeleteAccountSuccessAPIRoute,
    mockPasswordResetSuccessAPIRoute,
];
