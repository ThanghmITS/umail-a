import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import { convertMyProfileResponseModelToMyProfileModel } from "~/hooks/useMyProfile";
import {
    MyProfileModel,
    MyProfileResponseModel,
} from "~/models/myProfileModel";
import { mockServerPath } from "../utils";

export const myProfileResponseData: MyProfileResponseModel = {
    avatar: "https://picsum.photos/200/300",
    email: "example@example.com",
    email_signature: " email\nsignature",
    first_name: "user",
    last_name: "example",
    user_service_id: "user_0000000001",
    modified_time: "2022-04-12T17:44:01.895951+09:00",
    modified_user__name: "user example",
    role: "master",
    tel1: "001",
    tel2: "112",
    tel3: "223",
};

export const myProfileModelData: MyProfileModel =
    convertMyProfileResponseModelToMyProfileModel(myProfileResponseData);

export const mockFetchMyProfileSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.myProfilePath),
    (req, res, ctx) => {
        const responseData = { ...myProfileResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateMyProfileSuccessAPIRoute = rest.patch(
    mockServerPath("/" + Endpoint.myProfilePath),
    (req, res, ctx) => {
        const responseData = { ...myProfileResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const myProfileAPIMock = [
    mockFetchMyProfileSuccessAPIRoute,
    mockUpdateMyProfileSuccessAPIRoute,
];
