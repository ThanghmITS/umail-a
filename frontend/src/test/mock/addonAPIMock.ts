import _ from "lodash";
import moment from "moment";
import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import { Endpoint } from "~/domain/api";
import {
    AddonMasterResponseModel,
    AddonPurchaseItemResponseModel,
} from "~/models/addonModel";
import { randString } from "~/utils/utils";
import { mockServerPath } from "../utils";

export const createAddonMasterResponse = (
    id: number = 1
): AddonMasterResponseModel => {
    const obj: AddonMasterResponseModel = {
        id,
        description: randString() + randString() + randString(),
        expiration_time: moment().toLocaleString(),
        is_dashboard: true,
        is_my_company_setting: true,
        is_organizations: true,
        is_recommended: true,
        is_scheduled_mails: true,
        is_shared_mails: true,
        limit: 10,
        parents: ["parent1"],
        price: 2500,
        targets: ["target1"],
        title: randString(),
        help_url: "https://google.com",
    };
    return obj;
};

export const addonFetchAddonMasterResponseData: AddonMasterResponseModel[] =
    _.range(1, 4).map((val, index) => createAddonMasterResponse(val));

export const mockAddonFetchAddonMasterSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.addonMaster),
    (req, res, ctx) => {
        const responseData = [...addonFetchAddonMasterResponseData];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockAddonFetchAddonMasterFailureAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.addonMaster),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const createAddonPurchaseItemResponse = (
    id: number = 1
): AddonPurchaseItemResponseModel => {
    const obj: AddonPurchaseItemResponseModel = {
        id,
        addon_master_id: id,
        next_payment_date: moment().toLocaleString(),
    };
    return obj;
};

export const addonFetchPurchaseItemResponseData: AddonPurchaseItemResponseModel[] =
    _.range(1, 4).map((val, index) => createAddonPurchaseItemResponse(val));

export const mockAddonFetchAddonPurchasedAddonSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.purchasedAddons),
    (req, res, ctx) => {
        const responseData = [...addonFetchPurchaseItemResponseData];
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockAddonFetchAddonPurchasedAddonFailureAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.purchasedAddons),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockAddonPurchaseAddonSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.purchaseAddon),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockAddonPurchaseAddonFailureAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.purchaseAddon),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockAddonRevokeAddonSuccessAPIRoute = rest.delete(
    mockServerPath("/" + Endpoint.revokePurchasedAddon + "/:id"),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockAddonRevokeAddonFailureAPIRoute = rest.delete(
    mockServerPath("/" + Endpoint.revokePurchasedAddon + "/:id"),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const addonAPIMock = [
    mockAddonFetchAddonMasterSuccessAPIRoute,
    mockAddonFetchAddonPurchasedAddonSuccessAPIRoute,
    mockAddonPurchaseAddonSuccessAPIRoute,
    mockAddonRevokeAddonSuccessAPIRoute,
];
