import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import { Endpoint } from "~/domain/api";
import { PaginationResponseModel } from "~/models/responseModel";
import { TagFormModel, TagResponseModel } from "~/models/tagModel";

export const tagResponseModelList: TagResponseModel[] = [
    {
        id: "375b2cab-553d-4feb-9657-7da94909cc3d",
        value: "react",
        color: "default",
        internal_value: "react",
        created_time: "2022-03-04T18:46:34.902823+09:00",
        created_user: "10160cbf-0bf3-49e8-9642-b50bb635781e",
        created_user__name: "master@example.com",
        modified_time: "2022-03-04T18:46:34.902862+09:00",
        modified_user: uuidv4(),
        modified_user__name: "master_edit@example.com",
    },
    {
        id: "dbbd0f35-228d-4baf-a66d-75b93dadd34a",
        value: "django",
        color: "volcano",
        internal_value: "django",
        created_time: "2022-03-04T18:43:14.437171+09:00",
        created_user: "10160cbf-0bf3-49e8-9642-b50bb635781e",
        created_user__name: "master@example.com",
        modified_time: "2022-03-04T18:43:14.437213+09:00",
        modified_user: uuidv4(),
        modified_user__name: "master_edit@example.com",
    },
];

export const mockTagListPaginationResponseData: PaginationResponseModel<
    TagResponseModel[]
> = {
    count: tagResponseModelList.length,
    next: "",
    previous: "",
    results: tagResponseModelList,
};

export const mockTagCreateResponseData: TagFormModel = {
    value: "example tag",
    color: "default",
};

export const mockTagEditResponseData: TagFormModel = {
    id: uuidv4(),
    value: "example tag edited",
    color: "cyan",
};

export const mockFetchTagsSuccessAPIRoute = rest.get(
    `/app_staffing/${Endpoint.tags}`,
    (req, res, ctx) => {
        let responseData = { ...mockTagListPaginationResponseData };
        const value = req.url.searchParams.get("value");
        if (value) {
            const filteredResults = tagResponseModelList.filter((mockData) =>
                mockData.value.includes(value)
            );
            responseData.results = filteredResults;
            responseData.count = filteredResults.length;
        }
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockFetchTagSuccessAPIRoute = rest.get(
    `/app_staffing/${Endpoint.tags}/:id`,
    (req, res, ctx) => {
        const data = tagResponseModelList[0];
        const responseData = { ...data };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockCreateTagSuccessAPIRoute = rest.post(
    `/app_staffing/${Endpoint.tags}`,
    (req, res, ctx) => {
        const responseData = { ...mockTagCreateResponseData };
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const mockEditTagSuccessAPIRoute = rest.patch(
    `/app_staffing/${Endpoint.tags}/:id`,
    (req, res, ctx) => {
        const responseData = { ...mockTagEditResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockCreateTagFailsWithDuplicateNameAPIRoute = rest.post(
    `/app_staffing/${Endpoint.tags}`,
    (req, res, ctx) => {
        const responseData = {
            detail: "入力したタグ名は、既に登録されています。",
        };
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const tagAPIMock = [
    mockFetchTagsSuccessAPIRoute,
    mockFetchTagSuccessAPIRoute,
    mockEditTagSuccessAPIRoute,
    mockCreateTagSuccessAPIRoute,
];
