import { accountAPIMock } from "./accountAPIMock";
import { addonAPIMock } from "./addonAPIMock";
import { authAPIMock } from "./authAPIMock";
import { contactAPIMock } from "./contactAPIMock";
import { displaySettingAPIMock } from "./displaySettingAPIMock";
import { myCompanyAPIMock } from "./myCompanyAPIMock";
import { myProfileAPIMock } from "./myProfileAPIMock";
import { personnelBoardAPIMock } from "./personnelBoardAPIMock";
import { planAPIMock } from "./planAPIMock";
import { projectBoardAPIMock } from "./projectBoardAPIMock";
import { purchaseHistoryAPIMock } from "./purchaseHistoryMock";
import { scheduledEmailAPIMock } from "./scheduledEmailMock";
import { scheduledEmailSettingAPIMock } from "./scheduledEmailSettingMock";
import { searchTemplateAPIMock } from "./searchTemplateAPIMock";
import { sharedEmailAPIMock } from "./sharedEmailAPIMock";
import { tagAPIMock } from "./tagAPIMock";
import { tenantAPIMock } from "./tenantAPIMock";
import { thumbnailAPIMock } from "./thumbnailAPIMock";
import { userAPIMock } from "./userAPIMock";

export const mockHandlers = [
    ...accountAPIMock,
    ...addonAPIMock,
    ...authAPIMock,
    ...contactAPIMock,
    ...displaySettingAPIMock,
    ...myCompanyAPIMock,
    ...myProfileAPIMock,
    ...personnelBoardAPIMock,
    ...planAPIMock,
    ...projectBoardAPIMock,
    ...purchaseHistoryAPIMock,
    ...scheduledEmailAPIMock,
    ...scheduledEmailSettingAPIMock,
    ...searchTemplateAPIMock,
    ...sharedEmailAPIMock,
    ...tagAPIMock,
    ...tenantAPIMock,
    ...thumbnailAPIMock,
    ...userAPIMock,
];
