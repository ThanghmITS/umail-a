export type DisplayTextAndForeignKeyModel = {
    displayText: string;
    foreignKey: string;
};

export type AjaxItemModel = {
  keyNumber?: number;
  value: string;
};

export enum TypeAjaxPageModel {
  MY_COMPANY_ORGANIZATION = 'my_company_organization'
};

