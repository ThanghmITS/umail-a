export type PlanRegisterPostAPIModel = {
    plan_master_id: number;
};

export type PlanRegisterPostModel = {
    planMasterId: number;
};

export type PlanUserDataAPIModel = {
    id: string;
    plan_master_id: number;
    user_registration_limit: number;
    expiration_date: string;
    default_user_count: number;
    current_user_count: number;
};

export type PlanUserDataModel = {
    id: string;
    planMasterId: number;
    userRegistrationLimit: number;
    expirationDate: string;
    defaultUserCount: number;
    currentUserCount: number;
};

export type PlanFreeTrialPlanIdModel = 0;

export type PlanPayedPlanIdModel = 1 | 2 | 3;

export type PlanIdModel = PlanFreeTrialPlanIdModel | PlanPayedPlanIdModel;

export type PlanFreeTrialPlanValueModel = "無料トライアル";

export type PlanPayedPlanValueModel =
    | "ライト"
    | "スタンダード"
    | "プロフェッショナル";

export type PlanTitleValueModel =
    | PlanFreeTrialPlanValueModel
    | PlanPayedPlanValueModel;

export type PlanSummaryResponseModel = {
    plan_id: PlanIdModel;
    plan_name: PlanTitleValueModel;
    remaining_days: string;
    payment_error_exists: boolean;
    payment_error_card_ids: string[];
};

export type PlanSummaryModel = {
    planId: PlanIdModel;
    planName: PlanTitleValueModel;
    remainingDays: string;
    paymentErrorExists: boolean;
    paymentErrorCardIds: string[];
};

export type PlanCardModel = {
    planId: number;
    planTitle: PlanPayedPlanValueModel;
    defaultCount: number;
    monthlyPrice: number;
    planSummary: string[];
    aimedTo: string[];
    functions: string[];
};

export type PlanTableYesNoValueModel = "⚪︎" | "-";

export type PlanTableRowModel = {
    functionName: string;
    isLightPlanAvailable: PlanTableYesNoValueModel;
    isStandardPlanAvailable: PlanTableYesNoValueModel;
    isProfessionalPlanAvailable: PlanTableYesNoValueModel;
};
