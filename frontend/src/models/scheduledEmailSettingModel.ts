export type ScheduledEmailSettingRequestModel = {
    hostname: string;
    password: string;
    port_number: number;
    connection_type: number;
    username: string;
    file_type: number;
};

export type ScheduledEmailSettingResponseModel = {
    file_type: number;
    company: string;
    hostname: string;
    is_open_count_available: boolean;
    is_open_count_extra_period_available: boolean;
    is_use_attachment_max_size_available: boolean;
    is_use_delivery_interval_available: boolean;
    is_use_remove_promotion_available: boolean;
    modified_time: string;
    modified_user: string;
    modified_user__name: string;
    password: string;
    port_number: number;
    target_count_addon_purchase_count: 0 | 1 | 2 | 3 | 4;
    use_attachment_max_size: boolean;
    use_open_count: boolean;
    use_open_count_extra_period: boolean;
    use_remove_promotion: boolean;
    connection_type: number;
    username: string;
};

export type ScheduledEmailSettingBaseSettingFormModel = {
    hostname: string;
    password: string;
    port_number: number;
    connection_type: number;
    username: string;
};

export type ScheduledEmailSettingDetailSettingFormModel = {
    file_type: number;
    is_open_count_available: boolean;
    is_open_count_extra_period_available: boolean;
    is_use_attachment_max_size_available: boolean;
    is_use_delivery_interval_available: boolean;
    is_use_remove_promotion_available: boolean;
    target_count_addon_purchase_count: 0 | 1 | 2 | 3 | 4;
    use_attachment_max_size: boolean;
    use_open_count: boolean;
    use_open_count_extra_period: boolean;
    use_remove_promotion: boolean;
};

export type ScheduledEmailSettingModel =
    ScheduledEmailSettingBaseSettingFormModel &
        ScheduledEmailSettingDetailSettingFormModel & {
            company: string;
            modified_time: string;
            modified_user: string;
        };
