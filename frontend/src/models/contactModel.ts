import { Override, TAG_MODEL } from "~/utils/types";
import { TableCommentModel } from "./commentModel";
import { TagContactTableModel } from "./tagModel";

export type ContactSendTypeModel = Override<
    TAG_MODEL,
    {
        value: "job" | "personnel";
    }
>;

export type ContactWantesLocationModel = Override<
    TAG_MODEL,
    {
        value:
            | "wants_location_hokkaido_japan"
            | "wants_location_touhoku_japan"
            | "wants_location_kanto_japan"
            | "wants_location_kansai_japan"
            | "wants_location_chubu_japan"
            | "wants_location_kyushu_japan"
            | "wants_location_other_japan"
            | "wants_location_chugoku_japan"
            | "wants_location_shikoku_japan"
            | "wants_location_toukai_japan"
            | "not:wants_location_hokkaido_japan"
            | "not:wants_location_touhoku_japan"
            | "not:wants_location_kanto_japan"
            | "not:wants_location_kansai_japan"
            | "not:wants_location_chubu_japan"
            | "not:wants_location_kyushu_japan"
            | "not:wants_location_other_japan"
            | "not:wants_location_chugoku_japan"
            | "not:wants_location_shikoku_japan"
            | "not:wants_location_toukai_japan";
    }
>;

export type ContactSearchTemplateContentResponseModel = {
    ignore_filter?: boolean;
    ignore_blocklist_filter?: boolean;
    last_name?: string;
    first_name?: string;
    organization__name?: string;
    email?: string;
    cc_mails?: string;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    position?: string;
    department?: string;
    staff?: string[];
    date_range?: Date[];
    tags__suffix?: string;
    category_inequality?: string;
    contact_preference?: string[];
    wants_location?: string[];
    comment_user?: string[];
    created_user?: string[];
    modified_user?: string[];
    tags?: string[];
    category?: string;
};

export type ContactSearchTemplateContentRequestModel =
    ContactSearchTemplateContentResponseModel;

export type ContactPreferenceResponseModel = {
    wants_location_hokkaido_japan: boolean;
    wants_location_touhoku_japan: boolean;
    wants_location_kanto_japan: boolean;
    wants_location_chubu_japan: boolean;
    wants_location_toukai_japan: boolean;
    wants_location_kansai_japan: boolean;
    wants_location_shikoku_japan: boolean;
    wants_location_chugoku_japan: boolean;
    wants_location_kyushu_japan: boolean;
    wants_location_other_japan: boolean;
};

export type ContactPreferenceModel = ContactPreferenceResponseModel;

export type ContactListResponseModel = {
    id: string;
    display_name: string;
    email: string;
    cc_mails: string[];
    position?: string;
    department?: string;
    organization__name: string;
    staff__name: string;
    last_visit?: string;
    created_time: string;
    modified_time: string;
    comments: TableCommentModel[];
    tel1: string;
    tel2: string;
    tel3: string;
    tags: string[];
    tag_objects: TagContactTableModel[];
    contactpreference: ContactPreferenceResponseModel;
    category?: "heart" | "frown";
    is_ignored: boolean;
    contactjobtypepreferences: string[];
    contactpersonneltypepreferences: string[];
};

export type ContactListModel = {
    id: string;
    display_name: string;
    email: string;
    cc_mails?: string[];
    cc_addresses__email?: string;
    position?: string;
    department?: string;
    organization?: string;
    organization__name: string;
    staff__name?: string;
    staff__last_name?: string;
    last_visit?: string;
    created_time: string;
    modified_time: string;
    comments: TableCommentModel[];
    tel1?: string;
    tel2?: string;
    tel3?: string;
    contactjobtypepreferences: string[];
    contactpersonneltypepreferences: string[];
    contactpreference: ContactPreferenceModel;
    tags: string[];
    tag_objects: TagContactTableModel[];
    category?: "heart" | "frown";
    is_ignored?: boolean;
};
