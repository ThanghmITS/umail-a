export type OrganizationStatusModel = {
    value:
        | "prospective"
        | "approached"
        | "exchanged"
        | "client"
        | "blacklist"
        | "not:prospective"
        | "not:approached"
        | "not:exchanged"
        | "not:client"
        | "not:blacklist";
    title: string;
    color: string;
    search: boolean;
};

export type OrganizationSearchTemplateContentResponseModel = {
    ignore_filter?: boolean;
    ignore_blocklist_filter?: boolean;
    category?: string[];
    score?: number;
    country?: string[];
    employee_number?: string[];
    comment_user?: string[];
    created_user?: string[];
    modified_user?: string[];
    score_inequality?: string;
    corporate_number?: string;
    name?: string;
    establishment_date_range?: string[];
    settlement_month?: number[];
    address?: string;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    fax1?: string;
    fax2?: string;
    fax3?: string;
    domain_name?: string;
    has_distribution?: boolean;
    contract?: boolean;
    capital_gt?: string;
    capital_lt?: string;
    capital?: string;
    license?: string[];
    branch_name?: string;
    branch_address?: string;
    branch_tel1?: string;
    branch_tel2?: string;
    branch_tel3?: string;
    branch_fax1?: string;
    branch_fax2?: string;
    branch_fax3?: string;
    establishment_year_gt?: string;
    establishment_year_lt?: string;
    establishment_year?: string;
    capital_man_yen_required_for_transactions_gt?: string;
    capital_man_yen_required_for_transactions_lt?: string;
    capital_man_yen_required_for_transactions?: string;
    license_required_for_transactions?: string[];
};

export type OrganizationSearchTemplateContentRequestModel =
    OrganizationSearchTemplateContentResponseModel;

export type OrganizationListModel = {
    id?: string;
    address?: string;
    building?: string;
    branches?: string[];
    contract?: string;
    corporate_number?: string;
};