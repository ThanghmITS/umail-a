export type SystemNotificationModel = {
    id: number;
    title: string;
    order: number;
    release_time: string;
    is_checked: boolean;
    url: string;
};
