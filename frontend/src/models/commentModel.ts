export type TableCommentModel = {
    content: string;
    created_time: string;
    created_user__name: string;
    modified_time: string;
    modified_user__name: string;
};
export type CommentThreadModel = {
    parent_content: string;
    id: string;
    parent: string;
    is_important: boolean;
}

export type CommentThreadChildModel = {
    parent_content: string;
    id: string;
    parent: string;
    parent_created_user_name: string,
    parent_created_time: string
}
