import { ReactNode } from "react";

export type HeaderIconPropsModel = {
    title: ReactNode | undefined;
    content: ReactNode;
    icon: ReactNode;
};
