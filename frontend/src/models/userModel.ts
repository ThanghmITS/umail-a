import { Moment } from "moment";
import { RoleModel } from "./authModel";

export type UserSpecificModel = {
    is_active: boolean;
};

export type UserRegisterFormModel = UserSpecificModel & {
    avatar?: string | File;
    last_name: string;
    first_name: string;
    email: string;
    email_signature?: string;
    user_service_id: string;
    role?: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    password?: string;
};

export type UserFormModel = UserSpecificModel & {
    avatar?: string | File;
    last_name: string;
    first_name: string;
    email: string;
    email_signature?: string;
    user_service_id: string;
    role?: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    password?: string;
    pk: string;
};

export type UserRegisterRequestDataModel = UserRegisterFormModel & {
    avatar?: File;
};

export type UserRegisterRequestModel = {
    registerToken: string;
    data: UserRegisterRequestDataModel;
};

export type UserUpdateRequestDataModel = UserFormModel & {
    avatar?: File;
    pk: string;
};

export type UserUpdateRequestModel = {
    userId: string;
    data: UserUpdateRequestDataModel;
};

export type UserResponseModel = UserSpecificModel & {
    id?: string;
    username?: string;
    email: string;
    email_signature?: string;
    first_name: string;
    last_name: string;
    modified_time: string;
    modified_user__name: string;
    name?: string;
    password?: string;
    role?: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type UserModel = UserSpecificModel & {
    id?: string;
    avatar?: string;
    username?: string;
    email: string;
    email_signature?: string;
    first_name: string;
    last_name: string;
    user_service_id: string;
    modified_time: string;
    modified_user: string;
    name?: string;
    password?: string;
    role?: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type UserFormFieldErrorResponseModel = UserSpecificModel & {
    detail?: string;
    id?: string;
    avatar?: string;
    username?: string;
    email: string;
    email_signature?: string;
    first_name?: string;
    last_name?: string;
    user_service_id?: string;
    modified_time?: string;
    modified_user?: string;
    name?: string;
    password?: string;
    role?: RoleModel;
    tel?: string;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type UserSearchFormModel = {
    display_name: string;
    email: string;
    role?: string;
    last_login: Moment[];
    first_name: string;
    last_name: string;
    tel1: string;
    tel2: string;
    tel3: string;
    inactive_filter: boolean;
};

export type UserListItemAPIModel = {
    display_name: string;
    editable: boolean;
    email: string;
    email_signature?: string;
    first_name: string;
    id: string;
    is_active: boolean;
    is_user_admin: boolean;
    last_login: string;
    last_name: string;
    modified_time: string;
    modified_user__name: string;
    old_id?: string | null;
    registed_at?: string | null;
    role: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    username: string;
    user_service_id: string;
};

export type UserListItemModel = {
    id: string;
    display_name: string;
    email: string;
    last_login: string;
    is_active: boolean;
    date_joined?: string;
    first_name: string;
    last_name: string;
    role: RoleModel;
    tel?: string;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    user_service_id: string;
};
