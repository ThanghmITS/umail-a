type ContactStatsStreamItemModel = {
    development: number[];
    infrastructure: number[];
    others: number[];
};

export type ContactStatsGraphModel = {
    jobs: ContactStatsStreamItemModel;
    personnel: ContactStatsStreamItemModel;
    announcement: number[];
};

export type OrganizationStatsGraphModel = {
    prospective: number[];
    approached: number[];
    exchanged: number[];
    client: number[];
    blocklist: number[];
};

export type ScheduledEmailStatsGraphModel = {
    jobs: number[];
    personnel: number[];
    announcement: number[];
};

export type StaffMyCompanyGraphModel = {
    jobs: number[];
    personnel: number[];
    announcement: number[];
};
