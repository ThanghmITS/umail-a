export type CommentTemplateModel = {
    title: string;
    content: string;
};
