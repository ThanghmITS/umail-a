export type SharedEmailCategoryValueModel = "job" | "person" | "other";

export type SharedEmailAttachmentModel = {
    id: string;
    name: string;
};

export type SharedEmailFormatValueModel = "html" | "text";

export type SharedEmailResponseModel = {
    id: string;
    message_id: string;
    attachments: SharedEmailAttachmentModel[];
    category?: SharedEmailCategoryValueModel;
    estimated_category?: SharedEmailCategoryValueModel;
    from_address: string;
    from_name: string;
    html: string;
    sent_date: string;
    shared: boolean;
    staff_in_charge__id: string;
    staff_in_charge__name: string;
    subject: string;
    text: string;
};

export type SharedEmailModel = {
    id: string;
    messageId: string;
    attachments: SharedEmailAttachmentModel[];
    category?: SharedEmailCategoryValueModel;
    estimatedCategory?: SharedEmailCategoryValueModel;
    fromAddress: string;
    fromName: string;
    html: string;
    sentDate: string;
    shared: boolean;
    staffInChargeId: string;
    staffInChargeName: string;
    subject: string;
    text: string;
    format: SharedEmailFormatValueModel;
};

export type SharedEmailFormModel = {
    staffInChargeId: string;
};

export type SharedEmailRequestModel = {
    staff_in_charge_id: string;
};
