import { RoleModel } from "./authModel";

export type MyProfileFormModel = {
    avatar?: string | File;
    lastName: string;
    firstName: string;
    userServiceId: string;
    email: string;
    emailSignature?: string;
    role: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    password?: string;
    pk?: string;
};

export type MyProfileFormFieldErrorResponseModel = {
    detail?: string;
    avatar?: string;
    email?: string;
    email_signature?: string;
    first_name?: string;
    last_name?: string;
    user_service_id?: string;
    name?: string;
    password?: string;
    role?: string;
    tel?: string;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type MyProfileResponseModel = {
    avatar: string;
    email: string;
    email_signature?: string;
    first_name: string;
    last_name: string;
    user_service_id: string;
    modified_time: string;
    modified_user__name: string;
    name?: string;
    password?: string;
    role: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type MyProfileModel = {
    avatar?: string;
    email: string;
    emailSignature?: string;
    firstName: string;
    lastName: string;
    userServiceId: string;
    modifiedTime: string;
    modifiedUser: string;
    name?: string;
    password?: string;
    role: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
};

export type MyProfileRequestModel = {
    avatar?: File;
    last_name: string;
    first_name: string;
    user_service_id: string;
    email: string;
    email_signature?: string;
    role: RoleModel;
    tel1?: string;
    tel2?: string;
    tel3?: string;
    password?: string;
};
