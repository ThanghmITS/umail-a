import { Override } from "~/utils/types";
import { TransferModel } from "./transferModel";

type DisplaySettingItemModel = {
    page_size: number;
    search: TransferModel[];
    table: TransferModel[];
};

export type DisplaySettingModel = {
    organizations: Override<
        DisplaySettingItemModel,
        { require: TransferModel[] }
    >;
    contacts: Override<DisplaySettingItemModel, { require: TransferModel[] }>;
    shared_emails: DisplaySettingItemModel;
    scheduled_mails: DisplaySettingItemModel;
    users: DisplaySettingItemModel;
};

export type DisplaySettingFieldTypes =
    | "contacts"
    | "organizations"
    | "scheduled_mails"
    | "shared_emails"
    | "users";

export type DisplaySettingAPIModel = {
    organizations: {
        page_size: number;
        search: string[];
        table: string[];
        require: string[];
    };
    contacts: {
        page_size: number;
        search: string[];
        table: string[];
        require: string[];
    };
    shared_emails: {
        page_size: number;
        search: string[];
        table: string[];
        require?: string[];
    };
    scheduled_mails: {
        page_size: number;
        search: string[];
        table: string[];
        require?: string[];
    };
    users: {
        page_size: number;
        search: string[];
        table: string[];
        require?: string[];
    };
};

export type DisplaySettingChunkAPIModel = {
    company: string;
    content_hash: DisplaySettingAPIModel;
    modified_time: string;
    modified_user__name: string;
};

export type DisplaySettingChunkModel = {
    company: string;
    content_hash: DisplaySettingAPIModel;
    modified_time: string;
    modified_user: string;
}

export type UserDisplaySettingAPIModel = {
    content_hash: DisplaySettingAPIModel;
    user: string;
};
