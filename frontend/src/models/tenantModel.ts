import { Moment } from "moment";
import { PaymentCreateModel } from "./paymentModel";

export type TokenPostModel = { authToken: string };

export type TenantRecaptchaResponseModel = {
    sitekey: string;
    test_mode?: boolean;
};

export type TenantRecaptchaModel = {
    sitekey: string;
    testMode?: boolean;
};

export type TenantRegisterBasicInfoModel = {
    email: string;
    password: string;
    password_confirm: string;
};

export type TenantMyCompanyResponseModel = {
    name: string;
    establishment_date: string;
    address: string;
    building: string;
    domain_name: string;
    capital_man_yen: number;
    has_distribution: boolean;
    has_invoice_system: boolean;
    has_p_mark_or_isms: boolean;
    has_haken: boolean;
};

export type TenantMyCompanyRequestModel = TokenPostModel &
    TenantMyCompanyResponseModel;

export type TenantMyCompanyFormModel = {
    name: string;
    establishment_date: Moment;
    address: string;
    building: string;
    domain_name: string;
    capital_man_yen: number;
    has_distribution: boolean;
    has_invoice_system: boolean;
    has_p_mark_or_isms: boolean;
    has_haken: boolean;
    // settlement_month: number;
};

export type TenantMyProfileModel = {
    avatar?: string;
    last_name: string;
    first_name: string;
    email: string;
    role: string;
    tel1: string;
    tel2: string;
    tel3: string;
    password?: string;
    email_signature: string;
};

export type TenantMyProfileFormModel = {
    avatar?: string | File;
    last_name: string;
    first_name: string;
    email: string;
    role: string;
    tel1: string;
    tel2: string;
    tel3: string;
    password: string;
    email_signature: string;
    user_service_id: string;
};

export type TenantMyProfileFormFieldErrorResponseModel = {
  detail?: string;
  avatar?: string;
  last_name?: string;
  first_name?: string;
  email?: string;
  role?: string;
  tel?: string;
  password?: string;
  email_signature?: string;
  user_service_id?: string;
};

export type TenantMyProfileRequestDataModel = {
    detail?: string;
    avatar?: File;
    last_name: string;
    first_name: string;
    user_service_id: string;
    email: string;
    role: string;
    tel1: string;
    tel2: string;
    tel3: string;
    password: string;
    email_signature: string;
};

export type TenantMyProfileRequestModel = TokenPostModel &
    TenantMyProfileRequestDataModel;

export type TenantPaymentRequestModel = TokenPostModel & PaymentCreateModel;

export type TenantCurrentStepModel = {
    current_step: number;
    is_completed: boolean;
    is_registered?: boolean;
};
