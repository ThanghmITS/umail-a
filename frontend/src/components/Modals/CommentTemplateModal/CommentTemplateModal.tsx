import React, { useState } from "react";
import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Col, Divider, List, Modal, Row, Tooltip } from "antd";
import CommentTemplateListItem from "~/components/DataDisplay/CommentList/CommentTemplateListItem/CommentTemplateListItem";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { useReduxState } from "~/hooks/useReduxState";
import { ErrorMessages } from "~/utils/constants";
import styles from "./CommentTemplateModal.scss";
import { CommentTemplateModel } from "~/models/commentTemplateModel";
import LimitDivider from "~/components/Common/LimitDivider/LimitDivider";

type REDUCER_ID =
    | "organizationRegisterPage"
    | "organizationEditPageComments"
    | "contactRegisterPage"
    | "contactEditPageComments"
    | "sharedEmailDetailPageComments";

type Props = {
    reducerId: REDUCER_ID;
    isModalVisible: boolean;
    onModalClose: () => void;
    onCreateTemplate: () => void;
    onInsertTemplate: (index: number) => void;
    onEditTemplate: (index: number) => void;
    onDeleteTemplate: (index: number) => void;
};

const CommentTemplateModal = ({
    reducerId,
    isModalVisible,
    onModalClose,
    onCreateTemplate,
    onInsertTemplate,
    onEditTemplate,
    onDeleteTemplate,
}: Props) => {
    const [isEditing, setIsEditing] = useState(false);
    const { create: createAuthorization, update: updateAuthorization } =
        useAuthorizedActions("comment_template");
    const commentState = useReduxState(reducerId);

    const renderCreateButton = () => {
        const isAddable =
            commentState &&
            commentState.comment_templates &&
            commentState.totalAvailableCount &&
            commentState.totalAvailableCount >
                commentState.comment_templates.length;
        const button = (
            <Button
                className={styles.modalButton}
                size="small"
                onClick={onCreateTemplate}
                disabled={!isAddable || !createAuthorization}>
                <PlusOutlined />
                作成
            </Button>
        );

        if (!createAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        if (!isAddable) {
            <Tooltip title={"登録可能数の上限に達しています。"}>
                {button}
            </Tooltip>;
        }
        return button;
    };

    const renderEditButton = () => {
        const button = (
            <Button
                className={styles.modalButton}
                size="small"
                onClick={() => setIsEditing(!isEditing)}
                disabled={!updateAuthorization}>
                <EditOutlined />
                {!isEditing ? '編集' : 'キャンセル'}
            </Button>
        );
        if (!updateAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        return button;
    };

    return (
        <Modal
            title="コメントのテンプレート"
            visible={isModalVisible}
            onCancel={() => {
                onModalClose();
                setIsEditing(false);
            }}
            zIndex={1080}
            footer={null}>
            <div>
                <List
                    size="small"
                    bordered={false}
                    split={false}
                    dataSource={commentState && commentState.comment_templates}
                    className={styles.listContainer}
                    renderItem={(item: CommentTemplateModel, index) => (
                        <CommentTemplateListItem
                            title={item.title}
                            isEditing={isEditing}
                            onInsertTemplate={() => onInsertTemplate(index)}
                            onEditTemplate={() => onEditTemplate(index)}
                            onDeleteTemplate={() => onDeleteTemplate(index)}
                        />
                    )}
                />
                <Row>
                    <LimitDivider
                        currentCount={
                            (commentState &&
                                commentState.comment_templates &&
                                commentState.comment_templates.length) ??
                            0
                        }
                        limitCount={
                            (commentState &&
                                commentState.totalAvailableCount) ??
                            0
                        }
                    />
                </Row>
                <Row className={styles.modalActions}>
                    <Col span={5}></Col>
                    <Col span={6}>{renderCreateButton()}</Col>
                    <Col span={2}></Col>
                    <Col span={6}>{renderEditButton()}</Col>
                    <Col span={5}></Col>
                </Row>
            </div>
        </Modal>
    );
};

export default CommentTemplateModal;
