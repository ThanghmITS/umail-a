import React, { useState, MouseEvent } from "react";
import {
    Button,
    Col,
    FormProps,
    Modal,
    Tooltip,
    TransferProps,
    Row,
} from "antd";
import { InfoCircleTwoTone, SettingOutlined } from "@ant-design/icons";
import TableDisplaySettingForm from "~/components/Forms/DisplaySettingsForms/TableDisplaySettingForm/TableDisplaySettingForm";
import { TransferModel } from "~/models/transferModel";
import { DisplaySettingFieldTypes } from "~/models/displaySetting";
import { infoColor, warningColor } from "~/utils/constants";
import styles from "./TableDisplaySettingModal.scss";

type Props = FormProps & {
    fieldName: DisplaySettingFieldTypes;
    formId: string;
    transferProps?: TransferProps<TransferModel>;
    onAfterClose?: () => void;
};

const TableDisplaySettingModal = ({
    fieldName,
    transferProps,
    formId,
    onAfterClose = () => {},
    ...props
}: Props) => {
    const title = "検索結果表示項目設定";
    const infoMessage =
        "この設定は個人ごとに保持されますので、別ユーザーと共有されません。";

    const warningMessage = "表示項目は、必ず1つは表示させる必要があります。";

    const [isModalOpen, setIsModalOpen] = useState(false);

    const onCloseModal = (event: MouseEvent) => {
        event.stopPropagation();
        setIsModalOpen(false);
    };

    const allItemSelected = () => {
        const dataSource = transferProps?.dataSource ?? [];
        const targetKey = transferProps?.targetKeys ?? [];
        const leftPaneDataSource = dataSource.filter(
            (data) => !targetKey.includes(data.key)
        );
        return !leftPaneDataSource.length;
    };

    const renderTitle = () => {
        return (
            <Col span={24}>
                <Row wrap={false}>
                    <Col span={1}></Col>
                    <Col flex="auto" data-testid="modal-title">
                        {title}
                    </Col>
                    {allItemSelected() ? (
                        <Col span={1}>
                            <Tooltip
                                title={warningMessage}
                                color={warningColor}>
                                <InfoCircleTwoTone
                                    twoToneColor={warningColor}
                                />
                            </Tooltip>
                        </Col>
                    ) : (
                        <div />
                    )}
                    <Col span={1}></Col>
                    <Col span={1}>
                        <Tooltip title={infoMessage} color={infoColor}>
                            <InfoCircleTwoTone
                                data-testid="table-display-setting-modal-info-icon"
                                twoToneColor={infoColor}
                            />
                        </Tooltip>
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderCancelButton = () => {
        return (
            <Button key="cancelButton" onClick={onCloseModal}>
                キャンセル
            </Button>
        );
    };

    const renderUpdateButton = () => {
        const button = (
            <Button
                key="updateButton"
                type="primary"
                form={formId}
                htmlType="submit"
                disabled={allItemSelected()}
                onClick={onCloseModal}
                style={{ marginLeft: 8 }}>
                更新
            </Button>
        );

        const isAllItemSelected = allItemSelected();
        if (isAllItemSelected) {
            <Tooltip title={warningMessage}>{button}</Tooltip>;
        }
        return button;
    };

    return (
        <Tooltip title={title}>
            <Button
                key="modalButton"
                icon={
                    <SettingOutlined data-testid="table-display-setting-modal-open-button-icon" />
                }
                onClick={() => setIsModalOpen(true)}
                size="small"></Button>
            <Modal
                title={renderTitle()}
                visible={isModalOpen}
                forceRender
                closable={false}
                afterClose={onAfterClose}
                footer={[renderCancelButton(), renderUpdateButton()]}>
                <TableDisplaySettingForm
                    id={formId}
                    fieldName={fieldName}
                    {...props}
                    transferProps={transferProps}
                />
            </Modal>
        </Tooltip>
    );
};

export default TableDisplaySettingModal;
