import React from "react";
import { render, screen } from "~/test/utils";
import GenericModal from "../GenericModal";

const mockOnOk = jest.fn();
const mockOnCancel = jest.fn();

describe("GenericModal.tsx", () => {
    describe("without type icon", () => {
        test("simple render test", () => {
            render(<GenericModal visible>Content</GenericModal>);
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
        });

        test("render test with title", () => {
            const wrapper = (
                <GenericModal visible title="Generic Modal Title">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const titleElement = screen.getByText(/Generic Modal Title/);
            expect(titleElement).toBeInTheDocument();
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
        });

        test("default buttons exist", () => {
            const wrapper = (
                <GenericModal visible onOk={mockOnOk} onCancel={mockOnCancel}>
                    Content
                </GenericModal>
            );
            render(wrapper);
            const okButtonElement = screen.getByText(/OK/);
            expect(okButtonElement).toBeInTheDocument();
            const cancelButtonElement = screen.getByText(/キャンセル/);
            expect(cancelButtonElement).toBeInTheDocument();
        });

        test("render test changed ok text", () => {
            const wrapper = (
                <GenericModal visible onOk={mockOnOk} okText="オッケー">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const okButtonElement = screen.getByText(/オッケー/);
            expect(okButtonElement).toBeInTheDocument();
        });

        test("render test changed cancel text", () => {
            const wrapper = (
                <GenericModal
                    visible
                    onCancel={mockOnCancel}
                    cancelText="Cancel">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const cancelButtonElement = screen.getByText(/Cancel/);
            expect(cancelButtonElement).toBeInTheDocument();
        });

        test("icons does not exist", () => {
            render(<GenericModal visible>Content</GenericModal>);
            const infoIconElement = screen.queryByTestId("info-icon");
            expect(infoIconElement).not.toBeInTheDocument();
            const successIconElement = screen.queryByTestId("success-icon");
            expect(successIconElement).not.toBeInTheDocument();
            const warningIconElement = screen.queryByTestId("warning-icon");
            expect(warningIconElement).not.toBeInTheDocument();
            const errorIconElement = screen.queryByTestId("error-icon");
            expect(errorIconElement).not.toBeInTheDocument();
        });
    });

    describe("with type icon", () => {
        test("render test - info type", () => {
            const wrapper = (
                <GenericModal visible type="info" title="Generic Modal Title">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const infoIconElement = screen.getByTestId("info-icon");
            expect(infoIconElement).toBeInTheDocument();
            const titleElement = screen.getByText(/Generic Modal Title/);
            expect(titleElement).toBeInTheDocument();
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
            const successIconElement = screen.queryByTestId("success-icon");
            expect(successIconElement).not.toBeInTheDocument();
            const warningIconElement = screen.queryByTestId("warning-icon");
            expect(warningIconElement).not.toBeInTheDocument();
            const errorIconElement = screen.queryByTestId("error-icon");
            expect(errorIconElement).not.toBeInTheDocument();
        });

        test("render test - success type", () => {
            const wrapper = (
                <GenericModal
                    visible
                    type="success"
                    title="Generic Modal Title">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const successIconElement = screen.getByTestId("success-icon");
            expect(successIconElement).toBeInTheDocument();
            const titleElement = screen.getByText(/Generic Modal Title/);
            expect(titleElement).toBeInTheDocument();
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
            const infoIconElement = screen.queryByTestId("info-icon");
            expect(infoIconElement).not.toBeInTheDocument();
            const warningIconElement = screen.queryByTestId("warning-icon");
            expect(warningIconElement).not.toBeInTheDocument();
            const errorIconElement = screen.queryByTestId("error-icon");
            expect(errorIconElement).not.toBeInTheDocument();
        });

        test("render test - warning type", () => {
            const wrapper = (
                <GenericModal
                    visible
                    type="warning"
                    title="Generic Modal Title">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const warningIconElement = screen.getByTestId("warning-icon");
            expect(warningIconElement).toBeInTheDocument();
            const titleElement = screen.getByText(/Generic Modal Title/);
            expect(titleElement).toBeInTheDocument();
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
            const infoIconElement = screen.queryByTestId("info-icon");
            expect(infoIconElement).not.toBeInTheDocument();
            const successIconElement = screen.queryByTestId("success-icon");
            expect(successIconElement).not.toBeInTheDocument();
            const errorIconElement = screen.queryByTestId("error-icon");
            expect(errorIconElement).not.toBeInTheDocument();
        });

        test("render test - error type", () => {
            const wrapper = (
                <GenericModal visible type="error" title="Generic Modal Title">
                    Content
                </GenericModal>
            );
            render(wrapper);
            const errorIconElement = screen.getByTestId("error-icon");
            expect(errorIconElement).toBeInTheDocument();
            const titleElement = screen.getByText(/Generic Modal Title/);
            expect(titleElement).toBeInTheDocument();
            const contentElement = screen.getByText(/Content/);
            expect(contentElement).toBeInTheDocument();
            const infoIconElement = screen.queryByTestId("info-icon");
            expect(infoIconElement).not.toBeInTheDocument();
            const successIconElement = screen.queryByTestId("success-icon");
            expect(successIconElement).not.toBeInTheDocument();
            const warningIconElement = screen.queryByTestId("warning-icon");
            expect(warningIconElement).not.toBeInTheDocument();
        });
    });
});
