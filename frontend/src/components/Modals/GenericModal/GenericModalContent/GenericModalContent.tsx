import React, { ReactNode } from "react";
import { Typography } from "antd";
import { ParagraphProps } from "antd/lib/typography/Paragraph";

const { Paragraph } = Typography;

type Props = ParagraphProps & {
    children: ReactNode;
};

const GenericModalContent = ({ children, ...props }: Props) => {
    return (
        <Paragraph
            ellipsis={{
                rows: 13,
                expandable: true,
            }}
            style={{
                textAlign: "left",
            }}
            {...props}>
            {children}
        </Paragraph>
    );
};

export default GenericModalContent;
