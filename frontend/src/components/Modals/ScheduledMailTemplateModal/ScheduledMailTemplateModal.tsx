import React, { useState } from "react";
import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Col, Divider, List, Modal, Row, Tooltip, Empty } from "antd";
import ScheduleMailTemplateListItem from "~/components/DataDisplay/ScheduledMail/ScheduleMailTemplateListItem/ScheduleMailTemplateListItem";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import styles from "./ScheduledMailTemplateModal.scss";
import { CommentTemplateModel } from "~/models/commentTemplateModel";
import { ScheduledMailTemplateModel } from "~/models/scheduledEmailModel";
import LimitDivider from "~/components/Common/LimitDivider/LimitDivider";

type Props = {
    isModalVisible: boolean;
    onModalClose: () => void;
    onCreateTemplate: () => void;
    onInsertTemplate: (index: number) => void;
    onEditTemplate: (index: number) => void;
    onDeleteTemplate: (index: number) => void;
    dataTemplate: ScheduledMailTemplateModel;
};

const ScheduledMailTemplateModal = ({
    isModalVisible,
    onModalClose,
    onCreateTemplate,
    onInsertTemplate,
    onEditTemplate,
    onDeleteTemplate,
    dataTemplate
}: Props) => {
    const [isEditing, setIsEditing] = useState(false);
    const { create: createAuthorization, update: updateAuthorization } =
        useAuthorizedActions("scheduled_mails");

    const renderCreateButton = () => {
        const isAddable =
            dataTemplate &&
            dataTemplate.templates &&
            dataTemplate.total_available_count &&
            dataTemplate.total_available_count >
            dataTemplate.templates.length;
        const button = (
            <Button
                className={styles.modalButton}
                size="small"
                onClick={onCreateTemplate}
                disabled={!isAddable || !createAuthorization}>
                <PlusOutlined />
                作成
            </Button>
        );

        if (!createAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        if (!isAddable) {
            <Tooltip title={"登録可能数の上限に達しています。"}>
                {button}
            </Tooltip>;
        }
        return button;
    };

    const renderEditButton = () => {
        const button = (
            <Button
                className={styles.modalButton}
                size="small"
                onClick={() => setIsEditing(!isEditing)}
                disabled={!updateAuthorization}>
                <EditOutlined />
                {!isEditing ? '編集' : 'キャンセル'}
            </Button>
        );
        if (!updateAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        return button;
    };

    return (
        <Modal
            title="配信メールのテンプレート"
            visible={isModalVisible}
            onCancel={() => {
                onModalClose();
                setIsEditing(false);
            }}
            zIndex={100}
            footer={null}>
            <div>
                <List
                    size="small"
                    bordered={false}
                    split={false}
                    dataSource={dataTemplate && dataTemplate.templates}
                    className={styles.listContainer}
                    locale={{emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="データがありません" />}}
                    renderItem={(item: CommentTemplateModel, index) => (
                        <ScheduleMailTemplateListItem
                            title={item.title}
                            isEditing={isEditing}
                            onInsertTemplate={() => onInsertTemplate(index)}
                            onEditTemplate={() => onEditTemplate(index)}
                            onDeleteTemplate={() => onDeleteTemplate(index)}
                        />
                    )}
                />
                <Row>
                    <LimitDivider
                        currentCount={
                            (dataTemplate &&
                              dataTemplate.templates &&
                              dataTemplate.templates.length) ??
                            0
                        }
                        limitCount={
                            (dataTemplate &&
                              dataTemplate.total_available_count) ??
                            0
                        }
                    />
                </Row>
                <Row className={styles.modalActions}>
                    <Col span={5}></Col>
                    <Col span={6}>{renderCreateButton()}</Col>
                    <Col span={2}></Col>
                    <Col span={6}>{renderEditButton()}</Col>
                    <Col span={5}></Col>
                </Row>
            </div>
        </Modal>
    );
};

export default ScheduledMailTemplateModal;
