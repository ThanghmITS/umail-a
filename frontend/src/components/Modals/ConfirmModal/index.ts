import { Modal, ModalFuncProps } from "antd";

type Props = ModalFuncProps & {};

export const confirmModal = ({ ...props }: Props) => {
    Modal.confirm({
        cancelText: "キャンセル",
        onCancel: () => {},
        style: {
            textAlign: "left",
        },
        ...props,
    });
};
