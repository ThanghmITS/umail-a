import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Doughnut } from 'react-chartjs-2';

/*
This is an wrapper of react-chartjs-2
Related documentations:
https://github.com/jerairrest/react-chartjs-2/blob/master/example/src/components/doughnut.js
*/

const doughnutData = {
  labels: [
    'Red',
    'Green',
    'Yellow',
  ],
  datasets: [{
    data: [300, 50, 100],
    backgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56',
    ],
  }],
};

class DoughnutChart extends Component {
  render() {
    return (<Doughnut data={doughnutData} />);
  }
}

export default DoughnutChart;
