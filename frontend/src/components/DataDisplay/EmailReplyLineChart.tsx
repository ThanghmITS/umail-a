import React from "react";
import PropTypes from "prop-types";
import { ChartData, Line } from "react-chartjs-2";
import { CommonGraphDataSet } from "~/utils/constants";
import { GraphDataModel } from "~/models/graphModel";
import GraphCard from "../Pages/DashboardPage/GraphCard/GraphCard";
import { ChartOptions } from "chart.js";

type Props = {
    data: GraphDataModel;
};

const EmailReplyLineChart = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "sum",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getDataProps = (data: GraphDataModel): ChartData<any> => {
        const graphData = {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "返信件数",
                    backgroundColor: "rgba(192,128,96,0.4)",
                    borderColor: "rgba(192,128,96,1)",
                    pointBorderColor: "rgba(192,128,96,1)",
                    pointHoverBackgroundColor: "rgba(192,128,96,1)",
                    data: data.values,
                    yAxisID: "sum",
                },
            ],
        };
        return graphData;
    };

    return (
        <GraphCard
            title="共有メールBOXへの返信件数"
            data={getDataProps(data)}
            options={options}
        />
    );
};

export default EmailReplyLineChart;
