import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages, iconPrimaryColor, primaryColor } from "~/utils/constants";
import { PushpinFilled, PushpinOutlined } from "@ant-design/icons";
import styles from "./CommentPinButton.scss";

// TODO(joshua-hashimoto): コメント固定が3箇所に跨っているので統一したい。

type Props = {
    isUserComment: boolean;
    isPinned: boolean;
    onPin: () => void;
};

const CommentPinButton = ({ isUserComment, isPinned, onPin }: Props) => {
    const { pin: pinAuthorization } = useAuthorizedActions("comment");
    const { pin: anotherUserCommentPinAuthorization } = useAuthorizedActions(
        "another_user_comment"
    );

    const iAmAuthorized = isUserComment && pinAuthorization;

    return (
        <Tooltip
            title={
                pinAuthorization || anotherUserCommentPinAuthorization
                    ? "コメントを固定"
                    : ErrorMessages.isNotAuthorized
            }>
            <Button
                className={isPinned ? styles.controlButton : styles.colorFocus}
                type={"primary"}
                style={{ borderColor: primaryColor}}
                icon={isPinned ? <PushpinFilled /> : <PushpinOutlined />}
                size="small"
                onClick={onPin}
                disabled={
                    !pinAuthorization && !anotherUserCommentPinAuthorization
                }
                hidden={!iAmAuthorized && !anotherUserCommentPinAuthorization}
            />
        </Tooltip>
    );
};

export default CommentPinButton;
