import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages, TooltipMessages } from "~/utils/constants";
import { DeleteOutlined } from "@ant-design/icons";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import { CommentThreadModel } from "~/models/commentModel";
import styles from "./CommentDeleteButton.scss";

type Props = {
    isUserComment: boolean;
    onDelete: () => void;
    commentData?: CommentThreadModel;
};

const CommentDeleteButton = ({ isUserComment, onDelete, commentData }: Props) => {
    const { delete: deleteAuthorization } = useAuthorizedActions("comment");
    const { delete: anotherUserCommentDeleteAuthorization } =
        useAuthorizedActions("another_user_comment");

    const iAmAuthorized = isUserComment && deleteAuthorization;

    const isCommentChildImportant = !!commentData?.parent && !!commentData?.is_important;

    const onConfirmDelete = () => {
        confirmModal({
            title: "このコメントを削除しますか？",
            content: (
                <div>
                    <p>{`OKを押すと、削除が実行されます。`}</p>
                </div>
            ),
            onOk: onDelete,
            onCancel: () => {},
        });
    };

    return (
        <Tooltip
            title={
                isCommentChildImportant ? TooltipMessages.comment.nonRemovableComment
                : deleteAuthorization || anotherUserCommentDeleteAuthorization
                ? "コメントを削除"
                : ErrorMessages.isNotAuthorized
            }>
            <Button
                className={styles.button}
                icon={<DeleteOutlined />}
                size="small"
                type="primary"
                danger
                onClick={onConfirmDelete}
                disabled={
                    !deleteAuthorization &&
                    !anotherUserCommentDeleteAuthorization || isCommentChildImportant
                }
                hidden={
                    !iAmAuthorized && !anotherUserCommentDeleteAuthorization
                }
            />
        </Tooltip>
    );
};

export default CommentDeleteButton;
