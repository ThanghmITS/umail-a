import React from "react";
import { Card, Tooltip, Button, List, Row, Col } from "antd";
import { DeleteOutlined, EditOutlined, CheckOutlined } from "@ant-design/icons";
import styles from "./CommentTemplateListItem.scss";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";

type Props = {
    title: string;
    isEditing: boolean;
    onInsertTemplate: () => void;
    onEditTemplate: () => void;
    onDeleteTemplate: () => void;
};

const CommentTemplateListItem = ({
    title,
    isEditing,
    onInsertTemplate,
    onEditTemplate,
    onDeleteTemplate,
}: Props) => {
    const { insert: insertAuthorization } =
        useAuthorizedActions("comment_template");

    const editingActions = isEditing
        ? [
              <Tooltip title="テンプレートを編集" key="template-edit-button">
                  <Button
                      className={styles.editActionButton}
                      size="small"
                      onClick={onEditTemplate}
                      type="primary"
                      icon={<EditOutlined />}
                  />
              </Tooltip>,
              <Tooltip title="テンプレートを削除" key="template-delete-button">
                  <Button
                      className={styles.editActionButton}
                      size="small"
                      type="primary"
                      danger
                      onClick={onDeleteTemplate}
                      icon={<DeleteOutlined />}
                  />
              </Tooltip>,
          ]
        : [];

    const actions = [
        <Tooltip
            title={
                insertAuthorization
                    ? "テンプレートを挿入"
                    : ErrorMessages.isNotAuthorized
            }
            key="template-insert-button">
            <Button
                size="small"
                onClick={onInsertTemplate}
                icon={<CheckOutlined />}
                disabled={!insertAuthorization}
            />
        </Tooltip>,
        ...editingActions,
    ];

    return (
        <List.Item title={title}>
            <Card>
                <Row>
                    <Col span={24}>
                        <Row>
                            <Col span={12}>
                                <Row justify="start" className={styles.title}>
                                    {title}
                                </Row>
                            </Col>
                            <Col span={12}>
                                <Row justify="end">
                                    {actions.map((action) => action)}
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Card>
        </List.Item>
    );
};

export default CommentTemplateListItem;
