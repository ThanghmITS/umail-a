import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { EditOutlined } from "@ant-design/icons";
import { ErrorMessages, primaryColor } from "~/utils/constants";
import styles from "./CommentUpdateButton.scss";

type Props = {
    isUserComment: boolean;
    onUpdate: () => void;
};

const CommentUpdateButton = ({ isUserComment, onUpdate }: Props) => {
    const { update: updateAuthorization } = useAuthorizedActions("comment");
    const { update: anotherUserCommentUpdateAuthorization } =
        useAuthorizedActions("another_user_comment");

    const iAmAuthorized = isUserComment && updateAuthorization;

    return (
        <Tooltip
            title={
                updateAuthorization || anotherUserCommentUpdateAuthorization
                    ? "コメントを編集"
                    : ErrorMessages.isNotAuthorized
            }>
            <Button
                className={styles.controlButton}
                icon={<EditOutlined />}
                style={{ borderColor: primaryColor }}
                type="primary"
                size="small"
                onClick={onUpdate}
                disabled={
                    !updateAuthorization &&
                    !anotherUserCommentUpdateAuthorization
                }
                hidden={
                    !iAmAuthorized && !anotherUserCommentUpdateAuthorization
                }
            />
        </Tooltip>
    );
};

export default CommentUpdateButton;
