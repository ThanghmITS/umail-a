import React from "react";
import CommentTemplateFormModal, {
    CommentTemplateFormModel,
} from "../CommentTemplateFormModel/CommentTemplateFormModel";
import styles from "./CommentTemplateCreateModal.scss";

type Props = {
    isOpen: boolean;
    onOk: (values: CommentTemplateFormModel) => void;
    onCancel: () => void;
};

const CommentTemplateCreateModal = ({ ...props }: Props) => {
    return <CommentTemplateFormModal title="テンプレート作成" {...props} />;
};

export default CommentTemplateCreateModal;
