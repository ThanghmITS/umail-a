import React from "react";
import { Button, Tooltip, Popover, Comment } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { CommentOutlined, SmileOutlined } from "@ant-design/icons";
import { ErrorMessages, primaryColor } from "~/utils/constants";
import getDateStr from "~/domain/date";
import { CommentThreadChildModel } from "~/models/commentModel";
import styles from "./CommentReplyButton.scss";
import CommentFilledIcon from "~/components/Common/CommentFilledIcon/CommentFilledIcon";

type Props = {
    onReply: () => void;
    commentData?: CommentThreadChildModel;
    ScrollTo: () => void;
    isReplying: boolean;
};

const CommentReplyButton = ({ onReply, commentData, ScrollTo, isReplying }: Props) => {
    const { create: replyAuthorization } = useAuthorizedActions("comment");

    let datetime_str = undefined;
      if (commentData?.parent_created_time) {
          datetime_str = getDateStr(commentData?.parent_created_time);
      }

    return (
      commentData?.parent_content ?
        <Popover
          overlayClassName={styles.contentStyles}
          title={
            <Comment
            avatar={<SmileOutlined className={styles.avatar} />}
            content={<p>{commentData.parent_content}</p>}
            author={commentData.parent_created_user_name}
            datetime={datetime_str}
            />
          }
          content={
            <div className={styles.buttonScroll}>
              <Button onClick={ScrollTo}>このメッセージに移動</Button>
            </div>
            }
        >
          <Button
              className={styles.replyCommentButton}
              size="small"
              type="primary"
          >返信元</Button>
        </Popover> :
        <Tooltip
            title={
                  replyAuthorization
                    ? "返信"
                    : ErrorMessages.isNotAuthorized
            }>
            <Button
                className={styles.controlButton}
                icon={!isReplying ? <CommentOutlined /> : <CommentFilledIcon style={{ transform: 'scale(1.6) translateY(1px)' }}/>}
                size="small"
                type="primary"
                onClick={onReply}
                hidden={
                    !replyAuthorization
                }
            />
        </Tooltip>
    );
};

export default CommentReplyButton;
