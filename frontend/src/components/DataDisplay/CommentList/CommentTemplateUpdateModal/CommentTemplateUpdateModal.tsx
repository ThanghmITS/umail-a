import React from "react";
import CommentTemplateFormModal, {
    CommentTemplateFormModel,
} from "../CommentTemplateFormModel/CommentTemplateFormModel";
import styles from "./CommentUpdateModal.scss";

type Props = {
    initialData: CommentTemplateFormModel;
    isOpen: boolean;
    onOk: (values: CommentTemplateFormModel) => void;
    onCancel: () => void;
};

const CommentTemplateUpdateModal = ({ ...props }: Props) => {
    return <CommentTemplateFormModal title="テンプレート更新" {...props} />;
};

export default CommentTemplateUpdateModal;
