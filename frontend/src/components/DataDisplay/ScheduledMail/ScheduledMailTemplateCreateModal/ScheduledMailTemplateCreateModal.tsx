import React, { MutableRefObject } from "react";
import {
  ScheduledMailTemplateFormModel
} from "~/models/scheduledEmailModel";
import ScheduledMailTemplateFormModal from "../ScheduledMailTemplateFormModal/ScheduledMailTemplateFormModal";
import {
  FormInstance
} from "antd";

type Props = {
    currentUserId: string;
    isOpen: boolean;
    onOk: (values: ScheduledMailTemplateFormModel) => void;
    onCancel: () => void;
    formRef: MutableRefObject<FormInstance>;
};

const ScheduledMailTemplateCreateModal = ({ ...props }: Props) => {
    return <ScheduledMailTemplateFormModal titleModal="テンプレート作成" {...props} />;
};

export default ScheduledMailTemplateCreateModal;
