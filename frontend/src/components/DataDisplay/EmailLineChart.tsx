import React from "react";
import { round } from "../helpers";
import { CommonGraphDataSet } from "~/utils/constants";
import { GraphDataModel } from "~/models/graphModel";
import { Override } from "~/utils/types";
import GraphCard from "../Pages/DashboardPage/GraphCard/GraphCard";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";

type ValuesModel = {
    personnelEmail: number[];
    jobEmail: number[];
};

type OverridedGraphDataModel = Override<
    GraphDataModel,
    {
        values: ValuesModel;
    }
>;

type Props = {
    data: OverridedGraphDataModel;
};

const EmailLineChart = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "sum",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
                {
                    id: "ratio",
                    type: "linear",
                    position: "right",
                    gridLines: {
                        display: false,
                    },
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getDataProps = (data: OverridedGraphDataModel): ChartData<any> => ({
        labels: data.labels,
        datasets: [
            {
                ...CommonGraphDataSet,
                label: "要員",
                backgroundColor: "rgba(192,96,192,0.4)",
                borderColor: "rgba(192,96,192,1)",
                pointBorderColor: "rgba(192,96,192,1)",
                pointHoverBackgroundColor: "rgba(192,96,192,1)",
                data: data.values.personnelEmail,
                yAxisID: "sum",
            },
            {
                ...CommonGraphDataSet,
                label: "案件",
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                pointBorderColor: "rgba(75,192,192,1)",
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                data: data.values.jobEmail,
                yAxisID: "sum",
            },
            {
                ...CommonGraphDataSet,
                label: "案件/要員 比",
                backgroundColor: "rgba(192,192,96,0.4)",
                borderColor: "rgba(192,192,96,1)",
                pointBorderColor: "rgba(192,192,96,1)",
                pointHoverBackgroundColor: "rgba(192,192,96,1)",
                data: data.values.jobEmail.map((value, index) =>
                    round(value / data.values.personnelEmail[index], 100)
                ),
                yAxisID: "ratio",
            },
        ],
    });

    return (
        <GraphCard
            title="共有メールBOXの受診件数"
            data={getDataProps(data)}
            options={options}
        />
    );
};

export default EmailLineChart;
