import React from "react";
import PropTypes from "prop-types";
import { ChartData, HorizontalBar } from "react-chartjs-2";
import _ from "lodash";
import { GraphDataModel } from "~/models/graphModel";
import GraphCard from "../Pages/DashboardPage/GraphCard/GraphCard";
import { ChartOptions } from "chart.js";

const labelTruncator = (label: string) => _.truncate(label, { length: 14 });

type Props = {
    data: GraphDataModel;
};

const MailRankingChart = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            xAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
            yAxes: [
                {
                    ticks: {
                        callback: labelTruncator,
                    },
                },
            ],
        },
        tooltips: {
            enabled: true,
            mode: "label",
            callbacks: {
                title: (tooltipItems: any, data: GraphDataModel) => {
                    const idx = tooltipItems[0].index;
                    return data.labels[idx];
                },
                label: (tooltipItems: any) => `受信件数:${tooltipItems.xLabel}`,
            },
        },
    };

    const getDataProps = (data: GraphDataModel): ChartData<any> => {
        const graphData = {
            labels: data.labels,
            datasets: [
                {
                    label: "受信件数",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: data.values,
                },
            ],
        };
        return graphData;
    };
    return (
        <GraphCard
            title="メールの送信元ランキング(先月分)"
            data={getDataProps(data)}
            options={options}
        />
    );
};

export default MailRankingChart;
