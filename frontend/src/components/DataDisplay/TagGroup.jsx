import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Input,
  Tag,
  Tooltip,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { iconPrimaryColor } from '~/utils/constants';
const { TextArea } = Input;

class EditableTagGroup extends Component {
  maxDisplayLength = 12;

  constructor(props) {
    super(props);
    this.state = {
      value: [],
      initialDataStored: false,
      inputVisible: false,
      inputValue: '',
    };
  }

  componentDidUpdate() {
    // Assuming the getFieldDecorator will give the value when using this component in antd forms.
    const { value } = this.props;
    const { initialDataStored } = this.state;
    if (value && !initialDataStored) {
      this.setState({
        value,
        initialDataStored: true,
      });
    }
  }

  syncValueToParent = () => {
    const { onChange } = this.props;
    const { value } = this.state;
    onChange({ target: { value } });
  };

  showInput = () => {
    this.setState({ inputVisible: true }, () => this.input.focus());
  };

  handleInputChange = (e) => {
    this.setState({ inputValue: e.target.value });
  };

  handleInputConfirm = () => {
    const { maxTags } = this.props;
    const { inputValue } = this.state;
    let { value } = this.state;
    if (inputValue && value.indexOf(inputValue) === -1 && value.length < maxTags) { // Prevent duplication
      value = [...value, inputValue];
    }
    this.setState({
      value,
      inputVisible: false,
      inputValue: '',
    }, () => this.syncValueToParent());
  };

  handleRemove = (removedTag) => {
    const { value } = this.state;
    const tags = value.filter(tag => tag !== removedTag);
    this.setState({ value: tags }, () => this.syncValueToParent());
  };

  saveInputRef = input => (this.input = input);

  render() {
    const { maxInputLength, maxTags } = this.props;
    const { value: tags, inputVisible, inputValue } = this.state;
    return (
      <div>
        {tags.map((tag) => {
          const isLongTag = tag.length > this.maxDisplayLength;
          const tagElem = (
            <Tag key={tag} closable onClose={() => this.handleRemove(tag)}>
              {isLongTag ? `${tag.slice(0, this.maxDisplayLength)}...` : tag}
            </Tag>
          );
          return isLongTag ? (
            <Tooltip title={tag} key={tag}>
              {tagElem}
            </Tooltip>
          ) : (
            tagElem
          );
        })}
        {inputVisible && (
          <TextArea
            ref={this.saveInputRef}
            type="text"
            autoSize={{minRows:1}}
            size="small"
            style={{ width: 78 }}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
            maxLength={maxInputLength}
          />
        )}
        {!inputVisible && (
          <Tag onClick={this.showInput} style={{ background: '#fff', borderStyle: 'dashed' }} hidden={tags.length >= maxTags}>
            <PlusOutlined style={{ color: iconPrimaryColor }} />
            追加
          </Tag>
        )}
      </div>
    );
  }
}

EditableTagGroup.propTypes = { // Workaround: Omitting "value" prop to avoid warnings when using this component in a antd Form with getFieldDecorator.
  onChange: PropTypes.func, // Antd Form support: Defined to behave as input element.
  maxInputLength: PropTypes.number,
  maxTags: PropTypes.number,
};

EditableTagGroup.defaultProps = {
  onChange: () => {},
  maxInputLength: 50,
  maxTags: 10,
};

export default EditableTagGroup;
