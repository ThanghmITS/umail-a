import React from "react";
import { Alert } from "antd";
import styles from "./Alert.scss";

type Props = {
    message?: string;
    errorMessage?: string;
};

const MessageAlert = ({ message, errorMessage }: Props) => {
    if (errorMessage) {
        return (
            <Alert
                message={errorMessage}
                type="error"
                className={styles.alert}
            />
        );
    }
    if (message) {
        return (
            <Alert message={message} type="success" className={styles.alert} />
        );
    }
    return null;
};

export default MessageAlert;
