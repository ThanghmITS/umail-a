import React, { ChangeEventHandler, ReactNode } from "react";
import { Modal, Input } from "antd";
import styles from "./Modal.scss";
const { TextArea } = Input;

/**
 * A modal creator function.
 * @param resourceName {String} A resource name for the text of the modal.
 * @param actionName {String} An action name for the text of the modal.
 * @returns {function(*=, *=): Function}
 */
export const createModal = (resourceName: string, actionName: string) => {
    const doNothing = () => {};
    /**
     *
     * @param okHandler {Function} A callback function to invoke when the user clicked OK button.
     * @param cancelHandler {Function} A callback function to invoke when the user clicked Cancel button.
     * @returns {Component} A react component. (antd.Modal)
     */
    const showModal =
        (
            okHandler: (...args: any[]) => any,
            cancelHandler: (...args: any[]) => any
        ) =>
        () => {
            var additionalMessage: ReactNode = "";
            var undoMessage: string = "";
            if (actionName == "削除") {
                undoMessage = "元には戻せません。";
                additionalMessage = (
                    <div>
                        <p>
                            ※配信ステータスが「配信中」のメールは削除することができません。
                        </p>
                    </div>
                );
            }
            Modal.confirm({
                title: `この${resourceName}を${actionName}しますか？`,
                content: (
                    <div>
                        <p>
                            {`OKを押すと、${actionName}が実行されます。`}
                            <br />
                            {undoMessage}
                        </p>
                        {additionalMessage}
                    </div>
                ),
                onOk: okHandler,
                cancelText: "キャンセル",
                onCancel: cancelHandler || doNothing,
                bodyStyle: {
                    textAlign: "left",
                },
                okButtonProps: {
                    className:`${styles.colorFocus}`
                }
            });
        };
    return showModal;
};

export type ShowDeleteModalResourceType =
    | "organizationEditPage"
    | "organizationSearchPage"
    | "contactEditPage"
    | "contactSearchPage"
    | "scheduledEmailSearchPage"
    | "userEditPage"
    | "userSearchPage"
    | "tagPage"
    | "tagEditPage"
    | "sharedEmailDetailPage";

const getShowDeleteModalText = (
    reducerName: ShowDeleteModalResourceType
): { additionalMessage: ReactNode; modalTitle: string } => {
    switch (reducerName) {
        case "organizationEditPage":
        case "organizationSearchPage":
            return {
                additionalMessage: (
                    <div>
                        <p>
                            ※この取引先の関連リソース(取引先担当者、配信メールの宛先)も同時に削除されます。
                            <br />
                            ※配信ステータスが「配信中」のメールの宛先に指定されている場合は削除することができません。
                        </p>
                    </div>
                ),
                modalTitle: "この取引先を削除しますか？",
            };
        case "contactEditPage":
        case "contactSearchPage":
            return {
                additionalMessage: (
                    <div>
                        <p>
                            ※この取引先担当者の関連リソース(配信メールの宛先)も同時に削除されます。
                            <br />
                            ※配信ステータスが「配信中」のメールの宛先に指定されている場合は削除することができません。
                        </p>
                    </div>
                ),
                modalTitle: "この取引先担当者を削除しますか？",
            };
        case "scheduledEmailSearchPage":
            return {
                additionalMessage: (
                    <div>
                        <p>
                            ※配信ステータスが「配信中」のメールは削除することができません。
                        </p>
                    </div>
                ),
                modalTitle: "この配信メールを削除しますか？",
            };
        case "userEditPage":
        case "userSearchPage":
            return {
                additionalMessage: (
                    <div>
                        <p>
                            ※関連リソースに配信ステータスが「下書き」「配信待ち」「配信中」の配信メールが含まれていると実行されません。
                        </p>
                    </div>
                ),
                modalTitle: "このユーザーを削除しますか？",
            };
        case "tagPage":
        case "tagEditPage":
            return {
                additionalMessage: (
                    <div>
                        <p>
                            OKを押すと、削除が実行されます。
                            <br />
                            タグを設定している箇所からも、同時に削除されます。
                        </p>
                        <p>
                            ※配信ステータスが「下書き」「配信待ち」「配信中」のメールにこのタグが含まれている場合は実行されません。
                        </p>
                    </div>
                ),
                modalTitle: "このタグを削除しますか？",
            };
        default:
            return {
                additionalMessage: "",
                modalTitle: "",
            };
    }
};

export const showDeleteModal =
    (deleteHandler: () => void, reducerName: ShowDeleteModalResourceType) =>
    () => {
        const { modalTitle, additionalMessage } =
            getShowDeleteModalText(reducerName);
        Modal.confirm({
            title: `${modalTitle}`,
            content: (
                reducerName === "tagEditPage" || reducerName === "tagPage" ? 
                <div>
                    <p>
                        元には戻せません。
                    </p>
                    {additionalMessage}
                </div>
                :
                <div>
                    <p className={styles.firstMessage}>
                        OKを押すと、削除が実行されます。
                    </p>
                    <p>元には戻せません。</p>
                    {additionalMessage}
                </div>
            ),
            onOk: deleteHandler,
            cancelText: "キャンセル",
            className:"",
            onCancel: () => {},
            bodyStyle: {
                textAlign: "left",
            },
            okButtonProps: {
                className:`${styles.colorFocus}`
            }
        });
    };

export const showSaveTemplateModal =
    (
        onChange: ChangeEventHandler<HTMLTextAreaElement>,
        onOk: (...args: any[]) => any,
        onCancel: (...args: any[]) => any
    ) =>
    () => {
        Modal.confirm({
            title: "保存するテンプレートの名称を入力してください。",
            content: (
              <TextArea
                    autoSize={{minRows:1}}
                    placeholder="テンプレート名称"
                    onChange={onChange}
                    allowClear
                    autoFocus
                />
            ),
            onOk: onOk,
            cancelText: "キャンセル",
            onCancel: onCancel,
            width: 450,
        });
    };
