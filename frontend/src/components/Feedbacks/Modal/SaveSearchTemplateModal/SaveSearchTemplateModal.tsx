import React, { MouseEvent } from "react";
import { Form, Input, Col, Row, Button } from "antd";
import { useReduxState } from "~/hooks/useReduxState";
import LimitDivider from "~/components/Common/LimitDivider/LimitDivider";
import styles from "./SaveSearchTemplateModal.scss";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";

type NewSearchTemplateFormModel = {
    newTemplateName: string;
};

type Props = {
    templateReducerName:
        | "organizationSearchPage"
        | "contactSearchPage"
        | "scheduledEmailRegisterPageContactSearchForm"
        | "scheduledEmailEditPageContactSearchForm";
    isSaveTemplateModalVisible: boolean;
    onCancel: (event: MouseEvent<HTMLElement>) => void;
    onOk: (values: NewSearchTemplateFormModel) => void;
};

const SaveSearchTemplateModal = ({
    templateReducerName,
    isSaveTemplateModalVisible,
    onCancel,
    onOk,
}: Props) => {
    const [form] = Form.useForm<NewSearchTemplateFormModel>();
    const { currentSearchTemplates, searchTemplateTotalAvailableCount } =
        useReduxState(templateReducerName);

    return (
        <GenericModal
            title="保存するテンプレートの名称を入力してください。"
            visible={isSaveTemplateModalVisible}
            zIndex={9999}
            destroyOnClose={true}
            afterClose={() => {
                form.resetFields();
            }}
            closable={false}>
            <Col span={24}>
                <Form form={form} onFinish={onOk}>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Form.Item
                                className={styles.field}
                                name="newTemplateName"
                                rules={[
                                    {
                                        max: 50,
                                        message:
                                            "テンプレート名は50文字以内で入力してください。",
                                    },
                                    {
                                        required: true,
                                        message:
                                            "テンプレート名を入力してください",
                                    },
                                ]}>
                                <Input
                                    placeholder="テンプレート名称"
                                    allowClear
                                    autoFocus
                                />
                            </Form.Item>
                        </Col>
                    </Form.Item>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <LimitDivider
                                currentCount={
                                    currentSearchTemplates.length ?? 0
                                }
                                limitCount={
                                    searchTemplateTotalAvailableCount ?? 0
                                }
                                prefix="現在登録数:"
                            />
                        </Col>
                    </Form.Item>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="end" gutter={6}>
                                <Col>
                                    <Button type="default" onClick={onCancel}>
                                        キャンセル
                                    </Button>
                                </Col>
                                <Col>
                                    <Form.Item shouldUpdate noStyle>
                                        {() => (
                                            <Button
                                                htmlType="submit"
                                                type="primary"
                                                disabled={
                                                    !form.isFieldsTouched(
                                                        ["newTemplateName"],
                                                        true
                                                    ) ||
                                                    !!form
                                                        .getFieldsError()
                                                        .filter(
                                                            ({ errors }) =>
                                                                errors.length
                                                        ).length
                                                }>
                                                OK
                                            </Button>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                </Form>
            </Col>
        </GenericModal>
    );
};

export default SaveSearchTemplateModal;
