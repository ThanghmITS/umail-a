export const mobileDeviceBreakpoint = 768;

export const isMobileDevices = () => window.innerWidth < mobileDeviceBreakpoint;

export const formatMoneyNumber = value => (value ? value.toLocaleString() : value);
export const formatMoneyNumberString = str => `${str}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
export const parseMoneyNumberString = str => str.replace(/(,*)/g, '');

export const round = (value, base) => Math.round(value * base) / base;
