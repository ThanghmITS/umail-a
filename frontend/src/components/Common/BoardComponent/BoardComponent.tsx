import React, { ReactNode } from "react";
import { BoardColumnModel, BoardModel } from "~/models/boardCommonModel";
import BoardBaseComponent from "./BoardBaseComponent/BoardBaseComponent";
import { SettingFn } from "./BoardBaseComponent/MenuBoardSettingsComponent/MenuBoardSettingsComponent";

/**
 * NOTE(joshua-hashimoto):
 * use this component to type the react-kanban component.
 */

export type RenderCardActions = {
    removeCard: () => void;
    dragging: () => void;
};

type Props<T> = {
    typeCard: string;
    boardData: BoardModel<T>;
    renderCard: (record: T, actions: RenderCardActions) => ReactNode;
    onSettings: SettingFn;
    dataBoardList?: BoardColumnModel[];
};

function BoardComponent<T>({
    typeCard,
    boardData,
    renderCard,
    onSettings,
}: Props<T>) {
    return (
        <BoardBaseComponent
            typeCard={typeCard}
            boardData={boardData}
            renderCard={renderCard}
            onSettings={onSettings}
        />
    );
}

export default BoardComponent;
