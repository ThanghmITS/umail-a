import React from 'react'
import BaseCard from "../../BoardCommon/BaseCard/BaseCard";
import {
  ProjectBoardListCardModel,
  ProjectBoardWorkingHoursCardModel,
  ProjectBoardSettleWidthCardModel,
} from "~/models/projectBoardModel";
type Props = {
        itemCard: ProjectBoardListCardModel;
        onDetailCard: (cardId: string) => () => void;
}
const BoardBaseProjectItemCard = ({ itemCard, onDetailCard }: Props) => {
    const genRangeDate = ({
      start,
      end,
    }:
      | ProjectBoardWorkingHoursCardModel
      | ProjectBoardSettleWidthCardModel) => {
        if(!start || !end) return null
        return `${start} - ${end}`
      };
    return (
        <BaseCard
            isSimpleCard={false}
            onDetailCard={onDetailCard(itemCard?.id)}
            onStartDateSelect={() => {}}
            onEndDateSelect={() => {}}
            onRemoveAssign={() => {}}
            handleChange={() => {}}
            data={{
                id: itemCard.id,
                title: itemCard?.detail,
                priority: "high",
                affiliation: '',
                info: [
                    {
                        label: "期限",
                        value: itemCard?.isImmediate?"即日":null,
                    },
                    {
                        label: "作業場所",
                        value: itemCard?.place,
                    },
                    {
                        label: "就業時間",
                        value: genRangeDate(itemCard?.workingHours),
                    },
                    {
                        label: "人数",
                        value: itemCard?.peopleAmount?`${itemCard?.peopleAmount}名`: null,
                    },
                    {
                        label: "単金",
                        value: itemCard?.price,
                    },
                    {
                        label: "精算幅",
                        value: genRangeDate(itemCard?.settleWidth),
                    },
                    {
                        label: "精算方法",
                        value: itemCard?.settleMethod,
                    },
                    {
                        label: "精算単位",
                        value: itemCard?.settleIncrement,
                    },
                    {
                        label: "支払サイト",
                        value: itemCard?.paymentDays,
                    },
                    {
                        label: "面談",
                        value: itemCard?.interviewAmount?`${itemCard?.interviewAmount}回`: null,
                    },
                    {
                        label: "商流",
                        value: itemCard?.distribution,
                    },
                ],
                assignees: itemCard?.assignees
            }}
        />
    );
}

export default BoardBaseProjectItemCard;

