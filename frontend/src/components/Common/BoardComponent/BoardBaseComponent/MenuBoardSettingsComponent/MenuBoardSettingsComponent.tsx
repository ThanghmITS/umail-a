import { Col } from "antd";
import React, { useCallback, useMemo, useState } from "react";
import { usePersonnelBoardFetchListsAPIQuery } from "~/hooks/usePersonnelBoard";
import { PersonnelBoardCardMenuOptionModel } from "~/models/personnelBoardModel";
import {
  PERSONNEL_CARD_ARCHIVE_MENUES,
  PERSONNEL_CARD_SORT_MENUES
} from "~/utils/constants";
import { LooseObject } from "~/utils/types";
import MenuBoardSubSettingsComponent from "../MenuBoardSubSettingsComponent/MenuBoardSubSettingsComponent";
import styles from "./MenuBoardSettingsComponent.scss";

export type SettingFn = (type: string, params?: LooseObject) => void;

export enum SETTING_ENUM {
  SORT = "SORT",
  MOVE_ALL_CARD = "MOVE_ALL_CARD",
  ARCHIVE_ALL_CARD = "ARCHIVE_ALL_CARD",
}

type Props = {
  setVisibleSetting: (v: boolean) => void;
  onSettings: SettingFn;
  listId: string;
};

function MenuBoardSettings({ setVisibleSetting, onSettings, listId }: Props) {
  const [subMenuVisible, setSubMenuVisible] = useState<string | undefined>();
  const { data: _moveCardList } = usePersonnelBoardFetchListsAPIQuery({});

  const _onSortBoardHandler = useCallback(
    async (type: string, value: string) =>
      onSettings(type, { listId, orderBy: value }),
    [onSettings, listId]
  );

  const _onMoveBoardHandler = useCallback(
    async (type: string, value: string) =>
      onSettings(type, { listId, newListId: value }),
    [onSettings, listId]
  );

  const _onArchiveBoardHandler = useCallback(
    async (type: string, _) => onSettings(type, { listId }),
    [onSettings, listId]
  );

  const onOpenSubMenu = useCallback(
    (type?: string) => {
      if (subMenuVisible) return;

      setSubMenuVisible(type);
      setVisibleSetting(false);
    },
    [setVisibleSetting, subMenuVisible]
  );

  const _onBack = useCallback(() => {
    setVisibleSetting(true);
    setSubMenuVisible(undefined);
  }, [setSubMenuVisible]);

  const _onCloseSubMenu = useCallback(() => {
    setSubMenuVisible(undefined);
    setVisibleSetting(false);
  }, [setSubMenuVisible]);

  const _moveCardMenues: PersonnelBoardCardMenuOptionModel[] = useMemo(
    () =>
      (_moveCardList ?? []).map((item) => {
        return { label: item.title, value: item.id };
      }),
    [_moveCardList]
  );

  return (
    <Col className={styles.container}>
      <MenuBoardSubSettingsComponent
        onChange={_onSortBoardHandler}
        title="ソート操作"
        items={PERSONNEL_CARD_SORT_MENUES}
        onClick={onOpenSubMenu}
        onBack={_onBack}
        onClose={_onCloseSubMenu}
        type={SETTING_ENUM.SORT}
        visible={subMenuVisible === SETTING_ENUM.SORT}
      />
      <MenuBoardSubSettingsComponent
        onChange={_onMoveBoardHandler}
        title="リスト内の全てのカードを移動"
        items={_moveCardMenues}
        onClick={onOpenSubMenu}
        onBack={_onBack}
        onClose={_onCloseSubMenu}
        type={SETTING_ENUM.MOVE_ALL_CARD}
        visible={subMenuVisible === SETTING_ENUM.MOVE_ALL_CARD}
      />

      <MenuBoardSubSettingsComponent
        onChange={_onArchiveBoardHandler}
        title="リスト内の全てのカードをアーカイブ"
        items={PERSONNEL_CARD_ARCHIVE_MENUES}
        onClick={onOpenSubMenu}
        onBack={_onBack}
        onClose={_onCloseSubMenu}
        type={SETTING_ENUM.ARCHIVE_ALL_CARD}
        visible={subMenuVisible === SETTING_ENUM.ARCHIVE_ALL_CARD}
      />
    </Col>
  );
}

export default MenuBoardSettings;
