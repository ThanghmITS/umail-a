import { RightOutlined } from "@ant-design/icons";
import { Row, Dropdown, Col, Menu } from "antd";
import Text from "antd/lib/typography/Text";
import React, { useCallback, useMemo } from "react";
import HeaderMenuBoard from "~/components/Common/BoardCommon/HeaderMenuBoard/HeaderMenuBoard";
import { SETTING_ENUM } from "../MenuBoardSettingsComponent/MenuBoardSettingsComponent";
import styles from "./MenuBoardSubSettingsComponent.scss";

export type MenuSelectType = {
  key: string;
};

type Props = {
  onClick: (type?: string) => void;
  items?: any[];
  title: string;
  onBack: () => void;
  onClose: () => void;
  onChange: (type: string, v: string) => void;
  type?: string;
  visible: boolean;
};

function MenuBoardSubSettingsComponent({
  onClick,
  onBack,
  onChange,
  title,
  items,
  type,
  visible,
  onClose,
}: Props) {
  const _onVisibleChange = useCallback(
    (v: boolean) => {
      if (v) return;

      onClose();
    },
    [onClose]
  );

  const _menuItems = useMemo(
    () => {

      return (
        <Col
          className={
            type === SETTING_ENUM.SORT
              ? styles.sortMenuWrapper
              : styles.menuWrapper
          }
        >
          <HeaderMenuBoard
            title={title}
            onClose={onClose}
            className={styles.headerMenu}
            onBack={() => onBack()}
          />
  
          <Menu
            theme="light"
            className={styles.menu}
            onClick={(info: MenuSelectType) => {
              const _types = info.key.split(`${type}-`);
  
              if (_types.length > 1 && type) onChange(type, _types[1]);
  
              onClose()
            }}
          >
            {
              (items ?? []).map((menu) => (
                <Menu.Item key={`${type}-${menu.value}`}>{menu.label}</Menu.Item>
              ))
            }
          </Menu>
        </Col>
      )
    },
    [items, onBack, onClose, title, type]
  );

  return (
    <Row
      className={styles.container}
      justify="space-between"
      onClick={() => onClick(type)}
    >
      <Text>{title}</Text>

      <Dropdown
        overlay={_menuItems}
        trigger={["click"]}
        placement="bottom"
        className={styles.dropdown}
        visible={visible}
        onVisibleChange={_onVisibleChange}
      >
        <RightOutlined />
      </Dropdown>
    </Row>
  );
}

export default MenuBoardSubSettingsComponent;
