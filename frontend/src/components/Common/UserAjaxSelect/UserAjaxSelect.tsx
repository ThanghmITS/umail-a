import React, { useState } from "react";
import { Empty, Result, Select, SelectProps, Spin } from "antd";
import { useFetchUsersAPIQuery } from "~/hooks/useUser";
import { PaginationRequestModel } from "~/models/requestModel";
import { UserListItemAPIModel, UserSpecificModel } from "~/models/userModel";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import styles from "./UserAjaxSelect.scss";

type Props = SelectProps<UserListItemAPIModel> & {};

const UserAjaxSelect = ({ ...props }: Props) => {
    const [deps, setDeps] = useState<
        UserSpecificModel & PaginationRequestModel & { display_name?: string }
    >({
        is_active: true,
        page: 1,
        pageSize: 1000,
        display_name: undefined,
    });
    const { data, isLoading } = useFetchUsersAPIQuery({ deps });

    const onScroll = (event: any) => {
        const target = event?.target;
        if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
            setDeps({
                ...deps,
                page: deps.page ?? 0 + 1,
            });
        }
    };

    const onSearch = useCustomDebouncedCallback((value: string) => {
        setDeps({
            ...deps,
            page: 1,
            display_name: value,
        });
    });

    const onBlur = () => {
        if (deps.display_name) {
            setDeps({
                ...deps,
                display_name: undefined,
            });
        }
    };

    return (
        <Spin size="small" spinning={isLoading}>
            <Select
                showSearch
                filterOption={false}
                placeholder="クリックして選択"
                notFoundContent={
                    <Result
                        icon={
                            <Empty
                                image={Empty.PRESENTED_IMAGE_SIMPLE}
                                description="データがありません"
                            />
                        }
                    />
                }
                {...props}
                onSearch={onSearch}
                onPopupScroll={onScroll}
                onBlur={onBlur}
                style={{ width: "100%", textAlign: "left" }}
                data-testid="user-ajax-select">
                {data?.results.map((user) => (
                    <Select.Option key={user.id} value={user.id}>
                        {user.display_name}
                    </Select.Option>
                ))}
            </Select>
        </Spin>
    );
};

export default UserAjaxSelect;
