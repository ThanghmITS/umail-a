import React from "react";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { renderWithQueryClient } from "~/test/utils";
import UserAjaxSelect from "../UserAjaxSelect";
import { renderHook } from "@testing-library/react-hooks";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

describe("UserAjaxSelect.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", () => {
        renderWithQueryClient(<UserAjaxSelect />);
        const selectElement = screen.getByTestId("user-ajax-select");
        expect(selectElement).toBeInTheDocument();
        const placeholderElement = screen.getByText(/クリックして選択/);
        expect(placeholderElement).toBeInTheDocument();
    });

    test("click test", async () => {
        renderWithQueryClient(<UserAjaxSelect />);
        const placeholderElement = screen.getByText(/クリックして選択/);
        await userEvent.click(placeholderElement);
        const optionElement = await screen.findByText(/example user 2/);
        expect(optionElement).toBeInTheDocument();
    });
});
