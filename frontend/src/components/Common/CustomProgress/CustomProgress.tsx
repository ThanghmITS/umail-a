import React from "react";
import { Progress } from "antd";
import {CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { primaryColor, dangerColor } from "~/utils/constants";
import styles from "./CustomProgress.scss";


type Props = {
  percentage: number;
  progressCompleted: boolean; // add args if needed
}

const CustomProgress = ({ percentage, progressCompleted}: Props) => {

  const statusProgress = (progressCompleted: boolean, percentage: number) => {
      if (progressCompleted && percentage === 100) {
        return 'success'
      }
      else if (!progressCompleted && percentage === 100) {
        return 'exception';
      }
      else
        return 'normal';
  };

  const formatProgress = (progressCompleted: boolean, percentage: any) => {
      if (progressCompleted && percentage === 100) {
        return <CheckOutlined style={{ color: primaryColor }}/>;
      }
      else if (!progressCompleted && percentage === 100) {
        return <CloseOutlined style={{ color: dangerColor }}/>;
      }
      else
        return `${percentage}%`;
  };

  const strokeColorProgress = (progressCompleted: boolean, percentage: number) => {
      if (!progressCompleted && percentage === 100) {
        return dangerColor;
      }
      else
        return primaryColor;
  };

  return (
      <div className={styles.customProgressBar}>
          <Progress type="circle" percent={percentage} status={statusProgress(progressCompleted, percentage)}
              format={(percentage) =>  formatProgress(progressCompleted, percentage)}
              strokeColor={strokeColorProgress(progressCompleted, percentage)}/>
      </div>
  )
}

export default CustomProgress;
