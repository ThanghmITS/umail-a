
import React, { ReactNode } from "react";
import { Col, Modal, ModalProps } from "antd";
import BoardBaseModal from "../../BoardCommon/BoardBaseModal/BoardBaseModal";

type Props = ModalProps & {
  
};

const NewProjectBoard = ({ ...props }: Props) => {
  return (
    <BoardBaseModal {...props}>
      <Col>Test</Col>
    </BoardBaseModal>
  );
};

export default NewProjectBoard;
