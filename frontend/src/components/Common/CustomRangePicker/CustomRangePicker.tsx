import React from "react";
import { DatePicker } from "antd";
import { RangePickerProps } from "antd/lib/date-picker";
import { pickerLocaleConfig } from "~/utils/constants";

const { RangePicker } = DatePicker;

type Props = RangePickerProps & {};

const CustomRangePicker = ({ ...props }: Props) => {
    return <RangePicker {...props} locale={pickerLocaleConfig} />;
};

export default CustomRangePicker;
