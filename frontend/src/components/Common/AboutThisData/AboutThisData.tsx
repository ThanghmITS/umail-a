import React from "react";
import styles from "./AboutThisData.scss";

export type AboutThisDataModel = {
    created_user?: string;
    created_time?: string;
    modified_user?: string;
    modified_time?: string;
};

type Props = {
    data: AboutThisDataModel;
};

const AboutThisData = ({
    data: { created_user, created_time, modified_user, modified_time },
}: Props) => {
    return (
        <div className={styles.container}>
            {created_user ? (
                <p className={styles.pStyle}>作成者: {created_user}</p>
            ) : (
                <br />
            )}
            <p className={styles.pStyle}>
                更新日時: {modified_time} 更新者: {modified_user}
            </p>
        </div>
    );
};

export default AboutThisData;
