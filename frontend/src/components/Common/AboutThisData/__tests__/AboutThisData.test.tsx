import React from "react";
import { render, screen } from "~/test/utils";
import moment from "moment";
import AboutThisData, { AboutThisDataModel } from "../AboutThisData";

const now = moment().format("YYYY-MM-DD HH:mm");
const mockAllData: AboutThisDataModel = {
    created_user: "example_created_user@example.com",
    created_time: now,
    modified_user: "example_modified_user@example.com",
    modified_time: now,
};

const mockModifiedData: AboutThisDataModel = {
    modified_user: "example_modified_user@example.com",
    modified_time: now,
};

describe("AboutThisData.tsx", () => {
    test("render test with all data", () => {
        render(<AboutThisData data={mockAllData} />);
        const createdUserLabelElement = screen.getByText(/作成者:/);
        expect(createdUserLabelElement).toBeInTheDocument();
        const createdUserElement = screen.getByText(
            /example_created_user@example.com/
        );
        expect(createdUserElement).toBeInTheDocument();
        const modifiedTimeLabelElement = screen.getByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = screen.getByText(new RegExp(now, "g"));
        expect(modifiedTimeElement).toBeInTheDocument();
        const modifiedUserLabelElement = screen.getByText(/更新者:/);
        expect(modifiedUserLabelElement).toBeInTheDocument();
        const modifiedUserElement = screen.getByText(
            /example_modified_user@example.com/
        );
        expect(modifiedUserElement).toBeInTheDocument();
    });
    test("render test with modified data", () => {
        render(<AboutThisData data={mockModifiedData} />);
        const createdUserLabelElement = screen.queryByText(/作成者:/);
        expect(createdUserLabelElement).not.toBeInTheDocument();
        const createdUserElement = screen.queryByText(
            /example_created_user@example.com/
        );
        expect(createdUserElement).not.toBeInTheDocument();
        const modifiedTimeLabelElement = screen.getByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = screen.getByText(new RegExp(now, "g"));
        expect(modifiedTimeElement).toBeInTheDocument();
        const modifiedUserLabelElement = screen.getByText(/更新者:/);
        expect(modifiedUserLabelElement).toBeInTheDocument();
        const modifiedUserElement = screen.getByText(
            /example_modified_user@example.com/
        );
        expect(modifiedUserElement).toBeInTheDocument();
    });
});
