import React, { ReactNode } from "react";

type Props = {
    children: ReactNode;
};

const PopoverMenuItem = ({ children }: Props) => (
    <div
        style={{
            margin: "8px auto",
        }}>
        {children}
    </div>
);

export default PopoverMenuItem;
