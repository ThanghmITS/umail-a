import React from "react";
import { DatePicker } from "antd";
import { MonthPickerProps } from "antd/lib/date-picker";
import { pickerLocaleConfig } from "~/utils/constants";

const { MonthPicker } = DatePicker;

type Props = MonthPickerProps & {};

const CustomMonthPicker = ({ ...props }: Props) => {
    return <MonthPicker {...props} locale={pickerLocaleConfig} />;
};

export default CustomMonthPicker;
