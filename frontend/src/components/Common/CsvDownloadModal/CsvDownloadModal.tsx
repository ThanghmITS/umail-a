import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row, Select } from "antd";
import { CsvDownloadFormModel } from "~/models/csvControlModel";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import { CSV_DOWNLOAD_LIMIT } from "~/utils/constants";
import { isCsvDownSuccess as isCsvDownSuccessAtom } from "~/recoil/atom";
import CustomProgress from "~/components/Common/CustomProgress/CustomProgress";
import { useRecoilValue } from "recoil";
import _ from "lodash";

const { Option } = Select;

type OptionModel = {
    start: number;
    end: number;
};

type Props = {
    totalCount: number;
    isModalOpen: boolean;
    onModalClose: () => void;
    onDownload: (downloadIndex: number) => void;
};

const CsvDownloadModal = ({
    totalCount,
    isModalOpen,
    onModalClose,
    onDownload,
}: Props) => {
    const [form] = Form.useForm<CsvDownloadFormModel>();
    const [downloadOptions, setDownloadOptions] = useState<OptionModel[]>([]);
    const [isProgressBar, setIsProgressBar] = useState(false);
    const [percentage, setPercentage] = useState(0);
    const isCsvDownSuccess = useRecoilValue(isCsvDownSuccessAtom);

    const calcDownloadRange = () => {
        const indexRange = Math.floor(totalCount / CSV_DOWNLOAD_LIMIT);
        const csvRangeIndexes = _.range(0, indexRange + 1);
        if (!csvRangeIndexes.length) {
            return [
                {
                    start: 1,
                    end: totalCount,
                },
            ];
        } else {
            // NOTE(joshua-hashimoto): .filterでundefinedを排除しようとしても、Typescriptがうまくそれを認知できなかった。時間がもったいないので、素直にforで回して値を作成
            let items: OptionModel[] = [];
            for (const csvRangeIndex of csvRangeIndexes) {
                const start = CSV_DOWNLOAD_LIMIT * csvRangeIndex + 1;
                let end = start + CSV_DOWNLOAD_LIMIT - 1;
                if (csvRangeIndex === indexRange) {
                    // 最後の範囲を設定
                    const lastCount =
                        totalCount - CSV_DOWNLOAD_LIMIT * indexRange;
                    // 最後の範囲が0だったら、そこは選択肢として必要ないのでcontinueで次のイテレーションへ -> それで完了
                    if (!lastCount) {
                        continue;
                    }
                    end = CSV_DOWNLOAD_LIMIT * indexRange + lastCount;
                }
                items.push({
                    start,
                    end,
                });
            }
            return items;
        }
    };

    var percentageValue = 0;

    const onFinish = ({ downloadRangeIndex }: CsvDownloadFormModel) => {
        processDownloadProgress({ downloadRangeIndex });
        form.setFieldsValue({
            downloadRangeIndex: undefined,
        });
        form.resetFields();
    };

    const processDownloadProgress = ({ downloadRangeIndex }: CsvDownloadFormModel) => {
      setIsProgressBar(true);
       setPercentage(0);
       percentageValue = 0;
       setTimeout(() => {
        let intervalID = setInterval(() => {
          if(percentageValue === 99) {
            onDownload(downloadRangeIndex);
            clearInterval(intervalID);
            setTimeout(() => {
              setPercentage(100);
            }, 500);
            setTimeout(() => {
              setIsProgressBar(false);
            }, 2000);
          } else {
            animate();
          }
        }, 10);
      }, 200);
    };

    useEffect(() => {
        setDownloadOptions(calcDownloadRange());
    }, [totalCount]);

    const animate = () => {
      percentageValue++;
      setPercentage(percentageValue);
    };

    return (
        <GenericModal
            visible={isModalOpen}
            title="ダウンロードする範囲を選択してください">
            {isProgressBar && <CustomProgress percentage={percentage} progressCompleted={isCsvDownSuccess} /> }
            <Col span={24}>
                <Form form={form} onFinish={onFinish}>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="center">
                                <Col span={12}>
                                    <Form.Item name="downloadRangeIndex">
                                        <Select>
                                            {downloadOptions.map(
                                                (downloadOption, index) => (
                                                    <Select.Option
                                                        key={index}
                                                        value={index}>
                                                        <span>
                                                            {
                                                                downloadOption.start
                                                            }
                                                            件目 ~{" "}
                                                            {downloadOption.end}
                                                            件目
                                                        </span>
                                                    </Select.Option>
                                                )
                                            )}
                                        </Select>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="end" gutter={6}>
                                <Col>
                                    <Button onClick={onModalClose}>
                                        キャンセル
                                    </Button>
                                </Col>
                                <Col>
                                    <Form.Item shouldUpdate noStyle>
                                        {() => (
                                            <Button
                                                type="primary"
                                                htmlType="submit"
                                                disabled={
                                                    !form.isFieldTouched(
                                                        "downloadRangeIndex"
                                                    ) ||
                                                    !!form
                                                        .getFieldsError()
                                                        .filter(
                                                            ({ errors }) =>
                                                                errors.length
                                                        ).length
                                                }>
                                                OK
                                            </Button>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                </Form>
            </Col>
        </GenericModal>
    );
};

export default CsvDownloadModal;
