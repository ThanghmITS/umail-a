import React from "react";

type Props = {
    height?: number;
    width?: number;
};

const Spacer = ({ height = 0, width = 0 }: Props) => {
    const spacerStyle = {
        height,
        width,
    };

    return <div style={spacerStyle}></div>;
};

export default Spacer;
