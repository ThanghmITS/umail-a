import React from "react";
import { Row, Col } from "antd";
import {
    dangerColor,
    helpTextColor,
    ONLY_HANKAKU_REGEX,
} from "~/utils/constants";
import "./ValidateTextArea.scss";

type Props = {
    contentTextArea: string | undefined;
    errorMessages: string;
    margin: string;
    className: string;
    isScheduledMails?: boolean;
    isCheckErrorTextArea?: boolean;
    extraErrorMessage?: string;
};

const ValidateTextArea = ({
    contentTextArea = "",
    errorMessages = "",
    margin = "",
    className = "",
    isScheduledMails = false,
    isCheckErrorTextArea = false,
    extraErrorMessage,
}: Props) => {
    let totalCharacter = 0;
    if (ONLY_HANKAKU_REGEX.test(contentTextArea)) {
        totalCharacter = 10000;
    } else {
        totalCharacter = 5000;
    }

    return (
        <>
            {isScheduledMails ? (
                <Col className={className} style={{ marginLeft: margin }}>
                    <Row
                        style={{
                            color: isCheckErrorTextArea
                                ? dangerColor
                                : helpTextColor,
                        }}>
                        {contentTextArea?.length > 0 && isCheckErrorTextArea
                            ? errorMessages
                            : ""}
                    </Row>
                    <Row
                        style={{
                            color: isCheckErrorTextArea
                                ? dangerColor
                                : helpTextColor,
                            display: "block",
                        }}>
                        {(contentTextArea?.length || 0) +
                            "/" +
                            totalCharacter +
                            "文字"}
                    </Row>
                </Col>
            ) : (
                <Col className={className} style={{ marginLeft: margin }}>
                    <Row
                        style={{
                            color:
                                contentTextArea?.length > 1000 ||
                                !!extraErrorMessage
                                    ? dangerColor
                                    : helpTextColor,
                        }}>
                        {!!extraErrorMessage
                            ? extraErrorMessage
                            : contentTextArea?.replace(/(\r\n|\n|\r)/gm, "")
                                  .length > 1000
                            ? errorMessages
                            : ""}
                    </Row>
                    <Row
                        style={{
                            color:
                                contentTextArea?.length > 1000 ||
                                !!extraErrorMessage
                                    ? dangerColor
                                    : helpTextColor,
                            display: "block",
                        }}>
                        {(contentTextArea?.replace(/(\r\n|\n|\r)/gm, "")
                            .length || 0) + "/1000文字"}
                    </Row>
                </Col>
            )}
        </>
    );
};

export default ValidateTextArea;
