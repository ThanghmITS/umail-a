import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ImageCropper from "../ImageCropper";

describe("ImageCropper.tsx", () => {
    const uploaderInputTestId = "image-cropper";
    const uploaderIconTestId = "thumbnail-uploader-icon";

    let file: File;

    beforeEach(() => {
        file = new File(["fasdfasdaf"], "random-image.png", {
            type: "image/png",
        });
    });

    test("render test", () => {
        render(<ImageCropper />);
        const uploaderTitleElement = screen.getByText(/^画像をアップロード$/);
        expect(uploaderTitleElement).toBeInTheDocument();
    });

    test("image set test", async () => {
        render(<ImageCropper />);
        const inputElement = screen.getByTestId(
            uploaderInputTestId
        ) as HTMLInputElement;
        expect(inputElement).not.toHaveValue();
        await userEvent.upload(inputElement, file);
        expect(inputElement).toHaveValue();
    });

    test("modal render test", async () => {
        render(<ImageCropper />);
        const inputElement = screen.getByTestId(
            uploaderInputTestId
        ) as HTMLInputElement;
        await userEvent.upload(inputElement, file);
        const modalTitleElement = await screen.findByText(/^画像をトリミング$/);
        expect(modalTitleElement).toBeInTheDocument();
        const cancelButtonElement = await screen.findByRole("button", {
            name: /^キャンセル$/,
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const okButtonElement = await screen.findByRole("button", {
            name: /^OK$/,
        });
        expect(okButtonElement).toBeInTheDocument();
        expect(okButtonElement).not.toBeDisabled();
    });
});
