import React from "react";
import { Tag, TagProps } from "antd";

type Props = TagProps & {};

const CustomSelectTag = ({ ...props }: Props) => {
    return (
        <Tag
            {...props}
            style={{
                marginTop: "3px",
                marginBottom: "3px",
                marginInlineEnd: "4.8px",
                paddingInlineStart: "8px",
                paddingInlineEnd: "4px",
                width: "100%",
                whiteSpace: "normal",
            }}>
            {props.title}
        </Tag>
    );
};

export default CustomSelectTag;
