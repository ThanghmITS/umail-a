import React, { useState } from "react";
import {
    Col,
    Form,
    FormItemProps,
    Row,
    Avatar,
    Button,
    UploadProps,
    Image,
} from "antd";
import { SmileFilled } from "@ant-design/icons";
import ImageCropper from "../ImageCropper/ImageCropper";
import ProfileThumbnailDeleteModal from "./ProfileThumbnailDeleteModal/ProfileThumbnailDeleteModal";
import { getBase64 } from "~/utils/utils";

const avatarSize = { xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 };

const uploadColumnSpan = 24;

type Props = FormItemProps & {
    thumbnailUrl?: string;
    onRemoveThumbnail: () => void;
    uploadProps?: UploadProps;
};

const ProfileThumbnailFormItem = ({
    thumbnailUrl,
    onRemoveThumbnail,
    uploadProps,
    ...props
}: Props) => {
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const [newFile, setNewFile] = useState<FileReader["result"] | undefined>(
        undefined
    );

    const normFile = (e: any) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    const getAvatar = () => {
        if (newFile) {
            return <Avatar size={avatarSize} src={newFile} />;
        }
        if (thumbnailUrl) {
            return (
                <Avatar
                    size={avatarSize}
                    src={
                        <Image
                            src={thumbnailUrl}
                            style={{ width: "100%" }}
                            preview={false}
                        />
                    }
                />
            );
        }
        return <Avatar size={avatarSize} icon={<SmileFilled />} />;
    };

    const renderAvatar = () => {
        const avatar = getAvatar();

        if (thumbnailUrl || newFile) {
            return (
                <>
                    <Row justify="center">
                        <Col>
                            <Row justify="center">{avatar}</Row>
                            
                        </Col>
                    </Row>
                    <ProfileThumbnailDeleteModal
                        isVisible={isDeleteModalVisible}
                        onOk={() => {
                            if (newFile) {
                                setNewFile(undefined);
                            } else {
                                onRemoveThumbnail();
                            }
                            setIsDeleteModalVisible(false);
                        }}
                        onCancel={() => setIsDeleteModalVisible(false)}
                    />
                </>
            );
        }
        return <Row justify="center">{avatar}</Row>;
    };

    return (
        <Form.Item
            label="プロフィール画像"
            name="avatar"
            getValueFromEvent={normFile}
            style={{ marginTop: "2%" }}
            {...props}>
            <Col span={24}>
                <Col span={uploadColumnSpan} style={{ marginTop: "5%" }}>{renderAvatar()}</Col>
                <Col span={uploadColumnSpan} style={{ margin: "20px 0" }}>
                    <Row justify="center">
                        <ImageCropper
                            {...uploadProps}
                            beforeUpload={(file, files) => {
                                getBase64(file, (result) => {
                                    setNewFile(result);
                                });
                                if (uploadProps?.beforeUpload) {
                                    return uploadProps?.beforeUpload(
                                        file,
                                        files
                                    );
                                }
                                return false;
                            }}
                        />
                    </Row>
                </Col>
                { thumbnailUrl || newFile ?  
                <Row justify="center">
                    <Button
                        danger
                        type="text"
                        onClick={() => {
                            setIsDeleteModalVisible(true);
                        }}>
                        画像を削除する
                    </Button>
                </Row>
                :
                ''
                }  
                </Col>
        </Form.Item>
    );
};

export default ProfileThumbnailFormItem;
