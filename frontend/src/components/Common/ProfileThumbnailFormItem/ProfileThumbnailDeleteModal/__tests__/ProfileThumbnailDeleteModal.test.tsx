import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ProfileThumbnailDeleteModal from "../ProfileThumbnailDeleteModal";

const mockOnOk = jest.fn();
const mockOnCancel = jest.fn();

describe("ProfileThumbnailDeleteModal.tsx", () => {
    test("render test", () => {
        render(
            <ProfileThumbnailDeleteModal
                isVisible
                onOk={mockOnOk}
                onCancel={mockOnCancel}
            />
        );
        const titleElement = screen.getByText(
            /^現在のプロフィール画像を削除しますか？$/
        );
        expect(titleElement).toBeInTheDocument();
        const contentElement = screen.getByText(
            /^OKを押すと削除が実行され、デフォルトのプロフィール画像が設定されます。$/
        );
        expect(contentElement).toBeInTheDocument();
        const cancelButtonElement = screen.getByRole("button", {
            name: /^キャンセル$/,
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const okButtonElement = screen.getByRole("button", { name: /^OK$/ });
        expect(okButtonElement).toBeInTheDocument();
        expect(okButtonElement).not.toBeDisabled();
    });

    test("test cancel button can be clicked", async () => {
        render(
            <ProfileThumbnailDeleteModal
                isVisible
                onOk={mockOnOk}
                onCancel={mockOnCancel}
            />
        );
        const buttonElement = screen.getByRole("button", {
            name: /^キャンセル$/,
        });
        await userEvent.click(buttonElement);
        expect(mockOnCancel).toHaveBeenCalled();
    });

    test("test ok button can be clicked", async () => {
        render(
            <ProfileThumbnailDeleteModal
                isVisible
                onOk={mockOnOk}
                onCancel={mockOnCancel}
            />
        );
        const buttonElement = screen.getByRole("button", {
            name: /^OK$/,
        });
        await userEvent.click(buttonElement);
        expect(mockOnOk).toHaveBeenCalled();
    });
});
