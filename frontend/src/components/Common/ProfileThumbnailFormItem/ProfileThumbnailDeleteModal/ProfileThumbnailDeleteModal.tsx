import React from "react";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import GenericModalContent from "~/components/Modals/GenericModal/GenericModalContent/GenericModalContent";
import { useDeleteThumbnailAPIMutation } from "~/hooks/useThumbnail";
import { ThumbnailDeleteRequestModel } from "~/models/thumbnailModel";

type Props = {
    isVisible: boolean;
    onOk: () => void;
    onCancel: () => void;
};

const ProfileThumbnailDeleteModal = ({ isVisible, onOk, onCancel }: Props) => {
    return (
        <GenericModal
            visible={isVisible}
            title="現在のプロフィール画像を削除しますか？"
            type="warning"
            width={416}
            onOk={() => {
                onOk();
            }}
            onCancel={onCancel}>
            <GenericModalContent>
                OKを押すと削除が実行され、デフォルトのプロフィール画像が設定されます。
            </GenericModalContent>
        </GenericModal>
    );
};

export default ProfileThumbnailDeleteModal;
