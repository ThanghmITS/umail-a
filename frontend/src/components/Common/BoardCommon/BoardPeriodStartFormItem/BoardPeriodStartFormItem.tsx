import React from "react";
import { Form, FormItemProps } from "antd";
import CustomDatePicker from "../../CustomDatePicker/CustomDatePicker";
import styles from "./BoardPeriodStartFormItem.scss";

type Props = FormItemProps & {};

const BoardPeriodStartFormItem = ({ ...props }: Props) => {
    return (
        <Form.Item
            name={["period", "start"]}
            {...props}
            label="開始"
            colon={false}>
            <CustomDatePicker className={styles.styleDatePicker} placeholder="日付を選択" />
        </Form.Item>
    );
};

export default BoardPeriodStartFormItem;
