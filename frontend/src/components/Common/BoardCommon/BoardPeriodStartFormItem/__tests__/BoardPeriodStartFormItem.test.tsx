import React from "react";
import { render, screen } from "@testing-library/react";
import BoardPeriodStartFormItem from "../BoardPeriodStartFormItem";

describe("BoardPeriodStartFormItem.tsx", () => {
    test("render test", () => {
        render(<BoardPeriodStartFormItem />);
        const labelElement = screen.getByText(/開始/);
        expect(labelElement).toBeInTheDocument();
        const placeholderElement = screen.getByPlaceholderText(/日付を選択/);
        expect(placeholderElement).toBeInTheDocument();
    });
});
