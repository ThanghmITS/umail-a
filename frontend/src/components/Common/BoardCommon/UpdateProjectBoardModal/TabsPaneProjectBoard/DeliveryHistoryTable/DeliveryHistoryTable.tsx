import { Table, Tag } from 'antd';
import type { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import moment from 'moment';
import React, { useState } from 'react';
import { useSearchScheduledMail } from '~/hooks/usePersonnelBoard';
import { ScheduledEmailSearchModel, ScheduledMailSearchType } from '~/models/scheduledEmailModel';

const formatDateTable = (date: string) => {
  return date ? moment(date).format('YYYY-MM-DD HH:mm') : '';
}

const columns: ColumnsType<ScheduledEmailSearchModel> = [
  {
    title: '配信完了日時',
    dataIndex: 'created_time',
    render: (_, record) => (
      <>{formatDateTable(record.created_time)}</>
    )
  },
  {
    title: '契約情報',
    dataIndex: 'text_format',
    render: (_, record) => (
      <Tag color={record.text_format === 'html' ? 'blue' : 'red'}>{record.text_format}</Tag>
    )
  },
  {
    title: '配信者',
    dataIndex: 'sender__name',
  },
  {
    title: '配信数',
    dataIndex: 'send_total_count',
  },
  {
    title: '開封数',
    dataIndex: 'open_count',
  },
  {
    title: '開封率',
    dataIndex: 'open_ratio',
  },
];

const DeliveryHistoryTable: React.FC = () => {

  const [params, setParams] = useState<ScheduledMailSearchType>({
    page: 1,
    page_size: 10,
    status: 'sent,error',
    send_type: '',
    text_format: '',
  });

  const { isLoading,listsScheduledMails } = useSearchScheduledMail(params);
  

  const handleTableChange = (newPagination: TablePaginationConfig) => {
    setParams({
      ...params,
      page:newPagination.current
    })
  };

  const pagination: TablePaginationConfig = {
    current: params.page,
    pageSize: params.page_size,
    total: listsScheduledMails?.count
  }

  return (
    <Table
      columns={columns}
      rowKey={record => record.id}
      dataSource={listsScheduledMails?.results}
      pagination={pagination}
      loading={isLoading}
      onChange={ handleTableChange}
    />
  );
};

export default DeliveryHistoryTable;
