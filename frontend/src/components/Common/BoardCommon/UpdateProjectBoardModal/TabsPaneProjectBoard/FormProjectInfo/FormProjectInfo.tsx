import { Button, Checkbox, Col, Form, Input, Radio, Row, Select } from "antd";
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import React, { CSSProperties, useMemo } from "react";
import DescriptionUserInfoFormItem from '~/components/Common/BoardCommon/DescriptionUserInfoFormItem/DescriptionUserInfoFormItem';
import SkillSelectFormItem from '~/components/Common/BoardCommon/SkillSelectFormItem/SkillSelectFormItem';
import BoardTrainStationSelectFormItem from "~/components/Common/BoardCommon/BoardTrainStationSelectFormItem/BoardTrainStationSelectFormItem";
import UploadDragger from '~/components/Common/BoardCommon/UploadDragger/UploadDragger';
import CustomDatePicker from '~/components/Common/CustomDatePicker/CustomDatePicker';
import { Endpoint } from '~/domain/api';
import { ProjectBoardDetailModel } from '~/models/projectBoardModel';
import { personnelBoardBasePath } from '~/networking/personnelBoard';
import { ErrorMessages } from '~/utils/constants';

const { TextArea } = Input;

const marginNone: CSSProperties = {
    marginBottom: 0
}

const marginFormStyle: CSSProperties = {
    marginBottom: 10
}

type Props = {
    initialData?: ProjectBoardDetailModel;
    onChangeBirthday: (_: any, dateString: string) => void;
    onChangeArchived: (e: CheckboxChangeEvent) => void;
    isArchived: boolean;
}
function FormProjectInfo({
    initialData,
    onChangeBirthday,
    onChangeArchived,
    isArchived
}: Props) {
    const descriptionUserInfoLimits = 5;
    const listAlphabet = "ABCDEFGHIKLMNOPQASTUVWXYZ".split("");
    const onTagAdd = () => {
    }
    
    const onFilePreviewImage = (file: UploadFile) => { }
    
    const onFileRemoveImage = (file: UploadFile) => { }

    const imageUploadURL = () =>`${Endpoint.getBaseUrl()}/${personnelBoardBasePath}/cards/${initialData?.id}`;
    
    return (
        <Col>
            <Form.Item
                label="要員名"
                style={marginNone}
            >
                <Row>
                    <Col span={8}>
                        <Form.Item
                            name="lastName"
                            style={marginFormStyle}
                            rules={[
                                ({ getFieldValue }) => ({
                                    validator: (_, value) => {
                                        const lastName = !!value ? value : null;

                                        const firstNameValue =
                                            getFieldValue("firstName");
                                        const firstName = !!firstNameValue
                                            ? firstNameValue
                                            : null;

                                        const fullName = lastName + firstName;

                                        // NOTE(joshua-hashimoto): 50文字制御
                                        if (fullName.length > 50) {
                                            return Promise.reject(
                                                new Error(
                                                    ErrorMessages.validation.length.max50
                                                )
                                            );
                                        }
                                        return Promise.resolve();
                                    },
                                }),
                            ]}
                        >
                            <Input placeholder="姓"/>
                        </Form.Item>
                    </Col>
                    <Col span={8} style={{ marginLeft: 10 }}>
                        <Form.Item name="firstName" style={marginFormStyle}>
                            <Input placeholder="名"/>
                        </Form.Item>
                    </Col>
                </Row>
            </Form.Item>
            <Form.Item
                label="要員イニシャル"
                style={marginNone}
            >
                <Row>
                    <Col span={8}>
                        <Form.Item
                            name="lastNameInitial"
                            style={marginFormStyle}
                            dependencies={['firstNameInitial']}
                            rules={[
                                ({ getFieldValue }) => ({
                                    validator: (_, value) => {
                                        const lastNameInitial = !!value ? value : null;

                                        const firstNameValue =
                                            getFieldValue("firstNameInitial");
                                        const firstNameInitial = !!firstNameValue
                                            ? firstNameValue
                                            : null;

                                        const fullName = lastNameInitial + firstNameInitial;

                                        // NOTE(joshua-hashimoto): 50文字制御
                                        if (fullName.length > 3) {
                                            return Promise.reject(
                                                new Error(
                                                    ErrorMessages.validation.length.max3
                                                )
                                            );
                                        }
                                        return Promise.resolve();
                                    },
                                }),
                            ]}
                        >
                            <Select placeholder="姓">
                                {listAlphabet.map(item => <Select.Option key={item} value={item} >{item}</Select.Option>)}
                            </Select>
                            {/* <Input placeholder="姓"/> */}
                        </Form.Item>
                    </Col>
                    <Col span={8} style={{ marginLeft: 10 }}>
                        <Form.Item name="firstNameInitial" style={marginFormStyle}>
                            <Select placeholder="名">
                                {listAlphabet.map(item => <Select.Option key={item} value={item}>{item}</Select.Option>)}
                            </Select>
                            {/* <Input placeholder="名"/> */}
                        </Form.Item>
                    </Col>
                </Row>
            </Form.Item>
            <Form.Item
                label="年齢"
                style={marginNone}
            >
                <Row>
                    <Col span={8}>
                        <Row justify="space-between" align="middle">
                            <Col span={22}>
                                <Form.Item
                                    name="age"
                                    style={marginFormStyle}
                                    rules={[
                                        ({}) => ({
                                            validator: (_, value) => {
                                                const age = !!value ? value : null;

                                                // NOTE(joshua-hashimoto): 50文字制御
                                                if (age.length > 3) {
                                                    return Promise.reject(
                                                        new Error(
                                                            ErrorMessages.validation.length.max3
                                                        )
                                                    );
                                                }
                                                return Promise.resolve();
                                            },
                                        }),
                                    ]}
                                >
                                    <Input
                                        type="number"
                                        placeholder="姓"
                                        disabled={isArchived ? true : false}
                                    />
                                </Form.Item>
                            </Col>
                            <p>歳</p>
                        </Row>
                    </Col>
                    <Col span={8} style={{ marginLeft: 10 }}>
                        <Row align="middle" justify="space-between">
                            <Col span={2}>
                                <Form.Item name="isArchived">
                                    <Checkbox onChange={onChangeArchived} checked={isArchived} />
                                </Form.Item>
                            </Col>
                            <Col span={22}>
                                <Form.Item name="birthday" label="生年月日">
                                    <CustomDatePicker
                                        format="YYYY-MM-DD"
                                        onChange={onChangeBirthday}
                                        disabled={isArchived ? false : true}
                                        placeholder="日付を選択"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Form.Item>
            <Form.Item
                label="性別"
                name="gender"
            >
                <Radio.Group>
                    <Radio value="male">男性</Radio>
                    <Radio value="female">女性</Radio>
                </Radio.Group>
            </Form.Item>
            <BoardTrainStationSelectFormItem />
            {/* <Form.Item
                label="最寄駅"
                wrapperCol={{ span: 6 }}
                name="trainStation"
            >
                <Input/>
            </Form.Item> */}
            <Form.Item
                label="所属"
                wrapperCol={{ span: 14 }}
                name="affiliation"
            >
                <Radio.Group>
                    <Radio value="proper">弊社プロパー</Radio>
                    <Radio value="freelancer"> 弊社フリーランス</Radio>
                    <Radio value="one_company_proper"> 1社先プロパー</Radio>
                    <Radio value="one_company_freelancer"> 1社先フリーランス</Radio>
                    <Radio value="two_company_proper"> 2社先プロパー</Radio>
                    <Radio value="two_company_freelancer"> 2社先フリーランス</Radio>
                    <Radio value="three_company_proper"> 3社先以上プロパー</Radio>
                    <Radio value="three_company_freelancer"> 社先以上フリーランス</Radio>
                </Radio.Group>
            </Form.Item>
            <SkillSelectFormItem
                defaults={[]}
                onTagAdd={onTagAdd}
            />
            <Form.Item
                label="稼働"
                name="operatePeriod"
                wrapperCol={{ span: 6 }}
            >
                <CustomDatePicker
                    style={{ width: '100%' }}
                    format="YYYY-MM-DD"
                    placeholder="日付を選択"
                />
            </Form.Item>
            <Form.Item
                label="並行"
                wrapperCol={{ span: 14 }}
                name="parallel"
            >
                <Radio.Group>
                    <Radio value="none">なし</Radio>
                    <Radio value="parallel"> あり</Radio>
                    <Radio value="parallel_one"> 1件</Radio>
                    <Radio value="parallel_two"> 2件</Radio>
                    <Radio value="parallel_three"> 3件以上</Radio>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                label="単金"
                style={marginNone}
                rules={[
                    {
                        max: 9,
                        message: ErrorMessages.validation.length.max9
                    }
                ]}
            >
                <Row>
                    <Col span={8}>
                        <Row justify="space-between" align="middle">
                            <Col span={20}>
                                <Form.Item style={marginFormStyle} name="price">
                                    <Input placeholder="姓"/>
                                </Form.Item>
                            </Col>
                            <p>万円</p>
                        </Row>
                    </Col>
                </Row>
            </Form.Item>
            <Form.Item
                label="希望"
                wrapperCol={{ span: 14 }}
                name="request"
            >
                <TextArea
                    placeholder="平日は17時まで可能です"
                    autoSize={{ minRows: 1 }}
                />
            </Form.Item>
            <Form.List name="dynamicRow">
                {(fields, { add, remove }) => {
                    return (
                        <>
                            {fields.map(
                                (field, index) => (
                                    <Col>
                                        <Col flex="auto">
                                            <DescriptionUserInfoFormItem
                                                {...field}
                                                key={field.key}
                                            />
                                            <Col style={{ margin:'0 10px 10px 0' }} span={18}>
                                                <Row justify="end">
                                                    {fields.length < descriptionUserInfoLimits && index === fields.length - 1 &&
                                                        <Button onClick={()=>add()} type="primary" style={{ marginRight: 10 }}>+</Button>
                                                    }
                                                    <Button 
                                                        onClick={
                                                            ()=>remove(field.name)
                                                        }
                                                        type="primary"
                                                        danger>
                                                        -
                                                    </Button>
                                                </Row>
                                            </Col>
                                        </Col>
                                    </Col>
                                )
                            )}
                            {!fields.length &&
                                <Row>
                                    <Button onClick={()=>add()} type="primary" style={{ marginLeft: '16%',marginBottom:20 }}>+</Button>
                                </Row>
                            }
                        </>
                    );
                }}
            </Form.List>
        </Col>
    )
}

export default FormProjectInfo;
