
import React, { CSSProperties, useState, useMemo } from "react";
import {
    Button,
    Checkbox,
    Col,
    Form,
    Input,
    Radio,
    Row,
    Typography,
    InputNumber,
} from "antd";
import { UploadFile } from 'antd/lib/upload/interface';
import DescriptionUserInfoFormItem from '~/components/Common/BoardCommon/DescriptionUserInfoFormItem/DescriptionUserInfoFormItem';
import SkillSelectFormItem from '~/components/Common/BoardCommon/SkillSelectFormItem/SkillSelectFormItem';
import BoardTrainStationSelectFormItem from "~/components/Common/BoardCommon/BoardTrainStationSelectFormItem/BoardTrainStationSelectFormItem";
import BoardSkillSheetFormItem from "~/components/Common/BoardCommon/BoardSkillSheetFormItem/BoardSkillSheetFormItem";
import UploadDragger from '~/components/Common/BoardCommon/UploadDragger/UploadDragger';
import CustomDatePicker from '~/components/Common/CustomDatePicker/CustomDatePicker';
import BoardPersonnelInitialNameInput from "~/components/Common/BoardCommon/BoardPersonnelInitialNameInput/BoardPersonnelInitialNameInput";
import { Endpoint } from '~/domain/api';
import { PersonnelBoardDetailModel } from '~/models/personnelBoardModel';
import { personnelBoardBasePath } from '~/networking/personnelBoard';
import {
    formatMoneyNumberString,
    parseMoneyNumberString,
} from "~/components/helpers";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import {
    ErrorMessages,
    RESTRICT_SPACE_REGEX,
    HANKAKU_NUMBER_REGEX,
} from "~/utils/constants";

const { TextArea } = Input;
const { Text } = Typography;

const marginNone: CSSProperties = {
        marginBottom: 0
}

const marginFormStyle: CSSProperties = {
        marginBottom: 10
}

type Props = {
        initialData?: PersonnelBoardDetailModel;
        onChangeBirthday: (_: any, dateString: string) => void;
}
function FormPersonnelInfo({
        initialData,
        onChangeBirthday
}: Props) {
        const descriptionUserInfoLimits = 5;
        const onTagAdd = () => {
        }
        
        const onFilePreviewImage = (file: UploadFile) => { }
        
        const onFileRemoveImage = (file: UploadFile) => { }

        const imageUploadURL = () =>`${Endpoint.getBaseUrl()}/${personnelBoardBasePath}/cards/${initialData?.id}`;
        const skillSheetUploadURL = () =>
            `${Endpoint.getBaseUrl()}/${personnelBoardBasePath}/cards/${
                initialData?.id
            }/skill-sheets`;


        const [isBirthday, setIsBirthday] = useState<boolean>(false);

        const defaultImage:UploadFile<any>[] = useMemo(()=> {
                if(initialData?.image) {
                        const url = initialData.image;
                        const nameSplit = initialData.image.split('/')
                        return [
                                {
                                        uid: initialData.image,
                                        name: nameSplit[nameSplit.length - 1],
                                        url: url,
                                },
                        ];

                }
           
                return []
        },[initialData])

        const onChangeBirthdayCheckBox = () => {
            setIsBirthday(!isBirthday);
        }
        
        return (
            <Col>
                <Form.Item label="要員名" style={marginNone}>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name="lastName"
                                dependencies={["firstName"]}
                                style={marginFormStyle}
                                rules={[
                                    ({ getFieldValue }) => ({
                                        validator: (_, value) => {
                                            const lastName = value || null;

                                            const firstName = getFieldValue("firstName") || null;

                                            const fullName = lastName + firstName;

                                            // NOTE(joshua-hashimoto): 50文字制御
                                            if (fullName.length > 50) {
                                                return Promise.reject(
                                                    new Error(ErrorMessages.validation.length.max50)
                                                );
                                            }
                                            return Promise.resolve();
                                        },
                                    }),
                                    {
                                        pattern: RESTRICT_SPACE_REGEX,
                                        message: ErrorMessages.validation.regex.space,
                                    },
                                ]}
                            >
                                <Input placeholder="姓" />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="firstName"
                                style={marginFormStyle}
                                rules={[
                                    {
                                        pattern: RESTRICT_SPACE_REGEX,
                                        message: ErrorMessages.validation.regex.space,
                                    },
                                ]}
                            >
                                <Input placeholder="名" />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item label="要員イニシャル" style={marginNone}>
                    <BoardPersonnelInitialNameInput
                        firstNameInputKey="firstNameInitial"
                        lastNameInputKey="lastNameInitial"
                        firstNamePlaceholder="名"
                        lastNamePlaceholder="姓"
                    />
                </Form.Item>
                <Form.Item label="年齢" style={marginNone}>
                    <Row gutter={10}>
                        <Col span={12}>
                            <Row justify="space-between" align="top">
                                <Col span={22}>
                                    <Form.Item
                                        name="age"
                                        style={marginFormStyle}
                                        rules={[
                                            ({}) => ({
                                                validator: (_, value) => {
                                                    const age = value || null;

                                                    // NOTE(joshua-hashimoto): 50文字制御
                                                    if (!!age && age.length > 3) {
                                                        return Promise.reject(
                                                            new Error(ErrorMessages.validation.length.max3)
                                                        );
                                                    }

                                                    return Promise.resolve();
                                                },
                                            }),
                                            {
                                                pattern: HANKAKU_NUMBER_REGEX,
                                                message:
                                                    ErrorMessages.validation.regex.onlyHankakuNumber,
                                            },
                                        ]}
                                    >
                                        <Input
                                            type="number"
                                            placeholder="姓"
                                            disabled={isBirthday ? true : false}
                                        />
                                    </Form.Item>
                                </Col>
                                <Text style={{ marginTop: 6 }}>歳</Text>
                            </Row>
                        </Col>
                        <Col span={12}>
                            <Row align="middle" justify="space-between">
                                <Col span={2}>
                                    <Form.Item name="isBirthday">
                                        <Checkbox
                                            onChange={onChangeBirthdayCheckBox}
                                            checked={isBirthday}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={22}>
                                    <Form.Item name="birthday" label="生年月日">
                                        <CustomDatePicker
                                            format="YYYY-MM-DD"
                                            onChange={onChangeBirthday}
                                            disabled={isBirthday ? false : true}
                                            placeholder="日付を選択"
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item label="性別" name="gender">
                    <Radio.Group>
                        <Radio value="male">男性</Radio>
                        <Radio value="female">女性</Radio>
                    </Radio.Group>
                </Form.Item>
                <BoardTrainStationSelectFormItem />
                <Form.Item label="所属" wrapperCol={{ span: 24 }} name="affiliation">
                    <Radio.Group>
                        <Radio value="proper">弊社プロパー</Radio>
                        <Radio value="freelancer"> 弊社フリーランス</Radio>
                        <Radio value="one_company_proper"> 1社先プロパー</Radio>
                        <Radio value="one_company_freelancer"> 1社先フリーランス</Radio>
                        <Radio value="two_company_proper"> 2社先プロパー</Radio>
                        <Radio value="two_company_freelancer"> 2社先フリーランス</Radio>
                        <Radio value="three_company_proper"> 3社先以上プロパー</Radio>
                        <Radio value="three_company_freelancer">
                            {" "}
                            3社先以上フリーランス
                        </Radio>
                    </Radio.Group>
                </Form.Item>
                <SkillSelectFormItem defaults={[]} onTagAdd={onTagAdd} />
                <Form.Item label="稼働" name="operatePeriod" wrapperCol={{ span: 10 }}>
                    <CustomDatePicker
                        style={{ width: "100%" }}
                        format="YYYY-MM-DD"
                        placeholder="日付を選択"
                    />
                </Form.Item>
                <Form.Item label="並行" wrapperCol={{ span: 24 }} name="parallel">
                    <Radio.Group>
                        <Radio value="none">なし</Radio>
                        <Radio value="parallel"> あり</Radio>
                        <Radio value="parallel_one"> 1件</Radio>
                        <Radio value="parallel_two"> 2件</Radio>
                        <Radio value="parallel_three"> 3件以上</Radio>
                    </Radio.Group>
                </Form.Item>
                <Form.Item label="単金" style={marginNone}>
                    <Row gutter={20}>
                        <Col span={12}>
                            <Row justify="space-between" align="top">
                                <Col span={16}>
                                    <Form.Item
                                        style={marginFormStyle}
                                        name="price"
                                        rules={[
                                            {
                                                type: "number",
                                                min: 1,
                                                message: "1以上の値を入力してください",
                                            },
                                            {
                                                pattern: HANKAKU_NUMBER_REGEX,
                                                message:
                                                    ErrorMessages.validation.regex.onlyHankakuNumber,
                                            },
                                            {
                                                validator: (_, value) => {
                                                    const convertedValue = String(value ?? "");
                                                    if (convertedValue.length > 9) {
                                                        return Promise.reject(
                                                            new Error(ErrorMessages.validation.length.max9)
                                                        );
                                                    }
                                                    return Promise.resolve();
                                                },
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            placeholder="姓"
                                            formatter={formatMoneyNumberString}
                                            parser={parseMoneyNumberString}
                                            step={100}
                                            style={{ width: "100%" }}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={8} style={{ padding: 6 }}>
                                    <Text>万円</Text>
                                </Col>
                            </Row>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                initialValue={initialData?.isNegotiable}
                                name="isNegotiable"
                                valuePropName="checked"
                            >
                                <Checkbox>応相談</Checkbox>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item
                    label="希望"
                    wrapperCol={{ span: 24 }}
                    name="request"
                    rules={[
                        {
                            max: 500,
                            message: ErrorMessages.validation.length.max500,
                        },
                    ]}
                >
                    <TextArea
                        placeholder="平日は17時まで可能です"
                        autoSize={{ minRows: 1 }}
                    />
                </Form.Item>
                <Form.List name="dynamicRows">
                    {(fields, { add, remove }) => {
                        return (
                            <>
                                {fields.map((field, index) => (
                                    <Col key={field.key}>
                                        <Col flex="auto">
                                            <DescriptionUserInfoFormItem {...field} key={field.key} />
                                            <Col style={{ margin: "0 10px 10px 0" }} span={24}>
                                                <Row justify="end">
                                                    {fields.length < descriptionUserInfoLimits &&
                                                        index === fields.length - 1 && (
                                                            <Button
                                                                onClick={() => add()}
                                                                type="primary"
                                                                style={{ marginRight: 10 }}
                                                            >
                                                                +
                                                            </Button>
                                                        )}
                                                    <Button
                                                        onClick={() => {
                                                            confirmModal({
                                                                title: "追加項目を削除しますか？",
                                                                okText: "追加項目を削除",
                                                                onOk: () => remove(field.name),
                                                                onCancel: () => {},
                                                            });
                                                            
                                                        }}
                                                        type="primary"
                                                        danger
                                                    >
                                                        -
                                                    </Button>
                                                </Row>
                                            </Col>
                                        </Col>
                                    </Col>
                                ))}
                                {!fields.length && (
                                    <Row>
                                        <Button
                                            onClick={() => add()}
                                            type="primary"
                                            style={{ marginLeft: "16%", marginBottom: 20 }}
                                        >
                                            +
                                        </Button>
                                    </Row>
                                )}
                            </>
                        );
                    }}
                </Form.List>
                <Form.Item label="人物写真" name="image" wrapperCol={{ span: 24 }}>
                    <UploadDragger
                        text="クリックまたはドラッグでファイルをアップロード"
                        name="image"
                        onFilePreview={onFilePreviewImage}
                        onFileRemove={onFileRemoveImage}
                        multiple={false}
                        maxCount={1}
                        action={imageUploadURL()}
                        defaultFileList={defaultImage}
                    />
                </Form.Item>
                <BoardSkillSheetFormItem
                    wrapperCol={{ span: 24 }}
                    action={skillSheetUploadURL()}
                />
            </Col>
        );
}

export default FormPersonnelInfo;

