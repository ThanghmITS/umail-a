import { Form } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { useForm } from 'antd/lib/form/Form';
import React, { ChangeEvent, useEffect } from 'react';
import BoardTabPanesForms from '~/components/Common/BoardCommon/BoardTabPanesForms/BoardTabPanesForms';
import { BoardChecklistModel, BoardCommentModel } from '~/models/boardCommonModel';
import { PersonnelBoardDetailModel } from '~/models/personnelBoardModel';
import BoardComment from '../../../BoardComment/BoardComment';

type Props = {
    children: React.ReactNode;
    valueTextAreaComment: string;
    onFinish: (values:PersonnelBoardDetailModel) => void;
    initialData?: PersonnelBoardDetailModel;
    dataComments: BoardCommentModel[];
    onChangeTextArea: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    onCreateComment: () => void;
    onPin: () => void;
    isImportant: boolean;
    onDeleteComment: (idComment: string) => () => void;
    onPinComment: (item: BoardCommentModel) => () => void;
    onSaveEditCommentContent: (item: BoardCommentModel) => void;
}
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 }, 
};
function TabsPanePersonnelInfo(props: Props) {
    const {
        onFinish,
        initialData,
        valueTextAreaComment,
        dataComments,
        onChangeTextArea,
        onCreateComment,
        onPin,
        isImportant,
        onDeleteComment,
        onPinComment,
        onSaveEditCommentContent,
        children
    } = props;

    const [form] = useForm<PersonnelBoardDetailModel>();

    useEffect(() => {
        if (initialData) {
            form.setFieldsValue(initialData)
        }
    }, [initialData, form])
    
    return (
        <Form
            {...layout}
            form={form}
            onFinish={onFinish}
        >
            <BoardTabPanesForms>
                <BoardComment
                    title="コメント一覧"
                    comments={dataComments}
                    value={valueTextAreaComment}
                    onChangeTextArea={onChangeTextArea}
                    onCreateComment={onCreateComment}
                    onPin={onPin}
                    isImportant={isImportant}
                    onPinComment={onPinComment}
                    onDeleteComment={onDeleteComment}
                    onSaveEditCommentContent={onSaveEditCommentContent}
                />
            </BoardTabPanesForms>
        </Form>

    )
}

export default TabsPanePersonnelInfo;

