import React, { CSSProperties, useState } from "react";
import { Checkbox, Form, FormInstance, Row } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { BoardScheduledEmailTemplateCheckboxModel } from "~/models/boardCommonModel";
import styles from "./BoardScheduledEmailTemplateSubCheckbox.scss";
import { PersonnelScheduledMailTemplateForm } from "~/models/personnelBoardModel";

const styleBottomCheckBox: CSSProperties = {
    marginBottom: 10,
};

type Props = {
    title: string;
    data: BoardScheduledEmailTemplateCheckboxModel;
    label: string;
    name: string;
    form: FormInstance<PersonnelScheduledMailTemplateForm>
    onCheckAllChange: (e: CheckboxChangeEvent) => void;
    onChangeCheckBox: (e: CheckboxChangeEvent) => void;
};

const BoardScheduledEmailTemplateSubCheckbox = ({
    title,
    data,
    name,
    onCheckAllChange,
    onChangeCheckBox
}: Props) => {
    
    return (
        <Form.Item>
            <Form.Item
                name={name}
                valuePropName="checked"
            >
                <Checkbox onChange={onCheckAllChange}>{title}</Checkbox>
            </Form.Item>
            <Form.Item
                label="配信職種詳細"
                labelCol={{
                    span: 6,
                }}>
                <Row>
                    {data?.selectedJobType?.map((jobType) => (
                        <Form.Item
                            name={jobType.name}
                            key={jobType.key}
                            valuePropName="checked"
                            style={styleBottomCheckBox}
                        >
                            <Checkbox
                                id={jobType.key}
                                onChange={onChangeCheckBox}
                            >
                                {jobType.title}
                            </Checkbox>
                        </Form.Item>
                    ))}
                </Row>
            </Form.Item>
            <Form.Item
                label={data?.selectedJobSkill ? "配信スキル詳細" : ''}
                labelCol={{
                    span: 6,
                }}>
                <Row>
                    {data?.selectedJobSkill?.map((jobSkill) => (
                        <Form.Item
                            name={jobSkill.name}
                            key={jobSkill.key}
                            valuePropName="checked"
                            style={styleBottomCheckBox}
                        >
                            <Checkbox id={jobSkill.key}>
                                {jobSkill.title}
                            </Checkbox>
                        </Form.Item>
                    ))}
                </Row>
            </Form.Item>
        </Form.Item>
    );
};

export default BoardScheduledEmailTemplateSubCheckbox;
