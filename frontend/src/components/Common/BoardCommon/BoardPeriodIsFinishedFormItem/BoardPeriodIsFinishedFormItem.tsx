import React, { useMemo } from "react";
import { Checkbox, Form, FormItemProps, Tooltip } from "antd";
import { PersonnelBoardDetailModel } from "~/models/personnelBoardModel";
import styles from "./BoardPeriodIsFinishedFormItem.scss";

type Props = FormItemProps & {
  personnelValues?: PersonnelBoardDetailModel;
};


const BoardPeriodIsFinishedFormItem = ({personnelValues, ...props }: Props) => {

    const endDate = useMemo(
      () => personnelValues?.period?.end,
      [personnelValues]
    );

    const isFinished = useMemo(
      () => personnelValues?.period?.isFinished,
      [personnelValues]
    );

    // return !isFinished ? renderCheckbox() : <Tooltip title="完了済">{renderCheckbox()}</Tooltip>;
    return (
      <Form.Item
        name={["period", "isFinished"]}
        {...props}
        valuePropName="checked"
        label={
          isFinished ? (
            <Tooltip title="完了済">
              <span>Finished</span>
            </Tooltip>
          ) : (
            "完了済"
          )
        }
      >
        <Checkbox disabled={!endDate} />
      </Form.Item>
    );
};

export default BoardPeriodIsFinishedFormItem;
