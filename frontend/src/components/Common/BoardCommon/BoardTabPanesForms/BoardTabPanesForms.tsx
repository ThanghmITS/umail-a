
import { PlusOutlined } from '@ant-design/icons';
import { Button, Col, Row } from 'antd';
import React from 'react';
import { BoardChecklistItemUpdateFormModel, BoardChecklistModel, BoardChecklistUpdateFormModel } from '~/models/boardCommonModel';
import {
    PersonnelBoardCopyCardFormModel, PersonnelBoardDetailModel
} from "~/models/personnelBoardModel";
import BoardChecklist from '../BoardChecklist/BoardChecklist';
import BoardComment from '../BoardComment/BoardComment';
import PersonnelBoardListForm from '../PersonnelBoardModal/PersonnelBoardListForm/PersonnelBoardListForm';
import ProjectBoardListForm from '../UpdateProjectBoardModal/ProjectBoardListForm/ProjectBoardListForm';
import styles from './BoardTabPanesForms.scss';

type Props = {
    typeCard: string;
    children: React.ReactNode;
    personnelValues?: PersonnelBoardDetailModel;
    dataCheckLists: BoardChecklistModel[];
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    onDelete: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
    onCreateCheckList: () => void;
    onDuplicateCard: (data: PersonnelBoardCopyCardFormModel) => void
    onDeleteCard: (cardId: string) => void
}


const BoardTabPanesForms = ({
    typeCard,
    personnelValues,
    children,
    dataCheckLists,
    onChecklistItemSubmit,
    onDelete,
    onAddItem,
    onSubmitFinishContent,
    onCreateCheckList,
    onDuplicateCard,
    onDeleteCard
}: Props) => {
    return (
        <Row justify="space-between" className={styles.container}>
            <Col span={16} >
                {children}
                <Col>
                    {dataCheckLists?.map((item) => (
                        <BoardChecklist
                            checklist={item}
                            onDelete={onDelete}
                            onChecklistItemSubmit={onChecklistItemSubmit}
                            onAddItem={onAddItem}
                            onSubmitFinishContent={onSubmitFinishContent}
                        />
                    ))}
                    <Row justify="end">
                        <Button
                            type="primary"
                            size="large"
                            icon={<PlusOutlined />}
                            onClick={onCreateCheckList}
                        />
                    </Row>
                </Col>
                <BoardComment/>
            </Col>
            <Col span={6}>
               {typeCard === "project" ? (
                    <ProjectBoardListForm />
                ) : (
                    <PersonnelBoardListForm
                        onDuplicateCard={onDuplicateCard}
                        onDeleteCard={onDeleteCard}
                        personnelValues={personnelValues}
                    />
                )}
            </Col>
        </Row>
    )
}

export default BoardTabPanesForms;

