import React, { useEffect } from "react";
import { Button, Col, Form, Row, Input } from "antd";
import BoardPopover from "~/components/Common/BoardCommon/BoardPopover/BoardPopover";
import {
  PersonnelBoardDetailModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import BoardPersonnelInitialNameInput from "~/components/Common/BoardCommon/BoardPersonnelInitialNameInput/BoardPersonnelInitialNameInput";
import PersonnelBoardListFormItem from "~/components/Common/BoardCommon/BoardListFormItem/PersonnelBoardListFormItem";
import styles from "./PersonnelBoardCopyCardPopover.scss";

type Props = {
  personnelValues?: PersonnelBoardDetailModel;
  onCoppy: (values: PersonnelBoardCopyCardFormModel) => void;
  onClose: () => void;
};

const PersonnelBoardCopyCardPopover = ({
  personnelValues,
  onCoppy,
  onClose,
}: Props) => {
  const [form] = Form.useForm<PersonnelBoardCopyCardFormModel>();

  useEffect(() => {
    onSetCardDefault();
  }, []);

  const onSetCardDefault = () => {
    form.setFieldsValue({
      cardId: personnelValues?.id,
      listId: personnelValues?.listId,
      lastNameInitial: personnelValues?.lastNameInitial,
      firstNameInitial: personnelValues?.firstNameInitial,
    });
  }

  const onCreateCard = (values: PersonnelBoardCopyCardFormModel) => {
    onCoppy(values);
    onSetCardDefault();
  };

  return (
    <BoardPopover title="要員をコピー" onClose={onClose}>
      <Row justify="center" style={{ width: "100%" }}>
        <Form form={form} onFinish={onCreateCard} className={styles.form}>
          <BoardPersonnelInitialNameInput
            firstNameInputKey="firstNameInitial"
            lastNameInputKey="lastNameInitial"
            firstNamePlaceholder="名"
            lastNamePlaceholder="姓"
          />
          <Row>
            <PersonnelBoardListFormItem label="" />
          </Row>
          <Row>
            <Col span={24}>
              <Button
                style={{
                  width: "100%",
                  marginTop: 10,
                  marginBottom: 10,
                }}
                type="primary"
                htmlType="submit"
              >
                コピー
              </Button>
            </Col>
          </Row>
          <Form.Item name="cardId" hidden>
            <Input />
          </Form.Item>
        </Form>
      </Row>
    </BoardPopover>
  );
};

export default PersonnelBoardCopyCardPopover;
