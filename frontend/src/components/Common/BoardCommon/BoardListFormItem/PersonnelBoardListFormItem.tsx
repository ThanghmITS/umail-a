import React from "react";
import { FormItemProps } from "antd";
import BoardListFormItem from "./BoardListFormItem";
import { usePersonnelBoardFetchListsAPIQuery } from "~/hooks/usePersonnelBoard";
import styles from "./BoardListFormItem.scss";

type Props = FormItemProps & {
    label?: string
};

const PersonnelBoardListFormItem = ({ label, ...props }: Props) => {
    const { data, isLoading } = usePersonnelBoardFetchListsAPIQuery({});

    return (
      <BoardListFormItem
        {...props}
        options={data}
        isLoading={isLoading}
        label={label}
      />
    );
};

export default PersonnelBoardListFormItem;
