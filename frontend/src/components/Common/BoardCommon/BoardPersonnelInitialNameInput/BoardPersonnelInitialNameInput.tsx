import React from "react";
import { Col, Form, Row, Select, Result, Empty } from "antd";
import styles from "./BoardPersonnelInitialNameInput.scss";
import { ErrorMessages } from "~/utils/constants";

type Props = {
    firstNameInputKey?: string;
    lastNameInputKey?: string;
    firstNamePlaceholder?: string;
    lastNamePlaceholder?: string;
};

const BoardPersonnelInitialNameInput = ({
    firstNameInputKey = 'first_name_initial',
    lastNameInputKey = 'last_name_initial',
    firstNamePlaceholder = "要員イニシャル(名)",
    lastNamePlaceholder = "要員イニシャル(姓)",
}: Props) => {
    const listAlphabet = "ABCDEFJGHIKLMNOPQRSTUVWXYZ".split("");

    return (
      <Row justify="space-between" align="middle">
        <Col className={styles.colInput}>
          <Form.Item
            name={lastNameInputKey}
            dependencies={[firstNameInputKey]}
            rules={[
              ({ getFieldValue }) => ({
                validator: (_, value) => {
                  const lastName = value || null;

                  const firstName = getFieldValue(firstNameInputKey) || null;

                  const fullName = lastName + firstName;

                  // NOTE(joshua-hashimoto): 必須項目制御
                  if (!firstName || !lastName || !fullName) {
                    return Promise.reject(
                      new Error(ErrorMessages.form.required)
                    );
                  }

                  // NOTE(joshua-hashimoto): 50文字制御
                  if (fullName.length > 3) {
                    return Promise.reject(
                      new Error(ErrorMessages.validation.length.max3)
                    );
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={lastNamePlaceholder}
              notFoundContent={
                <Result
                  icon={
                    <Empty
                      image={Empty.PRESENTED_IMAGE_SIMPLE}
                      description="データがありません"
                    />
                  }
                />
              }
              showSearch
            >
              {listAlphabet.map((item) => (
                <Select.Option key={item} value={item}>
                  {item}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col className={styles.colInput}>
          <Form.Item name={firstNameInputKey}>
            <Select
              placeholder={firstNamePlaceholder}
              notFoundContent={
                <Result
                  icon={
                    <Empty
                      image={Empty.PRESENTED_IMAGE_SIMPLE}
                      description="データがありません"
                    />
                  }
                />
              }
              showSearch
            >
              {listAlphabet.map((item) => (
                <Select.Option key={item} value={item}>
                  {item}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    );
};

export default BoardPersonnelInitialNameInput;
