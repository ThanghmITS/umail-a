import React, { useMemo } from "react";
import { Form, FormItemProps } from "antd";

import { usePersonnelBoardDetailCard } from "~/hooks/usePersonnelBoard";

import {
  PersonnelBoardSkillSheetModel,
} from "~/models/personnelBoardModel";

import { UploadFile } from "antd/lib/upload/interface";
import UploadDragger from "~/components/Common/BoardCommon/UploadDragger/UploadDragger";

type Props = FormItemProps & {
  action?: string;
};

const BoardSkillSheetFormItem = ({ action, ...props }: Props) => {
  const { skillSheets, personnelBoardDeleteSkillSheet } =
    usePersonnelBoardDetailCard();

  const defaultFiles = useMemo(()=> {
     const _initData: any = [];
     
     if (!!skillSheets) {
       skillSheets?.map((item: PersonnelBoardSkillSheetModel) => {
         _initData.push({
           ...item,
           uid: item.id,
           status: "done",
           url: item.file,
           name: item.name,
         });
       });
     }
     return _initData
  }, [skillSheets])

  const onFilePreviewImage = (file: UploadFile) => {};

  const onFileRemoveImage = (file: UploadFile | any) => {
    const fileID = file?.id || file?.response?.id
    personnelBoardDeleteSkillSheet(fileID);   
  };

  return (
      <Form.Item
        label="スキルシート"
        wrapperCol={{ span: 14 }}
        name="skillSheet"
        {...props}
      >
        <UploadDragger
          text="クリックまたはドラッグでファイルをアップロード"
          name="file"
          onFilePreview={onFilePreviewImage}
          onFileRemove={onFileRemoveImage}
          multiple={false}
          maxCount={10}
          action={action}
          defaultFileList={defaultFiles}
        />
      </Form.Item>
  );
};

export default BoardSkillSheetFormItem;
