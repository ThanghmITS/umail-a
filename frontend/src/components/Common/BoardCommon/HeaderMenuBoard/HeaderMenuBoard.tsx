import React, { CSSProperties } from "react";
import { CloseOutlined, LeftOutlined } from "@ant-design/icons";
import { Col, Row } from "antd";
import Text from "antd/lib/typography/Text";
import styles from "./HeaderMenuBoard.scss";

type Props = {
  title?: string;
  onClose?: () => void;
  className?: string | undefined;
  onBack?: () => void;
};
function HeaderMenuBoard({ title, onClose, onBack, className }: Props) {
  const cursorIcon: CSSProperties = {
    cursor: "pointer",
  };

  return (
    <Row
      justify="end"
      align="middle"
      className={className ?? styles.styleMenuBoard}
    >
      {onBack && <LeftOutlined style={cursorIcon} onClick={onBack} />}

      <Col span={22}>
        <Row justify="center">
          <Text>{title}</Text>
        </Row>
      </Col>

      <CloseOutlined style={cursorIcon} onClick={onClose} />
    </Row>
  );
}

export default HeaderMenuBoard;
