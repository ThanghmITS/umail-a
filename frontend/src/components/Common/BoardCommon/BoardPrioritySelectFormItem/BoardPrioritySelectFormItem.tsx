import React, { useState } from "react";
import { Form, FormItemProps, Select } from "antd";
import { BOARD_PRIORITY_OPTIONS } from "~/utils/constants";
import styles from "./BoardPrioritySelectFormItem.scss";

const { Option } = Select;

type Props = FormItemProps & {};

const BoardPrioritySelectFormItem = ({ ...props }: Props) => {
  const [colorSelected, setColorSelected] = useState<string>("");

  const onchangeSelect = (value: string) => {
      switch (value) {
        case 'urgent':
            setColorSelected("#EB5A46");
          break;
        case 'important':
            setColorSelected("#FFAB4A");
          break;
        case 'high':
            setColorSelected("#F2D600");
          break;
        case 'medium':
            setColorSelected("#61BD4F");
          break;
        default:
            setColorSelected("#0079BF");
          break;
      }
  };

    return (
        <Form.Item
            name={["priority", "value"]}
            {...props}
            label="優先度"
            colon={false}>
            <Select
                onChange={onchangeSelect}
                data-testid="board-priority-select"
                style={{
                  color: colorSelected
                }}
                allowClear
                placeholder="クリックして選択">
                {BOARD_PRIORITY_OPTIONS.map((priority, index) => (
                    <Option key={priority.value} value={priority.value} style={{background: priority.color, color: "white"}}>
                        {priority.title}
                    </Option>
                ))}
            </Select>
        </Form.Item>
    );
};

export default BoardPrioritySelectFormItem;
