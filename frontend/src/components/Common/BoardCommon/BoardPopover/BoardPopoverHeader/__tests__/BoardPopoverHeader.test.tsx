import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import BoardPopoverHeader from "../BoardPopoverHeader";

const mockOnClose = jest.fn();

describe("BoardPopoverHeader.tsx", () => {
    const closeButtonTestId = "close-button";

    test("render test", () => {
        render(
            <BoardPopoverHeader title="Header Title" onClose={mockOnClose} />
        );
        const titleElement = screen.getByText(/Header Title/);
        expect(titleElement).toBeInTheDocument();
        const closeButtonElement = screen.getByTestId(closeButtonTestId);
        expect(closeButtonElement).toBeInTheDocument();
    });

    test("onClose test", async () => {
        render(
            <BoardPopoverHeader title="Header Title" onClose={mockOnClose} />
        );
        const closeButtonElement = screen.getByTestId(closeButtonTestId);
        await userEvent.click(closeButtonElement);
        expect(mockOnClose).toHaveBeenCalled();
    });
});
