import React from "react";
import { Col, Row, Typography } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import styles from "./BoardPopoverHeader.scss";

const { Text } = Typography;

type Props = {
    title: string;
    onClose: () => void;
};

const BoardPopoverHeader = ({ title, onClose }: Props) => {
    return (
        <Col span={24}>
            <Row justify="space-between" align="middle" style={{ padding: 10 }}>
                <Col span={22}>
                    <Text>{title}</Text>
                </Col>
                <Col span={2}>
                    <CloseOutlined
                        onClick={onClose}
                        style={{ cursor: "pointer" }}
                        data-testid="close-button"
                    />
                </Col>
            </Row>
        </Col>
    );
};

export default BoardPopoverHeader;
