import React, { ReactNode } from "react";
import { Col, Modal, ModalProps } from "antd";
import styles from "./BoardBaseModal.scss";

type Props = ModalProps & {
    children: ReactNode;
    isArchived?: boolean
};

const BoardBaseModal = ({ children, isArchived, ...props }: Props) => {
    return (
        <Modal
            centered
            width={"70vw"}
            bodyStyle={{ backgroundColor: isArchived ? "#efefef" : "" }}
            {...props}
        >
            <Col
                style={{
                    height: "70vh",
                }}>
                {children}
            </Col>
        </Modal>
    );
};

export default BoardBaseModal;
