import React, { useMemo } from "react";
import { Form, FormItemProps } from "antd";
import CustomDatePicker from "../../CustomDatePicker/CustomDatePicker";
import { PersonnelBoardDetailModel } from "~/models/personnelBoardModel";
import moment, { Moment } from "moment";
import styles from "./BoardPeriodEndFormItem.scss";

type Props = FormItemProps & {
  personnelValues?: PersonnelBoardDetailModel;
};

const BoardPeriodEndFormItem = ({ personnelValues, ...props }: Props) => {
    const startDate = useMemo(
      () => personnelValues?.period?.start,
      [personnelValues]
    );

    const isFinished = useMemo(
      () => personnelValues?.period?.isFinished && !!personnelValues?.period?.end,
      [personnelValues]
    );

    const _status = useMemo(() => {
        if (!personnelValues?.period) {
            return ;
        }
        const { end, isFinished } = personnelValues?.period;
        if(isFinished || !end) {
            return;
        }
        if(moment(end).isSame(moment(), 'd')) {
            return 'warning'
        }
        if(moment(end).isBefore(moment())) {
            return 'error'
        }
    }, [personnelValues]);

    const disabledDate = (d: Moment) => {
        return !d || !!startDate && d.isBefore(moment(startDate).format('YYYY-MM-DD'));
      };
    
    return (
      <Form.Item
        name={["period", "end"]}
        {...props}
        label="期限"
        colon={false}
        validateStatus={isFinished ? "success" : _status}
        help={_status==='error' ? "期限切れ" : ""}
      >
        <CustomDatePicker
          className={`${styles.styleDatePicker} ${
            !isFinished ? "" : styles.finished
          }`}
          placeholder="日付を選択"
          disabledDate={disabledDate}
          status={_status}
        />
      </Form.Item>
    );
};

export default BoardPeriodEndFormItem;
