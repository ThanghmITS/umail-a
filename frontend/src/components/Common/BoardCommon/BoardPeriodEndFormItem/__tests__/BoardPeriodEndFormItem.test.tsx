import React from "react";
import { screen } from "@testing-library/react";
import { renderWithQueryClient } from "~/test/utils";
import BoardPeriodEndFormItem from "../BoardPeriodEndFormItem";

describe("BoardPeriodEndFormItem.tsx", () => {
    test("render test", () => {
        renderWithQueryClient(<BoardPeriodEndFormItem />);
        const labelElement = screen.getByText(/期限/);
        expect(labelElement).toBeInTheDocument();
        const placeholderElement = screen.getByPlaceholderText(/日付を選択/);
        expect(placeholderElement).toBeInTheDocument();
    });
});
