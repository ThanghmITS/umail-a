import { InboxOutlined } from '@ant-design/icons'
import { Upload } from 'antd'
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { RootState } from '~/models/store';
import { customErrorMessage, customSuccessMessage } from '../../AlertMessage/AlertMessage';

type Props = {
    name: string;
    multiple: boolean;
    action?: string;
    onFilePreview: (file: UploadFile) => void;
    onFileRemove: (file: UploadFile) => void;
    defaultFileList: UploadFile<any>[];
    text: string;
    maxCount?: number
};

function UploadDragger({
    name,
    multiple = true,
    action,
    onFilePreview,
    onFileRemove,
    defaultFileList,
    text,
    maxCount
}: Props) {
    const [uploadedFileList, setUploadedFileList] = useState<UploadFile<any>[]>([]);

    useEffect(() => {
        if(defaultFileList) setUploadedFileList(defaultFileList);
    }, [defaultFileList]);
    
    const { token } = useSelector(
        (state: RootState) => state.login
    );
    const onFileChange = (uploadInfo: UploadChangeParam<UploadFile<any>>) => {
        const { status } = uploadInfo.file;
        const files = [...uploadInfo.fileList];
        if (status !== "uploading") {
            console.info(uploadInfo.file, uploadInfo.fileList);
        }
        if (status === "done") {
            customSuccessMessage(`${uploadInfo.file.name} ファイルがアップロードされました`);
        } else if (status === "error") {
            if (
                "response" in uploadInfo.file &&
                "detail" in uploadInfo.file.response
            ) {
                customErrorMessage(uploadInfo.file.response.detail);
            } else {
                customErrorMessage(
                  `${uploadInfo.file.name} アップロードに失敗しました`
                );
            }
        }
        const filteredFiles = files.filter((file) => file.status !== "error");
        setUploadedFileList(filteredFiles);
    };
    return (
        <Upload.Dragger
            name={name}
            multiple={multiple}
            action={action}
            fileList={uploadedFileList}
            headers={{ Authorization: `Token ${token}` }}
            onPreview={onFilePreview}
            onRemove={onFileRemove}
            onChange={onFileChange}
            defaultFileList={defaultFileList}
            method="PATCH"
            maxCount={maxCount}
        >
            <p className="ant-upload-drag-icon">
                <InboxOutlined />
            </p>
            <p className="ant-upload-text">{text}</p>
        </Upload.Dragger>
    )
}

export default UploadDragger;

