import React from "react";
import { PaginationConfig } from "antd/lib/pagination";
import CustomPagination from "~/components/Common/CustomPagination/CustomPagination";
import { PaginationRequestModel } from "~/models/requestModel";
import styles from "./BoardScheduledEmailHistoryPagination.scss";

type Props = PaginationConfig & {};

const BoardScheduledEmailHistoryPagination = ({ ...props }: Props) => {
    return <CustomPagination {...props} />;
};

export default BoardScheduledEmailHistoryPagination;
