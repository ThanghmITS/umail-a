import { Col, Row } from "antd";
import React, { useState } from "react";
import { BoardScheduledEmailHistoryModel } from "~/models/boardCommonModel";
import { PaginationRequestModel } from "~/models/requestModel";
import { DEFAULT_PAGE_SIZE } from "~/utils/constants";
import BoardScheduledEmailHistoryPagination from "./BoardScheduledEmailHistoryPagination/BoardScheduledEmailHistoryPagination";
import BoardScheduledEmailHistoryTable from "./BoardScheduledEmailHistoryTable/BoardScheduledEmailHistoryTable";
import styles from "./BoardScheduledEmailHistoryPane.scss";

type Props = {
    data?: BoardScheduledEmailHistoryModel[];
    total?: number;
    isLoading: boolean;
};

const BoardScheduledEmailHistoryPane = ({ data, total, isLoading }: Props) => {
    const [deps, setDeps] = useState<PaginationRequestModel>({
        page: 1,
        pageSize: DEFAULT_PAGE_SIZE,
        value: undefined,
    });

    return (
        <Col span={24}>
            <Row style={{ marginTop: "1%", marginBottom: "1%" }}>
                <Col span={24}>
                    <BoardScheduledEmailHistoryTable
                        data={data ?? []}
                        loading={isLoading}
                    />
                </Col>
            </Row>
            <Row justify="end">
                <Col>
                    <BoardScheduledEmailHistoryPagination
                        current={deps.page}
                        total={total ?? 0}
                        defaultCurrent={deps.page}
                        onChange={(selectedPage, _) => {
                            setDeps({
                                ...deps,
                                page: selectedPage,
                            });
                        }}
                    />
                </Col>
            </Row>
        </Col>
    );
};

export default BoardScheduledEmailHistoryPane;
