import {
    CloseOutlined,
    DeleteOutlined,
    EyeInvisibleOutlined,
    EyeOutlined
} from "@ant-design/icons";
import { Button, Col, Dropdown, Form, Input, Menu, Progress, Row } from "antd";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import {
    BoardChecklistItemUpdateFormModel,
    BoardChecklistModel,
    BoardChecklistUpdateFormModel
} from "~/models/boardCommonModel";
import { personnelBoard as personnelBoardAtom } from "~/recoil/atom";
import HeaderMenuBoard from "../HeaderMenuBoard/HeaderMenuBoard";
import BoardChecklistItem from "./BoardChecklistItem/BoardChecklistItem";

type Props = {
    checklist: BoardChecklistModel;
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    onDelete: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
};

const { TextArea } = Input;

const BoardChecklist = ({
    checklist,
    onChecklistItemSubmit,
    onDelete,
    onAddItem,
    onSubmitFinishContent
}: Props) => {
    const [form] = Form.useForm<BoardChecklistUpdateFormModel>();
    const [isPopupDelete, setIsPopupDelete] = useState<boolean>(false);
    const [personnelBoardState, setPersonnelBoardState] = useRecoilState(personnelBoardAtom);
    const [valueCheck, setValueCheck] = useState<string>('');

    /**
     * TODO:
     *
     * [Checklist]
     * try to use react-query as state management.
     * react-query does not need to call an API. Instead it needs an asynchronous function to call.
     * One idea is the following;
     * 1. call CardDetail API to get the "original" checklists.
     * 2. put that to the react-query use(Custom)Query with the same key as the API call method (usePersonnel(Project)BoardFetchChecklistAPIQuery)
     * 3. call the new "local" version APIQuery method here and get the checklists
     * 4. pass that to render the checklist
     * 5. after creating a new checklist item or updating an item, update the "local" APIQuery state
     *
     *
     * [Checklist Item]
     * because checklist items are fully created by calling an API, this process must be as "non-call" as possible as to not call the API too many times.
     * For this reason I suggest to call the create API only when the "content" property of the Checklist item is not empty.
     * When onSubmit is called, check the following;
     *
     * - id
     * - content
     *
     * if the content is not empty and id does not exist, call the create API.
     * if the content is not empty and the id exist, call the update API.
     * if the content is empty and the id exist, call the delete API.
     * if the content is empty and id does not exist, just remove the local object and do not call any APIs.
     *
     * Maybe pass the checklist from react-query so all we need to do is update the state of the react-query.
     *
     */


    const calcPercent = (): number => {
        const checklistItemCount = checklist.items.length;
        const isFinishedItemCount = checklist.items.filter(
            (item) => item.isFinished
        ).length;
        const percentage = (isFinishedItemCount / checklistItemCount) * 100;
        return Math.round(percentage);
    };

    const onAdd = () => {
        setPersonnelBoardState({
            ...personnelBoardState,
            isCreateCheckItem: true,
            checkListId: checklist.id
        })
    };

    const onChangeValue = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setValueCheck(event.target.value)
    }

    const renderAddChecklistButton = () => {
        const { isCreateCheckItem , checkListId } = personnelBoardState;
        if (isCreateCheckItem && checkListId === checklist.id) {
            return ( 
                <Col span={24}>
                <TextArea
                    autoSize={{ minRows: 3 }}
                    placeholder="アイテムを追加"
                    onChange={onChangeValue}
                    value={valueCheck}
                />
                <Row style={{ marginTop: 10 }}>
                    <Button
                    disabled={!valueCheck ? true : false}
                    onClick={onAddItem(valueCheck, checklist.id)}
                    type="primary"
                    >
                    追加
                    </Button>
                    <Button
                    type="text"
                    icon={<CloseOutlined />}
                    onClick={() => {
                        setPersonnelBoardState({
                        ...personnelBoardState,
                        isCreateCheckItem: false,
                        });
                    }}
                    />
                </Row>
                </Col>
            );
        }
        return (
        <Button type="primary" size="small" onClick={onAdd}>
            アイテムを追加
        </Button>
        );
    };

    useEffect(() => {
        form.setFieldsValue(checklist);
    }, [checklist]);

    const onClose = () => {
        setIsPopupDelete(false)
    }

    const onDeleteCheckList = () => {
        setIsPopupDelete(false);
        onDelete(checklist.id)
    }

    const onSubmit = (values: BoardChecklistItemUpdateFormModel) => {
        onChecklistItemSubmit(values, checklist.id);
    }
    const renderPopupDelete = () => (
        <Menu>
            <Col style={{ width: 320 }}>
                <HeaderMenuBoard
                    title="チェックリストを削除しますか？"
                    onClose={onClose}
                />
                <Row justify="center">
                    <Button
                        type="primary"
                        style={{
                            width: 280,
                            marginBottom: 10
                        }}
                        onClick={onDeleteCheckList}
                        danger
                    >
                        チェックリストを削除
                    </Button>
                </Row>
            </Col>
        </Menu>
    )

    const onFinishTitle = (values: BoardChecklistUpdateFormModel) => {
        onSubmitFinishContent(values, checklist.id);
    }

    const onEye = () => {
        const showFinished =form.getFieldValue(
            "showFinished"
        );
        const title = form.getFieldValue(
            "title"
        )
        onFinishTitle({
            title,
            showFinished:!showFinished
        })
    }

    return (
        <Col span={24} style={{ marginBottom: 25 }}>
            <Col span={24}>
                <Form form={form} onFinish={onFinishTitle}>
                    <Row justify="space-between">
                        <Col>
                            <Form.Item name="title">
                                <Input
                                    bordered={false}
                                    style={{ fontSize: 18, fontWeight: 800 }}
                                    data-testid="title-input"
                                    onPressEnter={form.submit}
                                />
                            </Form.Item>
                        </Col>
                        <Col>
                            <Row gutter={6}>
                                <Col>
                                    <Form.Item name="showFinished">
                                        <Form.Item shouldUpdate noStyle>
                                            {({
                                                getFieldValue
                                            }) => {
                                                const showFinished =
                                                    getFieldValue(
                                                        "showFinished"
                                                    );
                                                if (showFinished) {
                                                    return (
                                                        <Button
                                                            type="primary"
                                                            icon={
                                                                <EyeOutlined />
                                                            }
                                                            onClick={onEye}
                                                            data-testid="eye-icon"
                                                        />
                                                    );
                                                }
                                                return (
                                                    <Button
                                                        type="primary"
                                                        icon={
                                                            <EyeInvisibleOutlined />
                                                        }
                                                        onClick={onEye}
                                                        data-testid="eye-invisible-icon"
                                                    />
                                                );
                                            }}
                                        </Form.Item>
                                    </Form.Item>
                                </Col>
                                <Col>
                                    <Dropdown
                                        overlay={renderPopupDelete()}
                                        trigger={['click']}
                                        placement="bottomCenter"
                                        visible={isPopupDelete}
                                    >
                                        <Button
                                            danger
                                            type="primary"
                                            icon={<DeleteOutlined />}
                                            data-testid="delete-icon"
                                            onClick={() => {
                                                setIsPopupDelete(true)
                                            }}
                                        />
                                    </Dropdown>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Form>
                <Row>
                    <Progress percent={calcPercent()} data-testid="progress" />
                </Row>
                <Row>
                    <Col span={24}>
                        {checklist.items.map((item) => (
                            !checklist.showFinished ?
                                (
                                    !item.isFinished ?
                                    <Row key={item.id}>
                                        <BoardChecklistItem
                                            item={item}
                                            onSubmit={onSubmit}
                                        />
                                    </Row> : null
                                ) : <Row key={item.id}>
                                        <BoardChecklistItem
                                            item={item}
                                            onSubmit={onSubmit}
                                        />
                                    </Row>
                        ))}
                    </Col>
                </Row>
                <Row justify="start">
                    <Col span={24}>{renderAddChecklistButton()}</Col>
                </Row>
            </Col>
        </Col>
    );
};

export default BoardChecklist;
