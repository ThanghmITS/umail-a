import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import BoardChecklist from "../BoardChecklist";
import { createChecklist } from "~/test/mock/personnelBoardAPIMock";

const mockOnChecklistUpdate = jest.fn();
const mockOnChecklistItemUpdate = jest.fn();

describe("BoardChecklist,tsx", () => {
    const titleInputTestId = "title-input";
    const eyeIconTestId = "eye-icon";
    const eyeInvisibleIconTestId = "eye-invisible-icon";
    const deleteIconTestId = "delete-icon";
    const progressTestId = "progress";

    test("render test with showFinished true", () => {
        render(
            <BoardChecklist
                checklist={createChecklist(undefined, undefined, true)}
                onChecklistSubmit={mockOnChecklistUpdate}
                onChecklistItemSubmit={mockOnChecklistItemUpdate}
            />
        );
        const titleInputElement = screen.getByTestId(
            titleInputTestId
        ) as HTMLInputElement;
        expect(titleInputElement).toBeInTheDocument();
        const showFinishedTrueButtonElement = screen.getByTestId(eyeIconTestId);
        expect(showFinishedTrueButtonElement).toBeInTheDocument();
        const showFinishedFalseButtonElement = screen.queryByTestId(
            eyeInvisibleIconTestId
        );
        expect(showFinishedFalseButtonElement).not.toBeInTheDocument();
        const deleteButtonElement = screen.getByTestId(deleteIconTestId);
        expect(deleteButtonElement).toBeInTheDocument();
        const progressButtonElement = screen.getByTestId(progressTestId);
        expect(progressButtonElement).toBeInTheDocument();
        const addItemButtonElement = screen.getByRole("button", {
            name: /アイテムを追加/,
        });
        expect(addItemButtonElement).toBeInTheDocument();
        expect(addItemButtonElement).not.toBeDisabled();
    });

    test("render test with showFinished false", () => {
        render(
            <BoardChecklist
                checklist={createChecklist()}
                onChecklistSubmit={mockOnChecklistUpdate}
                onChecklistItemSubmit={mockOnChecklistItemUpdate}
            />
        );
        const titleInputElement = screen.getByTestId(
            titleInputTestId
        ) as HTMLInputElement;
        expect(titleInputElement).toBeInTheDocument();
        const showFinishedTrueButtonElement =
            screen.queryByTestId(eyeIconTestId);
        expect(showFinishedTrueButtonElement).not.toBeInTheDocument();
        const showFinishedFalseButtonElement = screen.getByTestId(
            eyeInvisibleIconTestId
        );
        expect(showFinishedFalseButtonElement).toBeInTheDocument();
        const deleteButtonElement = screen.getByTestId(deleteIconTestId);
        expect(deleteButtonElement).toBeInTheDocument();
        const progressButtonElement = screen.getByTestId(progressTestId);
        expect(progressButtonElement).toBeInTheDocument();
        const addItemButtonElement = screen.getByRole("button", {
            name: /アイテムを追加/,
        });
        expect(addItemButtonElement).toBeInTheDocument();
        expect(addItemButtonElement).not.toBeDisabled();
    });
});
