import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import BoardChecklistItem from "../BoardChecklistItem";
import { checklistItemResponse } from "~/test/mock/personnelBoardAPIMock";

const mockOnPress = jest.fn();

describe("BoardChecklistItem.tsx", () => {
    const itemInputTestId = "item-input";

    test("render test", () => {
        render(
            <BoardChecklistItem
                item={checklistItemResponse}
                onSubmit={mockOnPress}
            />
        );
        const checkboxElement = screen.getByRole("checkbox");
        expect(checkboxElement).toBeInTheDocument();
        const inputElement = screen.getByTestId(
            itemInputTestId
        ) as HTMLTextAreaElement;
        expect(inputElement).toBeInTheDocument();
        const idElement = screen.getByTestId("hidden-id");
        expect(idElement).toBeInTheDocument();
    });
});
