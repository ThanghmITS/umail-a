import React, { useEffect } from "react";
import { Button, Checkbox, Col, Form, Input, Row } from "antd";
import {
    BoardChecklistItemCreateFormModel,
    BoardChecklistItemModel,
    BoardChecklistItemUpdateFormModel,
} from "~/models/boardCommonModel";
import styles from "./BoardChecklistItem.scss";

type Props = {
    item: BoardChecklistItemModel;
    onSubmit: (values: BoardChecklistItemUpdateFormModel) => void;
};

const BoardChecklistItem = ({ item, onSubmit }: Props) => {
    const idFieldName = "id";
    const contentFieldName = "content";
    const isFinishedFieldName = "isFinished";
    const [form] = Form.useForm<BoardChecklistItemUpdateFormModel>();

    useEffect(() => {
        form.setFieldsValue(item);
    }, [item]);

    return (
        <Col span={24}>
            <Row>
                <Form form={form} onFinish={onSubmit}>
                    <Row justify="start">
                        <Col>
                            <Form.Item
                                name={isFinishedFieldName}
                                valuePropName="checked">
                                <Checkbox onChange={form.submit}/>
                            </Form.Item>
                        </Col>
                        <Col>
                            <Form.Item name={contentFieldName}>
                                <Input
                                    bordered={false}
                                    onPressEnter={form.submit}
                                    data-testid="item-input"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Form.Item
                        hidden
                        name={idFieldName}
                        data-testid="hidden-id"
                    >
                        {/** NOTE(joshua-hashimoto): エラーが発生しないように設定。実際に使用はしていない */}
                        <Input />
                    </Form.Item>
                </Form>
            </Row>
        </Col>
    );
};

export default BoardChecklistItem;
