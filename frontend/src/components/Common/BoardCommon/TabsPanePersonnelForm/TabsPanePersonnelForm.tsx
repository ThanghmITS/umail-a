import React, { useEffect, useState } from 'react';
import { Form } from 'antd';
import { FormInstance } from 'antd/lib/form/Form';
import moment from 'moment';
import BoardTabPanesForms from '~/components/Common/BoardCommon/BoardTabPanesForms/BoardTabPanesForms';
import { BoardChecklistItemUpdateFormModel, BoardChecklistModel, BoardChecklistUpdateFormModel } from '~/models/boardCommonModel';
import {
    PersonnelBoardCopyCardFormModel, PersonnelBoardDetailModel
} from "~/models/personnelBoardModel";

type Props = {
    children: React.ReactNode;
    onFinish: (values:PersonnelBoardDetailModel) => void;
    initialData?: PersonnelBoardDetailModel;
    form: FormInstance<PersonnelBoardDetailModel>;
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    dataCheckLists: BoardChecklistModel[];
    onDeleteCheckList: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
    onCreateCheckList: () => void;
    onDuplicateCard: (data: PersonnelBoardCopyCardFormModel) => void;
    onDeleteCard: (cardId: string) => void;
}
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};

const TabsPanePersonnelForm = ({
        onChecklistItemSubmit,
        onFinish,
        initialData,
        dataCheckLists,
        children,
        onDeleteCheckList,
        form,
        onAddItem,
        onSubmitFinishContent,
        onCreateCheckList,
        onDuplicateCard,
        onDeleteCard
    }: Props) => {

    const typeCard = 'personnel';
    const [formValues, setFormValues] = useState <PersonnelBoardDetailModel>();

    useEffect(() => {
        if(initialData) {
            setFormValues(initialData);
        }
    }, [initialData]);

    useEffect(() => {
        if(!formValues?.period) {
            return
        }

        const { start, end } = formValues.period;
        if(start && end && moment(start).diff(moment(end), 'days') > 0) {
            form.setFieldsValue({
              period: {
                end: start,
              },
            });
        }
        if (!end) {
          form.setFieldsValue({
            period: {
              isFinished: false
            },
          });
        }
    }, [formValues?.period]);

    const onSetFormValues = (_: any, values: PersonnelBoardDetailModel) => {
      setFormValues(values);
    };

    return (
        <Form
            {...layout}
            form={form}
            onFinish={onFinish}
            onValuesChange={onSetFormValues}
        >
            <BoardTabPanesForms
                typeCard={typeCard}
                personnelValues={formValues}
                onDelete={onDeleteCheckList}
                onSubmitFinishContent={onSubmitFinishContent}
                onChecklistItemSubmit={onChecklistItemSubmit}
                dataCheckLists={dataCheckLists}
                onCreateCheckList={onCreateCheckList}
                onAddItem={onAddItem}
                onDuplicateCard={onDuplicateCard}
                onDeleteCard={onDeleteCard}
            >
                {children}
            </BoardTabPanesForms>
        </Form>

    )
}

export default TabsPanePersonnelForm;

