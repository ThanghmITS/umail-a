import React from "react";
import { Popover, PopoverProps } from "antd";
import styles from "./CustomPopover.scss";

type Props = PopoverProps & {};

const CustomPopover = ({ children, ...props }: Props) => {
    return (
        <Popover
            trigger="hover"
            overlayStyle={{
                maxWidth: 500,
                overflowWrap: "break-word",
                opacity: "95%",
            }}
            color="#615748"
            {...props}>
            {children}
        </Popover>
    );
};

export default CustomPopover;
