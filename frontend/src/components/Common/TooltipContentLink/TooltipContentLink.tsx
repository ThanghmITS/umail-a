import React from "react";
import { Link, LinkProps } from "react-router-dom";
import { Typography } from "antd";
import { infoTooltipLinkColor, alertTooltipLinkColor } from "~/utils/constants";

type Props = LinkProps & {
    title: string;
};

export const TooltipContentLinkOpenNewTab = ({ title, ...props }: Props) => {
    return (
        <span onClick={(e) => e.stopPropagation() }>
            <Link
                {...props}
                style={{ color: infoTooltipLinkColor }}
                component={Typography.Link}
                target="_blank"
                rel="noopener noreferrer">
                {title}
            </Link>
        </span>
    );
};

export const TooltipContentLink = ({ title, ...props }: Props) => {
    return (
        <Link
            {...props}
            style={{ color: infoTooltipLinkColor }}
            component={Typography.Link}>
            {title}
        </Link>
    );
};

export const AlertTooltipContentLink = ({ title, ...props }: Props) => {
    return (
        <Link
            {...props}
            style={{ color: alertTooltipLinkColor }}
            component={Typography.Link}>
            {title}
        </Link>
    );
};

