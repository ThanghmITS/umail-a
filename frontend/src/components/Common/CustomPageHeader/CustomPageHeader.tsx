import React, { ReactNode } from "react";
import { PageHeader, PageHeaderProps } from "antd";
import styles from "./CustomPageHeader.scss";

type Props = PageHeaderProps & {
    title: ReactNode;
};

const CustomPageHeader = ({ ...props }: Props) => {
    return <PageHeader style={{ padding: "16px 0px" }} {...props} />;
};

export default CustomPageHeader;
