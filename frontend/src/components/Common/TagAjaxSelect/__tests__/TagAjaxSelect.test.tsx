import React from "react";
import {
    renderHook,
    renderWithQueryClient,
    screen,
    userEvent,
} from "~/test/utils";
import TagAjaxSelect from "../TagAjaxSelect";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

describe("TagAjaxSelect.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("enabled render test", async () => {
        renderWithQueryClient(<TagAjaxSelect />);
        const selectPlaceholderElement = screen.getByText(/クリックして選択/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const selectOptionElement = await screen.findByText(/django/);
        expect(selectOptionElement).toBeInTheDocument();
    });

    test("disabled render test", async () => {
        renderWithQueryClient(<TagAjaxSelect disabled />);
        const selectPlaceholderElement = screen.getByText(/クリックして選択/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const selectOptionElement = screen.queryByText(/django/);
        expect(selectOptionElement).not.toBeInTheDocument();
    });
});
