import React, { MouseEvent, useEffect, useState } from "react";
import { Select, SelectProps, Spin, Tag } from "antd";
import { useFetchTagsAPIQuery } from "~/hooks/useTag";
import { PaginationRequestModel } from "~/models/requestModel";
import { TagModel } from "~/models/tagModel";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import styles from "./TagAjaxSelect.scss";
import { TYPE_SELECT } from "~/utils/types";

type Props = SelectProps<TagModel> & {
    type?:string
};

const TagAjaxSelect = ({ ...props }: Props) => {
    const [deps, setDeps] = useState<PaginationRequestModel>({
        page: 1,
        value: "",
        pageSize: 2000,
    });
    const [dataSelect, setDataSelect] = useState<TagModel[]>();
    const { data, isLoading, isError } = useFetchTagsAPIQuery({ deps });

    useEffect(() => {
        //TODO(SyBv): add skill to tag
        if (data) {
            if (props.type === TYPE_SELECT.SKILL) {
                const skills = data.results.filter((item) => item.is_skill);
                setDataSelect(skills)
            } else {
                const tags = data.results.filter((item) => !item.is_skill);
                setDataSelect(tags)
            }
        }
    },[data])

    const onScroll = (event: any) => {
        const target = event?.target;
        if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
            setDeps({
                ...deps,
                page: deps.page ?? 0 + 1,
            });
        }
    };

    const onSearch = useCustomDebouncedCallback((value: string) => {
        setDeps({
            ...deps,
            page: 1,
            value,
        });
    });

    const onPreventMouseDown = (event: MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();
    };

    return (
        <Spin size="small" spinning={isLoading}>
            <Select
                showSearch
                allowClear
                autoClearSearchValue
                filterOption={false}
                mode="multiple"
                placeholder="クリックして選択"
                {...props}
                onSearch={onSearch}
                onPopupScroll={onScroll}
                style={{ width: "100%" }}
                tagRender={(props) => {
                    if (typeof props.label === "string") {
                        return <Tag>{props.label}</Tag>;
                    }
                    return React.cloneElement(
                        props.label as React.ReactElement,
                        {
                            ...props,
                            onMouseDown: onPreventMouseDown,
                            style: {
                                marginTop: "3px",
                                marginBottom: "3px",
                                marginInlineEnd: "4.8px",
                                paddingInlineStart: "4px",
                                paddingInlineEnd: "4px",
                                whiteSpace: "normal",
                            },
                        }
                    );
                }}
                onBlur={(_) => {
                    if (deps.value) {
                        setDeps({
                            ...deps,
                            page: 1,
                            value: "",
                        });
                    }
                }}>
                {dataSelect?.map((tag) => (
                    <Select.Option key={tag.id} value={tag.id}>
                        <Tag
                            color={tag.color}
                            style={{
                                marginTop: "3px",
                                marginBottom: "3px",
                                marginInlineEnd: "4.8px",
                                paddingInlineStart: "8px",
                                paddingInlineEnd: "4px",
                                whiteSpace: "normal",
                            }}>
                            {tag.value}
                        </Tag>
                    </Select.Option>
                ))}
            </Select>
        </Spin>
    );
};

export default TagAjaxSelect;
