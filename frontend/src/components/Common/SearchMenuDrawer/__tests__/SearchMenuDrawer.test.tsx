import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import SearchMenuDrawer from "../SearchMenuDrawer";
import { Form } from "antd";

const mockIsDrawerOpen = jest.fn();
const mockForm = jest.fn();
const mockOnReset = jest.fn();
const mockOnUpdateUnshowList = jest.fn();
const mockOnFilter = jest.fn();
const mockOnDrawerClose = jest.fn();
const mockOnFieldAdd = jest.fn();
const mockOnFieldRemove = jest.fn();
const mockSetInitialFormSearch = jest.fn();

const Wrapper = () => {
    const [form] = Form.useForm();

    return (
        <SearchMenuDrawer
            isDrawerOpen={mockIsDrawerOpen}
            form={form}
            menuItems={[]}
            unshowList={[]}
            onReset={mockOnReset}
            onUpdateUnshowList={mockOnUpdateUnshowList}
            otherControls={[]}
            onFilter={mockOnFilter}
            searchTemplate={[]}
            onDrawerClose={mockOnDrawerClose}
            onFieldAdd={mockOnFieldAdd}
            onFieldRemove={mockOnFieldRemove}
            setInitialFormSearch={mockSetInitialFormSearch}
        />
    );
};

describe("SearchMenuDrawer.jsx", () => {
    test("not edit mode render test", () => {
        render(<Wrapper />);
        const resetButtonElement = screen.getByRole("button", {
            name: "検索条件をリセット",
        });
        expect(resetButtonElement).toBeInTheDocument();
        expect(resetButtonElement).not.toBeChecked();
        const showListAreaTitleElement = screen.getByText(/^検索対象$/);
        expect(showListAreaTitleElement).toBeInTheDocument();
        const unshowListAreaTitleElement = screen.getByText(/^検索対象外$/);
        expect(unshowListAreaTitleElement).toBeInTheDocument();
        const cancelButtonElement = screen.getByRole("button", {
            name: "キャンセル",
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
    });

    test("edit mode render test", async () => {
        render(<Wrapper />);
        const editModeOpenButtonElement = screen.getByTestId(
            "search-menu-drawer-edit-mode-open-button"
        );
        expect(editModeOpenButtonElement).toBeInTheDocument();
        expect(editModeOpenButtonElement).not.toBeDisabled();
        await userEvent.click(editModeOpenButtonElement);
        const editModeCloseButtonElement = screen.getByTestId(
            "search-menu-drawer-edit-mode-close-button"
        );
        expect(editModeCloseButtonElement).toBeInTheDocument();
        expect(editModeCloseButtonElement).not.toBeDisabled();
        const resetButtonElement = screen.getByRole("button", {
            name: "検索条件をリセット",
        });
        expect(resetButtonElement).toBeInTheDocument();
        expect(resetButtonElement).not.toBeChecked();
        const showListAreaTitleElement = screen.getByText(/^検索対象$/);
        expect(showListAreaTitleElement).toBeInTheDocument();
        const unshowListAreaTitleElement = screen.getByText(/^検索対象外$/);
        expect(unshowListAreaTitleElement).toBeInTheDocument();
        const cancelButtonElement = screen.getByRole("button", {
            name: "キャンセル",
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const updateButtonElement = screen.getByText(/検索対象を更新/);
        expect(updateButtonElement).toBeInTheDocument();
        expect(updateButtonElement).not.toBeDisabled();
    });

    test("render test unshow list open/close button", async () => {
        render(<Wrapper />);
        const openButtonElement = screen.getByTestId("open-button");
        expect(openButtonElement).toBeInTheDocument();
        await userEvent.click(openButtonElement);
        const closeButtonElement = screen.getByTestId("close-button");
        expect(closeButtonElement).toBeInTheDocument();
    });
});
