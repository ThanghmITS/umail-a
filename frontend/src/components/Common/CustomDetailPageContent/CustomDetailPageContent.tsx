import React, { ReactNode } from "react";
import { Card, CardProps } from "antd";

type Props = CardProps & {
    children: ReactNode;
};

const CustomDetailPageContent = ({ children, ...props }: Props) => {
    return (
        <Card bodyStyle={{ padding: "8px" }} {...props}>
            {children}
        </Card>
    );
};

export default CustomDetailPageContent;
