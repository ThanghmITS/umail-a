import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "antd";
import styles from "./BackButton.scss";

type Props = {
    to?: string;
};

const BackButton = ({ to }: Props) => {
    const router = useHistory();

    const toRoute = () => {
        if (!to) {
            router.goBack();
            return;
        }
        router.push(to);
    };

    return (
        <Button className={styles.backButton} onClick={toRoute}>
            戻る
        </Button>
    );
};

export default BackButton;
