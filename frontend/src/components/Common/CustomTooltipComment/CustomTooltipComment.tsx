import React from "react";
import { TooltipProps, Tooltip } from "antd";
import styles from "./CustomTooltipComment.scss";

type Props = TooltipProps & {};

const CustomTooltipComment = ({ children, ...props }: Props) => {
    return (
        <Tooltip
            overlayStyle={{
                maxWidth: 500,
                overflowWrap: "break-word"
            }}
            overlayInnerStyle={{
                padding: 0,
            }}
            {...props}>
            {children}
        </Tooltip>
    );
};

export default CustomTooltipComment;
