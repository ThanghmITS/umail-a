import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import CustomTooltipComment from "../CustomTooltipComment";

describe("CustomTooltipComment.tsx", () => {
    test("render test", async () => {
        render(
            <CustomTooltipComment title="Title">Content</CustomTooltipComment>
        );
        const contentElement = screen.getByText(/Content/);
        expect(contentElement).toBeInTheDocument();
        await userEvent.hover(contentElement);
        const tooltipElement = await screen.findByText(/Title/);
        expect(tooltipElement).toBeInTheDocument();
    });
});
