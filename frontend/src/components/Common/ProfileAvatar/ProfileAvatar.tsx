import React, { CSSProperties } from "react";
import { Image } from "antd";
import styles from "./ProfileAvatar.scss";

type Props = {
  preview?: boolean;
  style?: CSSProperties;
  avatar: string;
};

const ProfileAvatar = ({ preview = false, style, avatar }: Props) => {
   return (
      <Image
        preview={preview}
        style={style}
        className={styles.avatarIcon}
        src={avatar}
      />
    )
};

export default ProfileAvatar;
