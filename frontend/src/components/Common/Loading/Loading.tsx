import React from "react";
import { Col, Layout, Row, Spin } from "antd";

const { Content } = Layout;

type Props = {
    isLoading: boolean;
};

const Loading = ({ isLoading }: Props) => {
    return (
        <Content style={{ height: "100vh" }}>
            <Col span={24}>
                <Row justify="center" align="middle">
                    <Col>
                        <Spin spinning={isLoading} />
                    </Col>
                </Row>
            </Col>
        </Content>
    );
};

export default Loading;
