import React from "react";
import { render, screen } from "~/test/utils";
import LimitDivider from "../LimitDivider";

describe("LimitDivider.tsx", () => {
    test("render test with only count", () => {
        render(<LimitDivider currentCount={8} limitCount={10} />);
        const currentCountElement = screen.getByText(/8/);
        expect(currentCountElement).toBeInTheDocument();
        const limitCountElement = screen.getByText(/10/);
        expect(limitCountElement).toBeInTheDocument();
        const divideCountElement = screen.getByText(/／/);
        expect(divideCountElement).toBeInTheDocument();
        const unitElements = screen.getAllByText(/件/);
        expect(unitElements.length).toBe(2);
    });

    test("render with override unit", () => {
        render(<LimitDivider currentCount={8} limitCount={10} unit="KL" />);
        const currentCountElement = screen.getByText(/8/);
        expect(currentCountElement).toBeInTheDocument();
        const limitCountElement = screen.getByText(/10/);
        expect(limitCountElement).toBeInTheDocument();
        const divideCountElement = screen.getByText(/／/);
        expect(divideCountElement).toBeInTheDocument();
        const unitElements = screen.getAllByText(/KL/);
        expect(unitElements.length).toBe(2);
    });

    test("render with prefix", () => {
        render(
            <LimitDivider
                currentCount={8}
                limitCount={10}
                prefix="prefix-test"
            />
        );
        const currentCountElement = screen.getByText(/8/);
        expect(currentCountElement).toBeInTheDocument();
        const limitCountElement = screen.getByText(/10/);
        expect(limitCountElement).toBeInTheDocument();
        const divideCountElement = screen.getByText(/／/);
        expect(divideCountElement).toBeInTheDocument();
        const unitElements = screen.getAllByText(/件/);
        expect(unitElements.length).toBe(2);
        const prefixElement = screen.getByText(/prefix-test/);
        expect(prefixElement).toBeInTheDocument();
    });

    test("render with suffix", () => {
        render(
            <LimitDivider
                currentCount={8}
                limitCount={10}
                suffix="suffix-test"
            />
        );
        const currentCountElement = screen.getByText(/8/);
        expect(currentCountElement).toBeInTheDocument();
        const limitCountElement = screen.getByText(/10/);
        expect(limitCountElement).toBeInTheDocument();
        const divideCountElement = screen.getByText(/／/);
        expect(divideCountElement).toBeInTheDocument();
        const unitElements = screen.getAllByText(/件/);
        expect(unitElements.length).toBe(2);
        const suffixElement = screen.getByText(/suffix-test/);
        expect(suffixElement).toBeInTheDocument();
    });
});
