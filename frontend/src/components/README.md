# About
* This is a directory of React components.

# Files
* App.jsx: The root component of each contents.
* AppContainer.jsx: An wrapper of the App.jsx, which defines page layout.
* AppContainer.scss: CSS style definitions for App header and sidebar.
* helpers.js: A collection of helper functions.

# Directories
* Routes: Defines mapping between an URL and a Page component.
* Pages: Defines page components.
  * basically, pages are created by factories, which is [High Order Components(HOC)](https://ja.reactjs.org/docs/higher-order-components.html)
  * Some pages which provide a unique view are exceptional.
* Other directories are as the name suggests.
