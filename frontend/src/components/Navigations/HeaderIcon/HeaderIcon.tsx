import React from "react";
import { Popover } from "antd";
import styles from "./HeaderIcon.scss";
import { HeaderIconPropsModel } from "~/models/headerModel";

type Props = HeaderIconPropsModel & {};

const HeaderIcon = ({ title, content, icon }: Props) => {
    return (
        <Popover placement="bottomRight" content={content} title={title}>
            <button
                className={`ant-btn ant-btn-icon-only ant-btn-circle ${styles.headerIcon}`}>
                {icon}
            </button>
        </Popover>
    );
};

export default HeaderIcon;
