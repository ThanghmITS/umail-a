import React, { useState } from "react";
import { Col, Row, Form, Switch, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./IncludeInvalidSwitchFormItem.scss";

const IncludeInvalidSwitchFormItem = ({ inactiveFilter, onChange }) => {
    const [includeInvalidFilter, setIncludeInvalidFilter] = useState(!!inactiveFilter)

    const onChangeSwitch = (checked, event) => {
        setIncludeInvalidFilter()
        onChange(checked, event)
    }

    return (
        <Col span={24}>
            <Row className={styles.container} justify="end">
                <Col span={23}>
                    <Row justify="end">
                        <Form.Item
                            labelCol={{}}
                            wrapperCol={{}}
                            colon={false}
                            valuePropName="checked"
                            name="ignore_filter"
                            initialValue={includeInvalidFilter}
                            noStyle>
                            <Switch
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                                onChange={onChangeSwitch}
                            />
                        </Form.Item>
                        <label className={styles.switchLabel}>
                            無効含
                        </label>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip title="無効化しているユーザーは、チェックがないと結果表示されません。チェックがあると表示されます。">
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default IncludeInvalidSwitchFormItem;
