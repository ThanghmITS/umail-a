import React from "react";
import { Col, Form, Select } from "antd";
import styles from "./MasterRuleFormItem.scss";

const { Option } = Select;

const MasterRuleFormItem = ({ master_rule, onBlur, onChange }) => {
    return (
        <Col span={4} style={{ marginBottom: "1%" }}>
            <Form.Item colon={false} name="master_rule" initialValue={master_rule} noStyle>
                <Select
                    className={styles.container}
                    placeholder="ルール条件"
                    onBlur={onBlur}
                    onChange={onChange}
                    allowClear>
                    <Select.Option value="all">全ての条件に一致</Select.Option>
                    <Select.Option value="any">いずれかの条件に一致</Select.Option>
                </Select>
            </Form.Item>
        </Col>
    );
};

export default MasterRuleFormItem;
