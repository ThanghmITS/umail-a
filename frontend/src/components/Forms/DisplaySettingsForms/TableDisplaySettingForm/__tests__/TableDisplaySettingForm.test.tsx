import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import TableDisplaySettingForm from "../TableDisplaySettingForm";

describe("TableDisplaySettingForm.tsx", () => {
    test("render test", async () => {
        render(<TableDisplaySettingForm fieldName="contacts" />);
        const displayLabelElement = screen.getByText(/^表示$/);
        expect(displayLabelElement).toBeInTheDocument();
        const undisplayLabelElement = screen.getByText(/^非表示$/);
        expect(undisplayLabelElement).toBeInTheDocument();
        const pageSizeLabelElement = screen.getByText(/表示件数/);
        expect(pageSizeLabelElement).toBeInTheDocument();
        const defaultPageSizeElement = screen.getByText(/10/);
        expect(defaultPageSizeElement).toBeInTheDocument();
        await userEvent.click(defaultPageSizeElement);
        const pageSizeOption10Element = await screen.findByTestId(
            "table-display-setting-page-size-option-10"
        );
        expect(pageSizeOption10Element).toBeInTheDocument();
        const pageSizeOption50Element = await screen.findByTestId(
            "table-display-setting-page-size-option-50"
        );
        expect(pageSizeOption50Element).toBeInTheDocument();
        const pageSizeOption100Element = await screen.findByTestId(
            "table-display-setting-page-size-option-100"
        );
        expect(pageSizeOption100Element).toBeInTheDocument();
    });
});
