import React from "react";
import { Col, Form, Input, Tooltip, Row, Select } from "antd";
import styles from "./EmailSearchHasAttachmentsFormItem.scss";

const { Option } = Select;

const EmailSearchHasAttachmentsFormItem = () => {
    const fieldName = "has_attachments";

    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={24}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Select
                            placeholder="添付"
                            allowClear
                            className={styles.container}>
                            <Select.Option value="true">あり</Select.Option>
                            <Select.Option value="false">なし</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchHasAttachmentsFormItem;
