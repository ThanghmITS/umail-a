import React from "react";
import { Col, Form, Input, Tooltip, Row } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./EmailSearchEmailContentFormItem.scss";
import { iconCustomColor } from "~/utils/constants";

type Props = {
    onClear: (fieldName: string, value: string) => void;
};

const EmailSearchEmailContentFormItem = ({ onClear }: Props) => {
    const fieldName = "text";

    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="本文"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(fieldName, "");
                                }
                            }}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                スペース区切りの部分一致検索。スペース区切りはANDとなります。
                                <br />
                                例：神田 要件定義
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchEmailContentFormItem;
