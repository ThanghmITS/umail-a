import React, { useEffect, useState, MouseEvent, ReactNode } from "react";
import { useSelector } from "react-redux";
import moment from "moment";
import { Button, Row, Col, Form, Card, Tooltip } from "antd";
import { UndoOutlined, UpOutlined, DownOutlined } from "@ant-design/icons";
import { RootState } from "~/models/store";
import validateJapaneseMessages from "../validateMessages";
import EmailSearchSenderFormItem from "./EmailSearchSenderFormItem/EmailSearchSenderFormItem";
import EmailSearchEmailTitleFormItem from "./EmailSearchEmailTitleFormItem/EmailSearchEmailTitleFormItem";
import EmailSearchEmailContentFormItem from "./EmailSearchEmailContentFormItem/EmailSearchEmailContentFormItem";
import EmailSearchHasAttachmentsFormItem from "./EmailSearchHasAttachmentsFormItem/EmailSearchHasAttachmentsFormItem";
import EmailSearchStaffFormItem from "./EmailSearchStaffFormItem/EmailSearchStaffFormItem";
import EmailSearchCommentUserFormItem from "./EmailSearchCommentUserFormItem/EmailSearchCommentUserFormItem";
import EmailSearchRecieveDateFormItem from "./EmailSearchRecieveDateFormItem/EmailSearchRecieveDateFormItem";
import EmailSearchIncludeInvalidSwitchFormItem from "./EmailSearchIncludeInvalidSwitchFormItem/EmailSearchIncludeInvalidSwitchFormItem";
import SharedEmailSearchMenuDisplaySettingModal from "./SharedEmailSearchMenuDisplaySettingModal/SharedEmailSearchMenuDisplaySettingModal";
import styles from "./SharedEmailSearchForm.scss";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";

const filterType = "search";

const searchMenuLayout = {
    labelCol: {
        span: 0,
    },
    wrapperCol: {
        span: 24,
    },
};

const formLayout = {
    labelCol: {
        sm: { span: 24 },
        md: { span: 6 },
    },
    wrapperCol: {
        sm: { span: 24 },
        md: { span: 18 },
    },
};

type InitialDataModel = {
    date_range: any[];
    email?: string;
    has_attachments?: string;
    ignore_filter?: boolean;
    sender?: string;
    staff?: string[];
    subject?: string;
    text?: string;
    comment_user?: string[];
};

const emptyData: InitialDataModel = {
    date_range: [],
    email: "",
    has_attachments: undefined,
    ignore_filter: false,
    sender: "",
    staff: [],
    subject: "",
    text: "",
    comment_user: [],
};

type Props = {
    tableName: string;
    initialData: InitialDataModel;
    submitHandler: (initialValues: InitialDataModel) => void;
    onSearchMenu: (event: MouseEvent<HTMLInputElement>) => void;
    searchMenuOpen: boolean;
};

const SharedEmailSearchForm = ({
    tableName,
    initialData = emptyData,
    submitHandler,
    onSearchMenu,
    searchMenuOpen,
}: Props) => {
    const [form] = Form.useForm();
    const [searchFields, setSearchFields] = useState<ReactNode[]>([]);
    const { displaySetting } = useSelector(
        (state: RootState) => state.displaySettingPage
    );
    const submitToFilter = useCustomDebouncedCallback((values) => {
        submitHandler(values);
    });

    const onReset = () => {
        form.setFieldsValue(emptyData);
        submitHandler(emptyData);
    };

    const onClear = async (fieldName: string, value: string) => {
        try {
            const values = await form.validateFields();
            values[fieldName] = value;
            submitHandler(values);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        const tempData = { ...initialData };
        const dateRange = tempData.date_range ?? [];
        if (dateRange.length) {
            tempData.date_range = dateRange.map((date) => moment(date));
        }
        form.setFieldsValue(tempData);
    }, []);

    useEffect(() => {
        const unshowSearchFields =
            displaySetting &&
            displaySetting[tableName] &&
            displaySetting[tableName][filterType]
                ? displaySetting[tableName][filterType]
                : [];

        let searchItemsForDisplay = [];

        if (unshowSearchFields) {
            if (!unshowSearchFields.includes("sender")) {
                searchItemsForDisplay.push(
                    <EmailSearchSenderFormItem onClear={onClear} />
                );
            }
            if (!unshowSearchFields.includes("subject")) {
                searchItemsForDisplay.push(
                    <EmailSearchEmailTitleFormItem onClear={onClear} />
                );
            }
            if (!unshowSearchFields.includes("text")) {
                searchItemsForDisplay.push(
                    <EmailSearchEmailContentFormItem onClear={onClear} />
                );
            }
            if (!unshowSearchFields.includes("has_attachments")) {
                searchItemsForDisplay.push(
                    <EmailSearchHasAttachmentsFormItem />
                );
            }
            if (!unshowSearchFields.includes("staff")) {
                searchItemsForDisplay.push(<EmailSearchStaffFormItem />);
            }
            if (!unshowSearchFields.includes("date_range")) {
                searchItemsForDisplay.push(<EmailSearchRecieveDateFormItem />);
            }
            if (!unshowSearchFields.includes("comment_user")) {
                searchItemsForDisplay.push(<EmailSearchCommentUserFormItem />);
            }
        }
        setSearchFields(searchItemsForDisplay);
    }, [displaySetting]);

    return (
        <>
            <Col>
                <Row justify="end">
                    <Button
                        type="primary"
                        onClick={onSearchMenu}
                        size="small"
                        icon={
                            !searchMenuOpen ? <UpOutlined /> : <DownOutlined />
                        }>
                        検索メニュー
                    </Button>
                </Row>
            </Col>
            <Card className={styles.container} hidden={!searchMenuOpen}>
                <Form
                    {...searchMenuLayout}
                    layout="horizontal"
                    form={form}
                    initialValues={initialData}
                    onValuesChange={(changedValues, allValues) => {
                        submitToFilter(allValues);
                    }}
                    validateMessages={validateJapaneseMessages}>
                    <Row>
                        <Col span={20}>
                            <Row gutter={24}>
                                {searchFields.map((item, index) => (
                                    <React.Fragment key={index}>
                                        {item}
                                    </React.Fragment>
                                ))}
                            </Row>
                        </Col>
                        <Col span={4}>
                            <Row justify="end">
                                <SharedEmailSearchMenuDisplaySettingModal />
                                <Tooltip title="検索条件をリセット">
                                    <Button
                                        type="primary"
                                        icon={<UndoOutlined />}
                                        style={{ margin: "auto 6px" }}
                                        onClick={onReset}
                                    />
                                </Tooltip>
                            </Row>
                            <Row justify="end" style={{ marginTop: "7%" }}>
                                <EmailSearchIncludeInvalidSwitchFormItem />
                            </Row>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </>
    );
};

export default SharedEmailSearchForm;
