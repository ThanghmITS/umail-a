import React from "react";
import { Col, Form, Input, Tooltip, Row } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./EmailSearchSenderFormItem.scss";
import { iconCustomColor } from "~/utils/constants";

type Props = {
    onClear: (fieldName: string, value: string) => void;
};

const EmailSearchSenderFormItem = ({ onClear }: Props) => {
    const senderFieldName = "sender";
    const emailFieldName = "email";

    return (
        <Col span={12} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Row>
                        <Col span={12}>
                            <Form.Item
                                name={senderFieldName}
                                colon={false}
                                noStyle>
                                <Input
                                    className={styles.userInput}
                                    placeholder="差出人(名称)"
                                    allowClear
                                    onChange={(event) => {
                                        const value = event.target.value;
                                        if (value === "") {
                                            onClear(senderFieldName, "");
                                        }
                                    }}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                colon={false}
                                name={emailFieldName}
                                noStyle>
                                <Input
                                    className={styles.userInput}
                                    placeholder="差出人(メールアドレス)"
                                    allowClear
                                    onChange={(event) => {
                                        const value = event.target.value;
                                        if (value === "") {
                                            onClear(emailFieldName, "");
                                        }
                                    }}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip title="部分一致検索">
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchSenderFormItem;
