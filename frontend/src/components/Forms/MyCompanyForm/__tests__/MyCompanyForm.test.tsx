import React from "react";
import {
    renderWithAllProviders,
    renderWithQueryClient,
    screen,
    userEvent,
} from "~/test/utils";
import MyCompanyForm from "../MyCompanyForm";
import { mockMyCompanyMockData } from "~/test/mock/myCompanyAPIMock";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { generateRandomToken } from "~/utils/utils";

const submitHandler = jest.fn();

describe("MyCompanyForm.jsx", () => {
    describe("basic info", () => {
        test("render test", () => {
            renderWithQueryClient(
                <MyCompanyForm
                    resourceURL=""
                    initialData={mockMyCompanyMockData}
                    fieldErrors={{}}
                    submitHandler={submitHandler}
                />
            );
            const basicInfoTabElement = screen.getByText(/自社情報/);
            expect(basicInfoTabElement).toBeInTheDocument();
            const myCompanyNameLabelElement = screen.getByText(/自社名/);
            expect(myCompanyNameLabelElement).toBeInTheDocument();
            const myCompanyInputElement = screen.getByPlaceholderText(
                /サンプル株式会社/
            ) as HTMLInputElement;
            expect(myCompanyInputElement).toBeInTheDocument();
            expect(myCompanyInputElement.value).toBe("Example");
            const establishDateLabelElement = screen.getByText(/設立年月/);
            expect(establishDateLabelElement).toBeInTheDocument();
            const establishDataDateTimePickerElement =
                screen.getByPlaceholderText(/日付を選択/) as HTMLInputElement;
            expect(establishDataDateTimePickerElement).toBeInTheDocument();
            expect(establishDataDateTimePickerElement.value).toBe("2022-01");
            const addressLabelElement = screen.getByText(/住所/);
            expect(addressLabelElement).toBeInTheDocument();
            const addressInputElement = screen.getByPlaceholderText(
                /市区町村・町名・番地/
            ) as HTMLInputElement;
            expect(addressInputElement).toBeInTheDocument();
            expect(addressInputElement.value).toBe("example_address");
            const buildingInputElement = screen.getByPlaceholderText(
                /建物名/
            ) as HTMLInputElement;
            expect(buildingInputElement).toBeInTheDocument();
            expect(buildingInputElement.value).toBe("example_building");
            const urlLabelElement = screen.getByText(/URL/);
            expect(urlLabelElement).toBeInTheDocument();
            const urlInputElement = screen.getByPlaceholderText(
                /https:\/\//
            ) as HTMLInputElement;
            expect(urlInputElement).toBeInTheDocument();
            expect(urlInputElement.value).toBe("example.co.jp");
            const distributionLabelElement = screen.getByText(/商流/);
            expect(distributionLabelElement).toBeInTheDocument();
            const distributionFalseRadioElement = screen.getByRole("radio", {
                name: "抜けない",
            });
            expect(distributionFalseRadioElement).toBeInTheDocument();
            expect(distributionFalseRadioElement).not.toBeChecked();
            const distributionTrueRadioElement = screen.getByRole("radio", {
                name: "抜ける",
            });
            expect(distributionTrueRadioElement).toBeInTheDocument();
            expect(distributionTrueRadioElement).toBeChecked();
            const capitalLabelElement = screen.getByText(/資本金/);
            expect(capitalLabelElement).toBeInTheDocument();
            const capitalInputElement = screen.getByRole("spinbutton");
            expect(capitalInputElement).toBeInTheDocument();
            const capitalSubtextElement = screen.getByText(/万円/);
            expect(capitalSubtextElement).toBeInTheDocument();
            const pMarkAndISMSLabelElement = screen.getByText(/Pマーク／ISMS/);
            expect(pMarkAndISMSLabelElement).toBeInTheDocument();
            const pMarkAndISMSFalseRadioElement = screen.getByTestId(
                "has_p_mark_or_isms_false"
            );
            expect(pMarkAndISMSFalseRadioElement).toBeInTheDocument();
            expect(pMarkAndISMSFalseRadioElement).not.toBeChecked();
            const pMarkAndISMSTrueRadioElement = screen.getByTestId(
                "has_p_mark_or_isms_true"
            );
            expect(pMarkAndISMSTrueRadioElement).toBeInTheDocument();
            expect(pMarkAndISMSTrueRadioElement).toBeChecked();
            const invoiceLabelElement =
                screen.getByText(/インボイス登録事業者/);
            expect(invoiceLabelElement).toBeInTheDocument();
            const invoiceFalseRadioElement = screen.getByTestId(
                "has_invoice_system_false"
            );
            expect(invoiceFalseRadioElement).toBeInTheDocument();
            expect(invoiceFalseRadioElement).not.toBeChecked();
            const invoiceTrueRadioElement = screen.getByTestId(
                "has_invoice_system_true"
            );
            expect(invoiceTrueRadioElement).toBeInTheDocument();
            expect(invoiceTrueRadioElement).toBeChecked();
            const hakenLabelElement = screen.getByText(/労働者派遣事業/);
            expect(hakenLabelElement).toBeInTheDocument();
            const hakenFalseRadioElement =
                screen.getByTestId("has_haken_false");
            expect(hakenFalseRadioElement).toBeInTheDocument();
            expect(hakenFalseRadioElement).not.toBeChecked();
            const hakenTrueRadioElement = screen.getByTestId("has_haken_true");
            expect(hakenTrueRadioElement).toBeInTheDocument();
            expect(hakenTrueRadioElement).toBeChecked();
            const backButtonElement = screen.getByRole("button", {
                name: "戻る",
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const updateButtonElement = screen.getByRole("button", {
                name: "更 新",
            });
            expect(updateButtonElement).toBeInTheDocument();
            expect(updateButtonElement).not.toBeDisabled();
        });
    });
    describe("transaction condition", () => {
        test("render test", async () => {
            renderWithAllProviders(
                <MyCompanyForm
                    resourceURL=""
                    initialData={mockMyCompanyMockData}
                    fieldErrors={{}}
                    submitHandler={submitHandler}
                />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: generateRandomToken(),
                            },
                        },
                    }),
                }
            );
            const transactionConditionTabElement = screen.getByText(/取引条件/);
            expect(transactionConditionTabElement).toBeInTheDocument();
            await userEvent.click(transactionConditionTabElement);
            const establishYearConditionLabelElement =
                screen.getByText(/取引に必要な設立年数/);
            expect(establishYearConditionLabelElement).toBeInTheDocument();
            const establishYearConditionInputElement = screen.getByTestId(
                "establishment_year"
            ) as HTMLInputElement;
            expect(establishYearConditionInputElement).toBeInTheDocument();
            expect(establishYearConditionInputElement.value).toBe("4");
            const establishYearConditionSubtextElement =
                screen.getByText(/^以上$/);
            expect(establishYearConditionSubtextElement).toBeInTheDocument();
            const capitalConditionLabelElement =
                screen.getByText(/取引に必要な資本金/);
            expect(capitalConditionLabelElement).toBeInTheDocument();
            const capitalConditionInputElement = screen.getByTestId(
                "capital_man_yen_required_for_transactions"
            ) as HTMLInputElement;
            expect(capitalConditionInputElement).toBeInTheDocument();
            expect(capitalConditionInputElement.value).toBe("300");
            const capitalConditionSubtextElement = screen.getByText(/万円以上/);
            expect(capitalConditionSubtextElement).toBeInTheDocument();
            const qualificationConditionLabelElement =
                screen.getByText(/取引に必要な資格/);
            expect(qualificationConditionLabelElement).toBeInTheDocument();
            const qualificationPMarkAndISMSCheckboxElement = screen.getByRole(
                "checkbox",
                { name: "Pマーク／ISMS" }
            );
            expect(
                qualificationPMarkAndISMSCheckboxElement
            ).toBeInTheDocument();
            expect(qualificationPMarkAndISMSCheckboxElement).toBeChecked();
            const qualificationInvoiceCheckboxElement = screen.getByRole(
                "checkbox",
                { name: "インボイス登録事業者" }
            );
            expect(qualificationInvoiceCheckboxElement).toBeInTheDocument();
            expect(qualificationInvoiceCheckboxElement).toBeChecked();
            const qualificationHakenCheckboxElement = screen.getByRole(
                "checkbox",
                { name: "労働者派遣事業" }
            );
            expect(qualificationHakenCheckboxElement).toBeInTheDocument();
            expect(qualificationHakenCheckboxElement).toBeChecked();
            const exceptionalOrganizationsLabelElement =
                screen.getByText(/除外取引先/);
            expect(exceptionalOrganizationsLabelElement).toBeInTheDocument();
            const addExceptionalOrganizationButtonElement =
                screen.getByText(/取引先を追加/);
            expect(addExceptionalOrganizationButtonElement).toBeInTheDocument();
            expect(addExceptionalOrganizationButtonElement).not.toBeDisabled();
            const backButtonElement = screen.getByRole("button", {
                name: "戻る",
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const updateButtonElement = screen.getByRole("button", {
                name: "更 新",
            });
            expect(updateButtonElement).toBeInTheDocument();
            expect(updateButtonElement).not.toBeDisabled();
        });
    });
});
