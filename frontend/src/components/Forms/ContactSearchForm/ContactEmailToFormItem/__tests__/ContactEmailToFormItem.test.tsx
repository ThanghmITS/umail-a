import React from "react";
import { render, screen } from "~/test/utils";
import ContactEmailToFormItem from "../ContactEmailToFormItem";

const mockOnClear = jest.fn();

describe("ContactEmailToFormItem", () => {
    test("enabled render test", () => {
        render(<ContactEmailToFormItem onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /メールアドレス\(TO\)/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactEmailToFormItem disabled onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /メールアドレス\(TO\)/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).toBeDisabled();
    });
});
