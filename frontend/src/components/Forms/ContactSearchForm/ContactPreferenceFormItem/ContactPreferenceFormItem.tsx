import React from "react";
import { Col, Form, Row, Select, Tooltip } from "antd";
import { CONTACT_SEND_TYPE, Links, iconCustomColor } from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./ContactPreferenceFormItem.scss";

const { Option } = Select;

type Props = {
    disabled?: boolean;
};

const ContactPreferenceFormItem = ({ disabled }: Props) => {
    const fieldName = "contact_preference";
    const tags = CONTACT_SEND_TYPE;

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Select
                            className={styles.container}
                            placeholder="配信種別"
                            mode="multiple"
                            allowClear
                            tagRender={(props) => {
                                const tag = tags.find(
                                    (tag) => tag.value === props.value
                                );
                                return (
                                    <CustomSelectTag
                                        title={tag?.title}
                                        color={tag?.color}
                                        {...props}
                                    />
                                );
                            }}
                            disabled={disabled}>
                            {tags.map((tag) => {
                                return (
                                    <Select.Option key={tag.value} value={tag.value}>
                                        {tag.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactPreferenceFormItem;
