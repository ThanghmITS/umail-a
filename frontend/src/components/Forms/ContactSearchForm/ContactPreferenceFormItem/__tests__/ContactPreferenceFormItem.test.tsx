import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ContactPreferenceFormItem from "../ContactPreferenceFormItem";

describe("ContactPreferenceFormItem", () => {
    test("enabled render test", async () => {
        render(<ContactPreferenceFormItem />);
        const selectPlaceholderElement = screen.getByText(/配信種別/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        const roleSelectInputElement = screen.getByRole("combobox");
        expect(roleSelectInputElement).toBeInTheDocument();
        await userEvent.click(roleSelectInputElement);
        const jobSelectOptionElement = screen.getByText(/^案件$/);
        expect(jobSelectOptionElement).toBeInTheDocument();
        const personnelOptionElement = screen.getByText(/^要員$/);
        expect(personnelOptionElement).toBeInTheDocument();
    });

    test("disabled render test", () => {
        render(<ContactPreferenceFormItem disabled />);
        const selectElement = screen.getByRole("combobox");
        expect(selectElement).toBeDisabled();
    });
});
