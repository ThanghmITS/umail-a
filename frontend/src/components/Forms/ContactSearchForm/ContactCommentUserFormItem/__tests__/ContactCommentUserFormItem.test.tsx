import React from "react";
import ContactCommentUserFormItem from "../ContactCommentUserFormItem";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { generateRandomToken } from "~/utils/utils";

describe("ContactCommentUserFormItem.tsx", () => {
    test("enabled render test", async () => {
        renderWithAllProviders(<ContactCommentUserFormItem />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            contacts: {
                                create: true,
                                update: true,
                                delete: true,
                                column_setting: true,
                                csv_upload: true,
                                csv_download: true,
                                search_template: true,
                            },
                        },
                    },
                },
            }),
        });
        const selectPlaceholderElement = screen.getByText(/コメント作成者/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const userOptionElement = await screen.findByText(/example user 2/);
        expect(userOptionElement).toBeInTheDocument();
    });
    test("disable render test", async () => {
        renderWithAllProviders(<ContactCommentUserFormItem disabled />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            contacts: {
                                create: true,
                                update: true,
                                delete: true,
                                column_setting: true,
                                csv_upload: true,
                                csv_download: true,
                                search_template: true,
                            },
                        },
                    },
                },
            }),
        });
        const selectPlaceholderElement = screen.getByText(/コメント作成者/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const userOptionElement = screen.queryByText(/example user 2/);
        expect(userOptionElement).not.toBeInTheDocument();
    });
});
