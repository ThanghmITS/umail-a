import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ContactCategoryFormItem from "../ContactCategoryFormItem";

describe("ContactCategoryFormItem.tsx", () => {
    test("enable render test", async () => {
        render(<ContactCategoryFormItem />);
        const selectCategoryPlaceholderElement = screen.getByText(/相性/);
        expect(selectCategoryPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectCategoryPlaceholderElement);
        const categoryHeartOptionElement = screen.getByTestId("heart-option");
        expect(categoryHeartOptionElement).toBeInTheDocument();
        const categoryFrownOptionElement = screen.getByTestId("frown-option");
        expect(categoryFrownOptionElement).toBeInTheDocument();
        const selectConditionPlaceholderElement = screen.getByText(/条件/);
        expect(selectConditionPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectConditionPlaceholderElement);
        const conditionMatchOptionElement = screen.getByText(/と一致する/);
        expect(conditionMatchOptionElement).toBeInTheDocument();
        const conditionDoNotMatchOptionElement =
            screen.getByText(/と一致しない/);
        expect(conditionDoNotMatchOptionElement).toBeInTheDocument();
    });
    test("disable render test", () => {
        render(<ContactCategoryFormItem disabled />);
        const selectElements = screen.getAllByRole("combobox");
        selectElements.forEach((element) => {
            expect(element).toBeDisabled();
        });
    });
});
