import React from "react";
import { Col, Form, Row, Select, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    HeartTwoTone,
    FrownTwoTone,
} from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./ContactCategoryFormItem.scss";

const { Option } = Select;

type Props = {
    disabled?: boolean;
};

const ContactCategoryFormItem = ({ disabled }: Props) => {
    const categoryFieldName = "category";
    const categoryInqualityFieldName = "category_inequality";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} noStyle>
                        <Row>
                            <Col span={12}>
                                <Form.Item
                                    colon={false}
                                    name={categoryFieldName}
                                    noStyle>
                                    <Select
                                        placeholder="相性"
                                        allowClear
                                        className={styles.container}
                                        disabled={disabled}>
                                        <Select.Option value="heart" data-testid="heart-option">
                                            <HeartTwoTone twoToneColor="#eb2f96" />
                                        </Select.Option>
                                        <Select.Option value="frown" data-testid="frown-option">
                                            <FrownTwoTone />
                                        </Select.Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    colon={false}
                                    name={categoryInqualityFieldName}
                                    noStyle>
                                    <Select
                                        placeholder="条件"
                                        className={styles.container}
                                        disabled={disabled}>
                                        <Select.Option value="eq">と一致する</Select.Option>
                                        <Select.Option value="not_eq">
                                            と一致しない
                                        </Select.Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                相性はユーザー個人ごとに保存されているため、他のユーザーが登録した相性は検索対象外です。<br />
                                左の入力欄には検索対象とする相性を入力します。<br />
                                右側の入力欄には検索条件を入力します。<br />
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactCategoryFormItem;
