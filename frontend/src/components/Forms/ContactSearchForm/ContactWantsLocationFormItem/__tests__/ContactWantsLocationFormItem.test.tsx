import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ContactWantsLocationFormItem from "../ContactWantsLocationFormItem";

describe("ContactWantsLocationFormItem.tsx", () => {
    test("enabled render test", async () => {
        /**
         * NOTE(joshua-hashimoto):
         * 下でコメントアウトされているものは、スクロールが必要な領域のためDOMが検知できていないと思われる。そのためテストしようとしてもエラーになる。
         * not系は1つのリストなので、1つでもnot系がテストを通過できるということは他のnot系も問題なく存在すると推測できる。
         */
        render(<ContactWantsLocationFormItem />);
        const selectPlaceholderElement = screen.getByText(/^希望エリア$/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const isHokkaidoSelectOptionElement = screen.getByText(/^北海道$/);
        expect(isHokkaidoSelectOptionElement).toBeInTheDocument();
        const notHokkaidoSelectOptionElement = screen.getByText(/^not:北海道$/);
        expect(notHokkaidoSelectOptionElement).toBeInTheDocument();
        const isTohokuSelectOptionElement = screen.getByText(/^東北$/);
        expect(isTohokuSelectOptionElement).toBeInTheDocument();
        const notTohokuSelectOptionElement = screen.getByText(/^not:東北$/);
        expect(notTohokuSelectOptionElement).toBeInTheDocument();
        const isKantoSelectOptionElement = screen.getByText(/^関東$/);
        expect(isKantoSelectOptionElement).toBeInTheDocument();
        // const notKantoSelectOptionElement = screen.getByText(/^not:関東$/);
        // expect(notKantoSelectOptionElement).toBeInTheDocument();
        const isChubuSelectOptionElement = screen.getByText(/^中部$/);
        expect(isChubuSelectOptionElement).toBeInTheDocument();
        // const notChubuSelectOptionElement = screen.getByText(/^not:中部$/);
        // expect(notChubuSelectOptionElement).toBeInTheDocument();
        const isToukaiSelectOptionElement = screen.getByText(/^東海$/);
        expect(isToukaiSelectOptionElement).toBeInTheDocument();
        // const notToukaiSelectOptionElement = screen.getByText(/^not:東海$/);
        // expect(notToukaiSelectOptionElement).toBeInTheDocument();
        const isKansaiSelectOptionElement = screen.getByText(/^関西$/);
        expect(isKansaiSelectOptionElement).toBeInTheDocument();
        // const notKansaiSelectOptionElement = screen.getByText(/^not:関西$/);
        // expect(notKansaiSelectOptionElement).toBeInTheDocument();
        const isShikokuSelectOptionElement = screen.getByText(/^四国$/);
        expect(isShikokuSelectOptionElement).toBeInTheDocument();
        // const notShikokuSelectOptionElement = screen.getByText(/^not:四国$/);
        // expect(notShikokuSelectOptionElement).toBeInTheDocument();
        const isChugokuSelectOptionElement = screen.getByText(/^中国$/);
        expect(isChugokuSelectOptionElement).toBeInTheDocument();
        // const notChugokuSelectOptionElement = screen.getByText(/^not:中国$/);
        // expect(notChugokuSelectOptionElement).toBeInTheDocument();
        const isKyushuSelectOptionElement = screen.getByText(/^九州$/);
        expect(isKyushuSelectOptionElement).toBeInTheDocument();
        // const notKyushuSelectOptionElement = screen.getByText(/^not:九州$/);
        // expect(notKyushuSelectOptionElement).toBeInTheDocument();
        const isOtherSelectOptionElement = screen.getByText(/^その他$/);
        expect(isOtherSelectOptionElement).toBeInTheDocument();
        // const notOtherSelectOptionElement = screen.getByText(/^not:その他$/);
        // expect(notOtherSelectOptionElement).toBeInTheDocument();
    });
    test("disabled render test", async () => {
        render(<ContactWantsLocationFormItem disabled />);
        const selectPlaceholderElement = screen.getByText(/^希望エリア$/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        // NOTE(joshua-hashimoto): 上記でテストしている最初の要素がDOMに存在しないということ、click()してもselectのdropdownが表示されていないということ
        const isHokkaidoSelectOptionElement = screen.queryByText(/^北海道$/);
        expect(isHokkaidoSelectOptionElement).not.toBeInTheDocument();
    });
});
