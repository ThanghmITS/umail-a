import React from "react";
import { Col, Form, Select, Row, Tooltip } from "antd";
import { Links, NOT_WANTS_LOCATION, WANTS_LOCATION, iconCustomColor } from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./ContactWantsLocationFormItem.scss";

type Props = {
    disabled?: boolean;
};

const ContactWantsLocationFormItem = ({ disabled }: Props) => {
    const statuses = [...WANTS_LOCATION, ...NOT_WANTS_LOCATION];

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name="wants_location" noStyle>
                        <Select
                            className={styles.container}
                            placeholder="希望エリア"
                            mode="multiple"
                            allowClear
                            tagRender={(props) => {
                                const status = statuses.find(
                                    (status) => status.value === props.value
                                );
                                return (
                                    <CustomSelectTag
                                        color={status?.color}
                                        title={status?.title}
                                        {...props}
                                    />
                                );
                            }}
                            disabled={disabled}>
                            {statuses.map((item) => {
                                return (
                                    <Select.Option
                                        key={item.value}
                                        value={item.value}>
                                        {item.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactWantsLocationFormItem;
