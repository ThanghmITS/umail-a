import React from "react";
import { render, screen } from "~/test/utils";
import ContactOrganizationNameFormItem from "../ContactOrganizationNameFormItem";

const mockOnClear = jest.fn();

describe("ContactOrganizationNameFormItem", () => {
    test("enabled render test", () => {
        render(<ContactOrganizationNameFormItem onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /所属取引先/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(
            <ContactOrganizationNameFormItem disabled onClear={mockOnClear} />
        );
        const inputElement = screen.getByPlaceholderText(
            /所属取引先/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).toBeDisabled();
    });
});
