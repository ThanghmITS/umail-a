import React from "react";
import { Col, Form, Input, Row, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./ContactNameFormItem.scss";

type Props = {
    disabled?: boolean;
    onClear: (fieldName: string, value: string) => void;
};

const ContactNameFormItem = ({ disabled = false, onClear }: Props) => {
    const firstNameFieldName = "first_name";
    const lastNameFieldName = "last_name";
    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={lastNameFieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="取引先担当者名(姓)"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(lastNameFieldName, "");
                                }
                            }}
                            disabled={disabled}
                        />
                    </Form.Item>
                    <Form.Item colon={false} name={firstNameFieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="取引先担当者名(名)"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(firstNameFieldName, "");
                                }
                            }}
                            disabled={disabled}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                <a
                                    href={Links.helps.filter.partialMatch}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    部分一致検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactNameFormItem;
