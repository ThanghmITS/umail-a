import React from "react";
import { Col, Form, FormItemProps, Input, Row } from "antd";
import { ErrorMessages, HANKAKU_NUMBER_REGEX } from "~/utils/constants";
import styles from "./FaxInputFormItem.scss";

type Props = FormItemProps & {};

const FaxInputFormItem = ({ ...props }: Props) => {
    return (
        <Form.Item label="FAX" className={styles.field} {...props}>
            <Col span={24}>
                <Row>
                    <Col span={7}>
                        <Form.Item
                            className={styles.field}
                            name="fax1"
                            dependencies={["fax2", "fax3"]}
                            rules={[
                                ({ getFieldValue }) => ({
                                    validator: (_, value) => {
                                        const fax2 = getFieldValue("fax2");
                                        const fax3 = getFieldValue("fax3");
                                        const values = [value, fax2, fax3];
                                        const isNumber = values
                                            .map((value) => {
                                                const isNumber =
                                                    HANKAKU_NUMBER_REGEX.test(
                                                        value
                                                    );
                                                return !value || isNumber;
                                            })
                                            .every((value) => value);
                                        if (!isNumber) {
                                            return Promise.reject(
                                                new Error(
                                                    ErrorMessages.form.numberOnly
                                                )
                                            );
                                        }

                                        // NOTE(joshua-hashimoto): 入力欄が全て入力されているか全て空の場合はエラーではない
                                        const isAllFilled = values.every(
                                            (value) => !!value
                                        );

                                        if (props.required && !isAllFilled) {
                                            return Promise.reject(
                                                new Error(
                                                    ErrorMessages.form.required
                                                )
                                            );
                                        }

                                        if (
                                            isAllFilled &&
                                            values.join("").length > 15
                                        ) {
                                            return Promise.reject(
                                                new Error(
                                                    "3つの入力欄を合わせて" +
                                                        ErrorMessages.validation
                                                            .length.max15
                                                )
                                            );
                                        }

                                        const isAllUnfilled = values.every(
                                            (value) => !value
                                        );
                                        if (isAllFilled || isAllUnfilled) {
                                            return Promise.resolve();
                                        }

                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.form.faxNotComplete
                                            )
                                        );
                                    },
                                }),
                            ]}
                            noStyle>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col flex="auto">
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100%" }}>
                            <div>-</div>
                        </Row>
                    </Col>
                    <Col span={7}>
                        <Form.Item
                            label=" "
                            className={styles.field}
                            name="fax2"
                            noStyle>
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col flex="auto">
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100%" }}>
                            -
                        </Row>
                    </Col>
                    <Col span={7}>
                        <Form.Item
                            label=" "
                            className={styles.field}
                            name="fax3"
                            noStyle>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Form.Item>
    );
};

export default FaxInputFormItem;
