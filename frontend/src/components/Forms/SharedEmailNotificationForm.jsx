import React from "react";
import PropTypes from "prop-types";

import {
    Button,
    Form,
    Input,
    Select,
    Row,
    Col,
    message,
    Radio,
    Switch,
} from "antd";

import BaseForm from "./base/BaseForm";
import styles from "./EditForm.scss";
import { finalized } from "./helpers";

import {
    PlusOutlined,
    CheckOutlined,
    CloseOutlined,
    MinusOutlined,
} from "@ant-design/icons";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import validateJapaneseMessages from "./validateMessages";
import BackButton from "../Common/BackButton/BackButton";
import Paths from "../Routes/Paths";

const { Option } = Select;

const formLayoutLabelSpan = 5;
const formLayout = {
    labelCol: {
        span: formLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - formLayoutLabelSpan,
    },
};

const inputColXs = 16;
const inputColSm = 17;
const inputColMd = 18;
const inputColLg = 19;
const inputColXl = 20;
const inputColXxl = 21;

const inputCol = {
    xs: inputColXs,
    sm: inputColSm,
    md: inputColMd,
    lg: inputColLg,
    xl: inputColXl,
    xxl: inputColXxl,
};

const controlButtonCol = {
    xs: 24 - inputColXs,
    sm: 24 - inputColSm,
    md: 24 - inputColMd,
    lg: 24 - inputColLg,
    xl: 24 - inputColXl,
    xxl: 24 - inputColXxl,
};

export const target_types = [
    { value: "from_address", name: "差出人" },
    { value: "subject", name: "件名" },
    { value: "content", name: "本文" },
    { value: "attachment", name: "添付" },
];

export const condition_types = [
    { target_type: "from_address", value: "include", name: "に次を含む" },
    { target_type: "from_address", value: "exclude", name: "に次を含まない" },
    { target_type: "from_address", value: "equal", name: "が次と一致する" },
    { target_type: "from_address", value: "not_equal", name: "が次と異なる" },
    { target_type: "from_address", value: "start_with", name: "が次で始まる" },
    { target_type: "from_address", value: "end_with", name: "が次で終わる" },
    { target_type: "subject", value: "include", name: "に次を含む" },
    { target_type: "subject", value: "exclude", name: "に次を含まない" },
    { target_type: "subject", value: "equal", name: "が次と一致する" },
    { target_type: "subject", value: "not_equal", name: "が次と異なる" },
    { target_type: "subject", value: "start_with", name: "が次で始まる" },
    { target_type: "subject", value: "end_with", name: "が次で終わる" },
    { target_type: "content", value: "include", name: "に次を含む" },
    { target_type: "content", value: "exclude", name: "に次を含まない" },
    { target_type: "attachment", value: "exists", name: "あり" },
    { target_type: "attachment", value: "not_exists", name: "なし" },
];

class SharedEmailNotificationForm extends BaseForm {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.initialViewRendered = false;
    }

    state = {
        errorFields: [],
        ruleKeys: [],
        isActive: true,
    };

    handleChange(index, val) {
        const { setFieldsValue } = this.baseform.current;
        if (val == "keyword") {
            setFieldsValue({ [`rules[${index}].target`]: "content" });
            setFieldsValue({ [`rules[${index}].value`]: "" });
            setFieldsValue({ [`rules[${index}].condition`]: "include" });
        } else if (val == "attachment") {
            setFieldsValue({ [`rules[${index}].target`]: "attachment" });
            setFieldsValue({ [`rules[${index}].value`]: "0" });
            setFieldsValue({ [`rules[${index}].condition`]: "exists" });
        }
        this.forceUpdate(); // 再描画しないと選択肢の内容が切り替わらないため
    }

    handleChangeTarget(index, val) {
        const { setFieldsValue } = this.baseform.current;
        this.forceUpdate(); // 再描画しないと選択肢の内容が切り替わらないため
    }

    removeIcon = (key) => (
        <Button
            className={styles.button}
            size="small"
            onClick={() => this.remove(key)}
            icon={<MinusOutlined />}
        />
    );

    add = (targetKey) => {
        const { ruleKeys } = this.state;
        const lastIndex =
            ruleKeys.slice(-1)[0] !== undefined ? ruleKeys.slice(-1)[0] : -1;
        const nextKeys = ruleKeys.concat([lastIndex + 1]);
        this.setState({ ruleKeys: nextKeys });

        const new_rules = [];
        const rules = this.getCurrentRules();
        rules.forEach((rule) => {
            if (rule.key > targetKey) {
                new_rules.push({
                    key: rule.key + 1,
                    target: rule.target,
                    value: rule.value,
                    condition: rule.condition,
                });
            } else {
                new_rules.push({
                    key: rule.key,
                    target: rule.target,
                    value: rule.value,
                    condition: rule.condition,
                });
            }
        });
        new_rules.push({
            key: targetKey + 1,
            target: undefined,
            value: undefined,
            condition: undefined,
        });

        new_rules.forEach((rule) => {
            this.resetFieldValue(rule, rule.key);
        });
    };

    remove = (targetKey) => {
        const { ruleKeys } = this.state;
        if (ruleKeys.length === 1) {
            customErrorMessage('条件をこれ以上減らせません。(最低1つは必要)');
            return;
        }
        this.setState({
            ruleKeys: ruleKeys
                .filter((key) => key !== targetKey)
                .map((value, index) => index),
        });

        const rules = this.getCurrentRules();
        let new_index = 0;
        rules.forEach((rule) => {
            if (rule.key !== targetKey) {
                this.resetFieldValue(rule, new_index);
                new_index++;
            }
        });
        this.resetFieldValue(
            { target: "", value: "", condition: "" },
            rules.length
        );
    };

    getCurrentRules = () => {
        const { ruleKeys } = this.state;
        return ruleKeys.map((rulekey, index) => {
            return {
                key:
                    this.getCurrentFieldValue(`rules[${index}].key`) > 0
                        ? this.getCurrentFieldValue(`rules[${index}].key`)
                        : 0,
                target: this.getCurrentFieldValue(`rules[${index}].target`),
                value: this.getCurrentFieldValue(`rules[${index}].value`),
                condition: this.getCurrentFieldValue(
                    `rules[${index}].condition`
                ),
            };
        });
    };

    resetFieldValue = (rule, index) => {
        const { setFieldsValue } = this.baseform.current;
        setFieldsValue({ [`rules[${index}].key`]: index });
        setFieldsValue({ [`rules[${index}].target`]: rule.target });
        setFieldsValue({ [`rules[${index}].value`]: rule.value });
        setFieldsValue({ [`rules[${index}].condition`]: rule.condition });
    };

    componentDidMount() {
        this.add();
    }

    componentDidUpdate() {
        const { initialData } = this.props;
        const { setFieldsValue } = this.baseform.current;
        if (
            !this.initialViewRendered &&
            initialData &&
            initialData.rules &&
            this.baseform.current
        ) {
            setFieldsValue(initialData);
            initialData.rules.forEach((rule, index) => {
                this.resetFieldValue(rule, index);
            });
            this.setState({
                ruleKeys: initialData.rules.map((x) => x.key),
                isActive: initialData.is_active,
            });
            this.initialViewRendered = true;
        }
    }

    getCurrentFieldError = (name) => {
        return (
            this.baseform.current &&
            this.baseform.current.getFieldError(name).length > 0
        );
    };

    getCurrentFieldValue = (name) => {
        return this.baseform.current &&
            this.baseform.current.getFieldValue(name)
            ? this.baseform.current.getFieldValue(name)
            : undefined;
    };

    handleSubmitError = ({ values, errorFields, outOfDate }) => {
        if (errorFields) {
            let errorFieldNames = errorFields.map((field) => {
                return field["name"][0];
            });
            this.setState({ errorFields: errorFieldNames }); // stateに変更を入れないとエラーが画面に反映されないため
        }
    };

    handleSubmitWithRules = (event) => {
        const { isActive } = this.state;
        this.baseform.current.validateFields().then((values) => {
            values["rules"] = this.getCurrentRules();
            values["is_active"] = isActive;
            this.submitHandler(values);
        });
    };

    onChangeActiveSwitch = () => {
        const { isActive } = this.state;
        this.setState({ isActive: !isActive });
    };

    render() {
        const { initialData, fieldErrors, created, deleteButton } = this.props;
        const { ruleKeys, isActive } = this.state;

        // Caliculate the dynamic field first.
        const ruleItems = ruleKeys.map((key, index) => (
            <Col key={key} span={24} style={{ marginBottom: "1%" }}>
                <Row gutter={5}>
                    <Col {...inputCol}>
                        <Row>
                            <Col span={8}>
                                <Form.Item
                                    colon={false}
                                    name={`rules[${index}].target`}
                                    rules={[
                                        {
                                            required: true,
                                            message: "この項目は必須です。",
                                        },
                                    ]}
                                    required={false}
                                    style={{ marginBottom: 0 }}>
                                    <Select
                                        placeholder="対象"
                                        allowClear
                                        onChange={this.handleChangeTarget.bind(
                                            this,
                                            index
                                        )}
                                        disabled={!isActive}>
                                        {target_types.map((type) => (
                                            <Select.Option
                                                key={type.value}
                                                value={type.value}>
                                                {type.name}
                                            </Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    colon={false}
                                    name={`rules[${index}].condition`}
                                    rules={[
                                        {
                                            required: true,
                                            message: "この項目は必須です。",
                                        },
                                    ]}
                                    required={false}
                                    style={{ marginBottom: 0 }}>
                                    <Select
                                        allowClear
                                        disabled={!isActive}
                                        placeholder="条件">
                                        {condition_types
                                            .filter(
                                                (condition_type) =>
                                                    condition_type.target_type ===
                                                    this.getCurrentFieldValue(
                                                        `rules[${index}].target`
                                                    )
                                            )
                                            .map((type) => (
                                                <Select.Option
                                                    key={type.value}
                                                    value={type.value}>
                                                    {type.name}
                                                </Select.Option>
                                            ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col
                                span={8}
                                style={{
                                    display:
                                        this.getCurrentFieldValue(
                                            `rules[${index}].target`
                                        ) == "attachment"
                                            ? "none"
                                            : "",
                                }}>
                                <Form.Item
                                    colon={false}
                                    name={`rules[${index}].value`}
                                    rules={[
                                        {
                                            required:
                                                this.getCurrentFieldValue(
                                                    `rules[${index}].target`
                                                ) != "attachment",
                                            message: "この項目は必須です。",
                                        },
                                    ]}
                                    required={false}
                                    validateStatus={
                                        fieldErrors.conditions &&
                                        fieldErrors.conditions[index]
                                            ? "error"
                                            : "success"
                                    }
                                    help={
                                        fieldErrors.conditions &&
                                        fieldErrors.conditions[index]
                                            ? fieldErrors.conditions[index]
                                            : undefined
                                    }
                                    style={{ marginBottom: 0 }}>
                                    <Input
                                        placeholder="値"
                                        allowClear
                                        disabled={!isActive}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                    <Col {...controlButtonCol}>
                        <Row align="middle">
                            <Col span={12}>
                                <Form.Item colon={false} noStyle>
                                    <Button
                                        type="primary"
                                        onClick={() => this.add(key)}
                                        disabled={!isActive}
                                        icon={<PlusOutlined />}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item colon={false} noStyle>
                                    <Button
                                        onClick={() => this.remove(key)}
                                        disabled={!isActive}
                                        danger
                                        type="primary"
                                        icon={<MinusOutlined />}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        ));

        return (
            <Form
                onFinish={this.handleSubmitWithRules}
                onFinishFailed={this.handleSubmitError}
                ref={this.baseform}
                style={{ textAlign: "left" }}
                validateMessages={validateJapaneseMessages}>
                <Col span={14}>
                    <Row justify="start">
                        <Col span={24}>
                            <Form.Item
                                {...formLayout}
                                label="有効"
                                className={styles.field}
                                name="is_active">
                                <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    onChange={this.onChangeActiveSwitch}
                                    checked={isActive}
                                />
                            </Form.Item>
                            <Form.Item
                                {...formLayout}
                                label="ルール名"
                                className={styles.field}
                                name="name"
                                validateStatus={
                                    fieldErrors.name ? "error" : "success"
                                }
                                help={fieldErrors.name}
                                rules={[
                                    {
                                        required: true,
                                        message: "この項目は必須です。",
                                    },
                                ]}>
                                <Input disabled={!isActive} allowClear />
                            </Form.Item>
                            <Form.Item
                                {...formLayout}
                                label="ルール条件"
                                className={styles.field}
                                name="master_rule"
                                rules={[
                                    {
                                        required: true,
                                        message: "この項目は必須です。",
                                    },
                                ]}>
                                <Radio.Group
                                    disabled={!isActive}
                                    style={{
                                        marginTop: "5px",
                                        marginBottom: "3%",
                                    }}>
                                    <Radio value={"all"}>
                                        全ての条件に一致
                                    </Radio>
                                    <Radio value={"any"}>
                                        いずれかの条件に一致
                                    </Radio>
                                </Radio.Group>
                            </Form.Item>
                            <Row>
                                <Col
                                    push={formLayoutLabelSpan}
                                    span={24 - formLayoutLabelSpan}>
                                    {ruleItems}
                                </Col>
                            </Row>
                        </Col>
                        <Col span={24} style={{ marginTop: "5%" }}>
                            <Row justify="start">
                                <Col span={12}>
                                    <Row justify="start">
                                        <BackButton
                                            to={Paths.sharedMailNotifications}
                                        />
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            disabled={created}
                                            style={{ marginRight: "5px" }}>
                                            {initialData.id ? "更新" : "登録"}
                                        </Button>
                                    </Row>
                                </Col>
                                <Col span={12}>
                                    <Row justify="end">{deleteButton}</Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Form>
        );
    }
}

SharedEmailNotificationForm.propTypes = {
    initialData: PropTypes.shape({
        // Corresponds to backend API.
        name: PropTypes.string,
        rules: PropTypes.arrayOf(
            PropTypes.shape({
                type: PropTypes.string,
                value: PropTypes.string,
                target: PropTypes.string,
                condition: PropTypes.string,
            })
        ),
    }).isRequired, // Override in child class and use PropTypes.shape instead.
    fieldErrors: PropTypes.shape({
        name: PropTypes.arrayOf(PropTypes.string),
        rules: PropTypes.arrayOf(
            PropTypes.shape({
                type: PropTypes.arrayOf(PropTypes.string),
                value: PropTypes.arrayOf(PropTypes.string),
                target: PropTypes.arrayOf(PropTypes.string),
                condition: PropTypes.arrayOf(PropTypes.string),
            })
        ),
    }).isRequired,
    submitHandler: PropTypes.func.isRequired,
    resetFormHandler: PropTypes.func.isRequired,
    created: PropTypes.bool,
    deleteButton: PropTypes.object,
};

SharedEmailNotificationForm.defaultProps = {
    created: false,
};

const ScheduledEmailNotificationFormWrapper = finalized(
    SharedEmailNotificationForm
);
export default ScheduledEmailNotificationFormWrapper;
