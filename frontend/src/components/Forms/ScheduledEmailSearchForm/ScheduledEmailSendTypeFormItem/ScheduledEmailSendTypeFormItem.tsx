import React from "react";
import { Col, Form, Select, Row, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, SCHEDULED_EMAIL_SEND_TYPE, iconCustomColor } from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import styles from "./ScheduledEmailSendTypeFormItem.scss";

const { Option } = Select;

type Props = {
    disabled?: boolean;
};

const ScheduledEmailSendTypeFormItem = ({ disabled }: Props) => {
    const fieldName = "send_type";

    const sendTypes = SCHEDULED_EMAIL_SEND_TYPE;

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Select
                            className={styles.container}
                            placeholder="配信種別"
                            mode="multiple"
                            allowClear
                            tagRender={(props) => {
                                const sendType = sendTypes.find(
                                    (sendType) => sendType.value === props.value
                                );
                                return (
                                    <CustomSelectTag
                                        color={sendType?.color}
                                        title={sendType?.title}
                                        {...props}
                                    />
                                );
                            }}
                            disabled={disabled}>
                            {sendTypes.map((sendType) => {
                                return (
                                    <Option
                                        key={sendType.value}
                                        value={sendType.value}>
                                        {sendType.title}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailSendTypeFormItem;
