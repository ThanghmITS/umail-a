import React from "react";
import { Col, Form, Row, Tooltip } from "antd";
import CustomRangePicker from "~/components/Common/CustomRangePicker/CustomRangePicker";
import { QuestionCircleFilled } from "@ant-design/icons";
import { disabledFutureDates } from "~/utils/utils";
import { iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailSentDateFormItem.scss";

type Props = {
    disabled?: boolean;
};

const ScheduledEmailSentDateFormItem = ({ disabled }: Props) => {
    const fieldName = "send_date";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <CustomRangePicker
                            className={styles.container}
                            placeholder={[
                                "配信完了日(開始)",
                                "配信完了日(終了)",
                            ]}
                            disabled={disabled}
                            inputReadOnly
                            disabledDate={disabledFutureDates}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                左の入力欄には開始日を入力します。
                                <br />
                                例：「2020-02-04」を入力した場合、2020年2月4日以降に配信が完了した配信メールが対象となります。
                                <br />
                                右の入力欄には終了日を入力します。
                                <br />
                                例：「2020-02-04」を入力した場合、2020年2月4日以前に配信が完了した配信メールが対象となります。
                                <br />
                                ※未来日付を選択することはできません。
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailSentDateFormItem;
