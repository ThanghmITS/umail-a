import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Badge, Button, Col, Form, Row, Tooltip } from "antd";
import { CloseOutlined, SearchOutlined } from "@ant-design/icons";
import moment from "moment";
import { Endpoint } from "~/domain/api";
import { SCHEDULED_MAIL_SEARCH_PAGE } from "~/components/Pages/pageIds";
import ScheduledEmailReservationDateFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailReservationDateFormItem/ScheduledEmailReservationDateFormItem";
import ScheduledEmailSendTypeFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSendTypeFormItem/ScheduledEmailSendTypeFormItem";
import ScheduledEmailSubjectFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSubjectFormItem/ScheduledEmailSubjectFormItem";
import ScheduledEmailTextFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailTextFormItem/ScheduledEmailTextFormItem";
import ScheduledEmailAttachmentsFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailAttachmentsFormItem/ScheduledEmailAttachmentsFormItem";
import ScheduledEmailSenderFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSenderFormItem/ScheduledEmailSenderFormItem";
import ScheduledEmailStatusFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailStatusFormItem/ScheduledEmailStatusFormItem";
import ScheduledEmailSentDateFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSentDateFormItem/ScheduledEmailSentDateFormItem";
import ScheduledEmailTextFormatFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailTextFormatFormItem/ScheduledEmailTextFormatFormItem";
import ScheduledEmailSendTotalCountFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSendTotalCountFormItem/ScheduledEmailSendTotalCountFormItem";
import ScheduledEmailOpenCountFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailOpenCountFormItem/ScheduledEmailOpenCountFormItem";
import ScheduledEmailCreatedUserFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailCreatedUserFormItem/ScheduledEmailCreatedUserFormItem";
import ScheduledEmailModifiedUserFormItem from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailModifiedUserFormItem/ScheduledEmailModifiedUserFormItem";
import SearchTemplateSelectFormItem from "~/components/Forms/SearchTemplateSelectFormItem/SearchTemplateSelectFormItem";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import SearchMenuDrawer from "~/components/Common/SearchMenuDrawer/SearchMenuDrawer";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { useSearchTemplateUtils } from "~/hooks/useSearchTemplate";
import { useSearchSync } from "~/hooks/useSearch";
import styles from "./ScheduledEmailSearchForm.scss";

const pageId = SCHEDULED_MAIL_SEARCH_PAGE;
const filterType = "search";
const searchTemplateURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.scheduledEmailSearchTemplate
}`;
const templateReducerName = "scheduledEmailSearchPage";
const scheduledMails = "scheduledMails";

const emptyData = {
    attachments: undefined,
    date_range: [],
    send_type: [],
    sender_id: undefined,
    sent_date: [],
    status: [],
    text_format: [],
    subject: undefined,
    text: undefined,
    send_total_count: 0,
    send_total_count_gt: undefined,
    send_total_count_lt: undefined,
    open_count: 0,
    open_count_gt: undefined,
    open_count_lt: undefined,
    created_user: [],
    modified_user: [],
};

const ScheduledEmailSearchForm = React.forwardRef(
    (
        {
            tableName,
            initialData = emptyData,
            submitHandler,
            isDefaultTemplateAttached,
            searchConditionSanitizer,
        },
        ref
    ) => {
        const [form] = Form.useForm();
        const { displaySetting } = useSelector(
            (state) => state.displaySettingPage
        );
        const [isSearchDrawerOpen, setIsSearchDrawerOpen] = useState(false);
        const [unshowSearchFields, setUnshowSearchFields] = useState([]);
        const [filledSearchFieldCount, setFilledSearchFieldCount] = useState(0);
        const [selectedTemplateName, setSelectedTemplateName] =
            useState(undefined);
        const { updateDisplaySetting } = useDisplaySettingAPI();
        const { saveCurrentSearchCondition } = useSearchTemplateUtils();
        const { syncToUrl, queryParamsToObj } = useSearchSync({
            parseBooleans: false,
        });

        const countFilterField = (formValues) => {
            let count = 0;
            if (!!formValues.send_type && !!formValues.send_type.length) {
                count += 1;
            }
            if (!!formValues.subject) {
                count += 1;
            }
            if (!!formValues.text) {
                count += 1;
            }
            if (!!formValues.attachments) {
                count += 1;
            }
            if (!!formValues.status && !!formValues.status.length) {
                count += 1;
            }
            if (!!formValues.sender_id) {
                count += 1;
            }
            if (!!formValues.date_range && !!formValues.date_range.length) {
                count += 1;
            }
            if (!!formValues.sent_date && !!formValues.sent_date.length) {
                count += 1;
            }
            if (!!formValues.text_format && !!formValues.text_format.length) {
                count += 1;
            }
            if (
                !!formValues.send_total_count_gt ||
                !!formValues.send_total_count_lt
            ) {
                count += 1;
            }
            if (
                !!formValues.send_success_count_gt ||
                !!formValues.send_success_count_lt
            ) {
                count += 1;
            }
            if (
                !!formValues.send_error_count_gt ||
                !!formValues.send_error_count_lt
            ) {
                count += 1;
            }
            if (!!formValues.open_count_gt || !!formValues.open_count_lt) {
                count += 1;
            }
            if (!!formValues.created_user && !!formValues.created_user.length) {
                count += 1;
            }
            if (
                !!formValues.modified_user &&
                !!formValues.modified_user.length
            ) {
                count += 1;
            }
            return count;
        };

        const submitToFilter = useCustomDebouncedCallback((values) => {
            syncToUrl(values);
            saveCurrentSearchCondition(
                scheduledMails,
                selectedTemplateName,
                values
            );
            submitHandler(values);
            const count = countFilterField(values);
            setFilledSearchFieldCount(count);
        });

        const onReset = () => {
            form.setFieldsValue(emptyData);
            setSelectedTemplateName(undefined);
            submitHandler(emptyData);
            syncToUrl({});
            const count = countFilterField({});
            setFilledSearchFieldCount(count);
            saveCurrentSearchCondition(
                scheduledMails,
                selectedTemplateName,
                {}
            );
        };

        const onClear = async (fieldKey, clearValue) => {
            try {
                const values = await form.validateFields();
                values[fieldKey] = clearValue;
                submitHandler(values);
            } catch (err) {
                console.error(err);
            }
        };

        const onTemplateSelect = (templateName, templateValues) => {
            if (!templateValues) {
                let retrievedObject = localStorage.getItem(
                    scheduledMails + "_default"
                );
                localStorage.setItem(scheduledMails + "_selected", "");
                const parsedObj = JSON.parse(retrievedObject);
                const newObject = {
                    ...emptyData,
                    ...parsedObj,
                };
                submitHandler(newObject);
                return;
            }
            // NOTE(joshua-hashimoto): 日付系の入力値は文字列からMomentに変換してあげる必要がある
            if (templateValues && templateValues.date_range) {
                templateValues.date_range = templateValues.date_range.map(
                    (date) => moment(date)
                );
            }
            if (templateValues && templateValues.sent_date) {
                templateValues.sent_date = templateValues.sent_date.map(
                    (date) => moment(date)
                );
            }
            const formValues = templateValues
                ? {
                      ...emptyData,
                      ...templateValues,
                  }
                : emptyData;
            var retrievedObject = localStorage.getItem(
                scheduledMails + "_" + templateName
            );
            localStorage.setItem(scheduledMails + "_selected", templateName);
            const parsedObj = JSON.parse(retrievedObject);
            const newObject = {
                ...formValues,
                ...parsedObj,
            };
            const newData = searchConditionSanitizer(
                unshowSearchFields,
                newObject
            );
            form.setFieldsValue(newData);
            submitHandler(newData);
            syncToUrl(newData);
            const count = countFilterField(newData);
            setFilledSearchFieldCount(count);
        };

        const menuItems = [
            {
                fieldKey: "send_type",
                searchField: ScheduledEmailSendTypeFormItem,
                fieldNames: ["send_type"],
                onClear: undefined,
            },
            {
                fieldKey: "subject",
                searchField: ScheduledEmailSubjectFormItem,
                fieldNames: ["subject"],
                onClear,
            },
            {
                fieldKey: "text",
                searchField: ScheduledEmailTextFormItem,
                fieldNames: ["text"],
                onClear,
            },
            {
                fieldKey: "attachments",
                searchField: ScheduledEmailAttachmentsFormItem,
                fieldNames: ["attachments"],
                onClear: undefined,
            },
            {
                fieldKey: "status",
                searchField: ScheduledEmailStatusFormItem,
                fieldNames: ["status"],
                onClear: undefined,
            },
            {
                fieldKey: "sender",
                searchField: ScheduledEmailSenderFormItem,
                fieldNames: ["sender_id"],
                onClear: undefined,
            },
            {
                fieldKey: "date_to_send",
                searchField: ScheduledEmailReservationDateFormItem,
                fieldNames: ["date_range"],
                onClear: undefined,
            },
            {
                fieldKey: "send_date",
                searchField: ScheduledEmailSentDateFormItem,
                fieldNames: ["send_date"],
                onClear: undefined,
            },
            {
                fieldKey: "text_format",
                searchField: ScheduledEmailTextFormatFormItem,
                fieldNames: ["text_format"],
                onClear: undefined,
            },
            {
                fieldKey: "send_total_count",
                searchField: ScheduledEmailSendTotalCountFormItem,
                fieldNames: ["send_total_count_gt", "send_total_count_lt"],
                onClear,
            },
            {
                fieldKey: "open_count",
                searchField: ScheduledEmailOpenCountFormItem,
                fieldNames: ["open_count_gt", "open_count_lt"],
                onClear,
            },
            {
                fieldKey: "created_user",
                searchField: ScheduledEmailCreatedUserFormItem,
                fieldNames: ["created_user"],
                onClear: undefined,
            },
            {
                fieldKey: "modified_user",
                searchField: ScheduledEmailModifiedUserFormItem,
                fieldNames: ["modified_user"],
                onClear: undefined,
            },
        ];

        const onUpdateSearchField = () => {
            const postData = {
                content: {
                    scheduled_mails: {
                        search: [...unshowSearchFields],
                    },
                },
            };
            updateDisplaySetting(postData);
        };

        const onDrawerClose = () => {
            setIsSearchDrawerOpen(false);
        };

        const onFieldAdd = (newUnshowFields, fieldKey) => {
            setUnshowSearchFields(newUnshowFields);
        };

        const onFieldRemove = (newUnshowFields, fieldNames) => {
            setUnshowSearchFields(newUnshowFields);
            form.resetFields(fieldNames);
            const values = form.getFieldsValue();
            submitHandler(values);
            syncToUrl(values);
        };

        useEffect(() => {
            const tempData = { ...initialData };
            const dateRange = tempData.date_range ?? [];
            if (dateRange.length) {
                tempData.date_range = dateRange.map((date) => moment(date));
            }
            const sentDate = tempData.sent_date ?? [];
            if (sentDate.length) {
                tempData.sent_date = sentDate.map((date) => moment(date));
            }
            const queryParamsObj = queryParamsToObj();
            const formValues = {
                ...tempData,
                ...queryParamsObj,
            };
            form.setFieldsValue(formValues);
            const count = countFilterField(formValues);
            setFilledSearchFieldCount(count);
            submitHandler(formValues);
            syncToUrl(formValues);
        }, []);

        useEffect(() => {
            setInitialFormSearch();
        }, [displaySetting]);

        const setInitialFormSearch = () => {
            const dataExist =
                displaySetting &&
                displaySetting[tableName] &&
                displaySetting[tableName][filterType];
            const unshowSearchFields = dataExist
                ? displaySetting[tableName][filterType]
                : [];
            setUnshowSearchFields(unshowSearchFields);
        };
        return (
            <>
                <Col>
                    <Row justify="end">
                        <Button.Group>
                            <Button
                                type="primary"
                                size="small"
                                icon={<SearchOutlined />}
                                onClick={() => setIsSearchDrawerOpen(true)}>
                                絞り込み検索
                            </Button>
                            {filledSearchFieldCount ? (
                                <Tooltip title="現在検索中のフィールドを全てクリアします">
                                    <Badge
                                        count={filledSearchFieldCount}
                                        size="small">
                                        <Button
                                            type="primary"
                                            size="small"
                                            icon={<CloseOutlined />}
                                            onClick={onReset}
                                        />
                                    </Badge>
                                </Tooltip>
                            ) : undefined}
                        </Button.Group>
                    </Row>
                </Col>
                <SearchMenuDrawer
                    isDrawerOpen={isSearchDrawerOpen}
                    onDrawerClose={onDrawerClose}
                    form={form}
                    menuItems={menuItems}
                    unshowList={unshowSearchFields}
                    setUnshowList={setUnshowSearchFields}
                    setInitialFormSearch={setInitialFormSearch}
                    onReset={onReset}
                    onUpdateUnshowList={onUpdateSearchField}
                    otherControls={[]}
                    onFilter={submitToFilter}
                    searchTemplate={
                        <SearchTemplateSelectFormItem
                            pageId={pageId}
                            searchTemplateUrl={searchTemplateURL}
                            tableName={tableName}
                            reducerName={templateReducerName}
                            onSelect={onTemplateSelect}
                            onDelete={onReset}
                            isDefaultTemplateAttached={
                                isDefaultTemplateAttached
                            }
                            selectedTemplateName={selectedTemplateName}
                            setSelectedTemplateName={setSelectedTemplateName}
                        />
                    }
                    onFieldAdd={onFieldAdd}
                    onFieldRemove={onFieldRemove}
                />
            </>
        );
    }
);

export default ScheduledEmailSearchForm;
