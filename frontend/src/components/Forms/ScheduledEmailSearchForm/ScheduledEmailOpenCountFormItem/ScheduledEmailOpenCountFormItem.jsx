import React from "react";
import { Col, Form, Row, Input, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailOpenCountFormItem.scss";

const ScheduledEmailOpenCountFormItem = ({ disabled = false, onClear }) => {
    const maxOpenCountFieldName = "open_count_gt";
    const minOpenCountFieldName = "open_count_lt";

    return (
        <Col
            span={24}
            style={{
                marginBottom: "1%",
            }}>
            <Form.Item
                colon={false}
                name="open_count"
                wrapperCol={{ span: 24 }}
                noStyle>
                <Row className={styles.container} align="middle">
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={maxOpenCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="開封数(下限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(maxOpenCountFieldName, "");
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1}>
                        <Row justify="center">
                            <span>〜</span>
                        </Row>
                    </Col>
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={minOpenCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="開封数(上限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(minOpenCountFieldName, "");
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    左の入力欄には下限値を入力します。<br />
                                    例：「50」を入力した場合、開封数50件以上が対象となります。<br />
                                    右の入力欄には上限値を入力します。<br />
                                    例：「50」を入力した場合、開封数50件以下が対象となります。
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default ScheduledEmailOpenCountFormItem;
