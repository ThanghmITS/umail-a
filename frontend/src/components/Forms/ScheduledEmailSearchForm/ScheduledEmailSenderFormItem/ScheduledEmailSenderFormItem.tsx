import React from "react";
import { Col, Form, Row, Tooltip } from "antd";
import { Endpoint } from "~/domain/api";
import { QuestionCircleFilled } from "@ant-design/icons";
import AjaxSelect from "../../ajax/AjaxSelect";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailSenderFormItem.scss";

type Props = {
    disabled?: boolean;
};

const ScheduledEmailSenderFormItem = ({ disabled }: Props) => {
    const fieldName = "sender_id";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Row className={styles.container}>
                        <Col span={24}>
                            <Form.Item colon={false} name={fieldName} noStyle>
                                <AjaxSelect
                                    pageSize={1000}
                                    resourceUrl={`${Endpoint.getBaseUrl()}/${
                                        Endpoint.users
                                    }?is_active=true`}
                                    displayKey="display_name"
                                    searchParam="full_name"
                                    mode="multiple"
                                    placeholder="配信者"
                                    disabled={disabled}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                無効化されているユーザーは選択表示されません。
                                <br />
                                <a
                                    href={Links.helps.users.activeToggle}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    詳細
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled
                            style={{ color: iconCustomColor }}
                            className={styles.tooltip}
                        />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailSenderFormItem;
