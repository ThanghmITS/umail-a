import React, { ChangeEvent } from "react";
import { Button, Col, Form, Row, Tooltip } from "antd";
import CommentForm from "../CommentForm/CommentForm";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import styles from "./CommentUpdateForm.scss";

type Props = {
    value: string;
    onInputChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    onUpdate: () => void;
    onCancel: () => void;
    isPinned: boolean;
    onPinnedChange: () => void;
    onSelectCommentTemplate: () => void;
    commentError?: string;
    childrenId?: string;
};
const CommentUpdateForm = ({
    value,
    onInputChange,
    onUpdate,
    onCancel,
    isPinned,
    onPinnedChange,
    onSelectCommentTemplate,
    commentError,
    childrenId
}: Props) => {
    const debounceInputChange = useCustomDebouncedCallback(onInputChange, 300);
    const { update: updateAuthorization } = useAuthorizedActions("comment");

    const renderCommentUpdateButton = () => {
        const button = (
            <Button
                className={styles.controlButton}
                size="small"
                onClick={onCancel}
                disabled={!updateAuthorization}>
                キャンセル
            </Button>
        );
        if (!updateAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        return button;
    };

    return (
        <CommentForm
            value={value}
            onInputChange={debounceInputChange}
            isPinned={isPinned}
            onPinnedChange={onPinnedChange}
            onSelectCommentTemplate={onSelectCommentTemplate}
            commentError={commentError}
            disableIconPin={childrenId ? true : false}>
            <Row>
                <Col>
                    <Form.Item>
                        <Button
                            htmlType="button"
                            onClick={onUpdate}
                            type="primary"
                            size="small"
                            disabled={!value || value.length > 1000}>
                            コメントを更新
                        </Button>
                    </Form.Item>
                </Col>
                <Col className={styles.editCancelButton}>
                    <Form.Item>{renderCommentUpdateButton()}</Form.Item>
                </Col>
            </Row>
        </CommentForm>
    );
};

export default CommentUpdateForm;
