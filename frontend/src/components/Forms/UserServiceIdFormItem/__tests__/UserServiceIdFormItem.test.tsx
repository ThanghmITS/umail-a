import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import UserServiceIdFormItem from "../UserServiceIdFormItem";

describe("UserServiceIdFormItem.tsx", () => {
    const userServiceIdInputTestId = "user-service-id-input";
    const userServiceTooltipTestId = "user-service-id-tooltip";

    test("render test", async () => {
        render(<UserServiceIdFormItem />);
        const labelElement = screen.getByText("ユーザーID");
        expect(labelElement).toBeInTheDocument();
        const inputElement = screen.getByTestId(userServiceIdInputTestId);
        expect(inputElement).toBeInTheDocument();
        expect(inputElement).not.toHaveValue();
        const tooltipElement = screen.getByTestId(userServiceTooltipTestId);
        expect(tooltipElement).toBeInTheDocument();
        await userEvent.hover(tooltipElement);
        const tooltipLine1Element = await screen.findByText(
            /ここで設定したユーザーIDはメンション機能で使用されます。/
        );
        expect(tooltipLine1Element).toBeInTheDocument();
        const tooltipLine2Element = await screen.findByText(
            /15文字以内の大小英数字とアンダースコア\(_\)のみ使用可能です。/
        );
        expect(tooltipLine2Element).toBeInTheDocument();
        const tooltipLine3Element = await screen.findByText(
            /また、テナント内で同一のユーザーIDを設定することはできません/
        );
        expect(tooltipLine3Element).toBeInTheDocument();
    });
});
