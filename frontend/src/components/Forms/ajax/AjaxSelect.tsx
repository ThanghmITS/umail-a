import React, { useEffect, useState } from "react";
import { Select, SelectProps, Spin, Tooltip } from "antd";
import { simpleFetch } from "../../../actions/data";
import { DisplayTextAndForeignKeyModel } from "~/models/displayTextAndForeignKeyModel";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";

const { Option } = Select;

type APIModelSignature<K extends string> = {
    [key in K]: string;
};

type APIModel = APIModelSignature<string> & {
    id: string;
};

type Props = SelectProps<any> & {
    pageSize?: number;
    resourceUrl: string;
    displayKey: string;
    itemUpdateKey?: string;
    defaultSelect?:
        | DisplayTextAndForeignKeyModel
        | DisplayTextAndForeignKeyModel[];
    rebuildItems?: (
        values: DisplayTextAndForeignKeyModel[]
    ) => DisplayTextAndForeignKeyModel[]; // This function can be used to rebuild 'items' received from API.
    searchParam?: string;
};

const AjaxSelect = ({
    pageSize = 100,
    resourceUrl,
    displayKey,
    itemUpdateKey,
    defaultSelect,
    rebuildItems,
    searchParam,
    value = undefined,
    placeholder = "クリックして選択",
    disabled,
    ...props
}: Props) => {
    const token = useSelector((state: RootState) => state.login.token);
    const [items, setItems] = useState<DisplayTextAndForeignKeyModel[]>([]);
    const [isFetching, setIsFetching] = useState(true);
    const [searchValue, setSearchValue] = useState("");

    const currentPage = (
        items: DisplayTextAndForeignKeyModel[],
        defaultSelect?:
            | DisplayTextAndForeignKeyModel
            | DisplayTextAndForeignKeyModel[]
    ) => {
        let easyPage = 0;
        if (defaultSelect && defaultSelect.constructor.name === "Array") {
            const defaultModels: DisplayTextAndForeignKeyModel[] =
                defaultSelect as DisplayTextAndForeignKeyModel[];
            easyPage = (items.length - defaultModels.length) / pageSize;
        } else if (
            defaultSelect &&
            defaultSelect.constructor.name === "Object"
        ) {
            easyPage = (items.length - 1) / pageSize;
        } else {
            easyPage = items.length / pageSize;
        }
        if (!easyPage) {
            return 1;
        }
        const page = Math.ceil(easyPage) + 1;
        return page;
    };

    const fetchData = async (page: number, value = "") => {
        try {
            let url = resourceUrl;
            if (url.includes("?")) {
                url += `&page_size=${pageSize}&page=${page}`;
            } else {
                url += `?page_size=${pageSize}&page=${page}`;
            }
            if (searchParam) {
                url += `&${searchParam}=${value}`;
            }
            const response: { results: APIModel[] } = await simpleFetch(
                token,
                url
            );
            const data = response.results;
            const apiDatas: DisplayTextAndForeignKeyModel[] = data.map(
                (entry) => ({
                    displayText: entry[displayKey],
                    foreignKey: entry.id,
                })
            );
            const rebuildedItems = rebuildItems
                ? rebuildItems(apiDatas)
                : apiDatas;

            const isSearchChanged = searchValue !== value;
            let newItems = [];
            if (isSearchChanged) {
                newItems = [...rebuildedItems];
            } else {
                newItems = [...items, ...rebuildedItems];
            }
            if (defaultSelect === undefined) {
                setItems(newItems);
            } else {
                const apiValues = newItems.map((item) => item.foreignKey);
                if (defaultSelect.constructor.name === "Array") {
                    const val =
                        defaultSelect as DisplayTextAndForeignKeyModel[];
                    const validVals = val.filter(
                        (displayTextAndForeignKeyModel) =>
                            displayTextAndForeignKeyModel.displayText &&
                            displayTextAndForeignKeyModel.foreignKey
                    );
                    const filtered = validVals.filter(
                        (validVal) => !apiValues.includes(validVal.foreignKey)
                    );
                    newItems = [...filtered, ...newItems];
                } else {
                    const val = defaultSelect as DisplayTextAndForeignKeyModel;
                    if (
                        val.displayText &&
                        val.foreignKey &&
                        !apiValues.includes(val.foreignKey)
                    ) {
                        newItems = [val, ...newItems];
                    }
                }
                setItems(newItems);
            }
        } catch (err) {
            // NOTE(joshua-hashimoto): あえて何もしない
        }
        setIsFetching(false);
    };

    const fetchDataFromAPI = useCustomDebouncedCallback(
        (value) => fetchData(currentPage(items, defaultSelect), value),
        1000
    );
    const searchAPIValue = useCustomDebouncedCallback((value) => {
        if (searchValue !== value) {
            fetchData(1, value);
        } else {
            fetchData(currentPage(items, defaultSelect), value);
        }
        setSearchValue(value);
    });

    const onScroll = (event: any) => {
        const target = event.target;
        if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
            setIsFetching(true);
            fetchDataFromAPI(searchValue);
        }
    };

    useEffect(() => {
        fetchDataFromAPI();
    }, [itemUpdateKey]);

    const renderSelect = () => {
        return (
            <Select
                showSearch
                filterOption={(input, option) => {
                    return (
                        option?.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                    );
                }} // Search by displayName, partial match.
                allowClear
                style={{ width: "100%" }}
                {...props}
                value={value}
                placeholder={placeholder}
                onPopupScroll={onScroll}
                dropdownRender={(originNode) => (
                    <Spin spinning={isFetching}>{originNode}</Spin>
                )}
                onSearch={searchAPIValue}
                onBlur={(_) => {
                    if (searchValue) {
                        searchAPIValue("");
                    }
                }}>
                {items.map((entry) => (
                    <Select.Option
                        key={entry.foreignKey}
                        value={entry.foreignKey}>
                        {entry.displayText}
                    </Select.Option>
                ))}
            </Select>
        );
    };
    return (
        <Spin size="small" spinning={isFetching}>
            {disabled ? (
                <Tooltip title={"特定の権限で操作できます"}>
                    {renderSelect()}
                </Tooltip>
            ) : (
                <>{renderSelect()}</>
            )}
        </Spin>
    );
};

export default AjaxSelect;
