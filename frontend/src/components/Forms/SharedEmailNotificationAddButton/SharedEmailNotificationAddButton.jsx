import React from "react";
import { Button } from "antd";
import { BellOutlined } from "@ant-design/icons";
import Paths from "../../Routes/Paths";

const SharedEmailNotificationAddButton = () => {
    return (
        <Button
            type="primary"
            icon={<BellOutlined />}
            href={Paths.sharedMailNotificationRegister}
            size="small">
            通知条件追加
        </Button>
    );
};

export default SharedEmailNotificationAddButton;
