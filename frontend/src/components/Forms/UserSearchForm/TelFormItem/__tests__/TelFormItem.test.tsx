import React from "react";
import { render, screen } from "~/test/utils";
import TelFormItem from "../TelFormItem";

const mockOnClear = jest.fn();

describe("TelFormItem.tsx", () => {
    test("enabled render test", () => {
        render(<TelFormItem onClear={mockOnClear} />);
        const tel1InputElement = screen.getByPlaceholderText(/TEL/);
        expect(tel1InputElement).toBeInTheDocument();
        expect(tel1InputElement).not.toBeDisabled();
        const tel2InputElement = screen.getByTestId("tel2");
        expect(tel2InputElement).toBeInTheDocument();
        expect(tel2InputElement).not.toBeDisabled();
        const tel3InputElement = screen.getByTestId("tel3");
        expect(tel3InputElement).toBeInTheDocument();
        expect(tel3InputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<TelFormItem disabled onClear={mockOnClear} />);
        const tel1InputElement = screen.getByPlaceholderText(/TEL/);
        expect(tel1InputElement).toBeInTheDocument();
        expect(tel1InputElement).toBeDisabled();
        const tel2InputElement = screen.getByTestId("tel2");
        expect(tel2InputElement).toBeInTheDocument();
        expect(tel2InputElement).toBeDisabled();
        const tel3InputElement = screen.getByTestId("tel3");
        expect(tel3InputElement).toBeInTheDocument();
        expect(tel3InputElement).toBeDisabled();
    });
});
