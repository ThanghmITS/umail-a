import React from "react";
import { Col, Row, Form, Input, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./UsernameFormItem.scss";

type Props = {
    disabled?: boolean;
    onClear: (fieldName: string, value: string) => void;
};

const UsernameFormItem = ({ disabled, onClear }: Props) => {
    const lastNameFieldName = "last_name";
    const firstNameFieldName = "first_name";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Form.Item wrapperCol={{ span: 24 }} noStyle>
                <Row className={styles.container}>
                    <Col span={23}>
                        <Row>
                            <Col span={12}>
                                <Form.Item
                                    colon={false}
                                    name={lastNameFieldName}
                                    noStyle>
                                    <Input
                                        className={styles.userInput}
                                        placeholder="ユーザー名(姓)"
                                        allowClear
                                        onChange={(event) => {
                                            const value = event.target.value;
                                            if (value === "") {
                                                onClear(lastNameFieldName, "");
                                            }
                                        }}
                                        disabled={disabled}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    colon={false}
                                    name={firstNameFieldName}
                                    noStyle>
                                    <Input
                                        className={styles.userInput}
                                        placeholder="ユーザー名(名)"
                                        allowClear
                                        onChange={(event) => {
                                            const value = event.target.value;
                                            if (value === "") {
                                                onClear(firstNameFieldName, "");
                                            }
                                        }}
                                        disabled={disabled}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                    <Col className={styles.infoIcon} span={1}>
                        <Tooltip
                            title={
                                <span>
                                    <a
                                        href={Links.helps.filter.partialMatch}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        部分一致検索
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default UsernameFormItem;
