import React from "react";
import { render, screen } from "~/test/utils";
import IncludeInvalidSwitchFormItem from "../IncludeInvalidSwitchFormItem";

describe("IncludeInvalidSwitchFormItem.tsx", () => {
    test("render test", () => {
        render(<IncludeInvalidSwitchFormItem />);
        const includeInvalidLabelElement = screen.getByText(/無効含/);
        expect(includeInvalidLabelElement).toBeInTheDocument();
        const includeINvalidSwitchElement = screen.getByRole("switch");
        expect(includeINvalidSwitchElement).toBeInTheDocument();
    });
});
