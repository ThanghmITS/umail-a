import React from "react";
import { Col, Row, Form, Tooltip, Input } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./EmailFormItem.scss";

type Props = {
    disabled?: boolean;
    onClear: (fieldName: string, value: string) => void;
};

const EmailFormItem = ({ disabled, onClear }: Props) => {
    const fieldName = "email";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Form.Item wrapperCol={{ span: 20 }} noStyle>
                <Row className={styles.container}>
                    <Col span={23}>
                        <Form.Item noStyle colon={false} name={fieldName}>
                            <Input
                                className={styles.userInput}
                                placeholder="メールアドレス"
                                onChange={(event) => {
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(fieldName, "");
                                    }
                                }}
                                allowClear
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    <a
                                        href={Links.helps.filter.partialMatch}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        部分一致検索
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default EmailFormItem;
