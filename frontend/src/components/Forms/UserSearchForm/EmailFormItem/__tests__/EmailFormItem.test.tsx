import React from "react";
import { render, screen } from "~/test/utils";
import EmailFormItem from "../EmailFormItem";

const onClear = jest.fn();

describe("EmailFormItem.tsx", () => {
    test("enabled render test", () => {
        render(<EmailFormItem onClear={onClear} />);
        const emailAddressInputElement =
            screen.getByPlaceholderText(/メールアドレス/);
        expect(emailAddressInputElement).toBeInTheDocument();
        expect(emailAddressInputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<EmailFormItem disabled onClear={onClear} />);
        const emailAddressInputElement =
            screen.getByPlaceholderText(/メールアドレス/);
        expect(emailAddressInputElement).toBeInTheDocument();
        expect(emailAddressInputElement).toBeDisabled();
    });
});
