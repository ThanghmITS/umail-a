import React, { ChangeEvent } from "react";
import { Button, Col, Form, Row, Tooltip } from "antd";
import CommentForm from "../CommentForm/CommentForm";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";

type Props = {
    value: string;
    onInputChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    onReply: () => void;
    isPinned: boolean;
    onPinnedChange: () => void;
    onSelectCommentTemplate: () => void;
    commentError?: string;
};
const CommentReplyForm = ({
    value,
    onInputChange,
    onReply,
    isPinned,
    onPinnedChange,
    onSelectCommentTemplate,
    commentError,
}: Props) => {
    const debounceInputChange = useCustomDebouncedCallback(onInputChange, 300);

    return (
        <div style={{marginLeft: "62px" }}>
          <CommentForm
            value={value}
            onInputChange={debounceInputChange}
            isPinned={isPinned}
            onPinnedChange={onPinnedChange}
            onSelectCommentTemplate={onSelectCommentTemplate}
            commentError={commentError}
            disableIconPin={true}>
            <Row>
                <Col>
                    <Form.Item>
                        <Button
                            htmlType="button"
                            onClick={onReply}
                            type="primary"
                            size="small"
                            disabled={!value || value.length > 1000}>
                            返信を追加
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
          </CommentForm>
        </div>
    );
};

export default CommentReplyForm;
