import React from "react";
import { Button, Col, Form, Input, Row } from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";
import { FormListFieldData } from "antd/lib/form/FormList";
import {
    ErrorMessages,
    HANKAKU_NUMBER_REGEX,
    RESTRICT_SPACE_REGEX,
} from "~/utils/constants";
import styles from "./OrganizationBranchField.scss";

const formItemLayoutLabelSpan = 3;
const formItemLayout = {
    labelCol: {
        span: formItemLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - formItemLayoutLabelSpan,
    },
};

type Props = FormListFieldData & {
    onAdd: (defaultValue?: any, insertIndex?: number) => void;
    onRemove: (fieldName: number) => void;
    isAddable: boolean;
};

const OrganizationBranchField = ({
    name,
    onAdd,
    onRemove,
    isAddable,
    ...props
}: Props) => {
    const parentFieldName = "branches";
    const branchNameFieldName = "name";
    const branchAddressFieldName = "address";
    const branchBuildingFieldName = "building";
    const branchTel1FieldName = "tel1";
    const branchTel2FieldName = "tel2";
    const branchTel3FieldName = "tel3";
    const branchFax1FieldName = "fax1";
    const branchFax2FieldName = "fax2";
    const branchFax3FieldName = "fax3";

    return (
        <Form.Item {...props} noStyle>
            <Col
                span={24}
                style={{
                    marginBottom: 30,
                }}>
                <Row>
                    <Col span={24}>
                        <Form.Item
                            {...formItemLayout}
                            label="支店名"
                            className={styles.field}
                            name={[name, branchNameFieldName]}
                            rules={[
                                { required: true },
                                {
                                    max: 100,
                                    message:
                                        ErrorMessages.validation.length.max100,
                                },
                                {
                                    pattern: RESTRICT_SPACE_REGEX,
                                    message:
                                        ErrorMessages.validation.regex.space,
                                },
                            ]}>
                            <Input placeholder="サンプル支店" />
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="住所"
                            className={styles.field}>
                            <Form.Item
                                name={[name, branchAddressFieldName]}
                                style={{ marginBottom: 0 }}
                                noStyle>
                                <Input placeholder="市区町村・町名・番地" />
                            </Form.Item>
                            <Form.Item
                                name={[name, branchBuildingFieldName]}
                                dependencies={[
                                    [
                                        parentFieldName,
                                        name,
                                        branchAddressFieldName,
                                    ],
                                ]}
                                rules={[
                                    ({ getFieldValue }) => ({
                                        validator: (_, value) => {
                                            const address = getFieldValue([
                                                parentFieldName,
                                                name,
                                                branchAddressFieldName,
                                            ]);
                                            if (
                                                !RESTRICT_SPACE_REGEX.test(
                                                    address
                                                )
                                            ) {
                                                return Promise.reject(
                                                    new Error(
                                                        ErrorMessages.validation.regex.space
                                                    )
                                                );
                                            }

                                            const building = !!value
                                                ? value
                                                : null;
                                            if (
                                                !RESTRICT_SPACE_REGEX.test(
                                                    building
                                                )
                                            ) {
                                                return Promise.reject(
                                                    new Error(
                                                        ErrorMessages.validation.regex.space
                                                    )
                                                );
                                            }

                                            if (
                                                (
                                                    (address ?? "") +
                                                    (building ?? "")
                                                ).length > 100
                                            ) {
                                                return Promise.reject(
                                                    new Error(
                                                        "建物名を合わせて" +
                                                            ErrorMessages
                                                                .validation
                                                                .length.max100
                                                    )
                                                );
                                            }

                                            return Promise.resolve();
                                        },
                                    }),
                                ]}
                                style={{ marginBottom: 0 }}
                                noStyle>
                                <Input placeholder="建物名" />
                            </Form.Item>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="TEL"
                            className={styles.field}>
                            <Row>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label="TEL"
                                        className={styles.field}
                                        name={[name, branchTel1FieldName]}
                                        dependencies={[
                                            [
                                                parentFieldName,
                                                name,
                                                branchTel2FieldName,
                                            ],
                                            [
                                                parentFieldName,
                                                name,
                                                branchTel3FieldName,
                                            ],
                                        ]}
                                        rules={[
                                            ({ getFieldValue }) => ({
                                                validator: (_, value) => {
                                                    const tel2 = getFieldValue([
                                                        parentFieldName,
                                                        name,
                                                        branchTel2FieldName,
                                                    ]);
                                                    const tel3 = getFieldValue([
                                                        parentFieldName,
                                                        name,
                                                        branchTel3FieldName,
                                                    ]);
                                                    const values = [
                                                        value,
                                                        tel2,
                                                        tel3,
                                                    ];
                                                    const isNumber = values
                                                        .map((value) => {
                                                            const isNumber =
                                                                HANKAKU_NUMBER_REGEX.test(
                                                                    value
                                                                );
                                                            return (
                                                                !value ||
                                                                isNumber
                                                            );
                                                        })
                                                        .every(
                                                            (value) => value
                                                        );
                                                    if (!isNumber) {
                                                        return Promise.reject(
                                                            new Error(
                                                                ErrorMessages.form.numberOnly
                                                            )
                                                        );
                                                    }

                                                    // NOTE(joshua-hashimoto): 入力欄が全て入力されているか全て空の場合はエラーではない
                                                    const isAllFilled =
                                                        values.every(
                                                            (value) => !!value
                                                        );
                                                    if (
                                                        isAllFilled &&
                                                        values.join("").length >
                                                            15
                                                    ) {
                                                        return Promise.reject(
                                                            new Error(
                                                                "3つの入力欄を合わせて" +
                                                                    ErrorMessages
                                                                        .validation
                                                                        .length
                                                                        .max15
                                                            )
                                                        );
                                                    }
                                                    const isAllUnfilled =
                                                        values.every(
                                                            (value) => !value
                                                        );
                                                    if (
                                                        isAllFilled ||
                                                        isAllUnfilled
                                                    ) {
                                                        return Promise.resolve();
                                                    }

                                                    return Promise.reject(
                                                        new Error(
                                                            ErrorMessages.form.telNotComplete
                                                        )
                                                    );
                                                },
                                            }),
                                        ]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                                <Col
                                    flex="auto"
                                    style={{
                                        alignSelf: "center",
                                        textAlign: "center",
                                    }}>
                                    <span className={styles.phonenumberDash}>
                                        -
                                    </span>
                                </Col>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label=" "
                                        className={styles.field}
                                        name={[name, branchTel2FieldName]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                                <Col
                                    flex="auto"
                                    style={{
                                        alignSelf: "center",
                                        textAlign: "center",
                                    }}>
                                    <span className={styles.phonenumberDash}>
                                        -
                                    </span>
                                </Col>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label=" "
                                        className={styles.field}
                                        name={[name, branchTel3FieldName]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="FAX"
                            className={styles.field}>
                            <Row>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label="FAX"
                                        className={styles.field}
                                        name={[name, branchFax1FieldName]}
                                        dependencies={[
                                            [
                                                parentFieldName,
                                                name,
                                                branchFax2FieldName,
                                            ],
                                            [
                                                parentFieldName,
                                                name,
                                                branchFax3FieldName,
                                            ],
                                        ]}
                                        rules={[
                                            ({ getFieldValue }) => ({
                                                validator: (_, value) => {
                                                    const fax2 = getFieldValue([
                                                        parentFieldName,
                                                        name,
                                                        branchFax2FieldName,
                                                    ]);
                                                    const fax3 = getFieldValue([
                                                        parentFieldName,
                                                        name,
                                                        branchFax3FieldName,
                                                    ]);
                                                    const values = [
                                                        value,
                                                        fax2,
                                                        fax3,
                                                    ];
                                                    const isNumber = values
                                                        .map((value) => {
                                                            const isNumber =
                                                                HANKAKU_NUMBER_REGEX.test(
                                                                    value
                                                                );
                                                            return (
                                                                !value ||
                                                                isNumber
                                                            );
                                                        })
                                                        .every(
                                                            (value) => value
                                                        );
                                                    if (!isNumber) {
                                                        return Promise.reject(
                                                            new Error(
                                                                ErrorMessages.form.numberOnly
                                                            )
                                                        );
                                                    }

                                                    // NOTE(joshua-hashimoto): 入力欄が全て入力されているか全て空の場合はエラーではない
                                                    const isAllFilled =
                                                        values.every(
                                                            (value) => !!value
                                                        );
                                                    if (
                                                        isAllFilled &&
                                                        values.join("").length >
                                                            15
                                                    ) {
                                                        return Promise.reject(
                                                            new Error(
                                                                "3つの入力欄を合わせて" +
                                                                    ErrorMessages
                                                                        .validation
                                                                        .length
                                                                        .max15
                                                            )
                                                        );
                                                    }
                                                    const isAllUnfilled =
                                                        values.every(
                                                            (value) => !value
                                                        );
                                                    if (
                                                        isAllFilled ||
                                                        isAllUnfilled
                                                    ) {
                                                        return Promise.resolve();
                                                    }

                                                    return Promise.reject(
                                                        new Error(
                                                            ErrorMessages.form.telNotComplete
                                                        )
                                                    );
                                                },
                                            }),
                                        ]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                                <Col
                                    flex="auto"
                                    style={{
                                        alignSelf: "center",
                                        textAlign: "center",
                                    }}>
                                    <span className={styles.phonenumberDash}>
                                        -
                                    </span>
                                </Col>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label=" "
                                        className={styles.field}
                                        name={[name, branchFax2FieldName]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                                <Col
                                    flex="auto"
                                    style={{
                                        alignSelf: "center",
                                        textAlign: "center",
                                    }}>
                                    <span className={styles.phonenumberDash}>
                                        -
                                    </span>
                                </Col>
                                <Col span={7}>
                                    <Form.Item
                                        {...formItemLayout}
                                        label=" "
                                        className={styles.field}
                                        name={[name, branchFax3FieldName]}
                                        noStyle>
                                        <Input maxLength={15} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form.Item>
                    </Col>
                </Row>
                <Row justify="end">
                    {isAddable ? (
                        <Form.Item noStyle>
                            <Button
                                onClick={onAdd}
                                type="primary"
                                icon={<PlusOutlined />}
                            />
                        </Form.Item>
                    ) : null}
                    <Form.Item noStyle>
                        <Button
                            onClick={() => onRemove(name)}
                            danger
                            type="primary"
                            icon={<MinusOutlined />}
                            style={{
                                marginLeft: 5,
                            }}
                        />
                    </Form.Item>
                </Row>
            </Col>
        </Form.Item>
    );
};

export default OrganizationBranchField;
