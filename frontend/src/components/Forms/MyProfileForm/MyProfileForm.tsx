import React, { useEffect, useState } from "react";
import {
    Button,
    Form,
    Input,
    Tooltip,
    Radio,
    Typography,
    Row,
    Col,
} from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import validateJapaneseMessages from "../validateMessages";
import BackButton from "../../Common/BackButton/BackButton";
import {
    ErrorMessages,
    RESTRICT_SPACE_REGEX,
    Links,
    ROLES,
    PASSWORD_REGEX,
    iconCustomColor,
} from "~/utils/constants";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import Paths from "~/components/Routes/Paths";
import {
    MyProfileFormFieldErrorResponseModel,
    MyProfileFormModel,
    MyProfileModel,
} from "~/models/myProfileModel";
import TelInputFormItem from "../TelInputFormItem/TelInputFormItem";
import ProfileThumbnailFormItem from "~/components/Common/ProfileThumbnailFormItem/ProfileThumbnailFormItem";
import { useDeleteThumbnailAPIMutation } from "~/hooks/useThumbnail";
import styles from "./MyProfileForm.scss";
import UserServiceIdFormItem from "../UserServiceIdFormItem/UserServiceIdFormItem";

const { TextArea } = Input;
const { Text } = Typography;

const formItemLayout = {
    labelCol: {
        xs: { span: 4 },
        sm: { span: 4 },
        md: { span: 4 },
    },
    wrapperCol: {
        span: 14,
    },
};

const buttonsLayout = {
    labelCol: {},
    wrapperCol: {
        xs: { span: 14 },
        sm: { span: 14 },
        md: { span: 11 },
    },
};

const emptyInitialData: MyProfileModel = {
    email: "",
    emailSignature: undefined,
    firstName: "",
    lastName: "",
    userServiceId: "",
    modifiedTime: "",
    modifiedUser: "",
    role: "guest",
    tel1: undefined,
    tel2: undefined,
    tel3: undefined,
};

type Props = {
    initialData?: MyProfileModel;
    fieldErrors?: MyProfileFormFieldErrorResponseModel;
    onFinish: (values: MyProfileFormModel) => void;
};

const MyProfileForm = ({
    initialData = emptyInitialData,
    fieldErrors,
    onFinish,
}: Props) => {
    const requiredFields = ["lastName", "firstName"];
    const [form] = Form.useForm<MyProfileFormModel>();
    const [thumbnailUrl, setThumbnailUrl] = useState<string | undefined>(
        undefined
    );
    const { mutate: deleteThumbnail } = useDeleteThumbnailAPIMutation();

    const onRemoveThumbnail = () => {
        const email = form.getFieldValue("email");
        deleteThumbnail(
            { email },
            {
                onSuccess: () => {
                    form.setFieldsValue({ avatar: undefined });
                    setThumbnailUrl(undefined);
                },
            }
        );
    };

    useEffect(() => {
        form.setFieldsValue(initialData);
        setThumbnailUrl(initialData.avatar);
    }, [initialData]);

    return (
        <Form
            onFinish={onFinish}
            form={form}
            className={styles.container}
            validateMessages={validateJapaneseMessages}
            labelAlign="right">
            {/* <ProfileThumbnailFormItem
                {...formItemLayout}
                className={styles.field}
                onRemoveThumbnail={onRemoveThumbnail}
                thumbnailUrl={thumbnailUrl}
                uploadProps={{
                    beforeUpload: (file) => {
                        form.setFieldsValue({
                            avatar: file,
                        });
                        // NOTE(joshua-hashimoto): falseを返却することでAntdのUploadの自動送信を阻止
                        return false;
                    },
                }}
            /> */}
            <Form.Item
                {...formItemLayout}
                label="ユーザー名"
                className={styles.field}
                validateStatus={fieldErrors?.name ? "error" : "success"}
                help={fieldErrors?.name}
                required>
                <Form.Item
                    name="lastName"
                    help={fieldErrors?.last_name}
                    dependencies={["firstName"]}
                    rules={[
                        ({ getFieldValue }) => ({
                            validator: (_, value) => {
                                const lastName = !!value ? value : null;
                                // NOTE(joshua-hashimoto): スペース制御
                                if (!RESTRICT_SPACE_REGEX.test(lastName)) {
                                    return Promise.reject(
                                        new Error(
                                            ErrorMessages.validation.regex.space
                                        )
                                    );
                                }

                                const firstNameValue =
                                    getFieldValue("firstName");
                                const firstName = !!firstNameValue
                                    ? firstNameValue
                                    : null;
                                // NOTE(joshua-hashimoto): スペース制御
                                if (!RESTRICT_SPACE_REGEX.test(firstName)) {
                                    return Promise.reject(
                                        new Error(
                                            ErrorMessages.validation.regex.space
                                        )
                                    );
                                }

                                const fullName = lastName + firstName;

                                // NOTE(joshua-hashimoto): 必須項目制御
                                if (!lastName || !firstName || !fullName) {
                                    return Promise.reject(
                                        new Error(ErrorMessages.form.required)
                                    );
                                }

                                // NOTE(joshua-hashimoto): 50文字制御
                                if (fullName.length > 50) {
                                    return Promise.reject(
                                        new Error(
                                            ErrorMessages.validation.length.max50
                                        )
                                    );
                                }
                                return Promise.resolve();
                            },
                        }),
                    ]}
                    noStyle>
                    <Input
                        style={{ width: "50%" }}
                        placeholder="姓"
                        allowClear
                    />
                </Form.Item>
                <Form.Item
                    name="firstName"
                    help={fieldErrors?.first_name}
                    noStyle>
                    <Input
                        style={{ width: "50%" }}
                        placeholder="名"
                        allowClear
                    />
                </Form.Item>
            </Form.Item>
            {/* <UserServiceIdFormItem {...formItemLayout} name="userServiceId" /> */}
            <Form.Item
                {...formItemLayout}
                label="メールアドレス"
                className={styles.field}
                name="email"
                initialValue={initialData.email}>
                <Input disabled placeholder="you@example.com" />
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label="権限"
                className={styles.field}
                help={fieldErrors?.role}
                name="role"
                initialValue={initialData.role}>
                <Radio.Group disabled>
                    {ROLES.map((role, index) => (
                        <Radio key={index} value={role.value}>
                            {role.title}
                        </Radio>
                    ))}
                </Radio.Group>
            </Form.Item>
            <TelInputFormItem
                {...formItemLayout}
                validateStatus={fieldErrors?.tel ? "error" : undefined}
                help={fieldErrors?.tel}
                ></TelInputFormItem>
            <Form.Item
                {...formItemLayout}
                label="パスワード"
                className={styles.field}>
                <Form.Item
                    validateStatus={fieldErrors?.password ? "error" : undefined}
                    help={fieldErrors?.password}
                    rules={[
                        {
                            required: false,
                            pattern: PASSWORD_REGEX,
                            message: ErrorMessages.validation.regex.password,
                        },
                    ]}
                    name="password"
                    noStyle>
                    <Input.Password
                        placeholder="大小英数字記号混在で10-50桁"
                        autoComplete="new-password"
                    />
                </Form.Item>
                <Text type="secondary" style={{ fontSize: 13 }}>
                    空欄の場合、パスワードは変更されません
                </Text>
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label={
                    <span>
                        メール署名&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    ここで設定をしたメール署名は
                                    <TooltipContentLink
                                        to={`${Paths.scheduledMails}/register`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="配信メール予約／編集"
                                    />
                                    で予約をしたメールに自動挿入されます。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.scheduledEmails
                                                .autoInsert
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{ color: iconCustomColor }}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                validateStatus={
                    fieldErrors?.email_signature ? "error" : "success"
                }
                help={fieldErrors?.email_signature}
                name="emailSignature"
                rules={[
                    {
                        max: 1000,
                        message: ErrorMessages.validation.length.max1000,
                    },
                ]}>
                <TextArea
                    data-testid="my-profile-form-email-signature"
                    rows={16}
                />
            </Form.Item>
            <Form.Item {...buttonsLayout} style={{ marginTop: "3%" }}>
                <Row justify="start">
                    <Col>
                        <BackButton />
                    </Col>
                    <Col>
                        <Form.Item shouldUpdate>
                            {() => (
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    disabled={
                                        !form.isFieldsTouched(
                                            requiredFields,
                                            true
                                        ) ||
                                        !!form
                                            .getFieldsError()
                                            .filter(
                                                ({ errors }) => errors.length
                                            ).length
                                    }>
                                    更新
                                </Button>
                            )}
                        </Form.Item>
                    </Col>
                </Row>
            </Form.Item>
        </Form>
    );
};

export default MyProfileForm;
