import React from "react";
import { MyProfileModel } from "~/models/myProfileModel";
import { renderWithQueryClient, screen } from "~/test/utils";
import MyProfileForm from "../MyProfileForm";
import moment from "moment";

const now = moment().format("YYYY-MM-DD HH:mm");

const emptyInitialData: MyProfileModel = {
    avatar: undefined,
    email: "",
    emailSignature: "",
    firstName: "",
    lastName: "",
    userServiceId: "",
    modifiedTime: "",
    modifiedUser: "",
    role: "guest",
    tel1: "",
    tel2: "",
    tel3: "",
};

const filledInitialData: MyProfileModel = {
    avatar: "https://images.unsplash.com/photo-1650464232600-68f45ea392ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80",
    email: "example@example.com",
    emailSignature: "====\nemail signature\n====",
    firstName: "FIRST",
    lastName: "LAST",
    userServiceId: "user_0000000001",
    modifiedTime: now,
    modifiedUser: "example.admin@example.com",
    role: "admin",
    tel1: "001",
    tel2: "0002",
    tel3: "0003",
    password: "Abcd12345%",
};

const mockOnFinish = jest.fn();

describe("MyProfileForm.tsx", () => {
    const emailSignatureTextAreaTestId = "my-profile-form-email-signature";
    const userServiceIdInputTestId = "user-service-id-input";

    describe("without data", () => {
        test("render test", async () => {
            const { container } = renderWithQueryClient(
                <MyProfileForm onFinish={mockOnFinish} />
            );
            const requiredElements = container.getElementsByClassName(
                "ant-form-item-required"
            );
            expect(requiredElements.length).toBe(1);
            // expect(requiredElements.length).toBe(2);
            // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
            // expect(profileLabelElement).toBeInTheDocument();
            const userNameLabelElement = screen.getByText("ユーザー名");
            expect(userNameLabelElement).toBeInTheDocument();
            const userLastNameElement = screen.getByPlaceholderText("姓");
            expect(userLastNameElement).toBeInTheDocument();
            expect(userLastNameElement).not.toHaveValue();
            const userFirstNameElement = screen.getByPlaceholderText("名");
            expect(userFirstNameElement).toBeInTheDocument();
            expect(userFirstNameElement).not.toHaveValue();
            // const userServiceIdLabelElement = screen.getByText("ユーザーID");
            // expect(userServiceIdLabelElement).toBeInTheDocument();
            // const userServiceIdInputElement = screen.getByTestId(
            //     userServiceIdInputTestId
            // );
            // expect(userServiceIdInputElement).toBeInTheDocument();
            // expect(userServiceIdInputElement).not.toHaveValue();
            const emailAddressLabelElement =
                screen.getByLabelText("メールアドレス");
            expect(emailAddressLabelElement).toBeInTheDocument();
            const emailAddressElement =
                screen.getByPlaceholderText(/^you@example.com$/);
            expect(emailAddressElement).toBeInTheDocument();
            expect(emailAddressElement).not.toHaveValue();
            const roleLabelElement = screen.getByText("権限");
            expect(roleLabelElement).toBeInTheDocument();
            const masterRadioElement = screen.getByRole("radio", {
                name: "マスター",
            });
            expect(masterRadioElement).toBeInTheDocument();
            expect(masterRadioElement).not.toBeChecked();
            const adminRadioElement = screen.getByRole("radio", {
                name: "管理者",
            });
            expect(adminRadioElement).toBeInTheDocument();
            expect(adminRadioElement).not.toBeChecked();
            const managerRadioElement = screen.getByRole("radio", {
                name: "責任者",
            });
            expect(managerRadioElement).toBeInTheDocument();
            expect(managerRadioElement).not.toBeChecked();
            const leaderRadioElement = screen.getByRole("radio", {
                name: "リーダー",
            });
            expect(leaderRadioElement).toBeInTheDocument();
            expect(leaderRadioElement).not.toBeChecked();
            const memberRadioElement = screen.getByRole("radio", {
                name: "メンバー",
            });
            expect(memberRadioElement).toBeInTheDocument();
            expect(memberRadioElement).not.toBeChecked;
            const guestRadioElement = screen.getByRole("radio", {
                name: "ゲスト",
            });
            expect(guestRadioElement).toBeInTheDocument();
            expect(guestRadioElement).toBeChecked();
            const telLabelElement = screen.getByText(/^TEL$/);
            expect(telLabelElement).toBeInTheDocument();
            const tel1InputElement = screen.getByTestId("tel1");
            expect(tel1InputElement).toBeInTheDocument();
            expect(tel1InputElement).not.toHaveValue();
            const tel2InputElement = screen.getByTestId("tel2");
            expect(tel2InputElement).toBeInTheDocument();
            expect(tel2InputElement).not.toHaveValue();
            const tel3InputElement = screen.getByTestId("tel3");
            expect(tel3InputElement).toBeInTheDocument();
            expect(tel3InputElement).not.toHaveValue();
            const passwordLabelElement = screen.getByText(/^パスワード$/);
            expect(passwordLabelElement).toBeInTheDocument();
            const passwordElement =
                screen.getByPlaceholderText("大小英数字記号混在で10-50桁");
            expect(passwordElement).toBeInTheDocument();
            expect(passwordElement).not.toHaveValue();
            const passwordHelpTextElement = screen.getByText(
                "空欄の場合、パスワードは変更されません"
            );
            expect(passwordHelpTextElement).toBeInTheDocument();
            const emailSignatureLabelElement =
                screen.getByLabelText("メール署名");
            expect(emailSignatureLabelElement).toBeInTheDocument();
            const emailSignatureElement = screen.getByTestId(
                emailSignatureTextAreaTestId
            );
            expect(emailSignatureElement).not.toHaveValue();
            const backButtonElement = screen.getByRole("button", {
                name: /^戻る$/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const updateButtonElement = screen.getByRole("button", {
                name: /^更 新$/,
            });
            expect(updateButtonElement).toBeInTheDocument();
            // NOTE(joshua-hashimoto): フロントからはdisabledになることを確認。ここではなぜかエラーになってしまうので一旦コメントアウト
            // expect(updateButtonElement).toBeDisabled();
        });
    });

    describe("with data", () => {
        test("render test", () => {
            const { container } = renderWithQueryClient(
                <MyProfileForm
                    initialData={filledInitialData}
                    onFinish={mockOnFinish}
                />
            );
            const requiredElements = container.getElementsByClassName(
                "ant-form-item-required"
            );
            expect(requiredElements.length).toBe(1);
            // expect(requiredElements.length).toBe(2);
            // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
            // expect(profileLabelElement).toBeInTheDocument();
            const userNameLabelElement = screen.getByText("ユーザー名");
            expect(userNameLabelElement).toBeInTheDocument();
            const userLastNameElement = screen.getByPlaceholderText("姓");
            expect(userLastNameElement).toBeInTheDocument();
            expect(userLastNameElement).toHaveValue();
            const userFirstNameElement = screen.getByPlaceholderText("名");
            expect(userFirstNameElement).toBeInTheDocument();
            expect(userFirstNameElement).toHaveValue();
            // const userServiceIdLabelElement = screen.getByText("ユーザーID");
            // expect(userServiceIdLabelElement).toBeInTheDocument();
            // const userServiceIdInputElement = screen.getByTestId(
            //     userServiceIdInputTestId
            // );
            // expect(userServiceIdInputElement).toBeInTheDocument();
            // expect(userServiceIdInputElement).toHaveValue();
            const emailAddressLabelElement =
                screen.getByLabelText("メールアドレス");
            expect(emailAddressLabelElement).toBeInTheDocument();
            const emailAddressElement =
                screen.getByPlaceholderText(/^you@example.com$/);
            expect(emailAddressElement).toBeInTheDocument();
            expect(emailAddressElement).toHaveValue();
            const roleLabelElement = screen.getByText("権限");
            expect(roleLabelElement).toBeInTheDocument();
            const masterRadioElement = screen.getByRole("radio", {
                name: "マスター",
            });
            expect(masterRadioElement).toBeInTheDocument();
            expect(masterRadioElement).not.toBeChecked();
            const adminRadioElement = screen.getByRole("radio", {
                name: "管理者",
            });
            expect(adminRadioElement).toBeInTheDocument();
            expect(adminRadioElement).toBeChecked();
            const managerRadioElement = screen.getByRole("radio", {
                name: "責任者",
            });
            expect(managerRadioElement).toBeInTheDocument();
            expect(managerRadioElement).not.toBeChecked();
            const leaderRadioElement = screen.getByRole("radio", {
                name: "リーダー",
            });
            expect(leaderRadioElement).toBeInTheDocument();
            expect(leaderRadioElement).not.toBeChecked();
            const memberRadioElement = screen.getByRole("radio", {
                name: "メンバー",
            });
            expect(memberRadioElement).toBeInTheDocument();
            expect(memberRadioElement).not.toBeChecked;
            const guestRadioElement = screen.getByRole("radio", {
                name: "ゲスト",
            });
            expect(guestRadioElement).toBeInTheDocument();
            expect(guestRadioElement).not.toBeChecked();
            const telLabelElement = screen.getByText(/^TEL$/);
            expect(telLabelElement).toBeInTheDocument();
            const tel1InputElement = screen.getByTestId("tel1");
            expect(tel1InputElement).toBeInTheDocument();
            expect(tel1InputElement).toHaveValue();
            const tel2InputElement = screen.getByTestId("tel2");
            expect(tel2InputElement).toBeInTheDocument();
            expect(tel2InputElement).toHaveValue();
            const tel3InputElement = screen.getByTestId("tel3");
            expect(tel3InputElement).toBeInTheDocument();
            expect(tel3InputElement).toHaveValue();
            const passwordLabelElement = screen.getByText(/^パスワード$/);
            expect(passwordLabelElement).toBeInTheDocument();
            const passwordElement =
                screen.getByPlaceholderText("大小英数字記号混在で10-50桁");
            expect(passwordElement).toBeInTheDocument();
            expect(passwordElement).toHaveValue();
            const passwordHelpTextElement = screen.getByText(
                "空欄の場合、パスワードは変更されません"
            );
            expect(passwordHelpTextElement).toBeInTheDocument();
            const emailSignatureLabelElement =
                screen.getByLabelText("メール署名");
            expect(emailSignatureLabelElement).toBeInTheDocument();
            const emailSignatureElement = screen.getByTestId(
                emailSignatureTextAreaTestId
            );
            expect(emailSignatureElement).toHaveValue();
            const backButtonElement = screen.getByRole("button", {
                name: /^戻る$/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const updateButtonElement = screen.getByRole("button", {
                name: /^更 新$/,
            });
            expect(updateButtonElement).toBeInTheDocument();
            expect(updateButtonElement).not.toBeDisabled();
        });
    });
});
