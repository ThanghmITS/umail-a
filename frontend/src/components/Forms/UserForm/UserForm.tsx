import React, { ReactNode, useEffect, useState } from "react";
import {
    Button,
    Form,
    Input,
    Switch,
    Tooltip,
    Row,
    Col,
    Radio,
    Typography,
} from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import BackButton from "~/components/Common/BackButton/BackButton";
import Paths from "~/components/Routes/Paths";
import {
    ErrorMessages,
    Links,
    ONLY_HANKAKU_REGEX,
    PASSWORD_REGEX,
    RESTRICT_SPACE_REGEX,
    ROLES,
    iconCustomColor,
    TooltipMessages,
} from "~/utils/constants";
import {
    UserFormFieldErrorResponseModel,
    UserFormModel,
    UserModel,
} from "~/models/userModel";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import TelInputFormItem from "../TelInputFormItem/TelInputFormItem";
import { useHistory, useParams } from "react-router-dom";
import {
    convertUserFormModelToUserUpdateRequestDataModel,
    useUserUpdateAPIMutation,
} from "~/hooks/useUser";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import GenericModalContent from "~/components/Modals/GenericModal/GenericModalContent/GenericModalContent";
import ProfileThumbnailFormItem from "~/components/Common/ProfileThumbnailFormItem/ProfileThumbnailFormItem";
import { useDeleteThumbnailAPIMutation } from "~/hooks/useThumbnail";
import UserServiceIdFormItem from "../UserServiceIdFormItem/UserServiceIdFormItem";
import ValidateTextArea from "~/components/Common/ValidateTextArea/ValidateTextArea";
import { useTypedSelector } from "~/models/store";
import styles from "./UserForm.scss";

const { TextArea } = Input;
const { Text } = Typography;

const formItemLayout = {
    labelCol: {
        xs: { span: 4 },
        sm: { span: 4 },
        md: { span: 4 },
    },
    wrapperCol: {
        span: 14,
    },
};

const buttonsLayout = {
    labelCol: {},
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
        md: { span: 24 },
    },
};

const emptyInitialData: UserModel = {
    id: "",
    email: "",
    email_signature: undefined,
    first_name: "",
    last_name: "",
    user_service_id: "",
    modified_time: "",
    modified_user: "",
    role: undefined,
    tel1: undefined,
    tel2: undefined,
    tel3: undefined,
    is_active: false,
};

type Props = {
    resourceURL: string;
    initialData?: UserModel;
    fieldErrors?: UserFormFieldErrorResponseModel;
    submitHandler: (values: UserFormModel) => void;
    created?: boolean;
    editable?: boolean;
    deleteButton: ReactNode;
};

const UserForm = ({
    resourceURL,
    initialData = emptyInitialData,
    fieldErrors,
    submitHandler,
    created = false,
    editable = true,
    deleteButton,
}: Props) => {
    const [form] = Form.useForm<UserFormModel>();
    const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
    const [thumbnailUrl, setThumbnailUrl] = useState<string | undefined>(
        undefined
    );
    const [contentTextArea, setContentTextArea] = useState("");
    const router = useHistory();
    const { id } = useParams<{ id: string }>();
    const { mutate: updateUser, error } = useUserUpdateAPIMutation();
    const { mutate: deleteThumbnail } = useDeleteThumbnailAPIMutation();

    const authorizedRoles = useAuthorizedActions("_editable_roles");
    const editAuthorized =
        initialData.role === undefined ||
        (authorizedRoles &&
            initialData.role &&
            authorizedRoles.includes(initialData.role));

    const isMasterUser = initialData.role && initialData.role === "master";
    const currentUserId = useTypedSelector((state) => state.login.userId);

    const onUpdateModalClose = () => {
        setIsUpdateModalOpen(false);
    };

    const onUpdateModalOpen = (values: UserFormModel) => {
        setIsUpdateModalOpen(true);
    };

    const renderUpdateModal = (values: UserFormModel) => {
        return (
            <GenericModal
                visible={isUpdateModalOpen}
                title="このユーザーを更新しますか？"
                type="warning"
                onOk={() => {
                    if (!initialData.id) {
                        return;
                    }
                    const data =
                        convertUserFormModelToUserUpdateRequestDataModel(
                            values
                        );
                    updateUser(
                        {
                            userId: initialData.id,
                            data,
                        },
                        {
                            onSuccess: (result) => {
                                router.push(Paths.users);
                            },
                            onSettled: () => {
                              setIsUpdateModalOpen(false);
                            },
                        }
                    );
                }}
                onCancel={onUpdateModalClose}>
                <GenericModalContent>
                    OKを押すと、更新が実行されます。
                    <br />
                    <br />
                    <span>
                        ※ユーザーを無効化する場合、関連リソースに配信ステータスが「下書き」「配信待ち」「配信中」の配信メールが含まれていると実行されません。
                    </span>
                    <br />
                    <span>
                        ※ユーザー無効化後は個人保有データが削除されます。
                    </span>
                    <br />
                    <span>
                        ※ユーザーを有効化する場合、ユーザー上限を超えていると実行されません。
                    </span>
                </GenericModalContent>
            </GenericModal>
        );
    };

    const onRemoveThumbnail = () => {
        const email = form.getFieldValue("email");
        deleteThumbnail(
            { email },
            {
                onSuccess: () => {
                    form.setFieldsValue({ avatar: undefined });
                    setThumbnailUrl(undefined);
                },
            }
        );
    };

    useEffect(() => {
        if (initialData && initialData.email) {
            form.setFieldsValue({ ...initialData, pk: id });
            setThumbnailUrl(initialData.avatar);
        }
    }, [initialData, id]);

    return (
        <>
            <Form
                onFinish={onUpdateModalOpen}
                form={form}
                className={styles.container}
                validateMessages={validateJapaneseMessages}
                labelAlign="right">
                {/* <ProfileThumbnailFormItem
                    {...formItemLayout}
                    className={styles.field}
                    onRemoveThumbnail={onRemoveThumbnail}
                    thumbnailUrl={thumbnailUrl}
                    uploadProps={{
                        beforeUpload: (file) => {
                            form.setFieldsValue({
                                avatar: file,
                            });
                            // NOTE(joshua-hashimoto): falseを返却することでAntdのUploadの自動送信を阻止
                            return false;
                        },
                    }}
                /> */}
                <Form.Item
                    {...formItemLayout}
                    label="ユーザー名"
                    className={styles.field}
                    validateTrigger={["onChange", "onSubmit"]}
                    validateStatus={
                      error?.response?.data.name ? "error" : "success"
                    }
                    help={error?.response?.data.name}
                    required>
                    <Form.Item
                        name="last_name"
                        help={error?.response?.data.last_name}
                        dependencies={["first_name"]}
                        rules={[
                            ({ getFieldValue }) => ({
                                validator: (_, value) => {
                                    const lastName = !!value ? value : null;
                                    // NOTE(joshua-hashimoto): スペース制御
                                    if (!RESTRICT_SPACE_REGEX.test(lastName)) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.regex.space
                                            )
                                        );
                                    }

                                    const firstNameValue =
                                        getFieldValue("first_name");
                                    const firstName = !!firstNameValue
                                        ? firstNameValue
                                        : null;
                                    // NOTE(joshua-hashimoto): スペース制御
                                    if (!RESTRICT_SPACE_REGEX.test(firstName)) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.regex.space
                                            )
                                        );
                                    }

                                    const fullName = lastName + firstName;

                                    // NOTE(joshua-hashimoto): 必須項目制御
                                    if (!lastName || !firstName || !fullName) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.form.required
                                            )
                                        );
                                    }

                                    // NOTE(joshua-hashimoto): 50文字制御
                                    if (fullName.length > 50) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.length.max50
                                            )
                                        );
                                    }
                                    return Promise.resolve();
                                },
                            }),
                        ]}
                        noStyle>
                        <Input
                            placeholder="姓"
                            allowClear
                            style={{ width: "50%" }}
                        />
                    </Form.Item>
                    <Form.Item
                        name="first_name"
                        help={error?.response?.data.first_name}
                        noStyle>
                        <Input
                            placeholder="名"
                            allowClear
                            style={{ width: "50%" }}
                        />
                    </Form.Item>
                </Form.Item>
                {/* <UserServiceIdFormItem {...formItemLayout} /> */}
                <Form.Item
                    {...formItemLayout}
                    label="メールアドレス"
                    className={styles.field}
                    validateStatus={
                        error?.response?.data.email ? "error" : undefined
                    }
                    help={
                        error?.response?.data.email ||
                        error?.response?.data.username
                    }
                    name="email"
                    rules={[
                        { required: true },
                        {
                            pattern: ONLY_HANKAKU_REGEX,
                            message: ErrorMessages.validation.regex.onlyHankaku,
                        },
                        {
                            pattern: RESTRICT_SPACE_REGEX,
                            message: ErrorMessages.validation.regex.space,
                        },
                        {
                            max: 100,
                            message: ErrorMessages.validation.length.max100,
                        },
                    ]}>
                    <Input placeholder="you@example.com" />
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="権限"
                    className={styles.field}
                    help={error?.response?.data.role}
                    name="role"
                    rules={[{ required: true }]}>
                    <Radio.Group>
                        {ROLES.map((role, index) => {
                            return (
                                <Radio
                                    key={`radio_role_${index}`}
                                    value={role.value}
                                    disabled={
                                        isMasterUser || role.value === "master"
                                    }>
                                    {role.title}
                                </Radio>
                            );
                        })}
                    </Radio.Group>
                </Form.Item>
                <TelInputFormItem
                    {...formItemLayout}
                    validateStatus={
                      error?.response?.data.tel ? "error" : undefined
                    }
                    help={error?.response?.data.tel}
                />
                <Form.Item
                    {...formItemLayout}
                    label="パスワード"
                    className={styles.field}>
                    <Form.Item
                        validateStatus={
                           error?.response?.data.password ? "error" : undefined
                        }
                        help={error?.response?.data.password}
                        rules={[
                            {
                                pattern: PASSWORD_REGEX,
                                message:
                                    ErrorMessages.validation.regex.password,
                            },
                        ]}
                        name="password"
                        noStyle>
                        <Input.Password
                            placeholder="大小英数字記号混在で10-50桁"
                            autoComplete="new-password"
                        />
                    </Form.Item>
                    <Text type="secondary" style={{ fontSize: 13 }}>
                        空欄の場合、パスワードは変更されません
                    </Text>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label={
                        <span>
                            メール署名&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        ここで設定をしたメール署名は
                                        <TooltipContentLink
                                            to={`${Paths.scheduledMails}/register`}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            title="配信メール予約／編集"
                                        />
                                        で予約をしたメールに自動挿入されます。
                                        <br />
                                        <a
                                            href={
                                                Links.helps.scheduledEmails
                                                    .autoInsert
                                            }
                                            target="_blank"
                                            rel="noopener noreferrer">
                                            詳細
                                        </a>
                                    </span>
                                }>
                                <QuestionCircleFilled
                                    style={{ color: iconCustomColor }}
                                />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    validateStatus={
                      error?.response?.data.email_signature
                      ? "error"
                      : undefined
                    }
                    help={error?.response?.data.email_signature}
                    name="email_signature"
                    rules={[
                        {
                            max: 1000,
                            message: ErrorMessages.validation.length.max1000,
                        },
                    ]}>
                    <TextArea
                        rows={16}
                        data-testid="user-form-email-signature"
                    />
                </Form.Item>
                <ValidateTextArea
                  contentTextArea={contentTextArea}
                  errorMessages={ErrorMessages.validation.length.max1000}
                  margin="132px"
                  className={styles.errMsgWidth + ' validate-textarea-error ant-col'}
                />
                <Form.Item
                    {...formItemLayout}
                    label="有効"
                    className={styles.field}
                    help={error?.response?.data.is_active}
                    valuePropName="checked"
                    name="is_active">
                    {
                        currentUserId === initialData.id && initialData.is_active
                            ? (
                                <Tooltip title={TooltipMessages.userEdit.cannotDisabledUser}>
                                    <Switch
                                        checkedChildren={<CheckOutlined />}
                                        unCheckedChildren={<CloseOutlined />}
                                        defaultChecked={initialData.is_active}
                                        disabled
                                    />
                                </Tooltip>)
                            : (
                                <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    defaultChecked={initialData.is_active}
                                />
                            )
                    }
                </Form.Item>
                <Form.Item hidden name="pk"></Form.Item>
                <Form.Item {...buttonsLayout} style={{ marginTop: "3%" }}>
                    <Row className={styles.listButton}>
                        <Col span={4}>
                            <Row justify="start">
                                <BackButton to={Paths.users} />
                                <Form.Item shouldUpdate>
                                    {() => (
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            className={styles.button}
                                            disabled={
                                                created ||
                                                !editable ||
                                                !editAuthorized ||
                                                !!form
                                                    .getFieldsError()
                                                    .filter(
                                                        ({ errors }) =>
                                                            errors.length
                                                    ).length
                                            }>
                                            {initialData.id ? "更新" : "登録"}
                                        </Button>
                                    )}
                                </Form.Item>
                            </Row>
                        </Col>
                        <Col span={14}>
                            <Row justify="end">{deleteButton}</Row>
                        </Col>
                    </Row>
                </Form.Item>
            </Form>
            {renderUpdateModal(form.getFieldsValue())}
        </>
    );
};

export default UserForm;
