import React from "react";
import { renderWithRedux, screen } from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { organizationSearchPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { Endpoint } from "~/domain/api";
import { ORGANIZATION_SEARCH_PAGE } from "~/components/Pages/pageIds";
import SearchTemplateSelectFormItem from "../SearchTemplateSelectFormItem";
import { configureStore } from "@reduxjs/toolkit";
import { generateRandomToken } from "~/utils/utils";

const mockOnSelect = jest.fn();
const mockOnDelete = jest.fn();
const mockSetSelectedTemplateName = jest.fn();

describe("SearchTemplateSelectFormItem.jsx", () => {
    test("render test", () => {
        const searchTemplateURL = `${Endpoint.getBaseUrl()}/${
            Endpoint.organizationSearchTemplate
        }`;
        renderWithRedux(
            <SearchTemplateSelectFormItem
                pageId={ORGANIZATION_SEARCH_PAGE}
                searchTemplateUrl={searchTemplateURL}
                tableName=""
                reducerName="organizationSearchPage"
                onSelect={mockOnSelect}
                onDelete={mockOnDelete}
                isDefaultTemplateAttached={false}
                selectedTemplateName={undefined}
                setSelectedTemplateName={mockSetSelectedTemplateName}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        organizationSearchPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: generateRandomToken(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                organizations: {
                                    create: true,
                                    update: true,
                                    delete: true,
                                    column_setting: true,
                                    csv_upload: true,
                                    csv_download: true,
                                    blacklist: true,
                                    search_template: true,
                                },
                            },
                        },
                        organizationSearchPage: {
                            ...SearchPageInitialState,
                        },
                    },
                }),
            }
        );
        const selectPlaceholderElement =
            screen.getByText(/検索条件テンプレートを選択/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        const deleteButtonElement = screen.getByTestId(
            "delete-search-template-button-icon"
        );
        expect(deleteButtonElement).toBeInTheDocument();
        const starButtonElement = screen.getByTestId(
            "favorite-search-template-button-icon"
        );
        expect(starButtonElement).toBeInTheDocument();
        const saveButtonElement = screen.getByTestId(
            "save-search-template-button-icon"
        );
        expect(saveButtonElement).toBeInTheDocument();
    });
});
