import React from "react";
import { render, screen } from "~/test/utils";
import TemplateControlButton from "../TemplateControlButton";
import { PlusCircleFilled } from "@ant-design/icons";

const mockOnClick = jest.fn();

describe("TemplateControlButton.tsx", () => {
    test("enabled render test", () => {
        render(
            <TemplateControlButton
                message="tooltip message"
                icon={<PlusCircleFilled />}
                onClick={mockOnClick}
            />
        );
        const buttonElement = screen.getByRole("button");
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(
            <TemplateControlButton
                message="tooltip message"
                icon={<PlusCircleFilled />}
                disabled
                onClick={mockOnClick}
            />
        );
        const buttonElement = screen.getByRole("button");
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).toBeDisabled();
    });
});
