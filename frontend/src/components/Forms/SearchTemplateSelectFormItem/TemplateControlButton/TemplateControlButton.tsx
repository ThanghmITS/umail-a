import React, { ReactNode } from "react";
import { Tooltip, Button } from "antd";
import styles from "./TemplateControlButton.scss";

type Props = {
    message: string;
    icon: ReactNode;
    onClick: () => void;
    disabled?: boolean;
    isDanger?: boolean;
};

const TemplateControlButton = ({
    message,
    icon,
    onClick,
    disabled = false,
    isDanger = false,
}: Props) => {
    return (
        <Tooltip title={message}>
            <Button
                className={styles.container}
                type="primary"
                danger={isDanger}
                icon={icon}
                onClick={onClick}
                disabled={disabled}
            />
        </Tooltip>
    );
};

export default TemplateControlButton;
