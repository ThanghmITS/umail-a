import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Path from "../Routes/Paths";

import {
    Button,
    Col,
    Form,
    Tabs,
    Tooltip,
    Input,
    Row,
    Alert,
    Modal,
    Radio,
} from "antd";

import BaseForm from "./base/BaseForm";
import styles from "./EditForm.scss";
import { finalized } from "./helpers";

import validateJapaneseMessages from "./validateMessages";

import {
    QuestionCircleFilled,
    InfoCircleTwoTone,
    ExclamationCircleOutlined,
} from "@ant-design/icons";

import { simpleAction } from "../../actions/data";
import { connect } from "react-redux";
import { Endpoint } from "../../domain/api";

import { LOADING, CANCELED } from "../../actions/actionTypes";
import WarningMessages from "../DataDisplay/WarningMessages/WarningMessages";
import BackButton from "../Common/BackButton/BackButton";
import {
    ErrorMessages,
    infoColor,
    PASSWORD_REGEX,
    iconCustomColor,
    EMAIL_HOST_BLOCKED,
    HelpMessages,
} from "~/utils/constants";
import {
    customSuccessMessage,
    customErrorMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
const { TextArea } = Input;

const { TabPane } = Tabs;

const warningToolTipMessage =
    "社名変更におけるメールアドレス変更などはお問い合わせください。";

const containerLayout = {
    xs: 24,
    sm: 24,
    md: 21,
    lg: 18,
    xl: 15,
    xxl: 12,
};

const formLayoutLabelSpan = 7;
const formLayout = {
    labelCol: {
        span: formLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - formLayoutLabelSpan,
    },
};

class SharedEmailSettingForm extends BaseForm {
    constructor(props) {
        super(props);
        this.initialViewRendered = false;
    }

    state = {
        isDomainSwitchOn: true,
        isConnectionTestSuccessed: false,
        isEdit: false,
        connectionTestFieldErrors: undefined,
        isTestHostSuccess: false,
    };

    warningMessages = [
        "共有メールで使用する受信メールアドレスを設定してください。変更はできませんので、確認の上ご対応してください。",
        "受信メールアドレスのデータは、共有メール一覧に取り込まれると、空になります。",
    ];

    componentDidUpdate() {
        const { initialData } = this.props;
        const { setFieldsValue } = this.baseform.current;
        if (
            !this.initialViewRendered &&
            initialData &&
            initialData.from_email
        ) {
            setFieldsValue(initialData);
            this.setState({
                isDomainSwitchOn: initialData.allow_self_domain,
                isConnectionTestSuccessed: true,
                isEdit: true,
            });
            this.initialViewRendered = true;
        }
    }

    onChangeDomainSwitch = (e) => {
      this.setState({ isDomainSwitchOn: e.target.value });
    };

    handleSubmitWithSwitchState = (event) => {
        const { isDomainSwitchOn } = this.state;
        this.baseform.current.validateFields().then((values) => {
            values["allow_self_domain"] = isDomainSwitchOn;
            this.submitHandler(values);
        });
    };

    onClickConnectionTestButton = () => {
        const { dispatch, token, pageId } = this.props;
        const { getFieldValue } = this.baseform.current;
        const data = {
            from_email: getFieldValue("from_email"),
            password: getFieldValue("password"),
            protocol: getFieldValue("protocol"),
        };
        dispatch({ type: pageId + LOADING });
        simpleAction(
            pageId,
            token,
            `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmailConnection}`,
            data
        )
            .then(this.onConnectionTestSuccessed)
            .catch((error) => {
                this.setState({
                    connectionTestFieldErrors: error.message,
                });
                this.onConnectionTestFailed();
            });
    };

    onConnectionTestSuccessed = () => {
        const { dispatch, pageId } = this.props;
        customSuccessMessage("接続テストに成功しました。登録／更新ボタンをクリックして設定を保存してください。");
        this.setState({
            isConnectionTestSuccessed: true,
            connectionTestFieldErrors: undefined,
        });
        dispatch({ type: pageId + CANCELED });
    };

    onConnectionTestFailed = () => {
        const { dispatch, pageId } = this.props;
        if (!!this.state.connectionTestFieldErrors) {
            customErrorMessage(this.state.connectionTestFieldErrors);
        } else {
            customErrorMessage(ErrorMessages.sharedEmails.connectionTest);
        }
        dispatch({ type: pageId + CANCELED });
    };

    onChangeConnectionValue = () => {
        this.setState({ isConnectionTestSuccessed: false });
    };

    onClickRegisterButton = () => {
        Modal.confirm({
            title: "共有メールアドレス登録",
            icon: <ExclamationCircleOutlined />,
            content: (
                <div>
                    入力した内容で登録を行います。
                    <br />
                    一度登録されると本画面から変更はできません。
                    <br />
                    変更が必要な場合は、お問い合わせください。
                </div>
            ),
            okText: "OK",
            cancelText: "キャンセル",
            onOk: this.onConfirmOk,
        });
    };

    onConfirmOk = () => {
        this.handleSubmit();
    };

    tooltipMessage = (value) => {
        if (value == "sharedMails") {
            return (
                <Link
                    to={`${Path.sharedMails}`}
                    target="_blank"
                    rel="noopener noreferrer">
                    共有メールBOX一覧
                </Link>
            );
        } else if (value == "scheduledMails") {
            return (
                <Link
                    to={`${Path.scheduledMails}/register`}
                    target="_blank"
                    rel="noopener noreferrer">
                    配信メール予約
                </Link>
            );
        }
    };

    render() {
        const {
            isDomainSwitchOn,
            isConnectionTestSuccessed,
            isEdit,
            connectionTestFieldErrors,
            isTestHostSuccess,
        } = this.state;

        const { fieldErrors } = this.props;

        return (
            <Row justify="start">
                <Col {...containerLayout}>
                    <Form
                        onFinish={this.handleSubmit}
                        ref={this.baseform}
                        validateMessages={validateJapaneseMessages}
                        style={{ textAlign: "left" }}
                        labelAlign="right">
                        <Tabs
                            defaultActiveKey="1"
                            tabBarExtraContent={
                                <Tooltip
                                    title={warningToolTipMessage}
                                    color={infoColor}>
                                    <InfoCircleTwoTone
                                        twoToneColor={infoColor}
                                    />
                                </Tooltip>
                            }>
                            <TabPane tab="基本設定" key="1">
                                <Col span={24}>
                                    <Row justify="center">
                                        <Col span={22}>
                                            <WarningMessages
                                                messages={this.warningMessages.filter(
                                                    (message, index) => {
                                                        if (isEdit) {
                                                            return !!index;
                                                        }
                                                        return !!message;
                                                    }
                                                )}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                                <Form.Item
                                    {...formLayout}
                                    label={
                                        <span>
                                            共有メールアドレス&nbsp;
                                            <Tooltip
                                                title={
                                                    <span>
                                                        ここで指定したメールアドレスに送られたメールが、
                                                        {this.tooltipMessage(
                                                            "sharedMails"
                                                        )}
                                                        に表示されます。
                                                    </span>
                                                }>
                                                <QuestionCircleFilled
                                                    style={{
                                                        color: iconCustomColor,
                                                    }}
                                                />
                                            </Tooltip>
                                        </span>
                                    }
                                    className={styles.field}
                                    required={true}
                                    validateStatus={
                                        fieldErrors.from_email ||
                                        (connectionTestFieldErrors &&
                                            connectionTestFieldErrors[
                                                "from_email"
                                            ])
                                            ? "error"
                                            : "success"
                                    }
                                    help={
                                        fieldErrors.from_email ||
                                        (connectionTestFieldErrors &&
                                            connectionTestFieldErrors[
                                                "from_email"
                                            ])
                                    }
                                    rules={[
                                        {
                                            validator: (_, hostname) => {
                                                const errorMessage =
                                                    hostname +
                                                    HelpMessages.cannotUsePrefix;
                                                const hostEmail =
                                                    hostname.slice(
                                                        hostname.indexOf("@") +
                                                            1
                                                    );
                                                if (
                                                    EMAIL_HOST_BLOCKED.includes(
                                                        hostEmail
                                                    )
                                                ) {
                                                    this.setState({
                                                        isTestHostSuccess: true,
                                                    });
                                                    return Promise.reject(
                                                        new Error(errorMessage)
                                                    );
                                                } else {
                                                    this.setState({
                                                        isTestHostSuccess: false,
                                                    });
                                                    return Promise.resolve();
                                                }
                                            },
                                        },
                                    ]}
                                    name="from_email">
                                    <TextArea
                                        autoSize={{ minRows: 1 }}
                                        placeholder="you@example.com"
                                        onChange={this.onChangeConnectionValue}
                                        disabled={isEdit}
                                    />
                                </Form.Item>
                                <Form.Item
                                    {...formLayout}
                                    label="パスワード"
                                    className={styles.field}
                                    required={true}
                                    validateStatus={
                                        fieldErrors.password ||
                                        (connectionTestFieldErrors &&
                                            connectionTestFieldErrors[
                                                "password"
                                            ])
                                            ? "error"
                                            : "success"
                                    }
                                    help={
                                        fieldErrors.password ||
                                        (connectionTestFieldErrors &&
                                            connectionTestFieldErrors[
                                                "password"
                                            ])
                                    }
                                    name="password">
                                    <Input.Password
                                        onChange={this.onChangeConnectionValue}
                                    />
                                </Form.Item>
                                <Form.Item
                                    {...formLayout}
                                    label="プロトコル"
                                    className={styles.field}
                                    name="protocol"
                                    required={true}
                                    initialValue={"imap"}>
                                    <Radio.Group
                                        onChange={this.onChangeConnectionValue}>
                                        <Radio value="imap">IMAP</Radio>
                                        <Radio value="pop3">POP3</Radio>
                                        <Radio value="imap+ssl">
                                            IMAP+SSL/TLS
                                        </Radio>
                                        <Radio value="pop3+ssl">
                                            POP3+SSL/TLS
                                        </Radio>
                                    </Radio.Group>
                                </Form.Item>
                                <Button
                                    type="default"
                                    htmlType="button"
                                    className={styles.button}
                                    onClick={this.onClickConnectionTestButton}
                                    disabled={
                                        isConnectionTestSuccessed ||
                                        isTestHostSuccess
                                    }>
                                    接続テスト
                                </Button>
                            </TabPane>
                            <TabPane tab="詳細設定" key="2">
                                <Form.Item
                                    label={
                                        <span>
                                            自社ドメインを表示&nbsp;
                                            <Tooltip
                                                title={
                                                    <span>
                                                        共有メールアドレス宛に届く差出人メールアドレスと同じドメインからのメールデータを
                                                        {this.tooltipMessage(
                                                            "sharedMails"
                                                        )}
                                                        に表示させるかを決めます。チェックがある場合は表示されます。ただし、配信メールで送信されたメールデータは
                                                        {this.tooltipMessage(
                                                            "scheduledMails"
                                                        )}
                                                        の「控えを共有メールBOXに送信」で決めます。
                                                    </span>
                                                }>
                                                <QuestionCircleFilled
                                                    style={{
                                                        color: iconCustomColor,
                                                    }}
                                                />
                                            </Tooltip>
                                        </span>
                                    }
                                    className={styles.field}
                                    name="allow_self_domain">
                                    <Radio.Group
                                        onChange={this.onChangeDomainSwitch}
                                        defaultValue={isDomainSwitchOn}>
                                        <Radio value={true}>表示</Radio>
                                        <Radio value={false}>非表示</Radio>
                                    </Radio.Group>
                                </Form.Item>
                            </TabPane>
                        </Tabs>
                        <br />
                        <Col span={24} style={{ marginTop: "5%" }}>
                            <Row justify="start">
                                <Col>
                                    <BackButton />
                                </Col>
                                <Col>
                                    {isEdit ? (
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            className={styles.button}
                                            disabled={
                                                !isConnectionTestSuccessed
                                            }>
                                            更新
                                        </Button>
                                    ) : (
                                        <Button
                                            type="primary"
                                            htmlType="button"
                                            className={styles.button}
                                            disabled={
                                                !isConnectionTestSuccessed
                                            }
                                            onClick={
                                                this.onClickRegisterButton
                                            }>
                                            登録
                                        </Button>
                                    )}
                                </Col>
                            </Row>
                        </Col>
                    </Form>
                </Col>
            </Row>
        );
    }
}

SharedEmailSettingForm.propTypes = {
    dispatch: PropTypes.func.isRequired,
    resourceURL: PropTypes.string.isRequired,
    initialData: PropTypes.shape({
        // Corresponds to backend API.
        allow_self_domain: PropTypes.string,
    }), // Override in child class and use PropTypes.shape instead.
    fieldErrors: PropTypes.shape({
        allow_self_domain: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
    submitHandler: PropTypes.func.isRequired,
    pageId: PropTypes.string,
};

SharedEmailSettingForm.defaultProps = {
    initialData: {},
};

function mapStateToProps(state) {
    return {
        token: state.login.token,
    };
}

const _SharedEmailSettingFormWrapper = finalized(SharedEmailSettingForm);
const SharedEmailSettingFormWrapper = connect(mapStateToProps)(
    _SharedEmailSettingFormWrapper
);
export default SharedEmailSettingFormWrapper;
