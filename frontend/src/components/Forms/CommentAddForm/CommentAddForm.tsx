import React, { ChangeEvent } from "react";
import { Button, Form, Tooltip } from "antd";
import CommentForm from "../CommentForm/CommentForm";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import styles from "./CommentAddForm.scss";

type Props = {
    value: string;
    onInputChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    isSubmitting: boolean;
    onSubmit: () => void;
    isPinned: boolean;
    onPinnedChange: () => void;
    onSelectCommentTemplate: () => void;
    commentError?: string;
};

const CommentAddForm = ({
    value,
    onInputChange,
    isSubmitting,
    onSubmit,
    isPinned,
    onPinnedChange,
    onSelectCommentTemplate,
    commentError,
}: Props) => {
    const debounceInputChange = useCustomDebouncedCallback(onInputChange, 300);
    const { create: createAuthorization } = useAuthorizedActions("comment");

    const renderCommentButton = () => {
        const button = (
            <Button
                htmlType="button"
                loading={isSubmitting}
                onClick={onSubmit}
                type="primary"
                size="small"
                disabled={
                    !createAuthorization || !value || value.length > 1000
                }>
                コメントを追加
            </Button>
        );
        if (!createAuthorization) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        return button;
    };

    return (
        <CommentForm
            value={value}
            onInputChange={debounceInputChange}
            isPinned={isPinned}
            onPinnedChange={onPinnedChange}
            onSelectCommentTemplate={onSelectCommentTemplate}
            commentError={commentError}
            disableIconPin={false}>
            <Form.Item>{renderCommentButton()}</Form.Item>
        </CommentForm>
    );
};

export default CommentAddForm;
