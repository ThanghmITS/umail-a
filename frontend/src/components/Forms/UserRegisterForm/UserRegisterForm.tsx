import React, { useEffect, useState } from "react";
import { Button, Form, Input, Radio, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import {
    ErrorMessages,
    ONLY_HANKAKU_REGEX,
    PASSWORD_REGEX,
    RESTRICT_SPACE_REGEX,
    ROLES,
    iconCustomColor,
} from "~/utils/constants";
import {
    UserFormFieldErrorResponseModel,
    UserRegisterFormModel,
    UserModel,
} from "~/models/userModel";
import TelInputFormItem from "../TelInputFormItem/TelInputFormItem";
import {
    convertUserRegisterFormModelToUserRegisterRequestDataModel,
    useUserRegisterAPIMutation,
} from "~/hooks/useUser";
import { useHistory, useParams } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import GenericModalContent from "~/components/Modals/GenericModal/GenericModalContent/GenericModalContent";
import { useDeleteThumbnailAPIMutation } from "~/hooks/useThumbnail";
import ProfileThumbnailFormItem from "~/components/Common/ProfileThumbnailFormItem/ProfileThumbnailFormItem";
import styles from "./UserRegisterForm.scss";
import UserServiceIdFormItem from "../UserServiceIdFormItem/UserServiceIdFormItem";

const { TextArea } = Input;

const formItemLayout = {
    labelCol: {
        xs: { span: 4 },
        sm: { span: 4 },
        md: { span: 4 },
    },
    wrapperCol: {
        span: 14,
    },
};

const emptyInitialData: UserModel = {
    email: "",
    email_signature: undefined,
    first_name: "",
    last_name: "",
    modified_time: "",
    modified_user: "",
    user_service_id: "",
    role: undefined,
    tel1: undefined,
    tel2: undefined,
    tel3: undefined,
    is_active: false,
};

type Props = {
    initialData?: UserModel;
    fieldErrors?: UserFormFieldErrorResponseModel;
};

const UserRegisterForm = ({
    initialData = emptyInitialData,
    fieldErrors,
}: Props) => {
    const [form] = Form.useForm<UserRegisterFormModel>();
    const { id: registerToken } = useParams<{ id: string }>();
    const router = useHistory();
    const [thumbnailUrl, setThumbnailUrl] = useState<string | undefined>(
        undefined
    );
    const [isRegisterModalOpen, setIsRegisterModalOpen] = useState(false);
    const { mutate: registerUser, error } = useUserRegisterAPIMutation();
    const { mutate: deleteThumbnail } = useDeleteThumbnailAPIMutation();

    const onRegisterModalClose = () => {
        setIsRegisterModalOpen(false);
    };

    const onRegisterModalOpen = (values: UserRegisterFormModel) => {
        setIsRegisterModalOpen(true);
    };

    const renderRegisterModal = (values: UserRegisterFormModel) => {
        return (
            <GenericModal
                visible={isRegisterModalOpen}
                title="このユーザーを登録しますか？"
                type="warning"
                onOk={() => {
                    const data =
                        convertUserRegisterFormModelToUserRegisterRequestDataModel(
                            values
                        );
                    registerUser(
                        {
                            registerToken,
                            data,
                        },
                        {
                            onSuccess: (result) => {
                                router.push(Paths.dashboard);
                            },
                            onSettled: () => {
                              setIsRegisterModalOpen(false);
                            },
                        }
                    );
                }}
                onCancel={onRegisterModalClose}>
                <GenericModalContent>
                    OKを押すと、登録が実行されます。
                    <br />
                    <br />
                    <span>※ユーザー上限を超えていると実行されません。</span>
                </GenericModalContent>
            </GenericModal>
        );
    };

    const onRemoveThumbnail = () => {
        const email = form.getFieldValue("email");
        deleteThumbnail(
            { email },
            {
                onSuccess: () => {
                    form.setFieldsValue({ avatar: undefined });
                    setThumbnailUrl(undefined);
                },
            }
        );
    };

    useEffect(() => {
        if (initialData && initialData.email) {
            form.setFieldsValue(initialData);
            setThumbnailUrl(initialData.avatar);
        }
    }, [initialData]);

    const requiredFields = initialData.name
        ? []
        : ["last_name", "first_name", "password"];

    return (
        <>
            <Form
                onFinish={onRegisterModalOpen}
                form={form}
                className={styles.container}
                validateMessages={validateJapaneseMessages}>
                {/* <ProfileThumbnailFormItem
                    {...formItemLayout}
                    className={styles.field}
                    onRemoveThumbnail={onRemoveThumbnail}
                    thumbnailUrl={thumbnailUrl}
                    uploadProps={{
                        beforeUpload: (file) => {
                            form.setFieldsValue({
                                avatar: file,
                            });
                            // NOTE(joshua-hashimoto): falseを返却することでAntdのUploadの自動送信を阻止
                            return false;
                        },
                    }}
                /> */}
                <Form.Item
                    {...formItemLayout}
                    label="ユーザー名"
                    className={styles.field}
                    validateStatus={error?.response?.data.name ? "error" : "success"}
                    help={error?.response?.data.name}
                    required>
                    <Form.Item
                        name="last_name"
                        getValueFromEvent={(e) => {
                            return e.currentTarget.value
                                .replace(" ", "")
                                .replace("　", "");
                        }}
                        help={error?.response?.data.last_name}
                        dependencies={["first_name"]}
                        rules={[
                            ({ getFieldValue }) => ({
                                validator: (_, value) => {
                                    const lastName = !!value ? value : null;
                                    // NOTE(joshua-hashimoto): スペース制御
                                    if (!RESTRICT_SPACE_REGEX.test(lastName)) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.regex.space
                                            )
                                        );
                                    }

                                    const firstNameValue =
                                        getFieldValue("first_name");
                                    const firstName = !!firstNameValue
                                        ? firstNameValue
                                        : null;
                                    // NOTE(joshua-hashimoto): スペース制御
                                    if (!RESTRICT_SPACE_REGEX.test(firstName)) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.regex.space
                                            )
                                        );
                                    }

                                    const fullName = lastName + firstName;

                                    // NOTE(joshua-hashimoto): 必須項目制御
                                    if (!lastName || !firstName || !fullName) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.form.required
                                            )
                                        );
                                    }

                                    // NOTE(joshua-hashimoto): 50文字制御
                                    if (fullName.length > 50) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.validation.length.max50
                                            )
                                        );
                                    }
                                    return Promise.resolve();
                                },
                            }),
                        ]}
                        noStyle>
                        <Input
                            style={{ width: "50%" }}
                            placeholder="姓"
                            allowClear
                        />
                    </Form.Item>
                    <Form.Item
                        name="first_name"
                        help={error?.response?.data.first_name}
                        noStyle>
                        <Input
                            style={{ width: "50%" }}
                            placeholder="名"
                            allowClear
                        />
                    </Form.Item>
                </Form.Item>
                {/* <UserServiceIdFormItem {...formItemLayout} /> */}
                <Form.Item
                    {...formItemLayout}
                    label="メールアドレス"
                    className={styles.field}
                    rules={[
                        {
                            pattern: ONLY_HANKAKU_REGEX,
                            message: ErrorMessages.validation.regex.onlyHankaku,
                        },
                        {
                            max: 100,
                            message: ErrorMessages.validation.length.max100,
                        },
                        {
                            pattern: RESTRICT_SPACE_REGEX,
                            message: ErrorMessages.validation.regex.space,
                        },
                    ]}
                    name="email">
                    <Input disabled placeholder="you@example.com" />
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="権限"
                    className={styles.field}
                    help={error?.response?.data.role}
                    name="role">
                    <Radio.Group disabled>
                        {ROLES.map((role) => {
                            return (
                                <Radio
                                    value={role.value}
                                    disabled={role.value === "master"}>
                                    {role.title}
                                </Radio>
                            );
                        })}
                    </Radio.Group>
                </Form.Item>
                <TelInputFormItem
                    {...formItemLayout}
                    validateStatus={error?.response?.data.tel ? "error" : undefined}
                    help={error?.response?.data.tel}
                />
                <Form.Item
                    {...formItemLayout}
                    label="パスワード"
                    className={styles.field}
                    help={error?.response?.data.password}
                    validateStatus={error?.response?.data.password ? "error" : undefined}
                    name="password"
                    rules={[
                        { required: true },
                        {
                            pattern: PASSWORD_REGEX,
                            message: ErrorMessages.validation.regex.password,
                        },
                    ]}>
                    <Input.Password
                        placeholder="大小英数字記号混在で10-50桁"
                        autoComplete="new-password"
                    />
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label={
                        <span>
                            メール署名&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        配信する際にメール署名として活用されます。
                                    </span>
                                }>
                                <QuestionCircleFilled
                                    style={{ color: iconCustomColor }}
                                />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    validateStatus={
                        error?.response?.data.email_signature
                          ? "error"
                          : undefined
                    }
                    help={error?.response?.data.email_signature}
                    name="email_signature"
                    rules={[
                        {
                            max: 1000,
                            message: ErrorMessages.validation.length.max1000,
                        },
                    ]}>
                    <TextArea
                        rows={16}
                        data-testid="user-register-form-email-signature"
                    />
                </Form.Item>
                <Form.Item shouldUpdate>
                    {() => (
                        <div className={styles.buttonWrapper}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={
                                    !form.isFieldsTouched(
                                        requiredFields,
                                        true
                                    ) ||
                                    !!form
                                        .getFieldsError()
                                        .filter(({ errors }) => errors.length)
                                        .length
                                }
                                className={styles.button}>
                                登録
                            </Button>
                        </div>
                    )}
                </Form.Item>
            </Form>
            {renderRegisterModal(form.getFieldsValue())}
        </>
    );
};

export default UserRegisterForm;
