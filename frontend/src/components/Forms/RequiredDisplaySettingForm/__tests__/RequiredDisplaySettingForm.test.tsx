import React from "react";
import { renderWithBrowserRouter, screen } from "~/test/utils";
import RequiredDisplaySettingFormWrapper from "../RequiredDisplaySettingForm";

describe("RequiredDisplaySettingForm.jsx", () => {
    test("render test", () => {
        renderWithBrowserRouter(
            <RequiredDisplaySettingFormWrapper initialData={{}} />
        );
        const optionalItemsLabelElements = screen.getAllByText(/任意/);
        expect(optionalItemsLabelElements.length).toBe(2);
        const requiredItemsLabelElements = screen.getAllByText(/必須/);
        expect(requiredItemsLabelElements.length).toBe(2);
        const organizationItemsLabelElement = screen.getByText(/取引先情報/);
        expect(organizationItemsLabelElement).toBeInTheDocument();
        const organizationNumberItemElement = screen.getByText(/法人番号/);
        expect(organizationNumberItemElement).toBeInTheDocument();
        const organizationScoreItemElement = screen.getByText(/取引先評価/);
        expect(organizationScoreItemElement).toBeInTheDocument();
        const organizationNationalityItemElement = screen.getByText(/国籍/);
        expect(organizationNationalityItemElement).toBeInTheDocument();
        const organizationEstablishDateItemElement =
            screen.getByText(/設立年月/);
        expect(organizationEstablishDateItemElement).toBeInTheDocument();
        const organizationSettlementMonthItemElement =
            screen.getByText(/決算期/);
        expect(organizationSettlementMonthItemElement).toBeInTheDocument();
        const organizationAddressItemElement = screen.getByText(/住所/);
        expect(organizationAddressItemElement).toBeInTheDocument();
        const organizationTelItemElement = screen.getAllByText(/TEL/)[0];
        expect(organizationTelItemElement).toBeInTheDocument();
        const organizationFaxItemElement = screen.getByText(/FAX/);
        expect(organizationFaxItemElement).toBeInTheDocument();
        const organizationUrlItemElement = screen.getByText(/URL/);
        expect(organizationUrlItemElement).toBeInTheDocument();
        const organizationEmployeeNumberItemElement =
            screen.getByText(/社員数/);
        expect(organizationEmployeeNumberItemElement).toBeInTheDocument();
        const organizationDistributionItemElement = screen.getByText(/商流/);
        expect(organizationDistributionItemElement).toBeInTheDocument();
        const organizationContractItemElement = screen.getByText(/請負/);
        expect(organizationContractItemElement).toBeInTheDocument();
        const organizationCapitalItemElement = screen.getByText(/資本金/);
        expect(organizationCapitalItemElement).toBeInTheDocument();
        const organizationLicenseItemElement = screen.getByText(/保有資格/);
        expect(organizationLicenseItemElement).toBeInTheDocument();
        const organizationNameItemElement = screen.getByText(/取引先名/);
        expect(organizationNameItemElement).toBeInTheDocument();
        const organizationStatusItemElement =
            screen.getByText(/取引先ステータス/);
        expect(organizationStatusItemElement).toBeInTheDocument();

        const contactItemsLabelElements = screen.getByText(/取引先担当者情報/);
        expect(contactItemsLabelElements).toBeInTheDocument();
        const contactEmailCCItemElement =
            screen.getByText(/メールアドレス\(CC\)/);
        expect(contactEmailCCItemElement).toBeInTheDocument();
        const contactTelItemElement = screen.getAllByText(/TEL/)[1];
        expect(contactTelItemElement).toBeInTheDocument();
        const contactPositionItemElement = screen.getByText(/役職/);
        expect(contactPositionItemElement).toBeInTheDocument();
        const contactDepartmentItemElement = screen.getByText(/部署/);
        expect(contactDepartmentItemElement).toBeInTheDocument();
        const contactAssigneeItemElement = screen.getByText(/自社担当者/);
        expect(contactAssigneeItemElement).toBeInTheDocument();
        const contactLastVisitItemElement = screen.getByText(/最終訪問日/);
        expect(contactLastVisitItemElement).toBeInTheDocument();
        const contactTagItemElement = screen.getByText(/タグ/);
        expect(contactTagItemElement).toBeInTheDocument();
        const contactScoreItemElement = screen.getByText(/相性/);
        expect(contactScoreItemElement).toBeInTheDocument();
        const contactNameItemElement = screen.getByText(/取引先担当者名/);
        expect(contactNameItemElement).toBeInTheDocument();
        const contactAssignedOrganizationItemElement =
            screen.getByText(/所属取引先/);
        expect(contactAssignedOrganizationItemElement).toBeInTheDocument();
        const contactEmailTOItemElement =
            screen.getByText(/メールアドレス\(TO\)/);
        expect(contactEmailTOItemElement).toBeInTheDocument();

        const backButtonElement = screen.getByRole("button", { name: /戻る/ });
        expect(backButtonElement).toBeInTheDocument();
        expect(backButtonElement).not.toBeDisabled();
        const updateButtonElement = screen.getByRole("button", {
            name: /更 新/,
        });
        expect(updateButtonElement).toBeInTheDocument();
        expect(updateButtonElement).not.toBeDisabled();
    });
});
