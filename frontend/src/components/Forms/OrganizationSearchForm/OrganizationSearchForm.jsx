import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Button, Row, Col, Form, Tooltip, Badge } from "antd";
import { SearchOutlined, CloseOutlined } from "@ant-design/icons";
import CorporateNameFormItem from "./CorporateNameFormItem/CorporateNameFormItem";
import CorporateNumberFormItem from "./CorporateNumberFormItem/CorporateNumberFormItem";
import CorporateAddressFormItem from "./CorporateAddressFormItem/CorporateAddressFormItem";
import CorporateDomainNameFormItem from "./CorporateDomainNameFormItem/CorporateDomainNameFormItem";
import CorporateSettlementMonthFormItem from "./CorporateSettlementMonthFormItem/CorporateSettlementMonthFormItem";
import CorporateCategoryFormItem from "./CorporateCategoryFormItem/CorporateCategoryFormItem";
import CorporateEmployeeNumberFormItem from "./CorporateEmployeeNumberFormItem/CorporateEmployeeNumberFormItem";
import CorporateContractFormItem from "./CorporateContractFormItem/CorporateContractFormItem";
import CorporateCapitalFormItem from "./CorporateCapitalFormItem/CorporateCapitalFormItem";
import CorporateCapitalManYenRequiredForTransactionsFormItem from "./CorporateCapitalManYenRequiredForTransactionsFormItem/CorporateCapitalManYenRequiredForTransactionsFormItem";
import CorporateCountryFormItem from "./CorporateCountryFormItem/CorporateCountryFormItem";
import CorporateEstablishmentDateFormItem from "./CorporateEstablishmentDateFormItem/CorporateEstablishmentDateFormItem";
import CorporateEstablishmentYearFormItem from "./CorporateEstablishmentYearFormItem/CorporateEstablishmentYearFormItem";
import CorporateHasDistributionFormItem from "./CorporateHasDistributionFormItem/CorporateHasDistributionFormItem";
import CorporateLicenseFormItem from "./CorporateLicenseFormItem/CorporateLicenseFormItem";
import CorporateLicenseRequiredForTransactionsFormItem from "./CorporateLicenseRequiredForTransactionsFormItem/CorporateLicenseRequiredForTransactionsFormItem";
import CorporateTelFormItem from "./CorporateTelFormItem/CorporateTelFormItem";
import CorporateFaxFormItem from "./CorporateFaxFormItem/CorporateFaxFormItem";
import CorporateScoreFormItem from "./CorporateScoreFormItem/CorporateScoreFormItem";
import CorporateCommentUserFormItem from "./CorporateCommentUserFormItem/CorporateCommentUserFormItem";
import CorporateCreatedUserFormItem from "./CorporateCreatedUserFormItem/CorporateCreatedUserFormItem";
import CorporateModifiedUserFormItem from "./CorporateModifiedUserFormItem/CorporateModifiedUserFormItem";
import IncludeInvalidSwitchFormItem from "./IncludeInvalidSwitchFromItem/IncludeInvalidSwitchFormItem";
import IncludeBlocklistSwitchFormItem from "./IncludeBlocklistSwitchFromItem/IncludeBlocklistSwitchFormItem";
import SearchTemplateSelectFormItem from "../SearchTemplateSelectFormItem/SearchTemplateSelectFormItem";
import { Endpoint } from "../../../domain/api";
import { ORGANIZATION_SEARCH_PAGE } from "../../Pages/pageIds";
import moment from "moment";
import CorporateBranchNameFormItem from "./CorporateBranchNameFormItem/CorporateBranchNameFormItem";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import SearchMenuDrawer from "~/components/Common/SearchMenuDrawer/SearchMenuDrawer";
import CorporateBranchTelFormItem from "./CorporateBranchTelFormItem/CorporateBranchTelFormItem";
import CorporateBranchFaxFormItem from "./CorporateBranchFaxFormItem/CorporateBranchFaxFormItem";
import CorporateBranchAddressFormItem from "./CorporateBranchAddressFormItem/CorporateBranchAddressFormItem";
import { useSearchTemplateUtils } from "~/hooks/useSearchTemplate";
import { useSearchSync } from "~/hooks/useSearch";
import styles from "./OrganizationSearchForm.scss";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";

const pageId = ORGANIZATION_SEARCH_PAGE;
const filterType = "search";
const searchTemplateURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.organizationSearchTemplate
}`;
const templateReducerName = "organizationSearchPage";

const emptyData = {
    address: undefined,
    branch_address: undefined,
    domain_name: undefined,
    settlement_month: undefined,
    capital: undefined,
    capital_gt: undefined,
    capital_lt: undefined,
    capital_man_yen_required_for_transactions: undefined,
    capital_man_yen_required_for_transactions_gt: undefined,
    capital_man_yen_required_for_transactions_lt: undefined,
    category: [],
    corporate_number: undefined,
    country: [],
    employee_number: [],
    contract: undefined,
    establishment_date_range: [],
    establishment_year: undefined,
    establishment_year_gt: undefined,
    establishment_year_lt: undefined,
    fax1: undefined,
    fax2: undefined,
    fax3: undefined,
    branch_fax1: undefined,
    branch_fax2: undefined,
    branch_fax3: undefined,
    ignore_filter: false,
    has_distribution: undefined,
    ignore_blocklist_filter: false,
    license: [],
    license_required_for_transactions: [],
    name: undefined,
    score: 3,
    score_inequality: undefined,
    tel1: undefined,
    tel2: undefined,
    tel3: undefined,
    branch_tel1: undefined,
    branch_tel2: undefined,
    branch_tel3: undefined,
    comment_user: [],
    created_user: [],
    modified_user: [],
    branch_name: undefined,
};
const organizations = "organizations";

const OrganizationSearchForm = React.forwardRef(
    (
        {
            tableName,
            initialData,
            submitHandler,
            isDefaultTemplateAttached,
            searchConditionSanitizer,
        },
        ref
    ) => {
        const [form] = Form.useForm();
        const [isSearchDrawerOpen, setIsSearchDrawerOpen] = useState(false);
        const [selectedTemplateName, setSelectedTemplateName] =
            useState(undefined);
        const [unshowSearchFields, setUnshowSearchFields] = useState([]);
        const [filledSearchFieldCount, setFilledSearchFieldCount] = useState(0);
        const { updateDisplaySetting } = useDisplaySettingAPI();
        const { displaySetting } = useSelector(
            (state) => state.displaySettingPage
        );
        const { saveCurrentSearchCondition } = useSearchTemplateUtils();
        const { syncToUrl, queryParamsToObj } = useSearchSync();

        const countFilterField = (formValues) => {
            let count = 0;
            if (!!formValues.ignore_filter) {
                count += 1;
            }
            if (!!formValues.ignore_blocklist_filter) {
                count += 1;
            }
            if (!!formValues.corporate_number) {
                count += 1;
            }
            if (!!formValues.name) {
                count += 1;
            }
            if (!!formValues.category && !!formValues.category.length) {
                count += 1;
            }
            if (!!formValues.score_inequality && !!formValues.score) {
                count += 1;
            }
            if (!!formValues.branch_name) {
                count += 1;
            }
            if (!!formValues.country && !!formValues.country.length) {
                count += 1;
            }
            if (
                !!formValues.establishment_date_range &&
                !!formValues.establishment_date_range.length
            ) {
                count += 1;
            }
            if (!!formValues.settlement_month) {
                count += 1;
            }
            if (!!formValues.address) {
                count += 1;
            }
            if (!!formValues.tel1 || !!formValues.tel2 || !!formValues.tel3) {
                count += 1;
            }
            if (!!formValues.fax1 || !!formValues.fax2 || !!formValues.fax3) {
                count += 1;
            }
            if (!!formValues.branch_address) {
                count += 1;
            }
            if (
                !!formValues.branch_tel1 ||
                !!formValues.branch_tel2 ||
                !!formValues.branch_tel3
            ) {
                count += 1;
            }
            if (
                !!formValues.branch_fax1 ||
                !!formValues.branch_fax2 ||
                !!formValues.branch_fax3
            ) {
                count += 1;
            }
            if (!!formValues.domain_name) {
                count += 1;
            }
            if (
                !!formValues.employee_number &&
                !!formValues.employee_number.length
            ) {
                count += 1;
            }
            if (typeof formValues.has_distribution !== "undefined") {
                count += 1;
            }
            if (typeof formValues.contract !== "undefined") {
                count += 1;
            }
            if (!!formValues.capital_gt || !!formValues.capital_lt) {
                count += 1;
            }
            if (!!formValues.license && !!formValues.license.length) {
                count += 1;
            }
            if (
                !!formValues.establishment_year_gt ||
                !!formValues.establishment_year_lt
            ) {
                count += 1;
            }
            if (
                !!formValues.capital_man_yen_required_for_transactions_gt ||
                !!formValues.capital_man_yen_required_for_transactions_lt
            ) {
                count += 1;
            }
            if (
                !!formValues.license_required_for_transactions &&
                !!formValues.license_required_for_transactions.length
            ) {
                count += 1;
            }
            if (!!formValues.comment_user && !!formValues.comment_user.length) {
                count += 1;
            }
            if (!!formValues.created_user && !!formValues.created_user.length) {
                count += 1;
            }
            if (
                !!formValues.modified_user &&
                !!formValues.modified_user.length
            ) {
                count += 1;
            }
            return count;
        };

        const submitToFilter = useCustomDebouncedCallback((values) => {
            syncToUrl(values);
            saveCurrentSearchCondition(
                organizations,
                selectedTemplateName,
                values
            );
            submitHandler(values);
            const count = countFilterField(values);
            setFilledSearchFieldCount(count);
        });

        const onReset = () => {
            form.setFieldsValue(emptyData);
            setSelectedTemplateName(undefined);
            submitHandler(emptyData);
            syncToUrl(emptyData);
            const count = countFilterField({});
            setFilledSearchFieldCount(count);
            saveCurrentSearchCondition(organizations, selectedTemplateName, {});
        };

        const onClear = async (fieldKey, clearValue) => {
            try {
                const values = await form.validateFields();
                values[fieldKey] = clearValue;
                submitHandler(values);
            } catch (err) {
                console.error(err);
            }
        };

        const onDrawerClose = () => {
            setIsSearchDrawerOpen(false);
        };

        const onTemplateSelect = (templateName, templateValues) => {
            // NOTE(joshua-hahsimoto): 設立期間がある場合は文字列からDateに変換してあげる必要がある
            if (!templateValues) {
                let retrievedObject = localStorage.getItem(
                    organizations + "_default"
                );
                localStorage.setItem(organizations + "_selected", "");
                const parsedObj = JSON.parse(retrievedObject);
                const newObject = {
                    ...emptyData,
                    ...parsedObj,
                };
                submitHandler(newObject);
                return;
            }
            if (templateValues && templateValues.establishment_date_range) {
                templateValues.establishment_date_range =
                    templateValues.establishment_date_range.map((date) =>
                        moment(date)
                    );
            }
            const formValues = templateValues
                ? {
                      ...emptyData,
                      ...templateValues,
                  }
                : emptyData;
            var retrievedObject = localStorage.getItem(
                organizations + "_" + templateName
            );
            localStorage.setItem(organizations + "_selected", templateName);
            const parsedObj = JSON.parse(retrievedObject);
            const newObject = {
                ...formValues,
                ...parsedObj,
            };
            const newData = searchConditionSanitizer(
                unshowSearchFields,
                newObject
            );
            form.setFieldsValue(newData);
            submitHandler(newData);
            syncToUrl(newData);
            const count = countFilterField(newData);
            setFilledSearchFieldCount(count);
        };

        const menuItems = [
            {
                fieldKey: "corporate_number",
                searchField: CorporateNumberFormItem,
                fieldNames: ["corporate_number"],
                onClear,
            },
            {
                fieldKey: "name",
                searchField: CorporateNameFormItem,
                fieldNames: ["name"],
                onClear,
            },
            {
                fieldKey: "category",
                searchField: CorporateCategoryFormItem,
                fieldNames: ["category"],
                onClear: undefined,
            },
            {
                fieldKey: "score",
                searchField: CorporateScoreFormItem,
                fieldNames: ["score", "score_inequality"],
                onClear: undefined,
            },
            {
                fieldKey: "country",
                searchField: CorporateCountryFormItem,
                fieldNames: ["country"],
                onClear: undefined,
            },
            {
                fieldKey: "establishment_date",
                searchField: CorporateEstablishmentDateFormItem,
                fieldNames: ["establishment_date_range"],
                onClear: undefined,
            },
            {
                fieldKey: "settlement_month",
                searchField: CorporateSettlementMonthFormItem,
                fieldNames: ["settlement_month"],
                onClear: undefined,
            },
            {
                fieldKey: "address",
                searchField: CorporateAddressFormItem,
                fieldNames: ["address"],
                onClear,
            },
            {
                fieldKey: "tel",
                searchField: CorporateTelFormItem,
                fieldNames: ["tel1", "tel2", "tel3"],
                onClear,
            },
            {
                fieldKey: "fax",
                searchField: CorporateFaxFormItem,
                fieldNames: ["fax1", "fax2", "fax3"],
                onClear,
            },
            {
                fieldKey: "domain_name",
                searchField: CorporateDomainNameFormItem,
                fieldNames: ["domain_name"],
                onClear,
            },
            {
                fieldKey: "employee_number",
                searchField: CorporateEmployeeNumberFormItem,
                fieldNames: ["employee_number"],
                onClear: undefined,
            },
            {
                fieldKey: "has_distribution",
                searchField: CorporateHasDistributionFormItem,
                fieldNames: ["has_distribution"],
                onClear: undefined,
            },
            {
                fieldKey: "contract",
                searchField: CorporateContractFormItem,
                fieldNames: ["contract"],
                onClear: undefined,
            },
            {
                fieldKey: "capital",
                searchField: CorporateCapitalFormItem,
                fieldNames: ["capital_gt", "capital_lt"],
                onClear,
            },
            {
                fieldKey: "license",
                searchField: CorporateLicenseFormItem,
                fieldNames: ["license"],
                onClear: undefined,
            },
            {
                fieldKey: "branch_name",
                searchField: CorporateBranchNameFormItem,
                fieldNames: ["branch_name"],
                onClear,
            },
            {
                fieldKey: "branch_address",
                searchField: CorporateBranchAddressFormItem,
                fieldNames: ["branch_address"],
                onClear,
            },
            {
                fieldKey: "branch_tel",
                searchField: CorporateBranchTelFormItem,
                fieldNames: ["branch_tel1", "branch_tel2", "branch_tel3"],
                onClear,
            },
            {
                fieldKey: "branch_fax",
                searchField: CorporateBranchFaxFormItem,
                fieldNames: ["branch_fax1", "branch_fax2", "branch_fax3"],
                onClear,
            },
            {
                fieldKey: "establishment_year",
                searchField: CorporateEstablishmentYearFormItem,
                fieldNames: ["establishment_year_gt", "establishment_year_lt"],
                onClear,
            },
            {
                fieldKey: "capital_man_yen_required_for_transactions",
                searchField:
                    CorporateCapitalManYenRequiredForTransactionsFormItem,
                fieldNames: [
                    "capital_man_yen_required_for_transactions_gt",
                    "capital_man_yen_required_for_transactions_lt",
                ],
                onClear,
            },
            {
                fieldKey: "license_required_for_transactions",
                searchField: CorporateLicenseRequiredForTransactionsFormItem,
                fieldNames: ["license_required_for_transactions"],
                onClear: undefined,
            },
            {
                fieldKey: "comment_user",
                searchField: CorporateCommentUserFormItem,
                fieldNames: ["comment_user"],
                onClear: undefined,
            },
            {
                fieldKey: "created_user",
                searchField: CorporateCreatedUserFormItem,
                fieldNames: ["created_user"],
                onClear: undefined,
            },
            {
                fieldKey: "modified_user",
                searchField: CorporateModifiedUserFormItem,
                fieldNames: ["modified_user"],
                onClear: undefined,
            },
        ];

        const onUpdateSearchField = () => {
            const postData = {
                content: {
                    organizations: {
                        search: [...unshowSearchFields],
                    },
                },
            };
            updateDisplaySetting(postData);
        };

        const onFieldAdd = (newUnshowFields, fieldKey) => {
            setUnshowSearchFields(newUnshowFields);
        };

        const onFieldRemove = (newUnshowFields, fieldNames) => {
            setUnshowSearchFields(newUnshowFields);
            form.resetFields(fieldNames);
            const values = form.getFieldsValue();
            submitHandler(values);
            syncToUrl(values);
        };

        useEffect(() => {
            const tempInitialData = { ...emptyData, ...initialData };
            const establishmentDateRange =
                tempInitialData["establishment_date_range"] ?? [];
            if (establishmentDateRange.length) {
                tempInitialData["establishment_date_range"] =
                    establishmentDateRange.map((establishmentDate) =>
                        moment(establishmentDate)
                    );
            }
            if (tempInitialData.score === 0) {
                tempInitialData.score = 1;
            }
            const queryParamsObj = queryParamsToObj();
            const formValues = {
                ...tempInitialData,
                ...queryParamsObj,
            };
            form.setFieldsValue(formValues);
            const count = countFilterField(formValues);
            setFilledSearchFieldCount(count);
            submitHandler(formValues);
            syncToUrl(formValues);
        }, []);

        useEffect(() => {
            setInitialFormSearch();
        }, [displaySetting]);

        const setInitialFormSearch = () => {
            const unshowDisplaySettingSearchFields =
                displaySetting &&
                displaySetting[tableName] &&
                displaySetting[tableName][filterType]
                    ? displaySetting[tableName][filterType]
                    : [];
            setUnshowSearchFields(unshowDisplaySettingSearchFields);
        };

        return (
            <>
                <Col>
                    <Row justify="end">
                        <Button.Group>
                            <Button
                                type="primary"
                                size="small"
                                icon={<SearchOutlined />}
                                onClick={() => setIsSearchDrawerOpen(true)}>
                                絞り込み検索
                            </Button>
                            {filledSearchFieldCount ? (
                                <Tooltip title="現在検索中のフィールドを全てクリアします">
                                    <Badge
                                        count={filledSearchFieldCount}
                                        size="small">
                                        <Button
                                            type="primary"
                                            size="small"
                                            icon={<CloseOutlined />}
                                            onClick={onReset}
                                        />
                                    </Badge>
                                </Tooltip>
                            ) : undefined}
                        </Button.Group>
                    </Row>
                </Col>
                <SearchMenuDrawer
                    isDrawerOpen={isSearchDrawerOpen}
                    onDrawerClose={onDrawerClose}
                    form={form}
                    menuItems={menuItems}
                    unshowList={unshowSearchFields}
                    setUnshowList={setUnshowSearchFields}
                    setInitialFormSearch={setInitialFormSearch}
                    onReset={onReset}
                    onUpdateUnshowList={onUpdateSearchField}
                    otherControls={[
                        <IncludeInvalidSwitchFormItem
                            initialValue={initialData.ignore_filter}
                        />,
                        <IncludeBlocklistSwitchFormItem
                            initialValue={initialData.ignore_blocklist_filter}
                        />,
                    ]}
                    onFilter={submitToFilter}
                    searchTemplate={
                        <SearchTemplateSelectFormItem
                            pageId={pageId}
                            searchTemplateUrl={searchTemplateURL}
                            tableName={tableName}
                            reducerName={templateReducerName}
                            onSelect={onTemplateSelect}
                            onDelete={onReset}
                            isDefaultTemplateAttached={
                                isDefaultTemplateAttached
                            }
                            selectedTemplateName={selectedTemplateName}
                            setSelectedTemplateName={setSelectedTemplateName}
                        />
                    }
                    onFieldAdd={onFieldAdd}
                    onFieldRemove={onFieldRemove}
                />
            </>
        );
    }
);

OrganizationSearchForm.defaultProps = {
    initialData: { ...emptyData },
};

export default OrganizationSearchForm;
