import React, { useState } from "react";
import { Col, Row, Form, Switch, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import Path from "~/components/Routes/Paths";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./IncludeInvalidSwitchFormItem.scss";

type Props = {
    initialValue: any;
};

const IncludeInvalidSwitchFormItem = ({ initialValue }: Props) => {
    const [isIgnoreFilter, setIsIgnoreFilter] = useState(!!initialValue);

    return (
        <Col span={24}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Row gutter={6} justify="end">
                        <Col>
                            <label className={styles.switchLabel}>
                                自社取引条件対象外含
                            </label>
                        </Col>
                        <Col>
                            <Form.Item
                                labelCol={{}}
                                wrapperCol={{}}
                                name="ignore_filter"
                                valuePropName="checked"
                                initialValue={isIgnoreFilter}
                                noStyle>
                                <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    onChange={setIsIgnoreFilter}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                <TooltipContentLink
                                    to={`${Path.myCompany}`}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    title="自社プロフィール"
                                />
                                の取引条件を満たしていない取引先を表示させる場合は有効にしてください。
                                <br />
                                <a
                                    href={
                                        Links.helps.filter
                                            .includingOutOfTradingCondition
                                    }
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    詳細
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default IncludeInvalidSwitchFormItem;
