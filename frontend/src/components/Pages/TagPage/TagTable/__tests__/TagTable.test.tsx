import React from "react";
import { TagModel } from "~/models/tagModel";
import TagTable from "../TagTable";
import { tagResponseModelList } from "~/test/mock/tagAPIMock";
import { mockHistoryPush, render, screen, userEvent } from "~/test/utils";
import Paths from "~/components/Routes/Paths";

const mockOnRowSelected = jest.fn();

const mockData: TagModel[] = [
    ...tagResponseModelList.map((tag) => ({
        id: tag.id,
        value: tag.value,
        color: tag.color,
        internalValue: tag.internal_value,
        createdTime: tag.created_time,
        createdUser: tag.created_user__name,
        modifiedTime: tag.modified_time,
        modifiedUser: tag.modified_user__name,
    })),
];

describe("TagTable.tsx", () => {
    test("render test", async () => {
        const { container } = render(
            <TagTable dataSource={mockData} onRowSelected={mockOnRowSelected} />
        );
        const table = screen.getByRole("table");
        expect(table).toBeInTheDocument();
        // NOTE(joshua-hashimoto): タグ色のテスト
        for (const data of mockData) {
            expect(
                container.getElementsByClassName(`ant-tag-${data.color}`).length
            ).toBe(1);
        }
    });

    test("row select test", async () => {
        render(
            <TagTable dataSource={mockData} onRowSelected={mockOnRowSelected} />
        );
        const rowElement = screen.getByText("django");
        await userEvent.click(rowElement);
        const targetTag = tagResponseModelList.find(
            (tag) => tag.value === "django"
        );
        const tagId = targetTag?.id;
        expect(mockHistoryPush).toHaveBeenCalledWith(Paths.tags + "/" + tagId);
    });
});
