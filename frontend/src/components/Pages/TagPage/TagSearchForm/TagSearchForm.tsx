import React, { useEffect } from "react";
import { Form, Input } from "antd";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { TagSearchFormModel } from "~/models/tagModel";
import { useSearchSync } from "~/hooks/useSearch";

type Props = {
    onSearchValueChange: (values: TagSearchFormModel) => void;
};

const TagSearchForm = ({ onSearchValueChange }: Props) => {
    const [form] = Form.useForm<TagSearchFormModel>();
    const { syncToUrl, queryParamsToObj } = useSearchSync();
    const setSearchValueDebounced = useCustomDebouncedCallback(
        (values: TagSearchFormModel) => {
            onSearchValueChange(values);
            syncToUrl(values ?? {});
        }
    );

    useEffect(() => {
        const queryParamsObj = queryParamsToObj<TagSearchFormModel>();
        form.setFieldsValue(queryParamsObj);
    }, []);

    return (
        <Form
            form={form}
            onValuesChange={(changedValues, allValues) => {
                setSearchValueDebounced(allValues);
            }}>
            <Form.Item name="value" noStyle>
                <Input placeholder="タグ名" allowClear />
            </Form.Item>
        </Form>
    );
};

export default TagSearchForm;
