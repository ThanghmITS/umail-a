import React from "react";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import { TagOutlined } from "@ant-design/icons";
import Paths from "~/components/Routes/Paths";

const TagAddButton = () => {
    const router = useHistory();

    return (
        <Button
            type="primary"
            icon={<TagOutlined />}
            onClick={() => router.push(Paths.tagRegister)}
            size="small">
            タグ追加
        </Button>
    );
};

export default TagAddButton;
