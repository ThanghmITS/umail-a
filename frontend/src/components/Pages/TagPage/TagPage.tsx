import React, { useEffect, useState } from "react";
import { Button, Col, Popover, Row, Spin } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { showDeleteModal } from "~/components/Feedbacks/Modal/Modal";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { useBulkDeleteTags, useFetchTagsAPIQuery } from "~/hooks/useTag";
import { PaginationRequestModel } from "~/models/requestModel";
import {
    TagBulkDeleteModel,
    TagModel,
    TagSearchFormModel,
} from "~/models/tagModel";
import TagAddButton from "./TagAddButton/TagAddButton";
import TagSearchForm from "./TagSearchForm/TagSearchForm";
import TagTable from "./TagTable/TagTable";
import CustomPagination from "~/components/Common/CustomPagination/CustomPagination";
import TableDeleteButton from "~/components/Common/TableDeleteButton/TableDeleteButton";
import NotFoundPage from "../NotFoundPage";
import { SearchOutlined } from "@ant-design/icons";
import { useSearchSync } from "~/hooks/useSearch";
import { isEmpty } from "~/utils/utils";
import styles from "./TagPage.scss";

const TagPage = () => {
    const { queryParamsToObj } = useSearchSync();
    const [deps, setDeps] = useState<PaginationRequestModel>({
        page: 1,
        value: "",
    });
    const [selectedRows, setSelectedRows] = useState<TagModel[]>([]);
    const { _all: tagAuthorization } = useAuthorizedActions("tags");
    const { mutate: bulkDelete, isLoading: isBulkDeleteProcessing } =
        useBulkDeleteTags();
    const { data, isLoading, isError } = useFetchTagsAPIQuery({
        deps,
    });

    const onSearch = ({ value }: TagSearchFormModel) => {
        setDeps({
            page: 1,
            value,
        });
    };

    useEffect(() => {
        const queryParamsObj = queryParamsToObj<TagSearchFormModel>();
        if (!!queryParamsObj && !isEmpty(queryParamsObj)) {
            onSearch(queryParamsObj);
        }
    }, []);

    if (!tagAuthorization) {
        return <NotFoundPage />;
    }

    return (
        <Spin spinning={isLoading}>
            <Col span={24}>
                <Row justify="space-between" align="middle">
                    <Col span={12}>
                        <CustomPageHeader title="タグ設定" />
                    </Col>
                    <Col span={12}>
                        <Row justify="end">
                            <TagAddButton />
                            <Popover
                                placement="bottom"
                                content={
                                    <TagSearchForm
                                        onSearchValueChange={onSearch}
                                    />
                                }
                                trigger="click">
                                <Button
                                    type="primary"
                                    style={{ marginLeft: "10px" }}
                                    size="small"
                                    icon={<SearchOutlined />}>
                                    絞り込み検索
                                </Button>
                            </Popover>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Row style={{ marginTop: "1%", marginBottom: "1%" }}>
                            <Col span={24}>
                                <TagTable
                                    dataSource={data?.results}
                                    onRowSelected={(_, selected) =>
                                        setSelectedRows(selected)
                                    }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Row justify="start">
                                    <TableDeleteButton
                                        selectedRows={selectedRows}
                                        isAuthorized={tagAuthorization}
                                        isLoading={isBulkDeleteProcessing}
                                        onClick={showDeleteModal(() => {
                                            const postData: TagBulkDeleteModel =
                                                {
                                                    source: selectedRows.map(
                                                        (selectedRow) =>
                                                            selectedRow.id
                                                    ),
                                                };
                                            bulkDelete(postData);
                                        }, "tagPage")}
                                    />
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Row justify="end">
                                    <Col>
                                        <CustomPagination
                                            current={deps.page}
                                            total={data?.count ?? 0}
                                            defaultCurrent={deps.page}
                                            onChange={(selectedPage, _) => {
                                                setDeps({
                                                    ...deps,
                                                    page: selectedPage,
                                                });
                                            }}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </Spin>
    );
};

export default TagPage;
