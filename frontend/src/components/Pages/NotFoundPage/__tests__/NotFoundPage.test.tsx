import React from "react";
import {
    mockHistoryGoBack, renderWithAllProviders,
    screen,
    userEvent,
} from "~/test/utils";
import NotFoundPage from "../../NotFoundPage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";

describe("NotFoundPage.tsx", () => {
    test("render test", () => {
        renderWithAllProviders(<NotFoundPage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                    },
                },
            }),
        });
        const titleElement = screen.getByText(/^ページが見つかりません$/);
        expect(titleElement).toBeInTheDocument();
        const subtextElement = screen.getByText(
            /^ご指定のURLに対応するページが見つかりませんでした。$/
        );
        expect(subtextElement).toBeInTheDocument();
        const backButtonElement = screen.getByRole("button", {
            name: /^戻る$/,
        });
        expect(backButtonElement).toBeInTheDocument();
        expect(backButtonElement).not.toBeDisabled();
    });

    test("go back test", async () => {
        renderWithAllProviders(<NotFoundPage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                    },
                },
            }),
        });
        const backButtonElement = screen.getByRole("button", {
            name: /^戻る$/,
        });
        await userEvent.click(backButtonElement);
        expect(mockHistoryGoBack).toBeCalled();
    });
});
