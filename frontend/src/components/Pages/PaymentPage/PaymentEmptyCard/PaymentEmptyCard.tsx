import React from "react";
import { Button, Col, Row, Typography } from "antd";
import styles from "./PaymentEmptyCard.scss";
import { PlusOutlined } from "@ant-design/icons";

type Props = {
    isMainCard: boolean;
    onOpenRegisterModal: () => void;
};

const PaymentEmptyCard = ({
    isMainCard,
    onOpenRegisterModal,
}: Props) => {

    const title = isMainCard ? "クレジットカード情報を登録する" : "予備のクレジットカードを追加する";
    const description = isMainCard ? "" : "メインのクレジットカードで決済に失敗した場合、¥n予備のクレジットカードで決済が行われます。";
    const breakedText = description.split("¥n").map((line, key) => <span key={key}>{line}<br /></span>);
    const renderAddButton = () => {
        return (
            <Button
                type="link"
                icon={<PlusOutlined />}
                onClick={onOpenRegisterModal}>
                {title}
            </Button>
        );
    };

    return (
        <div className={styles.box}>
            <Col span={24} style={{ height: "100%" }}>
                <Row justify="center" align="middle" style={{ height: "100%" }}>
                    <Col>
                        <Row justify="center">{renderAddButton()}</Row>
                        <Row justify="center">
                            <Typography.Text>{breakedText}</Typography.Text>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </div>
    );
};

export default PaymentEmptyCard;
