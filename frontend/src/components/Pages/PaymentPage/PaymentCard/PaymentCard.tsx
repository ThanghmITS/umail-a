import React from "react";
import { Avatar, Button, Card, Col, Row, Form, Switch, Tooltip } from "antd";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import { PaymentCardDataModel, PaymentDeleteModel, PaymentUpdateModel } from "~/models/paymentModel";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import { zeroLeftPadding } from "~/utils/utils";
import { dangerColor, iconCustomColor } from "~/utils/constants";
import styles from "./PaymentCard.scss";
import {
    CheckOutlined,
    CloseOutlined,
    QuestionCircleFilled,
} from "@ant-design/icons";

type Props = {
    payment: PaymentCardDataModel;
    isMainCard: boolean;
    showSwitchDelete: boolean;
    onOpenUpdateModal: () => void;
    onDeleteModal: (postData: PaymentDeleteModel) => void
    onUpdate: (postData: PaymentUpdateModel) => void
};

const PaymentCard = ({ payment, isMainCard, showSwitchDelete, onOpenUpdateModal, onDeleteModal, onUpdate }: Props) => {
    // NOTE(joshua-hashimoto): クレジットカードのアイコンのURLを返却
    const creditCardIconURL = () => {
        return `${
            window.location.origin
        }/static/app_staffing/creditcard/${payment.brand
            .split(" ")
            .join("")
            .toLowerCase()}.svg`;
    };

    const { isLoading, isError, data } = usePlanSummaryAPIQuery({});
    if (isLoading || isError) {
        return null;
    }

    // NOTE(joshua-hashimoto): 有効月を左0埋めで返却
    const expMonth = (): string => {
        return zeroLeftPadding(payment.exp_month, 2);
    };

    // NOTE(joshua-hashimoto): 有効年の後2つを返却
    const expYear = (): string => {
        return payment.exp_year.toString().slice(-2);
    };

    const renderUpdateButton = () => {
        return <Button onClick={onOpenUpdateModal} style={{ marginRight: 10 }}>更新</Button>;
    };

    const onConfirmDelete = () => {
        confirmModal({
            title: "このクレジットカードを削除しますか？",
            content: (
                <>
                    <div>
                        <p>{`OKを押すと、削除が実行されます。`}</p>
                        <p>{`元には戻せません。`}</p>
                    </div>
                    <div>
                        <p>{`※メインのクレジットカードを削除する場合、削除後は現在の予備のクレジットカードがメインとして登録されます。`}</p>
                    </div>
                </>
            ),
            onOk: () => {onDeleteModal({ cardId: payment.id })},
            onCancel: () => {},
        });
    };

    const onUpdateMainCard = (checked: boolean) => {
        if (checked) {
            onUpdate({cardId: payment.id, defaultCardId: payment.id})
        } else {
            onUpdate({cardId: payment.id, defaultCardId: ''})
        }
    }

    const renderDeleteButton = () => {
        return <Button onClick={onConfirmDelete} type="primary" danger style={{ marginRight: 10 }}>削除</Button>;
    };

    const renderSwitchMainCard = () => {
        return <>
            <Col>
                <Form.Item style={{ marginBottom: 0 }}
                    label={
                        <span>
                            メインとして登録&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        有効にするとメインのクレジットカードとして登録されます。
                                        <br />
                                        メインのクレジットカードでの決済が失敗した場合、
                                        <br />
                                        予備のクレジットカードで決済が行われます。
                                    </span>
                                }
                                overlayStyle={{
                                    maxWidth: 500,
                                    overflowWrap: "break-word",
                                }}
                                >
                                <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                            </Tooltip>
                        </span>
                    }
                ></Form.Item>
            </Col>
            <Col style={{ marginRight: 10 }}>
                <Form.Item
                    labelCol={{}}
                    wrapperCol={{}}
                    name="is_main_card"
                    valuePropName="checked"
                    initialValue={isMainCard}
                    noStyle>
                    <Switch
                        checkedChildren={<CheckOutlined />}
                        unCheckedChildren={<CloseOutlined />}
                        onChange={(checked) => {onUpdateMainCard(checked)}}
                        defaultChecked={isMainCard}
                    />
                </Form.Item>
            </Col>
        </>;
    };

    const actions = [
        <Col span={24} style={{ paddingRight: "3%" }}>
            <Row justify="end" align="middle">
                {showSwitchDelete && renderSwitchMainCard()}
                {renderUpdateButton()}
                {showSwitchDelete && renderDeleteButton()}
            </Row>
        </Col>,
    ];

    const cardHasError =  data?.paymentErrorCardIds && data.paymentErrorCardIds.includes(payment.id)

    const warningCardStyle = {
        borderColor: cardHasError ? dangerColor : "unset",
        borderStyle: cardHasError ? "dashed" : "none",
    }

    return (
        <>
            <Card actions={actions} bordered style={{ width: "100%",...warningCardStyle }}>
                <Card.Meta
                    avatar={
                        <Avatar
                            src={creditCardIconURL()}
                            shape="square"
                            style={{ width: "100%" }}
                        />
                    }
                    title={`${payment.brand} **** ${payment.last4}`}
                    description={`有効期限 ${expMonth()}/${expYear()}`}
                    style={{ textAlign: "left" }}></Card.Meta>
            </Card>
        </>
    );
};

export default PaymentCard;
