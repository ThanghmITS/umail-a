import React, { useEffect, useState } from "react";
import {
    Avatar,
    Button,
    Col,
    Divider,
    Form,
    Input,
    Modal,
    Row,
    Typography,
} from "antd";
import {
    PaymentCreateModel,
    PaymentTokenGenerateModel,
} from "~/models/paymentModel";
import axios from "axios";
import {
    Links,
    PAYJP_DEV_PUBLIC_KEY,
    PAYJP_LIVE_PUBLIC_KEY,
    PRODUCTION_DOMAIN,
} from "~/utils/constants";
import styles from "./PayjpForm.scss";

declare global {
    interface Window {
        Payjp: any;
    }
}

type CreditCardBrandModel =
    | "Visa"
    | "MasterCard"
    | "JCB"
    | "American Express"
    | "Diners Club"
    | "Discover"
    | "unknown";

type CreditCardNumberInputModel = {
    elementType: string;
    complete: boolean;
    empty: boolean;
    error: any;
    brand: CreditCardBrandModel;
};

const formLayoutLabelColSpan = 6;
const formLayout = {
    labelCol: {
        span: formLayoutLabelColSpan,
    },
    wrapperCol: {
        span: 24 - formLayoutLabelColSpan,
    },
};

const domain = window.location.host;
const payjpKey =
    domain === PRODUCTION_DOMAIN ? PAYJP_LIVE_PUBLIC_KEY : PAYJP_DEV_PUBLIC_KEY;
const payjp = window.Payjp(payjpKey);
const elements = payjp.elements();
const commonStyle = {
    base: {
        color: "#262626",
        fontSize: "13px",
        "::placeholder": {
            color: "#D5D5D5",
            fontSize: "13px",
            lineHeight: "100%",
        },
    },
};

type Props = {
    onPaymentTokenGenerate: (data: PaymentCreateModel) => void;
    onFinish: () => void;
    oldCardId: string;
    cardOrder: number;
};

const PayjpForm = ({ onPaymentTokenGenerate, onFinish, oldCardId, cardOrder }: Props) => {
    const [creditCompanyName, setCreditCompanyName] =
        useState<CreditCardBrandModel>("unknown");
    const [name, setName] = useState<string | undefined>(undefined);
    const [isCreditNumberInputError, setIsCreditNumberInputError] =
        useState(true);
    const [isCreditExpiryInputError, setIsCreditExpiryInputError] =
        useState(true);
    const [isCreditCvcInputError, setIsCreditCvcInputError] = useState(true);
    const [isCreditNameInputError, setIsCreditNameInputError] = useState(true);

    const getCreditCardIconURL = (creditCardTitle: CreditCardBrandModel) => {
        const convertedBrandTitle = creditCardTitle
            .split(" ")
            .join("")
            .toLowerCase();
        return `${window.location.origin}/static/app_staffing/creditcard/${convertedBrandTitle}.svg`;
    };

    const onFinishTokenGenerate = () => {
        onFinish();
        setCreditCompanyName("unknown");
        setName(undefined);
        // NOTE(joshua-hashimoto): 手動で入力値をクリア。完了後、再度表示されたときに値が残ってしまうため。
        const numberElement = elements.getElement("cardNumber");
        const expiryElement = elements.getElement("cardExpiry");
        const cvcElement = elements.getElement("cardCvc");
        numberElement.clear();
        expiryElement.clear();
        cvcElement.clear();
    };

    const onTokenGenerate = async (event: any) => {
        try {
            const numberElement = elements.getElement("cardNumber");
            const response: PaymentTokenGenerateModel = await payjp.createToken(
                numberElement,
                {
                    card: {
                        name,
                    },
                }
            );
            onPaymentTokenGenerate({ token: response.id, oldCardId: oldCardId, cardOrder: cardOrder });
        } catch (err) {
            if (axios.isAxiosError(err)) {
                Modal.error({
                    title: "クレジットカード情報登録エラー",
                    content: err.message,
                    zIndex: 10000,
                });
            }
            console.error(err);
        }
        onFinishTokenGenerate();
    };

    const checkInputChangeError = (
        event: CreditCardNumberInputModel
    ): boolean => {
        const error = event.error;
        if (error) {
            return true;
        }
        const empty = event.empty;
        if (empty) {
            return true;
        }
        const complete = event.complete;
        if (!complete) {
            return true;
        }
        return false;
    };

    const onCreditNumberInputChange = (event: CreditCardNumberInputModel) => {
        const hasError = checkInputChangeError(event);
        setIsCreditNumberInputError(hasError);
        const svgName = event.brand;
        setCreditCompanyName(svgName);
    };

    const onCreditExpiryInputChange = (event: CreditCardNumberInputModel) => {
        const hasError = checkInputChangeError(event);
        setIsCreditExpiryInputError(hasError);
    };

    const onCreditCvcInputChange = (event: CreditCardNumberInputModel) => {
        const hasError = checkInputChangeError(event);
        setIsCreditCvcInputError(hasError);
    };

    const renderCreditCardAvatar = (creditTitle: CreditCardBrandModel) => {
        return (
            <Col>
                <Avatar
                    shape="square"
                    src={getCreditCardIconURL(creditTitle)}
                    style={{ width: "100%" }}
                />
            </Col>
        );
    };

    const renderCreditCardIcons = () => {
        return (
            <Row justify="center" gutter={6}>
                {renderCreditCardAvatar("Visa")}
                {renderCreditCardAvatar("MasterCard")}
                {renderCreditCardAvatar("JCB")}
                {renderCreditCardAvatar("American Express")}
                {renderCreditCardAvatar("Discover")}
                {renderCreditCardAvatar("Diners Club")}
            </Row>
        );
    };

    const renderRegisterButton = () => {
        return (
            <Row justify="center">
                <Col span={18}>
                    <Button
                        onClick={onTokenGenerate}
                        type="primary"
                        style={{ width: "100%" }}
                        disabled={
                            isCreditNumberInputError ||
                            isCreditExpiryInputError ||
                            isCreditCvcInputError ||
                            isCreditNameInputError
                        }>
                        お支払い情報を登録する
                    </Button>
                </Col>
            </Row>
        );
    };

    const renderPoweredBy = () => {
        return (
            <Row>
                <Col span={24}>
                    <Typography.Text>
                        Powered by{" "}
                        <Typography.Link href={Links.services.payjp} target="_blank" rel="noopener noreferrer">
                            PAY.JP
                        </Typography.Link>
                    </Typography.Text>
                </Col>
            </Row>
        );
    };

    useEffect(() => {
        // NOTE(joshua-hashimoto): 要素の存在確認。なければ作成。これをしないと、画面遷移の後にモーダルを開くとアプリケーションがクラッシュする
        let numberElement = elements.getElement("cardNumber");
        if (!numberElement) {
            numberElement = elements.create("cardNumber", {
                placeholder: "1111 2222 3333 4444",
                style: commonStyle,
            });
        }
        let expiryElement = elements.getElement("cardExpiry");
        if (!expiryElement) {
            expiryElement = elements.create("cardExpiry", {
                placeholder: "月 / 年",
                style: commonStyle,
            });
        }
        let cvcElement = elements.getElement("cardCvc");
        if (!cvcElement) {
            cvcElement = elements.create("cardCvc", {
                placeholder: "000",
                style: commonStyle,
            });
        }
        // NOTE(joshua-hashimoto): 入力欄を要素にmount
        numberElement.mount("#number-form");
        expiryElement.mount("#expiry-form");
        cvcElement.mount("#cvc-form");
        // NOTE(joshua-hashimoto): 入力イベントを設定
        numberElement.on("change", onCreditNumberInputChange);
        expiryElement.on("change", onCreditExpiryInputChange);
        cvcElement.on("change", onCreditCvcInputChange);

        return () => {
            // NOTE(joshua-hashimoto): 念の為コンポーネントが破壊されるときに要素をunmount
            const numberElement = elements.getElement("cardNumber");
            const expiryElement = elements.getElement("cardExpiry");
            const cvcElement = elements.getElement("cardCvc");
            numberElement.unmount();
            expiryElement.unmount();
            cvcElement.unmount();
        };
    }, []);

    return (
        <Col span={24}>
            <Row justify="center">
                <Col span={18}>
                    {renderCreditCardIcons()}
                    {/** NOTE(joshua-hashimoto): payjp.jsから要素のマウントが必要な関係上、レンダリング初期に以下の部分が必要なためここはメソッドに切り分けることができない。 */}
                    <Form labelAlign="right">
                        <Divider />
                        <Form.Item label="カード番号" {...formLayout}>
                            <Row align="middle">
                                <Col flex="auto">
                                    <div
                                        id="number-form"
                                        className="payjs-outer"></div>
                                </Col>
                                <Col span={5}>
                                    <Avatar
                                        shape="square"
                                        src={getCreditCardIconURL(
                                            creditCompanyName
                                        )}
                                        style={{ height: "100%" }}
                                    />
                                </Col>
                            </Row>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="有効期限" {...formLayout}>
                            <Row align="middle">
                                <Col span={24}>
                                    <div
                                        id="expiry-form"
                                        className="payjs-outer"></div>
                                </Col>
                            </Row>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="CVC" {...formLayout}>
                            <Row align="middle">
                                <Col span={24}>
                                    <div
                                        id="cvc-form"
                                        className="payjs-outer"></div>
                                </Col>
                            </Row>
                        </Form.Item>
                        <Divider />
                        <Form.Item label="カード名義" {...formLayout}>
                            <Row align="middle">
                                <Col span={24}>
                                    <Input
                                        value={name}
                                        onChange={(event) => {
                                            const value = event.target.value;
                                            if (!value) {
                                                setIsCreditNameInputError(true);
                                                setName(value);
                                                return;
                                            }
                                            setIsCreditNameInputError(false);
                                            setName(value.toUpperCase());
                                        }}
                                        bordered={false}
                                        placeholder="TARO YAMADA"
                                        style={{
                                            paddingLeft: 0,
                                            paddingRight: 0,
                                        }}
                                    />
                                </Col>
                            </Row>
                        </Form.Item>
                        <Divider />
                    </Form>
                    {renderRegisterButton()}
                    <div style={{ height: "2%" }}></div>
                    {renderPoweredBy()}
                </Col>
            </Row>
        </Col>
    );
};

export default PayjpForm;
