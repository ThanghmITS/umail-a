import React, { useEffect, useState } from "react";
import { Card, Col, Row, Spin } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import PaymentCard from "./PaymentCard/PaymentCard";
import PaymentEmptyCard from "./PaymentEmptyCard/PaymentEmptyCard";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import { usePaymentAPI, usePaymentRedux } from "~/hooks/usePayment";
import { PaymentCardDataModel } from "~/models/paymentModel";
import PayjpModal from "./PayjpModal/PayjpModal";
import styles from "./PaymentPage.scss";

const PaymentPage = () => {
    const { fetchPayment } = usePaymentAPI();

    const { isPaymentInfoLoading, paymentInfo } = usePaymentRedux();
    const { createPayment, deletePayment, updatePayment } = usePaymentAPI();
    const { isLoading: isPaymentFormLoading } = useSelector(
        (state: RootState) => state.paymentForm
    );
    const { isLoading: isPaymentDeleteLoading } = useSelector(
        (state: RootState) => state.paymentDelete
    );
    const { isLoading: isPaymentUpdateLoading } = useSelector(
        (state: RootState) => state.paymentUpdate
    );
    const [isPayjpModalOpen, setIsPayjpModalOpen] = useState(false);
    const [paymentID, setPaymentID] = useState("");
    const [cardOrder, setCardOrder] = useState(0);

    const renderPaymentCard = (payment: PaymentCardDataModel, showSwitchDelete: boolean, order: number) => {
        return (
            <Col span={8} key={payment.id + payment.isMainCard}>
                <PaymentCard
                    payment={payment}
                    isMainCard={payment.isMainCard}
                    showSwitchDelete={showSwitchDelete}
                    onOpenUpdateModal={() => {setIsPayjpModalOpen(true);setPaymentID(payment.id);setCardOrder(order);}}
                    onDeleteModal={deletePayment}
                    onUpdate={updatePayment}
                />
            </Col>
        );
    };

    const renderMainPaymentEmptyCard = (isMainCard: boolean, order: number) => {
        return (
            <Col span={8}>
                <PaymentEmptyCard
                    isMainCard={isMainCard}
                    onOpenRegisterModal={() => {setIsPayjpModalOpen(true);setPaymentID("");setCardOrder(order);}}
                />
            </Col>
        );
    };

    useEffect(() => {
        if (!isPaymentFormLoading && !isPaymentDeleteLoading && !isPaymentUpdateLoading) {
            fetchPayment();
        }
    }, [isPaymentFormLoading, isPaymentDeleteLoading, isPaymentUpdateLoading]);

    return (
        <>
            <Spin
                spinning={
                    isPaymentInfoLoading ||
                    isPaymentFormLoading ||
                    isPaymentDeleteLoading ||
                    isPaymentUpdateLoading
                }>
                <CustomPageHeader title="お支払い" />
                <Card>
                    <Col span={24}>
                        <Row gutter={12}>
                            {paymentInfo?.data.length
                                ? renderPaymentCard(paymentInfo.data[0], paymentInfo.data.length > 1, 0)
                                : renderMainPaymentEmptyCard(true, 0)}
                        </Row>
                    </Col>
                </Card>
                {paymentInfo?.data.length
                    &&
                    <Card style={{ marginTop: 50 }}>
                        <Col span={24}>
                            <Row gutter={12}>
                                {paymentInfo.data.length > 1
                                    ? renderPaymentCard(paymentInfo.data[1], paymentInfo.data.length > 1, 1)
                                    : renderMainPaymentEmptyCard(false, 1)}
                            </Row>
                        </Col>
                    </Card>
                }
            </Spin>
            <PayjpModal
                visible={isPayjpModalOpen}
                onCancel={() => setIsPayjpModalOpen(false)}
                onFinish={() => setIsPayjpModalOpen(false)}
                onPaymentTokenGenerate={createPayment}
                oldCardId={paymentID}
                cardOrder={cardOrder}
            />
        </>
    );
};

export default PaymentPage;
