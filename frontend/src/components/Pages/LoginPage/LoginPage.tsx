import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import Messages from "~/components/Feedbacks/Alert";
import { RootState } from "~/models/store";
import LoginForm from "./LoginForm/LoginForm";
import styles from "./LoginPage.scss";

type Props = {
    redirectTo?: string;
};

const LoginPage = ({ redirectTo = Paths.dashboard }: Props) => {
    const { token, isLoggingOut } = useSelector(
        (state: RootState) => state.login
    );
    const errorMessage = useSelector((state: RootState) => state.login.message);
    const router = useHistory();

    useEffect(() => {
        if (!!token && !isLoggingOut) {
            router.push(redirectTo);
        }
    }, []);

    return (
        <div className={styles.container}>
            <Messages errorMessage={errorMessage} />
            <div className={styles.pageBody}>
                <h1>コモレビ へ ログイン</h1>
                <LoginForm />
            </div>
        </div>
    );
};

export default LoginPage;
