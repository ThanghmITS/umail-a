import React, { useEffect } from "react";
import { Button, Col, Form, Input, Row } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import Paths from "~/components/Routes/Paths";
import { Link, useHistory } from "react-router-dom";
import { LoginFormModel } from "~/models/authModel";
import {
    useTenantFetchRecaptchaAPIMutation,
    useTenantFetchRecaptchaAPIQuery,
} from "~/hooks/useTenant";
import {
    ErrorMessages,
    ONLY_HANKAKU_REGEX,
    RESTRICT_SPACE_REGEX,
} from "~/utils/constants";
import ReCAPTCHA from "react-google-recaptcha";
import { useTypedSelector } from "~/models/store";
import { useLoginAPIMutation } from "~/hooks/useAuth";
import styles from "./LoginForm.scss";

const formCol = {
    xxl: 5,
    xl: 5,
    lg: 5,
    md: 5,
    sm: 7,
    xs: 7,
};

const LoginForm = () => {
    const requiredFields = ["recaptcha"];
    const router = useHistory();
    const [form] = Form.useForm<LoginFormModel>();
    const { threeLogin } = useTypedSelector((state) => state.login);
    const { mutate: login } = useLoginAPIMutation();
    const { mutate: fetchRecaptcha } = useTenantFetchRecaptchaAPIMutation();
    const { data } = useTenantFetchRecaptchaAPIQuery({
        options: { enabled: false },
    });

    const handleSubmit = async () => {
        try {
            const values = await form.validateFields();
            login(values, {
                onSuccess: () => {
                    form.resetFields();
                    router.push(Paths.dashboard);
                },
            });
        } catch (err) {
            // NOTE(joshua-hashimoto): ログイン周りのエラーメッセージはここでは何も表示しない
        }
        /**
         * NOTE(joshua-hashimoto):
         *
         * ログイン処理を実行したら必ずパスワード入力欄を空にする。
         * ログインがエラーになればそのまま空になったものがユーザーに表示される(仕様通り)。
         */
        form.setFields([
            {
                name: "password",
                value: "",
            },
        ]);
    };

    useEffect(() => {
        if (threeLogin >= 3) {
            fetchRecaptcha();
        }
    }, [threeLogin]);

    return (
        <Col span={24}>
            <Row justify="center">
                <Col
                    {...formCol}
                    style={{
                        marginTop: "32px",
                    }}>
                    <Form
                        onFinish={handleSubmit}
                        form={form}
                        validateMessages={validateJapaneseMessages}>
                        <Form.Item
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: "メールアドレスを入力してください",
                                },
                                {
                                    pattern: ONLY_HANKAKU_REGEX,
                                    message:
                                        ErrorMessages.validation.regex
                                            .onlyHankaku,
                                },
                                {
                                    pattern: RESTRICT_SPACE_REGEX,
                                    message:
                                        ErrorMessages.validation.regex.space,
                                },
                                {
                                    max: 100,
                                    message:
                                        ErrorMessages.validation.length.max100,
                                },
                            ]}>
                            <Input
                                prefix={
                                    <UserOutlined
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder="メールアドレス"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: "パスワードを入力してください",
                                },
                            ]}>
                            <Input.Password
                                prefix={
                                    <LockOutlined
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder="パスワード"
                            />
                        </Form.Item>
                        {threeLogin >= 3 && data?.sitekey && (
                            <Form.Item noStyle>
                                <Col span={24}>
                                    <Row justify="center">
                                        <Col>
                                            {!data.testMode && (
                                                <Form.Item
                                                    name="recaptcha"
                                                    noStyle>
                                                    <ReCAPTCHA
                                                        sitekey={data.sitekey}
                                                    />
                                                </Form.Item>
                                            )}
                                        </Col>
                                    </Row>
                                </Col>
                            </Form.Item>
                        )}
                        <Form.Item shouldUpdate>
                            {() => (
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    className={styles.button}
                                    disabled={
                                        !form.isFieldsTouched(
                                            threeLogin >= 3 && data?.sitekey
                                                ? requiredFields.filter(
                                                      (field) => {
                                                          if (data.testMode) {
                                                              return (
                                                                  field !==
                                                                  "recaptcha"
                                                              );
                                                          }
                                                          return !data.testMode;
                                                      }
                                                  )
                                                : [],
                                            true
                                        ) ||
                                        !!form
                                            .getFieldsError()
                                            .filter(
                                                ({ errors }) => errors.length
                                            ).length
                                    }>
                                    ログイン
                                </Button>
                            )}
                        </Form.Item>
                        <Form.Item>
                            <div>
                                <Link to={`${Paths.passwordReset}`}>
                                    パスワードをお忘れの方
                                </Link>
                            </div>
                        </Form.Item>
                        <Form.Item>
                            <div>
                                <Link to={Paths.tenant}>
                                    新しく企業アカウントを作成する
                                </Link>
                            </div>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Col>
    );
};

export default LoginForm;
