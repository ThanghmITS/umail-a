import React from "react";
import { Col, Row, Spin } from "antd";
import { useHistory } from "react-router-dom";
import CustomDetailPageContent from "~/components/Common/CustomDetailPageContent/CustomDetailPageContent";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import MyProfileForm from "~/components/Forms/MyProfileForm/MyProfileForm";
import Paths from "~/components/Routes/Paths";
import {
    useFetchMyProfileAPIQuery,
    useUpdateMyProfileAPIMutation,
} from "~/hooks/useMyProfile";
import { MyProfileFormModel } from "~/models/myProfileModel";
import { useTypedSelector } from "~/models/store";
import AccountDeleteButton from "./AccountDeleteButton/AccountDeleteButton";

const MyProfilePage = () => {
    const router = useHistory();
    const role = useTypedSelector((state) => state.login.role);
    const { mutate: updateMyProfile, error } = useUpdateMyProfileAPIMutation();
    const { data, isLoading } = useFetchMyProfileAPIQuery({});

    const onFinish = (values: MyProfileFormModel) => {
        updateMyProfile(values, {
            onSuccess: () => {
                router.push(Paths.dashboard);
            },
        });
    };

    // NOTE(joshua-hashimoto): my_profileは権限情報はないので、権限による表示制限処理はない

    return (
        <Spin spinning={isLoading}>
            <Col span={24}>
                <Row>
                    <Col span={24}>
                        <CustomPageHeader title="個人プロフィール" />
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <CustomDetailPageContent>
                            <MyProfileForm
                                initialData={data}
                                fieldErrors={error?.response?.data}
                                onFinish={onFinish}
                            />
                        </CustomDetailPageContent>
                    </Col>
                </Row>
                {role === "master" && (
                    <Row justify="end">
                        <Col>
                            <AccountDeleteButton />
                        </Col>
                    </Row>
                )}
            </Col>
        </Spin>
    );
};

export default MyProfilePage;
