import React from "react";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import {
    mockHistoryPush,
    renderHook,
    renderWithAllProviders,
    screen,
    userEvent,
    waitForElementToBeRemoved,
} from "~/test/utils";
import MyProfilePage from "../MyProfilePage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import Paths from "~/components/Routes/Paths";
import { SuccessMessages } from "~/utils/constants";

describe("MyProfilePage.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test with master role", async () => {
        const { container } = renderWithAllProviders(<MyProfilePage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: true,
                            },
                        },
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() =>
            container.querySelector(".ant-spin-spinning")
        );
        const pageTitleElement = screen.getByText(/^個人プロフィール$/);
        expect(pageTitleElement).toBeInTheDocument();
        const accountDeleteButtonElement = screen.getByRole("button", {
            name: /^退会$/,
        });
        expect(accountDeleteButtonElement).toBeInTheDocument();
        expect(accountDeleteButtonElement).not.toBeDisabled();
        // NOTE(joshua-hashimoto): 簡易的なMyProfileFormの確認
        // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
        // expect(profileLabelElement).toBeInTheDocument();
    });

    test("render test with admin role", async () => {
        const { container } = renderWithAllProviders(<MyProfilePage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "admin",
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: false,
                            },
                        },
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() =>
            container.querySelector(".ant-spin-spinning")
        );
        const pageTitleElement = screen.getByText(/^個人プロフィール$/);
        expect(pageTitleElement).toBeInTheDocument();
        const accountDeleteButtonElement = screen.queryByRole("button", {
            name: /^退会$/,
        });
        expect(accountDeleteButtonElement).not.toBeInTheDocument();
    });

    test("update button router test", async () => {
        const { container } = renderWithAllProviders(<MyProfilePage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: true,
                            },
                        },
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() =>
            container.querySelector(".ant-spin-spinning")
        );
        const updateButtonElement = screen.getByRole("button", {
            name: /^更 新$/,
        });
        await userEvent.click(updateButtonElement);
        const successMessageRegex = new RegExp(SuccessMessages.generic.update);
        const successMessageElement = await screen.findByText(
            successMessageRegex
        );
        expect(successMessageElement).toBeInTheDocument();
        expect(mockHistoryPush).toHaveBeenCalledWith(Paths.dashboard);
    });
});
