import { SHARED_EMAIL_PAGE } from "./pageIds";
import createSearchPage from "~/components/Pages/Factories/createSearchPage";
import { Endpoint } from "~/domain/api";
import {
    sharedEmailSearchParamToAPI,
    convertSharedEmailResponseDataEntry,
} from "~/domain/data";
import SharedEmailSearchForm from "~/components/Forms/SharedEmailSearchForm/SharedEmailSearchForm";
import SharedEmailTable from "~/components/Tables/SharedEmailsTable/SharedEmailsTable";
import SharedEmailSearchInfo from "~/components/DataDisplay/SharedEmailSearchInfo/SharedEmailSearchInfo";
import SharedEmailTableDisplaySettingModal from "../Tables/SharedEmailsTable/SharedEmailTableDisplaySettingModal/SharedEmailTableDisplaySettingModal";

const pageId = SHARED_EMAIL_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmails}`;
const resourceName = "shared_emails";
const changeActiveStatusAuthorized = (authorizedActions) => {
    return true;
};
const deleteAuthorized = (authorizedActions) => {
    return (
        authorizedActions &&
        authorizedActions[resourceName] &&
        authorizedActions[resourceName]["delete"]
    );
};
const csvAuthorized = (authorizedActions) => {
    return true;
};
const columnSettingAuthorized = (authorizedActions) => {
    return true;
};
const accessAuthorized = (authorizedActions) => {
    return true;
};
const searchTemplateAuthorized = (authorizedActions) => {
    return true;
};

const SharedEmailPageContainer = createSearchPage(
    pageId,
    "sharedEmailPage",
    "共有メール 一覧",
    SharedEmailSearchForm,
    SharedEmailTable,
    resourceURL,
    convertSharedEmailResponseDataEntry,
    sharedEmailSearchParamToAPI,
    true,
    undefined,
    undefined,
    true,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    changeActiveStatusAuthorized,
    deleteAuthorized,
    csvAuthorized,
    columnSettingAuthorized,
    accessAuthorized,
    undefined,
    searchTemplateAuthorized,
    resourceName,
    false,
    [],
    [SharedEmailTableDisplaySettingModal],
    undefined,
    [SharedEmailSearchInfo]
);

export default SharedEmailPageContainer;
