import { SCHEDULED_MAIL_SEARCH_PAGE } from "./pageIds";
import createSearchPage from "~/components/Pages/Factories/createSearchPage";
import { Endpoint } from "~/domain/api";
import ScheduledEmailSearchForm from "~/components/Forms/ScheduledEmailSearchForm/ScheduledEmailSearchForm";
import ScheduledEmailsTable from "~/components/Tables/ScheduledMailsTable/ScheduledMailsTable";
import {
    scheduledEmailSearchParamToAPI,
    convertScheduledEmailSummaryResponseDataEntry,
} from "~/domain/data";
import ScheduledEmailTableDisplaySettingModal from "../Tables/ScheduledMailsTable/ScheduledEmailTableDisplaySettingModal/ScheduledEmailTableDisplaySettingModal";

const pageId = SCHEDULED_MAIL_SEARCH_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.scheduledEmails}`;
const columnSettingURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.scheduledEmailColumnSetting
}`;
const copyURL = `${Endpoint.getBaseUrl()}/${Endpoint.scheduledEmails}/${
    Endpoint.copyActionSuffix
}`;
const searchTemplateURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.scheduledEmailSearchTemplate
}`;

const columns = [
    {
        name: "配信日時",
        key: "date_to_send",
    },
    {
        name: "配信種別",
        key: "send_type",
    },
    {
        name: "件名",
        key: "subject",
    },
    {
        name: "添付",
        key: "attachments",
    },
    {
        name: "配信者",
        key: "sender__name",
    },
    {
        name: "配信ステータス",
        key: "status",
    },
    {
        name: "配信数",
        key: "send_total_count",
    },
];

const resourceName = "scheduled_mails";
const changeActiveStatusAuthorized = () => {
    return false;
};
const deleteAuthorized = (authorizedActions) => {
    return (
        authorizedActions &&
        authorizedActions[resourceName] &&
        authorizedActions[resourceName]["delete"]
    );
};
const csvAuthorized = () => {
    return false;
};
const columnSettingAuthorized = (authorizedActions) => {
    return (
        authorizedActions &&
        authorizedActions[resourceName] &&
        authorizedActions[resourceName]["column_setting"]
    );
};
const accessAuthorized = () => {
    return true;
};
const searchTemplateAuthorized = (authorizedActions) => {
    return true;
};

const searchConditionSanitizer = (unshowList, targetData) => {
    const newData = { ...targetData }
    for (const fieldName of unshowList) {
        if (fieldName === "date_to_send") {
            newData["date_range"] = undefined;
        } else if (fieldName === "send_total_count") {
            newData["send_total_count_gt"] = undefined;
            newData["send_total_count_lt"] = undefined;
        } else if (fieldName === "open_count") {
            newData["open_count_gt"] = undefined;
            newData["open_count_lt"] = undefined;
        } else if (fieldName === "sender") {
            newData["sender_id"] = undefined;
        } else {
            newData[fieldName] = undefined;
        }
    }
    return newData;
}

const ScheduledEmailPageContainer = createSearchPage(
    pageId,
    "scheduledEmailSearchPage",
    "配信メール 一覧",
    ScheduledEmailSearchForm,
    ScheduledEmailsTable,
    resourceURL,
    convertScheduledEmailSummaryResponseDataEntry,
    scheduledEmailSearchParamToAPI,
    false,
    undefined,
    undefined, // csvDownloadURL,
    true,
    true,
    columns,
    columnSettingURL,
    undefined,
    true,
    copyURL,
    changeActiveStatusAuthorized,
    deleteAuthorized,
    csvAuthorized,
    columnSettingAuthorized,
    accessAuthorized,
    undefined,
    searchTemplateAuthorized,
    resourceName,
    false,
    [],
    [ScheduledEmailTableDisplaySettingModal],
    searchConditionSanitizer
);

export default ScheduledEmailPageContainer;
