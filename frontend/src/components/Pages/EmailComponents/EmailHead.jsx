import React from "react";
import PropTypes from "prop-types";
import { Divider, Row } from "antd";

import styles from "./EmailHead.scss";
import Description from "../../Navigations/Description";
import DownloadLink from "../../DataDisplay/DownloadLink";
import { Endpoint } from "../../../domain/api";

const attachmentUrlBase = `${Endpoint.getBaseUrl()}/${
    Endpoint.sharedEmailAttachments
}`;

const renderMailAttachments = (attachments = []) => (
    <div className={styles.attachments}>
        {attachments.map((attachment) => (
            <DownloadLink
                key={attachment.name}
                resourceBaseUrl={attachmentUrlBase}
                resourceId={attachment.id}
                fileName={attachment.name}
            />
        ))}
    </div>
);

const MailHead = (props) => {
    const { data } = props;
    if (Object.keys(data).length > 0) {
        // If data is not an empty dict.
        return (
            <div className={styles.container}>
                <Row>
                    <Description
                        term={
                            data.attachments === undefined ? "配信者" : "差出人"
                        }>
                        {data.sender
                            ? `${data.sender}  (${data.email})`
                            : `${data.email}`}
                    </Description>
                    <Divider className={styles.divider} />
                    <Description term="件名">{data.subject}</Description>
                    <div hidden={data.attachments === undefined}>
                        <Divider className={styles.divider} />
                        <Description term="添付ファイル">
                            {renderMailAttachments(data.attachments)}
                        </Description>
                    </div>
                    <Divider className={styles.divider} />
                </Row>
            </div>
        );
    }
    return <Row />;
};

MailHead.propTypes = {
    data: PropTypes.shape({
        sender: PropTypes.string,
        email: PropTypes.string,
        subject: PropTypes.string,
        category: PropTypes.string, // TODO: render it.
        sentDate: PropTypes.string,
        attachments: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string.isRequired,
                name: PropTypes.string.isRequired,
            })
        ),
    }),
};

MailHead.defaultProps = {
    data: {},
};

export default MailHead;
