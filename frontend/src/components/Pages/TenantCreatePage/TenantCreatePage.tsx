import React, { useEffect, useState, ReactNode } from "react";
import { Button, Col, message, Row, Steps } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import TenantSteps from "./TenantSteps/TenantSteps";
import TenantBasicInfo from "./TenantBasicInfo/TenantBasicInfo";
import TenantMyCompanyInfo from "./TenantMyCompanyInfo/TenantMyCompanyInfo";
import TenantMyProfileInfo from "./TenantMyProfileInfo/TenantMyProfileInfo";
import TenantPaymentInfo from "./TenantPaymentInfo/TenantPaymentInfo";
import { useQueryString } from "~/hooks/useQueryString";
import { useHistory } from "react-router-dom";
import { useTenantFetchCurrentStepAPIQuery } from "~/hooks/useTenant";
import Paths from "~/components/Routes/Paths";
import { customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { ErrorMessages } from "~/utils/constants";
import styles from "./TenantCreatePage.scss";

const { Step } = Steps;

type WrapperProps = {
    children: ReactNode;
};

const CustomWrapper = ({ children }: WrapperProps) => {
    return (
        <Col span={24}>
            <Row justify="center">
                <Col span={18}>
                    <Row>{children}</Row>
                </Col>
            </Row>
        </Col>
    );
};

const TenantCreatePage = () => {
    const router = useHistory();
    const { queryParams } = useQueryString();
    const [authToken, setAuthToken] = useState("");
    const [currentStep, setCurrentStep] = useState(0);
    const {} = useTenantFetchCurrentStepAPIQuery({
        authToken,
        deps: [authToken],
        options: {
            onSuccess: (result) => {
                if (result.is_completed) {
                    customSuccessMessage(ErrorMessages.tenant.isRegistered);
                    router.push(Paths.login);
                    return;
                }
                setCurrentStep(result.current_step - 1);
            },
        },
    });

    useEffect(() => {
        const authToken = queryParams.auth_token;
        if (typeof authToken === "string") {
            setAuthToken(authToken);
        } else {
            setAuthToken("");
        }
    }, [queryParams]);

    return (
        <>
            <CustomPageHeader title="テナント作成" />
            <CustomWrapper>
                <TenantSteps currentStep={currentStep} />
                {currentStep === 0 ? <TenantBasicInfo /> : null}
                {currentStep === 1 ? (
                    <TenantMyCompanyInfo authToken={authToken} />
                ) : null}
                {currentStep === 2 ? (
                    <TenantMyProfileInfo authToken={authToken} />
                ) : null}
                {currentStep === 3 ? (
                    <TenantPaymentInfo authToken={authToken} />
                ) : null}
            </CustomWrapper>
        </>
    );
};

export default TenantCreatePage;
