import React, { ReactNode } from "react";
import { Col, Row, Typography } from "antd";
import { primaryColor } from "~/utils/constants";

const { Title, Text } = Typography;

type Props = {
    pointNumber: number;
    title: ReactNode;
    content: ReactNode;
};

const PointText = ({ pointNumber, title, content }: Props) => {
    return (
        <Col span={18} style={{ marginBottom: "5%" }}>
            <Row align="bottom" gutter={4}>
                <Col>
                    <Title level={3} style={{ color: primaryColor }}>
                        Point{pointNumber}.
                    </Title>
                </Col>
                <Col>
                    <Title level={4}>{title}</Title>
                </Col>
            </Row>
            <Row>
                <Col style={{ textAlign: "left" }}>
                    <Text>{content}</Text>
                </Col>
            </Row>
        </Col>
    );
};

export default PointText;
