import React, { ReactNode } from "react";
import {
    Button,
    Card,
    Checkbox,
    Col,
    Form,
    Input,
    message,
    Result,
    Row,
    Skeleton,
    Space,
    Spin,
    Typography,
} from "antd";
import { TenantRegisterBasicInfoModel } from "~/models/tenantModel";
import PointText from "./PointText/PointText";
import ReCAPTCHA from "react-google-recaptcha";
import {
    useTenantFetchRecaptchaAPIQuery,
    useTenantRegisterBasicInfoAPIMutate,
} from "~/hooks/useTenant";
import Spacer from "~/components/Common/Spacer/Spacer";
import { Link, useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import {
    ErrorMessages,
    Links,
    ONLY_HANKAKU_REGEX,
    PASSWORD_REGEX,
    RESTRICT_SPACE_REGEX,
} from "~/utils/constants";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { useLoginAPIMutation } from "~/hooks/useAuth";
import styles from "./TenantBasicInfo.scss";

const { Text, Link: TextLink } = Typography;

const formLayoutLabelSpan = 7;
const formLayout = {
    labelCol: {
        span: formLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - formLayoutLabelSpan,
    },
};

const TenantBasicInfo = () => {
    const requiredFields = [
        "email",
        "password",
        "password_confirm",
        "recaptcha",
        "agree_to_terms_of_service_and_privacy_policy",
    ];
    const router = useHistory();
    const [form] = Form.useForm<TenantRegisterBasicInfoModel>();
    const { mutate: login } = useLoginAPIMutation();
    const { mutate: register, isLoading } =
        useTenantRegisterBasicInfoAPIMutate();
    const {
        data,
        isLoading: isSitekeyLoading,
        isError,
    } = useTenantFetchRecaptchaAPIQuery({});

    const points: { title: ReactNode; content: ReactNode }[] = [
        {
            title: "初日から業務で使える",
            content:
                "無料トライアルの段階から全ての機能が解放され、すぐに使っていただくことができます。",
        },
        {
            title: "データを引き継げる",
            content:
                "無料トライアルで登録いただいたデータは、有料プラン転換後も引き継いで使用することができます。",
        },
    ];

    const disclaimers: string[] = [
        "有料プランへの転換はプランを購入したタイミングで開始されるため、無料トライアルの終了日や決済を気にすることなく、安心してお使いいただけます。",
    ];

    const onLogin = async (username: string, password: string) => {
        try {
            login(
                { username, password },
                {
                    onSuccess: () => {
                        router.push(Paths.dashboard);
                    },
                }
            );
        } catch (err) {}
    };

    const onRegisterBasicInfo = async () => {
        try {
            const values = await form.validateFields();
            register(values, {
                onSuccess: (response) => {
                    form.resetFields();
                    const result = response.data;
                    if (result.is_registered) {
                        onLogin(values.email, values.password);
                    }
                },
            });
        } catch (err) {
            customErrorMessage(ErrorMessages.tenant.registerBaseInfoForm);
        }
    };

    if (isSitekeyLoading) {
        return (
            <Col span={24}>
                <Row justify="center">
                    <Col span={18}>
                        <Skeleton active />
                    </Col>
                </Row>
            </Col>
        );
    }

    if (!data?.sitekey || isError) {
        return (
            <Col span={24}>
                <Row justify="center">
                    <Col>
                        <Result
                            status="warning"
                            title="画面の表示に失敗しました。画面をリフレッシュしてください。"
                            extra={
                                <Button
                                    type="primary"
                                    onClick={() => window.location.reload()}>
                                    画面をリフレッシュ
                                </Button>
                            }
                        />
                    </Col>
                </Row>
            </Col>
        );
    }

    const renderLeftPane = () => {
        return (
            <Card>
                <Form
                    form={form}
                    onFinish={onRegisterBasicInfo}
                    labelAlign="right">
                    <Form.Item
                        {...formLayout}
                        label="メールアドレス"
                        name="email"
                        rules={[
                            {
                                required: true,
                            },
                            {
                                pattern: RESTRICT_SPACE_REGEX,
                                message: ErrorMessages.validation.regex.space,
                            },
                            {
                                pattern: ONLY_HANKAKU_REGEX,
                                message:
                                    ErrorMessages.validation.regex.onlyHankaku,
                            },
                            {
                                max: 100,
                                message: ErrorMessages.validation.length.max100,
                            },
                        ]}
                        style={{ textAlign: "left" }}>
                        <Input type="email" placeholder="you@example.com" />
                    </Form.Item>
                    <Form.Item
                        {...formLayout}
                        label="パスワード"
                        name="password"
                        rules={[
                            {
                                required: true,
                            },
                            {
                                pattern: PASSWORD_REGEX,
                                message:
                                    ErrorMessages.validation.regex.password,
                            },
                        ]}
                        style={{ textAlign: "left" }}>
                        <Input.Password placeholder="大小英数字記号混在で10-50桁" />
                    </Form.Item>
                    <Form.Item
                        {...formLayout}
                        label="パスワード確認"
                        name="password_confirm"
                        dependencies={["password"]}
                        rules={[
                            {
                                required: true,
                            },
                            {
                                pattern: PASSWORD_REGEX,
                                message:
                                    ErrorMessages.validation.regex.password,
                            },
                            ({ getFieldValue }) => ({
                                validator: (_, value) => {
                                    const password = getFieldValue("password");
                                    if (password !== value) {
                                        return Promise.reject(
                                            new Error(
                                                ErrorMessages.form.passwordMismatch
                                            )
                                        );
                                    }
                                    return Promise.resolve();
                                },
                            }),
                        ]}
                        style={{ textAlign: "left" }}>
                        <Input.Password />
                    </Form.Item>
                    <Form.Item>
                        <Col span={24}>
                            <Row justify="center">
                                <Col>
                                    {!data.testMode && (
                                        <Form.Item name="recaptcha" noStyle>
                                            <ReCAPTCHA sitekey={data.sitekey} />
                                        </Form.Item>
                                    )}
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="center">
                                <Col span={24}>
                                    <Form.Item
                                        name="agree_to_terms_of_service_and_privacy_policy"
                                        valuePropName="checked"
                                        rules={[
                                            {
                                                validator: (_, value) => {
                                                    if (value) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        ErrorMessages.tenant
                                                            .registerBaseInfoTOSConfirm
                                                    );
                                                },
                                            },
                                        ]}>
                                        <Checkbox>
                                            <TextLink
                                                href={Links.services.tos}
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                利用規約
                                            </TextLink>
                                            ・
                                            <TextLink
                                                href={
                                                    Links.services
                                                        .privacyAgreement
                                                }
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                個人情報に関する同意書
                                            </TextLink>
                                            に同意する
                                        </Checkbox>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                    <Spacer height={50} />
                    <Col span={24}>
                        <Row justify="center">
                            <Col span={12}>
                                <Form.Item shouldUpdate>
                                    {() => (
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            disabled={
                                                !form.isFieldsTouched(
                                                    data?.testMode
                                                        ? requiredFields.filter(
                                                              (field) =>
                                                                  field !==
                                                                  "recaptcha"
                                                          )
                                                        : requiredFields,
                                                    true
                                                ) ||
                                                !!form
                                                    .getFieldsError()
                                                    .filter(
                                                        ({ errors }) =>
                                                            errors.length
                                                    ).length
                                            }>
                                            会員情報を登録する
                                        </Button>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Form>
            </Card>
        );
    };

    const renderRightPane = () => {
        return (
            <Row justify="center">
                <Col>
                    <Row justify="center">
                        <Col span={12}>
                            <img
                                src="/static/app_staffing/cmrb.png"
                                style={{ width: "100%" }}
                            />
                            <Row justify="end">
                                <Col>
                                    <Text>へようこそ</Text>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row
                        justify="center"
                        style={{ marginTop: 20, marginBottom: 20 }}>
                        <Col>
                            <Text
                                strong
                                style={{
                                    padding: 10,
                                    backgroundColor: "#ffb340",
                                    borderRadius: 10,
                                    color: "#ffffff",
                                }}>
                                10日間無料！
                            </Text>
                        </Col>
                    </Row>
                    <Spacer height={30} />
                    <Space>
                        <Row justify="center" gutter={4}>
                            {points.map((point, index) => (
                                <PointText
                                    key={index}
                                    pointNumber={index + 1}
                                    title={point.title}
                                    content={point.content}
                                />
                            ))}
                        </Row>
                    </Space>
                    <Spacer height={50} />
                    <Space>
                        <Row justify="center">
                            <Col className={styles.disclaimers} span={18}>
                                {disclaimers.map((disclaimer, index) => {
                                    return (
                                        <Col key={index} span={24}>
                                            <Row justify="center">
                                                <Col span={1}>
                                                    <Text strong>※</Text>
                                                </Col>
                                                <Col span={23}>
                                                    <p
                                                        className={
                                                            styles.disclaimerText
                                                        }>
                                                        {disclaimer}
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Col>
                                    );
                                })}
                            </Col>
                        </Row>
                    </Space>
                    <Spacer height={40} />
                    <Row>
                        <Col span={24}>
                            <Row justify="center">
                                <Col span={18}>
                                    <Row justify="center">
                                        <Text>アカウントをお持ちですか？</Text>
                                        <Link to={Paths.login}>
                                            ログインする
                                        </Link>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    };

    return (
        <Col span={24}>
            <Spin spinning={isSitekeyLoading || isLoading}>
                <Col span={24} style={{ marginTop: "3%" }}>
                    <Row>
                        <Col span={12}>{renderLeftPane()}</Col>
                        <Col span={12}>{renderRightPane()}</Col>
                    </Row>
                </Col>
            </Spin>
        </Col>
    );
};

export default TenantBasicInfo;
