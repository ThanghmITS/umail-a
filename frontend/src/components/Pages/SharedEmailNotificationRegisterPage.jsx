import CreateRegisterPage from './Factories/createRegisterPage';

import { SHARED_EMAIL_NOTIFICATION_REGISTER_PAGE } from './pageIds';

import { Endpoint } from '../../domain/api';
import { convertSharedEmailNotificationResponseDataEntry, SharedEmailNotificationFormToAPI } from '../../domain/data';

import Path from '../Routes/Paths';
import SharedEmailNotificationForm from '../Forms/SharedEmailNotificationForm';

const resourceName = 'notification_rules'
const accessAuthorized = (authorizedActions) => { return authorizedActions && authorizedActions[resourceName] && authorizedActions[resourceName]['_all'] }

const pageId = SHARED_EMAIL_NOTIFICATION_REGISTER_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmailNotifications}`;

const SharedEmailNotificationRegisterPageContainer = CreateRegisterPage(
  pageId,
  'sharedEmailNotificationRegisterPage',
  '共有メール通知条件 登録',
  SharedEmailNotificationForm,
  resourceURL,
  Path.sharedMailNotifications,
  convertSharedEmailNotificationResponseDataEntry,
  SharedEmailNotificationFormToAPI,
  undefined,
  accessAuthorized,
);

export default SharedEmailNotificationRegisterPageContainer;
