import createEditPage from './Factories/createEditPage';

import Paths from '../Routes/Paths';
import { SHARED_EMAIL_NOTIFICATION_EDIT_PAGE } from './pageIds';

import SharedEmailNotificationForm from '../Forms/SharedEmailNotificationForm';
import { Endpoint } from '../../domain/api';
import { convertSharedEmailNotificationResponseDataEntry, SharedEmailNotificationFormToAPI } from '../../domain/data';

const pageId = SHARED_EMAIL_NOTIFICATION_EDIT_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmailNotifications}`;

const resourceName = 'notification_rules'
const accessAuthorized = (authorizedActions) => { return authorizedActions && authorizedActions[resourceName] && authorizedActions[resourceName]['_all'] }

const SharedEmailNotificationEditPageContainer = createEditPage(
  pageId,
  'sharedEmailNotificationEditPage',
  '共有メール通知条件 編集',
  SharedEmailNotificationForm,
  resourceURL,
  '',
  Paths.sharedMailNotifications,
  convertSharedEmailNotificationResponseDataEntry,
  SharedEmailNotificationFormToAPI,
  undefined,
  undefined,
  () => { return true },
  accessAuthorized,
);

export default SharedEmailNotificationEditPageContainer;
