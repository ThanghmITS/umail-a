import createEditPage from './Factories/createEditPage';

import { SHARED_EMAIL_SETTING_PAGE } from './pageIds';

import SharedEmailSettingForm from '../Forms/SharedEmailSettingForm';

import { Endpoint } from '../../domain/api';
import Paths from '../Routes/Paths';

import { convertSharedEmailSettingDataEntry } from '../../domain/data';

const pageId = SHARED_EMAIL_SETTING_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmailSetting}`;

const resourceName = 'shared_email_settings'
const accessAuthorized = (authorizedActions) => { return authorizedActions && authorizedActions[resourceName] && authorizedActions[resourceName]['_all'] }

const SharedEmailSettingPage = createEditPage(
    pageId,
    'sharedEmailSettingPage',
    '共有メール設定',
    SharedEmailSettingForm,
    resourceURL,
    '',
    Paths.index,
    convertSharedEmailSettingDataEntry,
    data => data,
    undefined,
    undefined,
    () => { return true },
    accessAuthorized,
    undefined,
    false,
);
export default SharedEmailSettingPage;
