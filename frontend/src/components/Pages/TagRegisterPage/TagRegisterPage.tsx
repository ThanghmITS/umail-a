import React from "react";
import { Col, Row } from "antd";
import { useHistory } from "react-router-dom";
import CustomDetailPageContent from "~/components/Common/CustomDetailPageContent/CustomDetailPageContent";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import TagForm from "~/components/Forms/TagForm/TagForm";
import Paths from "~/components/Routes/Paths";
import { useCreateTagAPIMutation } from "~/hooks/useTag";
import { TagFormModel } from "~/models/tagModel";
import styles from "./TagRegisterPage.scss";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import NotFoundPage from "../NotFoundPage";

const TagRegisterPage = () => {
    const router = useHistory();
    const { _all: tagAuthorization } = useAuthorizedActions("tags");
    const { mutate: createTag } = useCreateTagAPIMutation();

    const onSubmit = (values: TagFormModel) => {
        createTag(values, {
            onSuccess: (response) => {
                router.push(Paths.tags);
            },
        });
    };

    if (!tagAuthorization) {
        return <NotFoundPage />;
    }

    return (
        <Col span={24}>
            <Row>
                <Col span={24}>
                    <CustomPageHeader
                        title="タグ 登録"
                        style={{
                            padding: "16px 0px 0px 0px",
                        }}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <CustomDetailPageContent>
                        <TagForm onSubmit={onSubmit} />
                    </CustomDetailPageContent>
                </Col>
            </Row>
        </Col>
    );
};

export default TagRegisterPage;
