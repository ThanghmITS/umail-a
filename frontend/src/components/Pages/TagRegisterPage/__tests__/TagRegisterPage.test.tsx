import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import { configureStore, PayloadAction } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import {
    contactSearchPage,
    scheduledEmailEditPageContactSearchForm,
} from "~/reducers/pages";
import { generateRandomToken } from "~/utils/utils";
import TagRegisterPage from "../TagRegisterPage";

const randomToken = generateRandomToken();

describe("TagRegisterPage.tsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<TagRegisterPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText(/タグ 登録/);
        expect(pageTitleElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): Formの簡易存在確認
        const tagNameLabelElement = screen.getByText(/タグ名/);
        expect(tagNameLabelElement).toBeInTheDocument();
        const registerButtonElement = await screen.findByRole("button", {
            name: /登 録/,
        });
        expect(registerButtonElement).toBeInTheDocument();
        expect(registerButtonElement).toBeDisabled();
    });

    test("authorized test", () => {
        renderWithAllProviders(<TagRegisterPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.queryByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).not.toBeInTheDocument();
    });

    test("unauthorized test", () => {
        renderWithAllProviders(<TagRegisterPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: false,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.getByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).toBeInTheDocument();
    });
});
