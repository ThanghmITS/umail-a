import React from "react";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import ContactCsvDownloadButton from "../ContactCsvDownloadButton";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { contactSearchPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { displaySettingPage } from "~/reducers/pages";
import { generateRandomToken } from "~/utils/utils";

describe("ContactCsvDownloadButton.tsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<ContactCsvDownloadButton />, {
            store: configureStore({
                reducer: {
                    login,
                    displaySettingPage,
                    contactSearchPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            contacts: {
                                create: true,
                                update: true,
                                delete: true,
                                column_setting: true,
                                csv_upload: true,
                                csv_download: true,
                                search_template: true,
                            },
                        },
                    },
                    displaySettingPage: {
                        requireRefresh: false,
                        canNotDelete: false,
                        loading: false,
                        updated: false,
                        deleted: false,
                        message: "",
                        errorMessage: "",
                        data: {},
                        fieldErrors: {},
                        displaySetting: {
                            users: {
                                search: [],
                            },
                        },
                        isDisplaySettingLoading: false,
                        initialData: {},
                    },
                    contactSearchPage: {
                        ...SearchPageInitialState,
                    },
                },
            }),
        });
        const csvControlButtonElement = screen.getByText(/CSV操作/);
        expect(csvControlButtonElement).toBeInTheDocument();
        expect(csvControlButtonElement).not.toBeDisabled();
        await userEvent.hover(csvControlButtonElement);
        const csvDownloadDropdownItemElement = await screen.findByText(
            /CSVダウンロード/
        );
        expect(csvDownloadDropdownItemElement).toBeInTheDocument();
        const csvUploadDropdownItemElement = await screen.findByText(
            /CSVアップロード/
        );
        expect(csvUploadDropdownItemElement).toBeInTheDocument();
    });
});
