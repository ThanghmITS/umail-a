import React, { useState } from "react";
import { Button, Dropdown, Menu, Tooltip } from "antd";
import {
    useContactCsvDownloadAPIMutation,
    useContactCsvDownloadWithConditionAPIMutation,
} from "~/hooks/useDownloadCsvAPI";
import {
    ContactCsvDownloadType,
    CsvDownloadRequestArgs,
} from "~/models/csvControlModel";
import { DownloadOutlined, FormOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { useReduxState } from "~/hooks/useReduxState";
import CsvDownloadModal from "~/components/Common/CsvDownloadModal/CsvDownloadModal";
import { ErrorMessages } from "~/utils/constants";

const ContactCsvDownloadButton = (params: any) => {
    const [isCsvDownloadModalOpen, setIsCsvDownloadModalOpen] = useState(false);
    const [downloadType, setDownloadType] =
        useState<ContactCsvDownloadType>("all");
    const {
        csv_upload: csvUploadAuthorized,
        csv_download: csvDownloadAuthorized,
    } = useAuthorizedActions("contacts");
    const { totalCount } = useReduxState("contactSearchPage");
    const { mutate: downloadCsv } = useContactCsvDownloadAPIMutation();
    const { mutate: downloadCsvWithCondition } =
        useContactCsvDownloadWithConditionAPIMutation();

    const onDownload = (downloadIndex: number) => {
        const queryString = params.queryString;
        const args: CsvDownloadRequestArgs = {
            index: downloadIndex,
            queryString,
        };
        if (downloadType === "all") {
            downloadCsvWithCondition(args, {
                onSuccess: () => {
                    onCloseDownloadModal();
                },
            });
        } else if (downloadType === "table") {
            downloadCsv(args, {
                onSuccess: () => {
                    onCloseDownloadModal();
                },
            });
        }
    };

    const onOpenDownloadModal = (downloadCsvType: ContactCsvDownloadType) => {
        setDownloadType(downloadCsvType);
        setIsCsvDownloadModalOpen(true);
    };

    const onCloseDownloadModal = () => {
        setIsCsvDownloadModalOpen(false);
    };

    const renderMenu = () => {
        return (
            <Menu>
                <Menu.SubMenu
                    title={
                        !totalCount ? (
                            <Tooltip
                                title={ErrorMessages.csv.nothingToDownload}>
                                CSVダウンロード
                            </Tooltip>
                        ) : (
                            <>CSVダウンロード</>
                        )
                    }
                    icon={<DownloadOutlined />}
                    key="csvDownload"
                    disabled={!totalCount}>
                    <Menu.Item
                        key="table"
                        onClick={() => onOpenDownloadModal("table")}>
                        配信条件を含まない
                    </Menu.Item>
                    <Menu.Item
                        key="all"
                        onClick={() => onOpenDownloadModal("all")}>
                        配信条件を含む
                    </Menu.Item>
                </Menu.SubMenu>
                <Menu.Item key="uploadCsvContact" icon={<FormOutlined />}>
                    {csvUploadAuthorized ? (
                        <Link to={Paths.contactsCsvUpload}>
                            CSVアップロード
                        </Link>
                    ) : (
                        <Tooltip title={"特定の権限で操作できます"}>
                            CSVアップロード
                        </Tooltip>
                    )}
                </Menu.Item>
            </Menu>
        );
    };

    const renderCsvControlButton = () => {
        const button = (
            <Button
                type="primary"
                size="small"
                disabled={!csvDownloadAuthorized}>
                CSV操作
            </Button>
        );
        if (csvDownloadAuthorized) {
            return (
                <Dropdown overlay={renderMenu()} placement="bottom">
                    {button}
                </Dropdown>
            );
        } else {
            return (
                <Tooltip title={"特定の権限で操作できます"}>{button}</Tooltip>
            );
        }
    };

    const renderCsvDownloadRangeModal = () => {
        return (
            <CsvDownloadModal
                totalCount={totalCount}
                isModalOpen={isCsvDownloadModalOpen}
                onModalClose={onCloseDownloadModal}
                onDownload={onDownload}
            />
        );
    };

    return (
        <>
            {renderCsvControlButton()}
            {renderCsvDownloadRangeModal()}
        </>
    );
};

export default ContactCsvDownloadButton;
