import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import ContactSearchPageContainer from "../ContactSearchPage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { contactSearchPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { defaultInitialState as EditPageInitialState } from "~/reducers/Factories/editPage";
import { displaySettingPage } from "~/reducers/pages";
import { generateRandomToken } from "~/utils/utils";
import { contactListContent } from "~/test/mock/contactAPIMock";

describe("ContactSearchPage.tsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<ContactSearchPageContainer />, {
            store: configureStore({
                reducer: {
                    login,
                    displaySettingPage,
                    contactSearchPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            contacts: {
                                create: true,
                                update: true,
                                delete: true,
                                column_setting: true,
                                csv_upload: true,
                                csv_download: true,
                                search_template: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        ...SearchPageInitialState,
                        data: [...contactListContent],
                    },
                    displaySettingPage: {
                        ...EditPageInitialState,
                        isDisplaySettingLoading: false,
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText(/取引先担当者 一覧/);
        expect(pageTitleElement).toBeInTheDocument();
        const csvControlButtonElement = screen.getByText(/CSV操作/);
        expect(csvControlButtonElement).toBeInTheDocument();
        const paginationLabelElement = await screen.findByText(
            /合計2件中, 1-2を表示/
        );
        expect(paginationLabelElement).toBeInTheDocument();
        // その他の要素の簡易確認
        const filterButtonElement = screen.getByText(/絞り込み検索/);
        expect(filterButtonElement).toBeInTheDocument();
        const contactTableContactNameDataElement = await screen.findByText(
            new RegExp(contactListContent[0].display_name)
        );
        expect(contactTableContactNameDataElement).toBeInTheDocument();
    });
});
