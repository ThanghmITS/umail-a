import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Col, Row, Spin, Typography } from "antd";
import { DASHBOARD_STATS } from "~/components/Pages/pageIds";
import { Endpoint } from "~/domain/api";
import { convertDashStatsDataEntry } from "~/domain/data";
import { fetchAction, clearAction, fetchApi } from "~/actions/data";
import { LoadingOutlined } from "@ant-design/icons";
import {
    AUTHORIZED_ACTION_LOADING,
    AUTHORIZED_ACTION_LOADED,
} from "~/actions/actionTypes";
import { RootState } from "~/models/store";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import ContactStatsGraph from "./ContactStatsGraph/ContactStatsGraph";
import OrganizationStatsGraph from "./OrganizationStatsGraph/OrganizationStatsGraph";
import ScheduledEmailStatsGraph from "./ScheduledEmailStatsGraph/ScheduledEmailStatsGraph";
import getDateStr from "~/domain/date";
import { useGuardPlanSummary } from "~/hooks/usePlan";
import styles from "./dashboardPage.scss";
import StaffMyCompanyGraph from "./StaffMyCompanyGraph/StaffMyCompanyGraph";

const { Text } = Typography;

const defaultGutter = 24;
const defaultSpan = 12;

const loadingIcon = <LoadingOutlined style={{ fontSize: 24 }} />;
const LoadingScreen = <Spin indicator={loadingIcon} />;

type Props = {};

const DashboardPage = ({}: Props) => {
    const dispatch = useDispatch();
    const token = useSelector((state: RootState) => state.login.token);
    const dashboardStats = useSelector(
        (state: RootState) => state.dashboardStatsContainer
    );
    const [errorMessage, setErrorMessage] = useState<any>(undefined);
    useGuardPlanSummary();

    const fetchDashboardStatistics = () => {
        dispatch(
            fetchAction(
                DASHBOARD_STATS,
                token,
                `${Endpoint.getBaseUrl()}/${Endpoint.dashboardStatistics}`,
                undefined,
                convertDashStatsDataEntry
            )
        );
    };

    const renderUpdateMessage = () => {
        // TODO(joshua-hashimoto): 更新日と次回更新日を実際のデータに置き換える
        return (
            <Col key="updateMessage">
                <Row key="diplayTime">
                    <Text>
                        現在表示されているのは、
                        {getDateStr(
                            dashboardStats.data?.dashboardTime?.calculate_time
                        )}
                        時点のデータです。
                    </Text>
                </Row>
                <Row key="updateTime">
                    <Text>
                        次回は、翌月1~5日に更新予定です。
                    </Text>
                </Row>
            </Col>
        );
    };

    useEffect(() => {
        dispatch(clearAction(DASHBOARD_STATS));
        fetchDashboardStatistics();
    }, []);

    useEffect(() => {
        setErrorMessage(dashboardStats.errorMessage);
    }, [dashboardStats]);

    return (
        <div>
            {errorMessage ? (
                <Alert type="error" message={errorMessage} />
            ) : (
                <span />
            )}
            <CustomPageHeader
                title="ダッシュボード"
                extra={[renderUpdateMessage()]}
            />
            <Row className={styles.row} gutter={defaultGutter}>
                <Col span={defaultSpan} key="organizationStats">
                    {dashboardStats.data &&
                    dashboardStats.data.organizationStats ? (
                        <OrganizationStatsGraph
                            data={dashboardStats.data.organizationStats}
                        />
                    ) : (
                        LoadingScreen
                    )}
                </Col>
                <Col span={defaultSpan} key="contactStats">
                    {dashboardStats.data && dashboardStats.data.contactStats ? (
                        <ContactStatsGraph
                            data={dashboardStats.data.contactStats}
                        />
                    ) : (
                        LoadingScreen
                    )}
                </Col>
            </Row>
            <Row className={styles.row} gutter={defaultGutter}>
                <Col span={defaultSpan} key="scheduledEmailStats">
                    {dashboardStats.data &&
                    dashboardStats.data.scheduledEmailStats ? (
                        <ScheduledEmailStatsGraph
                            data={dashboardStats.data.scheduledEmailStats}
                        />
                    ) : (
                        LoadingScreen
                    )}
                </Col>
                <Col span={defaultSpan} key="topUsersStats">
                    {dashboardStats.data && dashboardStats.data.topUsersStats ? (
                        <StaffMyCompanyGraph
                            data={dashboardStats.data.topUsersStats}
                        />
                    ) : (
                        LoadingScreen
                    )}
                </Col>
            </Row>
        </div>
    );
};

export default DashboardPage;
