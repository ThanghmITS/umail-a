import React from "react";
import { ChartDataSets, ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { ContactStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphCard from "../GraphCard/GraphCard";
import styles from "./ContactStatsGraph.scss";

type Props = {
    data: GraphDataModel<ContactStatsGraphModel>;
};

const ContactStatsGraph = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "contact_stats",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getChartData = (
        data: GraphDataModel<ContactStatsGraphModel>
    ): { labels: string[]; datasets: ChartData<any>[] } => {
        return {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "案件開発",
                    backgroundColor: "rgba(212,56,12,0.4)",
                    borderColor: "rgba(212,56,12,1)",
                    pointBorderColor: "rgba(212,56,12,1)",
                    pointHoverBackgroundColor: "rgba(212,56,12,1)",
                    data: data.values.jobs.development,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "案件インフラ",
                    backgroundColor: "rgba(212,108,11,0.4)",
                    borderColor: "rgba(212,108,11,1)",
                    pointBorderColor: "rgba(212,108,11,1)",
                    pointHoverBackgroundColor: "rgba(212,108,11,1)",
                    data: data.values.jobs.infrastructure,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "案件その他",
                    backgroundColor: "rgba(212,130,12,0.4)",
                    borderColor: "rgba(212,130,12,1)",
                    pointBorderColor: "rgba(212,130,12,1)",
                    pointHoverBackgroundColor: "rgba(212,130,12,1)",
                    data: data.values.jobs.others,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "要員開発",
                    backgroundColor: "rgba(30,56,195,0.4)",
                    borderColor: "rgba(30,56,195,1)",
                    pointBorderColor: "rgba(30,56,195,1)",
                    pointHoverBackgroundColor: "rgba(30,56,195,1)",
                    data: data.values.personnel.development,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "要員インフラ",
                    backgroundColor: "rgba(123,141,236,0.4)",
                    borderColor: "rgba(123,141,236,1)",
                    pointBorderColor: "rgba(123,141,236,1)",
                    pointHoverBackgroundColor: "rgba(123,141,236,1)",
                    data: data.values.personnel.infrastructure,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "要員その他",
                    backgroundColor: "rgba(126,206,238,0.4)",
                    borderColor: "rgba(126,206,238,1)",
                    pointBorderColor: "rgba(126,206,238,1)",
                    pointHoverBackgroundColor: "rgba(126,206,238,1)",
                    data: data.values.personnel.others,
                    yAxisID: "contact_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "ご案内",
                    backgroundColor: "rgba(12,212,100,0.4)",
                    borderColor: "rgba(12,212,100,1)",
                    pointBorderColor: "rgba(12,212,100,1)",
                    pointHoverBackgroundColor: "rgba(12,212,100,1)",
                    data: data.values.announcement,
                    yAxisID: "contact_stats",
                },
            ],
        };
    };

    return (
        <GraphCard
            title="配信可能な種別詳細（累計）"
            subTitle="取引先担当者一覧"
            data={getChartData(data)}
            options={options}
        />
    );
};

export default ContactStatsGraph;
