import React, { useState } from "react";
import { Button, Card, Col, Row } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { HorizontalBar, ChartComponentProps, Line } from "react-chartjs-2";
type Props = ChartComponentProps & {
    title: string;
    subTitle?: string;
    extra?: any;
};

const GraphHorizontalBar = ({ title, subTitle, extra, ...props }: Props) => {
    return (
        <Row justify="center" style={{ marginBottom: 20 }}>
            <Col span={24}>
                <Card>
                    <CustomPageHeader
                        title={title}
                        subTitle={subTitle}
                        extra={extra}
                    />
                    <Col span={24}>
                        <Row justify="center" align="middle">
                            <Col flex="auto">
                                <HorizontalBar {...props} />
                            </Col>
                        </Row>
                    </Col>
                </Card>
            </Col>
        </Row>
    );
};

export default GraphHorizontalBar;