import React from "react";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { ScheduledEmailStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphCard from "../GraphCard/GraphCard";
import styles from "./ScheduledEmailStatsGraph.scss";

type Props = {
    data: GraphDataModel<ScheduledEmailStatsGraphModel>;
};

const ScheduledEmailStatsGraph = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "scheduled_email_stats",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getChartData = (
        data: GraphDataModel<ScheduledEmailStatsGraphModel>
    ): ChartData<any> => {
        return {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "案件",
                    backgroundColor: "rgba(212,56,12,0.4)",
                    borderColor: "rgba(212,56,12,1)",
                    pointBorderColor: "rgba(212,56,12,1)",
                    pointHoverBackgroundColor: "rgba(212,56,12,1)",
                    data: data.values.jobs,
                    yAxisID: "scheduled_email_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "要員",
                    backgroundColor: "rgba(30,56,195,0.4)",
                    borderColor: "rgba(30,56,195,1)",
                    pointBorderColor: "rgba(30,56,195,1)",
                    pointHoverBackgroundColor: "rgba(30,56,195,1)",
                    data: data.values.personnel,
                    yAxisID: "scheduled_email_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "ご案内",
                    backgroundColor: "rgba(12,212,100,0.4)",
                    borderColor: "rgba(12,212,100,1)",
                    pointBorderColor: "rgba(12,212,100,1)",
                    pointHoverBackgroundColor: "rgba(12,212,100,1)",
                    data: data.values.announcement,
                    yAxisID: "scheduled_email_stats",
                },
            ],
        };
    };

    return (
        <GraphCard
            title="配信済の種別（月別）"
            subTitle="配信メール一覧"
            data={getChartData(data)}
            options={options}
        />
    );
};

export default ScheduledEmailStatsGraph;
