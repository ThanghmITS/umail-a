import React from "react";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { ContactStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphCard from "../GraphCard/GraphCard";
import styles from "./SharedEmailStatsGraph.scss";

type Props = {
    data: GraphDataModel;
};

const SharedEmailStatsGraph = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "shared_email_stats",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getChartData = (data: GraphDataModel): ChartData<any> => {
        return {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "自社担当者",
                    backgroundColor: "rgba(212,56,12,0.4)",
                    borderColor: "rgba(212,56,12,1)",
                    pointBorderColor: "rgba(212,56,12,1)",
                    pointHoverBackgroundColor: "rgba(212,56,12,1)",
                    data: data.values,
                    yAxisID: "shared_email_stats",
                },
            ],
        };
    };

    return (
        <GraphCard
            title="自社担当者（件数）"
            subTitle="共有メールBOX一覧"
            data={getChartData(data)}
            options={options}
        />
    );
};

export default SharedEmailStatsGraph;
