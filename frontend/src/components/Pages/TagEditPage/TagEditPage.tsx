import React from "react";
import { Button, Col, Row, Spin, Tooltip } from "antd";
import { useHistory, useParams } from "react-router-dom";
import CustomDetailPageContent from "~/components/Common/CustomDetailPageContent/CustomDetailPageContent";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import TagForm from "~/components/Forms/TagForm/TagForm";
import Paths from "~/components/Routes/Paths";
import {
    useDeleteTagAPIMutation,
    useFetchTagAPIQuery,
    useUpdateTagAPIMutation,
} from "~/hooks/useTag";
import { TagFormModel } from "~/models/tagModel";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { showDeleteModal } from "~/components/Feedbacks/Modal/Modal";
import { ErrorMessages } from "~/utils/constants";
import AboutThisData from "~/components/Common/AboutThisData/AboutThisData";
import NotFoundPage from "../NotFoundPage";
import styles from "./TagEditPage.scss";

const TagEditPage = () => {
    const { id } = useParams<{ id: string }>();
    const router = useHistory();
    const { _all: tagAuthorized } = useAuthorizedActions("tags");
    const { mutate: updateTag } = useUpdateTagAPIMutation();
    const { mutate: deleteTag } = useDeleteTagAPIMutation();
    const { data, isLoading } = useFetchTagAPIQuery({ tagId: id });

    const onSubmit = (values: TagFormModel) => {
        updateTag(
            { id, postData: values },
            {
                onSuccess: (response) => {
                    router.push(Paths.tags);
                },
            }
        );
    };

    const renderDeleteButton = () => {
        const button = (
            <Button
                htmlType="button"
                type="primary"
                danger
                disabled={!tagAuthorized}
                onClick={showDeleteModal(
                    () =>
                        deleteTag(id, {
                            onSuccess: (response) => {
                                router.push(Paths.tags);
                            },
                        }),
                    "tagEditPage"
                )}>
                削除
            </Button>
        );
        if (!tagAuthorized) {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
        return button;
    };

    if (!tagAuthorized) {
        return <NotFoundPage />;
    }

    return (
        <Spin spinning={isLoading}>
            <Col span={24}>
                <Row>
                    <Col span={24}>
                        <CustomPageHeader
                            title="タグ 編集"
                            extra={[
                                <AboutThisData
                                    key="tagEditPage-aboutThisData"
                                    data={{
                                        created_user: data?.created_user,
                                        modified_time: data?.modified_time,
                                        modified_user: data?.modified_user,
                                    }}
                                />,
                            ]}
                            style={{
                                padding: "16px 0px 0px 0px",
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <CustomDetailPageContent>
                            <TagForm
                                initialData={data}
                                onSubmit={onSubmit}
                                deleteButton={renderDeleteButton()}
                            />
                        </CustomDetailPageContent>
                    </Col>
                </Row>
            </Col>
        </Spin>
    );
};

export default TagEditPage;
