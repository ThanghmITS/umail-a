import { PlusOutlined } from "@ant-design/icons";
import { Button, Col, Row } from "antd";
import React from "react";
import BoardChecklist from "~/components/Common/BoardCommon/BoardChecklist/BoardChecklist";
import { BoardChecklistModel } from "~/models/boardCommonModel";
import styles from "./PersonnelBoardChecklist.scss";

type Props = {
    checklists: BoardChecklistModel[];
};

const PersonnelBoardChecklist = ({ checklists }: Props) => {
    return (
        <Col span={24}>
            <Row>
                {checklists.map((checklist) => (
                    <BoardChecklist
                        checklist={checklist}
                        onChecklistSubmit={() => {}}
                        onChecklistItemSubmit={() => {}}
                    />
                ))}
            </Row>
            <Row justify="end">
                <Col>
                    <Button type="primary" icon={<PlusOutlined />} />
                </Col>
            </Row>
        </Col>
    );
};

export default PersonnelBoardChecklist;
