import React from "react";
import { v4 as uuidv4 } from "uuid";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import PersonnelBoardCreatePopover from "../PersonnelBoardCreatePopover";

const mockOnCreate = jest.fn();
const mockOnClose = jest.fn();
const listId = uuidv4();

describe("PersonnelBoardCreatePopover.tsx", () => {
    const closeButtonTestId = "close-button";

    test("render test", () => {
        render(
            <PersonnelBoardCreatePopover
                list_id={listId}
                onCreate={mockOnCreate}
                onClose={mockOnClose}
            />
        );
        const titleElement = screen.getByText(/要員を作成/);
        expect(titleElement).toBeInTheDocument();
        const lastNameInputElement =
            screen.getByPlaceholderText("要員イニシャル(姓)");
        expect(lastNameInputElement).toBeInTheDocument();
        const firstNameInputElement =
            screen.getByPlaceholderText("要員イニシャル(名)");
        expect(firstNameInputElement).toBeInTheDocument();
        const createButtonElement = screen.getByRole("button", {
            name: /作 成/,
        });
        expect(createButtonElement).toBeInTheDocument();
    });

    describe("actions", () => {
        test("can call close button", async () => {
            render(
                <PersonnelBoardCreatePopover
                    list_id={listId}
                    onCreate={mockOnCreate}
                    onClose={mockOnClose}
                />
            );
            const closeButtonElement = screen.getByTestId(closeButtonTestId);
            expect(closeButtonElement).toBeInTheDocument();
            await userEvent.click(closeButtonElement);
            expect(mockOnClose).toHaveBeenCalled();
        });

        test("last name input", async () => {
            render(
                <PersonnelBoardCreatePopover
                    list_id={listId}
                    onCreate={mockOnCreate}
                    onClose={mockOnClose}
                />
            );
            const inputElement = screen.getByPlaceholderText(
                "要員イニシャル(姓)"
            ) as HTMLInputElement;
            await userEvent.type(inputElement, "example");
            expect(inputElement.value).toBe("example");
        });

        test("first name input", async () => {
            render(
                <PersonnelBoardCreatePopover
                    list_id={listId}
                    onCreate={mockOnCreate}
                    onClose={mockOnClose}
                />
            );
            const inputElement = screen.getByPlaceholderText(
                "要員イニシャル(名)"
            ) as HTMLInputElement;
            await userEvent.type(inputElement, "example");
            expect(inputElement.value).toBe("example");
        });

        test("can call create button", async () => {
            render(
                <PersonnelBoardCreatePopover
                    list_id={listId}
                    onCreate={mockOnCreate}
                    onClose={mockOnClose}
                />
            );
            const lastNameInputElement = screen.getByPlaceholderText(
                "要員イニシャル(姓)"
            ) as HTMLInputElement;
            await userEvent.type(lastNameInputElement, "D");
            const firstNameInputElement = screen.getByPlaceholderText(
                "要員イニシャル(名)"
            ) as HTMLInputElement;
            await userEvent.type(firstNameInputElement, "J");
            const createButtonElement = screen.getByRole("button", {
                name: /作 成/,
            });
            await userEvent.click(createButtonElement);
            expect(mockOnCreate).toHaveBeenCalled();
        });
    });
});
