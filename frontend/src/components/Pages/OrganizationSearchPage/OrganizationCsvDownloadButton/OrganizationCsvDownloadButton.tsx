import React, { useState } from "react";
import { Button, Dropdown, Menu, Tooltip } from "antd";
import { useOrganizationCsvDownloadAPIMutation } from "~/hooks/useDownloadCsvAPI";
import { Link } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { DownloadOutlined, FormOutlined } from "@ant-design/icons";
import { useReduxState } from "~/hooks/useReduxState";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import CsvDownloadModal from "~/components/Common/CsvDownloadModal/CsvDownloadModal";

const OrganizationCsvDownloadButton = (params: any) => {
    const [isCsvDownloadModalOpen, setIsCsvDownloadModalOpen] = useState(false);
    const {
        csv_upload: csvUploadAuthorized,
        csv_download: csvDownloadAuthorized,
    } = useAuthorizedActions("organizations");
    const { totalCount } = useReduxState("organizationSearchPage");
    const { mutate: downloadCsv } = useOrganizationCsvDownloadAPIMutation();

    const onOpenDownloadModal = () => {
        setIsCsvDownloadModalOpen(true);
    };

    const onCloseDownloadModal = () => {
        setIsCsvDownloadModalOpen(false);
    };

    const onDownload = (downloadIndex: number) => {
        const queryString = params.queryString;
        downloadCsv(
            { index: downloadIndex, queryString },
            {
                onSuccess: () => {
                    onCloseDownloadModal();
                },
            }
        );
    };

    const renderUploadButton = () => {
        if (csvUploadAuthorized) {
            return (
                <Link to={Paths.organizationsCsvUpload}>CSVアップロード</Link>
            );
        } else {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    CSVアップロード
                </Tooltip>
            );
        }
    };

    const renderMenu = () => (
        <Menu>
            <Menu.Item
                onClick={onOpenDownloadModal}
                icon={<DownloadOutlined />}
                key="csvDownload"
                disabled={!totalCount}>
                {!totalCount ? (
                    <Tooltip title={ErrorMessages.csv.nothingToDownload}>
                        CSVダウンロード
                    </Tooltip>
                ) : (
                    <>CSVダウンロード</>
                )}
            </Menu.Item>
            <Menu.Item key="uploadCsvOrg" icon={<FormOutlined />}>
                {renderUploadButton()}
            </Menu.Item>
        </Menu>
    );

    const renderCsvControlButton = () => {
        const button = (
            <Button
                type="primary"
                size="small"
                disabled={!csvDownloadAuthorized}>
                CSV操作
            </Button>
        );
        if (csvDownloadAuthorized) {
            return <Dropdown overlay={renderMenu()}>{button}</Dropdown>;
        } else {
            return (
                <Tooltip title={ErrorMessages.isNotAuthorized}>
                    {button}
                </Tooltip>
            );
        }
    };

    const renderCsvDownloadRangeModal = () => {
        return (
            <CsvDownloadModal
                totalCount={totalCount}
                isModalOpen={isCsvDownloadModalOpen}
                onModalClose={onCloseDownloadModal}
                onDownload={onDownload}
            />
        );
    };

    return (
        <>
            {renderCsvControlButton()}
            {renderCsvDownloadRangeModal()}
        </>
    );
};

export default OrganizationCsvDownloadButton;
