import React, { CSSProperties, ReactNode, useState } from "react";
import { Button, Col, Divider, Row, Tooltip, Typography } from "antd";
import { PlanCardModel } from "~/models/planModel";
import { DISABLED_COLOR, ErrorMessages, primaryColor } from "~/utils/constants";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import {
    useFetchUserPlanInfoAPIQuery,
    usePlanRegisterPlanMutation,
} from "~/hooks/usePlan";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import UpgradePlanModal from "./UpgradePlanModal/UpgradePlanModal";
import DowngradePlanModal from "./DowngradePlanModal/DowngradePlanModal";
import styles from "./PlanSummaryCard.scss";

const { Text, Paragraph } = Typography;

const planPurchaseButtonStyle: CSSProperties = {
    height: "32px",
    width: "100%",
    margin: "5% 0 10%",
};

type Props = {
    plan: PlanCardModel;
};

const PlanSummaryCard = ({ plan }: Props) => {
    const isLightPlan = plan.planId === 1;
    const [visibleUpgrade, setVisibleUpgrade] = useState<boolean>(false);
    const [visibleDowngrade, setVisibleDowngrade] = useState<boolean>(false);
    const { purchase: purchaseAuthorization } = useAuthorizedActions("plan");
    const { mutate: registerPlan, isLoading: isRegisterLoading } =
        usePlanRegisterPlanMutation();
    const { data, isLoading } = useFetchUserPlanInfoAPIQuery({});
    const currentPlanMasterId = data?.planMasterId ?? 0;

    const onUpgrade = () => {
        setVisibleUpgrade(true);
    };

    const onDowngrade = () => {
        setVisibleDowngrade(true);
    };

    const onRegister = () => {
        confirmModal({
            title: `「${plan.planTitle}プラン」を購入しますか？`,
            okText: "購入",
            onOk: () => {
                registerPlan({
                    plan_master_id: plan.planId,
                });
            },
        });
    };

    const renderUpgradePlanModal = () => {
        return (
            <UpgradePlanModal
                visible={visibleUpgrade}
                setVisible={setVisibleUpgrade}
                plan={plan}
                handleUpgrade={() =>
                    registerPlan({ plan_master_id: plan.planId })
                }
            />
        );
    };

    const renderDowngradePlanModal = () => {
        return (
            <DowngradePlanModal
                visible={visibleDowngrade}
                setVisible={setVisibleDowngrade}
                plan={plan}
                handleDowngrade={() =>
                    registerPlan({ plan_master_id: plan.planId })
                }
            />
        );
    };

    const renderPlanButton = (): ReactNode => {
        const isPlanPurchased = currentPlanMasterId === plan.planId;
        const isLightPlan = plan.planId === 1;
        const isLightPlanStyle = isLightPlan
            ? planPurchaseButtonStyle
            : {
                  ...planPurchaseButtonStyle,
                  background: DISABLED_COLOR,
                  borderColor: DISABLED_COLOR,
                  color: "#ffffff",
              };
        if (isPlanPurchased) {
            return (
                <Button
                    onClick={() => {}}
                    disabled
                    style={isLightPlanStyle}
                    type="primary">
                    ご利用中
                </Button>
            );
        }
        const isPlanDowngrade = currentPlanMasterId > plan.planId;
        if (isPlanDowngrade) {
            return (
                <Button
                    onClick={onDowngrade}
                    style={isLightPlanStyle}
                    type="primary">
                    ダウングレード
                </Button>
            );
        }

        const notAbleToPurchase = !purchaseAuthorization;

        const purchaseButton = (
            <Button
                onClick={onRegister}
                type="primary"
                disabled={
                    notAbleToPurchase || isRegisterLoading || !isLightPlan
                }
                style={isLightPlanStyle}>
                {isLightPlan ? "このプランを購入" : "Coming Soon..."}
            </Button>
        );

        if (notAbleToPurchase) {
            return (
                <Tooltip
                    title={ErrorMessages.isNotAuthorized}
                    style={{ width: "100%" }}>
                    {purchaseButton}
                </Tooltip>
            );
        }
        return purchaseButton;
    };

    if (isLoading) {
        return <span data-testid="loading"></span>;
    }

    return (
        <Col
            span={22}
            style={{
                padding: 13,
                borderRadius: 8,
                background:
                    plan.planId === currentPlanMasterId
                        ? primaryColor
                        : "#edf0fa",
            }}>
            <Row justify="center">
                <Col
                    span={24}
                    style={{ background: "#ffffff", padding: "16px 48px" }}>
                    <Row justify="center">
                        <Text
                            style={{
                                fontSize: 20,
                                color: !isLightPlan
                                    ? DISABLED_COLOR
                                    : undefined,
                            }}>
                            {plan.planTitle}
                        </Text>
                    </Row>
                    <Row
                        justify="center"
                        align="bottom"
                        style={{
                            margin: "8% 0",
                        }}>
                        <Text
                            style={{
                                fontSize: 36,
                                lineHeight: 1,
                                color: !isLightPlan
                                    ? DISABLED_COLOR
                                    : undefined,
                                fontFamily: "Avenir",
                                fontStyle: "normal",
                                fontWeight: "500",
                            }}>
                            {plan.monthlyPrice.toLocaleString()}
                        </Text>
                        <Text
                            style={{
                                fontSize: 14,
                                color: !isLightPlan
                                    ? DISABLED_COLOR
                                    : undefined,
                                marginLeft: 1,
                                fontFamily: "Avenir",
                                fontStyle: "normal",
                                fontWeight: "500",
                            }}>
                            円(税抜)/月
                        </Text>
                    </Row>
                    <Row justify="center">
                        {plan.planSummary.map((summary, index) => (
                            <Paragraph
                                key={index}
                                disabled
                                style={{
                                    fontSize: 12,
                                    width: "100%",
                                    marginBottom: 1,
                                }}>
                                {summary}
                            </Paragraph>
                        ))}
                    </Row>
                    <Row justify="center">
                        <Col span={24}>
                            <Row justify="center">
                                <Col span={20}>{renderPlanButton()}</Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Divider />
                        </Col>
                    </Row>
                    <Row justify="start">
                        <Col span={18}>
                            <Row>
                                <Text
                                    disabled
                                    style={{
                                        fontSize: 10,
                                        color: !isLightPlan
                                            ? DISABLED_COLOR
                                            : undefined,
                                    }}>
                                    このような方に
                                </Text>
                            </Row>
                            <Row justify="center">
                                <Col span={24}>
                                    <Paragraph
                                        style={{
                                            color: !isLightPlan
                                                ? DISABLED_COLOR
                                                : undefined,
                                        }}>
                                        <ul>
                                            {plan.aimedTo.map((text, index) => (
                                                <li
                                                    key={index}
                                                    style={{
                                                        width: "100%",
                                                        margin: "0 0 0 42px",
                                                        listStyleType: "disc",
                                                        textAlign: "left",
                                                    }}>
                                                    {text}
                                                </li>
                                            ))}
                                        </ul>
                                    </Paragraph>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Divider />
                        </Col>
                    </Row>
                    <Row justify="start">
                        <Col span={18} style={{ height: 200 }}>
                            <Row>
                                <Text
                                    disabled
                                    style={{
                                        fontSize: 10,
                                        color: !isLightPlan
                                            ? DISABLED_COLOR
                                            : undefined,
                                    }}>
                                    機能
                                </Text>
                            </Row>
                            <Row justify="center">
                                <Col span={24}>
                                    <Paragraph
                                        style={{
                                            color: !isLightPlan
                                                ? DISABLED_COLOR
                                                : undefined,
                                        }}>
                                        <ul>
                                            {plan.functions.map(
                                                (text, index) => (
                                                    <li
                                                        key={index}
                                                        style={{
                                                            width: "100%",
                                                            margin: "0 0 0 42px",
                                                            listStyleType:
                                                                "disc",
                                                            textAlign: "left",
                                                        }}>
                                                        {text}
                                                    </li>
                                                )
                                            )}
                                        </ul>
                                    </Paragraph>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Col>
    );
};

export default PlanSummaryCard;
