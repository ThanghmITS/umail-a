import React from "react";
import { renderHook, renderWithAllProviders, screen } from "~/test/utils";
import PlanPage from "../PlanPage";
import login, { LoginInitialState } from "~/reducers/login";
import { configureStore } from "@reduxjs/toolkit";
import { generateRandomToken } from "~/utils/utils";
import { useClient } from "~/hooks/useClient";
import { mockServer } from "~/test/setupTests";
import { mockPlanFetchLightPlanUserInfoSuccessAPIRoute } from "~/test/mock/planAPIMock";

describe("PlanPage", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        // NOTE(joshua-hashimoto): ライトプランの情報をmock APIから返却するようにする
        mockServer.use(mockPlanFetchLightPlanUserInfoSuccessAPIRoute);
        renderWithAllProviders(<PlanPage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            plan: {
                                _all: true,
                                purchase: true,
                                user_add: true,
                                user_delete: true,
                            },
                        },
                    },
                },
            }),
        });
        // await waitForElementToBeRemoved(() => screen.getByRole("div"));
        const titleElement = await screen.findByText("ご利用プラン");
        expect(titleElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): 細かい確認は各コンポーネント側でやっているので、各コンポーネントの要素を選んでテストする
        const currentPlanTitleElement = await screen.findByText(
            "現在のご利用プラン"
        );
        expect(currentPlanTitleElement).toBeInTheDocument();
        // const purchaseButtons = await screen.findAllByRole("button", {
        //     name: "このプランを購入",
        // });
        // expect(purchaseButtons.length).toBe(3);
        const functionComparisonTitleElement = await screen.findByText(
            "機能比較"
        );
        expect(functionComparisonTitleElement).toBeInTheDocument();
    });
});
