import React from "react";
import { Col, Row, Typography } from "antd";
import styles from "./PlanCurrentPlanInfo.scss";

const { Text } = Typography;

type Props = {
    planTitle: string;
};

const PlanCurrentPlanInfo = ({ planTitle }: Props) => {
    return (
        <Col>
            <Row justify="center">
                <Text type="secondary" className={styles.sectionTitle}>
                    現在のご利用プラン
                </Text>
            </Row>
            <Row justify="center">
                <Text className={styles.sectionContent}>{planTitle}</Text>
            </Row>
        </Col>
    );
};

export default PlanCurrentPlanInfo;
