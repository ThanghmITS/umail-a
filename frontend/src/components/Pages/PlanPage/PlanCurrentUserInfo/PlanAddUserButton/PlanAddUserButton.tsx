import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import { usePlanAddAccountLimitAPIMutation } from "~/hooks/usePlan";
import { ErrorMessages } from "~/utils/constants";
import styles from "./PlanAddUserButton.scss";

type Props = {
    extraDisabled?: boolean;
};

const PlanAddUserButton = ({ extraDisabled = false }: Props) => {
    const { user_add: planUserAddAuthorization } = useAuthorizedActions("plan");
    const { mutate: addAccountLimit, isLoading: isAddAccountLimitLoading } =
        usePlanAddAccountLimitAPIMutation();

    const onTryAdd = () => {
        confirmModal({
            title: "ユーザーを追加しますか？",
            content: "ユーザーの上限数を+1いたします。追加料金がかかります。",
            okText: "ユーザーを追加",
            onOk: () => {
                addAccountLimit();
            },
        });
    };

    const renderButton = () => {
        return (
            <Button
                onClick={onTryAdd}
                disabled={
                    isAddAccountLimitLoading ||
                    !planUserAddAuthorization ||
                    extraDisabled
                }
                size="small"
                type="primary">
                上限を追加
            </Button>
        );
    };

    if (!planUserAddAuthorization) {
        return (
            <Tooltip title={ErrorMessages.isNotAuthorized}>
                {renderButton()}
            </Tooltip>
        );
    }

    return renderButton();
};

export default PlanAddUserButton;
