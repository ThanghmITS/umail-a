import React from "react";
import { Col, Row, Typography } from "antd";
import { useFetchUserPlanInfoAPIQuery } from "~/hooks/usePlan";
import PlanAddUserButton from "../PlanAddUserButton/PlanAddUserButton";
import PlanDeleteUserButton from "../PlanDeleteUserButton/PlanDeleteUserButton";
import styles from "./PlanCurrentUserCountInfo.scss";
import TeamIcon from "~/components/Common/TeamIcon/TeamIcon";

const { Text } = Typography;

const PlanCurrentUserCountInfo = () => {
    const { data, isLoading } = useFetchUserPlanInfoAPIQuery({});

    if (isLoading || !data) {
        return <span data-testid="loading"></span>;
    }

    const renderUserMutate = () => {
        if (!data.planMasterId) {
            return (
                <Text>
                    有料プランにすると、ユーザー上限を増やすことができます。
                </Text>
            );
        }

        return (
            <Col span={24}>
                <Row justify="center">
                    <Text disabled className={styles.titleHelp}>
                        ユーザー上限が足りませんか？
                    </Text>
                </Row>
                <Row justify="center">
                    <Col>
                        <Row gutter={4} justify="center">
                            <Col>
                                <PlanAddUserButton />
                            </Col>
                            <Col>
                                <PlanDeleteUserButton />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        );
    };

    return (
        <Col span={24}>
            <Row justify="center">
                <Col span={24}>
                    <Row justify="center" className={styles.titleContainer}>
                        <Col>
                            <Text
                                type="secondary"
                                className={styles.sectionTitle}>
                                利用ユーザー数
                            </Text>
                        </Col>
                    </Row>
                    <Row align="bottom" justify="center" gutter={1}>
                        <Col>
                            <TeamIcon />
                        </Col>
                        <Col>
                            <Text className={styles.content}>
                                {data.currentUserCount} /{" "}
                                {data.userRegistrationLimit}
                            </Text>
                        </Col>
                        {data.userRegistrationLimit - data.defaultUserCount >
                        0 ? (
                            <Col>
                                <Text
                                    type="secondary"
                                    className={styles.contentHelp}>
                                    内、追加購入分+
                                    {data.userRegistrationLimit -
                                        data.defaultUserCount}
                                </Text>
                            </Col>
                        ) : undefined}
                    </Row>
                    <Row className={styles.buttonsContainer}>
                        {renderUserMutate()}
                    </Row>
                </Col>
            </Row>
        </Col>
    );
};

export default PlanCurrentUserCountInfo;
