import React from "react";
import { useClient } from "~/hooks/useClient";
import {
    mockPlanFetchUserInfoSameUserCountSuccessAPIRoute,
    mockPlanFetchUserInfoWithFreeTrialSuccessAPIRoute,
    mockPlanUserInfoAPIResponseData,
} from "~/test/mock/planAPIMock";
import { mockServer } from "~/test/setupTests";
import {
    render,
    renderHook,
    renderWithAllProviders,
    renderWithQueryClient,
    screen,
    waitForElementToBeRemoved,
} from "~/test/utils";
import { generateRandomToken } from "~/utils/utils";
import PlanCurrentUserCountInfo from "../PlanCurrentUserCountInfo";
import login, { LoginInitialState } from "~/reducers/login";
import {
    AuthorizedActionsModel,
    PlanAuthorizedActionsModel,
} from "~/models/authModel";
import { configureStore } from "@reduxjs/toolkit";

const loading = async () => {
    const loadingTestId = "loading";
    await waitForElementToBeRemoved(() => screen.getByTestId(loadingTestId));
};

describe("PlanCurrentUserCountInfo.tsx", () => {
    const loadingTestId = "loading";

    const planAuthorizedActions: PlanAuthorizedActionsModel = {
        _all: true,
        purchase: true,
        user_add: true,
        user_delete: true,
    };

    const planUnauthorizedActions: PlanAuthorizedActionsModel = {
        _all: false,
        purchase: false,
        user_add: false,
        user_delete: false,
    };

    const authorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planAuthorizedActions,
    };

    const unauthorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planUnauthorizedActions,
    };

    const preloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions,
        },
    };

    const unauthorizedPreloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions: unauthorizedActions,
        },
    };

    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test with free trial plan", async () => {
        mockServer.use(mockPlanFetchUserInfoWithFreeTrialSuccessAPIRoute);
        renderWithQueryClient(<PlanCurrentUserCountInfo />);
        await loading();
        const userAddHelpTextElement = screen.getByText(
            /有料プランにすると、ユーザー上限を増やすことができます。/
        );
        expect(userAddHelpTextElement).toBeInTheDocument();
    });

    test("render test", async () => {
        renderWithAllProviders(<PlanCurrentUserCountInfo />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...preloadedState,
                },
            }),
        });
        await loading();
        const titleElement = screen.getByText(/利用ユーザー数/);
        expect(titleElement).toBeInTheDocument();
        const userCountElement = screen.getByText(/6 \/ 11/);
        expect(userCountElement).toBeInTheDocument();
        const userCountHelpText = "内、追加購入分+";
        const userCountHelpTextRegex = new RegExp(userCountHelpText);
        const userCountHelpTextElement = screen.getByText(
            userCountHelpTextRegex
        );
        expect(userCountHelpTextElement).toBeInTheDocument();
        const userMutateHelpTextElement =
            screen.getByText(/ユーザー上限が足りませんか？/);
        expect(userMutateHelpTextElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): ボタン系の簡易存在確認
        const userAddButtonElement = screen.getByText(/上限を追加/);
        expect(userAddButtonElement).toBeInTheDocument();
        const userDeleteButtonElement = screen.getByText(/上限を削除/);
        expect(userDeleteButtonElement).toBeInTheDocument();
    });

    test("same user count", async () => {
        mockServer.use(mockPlanFetchUserInfoSameUserCountSuccessAPIRoute);
        renderWithAllProviders(<PlanCurrentUserCountInfo />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...preloadedState,
                },
            }),
        });
        await loading();
        const userCountHelpText = "内、追加購入分+";
        const userCountHelpTextRegex = new RegExp(userCountHelpText);
        const userCountHelpTextElement = screen.queryByText(
            userCountHelpTextRegex
        );
        expect(userCountHelpTextElement).not.toBeInTheDocument();
    });
});
