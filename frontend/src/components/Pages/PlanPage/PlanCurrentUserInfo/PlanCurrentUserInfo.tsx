import React from "react";
import { Col, Row } from "antd";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import PlanCurrentPlanInfo from "./PlanCurrentPlanInfo/PlanCurrentPlanInfo";
import PlanCurrentUserCountInfo from "./PlanCurrentUserCountInfo/PlanCurrentUserCountInfo";
import styles from "./PlanCurrentUserInfo.scss";

type Props = {};

const PlanCurrentUserInfo = ({}: Props) => {
    const { data: planSummary } = usePlanSummaryAPIQuery({});

    if (!planSummary) {
        return null;
    }

    return (
        <Col span={24}>
            <Row justify="center" align="top">
                <Col>
                    <PlanCurrentPlanInfo planTitle={planSummary.planName} />
                </Col>
                <Col span={2}></Col>
                <Col>
                    <PlanCurrentUserCountInfo />
                </Col>
            </Row>
        </Col>
    );
};

export default PlanCurrentUserInfo;
