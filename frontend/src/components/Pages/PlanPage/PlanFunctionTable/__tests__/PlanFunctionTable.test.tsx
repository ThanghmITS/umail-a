import React from "react";
import { render, screen } from "~/test/utils";
import PlanFunctionTable from "../PlanFunctionTable";
import { planCards, planTableRows } from "../../data";

describe("PlanFunctionTable.tsx", () => {
    test("render test", () => {
        render(<PlanFunctionTable />);
        const headingElement = screen.getByText("機能比較");
        expect(headingElement).toBeInTheDocument();
        const functionNameHeadingElement = screen.getByText("機能");
        expect(functionNameHeadingElement).toBeInTheDocument();
        const lightPlanHeadingElement = screen.getByText("ライト");
        expect(lightPlanHeadingElement).toBeInTheDocument();
        const standardPlanHeadingElement = screen.getByText("スタンダード");
        expect(standardPlanHeadingElement).toBeInTheDocument();
        const professionalPlanHeadingElement =
            screen.getByText("プロフェッショナル");
        expect(professionalPlanHeadingElement).toBeInTheDocument();

        const dashboardFunctionElement = screen.getByText("ダッシュボード");
        expect(dashboardFunctionElement).toBeInTheDocument();
        const organizationFunctionElement =
            screen.getByText("取引先一覧／検索");
        expect(organizationFunctionElement).toBeInTheDocument();
        const organizationInfoFunctionElement = screen.getByText("取引先情報");
        expect(organizationInfoFunctionElement).toBeInTheDocument();
        const organizationBranchFunctionElement =
            screen.getByText("取引先支店情報");
        expect(organizationBranchFunctionElement).toBeInTheDocument();
        const organizationSearchConditionFunctionElement =
            screen.getByText("取引先取引条件");
        expect(organizationSearchConditionFunctionElement).toBeInTheDocument();
        const organizationCommentFunctionElement =
            screen.getByText("取引先コメント");
        expect(organizationCommentFunctionElement).toBeInTheDocument();
        const organizationCsvDownloadFunctionElement =
            screen.getByText("取引先CSVダウンロード");
        expect(organizationCsvDownloadFunctionElement).toBeInTheDocument();
        const organizationCsvUploadFunctionElement =
            screen.getByText("取引先CSVアップロード");
        expect(organizationCsvUploadFunctionElement).toBeInTheDocument();
        const contactFunctionElement =
            screen.getByText("取引先担当者一覧／検索");
        expect(contactFunctionElement).toBeInTheDocument();
        const contactInfoFunctionElement = screen.getByText("取引先担当者情報");
        expect(contactInfoFunctionElement).toBeInTheDocument();
        const contactScheduledEmailSearchConditionFunctionElement =
            screen.getByText("取引先担当者配信条件");
        expect(
            contactScheduledEmailSearchConditionFunctionElement
        ).toBeInTheDocument();
        const contactCommentFunctionElement =
            screen.getByText("取引先担当者コメント");
        expect(contactCommentFunctionElement).toBeInTheDocument();
        const contactCsvDownloadFunctionElement =
            screen.getByText("取引先担当者CSVダウンロード");
        expect(contactCsvDownloadFunctionElement).toBeInTheDocument();
        const contactCsvUploadFunctionElement =
            screen.getByText("取引先担当者CSVアップロード");
        expect(contactCsvUploadFunctionElement).toBeInTheDocument();
        const scheduledEmailElement = screen.getByText("配信メール一覧／検索");
        expect(scheduledEmailElement).toBeInTheDocument();
        const scheduledEmailToElement1 = screen.getByText("配信メール予約");
        expect(scheduledEmailToElement1).toBeInTheDocument();
        const scheduledEmailToElement2 = screen.getByText("(TO配信)");
        expect(scheduledEmailToElement2).toBeInTheDocument();
        const scheduledEmailSenderSearchConditionElement =
            screen.getByText("配信メール宛先検索");
        expect(scheduledEmailSenderSearchConditionElement).toBeInTheDocument();
        const scheduledEmailFormatElement =
            screen.getByText("テキスト／HTML配信");
        expect(scheduledEmailFormatElement).toBeInTheDocument();
        const scheduledEmailMaxElement1 =
            screen.getByText("配信宛先最大1000件");
        expect(scheduledEmailMaxElement1).toBeInTheDocument();
        const scheduledEmailMaxElement2 =
            screen.getByText("(アドオン最大5000件)");
        expect(scheduledEmailMaxElement2).toBeInTheDocument();
        const scheduledEmailAttachmentMaxSizeElement1 =
            screen.getByText("配信添付容量最大2MB");
        expect(scheduledEmailAttachmentMaxSizeElement1).toBeInTheDocument();
        const scheduledEmailAttachmentMaxSizeElement2 =
            screen.getByText("(アドオン最大10MB)");
        expect(scheduledEmailAttachmentMaxSizeElement2).toBeInTheDocument();
        const scheduledEmailDataSaveSizeElement1 =
            screen.getByText("配信データ保持期間最大半年");
        expect(scheduledEmailDataSaveSizeElement1).toBeInTheDocument();
        const scheduledEmailDataSaveSizeElement2 =
            screen.getByText("(アドオン上限緩和)");
        expect(scheduledEmailDataSaveSizeElement2).toBeInTheDocument();
        const searchConditionTemplateElement =
            screen.getByText("検索条件テンプレート");
        expect(searchConditionTemplateElement).toBeInTheDocument();
        const commentTemplateElement = screen.getByText("コメントテンプレート");
        expect(commentTemplateElement).toBeInTheDocument();
        const myCompanyElement = screen.getByText("自社情報");
        expect(myCompanyElement).toBeInTheDocument();
        const myCompanyConditionElement = screen.getByText("自社取引条件");
        expect(myCompanyConditionElement).toBeInTheDocument();
        const addonElement = screen.getByText("アドオン");
        expect(addonElement).toBeInTheDocument();
        const userSettingElement = screen.getByText("ユーザー設定");
        expect(userSettingElement).toBeInTheDocument();
        const requiredElement = screen.getByText("必須項目設定");
        expect(requiredElement).toBeInTheDocument();
        const tagSettingElement = screen.getByText("タグ設定");
        expect(tagSettingElement).toBeInTheDocument();
        const scheduledEmailSettingElement = screen.getByText("配信メール設定");
        expect(scheduledEmailSettingElement).toBeInTheDocument();
        const myProfileElement = screen.getByText("個人プロフィール");
        expect(myProfileElement).toBeInTheDocument();
        const creditCardElement = screen.getByText("クレジットカード決済");
        expect(creditCardElement).toBeInTheDocument();
        const purchaseHistoryElement = screen.getByText("お支払い履歴");
        expect(purchaseHistoryElement).toBeInTheDocument();
        const otherElement = screen.getByText("その他、Coming soon...");
        expect(otherElement).toBeInTheDocument();

        const circleElements = screen.getAllByText("⚪︎");
        expect(circleElements.length).toBe(
            planTableRows.length * planCards.length - 1
        );
        const dashElements = screen.getAllByText("-");
        expect(dashElements.length).toBe(1);
    });
});
