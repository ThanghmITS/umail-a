import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import MyCompanyPage from "../../MyCompanyPage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { myCompanyPage } from "~/reducers/pages";
import { MemoryRouter } from "react-router-dom";
import { generateRandomToken } from "~/utils/utils";
import { mockMyCompanyMockData } from "~/test/mock/myCompanyAPIMock";
import { defaultInitialState } from "~/reducers/Factories/editPage";

const Wrapper = () => {
    return (
        <MemoryRouter>
            <MyCompanyPage match={{ params: { id: generateRandomToken() } }} />
        </MemoryRouter>
    );
};

describe("MyCompanyPage.jsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<Wrapper />, {
            store: configureStore({
                reducer: {
                    login,
                    myCompanyPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            my_company: {
                                _all: true,
                            },
                        },
                    },
                    myCompanyPage: {
                        ...defaultInitialState,
                        data: mockMyCompanyMockData,
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText("自社プロフィール");
        expect(pageTitleElement).toBeInTheDocument();
        const modifiedTimeLabelElement = await screen.findByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = await screen.findByText(
            new RegExp(mockMyCompanyMockData.modified_time, "g")
        );
        expect(modifiedTimeElement).toBeInTheDocument();
        const modifiedUserLabelElement = screen.getByText(/更新者:/);
        expect(modifiedUserLabelElement).toBeInTheDocument();
        const modifiedUserElement = await screen.findByText(
            new RegExp(mockMyCompanyMockData.modified_user, "g")
        );
        expect(modifiedUserElement).toBeInTheDocument();
        const backButtonElement = screen.getByRole("button", {
            name: "戻る",
        });
        expect(backButtonElement).toBeInTheDocument();
        expect(backButtonElement).not.toBeDisabled();
        const updateButtonElement = screen.getByRole("button", {
            name: "更 新",
        });
        expect(updateButtonElement).toBeInTheDocument();
        expect(updateButtonElement).not.toBeDisabled();
    });
});
