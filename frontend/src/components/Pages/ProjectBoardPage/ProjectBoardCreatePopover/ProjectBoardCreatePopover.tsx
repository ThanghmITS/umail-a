import React, { useEffect } from "react";
import { Button, Col, Form, Row, Input } from "antd";
import BoardPopover from "~/components/Common/BoardCommon/BoardPopover/BoardPopover";
import styles from "./ProjectBoardCreatePopover.scss";
import { ProjectBoardNewCardFormModel } from "~/models/projectBoardModel";

type Props = {
    list_id: string;
    onCreate: (values: ProjectBoardNewCardFormModel) => void;
    onClose: () => void;
};

const ProjectBoardCreatePopover = ({ list_id, onCreate, onClose }: Props) => {
    const [form] = Form.useForm<ProjectBoardNewCardFormModel>();

    useEffect(() => {
        form.setFieldsValue({ list_id });
    }, []);

    return (
        <BoardPopover title="案件を作成" onClose={onClose}>
            <Row justify="center" style={{ width: '100%' }}>
                <Form form={form} onFinish={onCreate}>
                    <Row justify="space-between" align="middle">
                          <Form.Item
                              name="detail"
                              dependencies={['detail']}
                              rules={[
                                { required: true }
                              ]}
                          >
                              <Input placeholder="案件概要" />
                          </Form.Item>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Button
                                style={{
                                    width: "100%",
                                    marginTop: 10,
                                    marginBottom: 10,
                                }}
                                type="primary"
                                htmlType="submit">
                                作成
                            </Button>
                        </Col>
                    </Row>
                    <Form.Item name="listId" hidden>
                        <Input />
                    </Form.Item>
                </Form>
            </Row>
        </BoardPopover>
    );
};

export default ProjectBoardCreatePopover;
