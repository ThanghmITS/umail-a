import React, {useState} from "react";
import { Col, Row, Spin } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { useRecoilState } from "recoil";
import BoardComponent from "~/components/Common/BoardComponent/BoardComponent";
import UpdateProjectBoardModal from "~/components/Common/BoardCommon/UpdateProjectBoardModal/UpdateProjectBoardModal";
import BoardBaseProjectItemCard from "~/components/Common/BoardComponent/BoardBaseProjectItemCard/BoardBaseProjectItemCard";
import NewProjectBoard from "~/components/Common/BoardForms/NewProjectBoard/NewProjectBoard";
import { BoardChecklistItemUpdateFormModel, BoardChecklistUpdateFormModel } from "~/models/boardCommonModel";
import { projectBoard } from "~/recoil/atom";
import {
  ProjectBoardListCardModel,
} from "~/models/projectBoardModel";
import {
  useDetailProjectCardBoard,
  useProjectBoardCreator,
} from "~/hooks/useProjectBoard";

const ProjectBoardPage = () => {
    const { boardData } = useProjectBoardCreator();
    const [projectBoardState, setProjectBoardState] = useRecoilState(projectBoard);
    const [newProjectVisible, setNewProjectVisible] = useState(false);
    const typeCard = 'project';

    const {
      isLoading,
      dataDetail,
      dataComments,
      checkLists,
      dataContacts,
      dataOrganizations,
      projectBoardDeleteChecklist,
      projectBoardCreateChecklistItem,
      projectBoardUpdateChecklistItem,
      projectBoardUpdateChecklist,
      projectBoardCreateChecklist,
      refetchFetchOrganizations,
      refetchFetchContacts
  } = useDetailProjectCardBoard();
  
    const onDetailCard = (projectId: string) => () => {
      setProjectBoardState({
          ...projectBoardState,
          isUpdateProject: true,
          projectId
      })
    }

    const renderCard = (cardItem: ProjectBoardListCardModel) => {
      return (
        <BoardBaseProjectItemCard
          key={`boardCard--${cardItem.id}`}
          itemCard={cardItem}
          onDetailCard={onDetailCard}
        />
      );
    };
    

    const onSettings = () => {};

    const onSubmitFinishContent = (values: BoardChecklistUpdateFormModel, checkListId: string) => {
        projectBoardUpdateChecklist(values, checkListId);
    };

    const onDeleteCheckList = (idCheckList: string) => {
        projectBoardDeleteChecklist(idCheckList);
    };

    const onAddItem = (value: string, idCheckList: string) => () => {
        projectBoardCreateChecklistItem(value, idCheckList);
    };

    const onCreateCheckList = () => {
        const { projectId } = projectBoardState;
        projectBoardCreateChecklist(projectId)
    };

    const onChecklistItemSubmit = (values: BoardChecklistItemUpdateFormModel, checklistId: string) => {
        projectBoardUpdateChecklistItem(values,checklistId);
    };

    const onDropdownVisibleChangeOrganizations = (open:boolean) => {
      if (open) {
          refetchFetchOrganizations();
      }
    };

    const onDropdownVisibleChangeContacts = (open:boolean) => {
      if (open) {
          refetchFetchContacts();
      }
    };

    return (
      <Spin spinning={false}>
        <Col span={24}>
          <Row>
            <CustomPageHeader title="案件ボード" />
          </Row>
          <Row>
            <Col span={24}>
              <BoardComponent
                typeCard={typeCard}
                boardData={boardData}
                renderCard={renderCard}
                onSettings={onSettings}
              />
            </Col>
            <NewProjectBoard
              visible={newProjectVisible}
              onCancel={()=> setNewProjectVisible(false)}
            />
          </Row>
        </Col>
        <UpdateProjectBoardModal
            isLoading={isLoading}
            dataCheckLists={checkLists}
            onSubmitFinishContent={onSubmitFinishContent}
            initialData={dataDetail}
            onDeleteCheckList={onDeleteCheckList}
            dataComments={dataComments || []}
            dataContacts= {dataContacts || []}
            dataOrganizations= {dataOrganizations || []}
            onAddItem={onAddItem}
            onCreateCheckList={onCreateCheckList}
            onChecklistItemSubmit={onChecklistItemSubmit}
            onDropdownVisibleChangeOrganizations={onDropdownVisibleChangeOrganizations}
            onDropdownVisibleChangeContacts={onDropdownVisibleChangeContacts}
        />
      </Spin>
    );
};

export default ProjectBoardPage;
