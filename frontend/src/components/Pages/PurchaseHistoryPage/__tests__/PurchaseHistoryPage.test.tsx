import React from "react";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import { renderHook, renderWithQueryClient, screen } from "~/test/utils";
import PurchaseHistoryPage from "../PurchaseHistoryPage";

describe("PurchaseHistoryPage.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        renderWithQueryClient(<PurchaseHistoryPage />);
        const pageTitleElement = screen.getByText("お支払い履歴 一覧");
        expect(pageTitleElement).toBeInTheDocument();
        const tableRowElements = await screen.findAllByRole("row");
        expect(tableRowElements.length).toBe(4); // NOTE(joshua-hashimoto): テーブルのヘッダーもrowなので、データ*3 + ヘッダー = 4
        // NOTE(joshua-hashimoto): パジネーションのラベルが取得できない
        // TODO(joshua-hashimoto): パジネーションラベルが正しいことをテストする
    });
});
