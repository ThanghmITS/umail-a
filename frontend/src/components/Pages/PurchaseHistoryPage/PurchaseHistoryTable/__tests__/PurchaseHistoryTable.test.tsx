import React from "react";
import { render, screen } from "~/test/utils";
import PurchaseHistoryTable from "../PurchaseHistoryTable";
import { mockPurchaseHistoryMockData } from "~/test/mock/purchaseHistoryMock";
import moment from "moment";

describe("PurchaseHistoryTable.tsx", () => {
    test("render test with loading=false", () => {
        render(
            <PurchaseHistoryTable
                data={mockPurchaseHistoryMockData}
                loading={false}
            />
        );
        const tableElement = screen.getByRole("table");
        expect(tableElement).toBeInTheDocument();
        const periodColumnTitleElement = screen.getByText("対象期間");
        expect(periodColumnTitleElement).toBeInTheDocument();
        const registeredPlanColumnTitleElement =
            screen.getByText("ご利用プラン");
        expect(registeredPlanColumnTitleElement).toBeInTheDocument();
        const optionsColumnTitleElement = screen.getByText("オプション");
        expect(optionsColumnTitleElement).toBeInTheDocument();
        const paymentMethodColumnTitleElement =
            screen.getByText("お支払い方法");
        expect(paymentMethodColumnTitleElement).toBeInTheDocument();
        const purchaseDateColumnTitleElement = screen.getByText("決済日");
        expect(purchaseDateColumnTitleElement).toBeInTheDocument();
        const priceColumnTitleElement = screen.getByText("税込金額");
        expect(priceColumnTitleElement).toBeInTheDocument();
        const receiptIdColumnTitleElement = screen.getByText("領収書ID");
        expect(receiptIdColumnTitleElement).toBeInTheDocument();
        const receiptColumnTitleElement = screen.getByText("領収書");
        expect(receiptColumnTitleElement).toBeInTheDocument();
        const receiptDownloadButtonElements = screen.getAllByRole("button");
        expect(receiptDownloadButtonElements.length).toBe(3);
        for (const mockData of mockPurchaseHistoryMockData) {
            const creditLast4Element = screen.getByText(
                new RegExp(mockData.cardLast4)
            );
            expect(creditLast4Element).toBeInTheDocument();
            const createdTimeElement = screen.getByText(
                new RegExp(moment(mockData.createdTime).format("YYYY-MM-DD"))
            );
            expect(createdTimeElement).toBeInTheDocument();
            const idElement = screen.getByText(new RegExp(mockData.id));
            expect(idElement).toBeInTheDocument();
            const optionNameElement = screen.getByText(mockData.optionNames[0]);
            expect(optionNameElement).toBeInTheDocument();
            const periodEndElement = screen.getByText(
                new RegExp(mockData.periodEnd)
            );
            expect(periodEndElement).toBeInTheDocument();
            const periodStartElement = screen.getByText(
                new RegExp(mockData.periodStart)
            );
            expect(periodStartElement).toBeInTheDocument();
            const planNameElement = screen.getByText(
                new RegExp(mockData.planName)
            );
            expect(planNameElement).toBeInTheDocument();
            const priceElement = screen.getByText(
                new RegExp(`${mockData.price.toLocaleString()}`)
            );
            expect(priceElement).toBeInTheDocument();
        }
    });
    test("render test with loading=true", async () => {
        render(
            <PurchaseHistoryTable
                data={mockPurchaseHistoryMockData}
                loading={true}
            />
        );
        const tableElement = screen.queryByRole("table");
        expect(tableElement).not.toBeInTheDocument();
    });
});
