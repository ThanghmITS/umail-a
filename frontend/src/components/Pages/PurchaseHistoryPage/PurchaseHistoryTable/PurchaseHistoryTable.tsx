import React from "react";
import { Tag, Table, TableProps, Space, Skeleton, Button, Modal, Result, Empty } from "antd";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { BarcodeOutlined } from "@ant-design/icons";
import { ColumnsType } from "antd/lib/table";
import { PurchaseHistoryModel } from "~/models/purchaseHistory";
import { usePurchaseHistory } from "~/hooks/usePurchaseHistory";
import moment from "moment";
import { formatMoneyNumber } from "~/components/helpers";
import { Document, Page } from "react-pdf/dist/esm/entry.webpack";
import styles from "./PurchaseHistoryTable.scss";

type Props = TableProps<PurchaseHistoryModel> & {
    loading?: boolean;
    data: PurchaseHistoryModel[];
};

const PurchaseHistoryTable = ({ loading = false, data, ...props }: Props) => {
    const { generatePurchaseReceipt, downloadPurchaseReceipt } =
        usePurchaseHistory();

    const onDownloadPurchaseReceipt = async (receipt: PurchaseHistoryModel) => {
        try {
            const pdfBlob = await generatePurchaseReceipt(receipt);
            if (!pdfBlob) {
                customErrorMessage(
                    "領収書のPDF出力に失敗しました。しばらく時間を置いてから再度お試しいただくか、サポートまでお問い合わせください。"
                );
                return;
            }
            const momentFormat = "YYYY年MM月DD日";
            const filename = `コモレビ_${moment(receipt.periodStart).format(
                momentFormat
            )}_${moment(receipt.periodEnd).format(momentFormat)}_${
                receipt.id
            }_領収書.pdf`;
            const url = URL.createObjectURL(pdfBlob);
            Modal.confirm({
                title: "領収書PDFをダウンロードしますか？",
                width: "700px",
                content: (
                    <Document file={url}>
                        <Page pageNumber={1} width={600} height={340} />
                    </Document>
                ),
                okText: "ダウンロード",
                onOk: () => {
                    downloadPurchaseReceipt(pdfBlob, filename);
                },
                cancelText: "キャンセル",
                style: {
                    textAlign: "left",
                },
            });
        } catch (err) {
            console.error(err);
        }
    };

    const columns: ColumnsType<PurchaseHistoryModel> = [
        {
            key: "expiration",
            dataIndex: "expiration",
            title: "対象期間",
            width: 320,
            render: (value, record, __) =>
                moment(record.periodStart).format("YYYY-MM-DD") +
                " ~ " +
                moment(record.periodEnd).format("YYYY-MM-DD"),
        },
        {
            key: "planName",
            dataIndex: "planName",
            title: "ご利用プラン",
            width: 180,
        },
        {
            key: "optionNames",
            dataIndex: "optionNames",
            title: "オプション",
            width: 250,
            render: (optionNames: string[], _, __) => {
                const addonTags = optionNames.map((addon, index) => {
                    return (
                        <Tag key={index} style={{ marginTop: "5px" }}>
                            {addon}
                        </Tag>
                    );
                });
                return <span>{addonTags}</span>;
            },
        },
        {
            key: "cardLast4",
            dataIndex: "cardLast4",
            title: "お支払い方法",
            width: 250,
            render: (value, _, __) =>
                "クレジットカード / **** **** **** " + value,
        },
        {
            key: "createdTime",
            dataIndex: "createdTime",
            title: "決済日",
            width: 150,
            render: (value, _, __) => moment(value).format("YYYY-MM-DD"),
        },
        {
            key: "price",
            dataIndex: "price",
            title: "税込金額",
            width: 150,
            render: (value, _, __) => {
                return (
                    <p style={{ textAlign: "right" }}>
                        {formatMoneyNumber(value)} 円
                    </p>
                );
            },
            align: "left",
        },
        {
            key: "id",
            dataIndex: "id",
            title: "領収書ID",
            width: 150,
        },
        {
            key: "receipt",
            dataIndex: "receipt",
            title: "領収書",
            width: 120,
            render: (value, record, index) => {
                return (
                    <Button
                        type="primary"
                        icon={<BarcodeOutlined />}
                        onClick={() => onDownloadPurchaseReceipt(record)}
                    />
                );
            },
        },
    ];

    if (loading) {
        return (
            <Space>
                <Skeleton.Input
                    style={{ height: 200, width: 1200 }}
                    active
                    size="large"
                />
            </Space>
        );
    }

    return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            rowKey={(record) => record.id}
        />
    );
};

export default PurchaseHistoryTable;
