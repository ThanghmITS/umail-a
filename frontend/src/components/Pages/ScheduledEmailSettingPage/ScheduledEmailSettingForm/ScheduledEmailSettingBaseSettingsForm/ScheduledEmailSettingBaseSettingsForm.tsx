import React, { useEffect, useState } from "react";
import { Button, Form, Input, InputNumber, Radio, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import {
    ScheduledEmailSettingBaseSettingFormModel,
    ScheduledEmailSettingModel,
} from "~/models/scheduledEmailSettingModel";
import {
    ErrorMessages,
    HANKAKU_NUMBER_REGEX,
    infoColor,
    Links,
    ONLY_HANKAKU_REGEX,
    RESTRICT_SPACE_REGEX,
    TooltipMessages,
    iconCustomColor,
    EMAIL_HOST_BLOCKED,
    HelpMessages,
} from "~/utils/constants";
import styles from "./ScheduledEmailSettingBaseSettingsForm.scss";
import { isFormDisabled } from "~/utils/utils";

const { TextArea } = Input;

const sectionLayoutLabelSpan = 6;
const sectionLayout = {
    labelCol: {
        span: sectionLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - sectionLayoutLabelSpan,
    },
};

const formLayoutLabelSpan = 5;
const formLayout = {
    labelCol: {
        span: formLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - formLayoutLabelSpan,
    },
};

export const formName = "baseSettingsForm";

type Props = {
    initialData?: ScheduledEmailSettingBaseSettingFormModel;
    fieldErrors?: ScheduledEmailSettingModel;
};

const ScheduledEmailSettingBaseSettingsForm = ({
    initialData,
    fieldErrors,
}: Props) => {
    const requiredFields = [
        "hostname",
        "password",
        "port_number",
        "connection_type",
        "username",
    ];
    const [form] = Form.useForm<ScheduledEmailSettingBaseSettingFormModel>();
    const [isConnectionTestable, setIsConnectionTestable] = useState(false);

    useEffect(() => {
        if (!initialData) {
            setIsConnectionTestable(true);
            return;
        }
        form.setFieldsValue(initialData);
        if (initialData.username) {
            setIsConnectionTestable(false);
        }
    }, [initialData]);

    return (
        <Form
            name={formName}
            form={form}
            style={{ textAlign: "left" }}
            labelAlign="right"
            onValuesChange={(changedValues, allValues) => {
                setIsConnectionTestable(true);
                if (changedValues.connection_type) {
                    form.setFieldsValue({
                        port_number:
                            changedValues.connection_type === 3 ? 465 : 587,
                    });
                }
            }}>
            <Form.Item
                {...sectionLayout}
                label="送信サーバーの設定"
                style={{
                    padding: 0,
                    margin: 0,
                }}></Form.Item>
            <Form.Item
                style={{
                    padding: 0,
                    marginTop: 0,
                    marginBottom: 8,
                }}>
                <Form.Item
                    {...formLayout}
                    label={
                        <span>
                            ユーザー名&nbsp;
                            <Tooltip
                                overlayStyle={{ minWidth: '270px' }}
                                title={
                                    <span>
                                        送信サーバーのユーザー名を入力します。
                                        <br />
                                        ユーザー名はご使用のサーバーにより形式が異なります。
                                        <br />
                                        例：「you」または「you@example.com」
                                    </span>
                                }>
                                <QuestionCircleFilled
                                    style={{
                                        color: iconCustomColor,
                                    }}
                                    className={styles.tooltip}
                                />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    required={true}
                    validateStatus={fieldErrors?.username ? "error" : undefined}
                    help={fieldErrors?.username}
                    rules={[
                        {
                            pattern: ONLY_HANKAKU_REGEX,
                            message: ErrorMessages.validation.regex.onlyHankaku,
                        },
                        {
                            pattern: RESTRICT_SPACE_REGEX,
                            message: ErrorMessages.validation.regex.space,
                        },
                        {
                            max: 50,
                            message: ErrorMessages.validation.length.max50,
                        },
                    ]}
                    name="username">
                    <TextArea autoSize={{ minRows: 1 }} placeholder="you／you@example.com" />
                </Form.Item>
                <Form.Item
                    {...formLayout}
                    label={
                        <span>
                            サーバー名&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        送信サーバーのサーバー名（ドメイン）を入力します。
                                        <br />
                                        例：「example.com」
                                    </span>
                                }>
                                <QuestionCircleFilled
                                    style={{
                                        color: iconCustomColor,
                                    }}
                                    className={styles.tooltip}
                                />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    required={true}
                    validateStatus={fieldErrors?.hostname ? "error" : undefined}
                    help={fieldErrors?.hostname}
                    rules={[
                        {
                            pattern: ONLY_HANKAKU_REGEX,
                            message: ErrorMessages.validation.regex.onlyHankaku,
                        },
                        {
                            pattern: RESTRICT_SPACE_REGEX,
                            message: ErrorMessages.validation.regex.space,
                        },
                        {
                            max: 50,
                            message: ErrorMessages.validation.length.max50,
                        },
                        {
                            validator: (_, hostname: string) => {
                                if (EMAIL_HOST_BLOCKED.includes(hostname)) {
                                    const errorMessage =
                                        hostname + HelpMessages.cannotUsePrefix;
                                    return Promise.reject(
                                        new Error(errorMessage)
                                    );
                                }
                                return Promise.resolve();
                            },
                        },
                    ]}
                    name="hostname">
                    <TextArea
                        autoSize={{ minRows: 1 }}
                        placeholder="example.com"
                    />
                </Form.Item>
                <Form.Item
                    {...formLayout}
                    label="ポート番号"
                    className={styles.field}
                    required={true}
                    validateStatus={
                        fieldErrors?.port_number ? "error" : undefined
                    }
                    help={fieldErrors?.port_number}
                    rules={[
                        {
                            pattern: HANKAKU_NUMBER_REGEX,
                            message:
                                ErrorMessages.validation.regex
                                    .onlyHankakuNumber,
                        },
                        {
                            validator: (_, value) => {
                                const portNumberString = String(value ?? "");
                                if (portNumberString.length > 5) {
                                    return Promise.reject(
                                        new Error("5桁以内で入力してください。")
                                    );
                                }
                                return Promise.resolve();
                            },
                        },
                    ]}
                    name="port_number">
                    <InputNumber
                        style={{
                            width: "100%",
                        }}
                    />
                </Form.Item>
                <Form.Item
                    {...formLayout}
                    label="パスワード"
                    className={styles.field}
                    required={true}
                    validateStatus={fieldErrors?.password ? "error" : undefined}
                    help={fieldErrors?.password}
                    name="password">
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    {...formLayout}
                    label="接続の保護"
                    className={styles.field}
                    name="connection_type"
                    required={true}>
                    <Radio.Group>
                        <Radio value={1}>SMTP</Radio>
                        <Radio value={2}>SMTP+TLS</Radio>
                        <Radio value={3}>SMTP+SSL</Radio>
                    </Radio.Group>
                </Form.Item>
            </Form.Item>
            <Form.Item noStyle shouldUpdate>
                {() => (
                    <Button
                        type="default"
                        htmlType="submit"
                        className={styles.button}
                        disabled={
                            !isConnectionTestable ||
                            isFormDisabled(form, requiredFields)
                        }>
                        接続テスト
                    </Button>
                )}
            </Form.Item>
        </Form>
    );
};

export default ScheduledEmailSettingBaseSettingsForm;
