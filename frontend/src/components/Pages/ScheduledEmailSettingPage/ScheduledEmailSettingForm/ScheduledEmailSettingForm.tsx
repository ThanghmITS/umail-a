import React, { useEffect, useState } from "react";
import {
    Button,
    Col,
    Form,
    Input,
    InputNumber,
    Radio,
    Row,
    Spin,
    Tabs,
    Tooltip,
} from "antd";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import {
    useTestConnectScheduledEmailSettingAPIMutate,
    useUpdateScheduledEmailSettingAPIMutate,
} from "~/hooks/useScheduledEmailSetting";
import {
    ScheduledEmailSettingBaseSettingFormModel,
    ScheduledEmailSettingModel,
    ScheduledEmailSettingRequestModel,
} from "~/models/scheduledEmailSettingModel";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import {
    ErrorMessages,
    HANKAKU_NUMBER_REGEX,
    infoColor,
    Links,
    ONLY_HANKAKU_REGEX,
    RESTRICT_SPACE_REGEX,
    TooltipMessages,
    iconCustomColor
} from "~/utils/constants";
import { InfoCircleTwoTone, QuestionCircleFilled } from "@ant-design/icons";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import Paths from "~/components/Routes/Paths";
import BackButton from "~/components/Common/BackButton/BackButton";
import { useHistory } from "react-router-dom";
import styles from "./ScheduledEmailSettingForm.scss";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import ScheduledEmailSettingBaseSettingsForm, {
    formName as baseSettingsFormName,
} from "./ScheduledEmailSettingBaseSettingsForm/ScheduledEmailSettingBaseSettingsForm";
import ScheduledEmailSettingDetailSettingsForm, {
    formName as detailSettingsFormName,
} from "./ScheduledEmailSettingDetailSettingsForm/ScheduledEmailSettingDetailSettingsForm";
import { FormChangeInfo, FormFinishInfo } from "rc-field-form/es/FormContext";
const { TextArea } = Input;

const { TabPane } = Tabs;

const containerLayout = {
    xs: 24,
    sm: 24,
    md: 21,
    lg: 18,
    xl: 15,
    xxl: 12,
};

const emptyData: ScheduledEmailSettingModel = {
    file_type: 2,
    company: "",
    hostname: "",
    is_open_count_available: false,
    is_open_count_extra_period_available: false,
    is_use_attachment_max_size_available: false,
    is_use_delivery_interval_available: false,
    is_use_remove_promotion_available: false,
    modified_time: "",
    modified_user: "",
    password: "",
    port_number: 587,
    target_count_addon_purchase_count: 0,
    use_attachment_max_size: false,
    use_open_count: false,
    use_open_count_extra_period: false,
    use_remove_promotion: false,
    connection_type: 1,
    username: "",
};

type Props = {
    initialData?: ScheduledEmailSettingModel;
    fieldErrors?: ScheduledEmailSettingModel;
};

const ScheduledEmailSettingForm = ({ initialData, fieldErrors }: Props) => {
    const [scheduledEmailSettings, setScheduledEmailSettings] =
        useState<ScheduledEmailSettingModel>(emptyData);
    const [isEdit, setIsEdit] = useState(false);
    const [isConnectionTestSuccess, setIsConnectionTestSuccess] =
        useState(false);
    const [form] = Form.useForm<ScheduledEmailSettingModel>();
    const router = useHistory();
    const { mutate: testConnection, isLoading: isTestConnectionProcessing } =
        useTestConnectScheduledEmailSettingAPIMutate();
    const {
        mutate: updateScheduledEmailSetting,
        isLoading: isUpdateProcessing,
    } = useUpdateScheduledEmailSettingAPIMutate();

    const onTestConnect = (
        values: ScheduledEmailSettingBaseSettingFormModel
    ) => {
        try {
            testConnection(values, {
                onSuccess: (response) => {
                    setIsConnectionTestSuccess(true);
                    setScheduledEmailSettings({
                        ...scheduledEmailSettings,
                        ...values,
                    });
                },
            });
        } catch (err) {
            console.error(err);
        }
    };

    const onRegister = (values: ScheduledEmailSettingRequestModel) => {
        confirmModal({
            title: "配信メール設定登録",
            content: <div>入力した内容で登録を行います。</div>,
            okText: "OK",
            cancelText: "キャンセル",
            onOk: () =>
                updateScheduledEmailSetting(values, {
                    onSuccess: (response) => {
                        router.push(Paths.dashboard);
                    },
                }),
        });
    };

    const onUpdate = (values: ScheduledEmailSettingRequestModel) => {
        updateScheduledEmailSetting(values, {
            onSuccess: (response) => {
                router.push(Paths.dashboard);
            },
        });
    };

    const onFinish = () => {
        if (isEdit) {
            onUpdate(scheduledEmailSettings);
        } else {
            onRegister(scheduledEmailSettings);
        }
    };

    const onFormChange = (
        name: string,
        { changedFields, forms }: FormChangeInfo
    ) => {
        if (name === detailSettingsFormName) {
            const { detailSettingsForm } = forms;
            const fileType = detailSettingsForm.getFieldValue("file_type");
            setScheduledEmailSettings({
                ...scheduledEmailSettings,
                file_type: fileType,
            });
        }
        if (name === baseSettingsFormName) {
            if (isConnectionTestSuccess) {
                setIsConnectionTestSuccess(false);
            }
        }
    };

    const onFormFinish = (name: string, { values, forms }: FormFinishInfo) => {
        if (name === baseSettingsFormName) {
            onTestConnect(values as ScheduledEmailSettingBaseSettingFormModel);
        }
    };

    useEffect(() => {
        if (!initialData) {
            return;
        }
        setScheduledEmailSettings({
            ...emptyData,
            ...initialData,
            port_number: !!initialData.port_number
                ? initialData.port_number
                : emptyData.port_number,
        });
        if (initialData.username) {
            setIsEdit(true);
            setIsConnectionTestSuccess(true);
        }
    }, [initialData]);

    return (
        <Row justify="start">
            <Col {...containerLayout}>
                <Spin
                    spinning={isTestConnectionProcessing || isUpdateProcessing}>
                    <Form.Provider
                        onFormChange={onFormChange}
                        onFormFinish={onFormFinish}>
                        <Tabs
                            defaultActiveKey="1"
                            tabBarExtraContent={
                                <Tooltip
                                    title={
                                        TooltipMessages.scheduledEmailSetting
                                            .warning
                                    }
                                    color={infoColor}>
                                    <InfoCircleTwoTone
                                        twoToneColor={infoColor}
                                    />
                                </Tooltip>
                            }>
                            <TabPane tab="基本設定" key="1">
                                <ScheduledEmailSettingBaseSettingsForm
                                    initialData={scheduledEmailSettings}
                                    fieldErrors={fieldErrors}
                                />
                            </TabPane>
                            <TabPane tab="詳細設定" key="2">
                                <ScheduledEmailSettingDetailSettingsForm
                                    initialData={scheduledEmailSettings}
                                    isNotRegistered={
                                        !scheduledEmailSettings.username ||
                                        !isEdit
                                    }
                                />
                            </TabPane>
                        </Tabs>
                        <br />
                        <Col span={24} style={{ marginTop: "5%" }}>
                            <Row justify="start">
                                <Col>
                                    <BackButton />
                                </Col>
                                <Col>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        className={styles.button}
                                        disabled={!isConnectionTestSuccess}
                                        onClick={onFinish}>
                                        {isEdit ? "更新" : "登録"}
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Provider>
                </Spin>
            </Col>
        </Row>
    );
};

export default ScheduledEmailSettingForm;
