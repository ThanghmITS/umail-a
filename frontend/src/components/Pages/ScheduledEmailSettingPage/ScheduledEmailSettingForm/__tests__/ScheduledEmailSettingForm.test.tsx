import React from "react";
import { ScheduledEmailSettingModel } from "~/models/scheduledEmailSettingModel";
import { renderWithQueryClient, screen, userEvent } from "~/test/utils";
import ScheduledEmailSettingForm from "../ScheduledEmailSettingForm";
import { scheduledEmailSettingAPIMockData } from "~/test/mock/scheduledEmailSettingMock";
import { convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel } from "~/hooks/useScheduledEmailSetting";

const emptyInitialData: ScheduledEmailSettingModel = {
    file_type: 2,
    company: "",
    hostname: "",
    is_open_count_available: false,
    is_open_count_extra_period_available: false,
    is_use_attachment_max_size_available: false,
    is_use_delivery_interval_available: false,
    is_use_remove_promotion_available: false,
    modified_time: "",
    modified_user: "",
    password: "",
    port_number: 587,
    target_count_addon_purchase_count: 0,
    use_attachment_max_size: false,
    use_open_count: false,
    use_open_count_extra_period: false,
    use_remove_promotion: false,
    connection_type: 1,
    username: "",
};

const filledInitialData: ScheduledEmailSettingModel = {
    ...convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel(
        scheduledEmailSettingAPIMockData
    ),
};

describe("ScheduledEmailSettingForm.text", () => {
    describe("register form", () => {
        test("render test basic info", async () => {
            renderWithQueryClient(
                <ScheduledEmailSettingForm initialData={emptyInitialData} />
            );
            const parentLabelElement = screen.getByText("送信サーバーの設定");
            expect(parentLabelElement).toBeInTheDocument();
            const usernameInputElement = screen.getByLabelText("ユーザー名");
            expect(usernameInputElement).toBeInTheDocument();
            const hostnameInputElement = screen.getByLabelText("サーバー名");
            expect(hostnameInputElement).toBeInTheDocument();
            const portNumberInputElement = screen.getByLabelText("ポート番号");
            expect(portNumberInputElement).toBeInTheDocument();
            const passwordInputElement = screen.getByLabelText("パスワード");
            expect(passwordInputElement).toBeInTheDocument();
            const connectionTypeRadioElement = screen.getByText("接続の保護");
            expect(connectionTypeRadioElement).toBeInTheDocument();
            const connectionTestButtonElement = screen.getByRole("button", {
                name: "接続テスト",
            });
            expect(connectionTestButtonElement).toBeInTheDocument();
            const backButtonElement = screen.getByRole("button", {
                name: "戻る",
            });
            expect(backButtonElement).toBeInTheDocument();
            const registerButtonElement = screen.getByRole("button", {
                name: "登 録", // NOTE(joshua-hashimoto): 間にスペースが入っている。おそらくサイズと見え方のためにAnt Designが自動でスペースを入れている
            });
            expect(registerButtonElement).toBeInTheDocument();
            expect(registerButtonElement).toBeDisabled();
        });

        test("render test detail info", async () => {
            renderWithQueryClient(
                <ScheduledEmailSettingForm initialData={emptyInitialData} />
            );
            const detailInfoTabButton = screen.getByText(/詳細設定/);
            await userEvent.click(detailInfoTabButton);
            const openCountAvailableElement =
                screen.getByText("開封情報の取得");
            expect(openCountAvailableElement).toBeInTheDocument();
            const openCountOption1 = screen.getByRole("radio", {
                name: "なし",
            });
            const openCountOption2 = screen.getByRole("radio", {
                name: "あり",
            });
            expect(openCountOption1).toBeChecked();
            expect(openCountOption2).not.toBeChecked();
            const openCountExtraPeriodAvailableElement =
                screen.queryByText("開封情報の取得期間");
            expect(
                openCountExtraPeriodAvailableElement
            ).not.toBeInTheDocument();
            const useAttachmentMaxSizeAvailableElement =
                screen.getByText("配信添付容量の上限");
            expect(useAttachmentMaxSizeAvailableElement).toBeInTheDocument();
            const useAttachmentMaxSizeOption1 = screen.getByRole("radio", {
                name: "2MB",
            });
            const useAttachmentMaxSizeOption2 = screen.getByRole("radio", {
                name: "10MB",
            });
            expect(useAttachmentMaxSizeOption1).toBeChecked();
            expect(useAttachmentMaxSizeOption2).not.toBeChecked();
            const useRemovePromotionAvailableElement =
                screen.getByText("HTML配信時の広告表示");
            expect(useRemovePromotionAvailableElement).toBeInTheDocument();
            const useRemovePromotionOption1 = screen.getByRole("radio", {
                name: "表示",
            });
            const useRemovePromotionOption2 = screen.getByRole("radio", {
                name: "非表示",
            });
            expect(useRemovePromotionOption1).toBeChecked();
            expect(useRemovePromotionOption2).not.toBeChecked();
            const targetCountAddonPurchaseCountElement =
                screen.getByText("配信件数の上限");
            expect(targetCountAddonPurchaseCountElement).toBeInTheDocument();
            const targetCountAddonPurchaseOption1 = screen.getByRole("radio", {
                name: "1000件",
            });
            const targetCountAddonPurchaseOption2 = screen.getByRole("radio", {
                name: "2000件",
            });
            const targetCountAddonPurchaseOption3 = screen.getByRole("radio", {
                name: "3000件",
            });
            const targetCountAddonPurchaseOption4 = screen.getByRole("radio", {
                name: "4000件",
            });
            const targetCountAddonPurchaseOption5 = screen.getByRole("radio", {
                name: "5000件",
            });
            expect(targetCountAddonPurchaseOption1).toBeChecked();
            expect(targetCountAddonPurchaseOption2).not.toBeChecked();
            expect(targetCountAddonPurchaseOption3).not.toBeChecked();
            expect(targetCountAddonPurchaseOption4).not.toBeChecked();
            expect(targetCountAddonPurchaseOption5).not.toBeChecked();
            const useDeliveryIntervalAvailableElement =
                screen.getByText("配信予約時間の間隔");
            expect(useDeliveryIntervalAvailableElement).toBeInTheDocument();
            const useDeliveryIntervalOption1 = screen.getByRole("radio", {
                name: "30分単位",
            });
            const useDeliveryIntervalOption2 = screen.getByRole("radio", {
                name: "10分単位",
            });
            expect(useDeliveryIntervalOption1).toBeChecked();
            expect(useDeliveryIntervalOption2).not.toBeChecked();
        });

        test("can input", async () => {
            // NOTE(joshua-hashimoto): v3テストの時に参考にするため残しておく
            renderWithQueryClient(
                <ScheduledEmailSettingForm initialData={emptyInitialData} />
            );
            const parentLabelElement = screen.getByText("送信サーバーの設定");
            expect(parentLabelElement).toBeInTheDocument();
            const usernameInputElement = screen.getByLabelText(
                "ユーザー名"
            ) as HTMLInputElement;
            await userEvent.type(usernameInputElement, "test_username");
            expect(usernameInputElement.value).toBe("test_username");
            const hostnameInputElement = screen.getByLabelText(
                "サーバー名"
            ) as HTMLInputElement;
            await userEvent.type(hostnameInputElement, "test_hostname");
            expect(hostnameInputElement.value).toBe("test_hostname");
            const portNumberInputElement = screen.getByLabelText(
                "ポート番号"
            ) as HTMLInputElement;
            await userEvent.clear(portNumberInputElement);
            await userEvent.type(portNumberInputElement, "100");
            expect(portNumberInputElement.value).toBe("100");
            const passwordInputElement = screen.getByLabelText(
                "パスワード"
            ) as HTMLInputElement;
            await userEvent.type(passwordInputElement, "Abcde1234%");
            expect(passwordInputElement.value).toBe("Abcde1234%");
            const smtpRadio = screen.getByRole("radio", { name: "SMTP" });
            const smtpTLSRadio = screen.getByRole("radio", {
                name: "SMTP+TLS",
            });
            const smtpSSLRadio = screen.getByRole("radio", {
                name: "SMTP+SSL",
            });
            expect(smtpRadio).toBeChecked();
            expect(smtpTLSRadio).not.toBeChecked();
            expect(smtpSSLRadio).not.toBeChecked();
            await userEvent.click(smtpTLSRadio);
            expect(smtpRadio).not.toBeChecked();
            expect(smtpTLSRadio).toBeChecked();
            expect(smtpSSLRadio).not.toBeChecked();
            await userEvent.click(smtpSSLRadio);
            expect(smtpRadio).not.toBeChecked();
            expect(smtpTLSRadio).not.toBeChecked();
            expect(smtpSSLRadio).toBeChecked();
        });
    });
    describe("edit form", () => {
        test("render test basic info", async () => {
            renderWithQueryClient(
                <ScheduledEmailSettingForm initialData={filledInitialData} />
            );
            const parentLabelElement = screen.getByText("送信サーバーの設定");
            expect(parentLabelElement).toBeInTheDocument();
            const usernameInputElement = screen.getByLabelText(
                "ユーザー名"
            ) as HTMLInputElement;
            expect(usernameInputElement).toBeInTheDocument();
            expect(usernameInputElement.value).toBe(filledInitialData.username);
            const hostnameInputElement = screen.getByLabelText(
                "サーバー名"
            ) as HTMLInputElement;
            expect(hostnameInputElement).toBeInTheDocument();
            expect(hostnameInputElement.value).toBe(filledInitialData.hostname);
            const portNumberInputElement = screen.getByLabelText(
                "ポート番号"
            ) as HTMLInputElement;
            expect(portNumberInputElement).toBeInTheDocument();
            expect(portNumberInputElement.value).toBe(
                `${filledInitialData.port_number}`
            );
            const passwordInputElement = screen.getByLabelText(
                "パスワード"
            ) as HTMLInputElement;
            expect(passwordInputElement).toBeInTheDocument();
            expect(passwordInputElement.value).toBe(filledInitialData.password);
            const connectionTypeRadioElement = screen.getByText("接続の保護");
            expect(connectionTypeRadioElement).toBeInTheDocument();
            expect(
                screen.getByRole("radio", { name: "SMTP+TLS" })
            ).toBeChecked();
            const connectionTestButtonElement = screen.getByRole("button", {
                name: "接続テスト",
            });
            expect(connectionTestButtonElement).toBeInTheDocument();
            const backButtonElement = screen.getByRole("button", {
                name: "戻る",
            });
            expect(backButtonElement).toBeInTheDocument();
            const registerButtonElement = screen.getByRole("button", {
                name: "更 新", // NOTE(joshua-hashimoto): 間にスペースが入っている。おそらくサイズと見え方のためにAnt Designが自動でスペースを入れている
            });
            expect(registerButtonElement).toBeInTheDocument();
            expect(registerButtonElement).not.toBeDisabled();
        });
        test("render test detail info", async () => {
            renderWithQueryClient(
                <ScheduledEmailSettingForm initialData={filledInitialData} />
            );
            const detailInfoTabButton = screen.getByText(/詳細設定/);
            await userEvent.click(detailInfoTabButton);
            const openCountAvailableElement =
                screen.getByText("開封情報の取得");
            expect(openCountAvailableElement).toBeInTheDocument();
            const openCountOption1 = screen.getByRole("radio", {
                name: "なし",
            });
            const openCountOption2 = screen.getByRole("radio", {
                name: "あり",
            });
            expect(openCountOption1).not.toBeChecked();
            expect(openCountOption2).toBeChecked();
            const openCountExtraPeriodAvailableElement =
                screen.getByText("開封情報の取得期間");
            expect(openCountExtraPeriodAvailableElement).toBeInTheDocument();
            const openCountExtraPeriodOption1 = screen.getByRole("radio", {
                name: "72時間",
            });
            const openCountExtraPeriodOption2 = screen.getByRole("radio", {
                name: "30日",
            });
            expect(openCountExtraPeriodOption1).not.toBeChecked();
            expect(openCountExtraPeriodOption2).toBeChecked();
            const useAttachmentMaxSizeAvailableElement =
                screen.getByText("配信添付容量の上限");
            expect(useAttachmentMaxSizeAvailableElement).toBeInTheDocument();
            const useAttachmentMaxSizeOption1 = screen.getByRole("radio", {
                name: "2MB",
            });
            const useAttachmentMaxSizeOption2 = screen.getByRole("radio", {
                name: "10MB",
            });
            expect(useAttachmentMaxSizeOption1).not.toBeChecked();
            expect(useAttachmentMaxSizeOption2).toBeChecked();
            const useRemovePromotionAvailableElement =
                screen.getByText("HTML配信時の広告表示");
            expect(useRemovePromotionAvailableElement).toBeInTheDocument();
            const useRemovePromotionOption1 = screen.getByRole("radio", {
                name: "表示",
            });
            const useRemovePromotionOption2 = screen.getByRole("radio", {
                name: "非表示",
            });
            expect(useRemovePromotionOption1).not.toBeChecked();
            expect(useRemovePromotionOption2).toBeChecked();
            const targetCountAddonPurchaseCountElement =
                screen.getByText("配信件数の上限");
            expect(targetCountAddonPurchaseCountElement).toBeInTheDocument();
            const targetCountAddonPurchaseOption1 = screen.getByRole("radio", {
                name: "1000件",
            });
            const targetCountAddonPurchaseOption2 = screen.getByRole("radio", {
                name: "2000件",
            });
            const targetCountAddonPurchaseOption3 = screen.getByRole("radio", {
                name: "3000件",
            });
            const targetCountAddonPurchaseOption4 = screen.getByRole("radio", {
                name: "4000件",
            });
            const targetCountAddonPurchaseOption5 = screen.getByRole("radio", {
                name: "5000件",
            });
            expect(targetCountAddonPurchaseOption1).not.toBeChecked();
            expect(targetCountAddonPurchaseOption2).not.toBeChecked();
            expect(targetCountAddonPurchaseOption3).not.toBeChecked();
            expect(targetCountAddonPurchaseOption4).toBeChecked();
            expect(targetCountAddonPurchaseOption5).not.toBeChecked();
            const useDeliveryIntervalAvailableElement =
                screen.getByText("配信予約時間の間隔");
            expect(useDeliveryIntervalAvailableElement).toBeInTheDocument();
            const useDeliveryIntervalOption1 = screen.getByRole("radio", {
                name: "30分単位",
            });
            const useDeliveryIntervalOption2 = screen.getByRole("radio", {
                name: "10分単位",
            });
            expect(useDeliveryIntervalOption1).not.toBeChecked();
            expect(useDeliveryIntervalOption2).toBeChecked();
        });
    });
});
