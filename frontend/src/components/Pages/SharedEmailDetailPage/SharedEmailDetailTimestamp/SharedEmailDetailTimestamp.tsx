import React from "react";
import { Col, Row, Typography } from "antd";
import moment from "moment";
import styles from "./SharedEmailDetailTimestamp.scss";

const { Paragraph } = Typography;

type Props = {
    date: string;
};

const SharedEmailDetailTimestamp = ({ date }: Props) => {
    const renderEmailDateString = () => {
        const today = moment();
        const emailDate = moment(date);

        const isSameYear = today.year() === emailDate.year();
        const isSameMonth = today.month() === emailDate.month();
        const isSameDate = today.date() === emailDate.date();

        if (isSameYear && isSameMonth && isSameDate) {
            return date.split(" ")[1];
        }
        return date;
    };

    return (
        <Col>
            <Paragraph>{renderEmailDateString()}</Paragraph>
        </Col>
    );
};

export default SharedEmailDetailTimestamp;
