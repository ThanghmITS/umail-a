import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import styles from "./SharedEmailDetailPageForwardButton.scss";

type Props = {
    disabled?: boolean;
};

const SharedEmailDetailPageForwardButton = ({ disabled = false }: Props) => {
    const { select_staff: selectStaffAuthorized } =
        useAuthorizedActions("shared_emails");

    const renderButton = () => {
        return (
            <Button
                type="primary"
                htmlType="submit"
                disabled={disabled || !selectStaffAuthorized}>
                自社担当者にメール転送
            </Button>
        );
    };

    if (!selectStaffAuthorized) {
        return (
            <Tooltip title={ErrorMessages.isNotAuthorized}>
                {renderButton()}
            </Tooltip>
        );
    }

    return renderButton();
};

export default SharedEmailDetailPageForwardButton;
