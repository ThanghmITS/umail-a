import React, { useEffect } from "react";
import { Card, Col, Form, Row, Spin } from "antd";
import { useHistory, useParams } from "react-router-dom";
import {
    useSharedEmailDeleteAPIMutation,
    useSharedEmailFetchDetailAPIQuery,
    useSharedEmailForwardAPIMutation,
} from "~/hooks/useSharedEmail";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import ErrorScreen from "~/components/Screens/ErrorScreen";
import { useTypedSelector } from "~/models/store";
import SharedEmailDetailTimestamp from "./SharedEmailDetailTimestamp/SharedEmailDetailTimestamp";
import SharedEmailDetailComment from "./SharedEmailDetailComment/SharedEmailDetailComment";
import EmailHead from "~/components/Pages/EmailComponents/EmailHead";
import EmailBody from "~/components/Pages/EmailComponents/EmailBody";
import UserAjaxSelect from "~/components/Common/UserAjaxSelect/UserAjaxSelect";
import { SharedEmailFormModel } from "~/models/sharedEmailModel";
import BackButton from "~/components/Common/BackButton/BackButton";
import Paths from "~/components/Routes/Paths";
import { isFormDisabled } from "~/utils/utils";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import SharedEmailDetailPageForwardButton from "./SharedEmailDetailPageForwardButton/SharedEmailDetailPageForwardButton";
import SharedEmailDetailPageDeleteButton from "./SharedEmailDetailPageDeleteButton/SharedEmailDetailPageDeleteButton";
import { useDispatch } from "react-redux";
import { CommentActions } from "~/actionCreators/commentActions";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import styles from "./SharedEmailDetailPage.scss";

const SharedEmailDetailPage = () => {
    const requiredFields = ["staffInChargeId"];
    const { id } = useParams<{ id: string }>();
    const router = useHistory();
    const [form] = Form.useForm<SharedEmailFormModel>();
    const dispatch = useDispatch();
    const { select_all_staff: selectAllStaffAuthorized } =
        useAuthorizedActions("shared_emails");
    const newCommentState = useTypedSelector(
        (state) => state.sharedEmailDetailPageNewComments
    );
    const editCommentState = useTypedSelector(
        (state) => state.sharedEmailDetailPageEditComments
    );
    const { mutate: forwardSharedEmail } = useSharedEmailForwardAPIMutation();
    const { mutate: deleteSharedEmail } = useSharedEmailDeleteAPIMutation();
    const { data, isLoading, error, isError } =
        useSharedEmailFetchDetailAPIQuery({ id });

    const onDelete = () => {
        confirmModal({
            content: (
                <span>
                    OKを押すと、削除が実行されます。
                    <br />
                    元には戻せません。
                </span>
            ),
            onOk: () => deleteSharedEmail(id),
        });
    };

    const onFinish = (values: SharedEmailFormModel) => {
        if (newCommentState.commentValue) {
            dispatch(CommentActions.newCommentExistAction());
            return;
        }
        if (editCommentState.commentValue) {
            dispatch(CommentActions.editCommentExistAction());
            return;
        }
        const data = {
            id,
            postData: values,
        };
        forwardSharedEmail(data, {
            onSuccess: (response) => {
                router.push(Paths.sharedMails);
            },
        });
    };

    useEffect(() => {
        if (data?.staffInChargeId) {
            form.setFieldsValue({ staffInChargeId: data.staffInChargeId });
        }
    }, [data]);

    useEffect(() => {
        return () => {
            dispatch(CommentActions.newCommentClearAction());
            dispatch(CommentActions.editCommentClearAction());
        };
    }, []);

    if (isError) {
        return <ErrorScreen message={error?.response?.data.detail!} />;
    }

    if (!data || isLoading) {
        return <Spin data-testid="shared-email-spin" />;
    }

    return (
        <Col span={24}>
            <Row>
                <Col span={24}>
                    <CustomPageHeader title="共有メール 詳細" />
                </Col>
            </Row>
            <Row justify="center">
                <Col span={19}>
                    <Form form={form} onFinish={onFinish}>
                        <Col span={24}>
                            <Row justify="end">
                                <Col>
                                    {data.sentDate && (
                                        <SharedEmailDetailTimestamp
                                            date={data.sentDate}
                                        />
                                    )}
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <Row>
                                        <Col span={24}>
                                            <Card>
                                                <EmailHead data={data} />
                                                <EmailBody
                                                    text={
                                                        !!data.html
                                                            ? data.html
                                                            : data.text
                                                    }
                                                    text_format={data.format}
                                                    mark={false}
                                                />
                                            </Card>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={24}>
                                            <Row>
                                                <Form.Item noStyle>
                                                    <Col
                                                        span={10}
                                                        className={
                                                            styles.staffSelectContainer
                                                        }>
                                                        <Row>
                                                            <Form.Item
                                                                label="自社担当者"
                                                                name="staffInChargeId"
                                                                className={
                                                                    styles.staffFormItem
                                                                }>
                                                                <UserAjaxSelect
                                                                    disabled={
                                                                        !selectAllStaffAuthorized
                                                                    }
                                                                />
                                                            </Form.Item>
                                                        </Row>
                                                    </Col>
                                                </Form.Item>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <SharedEmailDetailComment />
                                    </Row>
                                    <Row>
                                        <Col
                                            span={24}
                                            style={{ marginTop: "5%" }}>
                                            <Row justify="space-between">
                                                <Col>
                                                    <BackButton
                                                        to={Paths.sharedMails}
                                                    />
                                                    <Form.Item
                                                        noStyle
                                                        shouldUpdate>
                                                        {() => (
                                                            <SharedEmailDetailPageForwardButton
                                                                disabled={isFormDisabled(
                                                                    form,
                                                                    requiredFields
                                                                )}
                                                            />
                                                        )}
                                                    </Form.Item>
                                                </Col>
                                                <Col>
                                                    <Form.Item noStyle>
                                                        <SharedEmailDetailPageDeleteButton
                                                            onDelete={onDelete}
                                                        />
                                                    </Form.Item>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Form>
                </Col>
            </Row>
        </Col>
    );
};

export default SharedEmailDetailPage;
