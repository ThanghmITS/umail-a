const Paths = {
  index: '/',
  login: '/login/',
  users: '/users',
  dashboard: '/dashboard',
  userRegister: '/users/register',
  userInvite: '/users/invite',
  myProfile: '/myProfile',
  myCompany: '/myCompany',
  contacts: '/contacts',
  contactRegister: '/contacts/register',
  contactsCsvUpload: '/contacts/csvUpload',
  sharedMails: '/sharedMails',
  sharedMailNotifications: '/sharedMailNotifications',
  sharedMailNotificationRegister: '/sharedMailNotifications/register',
  organizations: '/organizations',
  organizationsRegister: '/organizations/register',
  organizationsCsvUpload: '/organizations/csvUpload',
  scheduledMails: '/scheduledMails',
  scheduledMailsRegister: '/scheduledMails/register',
  scheduledMailsDownloadAttachment: '/scheduledMails/files/download',
  tags: '/tags',
  tagRegister: '/tags/register',
  displaySettings: '/displaySettings',
  sharedEmailSettings: '/sharedEmailSettings',
  scheduledEmailSettings: '/scheduledEmailSettings',
  passwordReset: '/passwordReset',
  passwordChange: '/passwordChange',
  emailCheck: '/emailCheck',
  addons: '/addons',
  purchaseHistory: '/purchaseHistory',
  payment: '/payment',
  plan: '/plan',
  tenant: '/tenant',
  maintenance: '/maintenance',
  personnel: '/personnel',
  project: "/project",
};

export const Suffixes = {
  mailProfile: 'mailProfile', // This is a suffix string, not a prefix.
};

export default Paths;
