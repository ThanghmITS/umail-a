import React, { useEffect } from "react";
import { Route, RouteProps } from "react-router-dom";
import LoginPage from "~/components/Pages/LoginPage/LoginPage";
import { useGuardAuthorizedActions } from "~/hooks/useAuthorizedActions";

type Props = RouteProps & {
    path: string;
    isLoggedIn: boolean;
};

const PrivateRoute = ({ isLoggedIn, ...props }: Props) => {
    const { path } = { ...props };

    useGuardAuthorizedActions(path);

    if (isLoggedIn) {
        return <Route {...props} />;
    }
    return (
        <Route
            render={(givenProps) => (
                <LoginPage redirectTo={path} {...givenProps} />
            )}
        />
    );
};

export default PrivateRoute;
