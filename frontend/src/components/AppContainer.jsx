import React, { useState } from "react";
import PropTypes from "prop-types";
import { Layout, Button, Tooltip, Row, Typography, Col } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import Menu from "./Navigations/MainMenu";
import AppHeader from "./Navigations/AppHeader";
import { useGuardPlanSummary } from "~/hooks/usePlan";
import { Links } from "~/utils/constants";
import styles from "./AppContainer.scss";

const { Content, Sider, Footer } = Layout;
const { Text, Link } = Typography;

const sideBarWidth = 250;

const footerLayout = {
    textAlign: "center",
    zIndex: 3,
};

const CustomLink = ({ href = "", title = "" }) => {
    return (
        <Link href={href} className={styles.customLink}>
            {title}
        </Link>
    );
};

const AppContainer = ({ isLoggedIn, children }) => {
    const [isCollapsed, setIsCollapsed] = useState(false);
    const { isNotValidUser } = useGuardPlanSummary();

    const toggleMenuCollapse = () => {
        setIsCollapsed(!isCollapsed);
    };

    return (
        <div>
            <Layout className="layout">
                <AppHeader isLoggedIn={isLoggedIn} />
                <Layout>
                    {isLoggedIn && !isNotValidUser ? (
                        <Sider
                            width={sideBarWidth}
                            className={styles.sider}
                            breakpoint="md"
                            collapsedWidth="0"
                            trigger={null}
                            collapsed={isCollapsed}>
                            <Menu />
                        </Sider>
                    ) : null}
                    <Content className={styles.content}>
                        {isLoggedIn && !isNotValidUser ? (
                            <Tooltip
                                title={isCollapsed ? "展開" : "折りたたむ"}>
                                <Button
                                    shape="circle"
                                    type="primary"
                                    onClick={toggleMenuCollapse}
                                    className={styles.collapse}
                                    icon={
                                        isCollapsed ? (
                                            <RightOutlined />
                                        ) : (
                                            <LeftOutlined />
                                        )
                                    }
                                />
                            </Tooltip>
                        ) : null}
                        {children}
                    </Content>
                </Layout>
                <Footer style={footerLayout}>
                    <Col span={24} justify="center">
                        <Row justify="center">
                            <Col justify="center">
                                <CustomLink
                                    href={Links.services.management}
                                    title="運営会社"
                                />{" "}
                                /{" "}
                                <CustomLink
                                    href={Links.services.tos}
                                    title="利用規約"
                                />{" "}
                                /{" "}
                                <CustomLink
                                    href={Links.services.privacyPolicy}
                                    title="プライバシーポリシー"
                                />{" "}
                                /{" "}
                                <CustomLink
                                    href={Links.services.termsOfSales}
                                    title="特商法に基づく表示"
                                />
                            </Col>
                        </Row>
                        <Row justify="center">
                            <Col justify="center">
                                <Text>
                                    コモレビ ©2022 Created by Health Basis, Inc.
                                </Text>
                            </Col>
                        </Row>
                    </Col>
                </Footer>
            </Layout>
        </div>
    );
};

AppContainer.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    children: PropTypes.element.isRequired, // Depends on Ant Design Library, hard to specify type.
};

export default AppContainer;
