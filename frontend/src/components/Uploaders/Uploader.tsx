import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Upload, message, Typography, Tooltip } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { RootState } from "~/models/store";
import { UploadChangeParam, UploadFile } from "antd/lib/upload/interface";
import { TooltipContentLinkOpenNewTab } from "../Common/TooltipContentLink/TooltipContentLink";
import Path from "../Routes/Paths";
import { useFetchScheduledEmailAttachmentSizeExtendedAPIQuery } from "~/hooks/useScheduledEmail";
import { customErrorMessage, customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { TooltipMessages, ErrorMessages } from "~/utils/constants";

const { Text } = Typography;

type DataModel = {
  status?: string;
};

type Props = {
    name: string;
    multiple: boolean;
    action: string;
    onFilePreview: (file: UploadFile) => void;
    onFileRemove: (file: UploadFile) => void;
    disabled: boolean;
    dataMail: DataModel;
    attachments?: UploadFile<any>[];
};

const Uploader = ({
    name,
    multiple = true,
    action,
    onFilePreview,
    onFileRemove,
    disabled,
    dataMail,
    attachments = [],
}: Props) => {
    const { token, role } = useSelector(
      (state: RootState) => state.login
  );
    const [uploadedFileList, setUploadedFileList] = useState<UploadFile<any>[]>(
        []
    );
    const [tooltip, setTooltip] = useState("");
    const [disableIconUpload, setDisableIconUpload] = useState(disabled);
    const { isLoading, data } =
        useFetchScheduledEmailAttachmentSizeExtendedAPIQuery({});

    useEffect(() => {
        if (attachments) {
            setUploadedFileList(attachments);
        }
    }, []);

    useEffect(() => {
      if(dataMail.status === "sending" || dataMail.status === "sent" || dataMail.status === "error") {
        setTooltip(TooltipMessages.scheduledEmails.cancelUploadfileError);
        setDisableIconUpload(true);
      }
      if (role==="guest") {
        setTooltip(ErrorMessages.isNotAuthorized);
        setDisableIconUpload(true);
      }
  }, [dataMail]);

    const onFileChange = (uploadInfo: UploadChangeParam<UploadFile<any>>) => {
        const { status } = uploadInfo.file;
        const files = [...uploadInfo.fileList];
        if (status !== "uploading") {
            console.info(uploadInfo.file, uploadInfo.fileList);
        }
        if (status === "done") {
            customSuccessMessage(`${uploadInfo.file.name} ファイルがアップロードされました。`);
        } else if (status === "error") {
            if (
                "response" in uploadInfo.file &&
                "detail" in uploadInfo.file.response
            ) {
                customErrorMessage(uploadInfo.file.response.detail);
            } else {
                customErrorMessage(`${uploadInfo.file.name} アップロードに失敗しました。`);
            }
        }
        const filteredFiles = files.filter((file) => file.status !== "error");
        setUploadedFileList(filteredFiles);
    };

    const renderUpload = () =>{
        return (
            <div>
                <Upload
                    disabled={disableIconUpload}
                    type="drag"
                    showUploadList={{
                        showPreviewIcon: true,
                        showRemoveIcon: !disableIconUpload,
                    }}
                    name={name}
                    multiple={multiple}
                    action={action}
                    fileList={uploadedFileList}
                    headers={{ Authorization: `Token ${token}` }}
                    onChange={onFileChange}
                    onPreview={onFilePreview}
                    onRemove={onFileRemove}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">
                        クリックまたはドラッグでファイルをアップロード
                    </p>
                    <p className="ant-upload-hint">
                        ※ファイルは複数アップロードすることが可能です。
                    </p>
                    <p className="ant-upload-hint">
                        ※アップロードされたファイルはクリックをするとダウンロードすることが可能です。
                    </p>
                    <p className="ant-upload-hint">
                        ※1通のメールに添付できる容量の上限はヘッダー情報を含め
                        <Text strong>
                            {data?.isAttachmentSizeExtended ? 10 : 2}MB
                        </Text>
                        です。
                    </p>
                    {!data?.isAttachmentSizeExtended && (
                        <p className="ant-upload-hint">
                            ※
                            <TooltipContentLinkOpenNewTab
                                to={`${Path.addons}`}
                                title="アドオン"
                            />
                            を購入することで<Text strong>10MB</Text>
                            にすることができます。
                        </p>
                    )}
                </Upload>
            </div>
        )
    }

    // Note: Antd's Upload component with dragger returns multiple react element, so we need to wrap it with div.
    return (
        <div>
            {disableIconUpload ?
                <Tooltip title={tooltip}>
                    {renderUpload()}
                </Tooltip>
            :
                <>{renderUpload()}</>
            }
        </div>
    );
};

export default Uploader;
