import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import TableDisplaySettingModal from "~/components/Modals/TableDisplaySettingModal/TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./UserTableDisplaySettingModal.scss";

const UserTableDisplaySettingModal = () => {
    const fieldName = "users";
    const formId = "userTableDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) && !_.isEmpty(displaySetting?.users);
        const tableHiddenColumns =
            doesDisplaySettingExist && displaySetting?.users.table;
        if (tableHiddenColumns) {
            setTargetKeys(tableHiddenColumns);
        }
        if (doesDisplaySettingExist && displaySetting?.users.page_size) {
            form.setFieldsValue({
                content: {
                    users: {
                        page_size: displaySetting.users.page_size,
                    },
                },
            });
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.setFieldsValue({
            content: {
                users: {
                    table: displaySetting?.users.table,
                    page_size: displaySetting?.users.page_size,
                },
            },
        });
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <TableDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.users.table,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default UserTableDisplaySettingModal;
