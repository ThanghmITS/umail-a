import React from "react";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage } from "~/reducers/pages";
import { defaultInitialState as EditPageInitialState } from "~/reducers/Factories/editPage";
import ContactsTable from "../ContactsTable";
import { renderWithRedux, screen } from "~/test/utils";
import { configureStore } from "@reduxjs/toolkit";
import { CONTACT_SEARCH_PAGE } from "~/components/Pages/pageIds";
import { contactListContent } from "~/test/mock/contactAPIMock";
import { generateRandomToken } from "~/utils/utils";
import { CONTACT_SEND_TYPE, WANTS_LOCATION } from "~/utils/constants";

describe("ContactsTable.jsx", () => {
    test("render test", () => {
        const { container } = renderWithRedux(
            <ContactsTable
                pageId={CONTACT_SEARCH_PAGE}
                loading={false}
                data={contactListContent}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        displaySettingPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: generateRandomToken(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                contacts: {
                                    create: true,
                                    update: true,
                                    delete: true,
                                    column_setting: true,
                                    csv_upload: true,
                                    csv_download: true,
                                    search_template: true,
                                },
                            },
                        },
                        displaySettingPage: {
                            ...EditPageInitialState,
                            isDisplaySettingLoading: false,
                        },
                    },
                }),
            }
        );
        const contactNameColumnTitleElement =
            screen.getByText(/取引先担当者名/);
        expect(contactNameColumnTitleElement).toBeInTheDocument();
        const assignedOrganizationColumnTitleElement =
            screen.getByText(/所属取引先/);
        expect(assignedOrganizationColumnTitleElement).toBeInTheDocument();
        const emailToColumnTitleElement =
            screen.getByText(/メールアドレス\(TO\)/);
        expect(emailToColumnTitleElement).toBeInTheDocument();
        const emailCcColumnTitleElement =
            screen.getByText(/メールアドレス\(CC\)/);
        expect(emailCcColumnTitleElement).toBeInTheDocument();
        const telColumnTitleElement = screen.getByText(/TEL/);
        expect(telColumnTitleElement).toBeInTheDocument();
        const positionColumnTitleElement = screen.getByText(/役職/);
        expect(positionColumnTitleElement).toBeInTheDocument();
        const departmentColumnTitleElement = screen.getByText(/部署/);
        expect(departmentColumnTitleElement).toBeInTheDocument();
        const staffColumnTitleElement = screen.getByText(/自社担当者/);
        expect(staffColumnTitleElement).toBeInTheDocument();
        const lastVisitColumnTitleElement = screen.getByText(/最終訪問日/);
        expect(lastVisitColumnTitleElement).toBeInTheDocument();
        const tagColumnTitleElement = screen.getByText(/タグ/);
        expect(tagColumnTitleElement).toBeInTheDocument();
        const categoryColumnTitleElement = screen.getByText(/相性/);
        expect(categoryColumnTitleElement).toBeInTheDocument();
        const preferenceColumnTitleElement = screen.getByText(/配信種別/);
        expect(preferenceColumnTitleElement).toBeInTheDocument();
        const areaColumnTitleElement = screen.getByText(/希望エリア/);
        expect(areaColumnTitleElement).toBeInTheDocument();
        const createdTimeColumnTitleElement = screen.getByText(/作成日時/);
        expect(createdTimeColumnTitleElement).toBeInTheDocument();
        const modifiedTimeColumnTitleElement = screen.getByText(/更新日時/);
        expect(modifiedTimeColumnTitleElement).toBeInTheDocument();
        for (const data of contactListContent) {
            const contactNameDataElement = screen.getByText(
                new RegExp(data.display_name)
            );
            expect(contactNameDataElement).toBeInTheDocument();
            const emailToDataElement = screen.getByText(new RegExp(data.email));
            expect(emailToDataElement).toBeInTheDocument();
            if (data.cc_mails && data.cc_mails.length) {
                for (const ccEmail of data.cc_mails) {
                    const ccEmailDataElement = screen.getByText(
                        new RegExp(ccEmail)
                    );
                    expect(ccEmailDataElement).toBeInTheDocument();
                }
            }
            if (data.tel1 && data.tel2 && data.tel3) {
                const telDataElement = screen.getByText(
                    new RegExp(`${data.tel1}-${data.tel2}-${data.tel3}`)
                );
                expect(telDataElement).toBeInTheDocument();
            }
            if (data.position) {
                const positionDataElement = screen.getByText(
                    new RegExp(data.position)
                );
                expect(positionDataElement).toBeInTheDocument();
            }
            if (data.department) {
                const departmentDataElement = screen.getByText(
                    new RegExp(data.department)
                );
                expect(departmentDataElement).toBeInTheDocument();
            }
            if (data.staff__last_name) {
                const staffNameDataElement = screen.getByText(
                    new RegExp(data.staff__last_name)
                );
                expect(staffNameDataElement).toBeInTheDocument();
            }
            if (data.last_visit) {
                const lastVisitDataElement = screen.getByText(
                    new RegExp(data.last_visit)
                );
                expect(lastVisitDataElement).toBeInTheDocument();
            }
            if (data.category) {
                const categoryData = data.category;
                const categoryTestId =
                    categoryData === "heart"
                        ? "contact-table-category-heart"
                        : "contact-table-category-frown";
                const categoryDataElement = screen.getByTestId(categoryTestId);
                expect(categoryDataElement).toBeInTheDocument();
            }
            for (const tag of data.tag_objects) {
                expect(
                    container.getElementsByClassName(`ant-tag-${tag.color}`)
                        .length
                ).toBe(1);
            }
            for (const preference of data.contactjobtypepreferences) {
                const preferenceValue = CONTACT_SEND_TYPE.find(
                    (value) => value.value === preference
                )?.value;
                if (preferenceValue) {
                    const preferenceDataElement = screen.getByText(
                        new RegExp(preferenceValue)
                    );
                    expect(preferenceDataElement).toBeInTheDocument();
                }
            }
            for (const [key, value] of Object.entries(data.contactpreference)) {
                if (value) {
                    const areaValue = WANTS_LOCATION.find(
                        (val) => val.value === key
                    )?.title;
                    if (areaValue) {
                        const areaDataElement = screen.getByText(
                            new RegExp(areaValue)
                        );
                        expect(areaDataElement).toBeInTheDocument();
                    }
                }
            }
            const createdTimeDataElement = screen.getByText(
                new RegExp(data.created_time)
            );
            expect(createdTimeDataElement).toBeInTheDocument();
            const modifiedTimeDataElement = screen.getByText(
                new RegExp(data.modified_time)
            );
            expect(modifiedTimeDataElement).toBeInTheDocument();
        }
    });
});
