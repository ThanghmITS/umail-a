import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import TableDisplaySettingModal from "~/components/Modals/TableDisplaySettingModal/TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./ContactTableDisplaySettingModal.scss";

const ContactTableDisplaySettingModal = () => {
    const fieldName = "contacts";
    const formId = "contactTableDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) && !_.isEmpty(displaySetting?.contacts);
        const tableHiddenColumns =
            doesDisplaySettingExist && displaySetting?.contacts.table;
        if (tableHiddenColumns) {
            setTargetKeys(tableHiddenColumns);
        }
        if (doesDisplaySettingExist && displaySetting?.contacts.page_size) {
            form.setFieldsValue({
                content: {
                    contacts: {
                        page_size: displaySetting.contacts.page_size,
                    },
                },
            });
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.setFieldsValue({
            content: {
                contacts: {
                    table: displaySetting?.contacts.table,
                    page_size: displaySetting?.contacts.page_size,
                },
            },
        });
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <TableDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.contacts.table,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default ContactTableDisplaySettingModal;
