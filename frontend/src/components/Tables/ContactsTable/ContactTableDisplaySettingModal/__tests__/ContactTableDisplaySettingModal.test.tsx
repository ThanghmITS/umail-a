import React from "react";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage } from "~/reducers/pages";
import { defaultInitialState as EditPageInitialState } from "~/reducers/Factories/editPage";
import { renderWithRedux, screen, userEvent } from "~/test/utils";
import ContactTableDisplaySettingModal from "../ContactTableDisplaySettingModal";
import { configureStore } from "@reduxjs/toolkit";
import { generateRandomToken } from "~/utils/utils";
import { mockLoginResponseData } from "~/test/mock/authAPIMock";

describe("ContactTableDisplaySettingModal.tsx", () => {
    test("render test", async () => {
        renderWithRedux(<ContactTableDisplaySettingModal />, {
            store: configureStore({
                reducer: {
                    login,
                    displaySettingPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            contacts: {
                                create: true,
                                update: true,
                                delete: true,
                                column_setting: true,
                                csv_upload: true,
                                csv_download: true,
                                search_template: true,
                            },
                        },
                    },
                    displaySettingPage: {
                        ...EditPageInitialState,
                        displaySetting:
                            mockLoginResponseData.userDisplaySetting
                                .content_hash,
                        isDisplaySettingLoading: false,
                        requireRefresh: false,
                    },
                },
            }),
        });
        const modalOpenButtonElement = screen.getByTestId(
            "table-display-setting-modal-info-icon"
        );
        expect(modalOpenButtonElement).toBeInTheDocument();
        await userEvent.click(modalOpenButtonElement);
        const contactNameOptionElement = await screen.findByText(
            /取引先担当者名/
        );
        expect(contactNameOptionElement).toBeInTheDocument();
        const assignedOrganizationNameOptionElement = await screen.findByText(
            /所属取引先/
        );
        expect(assignedOrganizationNameOptionElement).toBeInTheDocument();
        const emailToOptionElement = await screen.findByText(
            /メールアドレス\(TO\)/
        );
        expect(emailToOptionElement).toBeInTheDocument();
        const emailCcOptionElement = await screen.findByText(
            /メールアドレス\(CC\)/
        );
        expect(emailCcOptionElement).toBeInTheDocument();
        const telOptionElement = await screen.findByText(/TEL/);
        expect(telOptionElement).toBeInTheDocument();
        const positionOptionElement = await screen.findByText(/役職/);
        expect(positionOptionElement).toBeInTheDocument();
        const departmentOptionElement = await screen.findByText(/部署/);
        expect(departmentOptionElement).toBeInTheDocument();
        const staffOptionElement = await screen.findByText(/自社担当者/);
        expect(staffOptionElement).toBeInTheDocument();
        const lastVisitOptionElement = await screen.findByText(/最終訪問日/);
        expect(lastVisitOptionElement).toBeInTheDocument();
        const tagOptionElement = await screen.findByText(/タグ/);
        expect(tagOptionElement).toBeInTheDocument();
        const categoryOptionElement = await screen.findByText(/相性/);
        expect(categoryOptionElement).toBeInTheDocument();
        const preferenceOptionElement = await screen.findByText(/配信種別/);
        expect(preferenceOptionElement).toBeInTheDocument();
        const areaOptionElement = await screen.findByText(/希望エリア/);
        expect(areaOptionElement).toBeInTheDocument();
        const createdTimeOptionElement = await screen.findByText(/作成日時/);
        expect(createdTimeOptionElement).toBeInTheDocument();
        const modifiedTimeOptionElement = await screen.findByText(/更新日時/);
        expect(modifiedTimeOptionElement).toBeInTheDocument();
    });
});
