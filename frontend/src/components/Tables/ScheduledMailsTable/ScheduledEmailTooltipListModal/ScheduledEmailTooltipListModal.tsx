import React from "react";
import { Link } from "react-router-dom";
import { Button, Modal, Table } from "antd";
import styles from "./ScheduledEmailTooltipListModal.scss";
import Paths from "~/components/Routes/Paths";
import { ScheduledEmailTooltipModalTableDataSourceModel } from "~/models/scheduledEmailModel";

type Props = {
    title: string;
    isModalOpen: boolean;
    onModalClose: () => void;
    dataSource: ScheduledEmailTooltipModalTableDataSourceModel[];
};

const ScheduledEmailTooltipListModal = ({
    title,
    isModalOpen,
    onModalClose,
    dataSource,
}: Props) => {
    // TODO(joshua-hashimoto): APIからのデータに合わせて修正する
    const columns = [
        {
            title: "取引先",
            key: "organization",
            dataIndex: "organization",
            render: (
                text: string,
                record: ScheduledEmailTooltipModalTableDataSourceModel,
                index: number
            ) => (
                <Link to={`${Paths.organizations}/${record.organization.id}`}>
                    {record.organization.name}
                </Link>
            ),
        },
        {
            title: "担当者",
            key: "contact",
            dataIndex: "contact",
            render: (
                text: string,
                record: ScheduledEmailTooltipModalTableDataSourceModel,
                index: number
            ) => (
                <Link to={`${Paths.contacts}/${record.contact.id}`}>
                    {record.contact.name}
                </Link>
            ),
        },
    ];

    return (
        <Modal
            visible={isModalOpen}
            onCancel={onModalClose}
            closable={true}
            title={title}
            footer={[<Button onClick={onModalClose}>閉じる</Button>]}
            destroyOnClose>
            <Table
                columns={columns}
                dataSource={dataSource}
                scroll={{ y: 300 }}
                pagination={false}
            />
        </Modal>
    );
};

export default ScheduledEmailTooltipListModal;
