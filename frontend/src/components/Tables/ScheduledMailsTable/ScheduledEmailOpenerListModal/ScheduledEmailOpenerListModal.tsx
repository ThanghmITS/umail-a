import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Modal, Table, Tag, Button, Divider, Typography } from "antd";
import { ScheduledEmailOpenerListItemModel } from "~/models/scheduledEmailModel";
import { useFetchScheduledEmailOpenerListAPIQuery } from "~/hooks/useScheduledEmail";
import Paths from "~/components/Routes/Paths";
import _ from "lodash";
import getDateStr from "~/domain/date";
import styles from "./ScheduledEmailOpenerListModal.scss";

const { Text } = Typography;

type Props = {
    scheduledEmailId: string;
};

const ScheduledEmailOpenerListModal = ({ scheduledEmailId }: Props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { isLoading, data } = useFetchScheduledEmailOpenerListAPIQuery({
        options: { enabled: isModalOpen },
        scheduledEmailId,
    });

    const columns = [
        {
            title: "取引先",
            key: "organization",
            dataIndex: "organization",
            render: (
                text: string,
                record: ScheduledEmailOpenerListItemModel,
                index: number
            ) => (
                <Link to={`${Paths.organizations}/${record.organization.id}`}>
                    {record.organization.name}
                </Link>
            ),
        },
        {
            title: "担当者",
            key: "contact",
            dataIndex: "contact",
            render: (
                text: string,
                record: ScheduledEmailOpenerListItemModel,
                index: number
            ) => (
                <Link to={`${Paths.contacts}/${record.contact.id}`}>
                    {record.contact.name}
                </Link>
            ),
        },
        {
            title: "開封日時",
            key: "opened_time",
            dataIndex: "opened_time",
            render: (
                text: string,
                record: ScheduledEmailOpenerListItemModel,
                index: number
            ) => getDateStr(text),
        },
    ];

    return (
        <>
            <Button type="link" onClick={() => setIsModalOpen(true)}>
                開封者の一覧を表示
            </Button>
            <Modal
                visible={isModalOpen}
                onCancel={() => setIsModalOpen(false)}
                closable={true}
                title="メール開封者一覧"
                footer={[
                    <Button onClick={() => setIsModalOpen(false)}>
                        閉じる
                    </Button>,
                ]}
                destroyOnClose>
                <Table
                    loading={isLoading}
                    columns={columns}
                    dataSource={data?.results}
                    scroll={{ y: 300 }}
                    pagination={false}
                    rowKey={(record) => record.id}
                />
                <Divider plain>
                    &nbsp;
                    <Text>
                        開封情報取得期間: 配信完了日時から
                        <Tag style={{ marginLeft: 1, marginRight: 1 }}>
                            {data?.isOpenCountExtraPeriodAvailable
                                ? "30日間"
                                : "72時間"}
                        </Tag>
                        まで
                    </Text>
                    &nbsp;
                </Divider>
            </Modal>
        </>
    );
};

export default ScheduledEmailOpenerListModal;
