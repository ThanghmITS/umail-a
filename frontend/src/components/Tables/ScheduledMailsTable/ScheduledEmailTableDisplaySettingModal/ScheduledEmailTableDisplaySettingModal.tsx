import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import TableDisplaySettingModal from "~/components/Modals/TableDisplaySettingModal/TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./ScheduledEmailTableDisplaySettingModal.scss";

const ScheduledEmailTableDisplaySettingModal = () => {
    const fieldName = "scheduled_mails";
    const formId = "scheduledMailsTableDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) &&
            !_.isEmpty(displaySetting?.scheduled_mails);
        const tableHiddenColumns =
            doesDisplaySettingExist && displaySetting?.scheduled_mails.table;
        if (tableHiddenColumns) {
            setTargetKeys(tableHiddenColumns);
        }
        if (
            doesDisplaySettingExist &&
            displaySetting?.scheduled_mails.page_size
        ) {
            form.setFieldsValue({
                content: {
                    scheduled_mails: {
                        page_size: displaySetting.scheduled_mails.page_size,
                    },
                },
            });
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.setFieldsValue({
            content: {
                scheduled_mails: {
                    table: displaySetting?.scheduled_mails.table,
                    page_size: displaySetting?.scheduled_mails.page_size,
                },
            },
        });
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <TableDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.scheduled_mails.table,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default ScheduledEmailTableDisplaySettingModal;
