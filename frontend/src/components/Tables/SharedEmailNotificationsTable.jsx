import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import Path from "../Routes/Paths";
import GenericTable from "./GenericTable";

const targetDisplayNames = {
    from_address: "差出人",
    subject: "件名",
    content: "本文",
    attachment: "添付",
};

const conditionDisplayNames = {
    include: "に次を含む",
    exclude: "に次を含まない",
    equal: "が次と一致する",
    not_equalual: "が次と異なる",
    start_with: "が次で始まる",
    end_with: "が次で終わる",
    exists: "あり",
    not_exists: "なし",
};

const columns = [
    {
        title: "ルール名",
        dataIndex: "name",
        key: "name",
        width: 300,
    },
    {
        title: "対象",
        dataIndex: "target",
        key: "target",
        render: (value) => targetDisplayNames[value],
        width: 200,
    },
    {
        title: "条件",
        dataIndex: "condition",
        key: "condition",
        render: (value) => conditionDisplayNames[value],
        width: 200,
    },
    {
        title: "値",
        dataIndex: "value",
        key: "value",
        width: 200,
    },
];

const SharedEmailNotificationsTable = (props) => {
    const { history, loading, data } = props;
    const onRowClick = (record) =>
        history.push(`${Path.sharedMailNotifications}/${record.id}`);

    const [dataSource, setDataSource] = useState([]);

    useEffect(() => {
        if (data) {
            setDataSource(
                data.map((item) => {
                    if (item.children && !item.children.length) {
                        delete item.children;
                        return item;
                    }
                    return item;
                })
            );
        }
    }, [data]);

    return (
        <GenericTable
            {...props}
            data={dataSource}
            columns={columns}
            onRowClick={onRowClick}
            loading={loading}
            withSelection
            isTreeData
            scroll={{ x: "100%" }}
        />
    );
};

SharedEmailNotificationsTable.propTypes = {
    currentPage: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    totalCount: PropTypes.number.isRequired,
    sortKey: PropTypes.string,
    sortOrder: PropTypes.string,
    onPageChange: PropTypes.func.isRequired,
    onPageSizeChange: PropTypes.func.isRequired,
    onTableChange: PropTypes.func.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    loading: PropTypes.bool.isRequired,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.number.isRequired,
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            rules: PropTypes.arrayOf(
                PropTypes.shape({
                    key: PropTypes.number.isRequired,
                    type: PropTypes.string.isRequired,
                    value: PropTypes.string.isRequired,
                    target: PropTypes.string.isRequired,
                    condition: PropTypes.string.isRequired,
                })
            ).isRequired,
        })
    ),
};

SharedEmailNotificationsTable.defaultProps = {
    data: [],
    sortKey: undefined,
    sortOrder: undefined,
};

export default withRouter(SharedEmailNotificationsTable);
