import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import TableDisplaySettingModal from "~/components/Modals/TableDisplaySettingModal/TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./OrganizationsTableDisplaySettingModal.scss";

const OrganizationsTableDisplaySettingModal = () => {
    const fieldName = "organizations";
    const formId = "organizationsTableDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) &&
            !_.isEmpty(displaySetting?.organizations);
        const tableHiddenColumns =
            doesDisplaySettingExist && displaySetting?.organizations.table;
        if (tableHiddenColumns) {
            setTargetKeys(tableHiddenColumns);
        }
        if (
            doesDisplaySettingExist &&
            displaySetting?.organizations.page_size
        ) {
            form.setFieldsValue({
                content: {
                    organizations: {
                        page_size: displaySetting.organizations.page_size,
                    },
                },
            });
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.setFieldsValue({
            content: {
                organizations: {
                    table: displaySetting?.organizations.table,
                    page_size: displaySetting?.organizations.page_size,
                },
            },
        });
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <TableDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.organizations.table,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default OrganizationsTableDisplaySettingModal;
