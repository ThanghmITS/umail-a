import { camelToSnakeCase, createQueryString } from "../utils";

describe("utils.ts", () => {
    describe("camelToSnakeCase()", () => {
        test("pattern 1", () => {
            const target1 = "pageSize";
            const convertedTarget1 = camelToSnakeCase(target1);
            expect(convertedTarget1).toBe("page_size");
            const target2 = "TestCase";
            const convertedTarget2 = camelToSnakeCase(target2);
            expect(convertedTarget2).toBe("test_case");
            const target3 = "page";
            const convertedTarget3 = camelToSnakeCase(target3);
            expect(convertedTarget3).toBe("page");
            const target4 = "VALUE";
            const convertedTarget4 = camelToSnakeCase(target4);
            expect(convertedTarget4).toBe("v_a_l_u_e");
        });

        test("pattern 2", () => {
            const regularTextSnakeCased = camelToSnakeCase("string");
            expect(regularTextSnakeCased).toBe("string");
            const camelTextSnakeCased = camelToSnakeCase("pageSize");
            expect(camelTextSnakeCased).toBe("page_size");
            const numberTextSnakeCased = camelToSnakeCase(
                "additionalOrganization1"
            );
            expect(numberTextSnakeCased).toBe("additional_organization1");
            const multiCamelTextSnakeCased = camelToSnakeCase(
                "ThisIsMultipleCamelCaseText"
            );
            expect(multiCamelTextSnakeCased).toBe(
                "this_is_multiple_camel_case_text"
            );
        });
    });

    describe("createQueryString()", () => {
        test("pattern 1", () => {
            const query = {
                is_active: false,
                page: 5,
                pageSize: 110,
                value: "example",
                undefinedVal: undefined,
                nullVal: null,
            };
            const queryString = createQueryString(query);
            const queryStringList = queryString.split("&");
            let obj: { [key: string]: string } = {};
            for (const item of queryStringList) {
                const items = item.split("=");
                obj[items[0]] = items[1];
            }
            expect(String(query.is_active)).toBe(obj["is_active"]);
            expect(String(query.page)).toBe(obj["page"]);
            expect(String(query.pageSize)).toBe(obj["page_size"]);
            expect(String(query.value)).toBe(obj["value"]);
            expect(obj.hasOwnProperty("undefinedVal")).toBeFalsy();
            expect(obj.hasOwnProperty("nullVal")).toBeFalsy();
            expect(obj.hasOwnProperty("undefined_val")).toBeFalsy();
            expect(obj.hasOwnProperty("null_val")).toBeFalsy();
            expect(queryString).toBe(
                "is_active=false&page=5&page_size=110&value=example"
            );
        });

        test("pattern 2", () => {
            const queryParamObj = {
                text: "param1",
                search: "param2",
                isActive: true,
                value: undefined,
                name: null,
            };
            const queryString = createQueryString(queryParamObj);
            expect(queryString.includes("text")).toBeTruthy();
            expect(queryString.includes("param1")).toBeTruthy();
            expect(queryString.includes("search")).toBeTruthy();
            expect(queryString.includes("param2")).toBeTruthy();
            expect(queryString.includes("is_active")).toBeTruthy();
            expect(queryString.includes("true")).toBeTruthy();
            expect(queryString.includes("value")).toBeFalsy();
            expect(queryString.includes("undefined")).toBeFalsy();
            expect(queryString.includes("name")).toBeFalsy();
            expect(queryString.includes("null")).toBeFalsy();
            const equalStrings = queryString.split("=");
            expect(equalStrings.length).toBe(4);
            const ampersandStrings = queryString.split("&");
            // NOTE(joshua-hashimoto): "&"は各key=valueを繋ぐものなので、数は必ず"="の数-1になる
            expect(ampersandStrings.length).toBe(equalStrings.length - 1);
            const strings = queryString.replace(/=/g, "&").split("&");
            expect(strings.length).toBe(6);
        });
    });
});
