export type Override<T1, T2> = Omit<T1, keyof T2> & T2;

export type TAG_MODEL = {
    value: string;
    title: string;
    color: string;
};

export enum TYPE_SELECT {
    SKILL = "SKILL",
    TAG = "TAG"
}

export type LooseObject = {
  [key: string]: any
}