import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { FIX_CURRENT_SEARCH_CONDITIONS } from "~/actions/actionTypes";
import { CONTACT_SEARCH_PAGE } from "~/components/Pages/pageIds";
import getDateStr, { getDateStrByDateFormat } from "~/domain/date";
import {
    ContactListModel,
    ContactListResponseModel,
} from "~/models/contactModel";
import { RootState } from "~/models/store";

export const convertContactListResponseModelToContactListModel = (
    data: ContactListResponseModel
): ContactListModel => {
    return {
        id: data.id,
        display_name: data.display_name,
        email: data.email,
        cc_mails: data.cc_mails ?? [],
        cc_addresses__email: data.cc_mails.length
            ? data.cc_mails.join(",")
            : "",
        position: data.position ?? "",
        department: data.department ?? "",
        organization__name: data.organization__name,
        staff__last_name: data.staff__name,
        staff__name: data.staff__name,
        last_visit: getDateStrByDateFormat(data.last_visit),
        created_time: getDateStr(data.created_time),
        modified_time: getDateStr(data.modified_time),
        comments: data.comments ?? [],
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        contactjobtypepreferences: data.contactjobtypepreferences,
        contactpersonneltypepreferences: data.contactpersonneltypepreferences,
        contactpreference: data.contactpreference,
        tags: data.tags ?? [],
        tag_objects: data.tag_objects ?? [],
        category: data.category,
        is_ignored: data.is_ignored,
    };
};

export const useContactUtils = () => {
    const dispatch = useDispatch();
    const { currentSearchConditions } = useSelector(
        (state: RootState) => state.contactSearchPage
    );

    const deleteCurrentSearchConditionTags = (tagIds: string[]) => {
        const copiedConditions = { ...currentSearchConditions };
        const contactTags: string[] = copiedConditions.tags;
        const remainingTags = _.difference(contactTags, tagIds);
        copiedConditions.tags = remainingTags;
        dispatch({
            type: CONTACT_SEARCH_PAGE + FIX_CURRENT_SEARCH_CONDITIONS,
            payload: {
                fixedCurrentSearchConditions: copiedConditions,
            },
        });
    };

    return {
        deleteCurrentSearchConditionTags,
    };
};
