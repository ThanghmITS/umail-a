import { useCallback, useEffect, useMemo, useState } from "react";
import { useQueries, UseQueryResult } from "react-query";
import {
  customErrorMessage,
  customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
import {
  BoardChecklistItemCreateFormModel,
  BoardChecklistItemModel,
  BoardChecklistItemUpdateFormModel,
  BoardChecklistModel,
  BoardChecklistUpdateFormModel,
  BoardColumnModel,
  BoardCommentCreateFormModel,
  BoardCommentModel,
  BoardCommentUpdateFormModel,
  BoardDataModel,
  BoardModel,
  CardChangePositionRequestModel,
} from "~/models/boardCommonModel";
import {
  PersonnelBoardContractModel,
  PersonnelBoardContractUpdateFormModel,
  PersonnelBoardDetailModel,
  PersonnelBoardFormModel,
  PersonnelBoardListCardModel,
  PersonnelBoardTrainStationModel,
  PersonnelBoardSkillSheetModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { personnelBoardAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import moment, { Moment } from "moment";
import { formatDateReq, formatDateTime, formatDateEndReq } from "~/utils/utils";
import { useRecoilState } from "recoil";
import { personnelBoard } from "~/recoil/atom";
import { useFetchScheduledEmailSearchAPIQuery } from "./useScheduledEmail";
import { ScheduledMailSearchType } from "~/models/scheduledEmailModel";
import { ContactListModel } from "~/models/contactModel";
import { OrganizationListModel } from "~/models/organizationModel";
import { LazyQueryDataType, LazyQueryType, useLazyQuery } from "./useLazyQuery";
import { LooseObject } from "~/utils/types";

const defaultDataCardDetail: PersonnelBoardDetailModel = {
  id: "",
  order: 0,
  listId: "",
  assignees: [],
  priority: {
    title: "中",
    value: "urgent",
    color: "#EB5A46",
  },
  period: {
    end: "",
    start: "",
    isFinished: false,
  },
  lastNameInitial: "",
  firstNameInitial: "",
  skills: [],
  dynamicRows: [],
  contracts: [],
  scheduledEmailHistory: [],
  checklist: [],
  comments: [],
  isArchived: false,
};
export const usePersonnelBoardFetchListsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<BoardColumnModel[]>): UseQueryResult<
  BoardColumnModel[]
> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.lists,
    deps,
    options: {
      ...options,
    },
    apiRequest: personnelBoardAPI.fetchLists,
  });
};

export const usePersonnelBoardFetchListCardsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PaginationResponseModel<PersonnelBoardListCardModel[]>,
  PaginationResponseModel<PersonnelBoardListCardModel[]>,
  { listId: string }
>): UseQueryResult<PaginationResponseModel<PersonnelBoardListCardModel[]>> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.cards,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchListCards(query?.listId!),
  });
};

export const usePersonnelBoardFetchListCardsAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.sortAllCards);
};

export const usePersonnelBoardCreateCardAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.createCard, {
    onSuccess: (response) => {},
    onError: (err) => {
      customErrorMessage("姓名合わせて3文字以内で入力してください。");
    },
  });
};

export const usePersonnelBoardFetchCardAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PersonnelBoardDetailModel,
  PersonnelBoardDetailModel,
  { cardId: string }
>): UseQueryResult<PersonnelBoardDetailModel> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.card,
    deps,
    options: {
      ...options,
      select: (result) => {
        return {
          ...result,
          birthday: result.birthday ? moment(result.birthday) : "",
          period: {
            ...result.period,
            start: result?.period?.start ? moment(result?.period?.start) : "",
            end: result?.period?.end ? moment(result?.period?.end) : "",
          },
          operatePeriod: result.operatePeriod
            ? moment(result.operatePeriod)
            : "",
        };
      },
    },
    apiRequest: (query) => personnelBoardAPI.fetchCard(query?.cardId!),
  });
};

export const usePersonnelBoardUpdateCardAPIMutation = () => {
  type RequestModel = {
    cardId: string;
    postData: PersonnelBoardFormModel;
  };

  const apiRequest = ({ cardId, postData }: RequestModel) => {
    return personnelBoardAPI.updateCard(cardId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: () => {
      customSuccessMessage(SuccessMessages.generic.update);
    },
    onError: () => {
      customErrorMessage(ErrorMessages.generic.update);
    },
  });
};

export const usePersonnelBoardDuplicateCardAPIMutation = () => {

  const apiRequest = (body: PersonnelBoardCopyCardFormModel) => {
    return personnelBoardAPI.duplicateCard(body);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: () => {
      customSuccessMessage(SuccessMessages.generic.copy);
    },
    onError: () => {
      customErrorMessage(ErrorMessages.generic.copy);
    },
  });
};

export const useContractInformationAPIMutation = () => {
  type RequestModel = {
    postData: PersonnelBoardContractUpdateFormModel;
  };

  const apiRequest = ({ postData }: RequestModel) => {
    return personnelBoardAPI.createContract(postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: () => {
      customSuccessMessage(SuccessMessages.generic.update);
    },
    onError: () => {
      customErrorMessage(ErrorMessages.generic.update);
    },
  });
};

export const usePersonnelBoardUpdateCardOrderAPIMutation = () => {
  type RequestModel = {
    cardId: string;
    postData: {
      listId: string;
      position: number;
    };
  };

  const apiRequest = ({ cardId, postData }: RequestModel) => {
    return personnelBoardAPI.updateCardOrder(cardId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: () => {},
    onError: () => {},
  });
};

export const usePersonnelBoardDeleteCardAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.deleteCard, {
    onSuccess: (response) => {
      customSuccessMessage(SuccessMessages.generic.delete);
    },
    onError: (err) => {
      customErrorMessage(ErrorMessages.generic.delete);
    },
  });
};

export const usePersonnelBoardChangeCardPositionAPIMutation = () => {
  type RequestModel = {
    cardId: string;
    postData: CardChangePositionRequestModel;
  };

  const apiRequest = ({ cardId, postData }: RequestModel) => {
    return personnelBoardAPI.changeCardPosition(cardId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchChecklistsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardChecklistModel[],
  BoardChecklistModel[],
  { cardId: string }
>): UseQueryResult<BoardChecklistModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.checklists,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchChecklists(query?.cardId!),
  });
};

export const usePersonnelBoardCreateChecklistAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.createChecklist, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchChecklistAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardChecklistModel,
  BoardChecklistModel,
  { checklistId: string }
>): UseQueryResult<BoardChecklistModel> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.checklist,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) =>
      personnelBoardAPI.fetchChecklist(query?.checklistId!),
  });
};

export const usePersonnelBoardUpdateChecklistAPIMutation = () => {
  type RequestModel = {
    checklistId: string;
    postData: BoardChecklistUpdateFormModel;
  };

  const apiRequest = ({ checklistId, postData }: RequestModel) => {
    return personnelBoardAPI.updateChecklist(checklistId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardDeleteChecklistAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.deleteChecklist, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardDeleteSkillSheetAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.deleteSkillSheet, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchChecklistItemsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardChecklistItemModel[],
  BoardChecklistItemModel[],
  { checklistId: string }
>): UseQueryResult<BoardChecklistItemModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.checklistItems,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) =>
      personnelBoardAPI.fetchChecklistItems(query?.checklistId!),
  });
};

export const usePersonnelBoardCreateChecklistItemAPIMutation = () => {
  type RequestModel = {
    checklistId: string;
    postData: BoardChecklistItemCreateFormModel;
  };

  const apiRequest = ({ checklistId, postData }: RequestModel) => {
    return personnelBoardAPI.createChecklistItem(checklistId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchChecklistItemAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardChecklistItemModel,
  BoardChecklistItemModel,
  { checklistId: string; itemId: string }
>): UseQueryResult<BoardChecklistItemModel> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.checklistItem,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) =>
      personnelBoardAPI.fetchChecklistItem(query?.checklistId!, query?.itemId!),
  });
};

export const usePersonnelBoardUpdateChecklistItemAPIMutation = () => {
  type RequestModel = {
    checklistId: string;
    itemId: string;
    postData: BoardChecklistItemUpdateFormModel;
  };

  const apiRequest = ({ checklistId, itemId, postData }: RequestModel) => {
    return personnelBoardAPI.updateChecklistItem(checklistId, itemId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardDeleteChecklistItemAPIMutation = () => {
  type RequestModel = {
    checklistId: string;
    itemId: string;
  };

  const apiRequest = ({ checklistId, itemId }: RequestModel) => {
    return personnelBoardAPI.deleteChecklistItem(checklistId, itemId);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchCommentsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardCommentModel[],
  BoardCommentModel[],
  { cardId: string }
>): UseQueryResult<BoardCommentModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.comments,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchComments(query?.cardId!),
  });
};

export const usePersonnelBoardCreateCommentAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.createComment, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchCommentAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardCommentModel,
  BoardCommentModel,
  { commentId: string }
>): UseQueryResult<BoardCommentModel> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.comment,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchComment(query?.commentId!),
  });
};

export const usePersonnelBoardUpdateCommentAPIMutation = () => {
  type RequestModel = {
    commentId: string;
    postData: BoardCommentUpdateFormModel;
  };

  const apiRequest = ({ commentId, postData }: RequestModel) => {
    return personnelBoardAPI.updateComment(commentId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardDeleteCommentAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.deleteComment, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchSubCommentsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  BoardCommentModel[],
  BoardCommentModel[],
  { commentId: string }
>): UseQueryResult<BoardCommentModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.subComments,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) =>
      personnelBoardAPI.fetchSubComments(query?.commentId!),
  });
};

export const usePersonnelBoardFetchTrainStationsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PaginationResponseModel<PersonnelBoardTrainStationModel[]>,
  PaginationResponseModel<PersonnelBoardTrainStationModel[]>,
  { name: string }
>): UseQueryResult<
  PaginationResponseModel<PersonnelBoardTrainStationModel[]>
> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.trainStations,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchTrainStations(query?.name!),
  });
};

export const usePersonnelBoardFetchSkillSheetAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PersonnelBoardSkillSheetModel[],
  PersonnelBoardSkillSheetModel[],
  { cardId: string }
>): UseQueryResult<PersonnelBoardSkillSheetModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.skillSheet,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchSkillSheet(query?.cardId!),
  });
};

export const usePersonnelBoardFetchContractsAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PersonnelBoardContractModel[],
  PersonnelBoardContractModel[],
  { cardId: string }
>): UseQueryResult<PersonnelBoardContractModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.contracts,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchContracts(query?.cardId!),
  });
};

export const usePersonnelBoardCreateContractAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.createContract, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardFetchContractAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  PersonnelBoardContractModel,
  PersonnelBoardContractModel,
  { cardId: string }
>): UseQueryResult<PersonnelBoardContractModel> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.contract,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchContract(query?.cardId!),
  });
};

export const usePersonnelBoardFetchContactAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  ContactListModel[],
  ContactListModel[]
>): UseQueryResult<ContactListModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.contracts,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchContacts(),
  });
};

export const usePersonnelBoardFetchOrganizationAPIQuery = ({
  deps,
  options,
}: CustomUseQueryOptions<
  OrganizationListModel[],
  OrganizationListModel[]
>): UseQueryResult<OrganizationListModel[]> => {
  return useCustomQuery({
    queryKey: QueryKeys.personnelBoard.organization,
    deps,
    options: {
      ...options,
    },
    apiRequest: (query) => personnelBoardAPI.fetchOrganizations(),
  });
};

export const usePersonnelBoardUpdateContractAPIMutation = () => {
  type RequestModel = {
    contractId: string;
    postData: PersonnelBoardContractUpdateFormModel;
  };

  const apiRequest = ({ contractId, postData }: RequestModel) => {
    return personnelBoardAPI.updateContract(contractId, postData);
  };

  return useCustomMutation(apiRequest, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoardDeleteContractAPIMutation = () => {
  return useCustomMutation(personnelBoardAPI.deleteContract, {
    onSuccess: (response) => {},
    onError: (err) => {},
  });
};

export const usePersonnelBoard = () => {
  const [idCard, setIdCard] = useState<string>("");
  const {
    mutate: personnelBoardCreateCardMutate,
    isSuccess,
    isError,
    data,
  } = usePersonnelBoardCreateCardAPIMutation();

  useEffect(() => {
    if (data?.data?.id) {
      setIdCard(data?.data?.id);
    }
  }, [data]);

  useEffect(() => {
    if (isSuccess) {
      customSuccessMessage(SuccessMessages.generic.create);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      customErrorMessage(ErrorMessages.generic.create);
    }
  }, [isError]);

  return {
    idCard,
    personnelBoardCreateCardMutate,
    isSuccess,
  };
};

export const usePersonnelBoardDetailCard = () => {
  const [deps, setDeps] = useState<{ cardId: string }>({
    cardId: "",
  });

  const title = "Title";

  const [checkLists, setCheckLists] = useState<BoardChecklistModel[]>([]);
  const [personnelBoardState, setPersonnelBoardState] =
    useRecoilState(personnelBoard);

  const {
    isLoading,
    isSuccess,
    data: dataDetail,
    refetch: refetchFetchCard,
  } = usePersonnelBoardFetchCardAPIQuery({
    deps,
    options: {
      enabled: !!deps.cardId,
    },
  });

  const { data: dataCheckLists, refetch: refetchFetchCheckList } =
    usePersonnelBoardFetchChecklistsAPIQuery({
      deps,
      options: {
        enabled: !!deps.cardId,
      },
    });

  const { data: dataComments, refetch: refetchFetchComments } =
    usePersonnelBoardFetchCommentsAPIQuery({
      deps,
      options: {
        enabled: !!deps.cardId,
      },
    });

  const { data: skillSheets, refetch: refetchListSkillSheet } =
    usePersonnelBoardFetchSkillSheetAPIQuery({
      deps,
      options: {
        enabled: !!deps.cardId,
      },
    });

  const { mutate: personnelBoardCreateChecklistItemMutate } =
    usePersonnelBoardCreateChecklistItemAPIMutation();

  const { mutate: personnelBoardDeleteChecklistMutate } =
    usePersonnelBoardDeleteChecklistAPIMutation();

  const { mutate: personnelBoardUpdateChecklistItemMutate } =
    usePersonnelBoardUpdateChecklistItemAPIMutation();

  const { mutate: personnelBoardUpdateChecklistMutate } =
    usePersonnelBoardUpdateChecklistAPIMutation();

  const { mutate: personnelBoardCreateChecklistMutate } =
    usePersonnelBoardCreateChecklistAPIMutation();

  const { mutate: personnelBoardUpdateCardOrderMutate } =
    usePersonnelBoardUpdateCardOrderAPIMutation();

  const { mutate: personnelBoardDeleteSkillSheetMutate } =
    usePersonnelBoardDeleteSkillSheetAPIMutation();

  const { data: dataOrganizations, refetch: refetchFetchOrganizations } =
    usePersonnelBoardFetchOrganizationAPIQuery({
      options: {
        enabled: !!deps.cardId,
      },
    });

  const { data: dataContacts, refetch: refetchFetchContacts } =
    usePersonnelBoardFetchContactAPIQuery({
      options: {
        enabled: !!deps.cardId,
      },
    });

  useEffect(() => {
    if (deps && deps.cardId) {
      refetchFetchCard();
      refetchFetchComments();
      refetchFetchCheckList();
      refetchFetchOrganizations();
      refetchFetchContacts();
      refetchListSkillSheet();
    }
  }, [deps, deps.cardId]);

  useEffect(() => {
    if (dataCheckLists) {
      setCheckLists(dataCheckLists);
    }
  }, [dataCheckLists]);

  useEffect(() => {
    if (personnelBoardState && personnelBoardState.cardId) {
      setDeps({ cardId: personnelBoardState.cardId });
    }
  }, [personnelBoardState, personnelBoardState.cardId]);

  const personnelBoardUpdateChecklistItem = (
    postData: BoardChecklistItemUpdateFormModel,
    checklistId: string
  ) => {
    personnelBoardUpdateChecklistItemMutate(
      {
        checklistId,
        itemId: postData.id,
        postData,
      },
      {
        onSuccess: (res) => {
          const lists = checkLists.map((m) => ({
            ...m,
            items: m.items.map((i) => ({
              ...i,
              isFinished:
                i.id === res.data.id ? res.data.isFinished : i.isFinished,
            })),
          }));
          setCheckLists(lists);
        },
        onError: () => {
          customErrorMessage(ErrorMessages.generic.update);
        },
      }
    );
  };

  const personnelBoardUpdateChecklist = (
    postData: BoardChecklistUpdateFormModel,
    checklistId: string
  ) => {
    personnelBoardUpdateChecklistMutate(
      {
        checklistId,
        postData,
      },
      {
        onSuccess: (res) => {
          const lists = checkLists.map((m) => ({
            ...m,
            title: m.id === res.data.id ? res.data.title : m.title,
            showFinished:
              m.id === res.data.id ? res.data.showFinished : m.showFinished,
          }));
          setCheckLists(lists);
          // customSuccessMessage(SuccessMessages.generic.update)
        },
        onError: () => {
          customErrorMessage(ErrorMessages.generic.update);
        },
      }
    );
  };

  const personnelBoardDeleteChecklist = (checklistId: string) => {
    personnelBoardDeleteChecklistMutate(checklistId, {
      onSuccess: () => {
        const data = checkLists?.filter((f) => f.id !== checklistId);
        setCheckLists(data || []);
        customSuccessMessage(SuccessMessages.generic.delete);
      },
      onError: () => {
        customErrorMessage(ErrorMessages.generic.delete);
      },
    });
  };

  const personnelBoardDeleteSkillSheet = (fileId: string) => {
    personnelBoardDeleteSkillSheetMutate(fileId, {
      onSuccess: () => {
        customSuccessMessage(SuccessMessages.generic.delete);
      },
      onError: () => {
        customErrorMessage(ErrorMessages.generic.delete);
      },
    });
  };

  const personnelBoardCreateChecklistItem = (
    value: string,
    checklistId: string
  ) => {
    personnelBoardCreateChecklistItemMutate(
      {
        checklistId,
        postData: {
          content: value,
        },
      },
      {
        onSuccess: (res) => {
          const lists = checkLists.map((item) => ({
            ...item,
            items: item.items.concat(res.data),
          }));
          setCheckLists(lists);
          setPersonnelBoardState({
            ...personnelBoardState,
            isCreateCheckItem: false,
          });
          customSuccessMessage(SuccessMessages.generic.create);
        },
        onError: () => {
          customErrorMessage(ErrorMessages.generic.create);
        },
      }
    );
  };

  const personnelBoardCreateChecklist = (cardId: string) => {
    personnelBoardCreateChecklistMutate(
      {
        cardId,
        title,
      },
      {
        onSuccess: (res) => {
          const lists: BoardChecklistModel = {
            id: res.data.id,
            title,
            items: [],
            showFinished: false,
            isFinished: false,
          };
          setCheckLists(checkLists.concat(lists));
          customSuccessMessage(SuccessMessages.generic.create);
        },
        onError: () => {
          customErrorMessage(ErrorMessages.generic.create);
        },
      }
    );
  };

  const personnelBoardUpdateCardOrder = (
    cardId: string,
    listId: string,
    position: number
  ) => {
    personnelBoardUpdateCardOrderMutate(
      {
        cardId,
        postData: {
          listId,
          position,
        },
      },
      {
        onSuccess: (res) => {},
        onError: (err) => {},
      }
    );
  };

  return {
    checkLists,
    isLoading,
    isSuccess,
    dataDetail,
    defaultDataCardDetail,
    dataComments,
    dataContacts,
    dataOrganizations,
    skillSheets,
    refetchFetchCard,
    refetchFetchComments,
    personnelBoardDeleteChecklist,
    personnelBoardCreateChecklistItem,
    personnelBoardUpdateChecklistItem,
    personnelBoardUpdateChecklist,
    personnelBoardCreateChecklist,
    personnelBoardUpdateCardOrder,
    refetchFetchContacts,
    refetchFetchOrganizations,
    personnelBoardDeleteSkillSheet,
  };
};
export const useSearchScheduledMail = (params?: ScheduledMailSearchType) => {
  const { data: listsScheduledMails, isLoading } =
    useFetchScheduledEmailSearchAPIQuery({
      deps: {
        params,
      },
      options: {
        enabled: !!params,
      },
    });

  return {
    isLoading,
    listsScheduledMails,
  };
};
export const useUpdatePersonnelCardBoard = () => {
  const { mutate: updateCardMutate, data: dataUpdates } =
    usePersonnelBoardUpdateCardAPIMutation();
  const updatePersonnelCardBoard = (
    values: PersonnelBoardDetailModel,
    cardId: string
  ) => {
    updateCardMutate({
      cardId,
      postData: {
        assignees: values.assignees,
        firstNameInitial: values.firstNameInitial,
        id: cardId,
        isArchived: values.isArchived,
        lastNameInitial: values.lastNameInitial,
        listId: values.listId,
        order: values.order,
        period: {
          ...values.period,
          start: formatDateReq(values?.period?.start) || "",
          end: formatDateEndReq(values.period?.end) || "",
        },
        priority: values?.priority?.value || "",
        projects: [],
        skills: values.skills,
        affiliation: values.affiliation?.length
          ? values.affiliation
          : undefined,
        age: values.age,
        birthday: formatDateReq(values.birthday),
        dynamicRows: values.dynamicRows,
        firstName: values.firstName,
        gender: values.gender,
        image: values.image,
        isNegotiable: values.isNegotiable,
        lastName: values.lastName,
        operatePeriod: formatDateTime(values.operatePeriod),
        parallel: values.parallel,
        price: values.price,
        request: values.request,
        skillSheet: values.skillSheet,
        trainStation: values.trainStation,
      },
    });
  };

  const updateAssigneesCardBoard = (assignees: string[], cardId: string) => {
    updateCardMutate({
      cardId,
      postData: {
        assignees,
      },
    });
  };

  const updateDateTimeCardBoard = (
    startDate: Moment | string,
    endData: string | Moment,
    cardId: string
  ) => {
    const period = {
      start: formatDateReq(startDate),
      end: formatDateEndReq(endData),
    };
    updateCardMutate({
      cardId,
      postData: {
        period,
      },
    });
  };

  return {
    updatePersonnelCardBoard,
    updateAssigneesCardBoard,
    updateDateTimeCardBoard,
    dataUpdates,
  };
};

export const useDuplicatePersonnelCardBoard = () => {
  const { mutate: dupliateCardMutate, data: dupliateCardData } =
    usePersonnelBoardDuplicateCardAPIMutation();
  const duplicatePersonnelCardBoard = (cardData: PersonnelBoardCopyCardFormModel) => {
    dupliateCardMutate(cardData);
  };

  return {
    duplicatePersonnelCardBoard,
    dupliateCardData,
  };
};

export const useDeletePersonnelCardBoard = () => {
  const { mutate: deleteCardMutate, data: deleteCardData } =
    usePersonnelBoardDeleteCardAPIMutation();
  const deletePersonnelCardBoard = (
    cardId: string
  ) => {
    deleteCardMutate(cardId);
  };

  return {
    deletePersonnelCardBoard,
    deleteCardData,
  };
};

export const useUpdateContractInformation = () => {
  const { mutate: updateContractMutate } = useContractInformationAPIMutation();
  const updateContractInformation = (
    values: PersonnelBoardContractUpdateFormModel
  ) => {
    updateContractMutate({
      postData: {
        cardId: values.cardId,
        detail: values.detail,
        projectPeriod: values.projectPeriod, // NOTE(joshua-hashimoto): Form側はmoment？
        higherOrganization: values.higherOrganization,
        higherContact: values.higherContact,
        lowerOrganization: values.lowerOrganization,
        lowerContact: values.lowerContact,
        price: values.price,
      },
    });
  };
  return {
    updateContractInformation,
  };
};

export const useCommentPersonnelCardBoard = () => {
  const [valueTextAreaComment, setValueTextAreaComment] = useState<string>("");

  const [comments, setComments] = useState<BoardCommentModel[]>([]);

  const [personnelBoardState, setPersonnelBoardState] =
    useRecoilState(personnelBoard);

  const { mutate: createCommentMutate } =
    usePersonnelBoardCreateCommentAPIMutation();

  const { mutate: deleteCommentMutate } =
    usePersonnelBoardDeleteCommentAPIMutation();

  const { mutate: updateCommentMutate } =
    usePersonnelBoardUpdateCommentAPIMutation();

  const mutateCreateComment = (postData: BoardCommentCreateFormModel) => {
    createCommentMutate(postData, {
      onSuccess: (res) => {
        const arr = comments.concat([res.data]);
        setComments(arr);
        setValueTextAreaComment("");
      },
      onError: (error) => {
        customErrorMessage(error.response?.data.detail || "");
      },
    });
  };

  const mutateDeleteComment = (commentId: string, cardId: string) => {
    deleteCommentMutate(
      {
        cardId,
        commentId,
      },
      {
        onSuccess: () => {
          const arr = comments.filter((f) => f.id !== commentId);
          setComments(arr);
        },
        onError: (error) => {
          customErrorMessage(error.response?.data.detail || "");
        },
      }
    );
  };

  const mutatePinComment = (
    commentId: string,
    postData: BoardCommentUpdateFormModel
  ) => {
    updateCommentMutate(
      {
        commentId,
        postData,
      },
      {
        onSuccess: () => {
          setPersonnelBoardState({
            ...personnelBoardState,
            cardId: postData.id,
          });
        },
        onError: (error) => {
          customErrorMessage(error.response?.data.detail || "");
        },
      }
    );
  };

  const mutateEditComment = (
    commentId: string,
    postData: BoardCommentUpdateFormModel
  ) => {
    updateCommentMutate(
      {
        commentId,
        postData,
      },
      {
        onSuccess: (res) => {
          const arr = comments.map((item) => ({
            ...item,
            content: res.data.id === item.id ? res.data.content : item.content,
          }));
          setComments(arr);
        },
        onError: (error) => {
          customErrorMessage(error.response?.data.detail || "");
        },
      }
    );
  };
  return {
    comments,
    setComments,
    mutateCreateComment,
    valueTextAreaComment,
    setValueTextAreaComment,
    mutateDeleteComment,
    mutatePinComment,
    mutateEditComment,
  };
};

export const usePersonnelSettingBoard = () => {
  const [isInitialized, setInitialized] = useState(true);
  const [columns, setColumns] = useState<
    BoardDataModel<PersonnelBoardListCardModel>[] | undefined
  >([]);
  const { data: lists } = usePersonnelBoardFetchListsAPIQuery({});
  const { lazy } = usePersonnelBoardListCardsAPIQuery({});
  const { mutate: _sortMutate } = usePersonnelBoardFetchListCardsAPIMutation();

  const { mutate: moveAllCardMutate } =
    usePersonnelBoardMoveAllCardsAPIMutation();
  const { mutate: archiveAllCardMutate } =
    usePersonnelBoardArchiveAllCardsAPIMutation();

  const onSortAllCard = useCallback(
    async (listId: string, orderBy?: string) => {
      _sortMutate(`${listId}?ordering=${orderBy}&isArchived=False`, {
        onSuccess: (response) => {
          const _index = columns?.findIndex((column) => column?.id === listId)

          if (!_index || _index < 0) return;
          const _list = (columns ?? [])[_index] ?? {}
          const _columns = [...(columns ?? [])]

          _columns[_index] = {
            ..._list,
            cards: response?.data?.results ?? []
          }

          setColumns(_columns)
        }
      })

    },
    [_sortMutate, columns]
  );

  const _onGetBoard = useCallback(async () => {
    const _lists = lists?.filter((list) => list.id !== undefined)

    if (_lists?.length && isInitialized) {
      setInitialized(false);

      const _columns = await Promise.all(_lists.map(async(list) => {
        const _response = await lazy({ listId: list.id, isArchived: false });

        return {
          ...list,
          cards: _response?.results ?? []
        }

      }))

      
      if (_columns?.length) {
        setColumns(_columns)
      }
    }
  }, [isInitialized, lists, lazy]);

  const onMoveAllCard = useCallback(
    (listId: string, newListId: string) => {
      if (!newListId) return;

      moveAllCardMutate({ listId, newListId },  {
        onSuccess: () => setInitialized(true)
      });
    },
    [moveAllCardMutate]
  );

  const onRefetchBoard = useCallback(() => {
    setInitialized(true);
  }, [setInitialized]);

  const onArchiveAllCard = useCallback( (listId: string) =>  archiveAllCardMutate(listId, {
    onSuccess: () => setInitialized(true)
  }), [archiveAllCardMutate]);

  const _cards = useMemo(() => columns?.filter((data) => typeof data !== "undefined"), [columns])

  useEffect(() => {
    _onGetBoard()
  }, [_onGetBoard]);

  return {
    onRefetchBoard,
    onSortAllCard,
    onMoveAllCard,
    onArchiveAllCard,
    boardData: {
      columns: _cards ?? [],
    },
  };
};

export const usePersonnelBoardListCardsAPIQuery = ({
  options,
}: CustomUseQueryOptions<PaginationResponseModel<PersonnelBoardListCardModel[]>>): {
  lazy: LazyQueryType;
  data: LazyQueryDataType;
} => {
  return useLazyQuery({
    queryKey: QueryKeys.personnelBoard.sortCards,
    options: {
      ...options,
    },
    apiRequest: (params: LooseObject) =>
      personnelBoardAPI.fetchListCardWithouArchives(params?.listId, params?.isArchived),
    isAutoData: false,
  });
};

export const usePersonnelBoardMoveAllCardsAPIMutation = () =>
  useCustomMutation(personnelBoardAPI.moveAllCards);

export const usePersonnelBoardArchiveAllCardsAPIMutation = () =>
  useCustomMutation(personnelBoardAPI.archiveAllCards);
