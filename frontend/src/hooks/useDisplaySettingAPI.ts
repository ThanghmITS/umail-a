import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    displaySettingUpdateAction,
    fetchDisplaySettingAction,
} from "~/actions/data";
import { DISPLAY_SETTING_PAGE } from "~/components/Pages/pageIds";
import { Endpoint } from "~/domain/api";
import { DisplaySettingModel } from "~/models/displaySetting";
import { RootState } from "~/models/store";

export const useDisplaySettingAPI = () => {
    const pageId = DISPLAY_SETTING_PAGE;
    const resourceURL = `${Endpoint.getBaseUrl()}/${
        Endpoint.userDisplaySetting
    }`;
    const resourceId = "";

    const token = useSelector((state: RootState) => state.login.token);
    const dispatch = useDispatch();

    const fetchDisplaySetting = () => {
        dispatch(fetchDisplaySettingAction(pageId, token, resourceURL));
    };

    const convertResponseDataEntry = (data: any) => {
        // do not conversion
        return data;
    };

    const convertFormDataToAPI = (data: any) => {
        // do not conversion
        return data;
    };

    const updateDisplaySetting = (values: Partial<DisplaySettingModel>) => {
        dispatch(
            displaySettingUpdateAction(
                pageId,
                token,
                resourceURL,
                resourceId,
                convertFormDataToAPI(values),
                convertResponseDataEntry
            )
        );
    };

    useEffect(() => {
        fetchDisplaySetting();
    }, []);

    return {
        fetchDisplaySetting,
        updateDisplaySetting,
    };
};
