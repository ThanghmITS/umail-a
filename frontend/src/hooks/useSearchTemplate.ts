export const useSearchTemplateUtils = () => {

    const removeSearchConditions = () => {
        const prefixURLSearchContact = 'contacts';
        const prefixURLSearchOrganizations = 'organizations';
        const prefixURLSearchScheduledMails = 'scheduledMails';
        const targetPrefixes = [prefixURLSearchContact, prefixURLSearchOrganizations, prefixURLSearchScheduledMails];
        for (let [key, value] of Object.entries(localStorage)) {
            const targetKey = /^(?!str_.*_selected).+$/
            for (let prefix of targetPrefixes) {
              if (key.includes(prefix + "_") && key.match(targetKey)) {
                localStorage.removeItem(key)
              }
            }
        }
    }

    const saveCurrentSearchCondition = (target: string, keyName: string, values: any) => {
        const savingValues = JSON.stringify(values)
        let savingKey = target + "_default"
        if (keyName) {
            savingKey = target + '_' + keyName
        }
        localStorage.setItem(savingKey, savingValues);
    }

    return {
        removeSearchConditions,
        saveCurrentSearchCondition
    }
} 