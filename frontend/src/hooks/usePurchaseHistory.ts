import {
    PurchaseHistoryModel,
    PurchaseHistoryResponseModel,
} from "~/models/purchaseHistory";
import labelmake from "labelmake";
import FileDownload from "js-file-download";
import ReceiptPdf from "~/utils/receiptPdf.js";
import { Template } from "labelmake/dist/types/type";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { useCustomQuery } from "./useCustomQuery";
import {
    DEFAULT_PAGE_SIZE,
    ErrorMessages,
    QueryKeys,
    SuccessMessages,
} from "~/utils/constants";
import { UseQueryResult } from "react-query";
import { purchaseHistoryAPI } from "~/networking/api";
import { PaginationRequestModel } from "~/models/requestModel";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const convertPurchaseHistoryResponseModelToPurchaseHistoryModel = (
    data: PurchaseHistoryResponseModel
): PurchaseHistoryModel => {
    return {
        periodStart: data.period_start,
        periodEnd: data.period_end,
        planName: data.plan_name,
        optionNames: data.option_names,
        cardLast4: data.card_last4,
        createdTime: data.created_time,
        price: data.price,
        id: data.id,
        methodName: data.method_name,
    };
};

export const useFetchPurchaseHistoryAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PaginationResponseModel<PurchaseHistoryResponseModel[]>,
    PaginationResponseModel<PurchaseHistoryModel[]>,
    PaginationRequestModel
>): UseQueryResult<PaginationResponseModel<PurchaseHistoryModel[]>> => {
    return useCustomQuery({
        queryKey: QueryKeys.purchaseHistory.purchaseHistories,
        deps,
        options: {
            onError: (err) => {
                if (err.response?.data.detail) {
                    const errorMessage = err.response.data.detail;
                    customErrorMessage(errorMessage);
                }
            },
            ...options,
            keepPreviousData: true,
            select: (result) => {
                return {
                    ...result,
                    results: result.results.map((item) =>
                        convertPurchaseHistoryResponseModelToPurchaseHistoryModel(
                            item
                        )
                    ),
                };
            },
        },
        apiRequest: (queryKey) =>
            purchaseHistoryAPI.fetchPurchaseHistory(
                queryKey ?? { page: 1, pageSize: DEFAULT_PAGE_SIZE, value: "" }
            ),
    });
};

export const usePurchaseHistory = () => {
    const pdfHeaderX = 250;
    const pdfTableX = 50;
    const defaultValueHeight = 30;
    const receiptPriceWidth = 22;

    const generatePurchaseReceipt = async (
        receipt: PurchaseHistoryModel
    ): Promise<Blob | undefined> => {
        try {
            const monospaceMediumTtfResponse = await fetch(
                "static/app_staffing/fonts/GenShinGothic/MonospaceMedium.ttf"
            );
            const MonospaceMedium =
                await monospaceMediumTtfResponse.arrayBuffer();
            const monospaceLightTtfResponse = await fetch(
                "static/app_staffing/fonts/GenShinGothic/MonospaceLight.ttf"
            );
            const MonospaceLight =
                await monospaceLightTtfResponse.arrayBuffer();
            const font = { MonospaceMedium, MonospaceLight };
            const template: Template = {
                fontName: "MonospaceLight",
                basePdf: ReceiptPdf,
                schemas: [
                    {
                        receiptId: {
                            type: "text",
                            position: { x: pdfHeaderX, y: 19 },
                            width: 100,
                            height: defaultValueHeight,
                            fontSize: 8,
                            fontName: "MonospaceMedium",
                        },
                        paymentDate: {
                            type: "text",
                            position: { x: pdfHeaderX, y: 25 },
                            width: 100,
                            height: defaultValueHeight,
                            fontSize: 8,
                            fontName: "MonospaceMedium",
                        },
                        paymentMethod: {
                            type: "text",
                            position: { x: pdfTableX, y: 62 },
                            width: 100,
                            height: defaultValueHeight,
                        },
                        price: {
                            type: "text",
                            position: { x: pdfTableX, y: 73 },
                            width: receiptPriceWidth,
                            height: defaultValueHeight,
                        },
                        pricePrefix: {
                            type: "text",
                            position: {
                                x: pdfTableX + receiptPriceWidth,
                                y: 75,
                            },
                            width: 25,
                            height: defaultValueHeight,
                            fontSize: 7,
                        },
                        period: {
                            type: "text",
                            position: { x: pdfTableX, y: 83 },
                            width: 160,
                            height: defaultValueHeight,
                        },
                        detail: {
                            type: "text",
                            position: { x: pdfTableX, y: 99 },
                            width: 160,
                            height: 90,
                        },
                    },
                ],
            };
            let detailString = "";
            const planName = receipt.planName;
            const optionNames = receipt.optionNames;
            if (planName) {
                detailString += planName + "プラン\n";
            }
            if (optionNames.length) {
                detailString += optionNames.join("・");
            }
            // NOTE(joshua-hashimoto): お支払い方法に関しては、クレジットカード以外のお支払い方法が追加されたらAPIから支払い方法を文字列で返却してもらう必要がある
            const inputs: {
                [key: string]: string;
            }[] = [
                {
                    receiptId: receipt.id,
                    paymentDate: receipt.periodStart,
                    paymentMethod: `**** **** **** ${receipt.cardLast4}（${receipt.methodName})`,
                    price: `¥${receipt.price.toLocaleString()}`,
                    pricePrefix: "（税込)",
                    period: `${receipt.periodStart} ~ ${receipt.periodEnd}`,
                    detail: detailString,
                },
            ];
            const pdf = await labelmake({ template, inputs, font });
            const blob: Blob = new Blob([pdf.buffer], {
                type: "application/pdf",
            });
            return blob;
        } catch (err) {
            console.error(err);
            customErrorMessage(ErrorMessages.purchaseHistory.download);
            return undefined;
        }
    };

    const downloadPurchaseReceipt = (
        pdf: Blob,
        filename: string = "コモレビ_領収書.pdf"
    ) => {
        FileDownload(pdf, filename);
        customSuccessMessage(SuccessMessages.purchaseHistory.download);
    };

    return {
        generatePurchaseReceipt,
        downloadPurchaseReceipt,
    };
};
