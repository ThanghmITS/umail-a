import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { AuthActions } from "~/actionCreators/authActions";
import { DisplaySettingActions } from "~/actionCreators/displaySettingActions";
import {
    LoginAPIResponseModel,
    LoginResponseCheckModel,
    LoginResponseModel,
} from "~/models/authModel";
import { useTypedSelector } from "~/models/store";
import { authAPI } from "~/networking/api";
import { ErrorMessages } from "~/utils/constants";
import { clearCacheData, isEmpty } from "~/utils/utils";
import { useClient } from "./useClient";
import { useCustomMutation } from "./useCustomMutation";
import { useIntercom } from "./useIntercom";
import Paths from "~/components/Routes/Paths";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";

export const convertLoginAPIResponseModelToLoginResponseModel = (
    data: LoginAPIResponseModel
): LoginResponseModel => {
    return {
        token: data.token,
        userId: data.user_id,
        displayName: data.display_name,
        isUserAdmin: data.is_user_admin,
        avatar: data.avatar,
        role: data.role,
        authorizedActions: data.authorized_actions,
        intercomUserHash: data.intercom_user_hash,
        userDisplaySetting: data.user_display_setting,
    };
};

export const doesRequiredFieldsExists = ({
    token,
    userId,
    displayName,
    isUserAdmin,
    avatar,
    role,
    authorizedActions,
    intercomUserHash,
    userDisplaySetting,
}: LoginResponseCheckModel) => {
    if (!token) {
        return false;
    }
    if (!userId) {
        return false;
    }
    if (!displayName) {
        return false;
    }
    if (isUserAdmin === undefined || isUserAdmin === null) {
        return false;
    }
    if (!role) {
        return false;
    }
    if (!authorizedActions || isEmpty(authorizedActions)) {
        return false;
    }
    if (!intercomUserHash) {
        return false;
    }
    // NOTE(): avatarはundefinedもしくは空文字列にもなるので、ここでチェックする必要はない
    if (
        !userDisplaySetting ||
        isEmpty(userDisplaySetting) ||
        !userDisplaySetting.content_hash ||
        isEmpty(userDisplaySetting.content_hash)
    ) {
        return false;
    }
    return true;
};

export const useLogoutAPIMutation = () => {
    const router = useHistory();
    const dispatch = useDispatch();
    const { removeTokenFromClient } = useClient();
    const { shutdownIntercom } = useIntercom();

    return useCustomMutation(authAPI.logout, {
        onMutate: (...args) => {
            dispatch(AuthActions.loggingOutAction());
        },
        onSuccess: (response) => {},
        onError: (err) => {
            // NOTE(joshua-hashimoto): ログアウトの場合はエラーでもログアウト処理を実行するので、ハンドリングする必要はない
        },
        onSettled: () => {
            dispatch(AuthActions.logoutAction());
            removeTokenFromClient();
            shutdownIntercom();
            clearCacheData();
            router.push(Paths.login);
        },
    });
};

export const useLoginAPIMutation = () => {
    const dispatch = useDispatch();
    const { setupIntercom } = useIntercom();
    const { setTokenToClient } = useClient();
    const { mutate: logout } = useLogoutAPIMutation();
    const { threeLogin } = useTypedSelector((state) => state.login);

    return useCustomMutation(authAPI.login, {
        onSuccess: (response) => {
            const data = response.data;
            const result =
                convertLoginAPIResponseModelToLoginResponseModel(data);
            const resultIsClean = doesRequiredFieldsExists(result);
            if (!resultIsClean) {
                customErrorMessage(ErrorMessages.auth.authorizedActions);
                logout();
                return;
            }
            setTokenToClient(result.token);
            setupIntercom(result);
            dispatch(AuthActions.loginAction(result));
            dispatch(
                DisplaySettingActions.loadedAction({
                    data: result.userDisplaySetting,
                })
            );
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.auth.login;
            if (err.response?.status === 400) {
              if(err.response?.data?.detail) {
                errorMessage = err.response?.data?.detail;
              }
              else {
                errorMessage = ErrorMessages.auth.invalidCredentials;
              }
            }
            customErrorMessage(errorMessage);
            dispatch(AuthActions.loginErrorAction({ threeLogin, error: err }));
        },
    });
};
