import { ScheduledEmailSettingResponseModel } from "~/models/scheduledEmailSettingModel";
import { v4 as uuidv4 } from "uuid";
import { convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel } from "../useScheduledEmailSetting";
import getDateStr from "~/domain/date";

describe("useScheduledEmailSetting.ts", () => {
    describe("convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel", () => {
        test("convert test", () => {
            const companyId = uuidv4();
            const userId = uuidv4();
            const apiResponseMockData: ScheduledEmailSettingResponseModel = {
                file_type: 2,
                company: companyId,
                connection_type: 1,
                hostname: "example.com",
                is_open_count_available: true,
                is_open_count_extra_period_available: true,
                is_use_attachment_max_size_available: true,
                is_use_delivery_interval_available: true,
                is_use_remove_promotion_available: true,
                modified_time: "2022-03-02T17:32:52.348962+09:00",
                modified_user: userId,
                modified_user__name: "example master",
                password: "Abcd1234",
                port_number: 587,
                target_count_addon_purchase_count: 4,
                use_attachment_max_size: true,
                use_open_count: true,
                use_open_count_extra_period: true,
                use_remove_promotion: true,
                username: "example_user",
            };
            const convertedData =
                convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel(
                    apiResponseMockData
                );
            expect(convertedData.company).toBe(apiResponseMockData.company);
            expect(convertedData.connection_type).toBe(
                apiResponseMockData.connection_type
            );
            expect(convertedData.hostname).toBe(apiResponseMockData.hostname);
            expect(convertedData.is_open_count_available).toBe(
                apiResponseMockData.is_open_count_available
            );
            expect(convertedData.is_open_count_extra_period_available).toBe(
                apiResponseMockData.is_open_count_extra_period_available
            );
            expect(convertedData.is_use_attachment_max_size_available).toBe(
                apiResponseMockData.is_use_attachment_max_size_available
            );
            expect(convertedData.is_use_delivery_interval_available).toBe(
                apiResponseMockData.is_use_delivery_interval_available
            );
            expect(convertedData.is_use_remove_promotion_available).toBe(
                apiResponseMockData.is_use_remove_promotion_available
            );
            expect(convertedData.modified_time).toBe(
                getDateStr(apiResponseMockData.modified_time)
            );
            expect(convertedData.modified_user).toBe(
                apiResponseMockData.modified_user__name
            );
            expect(convertedData.password).toBe(apiResponseMockData.password);
            expect(convertedData.port_number).toBe(
                apiResponseMockData.port_number
            );
            expect(convertedData.target_count_addon_purchase_count).toBe(
                apiResponseMockData.target_count_addon_purchase_count
            );
            expect(convertedData.use_attachment_max_size).toBe(
                apiResponseMockData.use_attachment_max_size
            );
            expect(convertedData.use_open_count).toBe(
                apiResponseMockData.use_open_count
            );
            expect(convertedData.use_open_count_extra_period).toBe(
                apiResponseMockData.use_open_count_extra_period
            );
            expect(convertedData.use_remove_promotion).toBe(
                apiResponseMockData.use_remove_promotion
            );
            expect(convertedData.username).toBe(apiResponseMockData.username);
        });
    });
});
