import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter, MemoryRouter } from "react-router-dom";
import { createQueryString } from "~/utils/utils";
import { renderHook } from "@testing-library/react-hooks";
import { useSearchSync } from "../useSearch";
import { renderWithBrowserRouter } from "~/test/utils";

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useLocation: jest.fn().mockReturnValue({
        pathname: "/",
        search: "",
        hash: "",
        state: null,
        key: "",
    }),
}));

describe("useSearch.ts", () => {
    test("syncToUrl", async () => {
        // NOTE(joshua-hashimoto): テスト方法がわからない。もう少し調査しなければならない
        // TODO(joshua-hashimoto): テストの追加
    });

    test("queryParamsToObj()", () => {
        const expectedParamsObj = {
            value: "text",
            pageSize: 100,
            tags: ["django", "react"],
        };
        const queryString = createQueryString(expectedParamsObj);
        const { result, waitFor } = renderHook(() => useSearchSync(), {
            wrapper: ({ children }) => (
                <MemoryRouter
                    initialEntries={[
                        { pathname: "/", search: "?" + queryString },
                    ]}>
                    {children}
                </MemoryRouter>
            ),
        });
        const { queryParamsToObj } = result.current;
        const value = queryParamsToObj();
        expect(value).toMatchObject({
            value: "text",
            page_size: 100,
            tags: ["django", "react"],
        });
    });
});
