import { renderHook } from "~/test/utils";
import { generateRandomToken } from "~/utils/utils";
import { useClient } from "../useClient";

describe("useClient.ts", () => {
    let { result, waitFor } = renderHook(() => useClient());

    beforeEach(() => {
        result = renderHook(() => useClient()).result;
        waitFor = renderHook(() => useClient()).waitFor;
    });

    describe("setTokenToClient", () => {
        test("can set token as expected", () => {
            const token = generateRandomToken();
            const { client, setTokenToClient } = result.current;
            setTokenToClient(token);
            expect(client.defaults.headers.common["Authorization"]).toBe(
                `Token ${token}`
            );
        });
    });

    describe("removeTokenFromClient", () => {
        test("can remove token as expected", () => {
            const { client, removeTokenFromClient } = result.current;
            removeTokenFromClient();
            expect(
                client.defaults.headers.common["Authorization"]
            ).toBeUndefined();
        });
    });
});
