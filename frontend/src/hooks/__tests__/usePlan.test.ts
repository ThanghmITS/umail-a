import {
    act,
    mockHistoryPush,
    QueryClientWrapper,
    renderHook,
    screen,
} from "~/test/utils";
import {
    mockPlanAddAccountLimitFailureAPIRoute,
    mockPlanDeleteAccountLimitFailureAPIRoute,
    mockPlanFetchUserInfoFailureAPIRoute,
    mockPlanRegisterFailureAPIRoute,
    mockPlanSummaryAPIResponseData,
    mockPlanSummaryExpiredFreeTrialSuccessAPIRoute,
    mockPlanSummaryExpiredPayedPlanSuccessAPIRoute,
    mockPlanSummaryFreeTrialSuccessAPIRoute,
    mockPlanSummaryLightPlanSuccessAPIRoute,
    mockPlanSummaryProfessionalPlanSuccessAPIRoute,
    mockPlanSummaryStandardPlanSuccessAPIRoute,
    mockPlanUserInfoAPIResponseData,
} from "~/test/mock/planAPIMock";
import {
    convertPlanSummaryResponseModelToPlanSummaryModel,
    convertPlanUserDataAPIModelToPlanUserDataModel,
    useFetchUserPlanInfoAPIQuery,
    useGuardPlanSummary,
    usePlanAddAccountLimitAPIMutation,
    usePlanRegisterPlanMutation,
    usePlanRemoveAccountLimitAPIMutation,
    usePlans,
    usePlanSummaryAPIQuery,
} from "../usePlan";
import { mockServer } from "~/test/setupTests";
import Paths from "~/components/Routes/Paths";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { useClient } from "../useClient";
import { generateRandomToken } from "~/utils/utils";

describe("usePlan.ts", () => {
    test("convertPlanUserDataAPIModelToPlanUserDataModel()", () => {
        const originalData = { ...mockPlanUserInfoAPIResponseData };
        const convertedData =
            convertPlanUserDataAPIModelToPlanUserDataModel(originalData);
        expect(convertedData.id).toBe(originalData.id);
        expect(convertedData.currentUserCount).toBe(
            originalData.current_user_count
        );
        expect(convertedData.defaultUserCount).toBe(
            originalData.default_user_count
        );
        expect(convertedData.expirationDate).toBe(originalData.expiration_date);
        expect(convertedData.planMasterId).toBe(originalData.plan_master_id);
        expect(convertedData.userRegistrationLimit).toBe(
            originalData.user_registration_limit
        );
    });

    describe("useFetchUserPlanInfoAPIQuery()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchUserPlanInfoAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const expectedData = convertPlanUserDataAPIModelToPlanUserDataModel(
                mockPlanUserInfoAPIResponseData
            );
            expect(result.current.data).toMatchObject(expectedData);
        });

        test("error", async () => {
            mockServer.use(mockPlanFetchUserInfoFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () =>
                    useFetchUserPlanInfoAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageRegex = new RegExp(ErrorMessages.plan.planInfo);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("usePlanRegisterPlanMutation()", () => {
        beforeAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() =>
                result.current.setTokenToClient(generateRandomToken())
            );
        });

        afterAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() => result.current.removeTokenFromClient());
        });

        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => usePlanRegisterPlanMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate({ plan_master_id: 1 });
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const successMessageRegex = new RegExp(
                SuccessMessages.plan.purchase
            );
            const successMessageElement = await screen.findByText(
                successMessageRegex
            );
            expect(successMessageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockPlanRegisterFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlanRegisterPlanMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate({ plan_master_id: 1 });
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageRegex = new RegExp(ErrorMessages.plan.purchase);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("usePlanAddAccountLimitAPIMutation()", () => {
        beforeAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() =>
                result.current.setTokenToClient(generateRandomToken())
            );
        });

        afterAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() => result.current.removeTokenFromClient());
        });

        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => usePlanAddAccountLimitAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const successMessageRegex = new RegExp(
                SuccessMessages.plan.purchase
            );
            const successMessageElement = await screen.findByText(
                successMessageRegex
            );
            expect(successMessageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockPlanAddAccountLimitFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlanAddAccountLimitAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageRegex = new RegExp(ErrorMessages.plan.addUser);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("usePlanRemoveAccountLimitAPIMutation()", () => {
        beforeAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() =>
                result.current.setTokenToClient(generateRandomToken())
            );
        });

        afterAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() => result.current.removeTokenFromClient());
        });

        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => usePlanRemoveAccountLimitAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const successMessageRegex = new RegExp(
                SuccessMessages.plan.removeUser
            );
            const successMessageElement = await screen.findByText(
                successMessageRegex
            );
            expect(successMessageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockPlanDeleteAccountLimitFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlanRemoveAccountLimitAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageRegex = new RegExp(ErrorMessages.plan.removeUser);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    test("convertPlanSummaryResponseModelToPlanSummaryModel()", () => {
        const originalData = { ...mockPlanSummaryAPIResponseData };
        const convertedData =
            convertPlanSummaryResponseModelToPlanSummaryModel(originalData);
        expect(convertedData.planId).toBe(originalData.plan_id);
        expect(convertedData.planName).toBe(originalData.plan_name);
        expect(convertedData.remainingDays).toBe(originalData.remaining_days);
        expect(convertedData.paymentErrorExists).toBe(
            originalData.payment_error_exists
        );
        expect(convertedData.paymentErrorCardIds).toMatchObject(
            originalData.payment_error_card_ids
        );
    });

    describe("usePlanSummaryAPIQuery", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    usePlanSummaryAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const expectedData =
                convertPlanSummaryResponseModelToPlanSummaryModel(
                    mockPlanSummaryAPIResponseData
                );
            expect(result.current.data).toMatchObject(expectedData);
        });
    });

    describe("usePlans()", () => {
        test("free trial", async () => {
            mockServer.use(mockPlanSummaryFreeTrialSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlans({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const {
                isFreeTrial,
                isLightPlan,
                isStandardPlan,
                isProfessionalPlan,
                isPayedPlan,
            } = result.current;
            expect(isFreeTrial).toBeTruthy();
            expect(isLightPlan).toBeFalsy();
            expect(isStandardPlan).toBeFalsy();
            expect(isProfessionalPlan).toBeFalsy();
            expect(isPayedPlan).toBeFalsy();
        });

        test("light plan", async () => {
            mockServer.use(mockPlanSummaryLightPlanSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlans({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const {
                isFreeTrial,
                isLightPlan,
                isStandardPlan,
                isProfessionalPlan,
                isPayedPlan,
            } = result.current;
            expect(isFreeTrial).toBeFalsy();
            expect(isLightPlan).toBeTruthy();
            expect(isStandardPlan).toBeFalsy();
            expect(isProfessionalPlan).toBeFalsy();
            expect(isPayedPlan).toBeTruthy();
        });

        test("standard plan", async () => {
            mockServer.use(mockPlanSummaryStandardPlanSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlans({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const {
                isFreeTrial,
                isLightPlan,
                isStandardPlan,
                isProfessionalPlan,
                isPayedPlan,
            } = result.current;
            expect(isFreeTrial).toBeFalsy();
            expect(isLightPlan).toBeFalsy();
            expect(isStandardPlan).toBeTruthy();
            expect(isProfessionalPlan).toBeFalsy();
            expect(isPayedPlan).toBeTruthy();
        });

        test("professional plan", async () => {
            mockServer.use(mockPlanSummaryProfessionalPlanSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePlans({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const {
                isFreeTrial,
                isLightPlan,
                isStandardPlan,
                isProfessionalPlan,
                isPayedPlan,
            } = result.current;
            expect(isFreeTrial).toBeFalsy();
            expect(isLightPlan).toBeFalsy();
            expect(isStandardPlan).toBeTruthy();
            expect(isProfessionalPlan).toBeTruthy();
            expect(isPayedPlan).toBeTruthy();
        });
    });

    describe("useGuardPlanSummary()", () => {
        test("passes guard", async () => {
            const { result, waitFor } = renderHook(
                () => useGuardPlanSummary({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const { isNotValidUser } = result.current;
            expect(isNotValidUser).toBeFalsy();
        });

        test("guard expired free trial", async () => {
            mockServer.use(mockPlanSummaryExpiredFreeTrialSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => useGuardPlanSummary({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const { isNotValidUser } = result.current;
            expect(isNotValidUser).toBeTruthy();
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.plan);
        });

        test("guard expired payed plan", async () => {
            mockServer.use(mockPlanSummaryExpiredPayedPlanSuccessAPIRoute);
            const { result, waitFor } = renderHook(
                () => useGuardPlanSummary({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            const { isNotValidUser } = result.current;
            expect(isNotValidUser).toBeTruthy();
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.payment);
        });
    });
});
