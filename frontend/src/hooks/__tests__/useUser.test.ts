import { convertUserSearchPageResponseDataEntry } from "~/components/Pages/UserSearchPage";
import getDateStr from "~/domain/date";
import { UserListItemAPIModel } from "~/models/userModel";
import { userFormData } from "~/test/mock/userAPIMock";
import { QueryClientWrapper, renderHook } from "~/test/utils";
import { randString } from "~/utils/utils";
import {
    convertUserRegisterFormModelToUserRegisterRequestDataModel,
    convertUserFormModelToUserUpdateRequestDataModel,
    convertUserListItemAPIModelToUserListItemModel,
    useFetchUsersAPIQuery,
} from "../useUser";

/**
 * API response from /app_staffing/users?inactive_filter=use_filter&page=1&page_size=10
 *
 * display_name: "example admin"
 * editable: true
 * email: "admin@example.com"
 * email_signature: "===========\nThe admin\n==========="
 * first_name: "admin"
 * id: "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb"
 * is_active: true
 * is_user_admin: true
 * last_login: "2022-04-07T11:45:01.108451+09:00"
 * last_name: "example"
 * modified_time: "2022-03-30T11:24:20.851136+09:00"
 * modified_user__name: "master@example.com"
 * old_id: null
 * registed_at: null
 * role: "admin"
 * tel1: null
 * tel2: null
 * tel3: null
 * username: "admin@example.com"
 */

const mockUserListItemAPIData: UserListItemAPIModel = {
    display_name: "example admin",
    editable: true,
    email: "admin@example.com",
    email_signature: "===========\nThe admin\n===========",
    first_name: "admin",
    id: "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
    is_active: true,
    is_user_admin: true,
    last_login: "2022-04-07T11:45:01.108451+09:00",
    last_name: "example",
    modified_time: "2022-03-30T11:24:20.851136+09:00",
    modified_user__name: "master@example.com",
    old_id: null,
    registed_at: null,
    role: "admin",
    tel1: "00",
    tel2: "1111",
    tel3: undefined,
    username: "admin@example.com",
    user_service_id: randString(),
};

describe("useUser.ts", () => {
    describe("convertUserListItemAPIModelToUserListItemModel()", () => {
        test("test converter", () => {
            const convertedData =
                convertUserListItemAPIModelToUserListItemModel(
                    mockUserListItemAPIData
                );
            // NOTE(joshua-hashimoto): APIのレスポンスにないdate_joinedとtelはテストしない
            expect(convertedData.id).toBe(mockUserListItemAPIData.id);
            expect(convertedData.display_name).toBe(
                mockUserListItemAPIData.display_name
            );
            expect(convertedData.email).toBe(mockUserListItemAPIData.email);
            expect(convertedData.last_login).toBe(
                getDateStr(mockUserListItemAPIData.last_login)
            );
            expect(convertedData.is_active).toBe(
                mockUserListItemAPIData.is_active
            );
            expect(convertedData.first_name).toBe(
                mockUserListItemAPIData.first_name
            );
            expect(convertedData.last_name).toBe(
                mockUserListItemAPIData.last_name
            );
            expect(convertedData.role).toBe(mockUserListItemAPIData.role);
            expect(convertedData.tel1).toBe(mockUserListItemAPIData.tel1);
            expect(convertedData.tel2).toBe(mockUserListItemAPIData.tel2);
            expect(convertedData.tel3).toBe(mockUserListItemAPIData.tel3);
        });

        test("test against old converter", () => {
            const convertedData =
                convertUserListItemAPIModelToUserListItemModel(
                    mockUserListItemAPIData
                );
            const oldConverterData = convertUserSearchPageResponseDataEntry(
                mockUserListItemAPIData,
                0
            );
            delete oldConverterData.key;
            expect(convertedData).toMatchObject(oldConverterData);
        });
    });

    describe("convertUserRegisterFormModelToUserRegisterRequestDataModel()", () => {
        const convertedData =
            convertUserRegisterFormModelToUserRegisterRequestDataModel(
                userFormData
            );
        expect(convertedData.avatar).toBe(undefined);
        expect(convertedData.email).toBe(userFormData.email);
        expect(convertedData.email_signature).toBe(
            userFormData.email_signature
        );
        expect(convertedData.first_name).toBe(userFormData.first_name);
        expect(convertedData.last_name).toBe(userFormData.last_name);
        expect(convertedData.role).toBe(userFormData.role);
        expect(convertedData.tel1).toBe(userFormData.tel1);
        expect(convertedData.tel2).toBe(userFormData.tel2);
        expect(convertedData.tel3).toBe(userFormData.tel3);
        expect(convertedData.password).toBe(userFormData.password);
        expect(convertedData.is_active).toBe(userFormData.is_active);
    });

    describe("convertUserFormModelToUserUpdateRequestDataModel()", () => {
        const convertedData =
            convertUserFormModelToUserUpdateRequestDataModel(userFormData);
        expect(convertedData.avatar).toBe(undefined);
        expect(convertedData.email).toBe(userFormData.email);
        expect(convertedData.email_signature).toBe(
            userFormData.email_signature
        );
        expect(convertedData.first_name).toBe(userFormData.first_name);
        expect(convertedData.last_name).toBe(userFormData.last_name);
        expect(convertedData.role).toBe(userFormData.role);
        expect(convertedData.tel1).toBe(userFormData.tel1);
        expect(convertedData.tel2).toBe(userFormData.tel2);
        expect(convertedData.tel3).toBe(userFormData.tel3);
        expect(convertedData.password).toBe(userFormData.password);
        expect(convertedData.is_active).toBe(userFormData.is_active);
    });

    describe("useFetchUsersAPIQuery()", () => {
        test("fetch active users", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchUsersAPIQuery({
                        deps: { is_active: true },
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toEqual("success");
            expect(result.current.data?.count).toEqual(2);
        });

        test("fetch inactive users", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchUsersAPIQuery({
                        deps: { is_active: false },
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toEqual("success");
            expect(result.current.data?.count).toEqual(1);
        });
    });
});
