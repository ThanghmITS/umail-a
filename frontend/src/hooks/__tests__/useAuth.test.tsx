import React, { ReactNode } from "react";
import Paths from "~/components/Routes/Paths";
import {
    mockLoginAPIResponseData,
    mockLoginResponseData,
    mockLogoutFailsWithInvalidCredentials,
} from "~/test/mock/authAPIMock";
import {
    act,
    mockHistoryPush,
    QueryClientWrapper,
    renderHook,
} from "~/test/utils";
import {
    convertLoginAPIResponseModelToLoginResponseModel,
    doesRequiredFieldsExists,
    useLoginAPIMutation,
    useLogoutAPIMutation,
} from "../useAuth";
import { MemoryRouter } from "react-router-dom";
import { configureStore, EnhancedStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { Provider } from "react-redux";
import { mockServer } from "~/test/setupTests";
import { LoginResponseModel } from "~/models/authModel";

const Wrapper = ({
    store,
    children,
}: {
    store: EnhancedStore;
    children: ReactNode;
}) => {
    return (
        <Provider store={store}>
            <QueryClientWrapper>
                <MemoryRouter>{children}</MemoryRouter>
            </QueryClientWrapper>
        </Provider>
    );
};

describe("useAuth.ts", () => {
    describe("convertLoginAPIResponseModelToLoginResponseModel()", () => {
        test("can convert given value as expected", () => {
            const convertedData =
                convertLoginAPIResponseModelToLoginResponseModel(
                    mockLoginAPIResponseData
                );
            expect(convertedData.token).toBe(mockLoginAPIResponseData.token);
            expect(convertedData.userId).toBe(mockLoginAPIResponseData.user_id);
            expect(convertedData.displayName).toBe(
                mockLoginAPIResponseData.display_name
            );
            expect(convertedData.isUserAdmin).toBe(
                mockLoginAPIResponseData.is_user_admin
            );
            expect(convertedData.avatar).toBe(mockLoginAPIResponseData.avatar);
            expect(convertedData.role).toBe(mockLoginAPIResponseData.role);
            expect(convertedData.authorizedActions).toMatchObject(
                mockLoginAPIResponseData.authorized_actions
            );
            expect(convertedData.intercomUserHash).toBe(
                mockLoginAPIResponseData.intercom_user_hash
            );
            expect(convertedData.userDisplaySetting).toMatchObject(
                mockLoginAPIResponseData.user_display_setting
            );
        });
    });

    describe("doesRequiredFieldsExists()", () => {
        const fnc = doesRequiredFieldsExists;

        test("mockLoginResponseData is valid", () => {
            expect(fnc(mockLoginResponseData)).toBeTruthy();
        });

        test("token is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                token: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                token: "",
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("userId is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                userId: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                userId: "",
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("displayName is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                displayName: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                displayName: "",
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("isUserAdmin is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                isUserAdmin: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                isUserAdmin: null,
            };
            expect(fnc(testingResponse2)).toBeFalsy();
            const testingResponse3 = {
                ...mockLoginResponseData,
                isUserAdmin: false,
            };
            expect(fnc(testingResponse3)).toBeTruthy();
        });

        test("avatar is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                avatar: "",
            };
            expect(fnc(testingResponse1)).toBeTruthy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                avatar: null,
            };
            expect(fnc(testingResponse2)).toBeTruthy();
            const testingResponse3 = {
                ...mockLoginResponseData,
                avatar: undefined,
            };
            expect(fnc(testingResponse3)).toBeTruthy();
            const testingResponse4: LoginResponseModel = {
                ...mockLoginResponseData,
                avatar: "/avatar/url",
            };
            expect(fnc(testingResponse4)).toBeTruthy();
        });

        test("role is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                role: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                role: "",
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("authorizedActions is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                authorizedActions: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                authorizedActions: {},
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("intercomUserHash is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                intercomUserHash: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                intercomUserHash: "",
            };
            expect(fnc(testingResponse2)).toBeFalsy();
        });

        test("userDisplaySetting is invalid", () => {
            const testingResponse1 = {
                ...mockLoginResponseData,
                userDisplaySetting: undefined,
            };
            expect(fnc(testingResponse1)).toBeFalsy();
            const testingResponse2 = {
                ...mockLoginResponseData,
                userDisplaySetting: {},
            };
            expect(fnc(testingResponse2)).toBeFalsy();
            const testingResponse3 = {
                ...mockLoginResponseData,
                userDisplaySetting: {
                    content_hash: undefined,
                },
            };
            expect(fnc(testingResponse3)).toBeFalsy();
            const testingResponse4 = {
                ...mockLoginResponseData,
                userDisplaySetting: {
                    content_hash: {},
                },
            };
            expect(fnc(testingResponse4)).toBeFalsy();
        });
    });

    describe("useLogoutAPIMutation", () => {
        let localStore = configureStore({
            reducer: { login },
            preloadedState: {
                login: {
                    ...LoginInitialState,
                    threeLogin: 2,
                },
            },
        });

        let { result, waitFor } = renderHook(() => useLogoutAPIMutation(), {
            wrapper: ({ children }) => (
                <Wrapper store={localStore}>{children}</Wrapper>
            ),
        });

        beforeEach(() => {
            localStore = configureStore({
                reducer: { login },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        threeLogin: 2,
                    },
                },
            });
            result = renderHook(() => useLogoutAPIMutation(), {
                wrapper: ({ children }) => (
                    <Wrapper store={localStore}>{children}</Wrapper>
                ),
            }).result;
            waitFor = renderHook(() => useLogoutAPIMutation(), {
                wrapper: ({ children }) => (
                    <Wrapper store={localStore}>{children}</Wrapper>
                ),
            }).waitFor;
        });

        test("hook can execute mutate", async () => {
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.login);
        });

        test("logout clears redux store", async () => {
            const originalState = localStore.getState().login;
            expect(originalState.threeLogin).toBe(2);
            expect(originalState).not.toMatchObject(LoginInitialState);
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isSuccess);
            const states = localStore.getState().login;
            expect(states).toMatchObject(LoginInitialState);
        });

        test("logout fails with invalid credentials", async () => {
            mockServer.use(mockLogoutFailsWithInvalidCredentials);
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.login);
        });
    });

    describe("useLoginAPIMutation", () => {
        let localStore = configureStore({
            reducer: { login },
            preloadedState: {
                login: {
                    ...LoginInitialState,
                    threeLogin: 2,
                },
            },
        });

        let { result, waitFor } = renderHook(() => useLoginAPIMutation(), {
            wrapper: ({ children }) => (
                <Wrapper store={localStore}>{children}</Wrapper>
            ),
        });

        beforeEach(() => {
            localStore = configureStore({
                reducer: { login },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        threeLogin: 2,
                    },
                },
            });
            result = renderHook(() => useLoginAPIMutation(), {
                wrapper: ({ children }) => (
                    <Wrapper store={localStore}>{children}</Wrapper>
                ),
            }).result;
            waitFor = renderHook(() => useLoginAPIMutation(), {
                wrapper: ({ children }) => (
                    <Wrapper store={localStore}>{children}</Wrapper>
                ),
            }).waitFor;
        });

        test("hook can execute mutate", async () => {
            act(() => {
                result.current.mutate({
                    username: "test_user",
                    password: "Abcd1234",
                });
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.login);
        });
    });
});
