import { LoginResponseModel } from "~/models/authModel";
import { IntercomModel } from "~/models/intercomModel";
import { intercomAppId } from "~/utils/constants";

export const useIntercom = () => {
    const bootupIntercom = () => {
        window.Intercom("boot", {
            app_id: intercomAppId,
        });
    };

    const setupIntercom = ({
        userId,
        displayName,
        intercomUserHash,
    }: IntercomModel) => {
        window.Intercom("boot", {
            app_id: intercomAppId,
            name: displayName,
            user_id: userId,
            user_hash: intercomUserHash,
        });
    };

    const shutdownIntercom = () => {
        document.cookie = `intercom-session-${intercomAppId}=;`;
        document.cookie = `intercom-id-${intercomAppId}=;`;
        window.Intercom("shutdown");
        window.Intercom("boot", {
            app_id: intercomAppId,
        });
    };

    return {
        bootupIntercom,
        setupIntercom,
        shutdownIntercom,
    };
};
