import { message } from "antd";
import { UseQueryResult } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { AuthActions } from "~/actionCreators/authActions";
import {
    AuthorizedActionsModel,
    AuthorizedActionsResponseModel,
} from "~/models/authModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { RootState } from "~/models/store";
import { authAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys } from "~/utils/constants";
import { isEmpty } from "~/utils/utils";
import { useLogoutAPIMutation } from "./useAuth";
import { useCustomQuery } from "./useCustomQuery";
import { customErrorMessage, customInfoMessage } from "~/components/Common/AlertMessage/AlertMessage";

type AuthorizedActions = {
    [key in keyof AuthorizedActionsModel]: AuthorizedActionsModel[key];
};

export const useAuthorizedActions = <
    TFnArg extends keyof AuthorizedActionsModel
>(
    id: TFnArg
): AuthorizedActions[TFnArg] => {
    const authorizedActions: AuthorizedActions = useSelector(
        (state: RootState) => state.login.authorizedActions
    );

    return authorizedActions[id];
};

export const useFetchAuthorizedActionsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    AuthorizedActionsResponseModel,
    AuthorizedActionsModel | undefined,
    string
>): UseQueryResult<AuthorizedActionsModel | undefined> => {
    return useCustomQuery({
        queryKey: QueryKeys.auth.authorizedActions,
        deps,
        options: {
            retry: false,
            refetchOnWindowFocus: false,
            ...options,
            select: (result) => result.authorized_action,
        },
        apiRequest: authAPI.fetchAuthorizedActions,
    });
};

export const useGuardAuthorizedActions = (path: string) => {
    const dispatch = useDispatch();
    const { mutate: logout } = useLogoutAPIMutation();

    const { isLoading } = useFetchAuthorizedActionsAPIQuery({
        deps: path,
        options: {
            onSuccess: async (result) => {
                // NOTE(joshua-hashimoto): resultが空やnull、undefinedだったらログアウトさせる
                if (!result || isEmpty(result)) {
                    customErrorMessage(ErrorMessages.auth.authorizedActions);
                    logout();
                    return;
                }
                dispatch(
                    AuthActions.updateAuthorizedActions({
                        authorizedActions: result,
                    })
                );
            },
            onError: async (err) => {
                let errorMessage = ErrorMessages.auth.authorizedActions;
                if (err) {
                    // Show message from BE with unauthorization actions
                    if (
                        err.response?.status === 401 &&
                        err.response?.data?.detail
                    ) {
                        errorMessage = err.response.data.detail;
                    }
                    if(err.response?.data?.code !== "user_token_expired"){
                        customInfoMessage(errorMessage);
                    }
                }
                // NOTE(joshua-hashimoto): AuthorizedActionsが取得できない場合もログアウトさせる
                logout();
            },
        },
    });

    return {
        isLoading,
    };
};
