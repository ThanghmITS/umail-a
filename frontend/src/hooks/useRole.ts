import { useSelector } from "react-redux";
import { RootState } from "~/models/store";

export const useRole = () => {
    const { role, isAdminUser } = useSelector(
        (state: RootState) => state.login
    );
    const isMasterRole = role === "master";

    return {
        role,
        isMasterRole,
        isAdminUser,
    };
};
