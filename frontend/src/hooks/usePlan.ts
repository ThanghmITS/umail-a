import { useEffect, useState } from "react";
import {
    PlanSummaryModel,
    PlanSummaryResponseModel,
    PlanUserDataAPIModel,
    PlanUserDataModel,
} from "~/models/planModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { useQueryClient, UseQueryResult } from "react-query";
import { useCustomQuery } from "./useCustomQuery";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { planAPI } from "~/networking/api";
import { useHistory, useLocation } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { useCustomMutation } from "./useCustomMutation";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
import { useSetRecoilState } from "recoil";
import { isAppLoading as isAppLoadingAtom } from "~/recoil/atom";

export const convertPlanUserDataAPIModelToPlanUserDataModel = (
    data: PlanUserDataAPIModel
): PlanUserDataModel => {
    const obj: PlanUserDataModel = {
        id: data.id,
        planMasterId: data.plan_master_id,
        userRegistrationLimit: data.user_registration_limit,
        expirationDate: data.expiration_date,
        defaultUserCount: data.default_user_count,
        currentUserCount: data.current_user_count,
    };
    return obj;
};

export const useFetchUserPlanInfoAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PlanUserDataAPIModel,
    PlanUserDataModel
>): UseQueryResult<PlanUserDataModel> => {
    return useCustomQuery<PlanUserDataAPIModel, PlanUserDataModel>({
        queryKey: QueryKeys.plan.userPlan,
        deps,
        options: {
            ...options,
            select: convertPlanUserDataAPIModelToPlanUserDataModel,
            onError: (err) => {
                let errorMessage = ErrorMessages.plan.planInfo;
                if (err.response?.data.detail) {
                    errorMessage = err.response.data.detail;
                }
                customErrorMessage(errorMessage);
            },
        },
        apiRequest: planAPI.fetchUserPlanInfo,
    });
};

export const usePlanRegisterPlanMutation = () => {
    const setIsAppLoading = useSetRecoilState(isAppLoadingAtom);

    return useCustomMutation(planAPI.registerPlan, {
        onMutate: () => {
            setIsAppLoading(true);
        },
        onSuccess: () => {
            customSuccessMessage("", {
                content: SuccessMessages.plan.purchase,
                onClose: () => {
                    // NOTE(joshua-hashimoto): 画面ロード後にsetIsAppLoadingを実行するように設定
                    window.onload = () => setIsAppLoading(false);
                    // NOTE(joshua-hashimoto): 強制リロード
                    window.location.reload();
                },
            });
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.plan.purchase;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
            setIsAppLoading(false);
        },
    });
};

export const usePlanAddAccountLimitAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(planAPI.addAccountLimit, {
        onSuccess: () => {
            customSuccessMessage(SuccessMessages.plan.addUser);
            // NOTE(joshua-hashimoto): 成功したら、ユーザープラン情報取得を再度呼び出し
            queryClient.invalidateQueries(QueryKeys.plan.userPlan);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.plan.addUser;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const usePlanRemoveAccountLimitAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(planAPI.removeAccountLimit, {
        onSuccess: () => {
            customSuccessMessage(SuccessMessages.plan.removeUser);
            // NOTE(joshua-hashimoto): 成功したら、ユーザープラン情報取得を再度呼び出し
            queryClient.invalidateQueries(QueryKeys.plan.userPlan);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.plan.removeUser;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const convertPlanSummaryResponseModelToPlanSummaryModel = (
    data: PlanSummaryResponseModel
): PlanSummaryModel => {
    const obj: PlanSummaryModel = {
        planId: data.plan_id,
        planName: data.plan_name,
        remainingDays: data.remaining_days,
        paymentErrorExists: data.payment_error_exists,
        paymentErrorCardIds: data.payment_error_card_ids,
    };
    return obj;
};

export const usePlanSummaryAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PlanSummaryResponseModel,
    PlanSummaryModel
> = {}): UseQueryResult<PlanSummaryModel> => {
    return useCustomQuery<PlanSummaryResponseModel, PlanSummaryModel>({
        queryKey: QueryKeys.plan.summary,
        deps,
        options: {
            ...options,
            retry: false,
            select: convertPlanSummaryResponseModelToPlanSummaryModel,
        },
        apiRequest: planAPI.fetchPlanSummary,
    });
};

export const usePlans = ({
    deps,
    options,
}: CustomUseQueryOptions<PlanSummaryResponseModel, PlanSummaryModel> = {}) => {
    const { data, ...others } = usePlanSummaryAPIQuery({ deps, options });

    const isFreeTrial = data?.planId === 0;
    const isLightPlan = data?.planId === 1;
    const isStandardPlan = data && data.planId >= 2;
    const isProfessionalPlan = data && data.planId >= 3;
    const isPayedPlan = isLightPlan || isStandardPlan || isProfessionalPlan;

    return {
        data,
        isFreeTrial,
        isLightPlan,
        isStandardPlan,
        isProfessionalPlan,
        isPayedPlan,
        ...others,
    };
};

export const useGuardPlanSummary = ({
    deps,
    options,
}: CustomUseQueryOptions<PlanSummaryResponseModel, PlanSummaryModel> = {}) => {
    const router = useHistory();
    const location = useLocation();
    const [isNotValidUser, setIsNotValidUser] = useState(true);
    const { data, isFreeTrial, isPayedPlan, ...others } = usePlans({
        deps,
        options,
    });

    useEffect(() => {
        if (!data) {
            return;
        }
        const isExpired = !data.remainingDays;

        const isForcedToPlanPage = isFreeTrial && isExpired;
        if (isForcedToPlanPage) {
            router.push(Paths.plan);
            setIsNotValidUser(isForcedToPlanPage);
            return;
        }

        const isPaymentHistoryPage =
            location.pathname === Paths.purchaseHistory;
        const isForcedToPaymentPage =
            isPayedPlan && isExpired && !isPaymentHistoryPage;
        if (isForcedToPaymentPage) {
            router.push(Paths.payment);
            setIsNotValidUser(isForcedToPaymentPage);
            return;
        }
        setIsNotValidUser(false);
    }, [data, isFreeTrial, isPayedPlan]);

    return {
        isNotValidUser,
        data,
        isFreeTrial,
        isPayedPlan,
        ...others,
    };
};
