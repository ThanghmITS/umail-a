import { message } from "antd";
import { UseQueryResult } from "react-query";
import getDateStr from "~/domain/date";
import { CustomUseQueryOptions } from "~/models/queryModel";
import {
    ScheduledEmailSettingModel,
    ScheduledEmailSettingResponseModel,
} from "~/models/scheduledEmailSettingModel";
import { scheduledEmailSettingAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel =
    (data: ScheduledEmailSettingResponseModel): ScheduledEmailSettingModel => {
        return {
            file_type: data.file_type,
            company: data.company,
            hostname: data.hostname,
            is_open_count_available: data.is_open_count_available,
            is_open_count_extra_period_available:
                data.is_open_count_extra_period_available,
            is_use_attachment_max_size_available:
                data.is_use_attachment_max_size_available,
            is_use_delivery_interval_available:
                data.is_use_delivery_interval_available,
            is_use_remove_promotion_available:
                data.is_use_remove_promotion_available,
            modified_time: getDateStr(data.modified_time),
            modified_user: data.modified_user__name,
            password: data.password,
            port_number: data.port_number,
            target_count_addon_purchase_count:
                data.target_count_addon_purchase_count,
            use_attachment_max_size: data.use_attachment_max_size,
            use_open_count: data.use_open_count,
            use_open_count_extra_period: data.use_open_count_extra_period,
            use_remove_promotion: data.use_remove_promotion,
            connection_type: data.connection_type,
            username: data.username,
        };
    };

export const useFetchScheduledEmailSettingAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ScheduledEmailSettingResponseModel,
    ScheduledEmailSettingModel
>): UseQueryResult<ScheduledEmailSettingModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.scheduledEmailSetting.detail,
        deps,
        options: {
            onError: (err) => {
                const errorMessage = err.response?.data.detail;
                if (errorMessage) {
                    customErrorMessage(errorMessage);
                }
            },
            ...options,
            select: (result) =>
                convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel(
                    result
                ),
        },
        apiRequest: scheduledEmailSettingAPI.fetchScheduledEmailSetting,
    });
};

export const useTestConnectScheduledEmailSettingAPIMutate = () => {
    return useCustomMutation(scheduledEmailSettingAPI.testConnection, {
        onSuccess: (response) => {
            customSuccessMessage(
                SuccessMessages.scheduledEmailSetting.testConnect
            );
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.scheduledEmailSetting.testConnect;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useUpdateScheduledEmailSettingAPIMutate = () => {
    return useCustomMutation(
        scheduledEmailSettingAPI.updateScheduledEmailSetting,
        {
            onSuccess: (response) => {
                customSuccessMessage(SuccessMessages.generic.update);
            },
            onError: (err) => {
                let errorMessage = ErrorMessages.generic.update;
                if (err.response?.data.detail) {
                    errorMessage = err.response.data.detail;
                }
                customErrorMessage(errorMessage);
            },
        }
    );
};
