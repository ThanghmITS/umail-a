import _ from "lodash";
import { UseQueryResult } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useSetRecoilState } from "recoil";
import { FIX_CURRENT_SEARCH_CONDITIONS } from "~/actions/actionTypes";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { SCHEDULED_MAIL_EDIT_PAGE_CONTACT_PICKER_FORM } from "~/components/Pages/pageIds";
import { BoardChecklistItemModel } from "~/models/boardCommonModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import {
    ScheduledEmailAttachmentSizeExtendedModel,
    ScheduledEmailAttachmentSizeExtendedResponseModel,
    ScheduledEmailOpenerListModel,
    ScheduledEmailOpenerListResponseModel,
    ScheduledMailTemplateResponseModel,
    ScheduledMailTemplateModel,
    ScheduledEmailSearchResponseModel,
    ScheduledMailSearchType
} from "~/models/scheduledEmailModel";
import { RootState } from "~/models/store";
import { scheduledEmailAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys } from "~/utils/constants";
import { Override } from "~/utils/types";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import { isAppLoading as isAppLoadingAtom } from "~/recoil/atom";
import JSZip from "jszip";
import moment from "moment";
import { b64toBlob } from "~/utils/utils";
import fileDownload from "js-file-download";

export const convertScheduledMailTemplateResponseModelToScheduledMailTemplateModel =
    (data: ScheduledMailTemplateResponseModel): ScheduledMailTemplateModel => {
        return {
            templates: data.templates,
            total_available_count: data.total_available_count,
        };
    };

export const useScheduledEmailUtils = () => {
    const dispatch = useDispatch();
    const { currentSearchConditions } = useSelector(
        (state: RootState) => state.scheduledEmailEditPageContactSearchForm
    );

    const deleteCurrentSearchConditionTags = (tagIds: string[]) => {
        const copiedConditions = { ...currentSearchConditions };
        const contactTags: string[] = copiedConditions.contact__tags;
        const remainingTags = _.difference(contactTags, tagIds);
        copiedConditions.tags = remainingTags;
        dispatch({
            type:
                SCHEDULED_MAIL_EDIT_PAGE_CONTACT_PICKER_FORM +
                FIX_CURRENT_SEARCH_CONDITIONS,
            payload: {
                fixedCurrentSearchConditions: copiedConditions,
            },
        });
    };

    return {
        deleteCurrentSearchConditionTags,
    };
};

export const useFetchScheduledEmailOpenerListAPIQuery = ({
    deps,
    options,
    scheduledEmailId,
}: Override<
    CustomUseQueryOptions<
        ScheduledEmailOpenerListResponseModel,
        ScheduledEmailOpenerListModel
    >,
    { scheduledEmailId: string }
>): UseQueryResult<ScheduledEmailOpenerListModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.scheduledEmail.openerList,
        deps,
        options: {
            ...options,
            select: (result) => {
                return {
                    ...result,
                    isOpenCountExtraPeriodAvailable:
                        result.is_open_count_extra_period_available,
                };
            },
        },
        apiRequest: () => scheduledEmailAPI.fetchOpenerList(scheduledEmailId),
    });
};

export const useFetchScheduledEmailAttachmentSizeExtendedAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ScheduledEmailAttachmentSizeExtendedResponseModel,
    ScheduledEmailAttachmentSizeExtendedModel
>): UseQueryResult<ScheduledEmailAttachmentSizeExtendedModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.scheduledEmail.attachmentSizeExtended,
        deps,
        options: {
            ...options,
            select: (result) => {
                return {
                    isAttachmentSizeExtended:
                        result.is_attachment_size_extended,
                };
            },
        },
        apiRequest: scheduledEmailAPI.scheduledEmailAttachmentSizeExtended,
    });
};
export const useFetchScheduledEmailSearchAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ScheduledEmailSearchResponseModel,
    ScheduledEmailSearchResponseModel,
    { params?:ScheduledMailSearchType }
>): UseQueryResult<ScheduledEmailSearchResponseModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.scheduledEmail.search,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) =>
            scheduledEmailAPI.fetchListScheduledMailSearch(query?.params),
    });
};

export const useScheduledEmailAuthorizeDownloadAPIMutation = () => {
    type RequestModel = {
        id: string;
        password: string;
    };

    const apiRequest = ({ id, password }: RequestModel) => {
        return scheduledEmailAPI.authorizeDownload(id, password);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (result) => {
            const data = result.data;
            const isAttachmentEmpty = data.attachments.length === 0;
            if (isAttachmentEmpty) {
                customErrorMessage(ErrorMessages.scheduledEmail.fileIsDeleted);
            }
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.scheduledEmail.download;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useScheduledEmailFileDownloadAPIMutation = () => {
    const setIsAppLoading = useSetRecoilState(isAppLoadingAtom);

    const processDownload = async (data: any) => {
        if (!data || !Object.values(data).length) {
            return;
        }
        const files: any[] = Object.values(data);
        if (files.length === 1) {
            files.map((item: any) => {
                const blob = b64toBlob(item.content, item.content_type);
                fileDownload(blob, item.file_name);
            });
            return;
        }
        // NOTE(): ファイルが複数の場合はzip化してダウンロードする
        const zip = new JSZip();
        const zipFileName = `コモレビ_ファイルダウンロード_${moment().format(
            "YYYY年MM月DD日"
        )}.zip`;
        for (const file of files) {
            const blob = b64toBlob(file.content, file.content_type);
            zip.file(file.file_name, blob, { binary: true });
        }
        const content: Blob = await zip.generateAsync({ type: "blob" });
        fileDownload(content, zipFileName);
    };

    return useCustomMutation(scheduledEmailAPI.downloadFiles, {
        onMutate: () => {
            setIsAppLoading(true);
        },
        onSuccess: (result) => {
            const data = result.data;
            processDownload(data);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.scheduledEmail.download;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
        onSettled: () => {
            setIsAppLoading(false);
        },
    });
};
