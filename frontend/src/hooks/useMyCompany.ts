import getDateStr, { DateStrToMoment } from "~/domain/date";
import { MyCompanyModel, MyCompanyResponseModel } from "~/models/myCompany";

export const convertMyCompanyResponseModelToMyCompanyModel = (
    data: MyCompanyResponseModel
): MyCompanyModel => {
    return {
        id: data.id,
        name: data.name,
        domain_name: data.domain_name,
        address: data.address,
        building: data.building,
        capital_man_yen: data.capital_man_yen,
        establishment_date: DateStrToMoment(data.establishment_date),
        establishment_year: data.establishment_year,
        has_p_mark_or_isms: data.has_p_mark_or_isms,
        has_invoice_system: data.has_invoice_system,
        has_haken: data.has_haken,
        has_distribution: data.has_distribution,
        capital_man_yen_required_for_transactions:
            data.capital_man_yen_required_for_transactions,
        p_mark_or_isms: data.p_mark_or_isms,
        invoice_system: data.invoice_system,
        haken: data.haken,
        exceptional_organization_names: data.exceptional_organization_names,
        exceptional_organizations: data.exceptional_organizations,
        modified_time: getDateStr(data.modified_time),
        modified_user: data.modified_user__name,
        settlement_month: data.settlement_month
    };
};
