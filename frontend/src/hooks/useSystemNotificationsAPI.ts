import { AxiosResponse } from "axios";
import { useDispatch, useSelector } from "react-redux";
import {
    LOADED,
    LOADING,
    SYSTEM_NOTIFICATIONS,
    UNREAD,
} from "~/actions/actionTypes";
import { simpleAction, simpleFetch } from "~/actions/data";
import { Endpoint } from "~/domain/api";
import { SystemNotificationModel } from "~/models/notificationModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { RootState } from "~/models/store";
import {
    SystemNotificationInitialStateModel,
    SystemNotificationPayloadActionModel,
} from "~/reducers/notificationReducer";
import { DEFAULT_PAGE_SIZE } from "~/utils/constants";

type FetchNotificationSearchParamModel = {
    currentPage: number;
    unread?: boolean;
};

export const useSystemNotificationsAPI = () => {
    const dispatch = useDispatch();
    const { token, userId } = useSelector((state: RootState) => state.login);
    const baseURL = Endpoint.getBaseUrl();
    const {
        isLoading,
        systemNotifications,
    }: SystemNotificationInitialStateModel = useSelector(
        (state: RootState) => state.systemNotificationReducer
    );

    const fetchSystemNotifications = async ({
        currentPage,
        unread = false,
    }: FetchNotificationSearchParamModel) => {
        let url = `${baseURL}/${Endpoint.systemNotifications}?page=${currentPage}&page_size=${DEFAULT_PAGE_SIZE}`;
        let reducerActionType = SYSTEM_NOTIFICATIONS;
        if (unread) {
            url += "&unread=True";
            reducerActionType += UNREAD;
        }
        dispatch({ type: SYSTEM_NOTIFICATIONS + LOADING });
        const responseData: PaginationResponseModel<SystemNotificationModel[]> =
            await simpleFetch(token, url);
        const responseSystemNotifications = responseData.results;
        let localSystemNotifications = [];
        if (currentPage === 1) {
            localSystemNotifications = [...responseSystemNotifications];
        } else {
            localSystemNotifications = [
                ...systemNotifications,
                ...responseSystemNotifications,
            ];
        }
        const payload: SystemNotificationPayloadActionModel = {
            systemNotifications: localSystemNotifications,
            previousUrl: responseData.previous,
            nextUrl: responseData.next,
            userId: userId
        };
        dispatch({
            type: SYSTEM_NOTIFICATIONS + LOADED,
            payload,
        });
    };

    const readNotifications = async (notificationIds: string[] | number[]) => {
        const url = `${baseURL}/${Endpoint.systemNotifications}`;
        const responseData: {
            data: SystemNotificationModel[];
        } = await simpleAction(SYSTEM_NOTIFICATIONS, token, url, {
            source: notificationIds,
        });
        const responseReadSystemNotifications = responseData.data;
        const updatedSystemNotificationIds =
            responseReadSystemNotifications.map((item) => item.id);
        const newSystemNotifications = systemNotifications.map(
            (systemNotification) => {
                if (
                    updatedSystemNotificationIds.includes(systemNotification.id)
                ) {
                    return responseReadSystemNotifications.find(
                        (item) => item.id === systemNotification.id
                    );
                }
                return systemNotification;
            }
        );
        dispatch({
            type: SYSTEM_NOTIFICATIONS + LOADED,
            payload: {
                systemNotifications: newSystemNotifications,
            },
        });
    };

    return {
        fetchSystemNotifications,
        readNotifications,
    };
};
