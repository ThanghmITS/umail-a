import { useDispatch, useSelector } from "react-redux";
import { removeBranchFromOrganizationAction } from "~/actions/data";
import { ORGANIZATION_EDIT_PAGE } from "~/components/Pages/pageIds";
import { Endpoint } from "~/domain/api";
import { RootState } from "~/models/store";

export const useOrganizationAPI = () => {
    const token = useSelector((state: RootState) => state.login.token);
    const dispatch = useDispatch();
    const baseURL = Endpoint.getBaseUrl();

    const removeOrganizationBranch = (branchId: string) => {
        const url = `${baseURL}/${Endpoint.organizationsBranches}/${branchId}`;
        dispatch(
            removeBranchFromOrganizationAction(
                ORGANIZATION_EDIT_PAGE,
                token,
                url
            )
        );
    };

    return { removeOrganizationBranch };
};
