import { AxiosError, AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { QueryKey, UseQueryOptions, UseQueryResult } from "react-query/types";
import { ErrorDetailModel } from "~/models/responseModel";
import client from "~/networking/api";

type Props<
    TQueryFnData = unknown,
    TData = TQueryFnData,
    TDeps = QueryKey,
    TError = ErrorDetailModel
> = {
    queryKey?: string | any[];
    deps?: TDeps;
    options?: UseQueryOptions<TQueryFnData, AxiosError<TError>, TData>;
    apiRequest: (queryKey?: TDeps) => Promise<AxiosResponse<TQueryFnData>>;
};

export const useCustomQuery = <
    TQueryFnData = unknown,
    TData = TQueryFnData,
    TDeps = QueryKey,
    TError = ErrorDetailModel
>({
    queryKey,
    deps,
    options,
    apiRequest,
}: Props<TQueryFnData, TData, TDeps, TError>): UseQueryResult<
    TData,
    AxiosError<TError>
> => {
    const k = deps ? [queryKey, deps] : [queryKey];
    return useQuery<TQueryFnData, AxiosError<TError>, TData>(
        k,
        async () => {
            try {
                const response = await apiRequest(deps);
                return response.data;
            } catch (err) {
                // NOTE(joshua-hashimoto): ここでエラーの共通処理を実行できる
                throw err;
            }
        },
        {
            refetchOnWindowFocus: false,
            enabled: !!client.defaults.headers.common["Authorization"], // NOTE(joshua-hashimoto): このメソッドを使える条件を設定できる。この条件では、共通axiosクライアントにAuthorizationヘッダーが設定されていない場合はAPIの呼び出しを行わない
            ...options,
        }
    ) as UseQueryResult<TData, AxiosError<TError>>;
};
