import {
    AddonMasterModel,
    AddonMasterResponseModel,
    AddonPurchaseItemModel,
    AddonPurchaseItemResponseModel,
} from "~/models/addonModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { useQueryClient, UseQueryResult } from "react-query";
import { useCustomQuery } from "./useCustomQuery";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { addonAPI } from "~/networking/api";
import { useCustomMutation } from "./useCustomMutation";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const convertAddonMasterResponseModelToAddonMasterModel = (
    data: AddonMasterResponseModel
): AddonMasterModel => {
    const obj: AddonMasterModel = {
        id: data.id,
        description: data.description,
        expirationTime: data.expiration_time,
        isDashboard: data.is_dashboard,
        isMyCompanySetting: data.is_my_company_setting,
        isOrganizations: data.is_organizations,
        isRecommended: data.is_recommended,
        isScheduledMails: data.is_scheduled_mails,
        isSharedMails: data.is_shared_mails,
        limit: data.limit,
        parents: data.parents,
        price: data.price,
        targets: data.targets,
        title: data.title,
        helpUrl: data.help_url,
    };
    return obj;
};

export const useFetchAddonMasterAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    AddonMasterResponseModel[],
    AddonMasterModel[]
>): UseQueryResult<AddonMasterModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.addon.masterList,
        deps,
        options: {
            onError: (err) => {
                let errorMessage = ErrorMessages.addon.addonMaster;
                if (err.response?.data.detail) {
                    errorMessage = err.response.data.detail;
                }
                customErrorMessage(errorMessage);
            },
            ...options,
            select: (result) => {
                const newResult: AddonMasterModel[] = result.map(
                    convertAddonMasterResponseModelToAddonMasterModel
                );
                return newResult;
            },
        },
        apiRequest: addonAPI.fetchAddonMaster,
    });
};

export const convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel = (
    data: AddonPurchaseItemResponseModel
): AddonPurchaseItemModel => {
    const obj: AddonPurchaseItemModel = {
        id: data.id,
        addonMasterId: data.addon_master_id,
        nextPaymentDate: data.next_payment_date,
    };
    return obj;
};

export const useFetchPurchasedAddonsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    AddonPurchaseItemResponseModel[],
    AddonPurchaseItemModel[]
>): UseQueryResult<AddonPurchaseItemModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.addon.purchasedList,
        deps,
        options: {
            onError: (err) => {
                let errorMessage = ErrorMessages.addon.purchasedList;
                if (err.response?.data.detail) {
                    errorMessage = err.response.data.detail;
                }
                customErrorMessage(errorMessage);
            },
            ...options,
            select: (result) => {
                const newResult: AddonPurchaseItemModel[] = result.map(
                    convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel
                );
                return newResult;
            },
        },
        apiRequest: addonAPI.fetchPurchasedAddons,
    });
};

export const usePurchaseAddonAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(addonAPI.purchaseAddon, {
        onSuccess: (response) => {
            // NOTE(joshua-hashimoto): 成功した場合は購入履歴を再度取得する
            customSuccessMessage(SuccessMessages.addon.purchase);
            queryClient.invalidateQueries(QueryKeys.addon.purchasedList);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.addon.purchase;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useRevokeAddonAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(addonAPI.revokeAddon, {
        onSuccess: (response) => {
            // NOTE(joshua-hashimoto): 成功した場合は購入履歴を再度取得する
            customSuccessMessage(SuccessMessages.addon.revoke);
            queryClient.invalidateQueries(QueryKeys.addon.purchasedList);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.addon.revoke;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};
