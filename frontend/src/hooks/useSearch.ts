import { useHistory } from "react-router-dom";
import { isEmpty, stringifyQueryString } from "~/utils/utils";
import { useQueryString } from "./useQueryString";
import queryString from "query-string";

export const useSearchSync = (options?: queryString.ParseOptions) => {
    const router = useHistory();
    const { queryParamsToObj } = useQueryString(options);

    // NOTE(joshua-hashimoto): 念の為ジェネリック型で定義。必要ないと判断されれば削除する
    const syncToUrl = <TValues extends Object>(values: TValues) => {
        if (isEmpty(values)) {
            router.push({ search: undefined });
            return;
        }
        router.push({
            search: stringifyQueryString(values),
        });
    };

    return {
        syncToUrl,
        queryParamsToObj,
    };
};
