import client from "~/networking/api";

export const useClient = () => {
    const setTokenToClient = (token: string) => {
        client.defaults.headers.common["Authorization"] = "Token " + token;
    };

    const removeTokenFromClient = () => {
        delete client.defaults.headers.common["Authorization"];
    };

    return {
        client,
        setTokenToClient,
        removeTokenFromClient,
    };
};
