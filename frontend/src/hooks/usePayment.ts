import { useEffect } from "react";
import { message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
    CLEAR_ERROR,
    CLEAR_SUCCESS,
    PAYMENT_DELETE,
    PAYMENT_FETCH,
    PAYMENT_FORM,
    PAYMENT_UPDATE,
} from "~/actions/actionTypes";
import {
    createPaymentInfoAction,
    deletePaymentInfoAction,
    fetchPaymentInfoAction,
    updatePaymentInfoAction,
} from "~/actions/data";
import { Endpoint } from "~/domain/api";
import { PaymentCreateModel, PaymentDeleteModel, PaymentUpdateModel} from "~/models/paymentModel";
import { RootState } from "~/models/store";
import { customSuccessMessage, customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";

export const usePaymentAPI = () => {
    const token = useSelector((state: RootState) => state.login.token);
    const dispatch = useDispatch();
    const baseURL = Endpoint.getBaseUrl();

    const fetchPayment = () => {
        const url = `${baseURL}/${Endpoint.payjpPayment}`;
        dispatch(fetchPaymentInfoAction(PAYMENT_FETCH, token, url));
    };

    const createPayment = (postData: PaymentCreateModel) => {
        const url = `${baseURL}/${Endpoint.payjpPayment}`;
        dispatch(createPaymentInfoAction(PAYMENT_FORM, token, url, postData));
    };

    const deletePayment = (postData: PaymentDeleteModel) => {
        const url = `${baseURL}/${Endpoint.payjpPayment}`;
        dispatch(deletePaymentInfoAction(PAYMENT_DELETE, token, url, postData));
    };

    const updatePayment = (postData: PaymentUpdateModel) => {
        const url = `${baseURL}/${Endpoint.payjpPayment}`;
        dispatch(updatePaymentInfoAction(PAYMENT_UPDATE, token, url, postData));
    };

    return {
        fetchPayment,
        createPayment,
        deletePayment,
        updatePayment,
    };
};

export const usePaymentRedux = () => {
    const dispatch = useDispatch();
    const {
        isLoading: isPaymentInfoLoading,
        paymentInfo,
        errorMessage: paymentInfoErrorMessage,
    } = useSelector((state: RootState) => state.paymentInfo);
    const {
        successMessage: paymentFormSuccessMessage,
        errorMessage: paymentFormErrorMessage,
    } = useSelector((state: RootState) => state.paymentForm);

    useEffect(() => {
        if (paymentFormSuccessMessage) {
          customSuccessMessage("", {
            content: paymentFormSuccessMessage,
            onClose: () => {
                dispatch({ type: PAYMENT_FORM + CLEAR_SUCCESS });
            },
          });
        }
    }, [paymentFormSuccessMessage]);

    useEffect(() => {
        if (paymentFormErrorMessage) {
            customErrorMessage("",{
              content: paymentFormErrorMessage,
              onClose: () => {
                  dispatch({ type: PAYMENT_FORM + CLEAR_ERROR });
              },
            })
        }
    }, [paymentFormErrorMessage]);

    useEffect(() => {
        if (paymentInfoErrorMessage) {
            customErrorMessage("", {
              content: paymentInfoErrorMessage,
              onClose: () => {
                  dispatch({ type: PAYMENT_FETCH + CLEAR_ERROR });
              },
          });
        }
    }, [paymentInfoErrorMessage]);

    return {
        isPaymentInfoLoading,
        paymentInfo,
        paymentInfoErrorMessage,
    };
};
