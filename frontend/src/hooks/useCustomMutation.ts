import { message } from "antd";
import { AxiosError, AxiosResponse } from "axios";
import {
    MutationFunction,
    useMutation,
    UseMutationOptions,
    UseMutationResult,
} from "react-query";
import { ErrorDetailModel } from "~/models/responseModel";

export const useCustomMutation = <
    TData = unknown,
    TVariables = void,
    TContext = unknown,
    TError = ErrorDetailModel
>(
    apiCall: MutationFunction<AxiosResponse<TData>, TVariables>,
    options?: UseMutationOptions<
        AxiosResponse<TData>,
        AxiosError<TError>,
        TVariables,
        TContext
    >
): UseMutationResult<
    AxiosResponse<TData>,
    AxiosError<TError>,
    TVariables,
    TContext
> => {
    return useMutation<
        AxiosResponse<TData>,
        AxiosError<TError>,
        TVariables,
        TContext
    >(apiCall, {
        ...options,
    });
};
