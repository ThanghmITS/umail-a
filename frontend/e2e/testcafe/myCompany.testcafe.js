import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import {
  TEST_COMPANY_NAME, TEST_COMPANY_DOMAIN_NAME, TEST_COMPANY_CAPITAL_MAN_YEN,
  TEST_COMPANY_NAME_TENNANT_B, TEST_COMPANY_DOMAIN_NAME_TENNANT_B, TEST_COMPANY_CAPITAL_MAN_YEN_TENNANT_B
} from './constants';
import { getPageUrl } from './helpers';


const errorAlert = Selector('.ant-message-error');
const app = Selector('.ant-message');

const resourceUrl = 'http://127.0.0.1:8000/myCompany';

const message = '更新されました';

fixture`MyCompany`
  .page`${resourceUrl}`;



export async function testUpdate(role, name, domain, capital) {
  await t
  .useRole(role)
  .expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
  .expect(Selector('#name').value).eql(name)
  .typeText(Selector('#name'), name, { replace: true })
  .typeText(Selector('#domain_name'), domain, { replace: true })
  .typeText(Selector('#capital_man_yen'), capital, { replace: true })
  .click(Selector('#capital_man_yen'))// Focus on form, to submit by press enter.
  .click(Selector('button[type="submit"]')) // submit
  .expect(app.textContent)
  .contains(`${message}`)
  .expect(getPageUrl())
  .notMatch(new RegExp(`^${resourceUrl}$`));
}



test('testUpdate', async (t) => {
  // WARNING: this test requires a test user on a remote server.
  await testUpdate(testUser, TEST_COMPANY_NAME, TEST_COMPANY_DOMAIN_NAME, TEST_COMPANY_CAPITAL_MAN_YEN)
});

test('testUpdate Tennant B', async (t) => {
    // WARNING: this test requires a test user on a remote server.
    await testUpdate(testUserTennantB, TEST_COMPANY_NAME_TENNANT_B, TEST_COMPANY_DOMAIN_NAME_TENNANT_B, TEST_COMPANY_CAPITAL_MAN_YEN_TENNANT_B)
});
