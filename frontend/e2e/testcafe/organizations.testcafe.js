// TODO: Remove duplications between other tests.
import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import { getPageUrl } from './helpers';
import {
  accessOrganizationsSearchPage, accessOrganizationsRegisterPage, searchOrganizations,
  selectOrganizations, createOrganizations, updateOrganizations, deleteOrganizations,
  updateColumnSettings, accessOrganizationsCsvUploadPage, uploadCsv
} from './helpers/organizations'

const errorAlert = Selector('.ant-message-error');
const resourcePath = 'organizations';
const generateResourceName = 'E2E_TestCafe';
const address = 'X-X-X'
const replaced_name = 'E2E_TestCafe_replaced'

const filename = '../../../static/app_staffing/organizations.csv';
const filename_jp = './files/取引先.csv';
const csv_organization_name = 'テスト会社名'

fixture`Organizations`
  .page`http://127.0.0.1:8000/${resourcePath}`;

const randomNumber = Math.floor(Math.random() * 100000000);



export async function assertAlertNotExists() {
  await t.expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
}

export async function assertListPage() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertDetailPage() {
  await t
    .expect(getPageUrl()).match(new RegExp(`${resourcePath}/[a-zA-Z0-9-]+$`))
}

export async function assertCreatedResource() {
  await t.expect(Selector('.app').textContent).contains(`${generateResourceName}`); // and make sure that the list page contains a created item.
}

export async function assertColumnSettingUpdated() {
  await t
    .expect(Selector('.ant-table-column-has-actions.ant-table-column-has-sorters').withText('取引先名').exists).ok('colimun setting does not loaded')
    .expect(Selector('.ant-table-column-has-actions.ant-table-column-has-sorters').withText('住所').exists).notOk('colimun setting does not loaded')
}

export async function testCreate(role) {
  await t.useRole(role)
  await accessOrganizationsRegisterPage()
  await assertAlertNotExists()
  await createOrganizations(generateResourceName, address, randomNumber)
  await assertListPage()
  await assertCreatedResource()
}

export async function testUpdateColumnSetting(role) {
  await t.useRole(role)
  await accessOrganizationsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchOrganizations(generateResourceName)
  await updateColumnSettings()
  await assertColumnSettingUpdated()
}

export async function testUpdate(role) {
  await t.useRole(role)
  await accessOrganizationsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchOrganizations(generateResourceName)
  await selectOrganizations(generateResourceName)
  await assertDetailPage()
  await updateOrganizations(replaced_name)
  await assertListPage()
}

export async function testDelete(role) {
  await t.useRole(role)
  await accessOrganizationsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchOrganizations(replaced_name)
  await selectOrganizations(replaced_name)
  await assertDetailPage()
  await deleteOrganizations()
  await assertListPage()
}

export async function testUploadCsv(role, filename) {
  await t.useRole(role)
  await accessOrganizationsCsvUploadPage()
  await assertAlertNotExists()
  await uploadCsv(filename)
}

export async function testDeleteCsvData(role) {
  await t.useRole(role)
  await accessOrganizationsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchOrganizations(csv_organization_name)
  await selectOrganizations(csv_organization_name)
  await assertDetailPage()
  await deleteOrganizations()
  await assertListPage()
}

test('testCreate', async (t) => {
  await testCreate(testUser)
});

test('testSearch', async (t) => {
  await t.useRole(testUserTennantB)
  await accessOrganizationsSearchPage()
  await searchOrganizations(generateResourceName)
  await t
    .expect(Selector('.ant-table-row.ant-table-row-level-0').find('td').exists).notOk('cross acount test fail.')
});

test('testUpdateColumnSetting', async (t) => {
  await testUpdateColumnSetting(testUser)
});

test('testCreateTennantB', async (t) => {
  await testCreate(testUserTennantB)
});

test('testUpdate', async (t) => {
  await testUpdate(testUser)
});

test('testUpdateTennantB', async (t) => {
  await testUpdate(testUserTennantB)
});

test('testDelete', async (t) => {
  await testDelete(testUser)
});

test('testDeleteTennantB', async (t) => {
  await testDelete(testUserTennantB)
});

test('testUploadCsv', async (t) => {
  await testUploadCsv(testUser, filename)
});

test('testDeleteCsvData', async (t) => {
  await testDeleteCsvData(testUser)
});

test('testUploadCsvJP', async (t) => {
  await testUploadCsv(testUser, filename_jp)
});

test('testDeleteCsvData', async (t) => {
  await testDeleteCsvData(testUser)
});
