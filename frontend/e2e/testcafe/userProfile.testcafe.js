import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import {
  TEST_USER_FIRST_NAME, TEST_USER_LAST_NAME, TEST_USER_TEL, TEST_PASSWORD,
  TEST_USER_FIRST_NAME_TENNANT_B, TEST_USER_LAST_NAME_TENNANT_B, TEST_USER_TEL_TENNANT_B, TEST_PASSWORD_TENNANT_B
} from './constants';
import { getPageUrl } from './helpers';
import { updateMyProfile } from './helpers/userProfile'

const errorAlert = Selector('.ant-message-error');
const app = Selector('.ant-message');

const resourceUrl = 'http://127.0.0.1:8000/myProfile';

fixture`UserProfile`
  .page`${resourceUrl}`;

export async function assertMyProfileIndexContains(lastname) {
  await t
    .expect(Selector('#email').with({visibilityCheck: true}).hasAttribute('disabled')).eql(true)
    .expect(Selector('#last_name').value).eql(lastname)
};

export async function assertUpdateMyProfileResult() {
  await t
    .expect(app.textContent).contains('更新されました')
    .expect(getPageUrl()).notMatch(new RegExp(`^${resourceUrl}$`));
};

export async function updateUserProfile(account, lastname, firstname, password, tel) {
  // WARNING: this test requires a test user on a remote server.
  await t
    .useRole(account)
    .expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
  await assertMyProfileIndexContains(lastname)
  await updateMyProfile(lastname, firstname, password, tel)
  await assertUpdateMyProfileResult();
};

test('updateUserProfile', async (t) => {
  await updateUserProfile(testUser, TEST_USER_LAST_NAME, TEST_USER_FIRST_NAME, TEST_PASSWORD, TEST_USER_TEL)
});

test('updateUserProfile Tennant B', async (t) => {
  await updateUserProfile(testUserTennantB, TEST_USER_LAST_NAME_TENNANT_B, TEST_USER_FIRST_NAME_TENNANT_B, TEST_PASSWORD_TENNANT_B, TEST_USER_TEL_TENNANT_B)
});
