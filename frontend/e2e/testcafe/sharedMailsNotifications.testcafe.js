import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import { getPageUrl } from './helpers';
import { accessSharedMailNotificationsListPage, accessSharedMailNotificationsRegisterPage, selectSharedMailNotifications, createSharedMailNotifications, updateSharedMailNotifications, deleteSharedMailNotifications} from './helpers/sharedMailNotifications';


const errorAlert = Selector('.ant-message-error');
const resourcePath = 'sharedMailNotifications';

const name = 'hogehoge';
const replaced_name = 'fugafuga'

fixture`SharedMailNotifications`
  .page`http://127.0.0.1:8000/${resourcePath}`;


export async function assertAlertNotExists() {
  await t.expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
}

export async function assertListPage() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertDetailPage() {
  await t
    .expect(getPageUrl()).match(new RegExp(`${resourcePath}/[a-zA-Z0-9-]+$`))
}

export async function assertCreatedResource() {
  await t.expect(Selector('.app').textContent).contains(`${name}`); // and make sure that the list page contains a created item.
}

export async function testCreate(role) {
  await t.useRole(role)
  await accessSharedMailNotificationsRegisterPage()
  await assertAlertNotExists()
  await createSharedMailNotifications(name)
  await assertListPage()
  await assertCreatedResource()
}

export async function testUpdate(role) {
  await t.useRole(role)
  await accessSharedMailNotificationsListPage()
  await assertAlertNotExists()
  await assertListPage()
  await selectSharedMailNotifications(name)
  await assertDetailPage()
  await updateSharedMailNotifications(replaced_name)
  await assertListPage()
}

export async function testDelete(role) {
  await t.useRole(role)
  await accessSharedMailNotificationsListPage()
  await assertAlertNotExists()
  await assertListPage()
  await selectSharedMailNotifications(replaced_name)
  await assertDetailPage()
  await deleteSharedMailNotifications()
  await assertListPage()
}

test('testCreate', async (t) => {
  await testCreate(testUser)
});

test('testSearch', async (t) => {
  await t.useRole(testUserTennantB)
  await accessSharedMailNotificationsListPage()
  await t.expect(Selector('.ant-table-row.ant-table-row-level-0').find('td').exists).notOk('cross acount test fail.')
});

test('testCreate TennantB', async (t) => {
  await testCreate(testUserTennantB)
});

test('testUpdate', async (t) => {
  await testUpdate(testUser)
});

test('testUpdate TennantB', async (t) => {
  await testUpdate(testUserTennantB)
});

test('testDelete', async (t) => {
  await testDelete(testUser)
});

test('testDelete TennantB', async (t) => {
  await testDelete(testUserTennantB)
});
