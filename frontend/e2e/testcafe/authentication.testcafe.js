import { Selector } from 'testcafe';

import { TEST_PASSWORD, TEST_USER } from './constants';
import { getPageUrl } from './helpers';

const resourceUrl = 'http://127.0.0.1:8000/login';

fixture`Authentication`
  .page`${resourceUrl}`;

test('Login', async (t) => {
  // WARNING: this test requires a test user on a remote server.
  await t
    .typeText(Selector('#username'), TEST_USER)
    .typeText(Selector('#password'), TEST_PASSWORD)
    .click(Selector('button[type="submit"]'))
    .expect(getPageUrl())
    .notMatch(new RegExp(`^${resourceUrl}$`)); // check if we left from login page.
});
