import { Selector, t } from 'testcafe';

const resourcePath = 'users';
const registerPageSuffix = 'register';

export async function accessUserSearchPage() {
    await t
      .navigateTo(`${resourcePath}`)
  }

export async function accessUserRegisterPage() {
  await t
    .navigateTo(`${resourcePath}/${registerPageSuffix}`)
}

export async function searchUser(email) {
  await accessUserSearchPage()
  await t
    .typeText(Selector('#email'), email, { replace: true }) // Search by email.
    .pressKey('enter')// Search update target.
    .click(Selector('.ant-table-row.ant-table-row-level-0').find('td')) // Pick the search result.
}

export async function createUser(lastname, firstname, email, tel, password) {
  await accessUserRegisterPage()
  await t
    .typeText(Selector('#last_name'), lastname, { replace: true }) // The input to a search form.
    .typeText(Selector('#first_name'), firstname, { replace: true }) // The input to a search form.
    .typeText(Selector('#email'), email, { replace: true })
    .typeText(Selector('#tel'), tel, { replace: true })
    .typeText(Selector('#password'), password, { replace: true })
    .click(Selector('button[type="submit"]'))// submit
}

export async function updateUser(user_email) {
  await searchUser(user_email)
  await t
    .click(Selector('button[type="submit"]'))// submit // execute update
}

export async function deleteUser(user_email) {
  await searchUser(user_email)
  await t
    .click(Selector('.ant-btn-danger'))
    .pressKey('enter') // execute delete
}
