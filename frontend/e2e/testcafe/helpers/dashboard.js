import { t } from 'testcafe';

const dashboard_resourceUrl = 'http://127.0.0.1:8000/dashboard';

export async function accessDashboard() {
    await t.navigateTo(`${dashboard_resourceUrl}`);
};
