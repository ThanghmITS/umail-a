import { Selector, t } from 'testcafe';

const resourcePath = 'organizations';
const registerPageSuffix = 'register';
const csvPageSuffix = 'csvUpload'



export async function accessOrganizationsSearchPage() {
  await t.navigateTo(`${resourcePath}`)
}

export async function accessOrganizationsRegisterPage() {
  await t.navigateTo(`${resourcePath}/${registerPageSuffix}`)
}

export async function accessOrganizationsCsvUploadPage() {
  await t.navigateTo(`${resourcePath}/${csvPageSuffix}`)
}

export async function searchOrganizations(name) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true }) // Search by name.
    .pressKey('enter')// Search deletion target.
}

export async function selectOrganizations(name) {
  await t.click(Selector('.ant-table-row.ant-table-row-level-0').find('td').withText(name)) // Pick the search result.
}

export async function createOrganizations(name, address, corporate_number) {
  await t
    .click(Selector('#country'))
    .pressKey('enter') // select dropdown items.
    .click(Selector('#category'))
    .pressKey('enter') // select dropdown items.
    .typeText(Selector('#name'), `${name}`, { replace: true }) // The input to a search form.
    .typeText(Selector('#address'), `${address}`, { replace: true })
    .typeText(Selector('#corporate_number'), `${corporate_number}`, { replace: true })
    .click(Selector('button[type="submit"]'))// submit
}

export async function updateOrganizations(name) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true }) // The input to a search form.
    .click(Selector('button[type="submit"]'))// submit // execute update
}

export async function deleteOrganizations() {
  await t
    .click(Selector('.ant-btn-danger'))
    .pressKey('enter') // execute delete
}

export async function updateColumnSettings() {
  await t
    .click(Selector('button[type="button"]').withText('表示項目変更'))
    .click(Selector('.ant-table-row.ant-table-row-level-0[data-row-key="name"]').find('input[type="checkbox"]'))
    .click(Selector('button[type="button"]').withText('OK'))
}

export async function uploadCsv(file_name) {
  await t
    .setFilesToUpload('input[type="file"]', [`${file_name}`])
}
