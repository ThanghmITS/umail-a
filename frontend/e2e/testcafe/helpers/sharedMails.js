import { Selector, t } from 'testcafe';

const resourcePath = 'sharedMails';


export async function accessSharedMailsSearchPage() {
  await t.navigateTo(`${resourcePath}`)
}

export async function searchSharedMails(sender, email, subject, text) {
  await t
    .typeText(Selector('#sender'), sender, { replace: true }) // The input to a search form.
    .typeText(Selector('#email'), email, { replace: true })
    .typeText(Selector('#subject'), subject, { replace: true })
    .typeText(Selector('#text'), text, { replace: true })
    .click(Selector('button[type="submit"]')) // submit
}

export async function searchAllSharedMails() {
  await t
    .click(Selector('button[type="reset"]')) // we need to remove date range search param to ensure to see emails.
    .click(Selector('button[type="submit"]'))
}

export async function selectSharedMails() {
  await t.click(Selector('.ant-table-row.ant-table-row-level-0').find('td')) // Pick the search result.
}

export async function forwardSharedMails() {
  await t.click(Selector('button[type="button"]').withText('このメールに返信する')) // submit
}
