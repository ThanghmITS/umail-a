import { Selector, t } from 'testcafe';

const resourcePath = 'contacts';
const registerPageSuffix = 'register';
const csvPageSuffix = 'csvUpload'

export async function accessContactsSearchPage() {
  await t.navigateTo(`${resourcePath}`)
}

export async function accessContactsRegisterPage() {
  await t.navigateTo(`${resourcePath}/${registerPageSuffix}`)
}

export async function accessContactsCsvUploadPage() {
  await t.navigateTo(`${resourcePath}/${csvPageSuffix}`)
}

export async function searchContacts(email) {
  await t
    .typeText(Selector('#email'), `${email}`, { replace: true }) // Search by email.
    .pressKey('enter')// Search update target.
}

export async function selectContacts(name) {
  await t.click(Selector('.ant-table-row.ant-table-row-level-0').find('td').withText(name)) // Pick the search result.
}

export async function createContacts(name, email, page_text) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true }) // The input to a search form.
    .click(Selector('.ant-form-item-no-colon').withText('メール'))
    .typeText(Selector('#email'), `${email}`, { replace: true })
    .click(Selector('#organization'))
    .pressKey('enter') // select dropdown items.
    .click(Selector('#staff'))
    .pressKey('enter') // select dropdown items.
    .click(Selector('#last_visit'))
    .pressKey('enter') // select dropdown items.
    .click(Selector('button[type="submit"]'))// submit
    .click(Selector('#willing_to_receive_ads'))
    .click(Selector('button[type="submit"]'))// submit
}

export async function updateContacts(name) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true }) // The input to a search form.
    .click(Selector('button[type="submit"]'))// submit // execute update
}

export async function deleteContacts() {
  await t
    .click(Selector('.ant-btn-danger'))
    .pressKey('enter') // execute delete
}

export async function updateColumnSettings() {
  await t
    .click(Selector('button[type="button"]').withText('表示項目変更'))
    .click(Selector('.ant-table-row.ant-table-row-level-0[data-row-key="display_name"]').find('input[type="checkbox"]'))
    .click(Selector('button[type="button"]').withText('OK'))
}

export async function uploadCsv(file_name) {
  await t
    .setFilesToUpload('input[type="file"]', [`${file_name}`])
}
