import { Selector, t } from 'testcafe';

const resourcePath = 'scheduledMails';
const registerPageSuffix = 'register';

const step_to_next = '次へ進む';
const search = '検 索';
const reserve = '配信予約';
const ok = 'OK';
const additional_date_to_send = '時刻を追加';

export async function accessScheduledMailsSearchPage() {
  await t.navigateTo(`${resourcePath}`)
}

export async function accessScheduledMailsRegisterPage() {
  await t.navigateTo(`${resourcePath}/${registerPageSuffix}`)
}

export async function searchScheduledMails(mail_subject) {
  await accessScheduledMailsSearchPage()
  await t
    .typeText(Selector('#subject'), `${mail_subject}`, { replace: true })
    .click(Selector('button[type="submit"]')) // search mail
}

export async function selectScheduledMails(mail_subject) {
  await searchScheduledMails(mail_subject)
  await t.click(Selector('.ant-table-row.ant-table-row-level-0').find('td')) // select mail
}

export async function createScheduledMails(mail_subject, mail_text, file_name) {
  await accessScheduledMailsRegisterPage()
  await t
    .typeText(Selector('#subject'), `${mail_subject}`, { replace: true })
    .typeText(Selector('#text'), `${mail_text}`, { replace: true })
    .click(Selector('.ant-calendar-picker-input.ant-input'))
    .click(Selector('.ant-calendar-date').withText('1'))
    .click(Selector('#subject'))
    .click(Selector('button[type="button"]').withText(`${additional_date_to_send}`))
    .click(Selector('#additional_date_to_send1'))
    .click(Selector('.ant-calendar-date').withText('2'))
    .click(Selector('#subject'))
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // update mail contains
    .setFilesToUpload('input[type="file"]', [`${file_name}`])
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // upload attachment file
    .click(Selector('button[type="submit"]').withText(`${search}`)) // search user
    .click(Selector('.ant-table-row.ant-table-row-level-0').find('.ant-checkbox-input')) // select user
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // update user
    .click(Selector('button[type="submit"]').withText(`${reserve}`)) // submit
    .click(Selector('button[type="button"]').withText(`${ok}`)) // OK
}

export async function updateScheduledMails(mail_subject, mail_text, file_name) {
  await selectScheduledMails(mail_subject)
  await t
    .typeText(Selector('#subject'), `${mail_subject}`, { replace: true })
    .typeText(Selector('#text'), `${mail_text}`, { replace: true })
    .click(Selector('.ant-calendar-picker-input.ant-input'))
    .click(Selector('.ant-calendar-date').withText('3'))
    .click(Selector('#subject'))
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // update mail contains
    .setFilesToUpload('input[type="file"]', [`${file_name}`])
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // upload attachment file
    .click(Selector('button[type="submit"]').withText(`${search}`)) // search user
    .click(Selector('.ant-table-row.ant-table-row-level-0').find('.ant-checkbox-input')) // select user
    .click(Selector('button[type="button"]').withText(`${step_to_next}`)) // update user
    .click(Selector('button[type="submit"]').withText(`${reserve}`)) // submit
    .click(Selector('button[type="button"]').withText(`${ok}`)) // OK
}

export async function deleteScheduledMails(mail_subject) {
  await selectScheduledMails(mail_subject)
  await t
    .click(Selector('.ant-btn-danger'))
    .pressKey('enter') // execute delete
}
