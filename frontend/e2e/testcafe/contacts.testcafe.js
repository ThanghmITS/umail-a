// TODO: Remove duplications between other tests.
import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import { getPageUrl } from './helpers';
import { accessContactsSearchPage, accessContactsRegisterPage, searchContacts, selectContacts, createContacts, updateContacts, deleteContacts, updateColumnSettings, accessContactsCsvUploadPage, uploadCsv } from './helpers/contacts'

const errorAlert = Selector('.ant-message-error');
const resourcePath = 'contacts';
const generateResourceName = 'hogefuga@example.com';
const contact_last_name = 'hoge';
const page_text = 'メール配信プロファイル';
const replace_last_name = 'fuga';

const filename = '../../../static/app_staffing/contacts.csv';
const filename_jp = './files/取引先担当者.csv';
const csv_contact_email = 'test_contact1@example.com'
const csv_contact_display_name = 'テスト担当者1'

fixture`Contacts`
  .page`http://127.0.0.1:8000/${resourcePath}`;



export async function assertAlertNotExists() {
  await t.expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
}

export async function assertListPage() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertDetailPage() {
  await t
    .expect(getPageUrl()).match(new RegExp(`${resourcePath}/[a-zA-Z0-9-]+$`))
}

export async function assertCreatedResource() {
  await t.expect(Selector('.app').textContent).contains(`${generateResourceName}`); // and make sure that the list page contains a created item.
}

export async function assertColumnSettingUpdated() {
  await t
    .expect(Selector('.ant-table-column-has-actions.ant-table-column-has-sorters').withText('取引先担当者').exists).ok('colimun setting does not loaded')
    .expect(Selector('.ant-table-column-has-actions.ant-table-column-has-sorters').withText('Email').exists).notOk('colimun setting does not loaded')
}


export async function testCreate(role) {
  await t.useRole(role)
  await accessContactsRegisterPage()
  await assertAlertNotExists()
  await createContacts(contact_last_name, generateResourceName, page_text)
  await assertListPage()
  await assertCreatedResource()
}

export async function testUpdateColumnSetting(role) {
  await t.useRole(role)
  await accessContactsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchContacts(generateResourceName)
  await updateColumnSettings()
  await assertColumnSettingUpdated()
}

export async function testUpdate(role) {
  await t.useRole(role)
  await accessContactsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchContacts(generateResourceName)
  await selectContacts(contact_last_name)
  await assertDetailPage()
  await updateContacts(replace_last_name)
  await assertListPage()
}

export async function testDelete(role) {
  await t.useRole(role)
  await accessContactsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchContacts(generateResourceName)
  await selectContacts(replace_last_name)
  await assertDetailPage()
  await deleteContacts()
  await assertListPage()
}

export async function testUploadCsv(role, filename) {
  await t.useRole(role)
  await accessContactsCsvUploadPage()
  await assertAlertNotExists()
  await uploadCsv(filename)
}

export async function testDeleteCsvData(role) {
  await t.useRole(role)
  await accessContactsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchContacts(csv_contact_email)
  await selectContacts(csv_contact_display_name)
  await assertDetailPage()
  await deleteContacts()
  await assertListPage()
}


test('testCreate', async (t) => {
  await testCreate(testUser)
});

test('testSearch', async (t) => {
  await t.useRole(testUserTennantB)
  await accessContactsSearchPage()
  await searchContacts(generateResourceName)
  await t
    .expect(Selector('.ant-table-row.ant-table-row-level-0').find('td').exists).notOk('cross acount test fail.')
});

test('testUpdateColumnSetting', async (t) => {
  await testUpdateColumnSetting(testUser)
});

test('testCreateTennantB', async (t) => {
  await testCreate(testUserTennantB)
});

test('testUpdate', async (t) => {
  await testUpdate(testUser)
});

test('testUpdateTennantB', async (t) => {
  await testUpdate(testUserTennantB)
});

test('testDelete', async (t) => {
  await testDelete(testUser)
});

test('testDeleteTennantB', async (t) => {
  await testDelete(testUserTennantB)
});

test('testUploadCsv', async (t) => {
  await testUploadCsv(testUser, filename)
});

test('testDeleteCsvData', async (t) => {
  await testDeleteCsvData(testUser)
});

test('testUploadCsvJP', async (t) => {
  await testUploadCsv(testUser, filename_jp)
});

test('testDeleteCsvData', async (t) => {
  await testDeleteCsvData(testUser)
});
