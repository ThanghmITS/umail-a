import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import { getPageUrl } from './helpers';
import { searchScheduledMails, createScheduledMails, updateScheduledMails, deleteScheduledMails } from './helpers/scheduledMails';

const resourcePath = 'scheduledMails';

const mail_subject = 'A sample scheduled email';
const mail_text = 'This is a text of an email.';

const file_name = './files/boo.jpg';

const updated = '更新されました'

fixture`ScheduledMails`
  .page`http://127.0.0.1:8000/${resourcePath}`;


export async function assertUrl() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertUpdateMessage() {
  await assertUrl()
  await t.expect(Selector('.ant-message').textContent).contains(`${updated}`)
}

export async function testCreate(role) {
  await t.useRole(role)
  await createScheduledMails(mail_subject, mail_text, file_name)
}

export async function testUpdate(role) {
  await t.useRole(role)
  await updateScheduledMails(mail_subject, mail_text, file_name)
}

export async function testDelete(role) {
  await t.useRole(role)
  await deleteScheduledMails(mail_subject)
}


test('testRegister', async (t) => {
  await testCreate(testUser)
});

test('testSearch', async (t) => {
  await t.useRole(testUserTennantB)
  await searchScheduledMails(mail_subject)
  await t
    .expect(Selector('.ant-table-row.ant-table-row-level-0').find('td').exists).notOk('cross acount test fail.')
});

test('testRegister Tennat B', async (t) => {
  await testCreate(testUserTennantB)
});

test('testUpdate', async (t) => {
  await testUpdate(testUser)
});

test('testUpdate Tennant B', async (t) => {
  await testUpdate(testUserTennantB)
});

test('testDelete', async (t) => {
  await testDelete(testUser)
});

test('testDelete Tennant B', async (t) => {
  await testDelete(testUserTennantB)
});
