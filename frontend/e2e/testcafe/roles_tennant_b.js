import { Role } from 'testcafe';
import { TEST_PASSWORD_TENNANT_B, TEST_USER_TENNANT_B } from './constants';

const testUserTennantB = Role('http://127.0.0.1:8000/login', async (t) => {
  await t
    .typeText('#username', TEST_USER_TENNANT_B)
    .typeText('#password', TEST_PASSWORD_TENNANT_B)
    .pressKey('enter');
});

export default testUserTennantB;
