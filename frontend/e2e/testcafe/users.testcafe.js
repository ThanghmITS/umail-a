// TODO: Remove duplications between other tests.
import { Selector, t } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';
import { getPageUrl } from './helpers';
import { createUser, updateUser, deleteUser } from './helpers/users';

const resourcePath = 'users';

const last_name = 'last';
const first_name = 'first';
const email = 'hogefuga@example.com';
const tel = '01-2345-6789';
const password = 'hogehoge';

fixture`Users`
  .page`http://127.0.0.1:8000/${resourcePath}`;


export async function assertUrl() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertCreatedResource() {
  await assertUrl()
  await t.expect(Selector('.app').textContent).contains(`${email}`); // and make sure that the list page contains a created item.
}

export async function assertResourceAlreadyCreated() {
  await t.expect(Selector('.ant-form-explain').textContent).contains('A user with that username already exists.');
}

export async function testCreate(role) {
  await t.useRole(role)
  await createUser(last_name, first_name, email, tel, password)
  await assertCreatedResource()
}

export async function testUpdate(role) {
  await t.useRole(role)
  await updateUser(email)
  await assertUrl()
}

export async function testDelete(role) {
  await t.useRole(role)
  await deleteUser(email)
  await assertUrl()
}

export async function testCreateDuplicatedUser(role) {
  await t.useRole(role)
  await createUser(last_name, first_name, email, tel, password)
  await assertResourceAlreadyCreated()
}

test('testCreate', async (t) => {
  await testCreate(testUser)
});

test('testCreateDuplicatedUser', async (t) => {
  await testCreateDuplicatedUser(testUserTennantB)
});

test('testUpdate', async (t) => {
  await testUpdate(testUser)
});

test('testDelete', async (t) => {
  await testDelete(testUser)
});

test('testCreateTennantB', async (t) => {
  await testCreate(testUserTennantB)
});

test('testUpdateTennantB', async (t) => {
  await testUpdate(testUserTennantB)
});

test('testDeleteTennantB', async (t) => {
  await testDelete(testUserTennantB)
});
