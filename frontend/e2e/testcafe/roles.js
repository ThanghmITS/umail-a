import { Role } from 'testcafe';
import { TEST_PASSWORD, TEST_USER } from './constants';

const testUser = Role('http://127.0.0.1:8000/login', async (t) => {
  await t
    .typeText('#username', TEST_USER)
    .typeText('#password', TEST_PASSWORD)
    .pressKey('enter');
});

export default testUser;
