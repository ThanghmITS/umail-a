import { t, Selector } from 'testcafe';

import testUser from './roles';
import testUserTennantB from './roles_tennant_b';

import { getPageUrl } from './helpers';
import { accessDashboard } from './helpers/dashboard';

const errorAlert = Selector('.ant-message-error');

const resourceUrl = 'http://127.0.0.1:8000/dashboard';

const mail_trend = '共有メールの受信件数';
const mail_reply = '共有メールへの返信件数';
const mail_ranking = 'メールの送信元ランキング(先月分)';
const organization_trend = '取引先の新規登録数';

fixture`Dashboard`
  .page`${resourceUrl}`;

export async function assertDashboardIndexContains() {
  await t
    .expect(getPageUrl()).match(new RegExp(`^${resourceUrl}$`))
    .expect(Selector('.app').textContent).contains(`${mail_trend}`)
    .expect(Selector('.app').textContent).contains(`${mail_reply}`)
    .expect(Selector('.app').textContent).contains(`${mail_ranking}`)
    .expect(Selector('.app').textContent).contains(`${organization_trend}`);
};

export async function assertIndex(account) {
  await t
    .useRole(account).expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
  await accessDashboard()
  await assertDashboardIndexContains()
};

test('index', async (t) => {
  await assertIndex(testUser)

});

test('index TennantB', async (t) => {
  await assertIndex(testUserTennantB)
});
