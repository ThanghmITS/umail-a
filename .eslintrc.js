module.exports = {
    parser: "babel-eslint",
    env: {
        browser: true,
        es6: true,
        jest: true,
    },
    extends: "airbnb",
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: "module",
    },
    plugins: ["react", "jest", "testcafe"],
    rules: {
        "no-console": 0,
        "no-underscore-dangle": 0,
        "max-len": "off",
        indent: "off",
        "react/jsx-indent": "off",
        "react/jsx-indent-props": "off",
        quotes: "off,",
    },
    settings: {
        "import/resolver": {
            alias: {
                map: [["~/", "./frontend/src/"]],
                extensions: [".js", ".jsx", ".ts", ".tsx"],
            },
        },
    },
};
